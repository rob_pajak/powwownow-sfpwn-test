
<div class="grid_24" id="raf_registration">
    <div id="raf_disclaimer">
        <h2>Talk about easy!</h2>
        <p>Just fill in the form to generate your unique PIN (don’t worry – we’ll never share your personal details with third parties). Once you have a PIN, you can start conference calling straight away. We’ll also send you a free welcome pack containing a fun Toff Trumps game, personalised PIN stickers and some tips for using our service. Lovely!</p>
    </div>
    <div id="raf_form_container">
        <?php echo form_tag(url_for($action), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-registration', 'class' => 'pwnform clearfix')); ?>  

            <div class="form-field">
                <?php echo $form['first_name']->renderLabel('First Name'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['first_name']->render(array('value' => $first_name)); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['last_name']->renderLabel('Last Name'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['last_name']->render(array('value' => $last_name)); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['company']->renderLabel('Company'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['company']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['industry']->renderLabel('Industry'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['industry']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['address_line_one']->renderLabel('Address Line 1'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['address_line_one']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['address_line_two']->renderLabel('Address Line 2'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['address_line_two']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['town']->renderLabel('Town'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['town']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['postcode']->renderLabel('Postcode'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['postcode']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['country']->renderLabel('Country'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['country']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['phone']->renderLabel('Phone'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['phone']->render(); ?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['email']->renderLabel('Email'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['email']->render(array('value' => $email)); ?>
                </span>
            </div>
            <input type="hidden" name="registration[contact_ref]" value="<?php echo $contact_ref ?>" />
            <div class="form-action">
                <button class="button-orange" type="submit" tabindex="14">
                    <span><?php echo __('Generate PIN'); ?></span>
                </button>
            </div>

        </form>

    </div>

</div>
<div class="grid_24">
    <em>*   Required fields</em><br/>
    <em>**  Welcome packs only available to UK residents</em>
</div>
<script type="text/javascript">
   
    $(document).ready(function () {

        // Default GBR
        $('#registration_country [value="GB"]').attr('selected', 'selected');

        var options = {
            successMessagePos: 'off',
            debug: 'on',
            html5Validation: 'off',
            requiredAsterisk: 'on',
            success: function (responseText) {
                var resp = $.parseJSON(responseText);
                window.location = '/s/Registration-Complete/' + resp.pin;
            }
        };
        $('#frm-registration').initMypwnForm(options);
    });
     
</script>