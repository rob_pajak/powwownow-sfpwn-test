<?php include_component('commonComponents', 'header'); ?>
<style>
    ol#raf-tc { margin: 20px 0;}
    ol#raf-tc li {
        margin-bottom: 10px;
    }

</style>
<div class="container_24 clearfix">

    <div class="grid_6">
        &nbsp;
    </div>
    <div class="grid_18">
        <div class="margin-top-10">
            <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => $breadcrumbs,'title' => __('Terms and Conditions'))) ?>
        </div>
        <div class="margin-top-20">
            <?php include_component('commonComponents', 'genericHeading', array('title' => __('Terms and Conditions'), 'headingSize' => 'xxl', 'user_type' => 'powwownow')); ?>
        </div>

        <h3>Powwownow Recommend a Friend: Terms and Conditions </h3>
        <p>Before entering Powwownow’s Recommend a Friend Prize Draw, please read the following Terms and Conditions.</p>
        <p>THIS PRIZE DRAW IS INTENEDED FOR ENTRY FROM THOSE IN THE UK ONLY AND SHALL BE CONSTRUCTED AND EVALUATED ACCORDING TO THE LAWS OF THE UNITED KINGDOM. DO NOT ENTER INTO THIS PRIZE DRAW UNLESS YOU ARE A LEGAL RESIDENT OF THE UNITED KINGDOM AND 18 YEARS OF AGE OR OLDER. YOU CANNOT ENTER IF YOU ARE AN EMPLOYEE OF POWWOWNOW, THEIR FAMILIES, AGENTS, PARTNERS, ASSOCIATES OR ANYONE ELSE PROFESSIONALLY ASSOCIATED WITH THE DRAW.</p>

        <ol id="raf-tc">
            <li>Details of how to enter form part of the Terms and Conditions. It is a condition of entry that all rules are accepted as final and that the person entering agrees to abide by these rules. The decision by the judges is final and correspondence will not be entered into.</li>
            <li>By recommending a friend to use the Powwownow service you automatically accept these Terms and Conditions.</li>
            <li>Entries that are late, illegible, incomplete, defaced or corrupt will not be accepted.</li>
            <li>The winners will be drawn at random from all correct entries received by the closing date. All entries must be received by 5pm on Thursday 8 November 2012.</li>
            <li>The prize draw will take place on Friday 9 November 2012 and the winner will be notified within 7 days of the closing date (8.11.12)</li>
            <li>The prize includes £300 credit to use with Secret Escapes at www.secretescapes.com and here only. Credit will be uploaded by Secret Escapes once you have registered with Secret Escapes online. The £300 credit can be used wholly or as a contributing fund to a break of your choice in the UK or abroad and must be used within 12 months of the prize draw end date. Date of credit expiry 8.11.13.</li>
            <li>All elements of the prizes are subject to availability, non-transferable and there are no cash alternatives. Travel to and from your chosen destination is subject to the offer decided by the entrant. Not all Secret Escapes come with personal travel. The prizes do not include personal expenditure.</li>
            <li>Powwownow is responsible for the Prize Draw but does not accept liability if Secret Escapes is to liquidate, or go out of business. Powwownow is not responsible for the quality of the hotel or experience whilst on the break and if the entrant feels that a complaint should be made about the break, this should be made directly to Secret Escapes.</li>
        </ol>
        <p><strong>Promoter:</strong> Powwownow, First Floor, Vectra House, 36 Paradise Road, Richmond, TW9 1SE</p>
        <p><strong>Timing:</strong> The Prize Draw begins on 24.11.12 at 11:00 Greenwich Mean Time ("GMT") and ends on 8.11.12 at 17:00 GMT (the "Constant Period"). The winner will be notified within 7 days of the closing date.</p>
        <p><strong>How to enter:</strong> Entry will be automatic when you Recommend a Friend to Powwownow.</p>
        <p><strong>Limit:</strong> Each Entrant may enter one submission per recommendation during the Prize Draw period.</p>
        <p><strong>General Rules:</strong> By receipt of any prize, winner hereby waives and releases, and agrees to hold harmless Powwownow, and any of its affiliated and subsidiary companies, authorised dealers, licensees, distributors, advertising and promotion agencies and all of their respective officers, directors, employees, representatives and agents, (collectively, the "Released Parties") from and against any and all rights, claims and causes of action whatsoever that he or she may have, or which may arise, against any of them for any liability for any matter, cause or thing whatsoever, including but not limited to any injury, loss, damage, travel complications, whether direct, compensatory, incidental or consequential, to person, including death and property, arising in whole or in part, directly or indirectly, from his or her acceptance, possession, use or misuse of the prize, his or her participation in the Prize Draw, or his or her participation in any Prize Draw, prize or prize-related activity. Winner also further acknowledges that Released Parties have neither made, nor are in any manner responsible or liable for, any warranty, representation or guarantee, express or implied, in fact or in law relative to any prize or this Prize Draw including but not limited to quality, condition or fitness for a particular purpose.</p>
        <p>By entering or participating in the Prize Draw, Entrants agree to be bound by these Official Rules and waive any right to claim any ambiguity or error in these Official Rules or the Prize Draw itself. Promoter is not responsible for: (1) any incorrect or inaccurate information, whether caused by Entrants, or by any of the holding company of Secret Escapes; (3) unauthorised human intervention in any part of the entry process or the Entrant; or (4) technical or human error which may occur in the administration of the Entrant or the processing of entries. Further, if, for any reason, the Entrant is not capable of attending the weekend break due to illness, bereavement, natural disasters, and/or work commitments. The Promoter will not be held accountable for any other reason why the Entrant is not able to claim their prize.</p>
        <p>Sponsor reserves the right, in its sole discretion, to disqualify any individual it finds to be tampering with the entry process or the operation of the Entrant or to be acting in violation of these Official Rules or those of any other promotion, or in an unsportsmanlike or disruptive manner. Any attempt by any person to deliberately damage any website or undermine the legitimate operation of the Entrant is a violation of criminal and civil law, and, should such an attempt be made,  the Promoter reserves the right to seek damages from any such person to the fullest extent permitted by law.</p>
        <p>Promoter's failure to enforce any term of these Official Rules shall not constitute a waiver of that provision. No prize substitution, cash redemption or transfer of prize will be permitted, however Promoter reserves the right to substitute a prize of equal or greater value at Promoter’s sole discretion.</p>
        <p>Entrant's Personal Information: Information collected from Entrants is subject to Promoter's privacy policy.</p>
        <p>Winner List: For a Winner List, send a hand-printed, self-addressed, stamped envelope to "Powwownow, First Floor, Vectra House, 36 Paradise Road, Richmond, TW9 1SE”</p>

    </div>
</div>
<div class="container_24">
    <?php include_component('commonComponents', 'footer'); ?>    
</div>