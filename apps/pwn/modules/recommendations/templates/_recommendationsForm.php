<?php echo form_tag(url_for($action), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-recommendations', 'class' => 'pwnform clearfix')); ?>  


<style>
    
    div.recommend-form-row {
        padding: 10px;
        border-radius: 10px;
    }

    div.recommend-form-row .form-field {
        float: left;
        width: 33%;
    }

    div.recommend-form-row .form-field label {
        display: block;
    }

    #frm-recommendations .error {
        background-color: white !important;
        color: #515151 !important;
    }

</style>

<?php for ($i=1; $i < 4; $i++) : ?>
    
<div class="recommend-form-row clearfix">
    <div class="form-field" style="z-index: 200; position: relative;">
        <?php echo $form['friend' . $i . '_first_name']->renderLabel(); ?>
        <span class="mypwn-input-container">
            <?php echo $form['friend' . $i . '_first_name']->render(); ?>
        </span>
    </div>
    <div class="form-field" style="z-index: 190; position: relative">      
        <?php echo $form['friend' . $i . '_last_name']->renderLabel(); ?>
        <span class="mypwn-input-container">
            <?php echo $form['friend' . $i . '_last_name']->render(); ?>
        </span>
    </div>
    <div class="form-field" style="z-index: 180; position: relative">      
        <?php echo $form['friend' . $i . '_email']->renderLabel(); ?>
        <span class="mypwn-input-container">
            <?php echo $form['friend' . $i . '_email']->render(); ?>
        </span>
    </div>  
</div>

<?php endfor; ?>




<div class="grid_12 alpha form-badge">


    <h2><?php echo __('Your details'); ?></h2>

    <div class="form-field">
        <?php echo $form['first_name']->renderLabel(); ?>
        <span class="mypwn-input-container">
            <?php echo $form['first_name']->render(array('value' => $first_name)); ?>
        </span>
    </div>    

    <div class="form-field">
        <?php echo $form['last_name']->renderLabel(); ?>
        <span class="mypwn-input-container">
            <?php echo $form['last_name']->render(array('value' => $last_name)); ?>
        </span>
    </div>        

    <div class="form-field email-field">
        <?php echo $form['email']->renderLabel(); ?>
        <span class="mypwn-input-container">
            <?php echo $form['email']->render(array('value' => $email)); ?>
        </span>
    </div>     

    <?php echo $form['contact_ref']->render(array('value' => $contact_ref)); ?>

    <button class="button-orange" type="submit" tabindex="12">
        <span><?php echo __('Submit'); ?></span>
    </button>    

</div>

<img src="/sfimages/recommendations/secret-escapes-logo.png" alt="Install" class="secret-escapes-logo" />

</form>

<?php include_partial('thankYou'); ?>  

<script type="text/javascript">
   

     
</script>
