<?php echo use_helper('pwnForm'); ?>
<?php include_component('commonComponents', 'header'); ?>

<?php use_stylesheet('/sfcss/pages/recommendations.css') ?>
<script src="/sfjs/pages/recommendations.js"></script>
<script>
    $(function() {
        Recommendations.initialize();
    });
</script>

<div class="container_24">

    <div class="grid_24 alpha omega breadcrumbs">        
        <?php
        include_partial('commonComponents/headerBreadcrumbs', array(
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
            'grid' => 24,
            'style' => false
        ));
        ?>
    </div>

    <div class="grid_24 share-a-secret clearfix">    

        <img src="/sfimages/recommendations/chateau.jpg" alt="Install" class="chateau" />

        <h1 class="rockwell blue"><?php echo __('Share a secret and win &pound;300 towards your own secret escape'); ?></h1>
        <p><?php echo __('Who said secrets had to be kept? Introduce up to three of your chatty chums to our free conference calling service to be in with a chance of winning &pound;300 towards a luxury getaway, courtesy of Secret Escapes. Each name you provide gets you an additional entry into the prize draw. Simply enter your friend\'s details below and cross your fingers and toes!'); ?></p>

        <?php
        include_partial('recommendationsForm', array(
            'form' => $form,
            'action' => 'recommendations/sendRecommendations',
            'contact_ref' => $contact_ref,
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name
        ));
        ?>
    </div>

    <div class="grid_24">
        <br/>
        <em>*   Required fields</em><br/><br/>
        <a href="/s/Recommend-A-Friend-TC" target="_blank">Click here to view terms and conditions</a>
    </div>
</div>

<div class="container_24">
    <?php //include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>    

</div>


