<?php echo use_helper('pwnForm'); ?>
<?php include_component('commonComponents', 'header'); ?>
<?php use_stylesheet('/sfcss/pages/recommendations.css') ?>

<div class="container_24 invitee clearfix">
    
    <?php
    include_partial('registrationForm', array(
        'form' => $form,
        'action' => '/s/recommendations/Registration-Complete',
        'contact_ref' => $contact_ref,
        'email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name
    ));
    ?>

    <?php // include_component('commonComponents', 'socialMediaFooter'); ?>
</div>    
<div class="container_24">
    <?php include_component('commonComponents', 'footer'); ?>    

</div>