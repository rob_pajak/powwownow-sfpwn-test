<?php

class recommendationsActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {

        $this->breadcrumbs = array();
        $this->title = 'Recommend A Friend';
        $this->form = new recommendationsForm();
        $this->contact_ref = $request->getParameter('contact_ref');
        $this->email =  $request->getParameter('email');                
        $this->first_name = $request->getParameter('first_name');
        $this->last_name = $request->getParameter('last_name');
    }

    public function executeSendRecommendations(sfWebRequest $request) {

        $post = $request->getPostParameters();

        for ($i = 1; $i <= 3; $i++) {

            if (strlen(trim($post['recommendations']['friend' . $i . '_email'])) === 0)
                continue;

            // store recommendation in db and send an email
            
            $args = array(
                'contact_ref' => $post['recommendations']['contact_ref'],
                'email' => $post['recommendations']['email'],
                'first_name' => $post['recommendations']['first_name'],
                'last_name' => $post['recommendations']['last_name'],
                'friend_email' => $post['recommendations']['friend' . $i . '_email'],
                'friend_first_name' => $post['recommendations']['friend' . $i . '_first_name'],
                'friend_last_name' => $post['recommendations']['friend' . $i . '_last_name']
            );

            $r = Hermes_Client_Rest::call('Recommendations.createRecommendation', $args);
            
        }

        return sfView::NONE;
    }

    public function executeRegistration(sfWebRequest $request) {
        
        $this->contact_ref = $request->getParameter('contact_ref');
        $this->form = new recommendationsRegistrationForm();

        $this->email =  $request->getParameter('email');                
        $this->first_name = $request->getParameter('first_name');
        $this->last_name = $request->getParameter('last_name');
    }

    public function executeRegistrationComplete(sfWebRequest $request) {
        $this->getContext()->getConfiguration()->loadHelpers('countryCodes');
        
        $post = $request->getPostParameters();

        try {

            $apiResponse = Hermes_Client_Rest::call('doBasicRegistration', array(
                'email' => $post['registration']['email'],
                'locale' => 'en_GB',
                'source' => '/RAF'
            ));

        } catch(Hermes_Client_Exception $e) {
            switch ($e->getCode()) {
                case '001:000:001':
                    $errorMessages[] = array('message' => 'API_RESPONSE_EMAIL_ALREADY_TAKEN', 'field_name' => 'registration[email]');
                    break;
                case '001:000:007':
                    $errorMessages[] = array('message' => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE', 'field_name' => 'alert');
                    break;
                default:
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }

            $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
            $this->getResponse()->setContentType('application/json');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        try {
            if (!empty($apiResponse['pin']['pin_ref'])) {
                $goal = 'conferencing';
                try{
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                    PWN_Logger::log('pin:'.$apiResponse['pin']['pin_ref'].' goal: '.$goal.' '.__FILE__ . '['.__LINE__.']', PWN_Logger::INFO, 'trackerLog');
                } catch(Exception $e) {

                }
                PWN_Tracking_GoalReferrers::achieved('conferencing', $apiResponse['pin']['pin_ref'], true);
            } else {
                $this->logMessage('After a registration, pin ref was null: ' . serialize($apiResponse), 'err');
            }
        } catch (Exception $e) {
            $this->logMessage('Could not track goal referrers: ' . serialize($e), 'err');
        }

        $userContactRef = $apiResponse['contact']['contact_ref'];
        $userPinRef = $apiResponse['pin']['pin_ref'];
        $userCountry = two2three($post['registration']['country']); 
        $pin = $apiResponse['pin']['pin'];

        // Update Contact with the Remaining Information
        try {
            Hermes_Client_Rest::call('updateContact', array(
                'contact_ref' => $userContactRef,
                'recommended_by' => $post['registration']['contact_ref'],
                'first_name' => $post['registration']['first_name'],
                'last_name' => $post['registration']['last_name'],
                'organisation' => $post['registration']['company'],
                'business_phone' => $post['registration']['phone'],
                'business_category' => $post['registration']['industry']
            ));
        } catch (Hermes_Client_Exception $e) {
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
            $this->getResponse()->setContentType('application/json');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Create New Address
        try {
            Hermes_Client_Rest::call('createAddress', array(
                'ref' => $userContactRef,
                'table' => 'contact',
                'address_type' => 'Company',
                'organisation' => $post['registration']['company'],
                'building' => $post['registration']['address_line_one'],
                'street' => $post['registration']['address_line_two'],
                'town' => $post['registration']['town'],
                'postal_code' => $post['registration']['postcode'],
                'country' => $userCountry
            ));
        }  catch (Hermes_Client_Exception $e) {
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
            $this->getResponse()->setContentType('application/json');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Request wallet card - only for UK residents
        if ($userCountry == 'GBR') {
            
            try {
                Hermes_Client_Rest::call('createWalletCardRequest', array(
                    'contact_ref' => $userContactRef,
                    'pin_ref' => $userPinRef,
                    'organisation' => $post['registration']['company'],
                    'building' => $post['registration']['address_line_one'],
                    'street' => ($post['registration']['address_line_two'])?$post['registration']['address_line_two']:'',
                    'town' => $post['registration']['town'],
                    'county' => '',
                    'post_code' => $post['registration']['postcode'],
                    'country_code' => $userCountry
                ));
            } catch (Hermes_Client_Exception $e) {
                $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR_3', 'field_name' => 'alert');
                $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
                $this->getResponse()->setContentType('application/json');
                // @todo: why not echo out error messages?
                //echo json_encode(array('error_messages' => $errorMessages));
                echo $e->getResponseBody();
                return sfView::NONE;
            }
        }

        // echo PIN out if everything goes well
        echo json_encode(array('pin' => $pin));
        return sfView::NONE;
    }
    

    public function executeTerms(sfWebRequest $request) {
        $this->breadcrumbs = array();
    }

}
