<?php

/**
 * Collects AJAX response messages and HTTP status.
 */
class AJAXResponse {

    /**
     * @var sfResponse
     */
    protected $response;

    /**
     * Actual content to be encoded.
     *
     * @var array
     */
    protected $responseContent = array();

    protected $errorKey;
    protected $genericErrorCode;
    protected $forbiddenErrorCode;
    protected $closed = false;
    protected $responseCode = 200;
    protected $responseContentType;

    public function __construct(sfResponse $response, $errorKey = 'error_messages', $genericErrorCode = 400, $forbiddenErrorCode = 403, $responseContentType = 'application/json') {
        $this->response = $response;
        $this->errorKey = $errorKey;
        $this->genericErrorCode = $genericErrorCode;
        $this->forbiddenErrorCode = $forbiddenErrorCode;
        $this->responseContentType = $responseContentType;
    }

    public function getErrorCode() {
        return $this->genericErrorCode;
    }

    public function getForbiddenCode() {
        return $this->forbiddenErrorCode;
    }

    /**
     * Adds content to the response; will be JSON-encoded in output method.
     *
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addContent($key, $value) {
        $this->responseContent[$key] = $value;
        return $this;
    }

    /**
     * Adds content to a nested container.
     *
     * @param string $parent
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addNestedContent($parent, $key, $value) {
        if (!isset($this->responseContent[$parent])) {
            $this->responseContent[$parent] = array();
        }
        $this->responseContent[$parent][$key] = $value;
        return $this;
    }

    /**
     * Indicates that the response should no longer be altered.
     *
     * @return bool
     */
    public function isClosed() {
        return $this->closed;
    }

    /**
     * Indicates that the response can still be altered.
     *
     * @return bool
     */
    public function isOpen() {
        return !$this->isClosed();
    }

    /**
     * Indicate that the response should no longer be altered.
     *
     * @return $this
     */
    public function close() {
        $this->closed = true;
        return $this;
    }

    /**
     * Outputs the JSON-encoded content, sets the content type, the status code and returns the default AJAX view key.
     *
     * @return string
     */
    public function outputResponse() {
        $this->response->setContentType($this->responseContentType);
        if (!empty($this->responseContent)) {
            echo json_encode($this->responseContent);
        }
        if ($this->responseCode !== 200) {
            $this->response->setStatusCode($this->responseCode);
        }
        return sfView::NONE;
    }

    /**
     * Sets the response HTTP Status code.
     *
     * @param int $code
     * @return $this
     */
    public function setStatuscode($code) {
        $this->responseCode = $code;
        return $this;
    }

    /**
     * Sets the response HTTP Status code to the error code.
     *
     * @return $this
     */
    public function setToGenericErrorCode() {
        return $this->setStatuscode($this->genericErrorCode);
    }

    /**
     * Sets the response HTTP Status code to the error code indicating a forbidden route.
     *
     * @return $this
     */
    public function setToForbiddenErrorCode() {
        return $this->setStatuscode($this->forbiddenErrorCode);
    }

    /**
     * Adds a new error message to the response content; optionally updating the response error code.
     *
     * @param array $error
     * @param null|int $newErrorCode
     *   If null is given, will set the error code to the error code (see: getErrorCode()).
     * @return $this
     */
    public function addErrorMessage(array $error, $newErrorCode = null) {
        // Add the error message.
        $this->initErrorMessages();
        $this->responseContent[$this->errorKey][] = $error;

        if (!$newErrorCode) {
            $newErrorCode = $this->getErrorCode();
        }
        $this->setStatuscode($newErrorCode);

        return $this;
    }

    public function setErrorMessages(array $errorMessages, $newErrorCode = null)
    {
        $this->initErrorMessages();
        if (!$newErrorCode) {
            $newErrorCode = $this->getErrorCode();
        }
        $this->setStatuscode($newErrorCode);
        $this->responseContent[$this->errorKey] = $errorMessages;
        return $this;
    }

    public function addAlertErrorMessage($message)
    {
        return $this->addErrorMessage(array('field_name' => 'alert', 'message' => $message));
    }

    /**
     * Adds a success message to the response content.
     *
     * @param string $message
     * @return $this
     */
    public function addAlertSuccessMessage($message)
    {
        return $this->addContent('success_message', $message)
            ->addContent('field_name', 'alert');
    }

    /**
     * Checks if the response has collected error messages.
     *
     * @return bool
     */
    public function hasErrors() {
        return isset($this->responseContent[$this->errorKey]);
    }

    protected function initErrorMessages() {
        if (!isset($this->responseContent[$this->errorKey])) {
            $this->responseContent[$this->errorKey] = array();
        }
    }

}
