<div class="grid_24 dialog-plus-main-content" id="plusExistingCustomerPremiumAdminAuth">
    <div class="grid_sub_22">
        <p>As a Premium customer you are unable to purchase a Bundle.</p>
        <p>For further assistance please contact our Customer Services on 0203 398 0398.</p>
    </div>
    <div class="grid_sub_1 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>