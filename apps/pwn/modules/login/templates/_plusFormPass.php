<div class="form-field">
    <label for="plus_signup_password">Password</label>
    <span class="mypwn-input-container">
        <input class="mypwn-input required" id="plus_signup_password" name="password" type="password" pattern="^.{6,}$" placeholder="Your password" />
    </span>
</div>
<div class="form-field">
    <label for="plus_signup_confirm_password">Confirm password</label>
    <span class="mypwn-input-container">
        <input class="mypwn-input required" id="plus_signup_confirm_password" name="confirm_password" type="password" pattern="^.{6,}$" placeholder="Retype your password" />
    </span>
</div>