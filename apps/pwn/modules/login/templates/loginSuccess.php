<?php $redirect = isset($redirect) ? $redirect : '/myPwn/';?>

<div class="row">
    <div class="small-12 medium-12 large-12 columns">
        <header class="logo">
            <a href="<?php echo url_for('@homepage');?>">
                <img src="/cx2/img/mobile/640_logo_strap_line.png" width="200" alt="Powwownow">
            </a>
        </header>
    </div>
</div>

<div class="row">
    <div class="small-12 medium-8 large-7 medium-centered large-centered columns">
        <section class="login-container">
            <h2 class="section-title">Log In to myPowwownow</h2>
            <p class="section-text">Access myPowwownow to find recordings, change your on-hold music and more...</p>
            <div id="js.login.container" data-bind="template: {name: 'login.container.data', afterAdd: setRedirect('<?php echo $redirect?>'), afterRender: setResetLink('<?php echo url_for('@forgotten_password')?>')}"></div>

            <script type="text/html" id="login.container.data">
                <form id="js-login-main-form" class='cx_form' data-bind="submit: doLogin" novalidate>

                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <input id="js-email" class="email-input" type='email' data-bind='textInput: model.login["email"]'
                                   placeholder="Enter your email address"/>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <input id="js-password" class="password-input" type='password' data-bind='textInput: model.login["password"]'
                                   placeholder="Password"/>
                        </div>
                    </div>

                    <div class="row collapse error-message" data-bind="fadeVisible: ui.errors.message().length > 0">
                        <div class="small-12 medium-12 large-12 columns">
                            <p data-bind="html: ui.errors.message()"></p>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <p class="right password-reminder"><a data-bind="attr: { href: computed.resetlink() }" href="<?php echo url_for('@forgotten_password')?>">What is my
                                    password?</a></p>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <img id="loader" src="/cx2/img/throbber.gif" alt="loading..." class="loader"
                                 data-bind="visible: ui.isEnabled.loader"/>
                            <button type="submit" class="right button-blue-set-a"
                                    data-bind="enable: ui.isEnabled.submit">Sign Me In
                            </button>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <div class="right">
                                <input type="checkbox" name="remember" value="KEEP_ME_LOGGED_IN" id="remember" data-bind='checked: model.login["remember"]'>
                                <label for="remember">Keep me signed in</label>
                            </div>
                        </div>
                    </div>
                </form>
            </script>
        </section>
    </div>
</div>

<div class="row">
    <div class="small-12 medium-8 large-7 medium-centered large-centered columns">
        <section class="new-customer-container">
            <p class="section-text"><span class="enlarge font_green">New Customer?</span> <a href="/?register=true">Click here</a> to generate your PIN</p>
        </section>
    </div>
</div>
