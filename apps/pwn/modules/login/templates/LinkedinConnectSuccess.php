<?php $rawUser = $liUser->getRawValue(); ?>
<?php $sf_response->setTitle(__('Free Conference Call | Conference Calls | Powwownow')); ?>
<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    
    <?php include_component('commonComponents', 'subPageHeader', array('title' => false, 'breadcrumbs' => false)); ?>
    <div class="grid_24 clearfix">
        <div>
            <?php echo link_to('back', '@mypwn_index'); ?>

            <div>
                id: <?php echo $rawUser->id; ?><br/>
                firstName: <?php echo $rawUser->firstName; ?><br/>
                lastName: <?php echo $rawUser->lastName; ?><br/>
                formattedName: <?php echo $rawUser->formattedName; ?><br/>
                educations: <?php echo $rawUser->educations->values[0]->schoolName; ?><br/>
                industry: <?php echo $rawUser->industry; ?><br/>
                location: <?php echo $rawUser->location->name; ?><br/>
                headline: <?php echo $rawUser->headline; ?><br/>
                emailAddress: <?php echo $rawUser->emailAddress; ?><br/>
            </div>
            <pre><?php //print_r($liUser); ?></pre>
            <div style="border: solid 1px blue;">
                log: <br/><?php echo strtr($log,array(';'=>'<br/>')); ?>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
