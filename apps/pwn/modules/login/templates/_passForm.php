<style>p.required-note {display:none;}</style>
<div class="floatright sprite plus-close" style="border-bottom: none" href="#" onclick="$(this).parent().dialog('close');">
    <span class="sprite-cross-white png"></span>
</div>
<h2>Enter your myPowwownow password</h2>
<form id="form-plus-switch-auth" method="post" enctype="application/x-www-form-urlencoded" action="/s/login/plusSignup" class="clearfix">
    <?php if (isset($email)) : ?><input type="hidden" name="email" id="plus_register_email" value="<?php echo $email; ?>"/><?php endif; ?>
    <?php if (!is_null($promoemail)) : ?><input type="hidden" id="promoemail" name="promoemail"  value="promo"/><?php endif; ?>
    <input type="hidden" id="trackingName" name="trackingName"  value="<?php echo (isset($trackingName)) ? $trackingName : 'Generic'; ?>"/>
    <span class="mypwn-input-container">
        <input class="mypwn-input required" id="plus_auth_password" name="password" type="password" placeholder="Your password"/>
    </span>
    <button class="button-orange floatright" type="submit" id="plus_switch_auth" style="position: relative; z-index: 1500;">
        <span>CONTINUE</span>
    </button><br/>
    <a href="<?php echo url_for('@forgotten_password'); ?>">Forgotten your password?</a>
    <p class="plus-reg-info">All of your existing Powwownow account settings will be transferred on to your Plus account, so there is nothing to worry about!</p>
</form>
<script>
$(function () {
    $('#form-plus-switch-auth').initMypwnForm({
        debug: 'on',
        messagePos: 'off',
        success: function (responseText) { 
            //console.log(responseText.html);
            $('#switch2plus').html(responseText.html);
        }
    });
    $('[name=password]').focus();
    dataLayer.push({'event': 'PlusAttemptPasswordForm/onLoad', 'page': '<?php echo (isset($trackingName)) ? $trackingName : 'Generic'; ?>'});
});
</script>