<style>p.required-note {display:none;} form#form-plus-signup .form-field{width:52%;}</style>
<div class="floatright sprite plus-close" style="border-bottom: none" href="#" onclick="$(this).parent().dialog('close');">
    <span class="sprite-cross-white png"></span>
</div>
<h2><?php if (isset($first_name)) : ?>Please provide us with a few details<?php else : ?>Please provide us with a few details<?php endif; ?></h2>
<form id="form-plus-signup" method="post" enctype="application/x-www-form-urlencoded" action="/s/login/enablePlusAccount" class="clearfix">
    <?php if (isset($email)) : ?><input type="hidden" name="email" id="plus_register_email" value="<?php echo $email; ?>"/><?php endif; ?>
    <?php if (isset($password)) : ?><input type="hidden" name="password" id="plus_register_password" value="<?php echo $password; ?>"/><?php endif; ?>
    <div class="form-field">
        <label for="plus_signup_first_name">First Name</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_first_name" name="first_name" type="text" placeholder="Your first name" value="<?php echo $first_name; ?>"/>
        </span>
    </div>
    <div class="form-field">
        <label for="plus_signup_last_name">Last Name</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_last_name" name="last_name" type="text" placeholder="Your last name" value="<?php echo $last_name; ?>"/>
        </span>
    </div>
    <div class="form-field">
        <label for="plus_signup_business_phone">Company</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_company_name" name="company_name" type="text" placeholder="Your company name" value="<?php echo $organisation; ?>"/>
        </span>
    </div>
    <div class="form-field">
        <label for="plus_signup_business_phone">Business Phone</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_business_phone" name="business_phone" type="text" placeholder="Business phone number" value="<?php echo $business_phone; ?>"/>
        </span>
    </div>