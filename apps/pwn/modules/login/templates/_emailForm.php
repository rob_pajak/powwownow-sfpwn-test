<div class="floatright sprite plus-close" style="border-bottom: none" href="#" onclick="$(this).parent().dialog('close');">
    <span class="sprite-cross-white png"></span>
</div>
<h2>Enter your email address</h2>
<form id="form-plus-switch-step1" method="post" enctype="application/x-www-form-urlencoded" action="/s/login/plusSignup" class="clearfix" style="position: relative">
    <span class="mypwn-input-container">
        <input class="mypwn-input email required" id="plus_register_email" name="email" type="text" maxlength="50" placeholder="Your email" value="<?php echo $promoemail; ?>"/>
        <?php if (!is_null($promoemail)) : ?><input id="promoemail" name="promoemail" type="hidden" value="promo"/><?php endif; ?>
        <input type="hidden" id="trackingName" name="trackingName"  value="<?php echo (isset($trackingName)) ? $trackingName : 'Generic'; ?>"/>
    </span>
    <button class="button-orange floatright" type="submit" id="plus_switch_step1" style="position: relative; z-index: 1500;">
        <span>CONTINUE</span>
    </button>
    <div class="form-field clearfix">
        <input type="checkbox" value="1" id="t_and_c" name="t_and_c" class="required" style="vertical-align: middle" />&nbsp;<span>I have read and agree to the <a class="white" title="Terms and Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>" target="_blank">terms &amp; conditions</a></span>
    </div>
</form>
<p class="plus-reg-info">You will receive all the great features of Powwownow for free, plus the option to purchase additional products and features to build a conference package to meet your business requirements.</p>
<script>
    $(function () {
        $('#form-plus-switch-step1').initMypwnForm({
            debug: 'off',
            messagePos: 'off',
            success: function (responseText) {
                $('#switch2plus').html(responseText.html);
            }
        });
        $('[name=email]').focus();
        dataLayer.push({'event': 'PlusAttemptEmailForm/onLoad', 'page': '<?php echo (isset($trackingName)) ? $trackingName : 'Generic'; ?>'});
    });
</script>