<div class="clearfix" style="margin-top:20px;"><?php echo $form[$field]->renderLabel(); ?></div>
<div class="form-field clearfix" style="z-index:980;">
    <span class="mypwn-input-container">
        <?php echo $form[$field]->render(array('class' => 'input-large font-small mypwn-input')); ?>
    </span>
</div>
<?php if (isset($text) && $field == 'password' && !is_null($text)) : echo $sf_data->getRaw('text'); endif; ?>
