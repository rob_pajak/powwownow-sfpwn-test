<div class="grid_24 dialog-plus-main-content" id="plusExistingCustomerPremiumAdmin">
    <div class="grid_sub_22">
        <h2>You are already a Powwownow customer!</h2>
        <p class="toggle-firstname">Hi </p>
        <p>We are unable to log you into the Plus account area as your email address is already registered for either Free or Premium service.</p>
        <p>For further assistance please contact our Customer Services on 0203 398 0398.</p>
    </div>
    <div class="grid_sub_1 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>