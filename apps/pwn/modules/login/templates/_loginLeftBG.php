<ul class="benefits-boxes">
    <li class="benefit-1"><h3 class="rockwell">Request a Wallet Card</h3><p>Request a free wallet-sized reminder card with your details on it: PIN, dial-in numbers etc.</p></li>
    <li class="benefit-2"><h3 class="rockwell">Manage call settings</h3><p>Here's where you can change the voice prompt language, the music on hold and much more!</p></li>
    <li class="benefit-3"><h3 class="rockwell">Use Scheduler</h3><p>Use this handy tool to organize your conference calls and invite your participants.</p></li>
</ul>