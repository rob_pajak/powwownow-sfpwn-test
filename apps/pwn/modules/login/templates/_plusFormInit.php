    <?php if (isset($postData['password'])) : ?>
    <div class="floatright">
        <p>* Required fields</p>
        <button class="button-orange" type="submit" id="plus_signup_submit">
            <span>Switch to Plus</span>
        </button>
    </div>
    <?php else : ?>
    <div class="floatright">
        <p>* Required fields</p>
        <button class="button-orange" type="submit" id="plus_signup_submit">
            <span>Get Plus</span>
        </button>
    </div>
    <?php endif; ?>
</form>
<script>
    $(function () {
        $('#switch2plus').dialog('close');
        $('#switch2plus').dialog();
        $('#form-plus-signup').initMypwnForm({
            debug: 'on',
            requiredAsterisk: 'on',
            messagePos: 'off',
            beforeSubmit: function() {
                var errors = [],
                    password = $('#plus_signup_password').val(),
                    confirm_password = $('#plus_signup_confirm_password').val(),
                    promocode = $('#promocode').val();

                if (password !== confirm_password) {
                    errors.push({'message': FORM_VALIDATION_PASSWORD_MISMATCH, 'field_name'  : 'confirm_password'});
                    $.showInputErrors(errors, '','', '#form-plus-signup');            
                    return false;  
                }

                return true;
            },
            success: function (responseText) {
                dataLayer.push({'event': 'Plus/RegistrationSuccess', 'page': '<?php echo (isset($trackingName)) ? $trackingName : 'Generic'; ?>', 'type': $('#registrationType').val()});
                $('#switch2plus').html(responseText.html);
            },
            error: function (responseText) {
                $('#plus_signup_first_name').parent().removeClass('mypwn-input-container').html($('#plus_signup_first_name').val());
                $('#plus_signup_last_name').parent().removeClass('mypwn-input-container').html($('#plus_signup_last_name').val());
                $('#plus_signup_company_name').parent().removeClass('mypwn-input-container').html($('#plus_signup_company_name').val());
                $('#plus_signup_business_phone').parent().removeClass('mypwn-input-container').html($('#plus_signup_business_phone').val());
                $('#plus_signup_password').parent().removeClass('mypwn-input-container').html('*****');
                $('#plus_signup_confirm_password').parent().removeClass('mypwn-input-container').html('*****');
                $('#plus_signup_submit span').html('Redeem Voucher');

                if (!$('#redeem_voucher_skip').length) {
                    $('#form-plus-signup .floatright').append('<button class="button-orange" id="redeem_voucher_skip" type="button" onclick="window.location=\'<?php echo url_for('@mypwn_existing_registration'); ?>\'"><span>Skip</span></button>');
                }
            }
        });
        dataLayer.push({'event': 'PlusAttemptFinalForm/onLoad', 'page': '<?php echo (isset($trackingName)) ? $trackingName : 'Generic'; ?>'});
    });
</script>
