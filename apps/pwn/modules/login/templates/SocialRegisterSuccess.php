<?php include_component('commonComponents', 'header', array('social_register' => true)); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => false, 'breadcrumbs' => false)); ?>
    <div class="grid_24 clearfix">
        <div class="grid_sub_12">
            <h2>Link existing myPowwownow account</h2>
            <div class="powwownowDetails">
                <p>
                    Please enter your myPowwownow details below to link to your <?php echo $media; ?> account.
                    You will only need to do this once.
                </p>
            </div>
            <form id="form-plus-signin" method="post" enctype="application/x-www-form-urlencoded" action="#" class="clearfix">
                <input type="hidden" id="media" name="media" value="<?php echo $media; ?>"/>
                <div class="form-field">
                    <label for="plus_signin_email">Email *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signin_email" name="email" type="text" placeholder="Your email" value="<?php echo $socialUser['email']; ?>"/>
                    </span>
                </div>
                <div class="form-field margin-10-0">
                    <label for="plus_signin_password">Password *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signin_password" name="password" type="password" pattern="^.{6,}$" placeholder="Your password" />
                    </span>
                </div>
                <div class="floatright">
                    <p>* Required fields</p>
                    <button class="button-orange" type="submit" id="plus_signin_submit">
                        <span>Connect Account</span>
                    </button>
                </div>
            </form>
            <div>
                <p>
                    Cancel registration with <?php echo $media; ?> ? <a href="<?php echo url_for('@social_cancel?flow=cancel&media=' . $media); ?>">Click here</a>.
                </p>
            </div>
        </div>
        <div class="grid_sub_12">
            <h2>Don't have an account?<br/>Create a new one here</h2>
            <form id="form-plus-signup" method="post" enctype="application/x-www-form-urlencoded" action="/login/SocialRegisterAjax" class="clearfix">
                <input type="hidden" id="media" name="media" value="<?php echo $media; ?>"/>
                <input type="hidden" id="service" name="service" value=""/>
                <div class="form-field">
                    <label for="plus_signup_email">Email *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signup_email" name="email" type="text" placeholder="Your email" value="<?php echo $socialUser['email']; ?>"/>
                    </span>
                </div>
                <div class="form-field">
                    <label for="plus_signup_first_name">First Name *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signup_first_name" name="first_name" type="text" placeholder="Your first name" value="<?php echo $socialUser['first_name']; ?>"/>
                    </span>
                </div>
                <div class="form-field">
                    <label for="plus_signup_last_name">Last Name *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signup_last_name" name="last_name" type="text" placeholder="Your last name" value="<?php echo $socialUser['last_name']; ?>"/>
                    </span>
                </div>
                <div class="form-field">
                    <label for="plus_signup_company_name">Company *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signup_company_name" name="company_name" type="text" placeholder="Your company name" value=""/>
                    </span>
                </div>
                <div class="form-field">
                    <label for="plus_signup_business_phone">Business Phone *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signup_business_phone" name="business_phone" type="text" placeholder="Business phone number" value=""/>
                    </span>
                </div>
                <div class="form-field">
                    <label for="plus_signup_password">Password *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signup_password" name="password" type="password" pattern="^.{6,}$" placeholder="Your password" />
                    </span>
                </div>
                <div class="form-field">
                    <label for="plus_signup_confirm_password">Confirm password *</label>
                    <span class="mypwn-input-container">
                        <input class="mypwn-input required" id="plus_signup_confirm_password" name="confirm_password" type="password" pattern="^.{6,}$" placeholder="Retype your password" />
                    </span>
                </div>
                <br/>
                <br/>
                <div style="display: none;" id="choose-service">
                    <div class="floatright sprite plus-close socialRegisterClose" onclick="$(this).parent().dialog('close');">
                        <span class="sprite-cross-white png"></span>
                    </div>
                    <h3 id="popup-header">Please confirm which service you would like to register for:</h3>
                    <div id="service-free">
                        <div class="popup-service">Free</div>
                        <div class="popup-description">
                            Free and instant conference calls with no booking, no billing, no fuss!
                            You just pay the cost of an 0844 call which is added to your regular phone bill.
                        </div>
                        <div class="popup-selector">
                            <input type="checkbox" id="service-free-checkbox" onfocus="blur();"/>
                        </div>
                    </div>
                    <div class="popup-row" id="service-plus">
                        <div class="popup-service">Plus</div>
                        <div class="popup-description">
                            You will receive all the great features of Powwownow for free, PLUS the option
                            to purchase additional products and features to build a conference package that
                            meets your business requirements.
                        </div>
                        <div class="popup-selector">
                            <input type="checkbox" id="service-plus-checkbox" onfocus="blur();"/>
                        </div>
                    </div>
                    <div id="service-premium">
                        <div class="popup-service">Premium</div>
                        <div class="popup-description">
                            This service is completely bespoke and the package will vary according to your business needs.
                            To apply for this service <a href="<?php echo url_for('@contact_us'); ?>">contact our Customer Service Team</a>
                            or call 0800 022 9781 for a tailor-made solution.
                        </div>
                        <div class="popup-selector">&nbsp;</div>
                    </div>
                    <br/>
                    <br/>
                    <div class="floatright">
                        <button class="button-orange" id="social-plus_signup_submit" onclick="$('#form-plus-signup').submit();">
                            <span>Go</span>
                        </button>
                    </div>
                </div>
                <div class="floatright">
                    <p>* Required fields</p>
                    <button class="button-orange">
                        <span>Register</span>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div id="social-login-register-result" style="display: none;"></div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
