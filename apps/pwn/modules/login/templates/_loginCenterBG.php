<div id="mypwn-login-middle">
    <h3 class="rockwell blue">myPowwownow</h3>
    <p>myPowwownow is your personal account area where you can manage your call preferences, use the scheduler tool, access call recordings, purchase products and much more.</p>
    <p>To access myPowwownow just login with your registered email address and password. If you don't have a password yet, go to the 'Create a login' tab and here you will be able to generate a password that will give you instant access to myPowwownow.</p>
</div>