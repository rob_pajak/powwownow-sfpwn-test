<?php $rawUser = $fbUser->getRawValue(); ?>
<?php $sf_response->setTitle(__('Free Conference Call | Conference Calls | Powwownow')); ?>
<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    
    <?php include_component('commonComponents', 'subPageHeader', array('title' => false, 'breadcrumbs' => false)); ?>
    <div class="grid_24 clearfix">
        <div>
            <?php echo link_to('back', '@mypwn_index'); ?>
            <div>
                id: <?php echo $rawUser->id; ?><br/>
                name: <?php echo $rawUser->name; ?><br/>
                first_name: <?php echo $rawUser->first_name; ?><br/>
                last_name: <?php echo $rawUser->last_name; ?><br/>
                username: <?php echo $rawUser->username; ?><br/>
                birthday: <?php echo $rawUser->birthday; ?><br/>
                hometown: <?php echo $rawUser->hometown->name; ?><br/>
                location: <?php echo $rawUser->location->name; ?><br/>
                gender: <?php echo $rawUser->gender; ?><br/>
                email: <?php echo $rawUser->email; ?><br/>
                locale: <?php echo $rawUser->locale; ?><br/>
            </div>
            <pre><?php //print_r($fbUser); ?></pre>
            <div style="border: solid 1px blue;">
            log: <br/><?php echo strtr($log,array(';'=>'<br/>')); ?>
            </div>
        </div>

    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
