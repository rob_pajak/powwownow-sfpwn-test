<style>p.required-note {display:none;} form#form-plus-signup .form-field{width:52%;}</style>
<div class="floatright sprite plus-close" style="border-bottom: none" href="#" onclick="$(this).parent().dialog('close');">
    <span class="sprite-cross-white png"></span>
</div>
<h2>Do you have a promotion code?</h2>
<form id="form-promo-signup" method="post" enctype="application/x-www-form-urlencoded" action="/s/login/plusSignup" class="clearfix">
    <?php if (isset($accountid)) : ?><input type="hidden" name="accountid" id="accountid" value="<?php echo $accountid; ?>"/><?php endif; ?>
    <?php if (isset($contactref)) : ?><input type="hidden" name="contactref" id="contactref" value="<?php echo $contactref; ?>"/><?php endif; ?>
    <div class="form-field">
        <label for="plus_signup_first_name">First Name</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_first_name" name="first_name" type="text" placeholder="Your first name" value="<?php echo $first_name; ?>"/>
        </span>
    </div>
    <div class="form-field">
        <label for="plus_signup_last_name">Last Name</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_last_name" name="last_name" type="text" placeholder="Your last name" value="<?php echo $last_name; ?>"/>
        </span>
    </div>
    <div class="form-field">
        <label for="plus_signup_business_phone">Company</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_company_name" name="company_name" type="text" placeholder="Your company name" value="<?php echo $organisation; ?>"/>
        </span>
    </div>
    <div class="form-field">
        <label for="plus_signup_business_phone">Business Phone</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input required" id="plus_signup_business_phone" name="business_phone" type="text" placeholder="Business phone number" value="<?php echo $business_phone; ?>"/>
        </span>
    </div>
    <div class="form-field">
        <label for="plus_signup_promocode">Promotion code</label>
        <span class="mypwn-input-container">
            <input class="mypwn-input" id="promocode" name="promocode" type="text" placeholder="Your Promotion Code"/>
        </span>
    </div>
    <div class="floatright">
        <p>* Required fields</p>
        <button class="button-orange" type="submit" id="plus_signup_submit">
            <span>Switch to Plus</span>
        </button>
    </div>
</form>
<script>
    $(function () {
        $('#switch2plus').dialog('close');
        $('#switch2plus').dialog();
        $('#form-promo-signup').initMypwnForm({
            debug: 'on',
            requiredAsterisk: 'on',
            messagePos: 'off',
            beforeSubmit: function() {
                var errors = [],
                    promocode = $('#promocode').val();

                if ("" === promocode) {
                    // $('#form-promo-signup').attr('action','/s/login/enablePlusAccountRedirectOnly');
                }

                return true;
            },
            success: function (responseText) {
              $('#switch2plus').html(responseText.html);
            }
        });
    });
</script>