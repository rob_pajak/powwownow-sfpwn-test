<div class="floatright sprite plus-close" style="border-bottom: none; padding-top: 0px; width: 22px; height: 14px;" href="#" onclick="$(this).parent().dialog('close');">
    <span class="sprite-cross-white png" style="margin: 2px;"></span>
</div>
<h2>Just a second...</h2>
<p style="padding-right: 20px;">
    It looks like you already have an account with us.
    Please use the login form to view your PIN details or call Customer Services on 0203 398 0398 for more information.
</p>
<script>
    $('#social-login-register-result').dialog('close');
    $('#social-login-register-result').dialog();
</script>