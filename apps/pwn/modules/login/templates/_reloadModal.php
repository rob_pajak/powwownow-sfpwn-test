<script>
    var data = {};
    <?php if (isset($email)) : ?>data.email = '<?php echo $email; ?>'; <?php endif; ?>
    <?php if (isset($password)) : ?>data.password = '<?php echo $password; ?>'; <?php endif; ?>
    <?php if (isset($promoemail)) : ?>data.promoemail = '<?php echo $promoemail; ?>'; <?php endif; ?>
    <?php if (isset($trackingName)) : ?>data.trackingName = '<?php echo $trackingName; ?>'; <?php endif; ?>
    $.ajax({
        url: '/s/login/plusSignup',
        type: 'POST',
        data: data,
        success: function(responseText) {
            $('#switch2plus').html(responseText.html);
        },
            error: function() {
            $('#switch2plus').html(window[FORM_COMMUNICATION_ERROR]);
        }
    });
    $('#switch2plus').dialog('close');
    $('#switch2plus').dialog();
</script>