<div class="mypwn-login-tab-1 tab-1-green png active" id="tab-1">
    <div class="rockwell font-large white">Login</div><div style="display:none" class="tab-shadow png"></div>
</div>
<div class="mypwn-login-tab-2 tab-2-green png" id="tab-2">
    <div class="rockwell font-large white">Create a login</div><div class="tab-shadow png"></div>
</div>

<div id="login-tabs-container" class="clearfix">
    <div class="white" id="tab-mypwn-login">
        <a href="/" class="floatright sprite tooltip-close" style="border-bottom: none" onClick="window.location.replace('/')"><span class="sprite-cross-white png"></span></a>
        <h2 class="rockwell white">Log in to myPowwownow</h2>
        <p>Enter your email address and password below to log in to myPowwownow.</p>
        <?php echo form_tag(url_for('login_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-log-in', 'class' => 'pwnform clearfix')); ?>
            <?php echo $loginForm['redirect']->render(); ?>
            <div class="margin-top-20">
                 <?php foreach ($sf_data->getRaw('loginFields') as $field) : ?>
                    <?php include_partial('loginFormField', array(
                        'field' => $field,
                        'form'  => $loginForm,
                    )); ?>
                <?php endforeach; ?>
            </div>
            <div class="margin-top-10"><a class="white" href="<?php echo url_for('@forgotten_password'); ?>">Forgotten your password?</a></div>
            <div class="form-action clearfix">
                <p class="white">* Required fields</p>
                <button class="button-orange floatright" type="submit" id="login-btn" style="z-index:901;">
                    <span>LOGIN NOW</span>
                </button>
            </div>
        </form>
    </div>

    <div class="white" id="tab-mypwn-create-login">
        <div id="mypwn-create-login-form-placeholder">
            <a href="/" onClick="window.location.replace('/')" class="floatright sprite tooltip-close" style="border-bottom: none"><span class="sprite-cross-white png"></span></a>
            <h2 class="rockwell white">Create a login</h2>
            <p>myPowwownow is where you can manage your call settings, use the scheduler tool, access recordings and much more!</p>
            <?php echo form_tag(url_for('create_a_login_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-create-log-in', 'class' => 'pwnform clearfix')); ?>
                <?php echo $createALoginForm['redirect']->render(); ?>
                <div class="margin-top-20">
                    <?php foreach ($sf_data->getRaw('createALoginFields') as $field) : ?>
                        <?php include_partial('loginFormField', array(
                            'field' => $field,
                            'form'  => $createALoginForm,
                            'text'  => ('password' == $field) ? '<br/><i>(minimum 6 characters)</i><br style="margin-bottom:20px;">' : null,
                        )); ?>
                    <?php endforeach; ?> 
                    <div class="form-action clearfix">
                        <p class="white">* Required fields</p>
                        <button class="button-orange" type="submit" id="create-a-login-btn">
                            <span>LOGIN NOW</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
