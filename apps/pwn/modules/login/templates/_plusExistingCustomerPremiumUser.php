<div class="grid_24 dialog-plus-main-content" id="plusExistingCustomerPremiumUser">
    <div class="grid_sub_22">
        <h2>You are already a Powwownow customer!</h2>
        <p class="toggle-firstname">Hi </p>
        <p>We are unable to register you for Plus as your email address is already registered for either our Plus or Premium service.</p>
        <p>For further assistance please contact your Account Administrator.</p>
    </div>
    <div class="grid_sub_1 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>