<div class="floatright sprite plus-close" style="border-bottom: none" href="#" onclick="$(this).parent().dialog('close');">
    <span class="sprite-cross-white png"></span>
</div>
<h2>You are already a Powwownow customer!</h2>
<?php if (isset($first_name)) : ?><p>Hi <?php echo $first_name; ?></p><?php endif ; ?>
<p>We are unable to switch you to Plus as your email address is already registered for either our Plus or Premium service.</p>
<p>For further assistance please contact our Customer Services on 0203 398 0398.</p>
<script> 
    $('#switch2plus').dialog('close');
    $('#switch2plus').dialog();
</script>