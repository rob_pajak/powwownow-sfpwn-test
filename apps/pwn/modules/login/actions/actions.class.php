<?php

require_once dirname(__FILE__) . '/../lib/AJAXResponse.php';

/**
 * Login actions.
 *
 * @package    powwownow
 * @subpackage login
 * @author     Asfer Tamimi
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z  Asfer Tamimi $
 */
class loginActions extends sfActions
{

    /**
     * Login [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeLogin(sfWebRequest $request)
    {
        // check if there was action output forward, if so that means we shall add 401 status header
        if (sfContext::getInstance()->getController()->getActionStack()->getSize() > 1) {
            $this->getResponse()->setStatusCode(401);
        }
        $route = $this->getRoute()->getParameters();
        if ($route['action'] == 'login' && $route['module'] == 'login') {
            $redirect = '/myPwn/';
        } else {
            $redirect = $request->getParameter('redirect', $request->getUri());
        }

        $this->getResponse()->setTitle('Login into your account. Let\'s get it done at Powwownow');
        $this->getResponse()->addMeta(
            'description',
            'Use myPowwownow to log into your account. Manage your call preferences, schedule calls, call recordings & purchase products with Powwownow'
        );

        $this->setVar('redirect', $redirect);
    }

    /**
     * @param sfWebRequest $request
     * @return string
     */
    public function executeLoginAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        $loginCredentials = $request->getParameter('login', false);

        if ($loginCredentials !== false && is_array($loginCredentials)) {
            if (array_key_exists('email', $loginCredentials) && array_key_exists('password', $loginCredentials)) {

                $email = $loginCredentials['email'];
                $password = $loginCredentials['password'];
                $rememberMe = $loginCredentials['remember'];

                $emailValidator = new sfValidatorEmail();
                $passwordValidator = new sfValidatorString( array('required' => true), array('min_length' => 3));

                try {
                    $emailValidator->clean($email);
                } catch (sfValidatorError $sfValidatorError) {
                    $this->getResponse()->setStatusCode(400);
                    echo json_encode(array('error' => 'FORM_VALIDATION_INVALID_EMAIL'));
                    return sfView::NONE;
                }

                try {
                    $passwordValidator->clean($password);
                } catch (sfValidatorError $sfValidatorError) {
                    $this->getResponse()->setStatusCode(400);
                    echo json_encode(array('error' => 'FORM_VALIDATION_INVALID_PASSWORD'));
                    return sfView::NONE;
                }
            }
        } else {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error' => 'FORM_LOGIN_TECHNICAL_ERROR'));
            return sfView::NONE;
        }

        try {
            $loginManager = new LoginManager();
            $loginStatus = $loginManager->doLogin($email, $password, $rememberMe);

            if ($loginStatus !== false && $loginStatus instanceof myUser) {
                echo json_encode(array());
            }

        } catch (Exception $exception) {
            switch ($exception->getCode()) {
                case '000:011:002':
                    $errorMessage = 'FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_ADMIN';
                    break;
                case '000:011:003':
                    $errorMessage = 'FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_USER';
                    break;
                case '000:011:001':
                    $errorMessage = 'FORM_VALIDATION_INVALID_PASSWORD';
                    break;
                default:
                    $errorMessage = 'FORM_VALIDATION_LOGIN_ERROR';
            }
            $this->getResponse()->setStatusCode(400);
            echo json_encode (array('error' => $errorMessage));
        }

        return sfView::NONE;
    }

    /**
     * Create A Login [Ajax]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeCreateALoginAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');

        // Retrieve the form and the POST values to it.
        $form = new loginCreateALoginForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments               = Common::getArguments($request->getParameter($form->getName(), array()), array());
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(500);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        // Find out if the Email Address is an Existing Email Address
        $resultPre = Common::getContact($arguments['email'], null, true);

        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";
        $locale  = (isset($_SERVER['locale'])) ? $_SERVER['locale'] : "en_GB";

        $result = Common::doCreateOrUpdateAccount(
            array(
                'email'      => $arguments['email'],
                'first_name' => $arguments['first_name'],
                'last_name'  => $arguments['last_name'],
                'password'   => $arguments['password'],
                'source'     => $country . '-Create-A-Login',
                'locale'     => $locale,
            )
        );

        // Check if there was an Error from the above Hermes Call
        if (isset($result['error'])) {
            $this->getResponse()->setStatusCode(500);
            if ('email' == $result['error']['field_name']) {
                $result['error']['field_name'] = 'createalogin[email]';
            }

            // Find out if the Email Address is an Existing Email Address
            if (!empty($resultPre) && isset($resultPre['service_ref'])) {
                if ((int)$resultPre['service_ref'] === 850) {
                    $result['error']['message'] = 'API_RESPONSE_EMAIL_TAKEN_AND_IS_PLUS';
                }
            }

            echo json_encode(array('error_messages' => $result['error']));
            return sfView::NONE;
        }

        // Create-A-Login Goal Tracking
        try {
            // If API call was create a pin / contact
            if ($result['statusCode'] == 204) {
                try {
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                    PWN_Logger::log(
                        'pin:' . $result['pin']['pin_ref'] . ' goal: Create-A-Login-With-Pin ' . __FILE__ . '[' . __LINE__ . ']',
                        PWN_Logger::INFO,
                        'trackerLog'
                    );
                } catch (Exception $e) {

                }
                PWN_Tracking_GoalReferrers::achieved('Create-A-Login-With-Pin', $result['pin']['pin_ref'], true);
            }
        } catch (Exception $e) {
            $this->logMessage(
                'Could not track goal referrers for pin_ref: ' . $result['pin']['pin_ref'] . ' ' . $e->getMessage(),
                'err'
            );
        }

        // Find out if the Email Address is an Existing Email Address
        if (!isset($resultPre['error']) && empty($resultPre)) {
            // New Email Address
            $emailType = 'New';
        } else {
            $emailType = 'Existing';
        }

        // Shomei API And PinRefHash
        if ($emailType === 'New') {
            $pinRef       = $result['pin']['pin_ref'];
            $shomei       = new Shomei();
            $shomeiResult = $shomei->registerFreePin($pinRef);
        }

        // Login to MyPowwownow - This needs to be in a Helper
        // Authentication Check - Login/Authenticate Does not have the Correct way of handling Errors etc
        try {
            if (Plus_Authenticate::logIn($arguments['email'], $arguments['password'])) {
                sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
                echo json_encode(
                    array(
                        'success_message' => array(
                            'success'   => get_partial('login/createALoginSuccessMessage', array()),
                            'redirect'  => $arguments['redirect'],
                            'emailType' => $emailType,
                            'pinRefHash' => (isset($shomeiResult)) ? $shomeiResult['apiResult']['responsiveBody']['transactionIdentifier'] : false
                        )
                    )
                );
                return sfView::NONE;
            }
        } catch (Exception $e) {
            $this->logMessage('Exception Called. Authentication Failed.' . $e->getMessage(), 'warning');
        }

        $this->getResponse()->setStatusCode(500);
        echo json_encode(
            array(
                'error_messages' => array(
                    'message'    => 'FORM_RESPONSE_LOG_IN_UNABLE',
                    'field_name' => 'createalogin[email]'
                )
            )
        );
        return sfView::NONE;
    }

    /**
     * Authenticates a User and Logs them in [Legacy]
     *
     * The Only Difference between this Legacy and Symfony Authenticate Methods, is the Error Handling.
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeAuthenticate(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->logMessage('Form is Not a POST or AJAX Request', 'err');
            $this->forward404();
        }

        $response = new AJAXResponse($this->getResponse(), null, null, null, 'text/html');
        $email    = $request->getPostParameter('loginEmail', null);
        $password = $request->getPostParameter('loginPassword', null);

        try {
            if (Plus_Authenticate::logIn($email, $password)) {
                $response->addContent('status', 'success');
            } else {
                $response->addContent('status', 'fail');
            }
        } catch (Exception $e) {
            $this->logMessage('Authenticating contact failed. Email: ' . $email . ', Password: ' . $password, 'err');

            switch ($e->getCode()) {
                case '000:011:002':
                    $response->addContent('status', 'suspended_admin');
                    break;
                case '000:011:003':
                    $response->addContent('status', 'suspended_user');
                    break;
                case '000:011:001':
                default:
                    $response->addContent('status', 'fail');
                    break;
            }
        }
        return $response->outputResponse();
    }

    /**
     * Authenticates a User and Logs them in [Symfony]
     *
     * The Only Difference between the old Legacy and this Symfony Authenticate Methods, is the Error Handling.
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeSymfonyAuthenticate(sfWebRequest $request)
    {
        if (!$request->isSecure()) {
            $this->logMessage('Form is not a HTTPS Request', 'err');
            $this->forward404();
        }

        $email    = $request->getParameter('loginEmail', null);
        $password = $request->getParameter('loginPassword', null);
        $callback = $request->getParameter('callback', null);

        if (!$callback === null) {
            $this->logMessage('Form is Not a POST or AJAX Request', 'err');
            $this->forward404();
        }

        $response = $this->getContext()->getResponse();
        $response->setContentType('application/json');

        $emailValidator = new sfValidatorEmail();
        try {
            $email = $emailValidator->clean($email);
        } catch (sfValidatorError $e) {
            echo $callback . '('.json_encode(array('status'=> 'fail')).')';
            return sfView::NONE;
        }

        try {
            if (Plus_Authenticate::logIn($email, $password)) {
                $data = array('status'=> 'success');
            } else {
                $data = array('status'=> 'fail');
            }
        } catch (Exception $e) {

            $this->logMessage('Authenticating contact failed. Email: ' . $email . ', Password: ' . $password, 'err');

            switch ($e->getCode()) {
                case '000:011:002':
                    $data = array('status'=> 'suspended_admin');
                    break;
                case '000:011:003':
                    $data = array('status'=> 'suspended_user');
                    break;
                case '000:011:001':
                default:
                    $data = array('status'=> 'fail');
                    break;
            }
        }
        echo $callback . '('.json_encode($data).')';
        return sfView::NONE;
    }

    /**
     * Logs a User in via God Mode [AJAX]
     *
     * @param sfWebRequest $request
     * @return string
     * @author Asfer Tamimi
     *
     */
    public function executeAuthenticateGodMode(sfWebRequest $request)
    {
        $response = $this->getContext()->getResponse();
        $response->setContentType('application/json');

        $data = array(
            'contact_ref'             => $request->getPostParameter('contact_ref', null),
            'email'                   => $request->getPostParameter('loginEmail', null),
            'password'                => $request->getPostParameter('god_mode_password', null),
            'accessor_user_reference' => $request->getPostParameter('accessorUserReference'),
            'country_site_redirect'   => $request->getPostParameter('country_name', 'GBR')
        );

        // Error Messages
        $errorMessages = array();

        // Ensure users IP is in our white list
        $ipValidator = new PWNSafeIp();
        $userIp      = $request->getHttpHeader('addr', 'remote');
        try {
            $ipValidator->clean($userIp);
        } catch (Exception $e) {
            $this->logMessage("IP $userIp tried to log in via god mode but is not in the 'allowed IP' list", 'err');
            $errorMessages[] = array('message' => 'FORM_NO_PERMISSION_ERROR', 'field_name' => 'alert');
        }

        // Check if the Correct Password was used
        if (md5($data['password']) !== '93f66cc09073294a29f5d985c62e2134') {
            $this->logMessage("IP $userIp tried to log in via god mode but incorrect password was specified", 'err');
            $errorMessages[] = array(
                'message'    => 'FORM_VALIDATION_WRONG_PASSWORD',
                'field_name' => 'god_mode_password'
            );
        }

        // Check if there were any Errors from the above Checks, then return 403 Status Code on Error.
        if (!empty($errorMessages)) {
            $response->setStatusCode(403);
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Get Contact Information
        $contact = Common::getContactByRef(array('contact_ref' => $data['contact_ref']));

        // Check if there was an Error, then return it
        if (isset($contact['error'])) {
            $this->logMessage("Contact Information Returned an Error:" . print_r($contact, true), 'err');
            $response->setStatusCode(500);
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'god_mode_password');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Check if the Email Address do not Match
        if ($contact['email'] !== $data['email']) {
            $this->logMessage(
                "Passed email '{$data['email']}' does not match retrieved (by contact_ref) user email '{$contact['email']}'.",
                'err'
            );
            $response->setStatusCode(500);
            $errorMessages[] = array(
                'message'    => 'FORM_GODMODE_NONMATCHING_CONTACT',
                'field_name' => 'god_mode_password'
            );
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Authenticate the Contact
        $result = Plus_Authenticate::logInViaGodMode(
            array(
                'email'                   => $data['email'],
                'contact_ref'             => $data['contact_ref'],
                'accessor_user_reference' => $data['accessor_user_reference'],
                'country_site_redirect'   => $data['country_site_redirect'],
                'password'                => $data['password']
            )
        );

        // Check if there was an Error
        if (isset($result['error'])) {
            // Known Errors
            $this->logMessage("Authentication Returned a Declared Error:" . print_r($result, true), 'err');
            $response->setStatusCode(500);
            switch ($result['error']) {
                case 'session_failed':
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'god_mode_password');
                    break;
                case 'contact_ref_failure':
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'god_mode_password');
                    break;
                case 'contact_info_failure':
                    $errorMessages[] = array('message' => 'FORM_VALIDATION_EMAIL_LOGIN_ERROR', 'field_name' => 'god_mode_password');
                    break;
                case 'invalid_country':
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'country_name');
                    break;
                default:
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'god_mode_password');
                    break;
            }
            echo json_encode(array('error_messages' => $errorMessages));
        } elseif (isset($result) && isset($result['status']) && $result['status'] === 'success') {
            // Success
            $authSection = ('GBR' === $data['country_site_redirect']) ? '/myPwn/' : '/mypwn/';
            echo json_encode(
                array(
                    'status'   => 'success',
                    'redirect' => 'http://' . SiteURL::getSiteURL($data['country_site_redirect']) . $authSection
                )
            );
        } else {
            // Unknown Error Occurred
            $this->logMessage("An unknown error occurred while trying to log a user in via god mode", 'err');
            $response->setStatusCode(500);
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'god_mode_password');
            echo json_encode(array('error_messages' => $errorMessages));
        }
        return sfView::NONE;
    }

    /**
     * Logs Out a User
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeLogout(sfWebRequest $request)
    {
        try {
            Plus_Authenticate::logOut();
            return 1;
        } catch (Exception $e) {
            $this->logMessage(
                'An Exception occurred trying to log a user out. Message : ' . $e->getMessage(
                ) . ', Code : ' . $e->getCode(),
                'err'
            );
            return 0;
        }
    }

    /**
     * @throws sfStopException
     */
    public function executeSymfonyLogout()
    {
        try {
            $loginManager = new LoginManager();
            $loginManager->doLogout();
        } catch (Exception $e) {
            $this->logMessage(
                'An Exception occurred trying to log a user out. Message: ' . $e->getMessage(
                ) . ', Code: ' . $e->getCode(),
                'err'
            );
        }

        $this->redirect('@homepage');
    }

    /**
     * Plus registration/switch hub
     *
     * @param sfWebRequest $request
     * @return string
     * @author Vitaly + Asfer Tamimi
     */
    public function executePlusSignup(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->logMessage('Form is Not a POST or AJAX Req uest' . serialize($request->isXmlHttpRequest()), 'err');
            $this->forward404();
        }

        $postData     = $request->getParameterHolder()->getAll();
        $promoemail   = ($request->hasParameter('promoemail')) ? $request->getParameter('promoemail') : null;
        $trackingName = $request->getParameter('trackingName', 'Not Set');

        $this->getResponse()->setContentType('application/json');

        if ($this->getUser()->getAttribute('contact_ref') !== '') {
            $contactData = Common::getContactByRef(
                array('contact_ref' => $this->getUser()->getAttribute('contact_ref'))
            );
            if (isset($contactData['error'])) {
                $contactData = array();
            }
        }

        if ($this->getUser()->isAuthenticated() === false) {
            $contactData = array();
        }

        // Email Form
        $email_form = $this->getPartial(
            'emailForm',
            array(
                'promoemail'   => $promoemail,
                'trackingName' => $trackingName
            )
        );

        // Password Form
        $pass_form = $this->getPartial(
            'passForm',
            array(
                'promoemail'   => $promoemail,
                'email'        => $postData['email'],
                'trackingName' => $trackingName
            )
        );

        // Start of the Plus Form
        $plus_form = $this->getPartial(
            'plusForm',
            array(
                'email'          => $postData['email'],
                'password'       => $postData['password'],
                'first_name'     => (isset($contactData['first_name'])) ? $contactData['first_name'] : '',
                'last_name'      => (isset($contactData['last_name'])) ? $contactData['last_name'] : '',
                'organisation'   => (isset($contactData['organisation'])) ? $contactData['organisation'] : '',
                'business_phone' => (isset($contactData['business_phone'])) ? $contactData['business_phone'] : '',
                'promoemail'     => $promoemail,
            )
        );

        // Plus Form Password
        $plus_form_pass = $this->getPartial('plusFormPass', array());

        // Promo Email Form
        $promo_email_form = (!is_null($promoemail)) ? $this->getPartial('promoEmailForm', array()) : '';

        // Plus Form Init (Technically its the End of the Form, however it contains the Form Initialising Script)
        $plus_form_init = $this->getPartial(
            'plusFormInit',
            array(
                'trackingName' => $trackingName
            )
        );

        // Reload Modal
        $reload_modal = $this->getPartial(
            'reloadModal',
            array(
                'email'        => $postData['email'],
                'password'     => $postData['password'],
                'promoemail'   => $promoemail,
                'trackingName' => $trackingName
            )
        );

        // Plus No Switch
        $plus_no_switch = $this->getPartial(
            'plusNoSwitch',
            array(
                'first_name' => (isset($contactData['first_name'])) ? $contactData['first_name'] : '',
            )
        );

        // Promo Incorrect Code Form
        $promo_incorrect_form = $this->getPartial(
            'promoIncorrectForm',
            array(
                'first_name'     => $postData['first_name'],
                'last_name'      => $postData['last_name'],
                'organisation'   => $postData['organisation'],
                'business_phone' => $postData['business_phone'],
                'account_id'     => -1,
                'contact_ref'    => -1,
                'trackingName'   => $trackingName
            )
        );

        // Plus Tracking Fields
        $plusExistingField = $this->getPartial('plusSwitchField', array());
        $plusNewField      = $this->getPartial('plusNewField', array());

        // There are 3 Types of Users: 
        // 1) Email and Password Not Set; 
        // 2) Email is set, and Password is Not Set; 
        // 3) Email and Password are set

        // Check if the User is Authenticated or Not Authenticated
        if ($this->getUser()->isAuthenticated() === true) {
            $isSwitchable = plusCommon::isSwitchableToPlus($this->getUser()->getAttribute('email', $_SESSION['email']));
            if ($isSwitchable == true) {
                $html = $plus_form . $promo_email_form . $plusExistingField . $plus_form_init;
            } else {
                $html = $plus_no_switch;
            }

        } elseif ($this->getUser()->isAuthenticated() === false) {

            if (!isset($postData['email']) && !isset($postData['password'])) {

                $html = $email_form;

            } elseif (isset($postData['email']) && !isset($postData['password'])) {

                $isSwitchable = plusCommon::isSwitchableToPlus($postData['email']);

                $contactData = Common::getContact($postData['email'], null, true);
                if (isset($contactData['error'])) {
                    $this->getResponse()->setStatusCode('500');
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR');
                    return $this->renderPartial(
                        'commonComponents/formErrors',
                        array('errors' => array('error_messages' => $errorMessages))
                    );
                }

                if (!isset($contactData['password'])) {
                    $contactData['password'] = null;
                }

                switch ($contactData['password']) {

                    // pwn user without a password
                    case '1e895a7034cb8f2b1b7da1a6220c1737':

                        //check if user can be switched to Plus, if yes - render a full form
                        if ($isSwitchable === true) {
                            $html = $plus_form . $plus_form_pass . $promo_email_form . $plusExistingField . $plus_form_init;
                        } else {
                            $html = $plus_no_switch;
                        }
                        break;

                    // new Plus registration
                    case null:
                        // render a full form and check if user is switchable to plus (just in case)
                        $html = $plus_form . $plus_form_pass . $promo_email_form . $plusNewField . $plus_form_init;
                        break;

                    default:
                        // pwn user with password 
                        if ($isSwitchable === true) {
                            $html = $pass_form;
                        } else {
                            $html = $plus_no_switch;
                        }
                        break;
                }
            } elseif (isset($postData['email']) && isset($postData['password'])) {

                // check if user can be switched to plus
                try {
                    Plus_Authenticate::logIn($postData['email'], $postData['password']);
                } catch (Exception $e) {

                    $this->getResponse()->setStatusCode(500);

                    echo json_encode(
                        array(
                            'error_messages' => array(
                                array(
                                    'message'    => 'FORM_VALIDATION_WRONG_PASSWORD',
                                    'field_name' => 'password'
                                )
                            )
                        )
                    );
                    return sfView::NONE;

                }

                if (plusCommon::isSwitchableToPlus($postData['email']) == true) {
                    $html = $reload_modal;
                } else {
                    $html = $plus_no_switch;
                }
            }
        }

        if (isset($html)) {
            echo json_encode(array('html' => $html));
        }

        return sfView::NONE;
    }

    /**
     * Confirm details and register/switch user to Plus
     *
     * @author Vitaly -> Asfer [Refactored]
     */
    public function executeEnablePlusAccount(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->logMessage('Form is Not a POST or AJAX Request' . serialize($request->isXmlHttpRequest()), 'err');
            $this->forward404();
        }

        $postData = $request->getParameterHolder()->getAll();
        $userData = $this->getUser()->getAttributeHolder()->getAll();

        $this->getResponse()->setContentType('application/json');

        // Plus No Switch
        $plus_no_switch1 = $this->getPartial('plusNoSwitch1', array());

        // Plus Success - Redirection to regEx
        $plus_redirection_existing = $this->getPartial('plusRedirectionExisting', array());

        // Plus Success - Redirection to regNew
        $plus_redirection_new = $this->getPartial('plusRedirectionNew', array());

        // Plus Success - Redirection to regEx
        $plus_redirection_existing_promo = $this->getPartial('plusRedirectionExistingPromo', array());

        // Plus Logging
        $this->logMessage(
            'Plus Logging :: ' . print_r(array('Post Data' => $postData, 'User Data' => $userData), true),
            'err'
        );

        // New Code - Check PromoCode ONLY
        if (!empty($postData['promocode']) && !empty($postData['first_name']) && !empty($userData['account_id'])) {
            $this->logMessage('PromoCode Only Check', 'err');
            $this->setProductstoAccount(
                $this->getUser()->getAttribute('account_id'),
                $this->getUser()->getAttribute('contact_ref'),
                $postData['promocode'],
                $plus_redirection_existing_promo
            );
            return sfView::NONE;
        }

        // Check if the User is Authenticated
        if ($this->getUser()->isAuthenticated() === true) {
            $this->logMessage('User is authenticated', 'err');
            // VK: Because of session problems I had to change $this->getUser()->getAttribute('email') with $_SESSION['email']
            if (plusCommon::isSwitchableToPlus($this->getUser()->getAttribute('email', $_SESSION['email'])) == true) {
                // @todo: Server side validation
                $firstName     = isset($postData['first_name']) ? $postData['first_name'] : '';
                $lastName      = isset($postData['last_name']) ? $postData['last_name'] : '';
                $businessPhone = isset($postData['business_phone']) ? $postData['business_phone'] : '';
                $companyName   = isset($postData['company_name']) ? $postData['company_name'] : '';

                if (empty($firstName)) {
                    $errorMessages[] = array(
                        'message'    => 'FORM_VALIDATION_NO_FIRST_NAME',
                        'field_name' => 'first_name'
                    );
                    $this->getResponse()->setStatusCode(500);
                    $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                    echo json_encode(array('error_messages' => $errorMessages));
                    return sfView::NONE;
                }

                if (empty($lastName)) {
                    $errorMessages[] = array('message' => 'FORM_VALIDATION_NO_SURNAME', 'field_name' => 'last_name');
                    $this->getResponse()->setStatusCode(500);
                    $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                    echo json_encode(array('error_messages' => $errorMessages));
                    return sfView::NONE;
                }

                if (empty($businessPhone)) {
                    $errorMessages[] = array(
                        'message'    => 'FORM_VALIDATION_FIELD_EMPTY',
                        'field_name' => 'business_phone'
                    );
                    $this->getResponse()->setStatusCode(500);
                    $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                    echo json_encode(array('error_messages' => $errorMessages));
                    return sfView::NONE;
                }

                if (empty($companyName)) {
                    $errorMessages[] = array(
                        'message'    => 'FORM_VALIDATION_FIELD_EMPTY',
                        'field_name' => 'company_name'
                    );
                    $this->getResponse()->setStatusCode(500);
                    $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                    echo json_encode(array('error_messages' => $errorMessages));
                    return sfView::NONE;
                }

                // Update Contact with New Information
                Common::updateContact(
                    array(
                        'contact_ref'    => $this->getUser()->getAttribute('contact_ref'),
                        'first_name'     => $postData['first_name'],
                        'last_name'      => $postData['last_name'],
                        'business_phone' => $postData['business_phone'],
                        'organisation'   => $postData['company_name']
                    )
                );

                // Upgrade Account from Enhanced to Plus
                $source = new Source();

                $accountInfo = plusCommon::upgradeEnhancedToPlus(
                    array(
                        'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                        'service_ref' => 850,
                        'source'      => $source->get(
                                'switch_to_plus',
                                $request->getReferer(),
                                $this->getUser()->getAttribute('homePageCroVariation', null, 'cro')
                            )
                    )
                );

                if (!empty($accountInfo)) {
                    // Commented out until we add tracking to all possible places to upgrade
                    //  PWN_Tracking_GoalReferrers::achieved('basic_to_plus', $accountInfo['pins']['chairman']['pin_ref'], true);
                } else {
                    $html = $plus_no_switch1;
                    echo json_encode(array('html' => $html));
                    return sfView::NONE;
                }

                // Log the User into Powwownow
                $email   = $this->getUser()->getAttribute('email', $_SESSION['email']);
                $contact = Common::getContact($email, 850, true);

                try {
                    if (!Plus_Authenticate::setCredentials($email, $contact)) {
                        throw new Exception('Unable to set credentials');
                    }
                } catch (Exception $e) {
                    Plus_Authenticate::logOut();
                    $this->logMessage('Unable to set credentials for ' . $email, 'err');
                }

                // Clear Teamsite cache
                try {
                    PWN_Cache_Clearer::modifiedPin($this->getUser()->getAttribute('contact_ref'));
                    PWN_Cache_Clearer::modifiedContact($this->getUser()->getAttribute('contact_ref'));
                    $this->logMessage(
                        'Cleared Cache for contact_ref: ' . $this->getUser()->getAttribute(
                            'contact_ref'
                        ) . ' while switching to Plus',
                        'info'
                    );
                } catch (Exception $e) {
                    $this->logMessage('Unable to clear cache after switching User to plus:' . serialize($e), 'err');
                }

                /**
                 * Clear sf cache
                 */
                try {

                    sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');

                    hermesCallRemoveCaching(
                        'getDialInNumbers',
                        array(
                            'locale'      => $this->getUser()->getCulture(),
                            'service_ref' => $this->getUser()->getAttribute('service_ref')
                        ),
                        'array'
                    );

                    hermesCallRemoveCaching(
                        'getContactByRef',
                        array(
                            'contact_ref'        => $this->getUser()->getAttribute('contact_ref'),
                            'master_service_ref' => 850
                        ),
                        'array'
                    );

                    hermesCallRemoveCaching('getContact', array('email' => $email), 'array');

                    hermesCallRemoveCaching(
                        'getAllSharedCostDialInNumbers',
                        array('language_code' => $_SERVER['language']),
                        'array'
                    );

                    hermesCallRemoveCaching(
                        'getContactDialInNumbers',
                        array(
                            'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                            'language'    => $_SERVER['language']
                        ),
                        'array'
                    );

                    hermesCallRemoveCaching(
                        'getDefaultPins',
                        array('contact_ref' => $this->getUser()->getAttribute('contact_ref')),
                        'array'
                    );

                    hermesCallRemoveCaching(
                        'getContactPins',
                        array('contact_ref' => $this->getUser()->getAttribute('contact_ref')),
                        'array'
                    );

                } catch (Exception $e) {
                    $this->logMessage('Unable to clear the cache: ' . $e->getMessage(), 'err');
                }

                $pageVar = ($this->getUser()->getAttribute('homePageCroVariation', null, 'cro')) ? $this->getUser(
                )->getAttribute('homePageCroVariation', null, 'cro') : 'Home';

                if (empty($postData['promocode'])) {
                    $html = $plus_redirection_existing;
                } else {
                    $this->setProductstoAccount(
                        $accountInfo['account']['id'],
                        $contact['contact_ref'],
                        $postData['promocode'],
                        $plus_redirection_existing_promo
                    );
                    return sfView::NONE;
                }

            } elseif (!empty($postData['promocode'])) {
                $this->setProductstoAccount(
                    $userData['account_id'],
                    $userData['contact_ref'],
                    $postData['promocode'],
                    $plus_redirection_existing_promo
                );
                return sfView::NONE;
            } else {
                $html = $plus_no_switch1;
            }

        }

        if ($this->getUser()->isAuthenticated() === false) {
            $this->logMessage('User is NOT authenticated', 'err');
            if (isset($postData['email'])) {
                $contactData = Common::getContact($postData['email'], null, true);
            }

            // existing pwn user without a password
            if (!empty($contactData)) {
                // update contact with new info
                // switch to plus
                if (plusCommon::isSwitchableToPlus($contactData['email']) == true) {


                    $firstName     = isset($postData['first_name']) ? $postData['first_name'] : '';
                    $lastName      = isset($postData['last_name']) ? $postData['last_name'] : '';
                    $businessPhone = isset($postData['business_phone']) ? $postData['business_phone'] : '';
                    $companyName   = isset($postData['company_name']) ? $postData['company_name'] : '';
                    $pass          = isset($postData['password']) ? $postData['password'] : '';
                    $passConfirm   = isset($postData['confirm_password']) ? $postData['confirm_password'] : '';

                    if (empty($firstName)) {
                        $errorMessages[] = array(
                            'message'    => 'FORM_VALIDATION_NO_FIRST_NAME',
                            'field_name' => 'first_name'
                        );
                        $this->getResponse()->setStatusCode(500);
                        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                        echo json_encode(array('error_messages' => $errorMessages));
                        return sfView::NONE;
                    }

                    if (empty($lastName)) {
                        $errorMessages[] = array(
                            'message'    => 'FORM_VALIDATION_NO_SURNAME',
                            'field_name' => 'last_name'
                        );
                        $this->getResponse()->setStatusCode(500);
                        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                        echo json_encode(array('error_messages' => $errorMessages));
                        return sfView::NONE;
                    }

                    if (empty($businessPhone)) {
                        $errorMessages[] = array(
                            'message'    => 'FORM_VALIDATION_FIELD_EMPTY',
                            'field_name' => 'business_phone'
                        );
                        $this->getResponse()->setStatusCode(500);
                        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                        echo json_encode(array('error_messages' => $errorMessages));
                        return sfView::NONE;
                    }

                    if (empty($companyName)) {
                        $errorMessages[] = array(
                            'message'    => 'FORM_VALIDATION_FIELD_EMPTY',
                            'field_name' => 'company_name'
                        );
                        $this->getResponse()->setStatusCode(500);
                        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                        echo json_encode(array('error_messages' => $errorMessages));
                        return sfView::NONE;
                    }

                    if (empty($pass)) {
                        $errorMessages[] = array(
                            'message'    => 'FORM_VALIDATION_PASSWORD_EMPTY',
                            'field_name' => 'password'
                        );
                        $this->getResponse()->setStatusCode(500);
                        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                        echo json_encode(array('error_messages' => $errorMessages));
                        return sfView::NONE;
                    }

                    if (strlen($pass) < 6) {
                        $errorMessages[] = array(
                            'message'    => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS',
                            'field_name' => 'password'
                        );
                        $this->getResponse()->setStatusCode(500);
                        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                        echo json_encode(array('error_messages' => $errorMessages));
                        return sfView::NONE;
                    }

                    if ($pass != $passConfirm) {
                        $errorMessages[] = array(
                            'message'    => 'FORM_VALIDATION_PASSWORD_MISMATCH',
                            'field_name' => 'confirm_password'
                        );
                        $this->getResponse()->setStatusCode(500);
                        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

                        echo json_encode(array('error_messages' => $errorMessages));
                        return sfView::NONE;
                    }

                    Hermes_Client_Rest::call(
                        'updateContact',
                        array(
                            'contact_ref'    => $contactData['contact_ref'],
                            'first_name'     => $postData['first_name'],
                            'last_name'      => $postData['last_name'],
                            'business_phone' => $postData['business_phone'],
                            'organisation'   => $postData['company_name'],
                            'password'       => $postData['password']
                        )
                    );

                    try {
                        // Upgrade Account
                        $source      = new Source();
                        $accountInfo = Hermes_Client_Rest::call(
                            'upgradeEnhancedToPlus',
                            array(
                                'contact_ref' => $contactData['contact_ref'],
                                'service_ref' => 850,
                                'source'      => $source->get(
                                        'switch_to_plus',
                                        $request->getReferer(),
                                        $this->getUser()->getAttribute('homePageCroVariation', null, 'cro')
                                    )
                            )
                        );
                    } catch (Exception $e) {
                        $this->logMessage('Error in upgradeEnhancedToPlus : ' . $e->getMessage(), 'err');
                        $html = $plus_no_switch1;
                        echo json_encode(array('html' => $html));
                        return sfView::NONE;
                    }

                    $loginUser = Plus_Authenticate::logIn($contactData['email'], $postData['password']);

                    // New Code - Check PromoCode
                    if (!empty($postData['promocode'])) {
                        $this->setProductstoAccount(
                            $accountInfo['account']['id'],
                            $contactData['contact_ref'],
                            $postData['promocode'],
                            $plus_redirection_existing_promo
                        );
                        return sfView::NONE;
                    }

                    if ($loginUser == true) {
                        $html = $plus_redirection_new;
                    }
                } else {
                    $html = $plus_no_switch1;
                }
            } else {
                if (isset($postData['email'])) {
                    $pageVar = ($this->getUser()->getAttribute('homePageCroVariation', null, 'cro')) ? $this->getUser(
                    )->getAttribute('homePageCroVariation', null, 'cro') : 'Home';

                    // locale set
                    $browser  = get_browser(null, true);
                    $locale   = (isset($_SERVER['locale'])) ? $_SERVER['locale'] : "en_GB";
                    $language = (isset($_SERVER['language'])) ? $_SERVER['language'] : "en";
                    $country  = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

                    try {
                        $response = Hermes_Client_Rest::call(
                            'doPlusRegistration',
                            array(
                                'email'                => $postData['email'],
                                'first_name'           => $postData['first_name'],
                                'last_name'            => $postData['last_name'],
                                'password'             => $postData['password'],
                                'organisation'         => $postData['company_name'],
                                'phone'                => $postData['business_phone'],
                                'service_ref'          => 850,
                                'source'               => '/Plus_Reg_Success_' . $pageVar,
                                'country_code'         => $country,
                                'language_code'        => $language,
                                'timezone'             => date_default_timezone_get(),
                                'locale'               => 'en_GB',
                                'registration_ip'      => $_SERVER['REMOTE_ADDR'],
                                'registration_browser' => $browser['parent'],
                                'registration_os'      => $browser['platform']
                            )
                        );

                        $goal = 'plus';
                        try {
                            PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                            PWN_Logger::log(
                                'pin:' . $response['pins']['chairman']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                                PWN_Logger::INFO,
                                'trackerLog'
                            );
                        } catch (Exception $e) {

                        }
                        PWN_Tracking_GoalReferrers::achieved('plus', $response['pins']['chairman']['pin_ref'], true);

                        $loginUser = Plus_Authenticate::logIn($postData['email'], $postData['password']);
                        $html      = $plus_redirection_new;
                    } catch (Exception $e) {
                        // this scenario can cover the case of having not 801 pin
                        $this->logMessage('Error in doPlusRegistration : ' . $e->getMessage(), 'err');
                        $html = $plus_no_switch1;
                        echo json_encode(array('html' => $html));
                        return sfView::NONE;
                    }
                }
            }
        }

        if (isset($html)) {
            $result = array('html' => $html);
//            if (isset($tracking)) {
//                $result['tracking'] = $tracking;
//            }
            echo json_encode($result);
        }

        return sfView::NONE;
    }

    /**
     * Set Products to Account
     * @param $account_id
     * @param $contact_ref
     * @param $promocode
     * @param $plus_redirection_existing_promo
     */
    private function setProductstoAccount($account_id, $contact_ref, $promocode, $plus_redirection_existing_promo)
    {
        try {
            $promoInfo = Hermes_Client_Rest::call(
                'PromoCodes.usePromoCode',
                array(
                    'contact_ref' => $contact_ref,
                    'account_id'  => $account_id,
                    'promo_code'  => $promocode
                )
            );
            $html      = $plus_redirection_existing_promo;

            $productGroups = Hermes_Client_Rest::call(
                'getPlusAccountUnassignedProductGroups',
                array(
                    'account_id' => $account_id,
                )
            );

            foreach ($productGroups as $products) {
                $productGroupsArr[]                                          = $products['product_group_id'];
                $sessionAssignedArr['prod-' . $products['product_group_id']] = $products['product_group_id'];
            }

            $updateAccount = Hermes_Client_Rest::call(
                'updatePlusAccountProductGroups',
                array(
                    'account_id'                 => $account_id,
                    'assign_product_group_ids'   => $productGroupsArr,
                    'unassign_product_group_ids' => array(),
                )
            );

            $accountPins = Hermes_Client_Rest::call(
                'getAccountPinsByContactRef',
                array(
                    'contact_ref' => $contact_ref,
                )
            );
            foreach ($accountPins as $pin) {
                $updatePins = Hermes_Client_Rest::call(
                    'updatePlusPinProductGroups',
                    array(
                        'pin_ref'                    => $pin['pin_ref'],
                        'assign_product_group_ids'   => $productGroupsArr,
                        'unassign_product_group_ids' => array(),
                    )
                );
            }

            // Store the Above into Session - Testing
            $_SESSION['assigned_products']   = $sessionAssignedArr;
            $_SESSION['unassigned_products'] = array();
            $_SESSION['credit_amount']       = null;
            $_SESSION['promocode']           = true;

            echo json_encode(array('html' => $html));

        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(
                array(
                    'error_messages' => array(
                        array(
                            'message'    => 'FORM_VALIDATION_INVALID_PROMOCODE',
                            'field_name' => 'promocode'
                        )
                    )
                )
            );
        }
        return;
    }

    /**
     * Plus Signup First Step
     * @param sfWebRequest $request
     * @return string
     */
    public function executePlusSignupFirstStep(sfWebRequest $request)
    {

        $user = $this->getUser();

        $response = new AJAXResponse($this->getResponse());

        // Assert AJAX request.
        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            $this->forward404();
        }

        // Assert that user is anonymous or enhanced.
        $isAnonymous = !$user->isAuthenticated();
        $isEnhanced  = !$isAnonymous && $user->hasCredential('POWWOWNOW') && $user->getAttribute(
                'service'
            ) === 'powwownow';

        if (!$isAnonymous && !$isEnhanced) {
            $response->setToForbiddenErrorCode();
        } else {
            $email       = $request->getParameter('email', false);
            $termsAgreed = (int)$request->getParameter('terms_and_conditions', 0);

            if (!$this->isValidEmail($email)) {
                $response->addErrorMessage(
                    array('message' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'field_name' => 'email')
                );
            }

            if ($termsAgreed !== 1) {
                $response->addErrorMessage(
                    array('message' => 'TERMS_ARE_REQUIRED', 'field_name' => 'terms_and_conditions')
                );
            }

            // Stop parameter processing if we already have errors.
            if (!$response->hasErrors()) {
                // Assert parameters for enhanced users.
                if ($isEnhanced) {
                    $enhancedEmail = $user->getAttribute('email');

                    // Assert the enhanced user email is the same as the given email.
                    $email = trim($email);
                    if ($enhancedEmail !== $email) {
                        $response->addErrorMessage(array('message' => 'FORBIDDEN_EMAIL', 'field_name' => 'email'));
                        $response->close();
                    }

                    // Assert the enhanced user can switch.
                    if ($response->isOpen()) {
                        $contactRef = $user->getAttribute('contact_ref');

                        $contactData = false;
                        try {
                            $contactData = Hermes_Client_Rest::call(
                                'getContactByRef',
                                array('contact_ref' => $contactRef)
                            );
                        } catch (Exception $e) {
                            // unable to prepopulate values
                            $this->logMessage(
                                'getContactByRef threw an exception when trying to preopulate plus switch form',
                                'info'
                            );
                        }
                        if (plusCommon::isSwitchableToPlus($enhancedEmail) && $contactData) {
                            $response
                                ->addContent('status', 'success')
                                ->addNestedContent('form_result', 'account_status', 'LOGGEDIN_ENHANCED')
                                ->addNestedContent('form_result', 'email', $email)
                                ->addNestedContent('form_result', 'first_name', $contactData['first_name'])
                                ->addNestedContent('form_result', 'last_name', $contactData['last_name'])
                                ->addNestedContent('form_result', 'company_name', $contactData['organisation'])
                                ->addNestedContent('form_result', 'business_phone', $contactData['business_phone']);
                        } else {
                            // A non-switchable Enhanced user.
                            $response->addErrorMessage(array('message' => 'NON_SWITCHABLE_ENHANCED_USER'));
                            $response->close();
                        }
                    }
                } elseif ($isAnonymous) {

                    if ($response->isOpen()) {

                        $contactData = Common::getContact($email);
                        if (isset($contactData['error'])) {
                            $contactData = array();
                        }

//                        var_dump($contactData);
//                        die;
                        if (count($contactData)) {

                            if ($contactData['service_user'] === 'POWWOWNOW') {

                                $contactFirstName = !empty($contactData['first_name']) ? $contactData['first_name'] : '';
                                $contactLastName  = !empty($contactData['last_name']) ? $contactData['last_name'] : '';

                                //default password
                                if ($contactData['password'] === '1e895a7034cb8f2b1b7da1a6220c1737') {
                                    $response
                                        ->addNestedContent(
                                            'form_result',
                                            'account_status',
                                            'ENHANCED_USER_WITH_NOACCOUNT'
                                        );

                                } elseif ($contactData['password'] !== '1e895a7034cb8f2b1b7da1a6220c1737') {

                                    $response
                                        ->addNestedContent(
                                            'form_result',
                                            'account_status',
                                            'ENHANCED_USER_WITH_ACCOUNT'
                                        );
                                }


                            } elseif ($contactData['service_user'] !== 'POWWOWNOW') {
                                #*
                                $response->addErrorMessage(
                                    array('message' => 'PLUS_PREM_ACCOUNT', 'field_name' => 'email')
                                );
                                $response->close();
                            }

                        } elseif (!$contactData) {

                            $response
                                ->addContent('status', 'success')
                                ->addNestedContent('form_result', 'account_status', 'NEW_ENHANCED')
                                ->addNestedContent('form_result', 'email', $email);
                        }
                        if ($response->isOpen()) {
                            $response
                                ->addContent('status', 'success')
                                ->addNestedContent('form_result', 'email', $email)
                                ->addNestedContent('form_result', 'terms_and_conditions', 1)
                                ->addNestedContent(
                                    'form_result',
                                    'first_name',
                                    !empty($contactData['first_name']) ? $contactData['first_name'] : ''
                                )
                                ->addNestedContent(
                                    'form_result',
                                    'last_name',
                                    !empty($contactData['last_name']) ? $contactData['last_name'] : ''
                                );
                        }

                    }
                }
            }
        }

        return $response->outputResponse();
    }

    /**
     * Does the Email Exist
     * @param $email
     * @return bool
     */
    protected function doesContactWithEmailExist($email)
    {
        try {
            $contactData = Hermes_Client_Rest::call('getContact', array('email' => $email));
            return (bool)$contactData;
        } catch (Exception $e) {
            $this->logMessage('Could not retrieve contact from email ' . $email);
        }
        return false;
    }

    /**
     * Is the Email Valid
     * @param $email
     * @return bool
     */
    protected function isValidEmail($email)
    {
        // Type and length validation.
        if (!is_string($email) || strlen($email) > 50) {
            return false;
        }

        // Symfony email validation.
        $validationGenerator = new validationGroupFactory();
        $validator           = $validationGenerator->email();
        try {
            $validator->clean($email);
        } catch (sfValidatorError $e) {
            return false;
        }
        return true;
    }

    /**
     * Plus Signup Second Step
     * @param sfWebRequest $request
     * @return string
     */
    public function executePlusSignupSecondStep(sfWebRequest $request)
    {

        $user     = $this->getUser();
        $response = new AJAXResponse($this->getResponse());
        // Assert AJAX request.
        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            $this->forward404();
        }
        // $response->addContent('registration_status', 'success');
        // return $response->outputResponse();
        // Assert anonymous or enhanced user.
        $isAnonymous = !$user->isAuthenticated();
        $isEnhanced  = !$isAnonymous && $user->hasCredential('POWWOWNOW') && $user->getAttribute(
                'service'
            ) === 'powwownow';

        if (!$isAnonymous && !$isEnhanced) {
            $response->setToForbiddenErrorCode();
        } else {
            $enhancedEmail = $user->getAttribute('email', false);
            // Assert, for enhanced user, that the user can switch to plus.
            if ($isEnhanced && !plusCommon::isSwitchableToPlus($enhancedEmail)) {
                $response->addErrorMessage(array('message' => 'NON_SWITCHABLE_ENHANCED_USER'));
                $response->close();
            }


            // Continue if the response is open.
            if ($response->isOpen()) {
                $validationGenerator = new validationGroupFactory();
                $email               = $request->getParameter('email', false);
                $firstName           = $request->getParameter('first_name', false);
                $lastName            = $request->getParameter('last_name', false);
                $companyName         = $request->getParameter('company_name', false);
                $businessPhone       = $request->getParameter('business_phone', false);
                $password            = $request->getParameter('password', false);
                $confirmPassword     = $request->getParameter('confirm_password', false);

                // Assert, for both user types, that the first name, last name, company name and business phone are valid.
                $this->assertGenericPlusSignupParameters(
                    $response,
                    $validationGenerator,
                    $firstName,
                    $lastName,
                    $companyName,
                    $businessPhone
                );

                // Assert enhanced-only parameters.
                if ($isEnhanced) {
                    // Assert, for enhanced user, that the email matches their own email.
                    $email        = trim($email);
                    $enhancedData = Common::getContact($email);
                    if (isset($contactData['error'])) {
                        $enhancedData = array();
                    }
                    $enhancedEmail = trim($email);
                    /**
                     * @todo this orignal function will never work. needs to be fixed!
                     */
                    //$this->assertEnhancedPlusSignupParameters($response, $email, $enhancedEmail);

                    if (!$response->hasErrors()) {
                        $registrationResult = $this->registerEnhancedUserAsPlus(
                            $user->getAttribute('contact_ref'),
                            $enhancedEmail,
                            $firstName,
                            $lastName,
                            $companyName,
                            $businessPhone,
                            $request
                        );
                        if (!$registrationResult) {
                            $response->addErrorMessage(
                                array('message' => 'REGISTRATION_ERROR', 'field_name' => 'alert')
                            );
                        }
                    }
                } else {

                    // Assert anonymous-only parameters.
                    $this->assertAnonymousPlusSignupParameters(
                        $response,
                        $request,
                        $validationGenerator,
                        $email,
                        $password,
                        $confirmPassword
                    );

                    // Assert we have no errors; if we don't, register the user.

                    if (!$response->hasErrors()) {
                        $pageVar = $this->retrievePageVar();

                        $browser = $this->getBrowser();

                        $language = isset($_SERVER['language']) ? $_SERVER['language'] : "en";

                        $country = isset($_SERVER['country']) ? $_SERVER['country'] : "GBR";

                        $registrationResult = $this->registerAnonymousUserAsPlus(
                            $email,
                            $firstName,
                            $lastName,
                            $password,
                            $companyName,
                            $businessPhone,
                            $pageVar,
                            $country,
                            $language,
                            $browser
                        );

                        if (!$registrationResult) {
                            $response->addErrorMessage(array('message' => 'REGISTRATION_ERROR'));
                        }
                    }
                }
            }
        }

        if (!$response->hasErrors()) {
            $response->addContent('registration_status', 'success');
        }

        return $response->outputResponse();
    }

    /**
     * Get Browser Information
     * @return mixed
     */
    protected function getBrowser()
    {
        if (ini_get("browscap")) {
            if (!isset($_SERVER['HTTP_USER_AGENT'])) {
                $_SERVER['HTTP_USER_AGENT'] = 'Unknown';
                return get_browser($_SERVER['HTTP_USER_AGENT'], true);
            }
            return get_browser($_SERVER['HTTP_USER_AGENT'], true);
        } else {
            return array();
        }
    }

    /**
     * Check Plus Signup Parameters
     * @param AJAXResponse $response
     * @param validationGroupFactory $validationGenerator
     * @param $firstName
     * @param $lastName
     * @param $companyName
     * @param $businessPhone
     */
    public function assertGenericPlusSignupParameters(
        AJAXResponse $response,
        validationGroupFactory $validationGenerator,
        $firstName,
        $lastName,
        $companyName,
        $businessPhone
    ) {
        $validationResult = $this->validateGenericPlusSignupDetails(
            $validationGenerator,
            $firstName,
            $lastName,
            $companyName,
            $businessPhone
        );
        if (!$validationResult['first_name']) {
            $response->addErrorMessage(array('message' => 'INVALID_FIRST_NAME', 'field_name' => 'first_name'));
        }
        if (!$validationResult['last_name']) {
            $response->addErrorMessage(array('message' => 'INVALID_LAST_NAME', 'field_name' => 'last_name'));
        }
        if (!$validationResult['company_name']) {
            $response->addErrorMessage(array('message' => 'INVALID_COMPANY_NAME', 'field_name' => 'company_name'));
        }
        if (!$validationResult['business_phone']) {
            $response->addErrorMessage(array('message' => 'INVALID_BUSINESS_PHONE', 'field_name' => 'business_phone'));
        }
    }

    /**
     * Check Anonymous Plus Signup Parameters
     * @param AJAXResponse $response
     * @param sfWebRequest $request
     * @param validationGroupFactory $validationGenerator
     * @param $email
     * @param $password
     * @param $confirmPassword
     */
    protected function assertAnonymousPlusSignupParameters(
        AJAXResponse $response,
        sfWebRequest $request,
        validationGroupFactory $validationGenerator,
        $email,
        $password,
        $confirmPassword
    ) {
        // Assert email
        if (!$this->isValidEmail($email)) {
            $response->addErrorMessage(array('message' => 'INVALID_EMAIL', 'field_name' => 'email'));
        } else {
            if ($this->doesContactWithEmailExist($email)) {
                $response->addErrorMessage(array('message' => 'FORBIDDEN_EMAIL', 'field_name' => 'email'));
            }
        }

        // Assert, that the terms and conditions are set to 1.
        $termsAgreed = (int)$request->getParameter('terms_and_conditions', 0);
        if ($termsAgreed !== 1) {
            $response->addErrorMessage(
                array('message' => 'TERMS_ARE_REQUIRED', 'field_name' => 'terms_and_conditions')
            );
        }

        // Assert valid password.
        $passwordValidator = $validationGenerator->password()->setOption('required', true);
        $validPassword     = $this->quickValidate($passwordValidator, $password);
        if (!$validPassword) {
            $response->addErrorMessage(array('message' => 'INVALID_PASSWORD', 'field_name' => 'password'));
        }

        // Assert matching passwords.
        if ($password !== $confirmPassword) {
            $response->addErrorMessage(
                array('message' => 'PASSWORDS_DO_NOT_MATCH', 'field_name' => 'confirm_password')
            );
        }
    }

    /**
     * Obtain the Page Variation
     * @return string
     */
    protected function retrievePageVar()
    {
        $pageVar = $this->getUser()->getAttribute('homePageCroVariation', null, 'cro');
        if (!$pageVar) {
            $pageVar = 'Home';
        }
        return $pageVar;
    }

    /**
     * Validates the generic data of the third step of the plus signup process.
     *
     * @param validationGroupFactory $validationGenerator
     * @param string $firstName
     * @param string $lastName
     * @param string $companyName
     * @param string $businessPhone
     * @return array
     *   A map containing the result of the validation, to the name of each relevant field.
     */
    protected function validateGenericPlusSignupDetails(
        validationGroupFactory $validationGenerator,
        $firstName,
        $lastName,
        $companyName,
        $businessPhone
    ) {
        $validationResult = array(
            'first_name'     => true,
            'last_name'      => true,
            'company_name'   => true,
            'business_phone' => true,
        );

        $firstNameValidator             = $validationGenerator->firstName();
        $validationResult['first_name'] = $this->quickValidate($firstNameValidator, $firstName);

        $lastNameValidator             = $validationGenerator->lastName();
        $validationResult['last_name'] = $this->quickValidate($lastNameValidator, $lastName);

        $companyValidator                 = $validationGenerator->company()->setOption('required', true);
        $validationResult['company_name'] = $this->quickValidate($companyValidator, $companyName);

        $businessPhoneValidator             = $validationGenerator->mobileNumber()->setOption('required', true);
        $validationResult['business_phone'] = $this->quickValidate($businessPhoneValidator, $businessPhone);

        return $validationResult;
    }

    /**
     * Clean the Validation Checker
     * @param sfValidatorBase $validator
     * @param $value
     * @return bool
     */
    protected function quickValidate(sfValidatorBase $validator, $value)
    {
        try {
            $validator->clean($value);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Register an Anonymous User as Plus
     * @param $email
     * @param $firstName
     * @param $lastName
     * @param $password
     * @param $companyName
     * @param $businessPhone
     * @param $pageVar
     * @param $country
     * @param $language
     * @param array $browser
     * @param bool $login
     * @return bool
     */
    protected function registerAnonymousUserAsPlus(
        $email,
        $firstName,
        $lastName,
        $password,
        $companyName,
        $businessPhone,
        $pageVar,
        $country,
        $language,
        array $browser,
        $login = true
    ) {

        try {
            $hermes = Hermes_Client_Rest::call(
                'doPlusRegistration',
                array(
                    'email'                => $email,
                    'first_name'           => $firstName,
                    'last_name'            => $lastName,
                    'password'             => $password,
                    'organisation'         => $companyName,
                    'phone'                => $businessPhone,
                    'service_ref'          => 850,
                    'source'               => '/Plus_Reg_Success_' . $pageVar,
                    'country_code'         => $country,
                    'language_code'        => $language,
                    'timezone'             => date_default_timezone_get(),
                    'locale'               => 'en_GB',
                    'registration_ip'      => $_SERVER['REMOTE_ADDR'],
                    'registration_browser' => isset($browser['parent']) ? $browser['parent'] : null,
                    'registration_os'      => isset($browser['platform']) ? $browser['platform'] : null,
                )
            );

            $goal = 'plus';
            try {
                PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                PWN_Logger::log(
                    'pin:' . $hermes['pins']['chairman']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                    PWN_Logger::INFO,
                    'trackerLog'
                );
            } catch (Exception $e) {

            }
            PWN_Tracking_GoalReferrers::achieved('plus', $hermes['pins']['chairman']['pin_ref'], true);

        } catch (Exception $e) {
            $this->logMessage('Unable to register anonymous user as Plus.', 'err');
            return false;
        }
        if ($login) {
            Plus_Authenticate::logIn($email, $password);
        }
        return true;
    }

    /**
     * Register an Enhanced User as Plus
     * @param $contactRef
     * @param $email
     * @param $firstName
     * @param $lastName
     * @param $companyName
     * @param $businessPhone
     * @param null $request
     * @return bool
     */
    protected function registerEnhancedUserAsPlus(
        $contactRef,
        $email,
        $firstName,
        $lastName,
        $companyName,
        $businessPhone,
        $request = null
    ) {

        // Update Contact with New Information
        Hermes_Client_Rest::call(
            'updateContact',
            array(
                'contact_ref'    => $contactRef,
                'first_name'     => $firstName,
                'last_name'      => $lastName,
                'business_phone' => $businessPhone,
                'organisation'   => $companyName,
            )
        );

        // Upgrade Account from Enhanced to Plus
        $source = new Source();
        try {
            Hermes_Client_Rest::call(
                'upgradeEnhancedToPlus',
                array(
                    'contact_ref' => $contactRef,
                    'service_ref' => 850,
                    'source'      => $source->get(
                            'switch_to_plus',
                            $request->getReferer(),
                            $this->getUser()->getAttribute('homePageCroVariation', null, 'cro')
                        )
                )
            );
        } catch (Exception $e) {
            $this->logMessage('Error in upgradeEnhancedToPlus: ' . $e->getMessage(), 'err');
            return false;
        }

        // Log the User into Powwownow
        try {
            $contact = Hermes_Client_Rest::call(
                'getContact',
                array(
                    'email'       => $email,
                    'service_ref' => 850,
                )
            );

            if (!Plus_Authenticate::setCredentials($email, $contact)) {

                $this->logMessage('Unable to set credentials', 'err');
            }


        } catch (Exception $e) {

            Plus_Authenticate::logOut();

            $this->logMessage('Unable to set credentials for ' . $email, 'err');

            return false;
        }

        // Clear Teamsite cache
        try {
            PWN_Cache_Clearer::modifiedPin($contactRef);
            PWN_Cache_Clearer::modifiedContact($contactRef);
            $this->logMessage('Cleared Cache for contact_ref: ' . $contactRef . ' while switching to Plus', 'info');
        } catch (Exception $e) {
            $this->logMessage('Unable to clear cache after switching User to plus:' . serialize($e), 'err');
            return false;
        }
        return true;
    }

    /**
     * New Plus Registration / Switch Logic
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     */
    public function executePlusSignUpNew(sfWebRequest $request)
    {
        // All Post Arguments
        $arguments = $request->getPostParameters();

        // Set Globals
        $response = $this->getResponse();
        $user     = $this->getUser();
        $auth     = $user->isAuthenticated();

        // Set Output
        $response->setContentType('application/json');

        // Form Validation
        $email = isset($arguments['email']) ? $arguments['email'] : '';
        $tandc = isset($arguments['t_and_c']) ? $arguments['t_and_c'] : '';

        if (empty($email)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_NO_EMAIL', 'field_name' => 'email');
        } elseif (empty($tandc)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_T_AND_C', 'field_name' => 't_and_c');
        }

        if (!empty($errorMessages)) {
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Contact Information
        if (!$auth) {
            if (isset($arguments['email'])) {
                $contactData = Common::getContact($arguments['email'], null, true);
                if (isset($contactData['error'])) {
                    $contactData = array();
                }
            } else {
                $contactData = array();
            }
        } else {
            $contactData = Common::getContact($user->getAttribute('email'), $user->getAttribute('service_ref'), true);
            if (isset($contactData['error'])) {
                $contactData = array();
            }
        }

        $this->logMessage(__METHOD__ . ', Initial Contact Information: ' . print_r($contactData, true), 'err');

        // Forcefully goto the First Popup, due to some Javascript Features
        if (isset($arguments['init']) && $arguments['init']) {
            $result = array('output' => 'plus_email_form');
        } else {
            $result = plusCommon::doPlusRegistrationTemplateSelection($arguments, $user, $auth);
        }

        // Check if there were any Errors
        if (!empty($result['error'])) {
            $response->setStatusCode('500');
            $this->logMessage('Plus Registration Error Found:' . print_r($result['error']['errors'], true), 'err');
            echo json_encode($result['error']['errors']);
            return sfView::NONE;
        }

        // Contact Information Again
        if (!$contactData && $result['contactData']) {
            $contactData = $result['contactData'];
        }

        if (isset($contactData['email'])) {
            $email = $contactData['email'];
        } elseif (isset($arguments['email'])) {
            $email = $arguments['email'];
        } else {
            $email = '';
        }

        echo json_encode(
            array(
                'template'   => $result['output'],
                'first_name' => isset($contactData['first_name']) ? $contactData['first_name'] : '',
                'email'      => $email,
                'password'   => isset($arguments['password']) ? $arguments['password'] : ''
            )
        );

        return sfView::NONE;
    }

    /**
     * Confirm details and register/switch user to Plus
     *
     * @author Asfer Tamimi
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeEnablePlusAccountNew(sfWebRequest $request)
    {
        // All Post Arguments
        $arguments = $request->getPostParameters();
        $this->logMessage('Post Arguments: ' . print_r($arguments, true), 'err');

        // Set Globals
        $response     = $this->getResponse();
        $user         = $this->getUser();
        $auth         = $user->isAuthenticated();
        $isSwitchable = plusCommon::isSwitchableToPlus($arguments['email']);

        // User Information
        $userData = $user->getAttributeHolder()->getAll();

        // Set Output
        $response->setContentType('application/json');

        // Form Validation
        $email            = isset($arguments['email']) ? $arguments['email'] : '';
        $fname            = isset($arguments['first_name']) ? $arguments['first_name'] : '';
        $lname            = isset($arguments['last_name']) ? $arguments['last_name'] : '';
        $company          = isset($arguments['company_name']) ? $arguments['company_name'] : '';
        $phone            = isset($arguments['business_phone']) ? $arguments['business_phone'] : '';
        $password         = isset($arguments['password']) ? $arguments['password'] : '';
        $confirm_password = isset($arguments['confirm_password']) ? $arguments['confirm_password'] : '';

        if (empty($email)) {
            $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
        } elseif (empty($fname)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_NO_FIRST_NAME', 'field_name' => 'first_name');
        } elseif (empty($lname)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_NO_SURNAME', 'field_name' => 'last_name');
        } elseif (empty($company)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_COMPANY_NAME_EMPTY', 'field_name' => 'company_name');
        } elseif (empty($phone)) {
            $errorMessages = array(
                'message'    => 'FORM_VALIDATION_INVALID_PHONE_NUMBER',
                'field_name' => 'business_phone'
            );
        } elseif (strlen($password) < 6) {
            $errorMessages = array(
                'message'    => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS',
                'field_name' => 'password'
            );
        } elseif (empty($password)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_PASSWORD_EMPTY', 'field_name' => 'password');
        } elseif ($password != $confirm_password) {
            $errorMessages = array(
                'message'    => 'FORM_VALIDATION_PASSWORD_MISMATCH',
                'field_name' => 'confirm_password'
            );
        }

        if (!empty($errorMessages)) {
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Check if the User is Authenticated or Unauthenticated
        if ($auth) {
            $this->logMessage('Plus Logic :: User is Authenticated', 'err');
            $result = $this->enablePlusAccountNewAuthenticated(
                $response,
                $user,
                $isSwitchable,
                $arguments,
                $request,
                $auth,
                $userData
            );
        } else {
            $this->logMessage('Plus Logic :: User is Not Authenticated', 'err');
            $result = $this->enablePlusAccountNewUnauthenticated(
                $response,
                $user,
                $isSwitchable,
                $arguments,
                $request,
                $auth,
                $userData
            );
        }

        // Check if the Result is an Error or Template
        if (is_array($result)) {
            echo json_encode($result);
        } else {
            echo json_encode(array('template' => $result));
        }

        return sfView::NONE;
    }

    /**
     * Create a New Plus Account where you are already Logged in
     *
     * @param $response
     * @param $user
     * @param $isSwitchable
     * @param $arguments
     * @param $request
     * @param $auth
     * @return string
     */
    private function enablePlusAccountNewAuthenticated($response, $user, $isSwitchable, $arguments, $request, $auth)
    {
        if ($isSwitchable) {
            $this->logMessage('Plus Logic :: User is Switchable', 'err');
            $arguments['contact_ref'] = $user->getAttribute('contact_ref');
            $accountInfo              = $this->enablePlusAccountNewFormValidation(
                $arguments,
                $user,
                $request,
                $response,
                $auth
            );

            // Check if the Account Information is Either Empty or has an Error Message
            if (!$accountInfo) {
                return 'plus_no_switch1';
            } elseif (isset($accountInfo['error_message'])) {
                return $accountInfo;
            }

            // Get Contact Information for the Newly Converted Plus Account
            $contact = Common::getContact($arguments['email'], 850);

            // Switch / Registration is Successful. Now Log the User into Powwownow
            try {
                if (!Plus_Authenticate::setCredentials($arguments['email'], $contact)) {
                    throw new Exception('Unable to set credentials');
                }
            } catch (Exception $e) {
                Plus_Authenticate::logOut();
                $this->logMessage('Unable to set credentials for ' . $arguments['email'], 'err');
            }
            return 'plus_redirection_existing';
        } else {
            $this->logMessage('Plus Logic :: User cannot switch', 'err');
            return 'plus_no_switch1';
        }
    }

    /**
     * Create a New Plus Account where you are not Logged in
     * @param $response
     * @param $user
     * @param $isSwitchable
     * @param $arguments
     * @param $request
     * @param $auth
     * @param $userData
     * @return string
     */
    private function enablePlusAccountNewUnauthenticated(
        $response,
        $user,
        $isSwitchable,
        $arguments,
        $request,
        $auth,
        $userData
    ) {
        // Check the Contact Information for the Given Email Address
        if (isset($arguments['email'])) {
            $contactData = Common::getContact($arguments['email']);
            if ($contactData == array('error' => 'FORM_COMMUNICATION_ERROR')) {
                $contactData = array();
            }
        } else {
            $contactData = array();
        }

        $this->logMessage('Contact Data: ' . print_r($contactData, true), 'err');

        // Existing User, without a Password
        if (!empty($contactData)) {

            // Check if the User is Switchable
            if ($isSwitchable) {
                $this->logMessage('Plus Logic :: User is Switchable', 'err');
                $arguments['contact_ref'] = $contactData['contact_ref'];
                $accountInfo              = $this->enablePlusAccountNewFormValidation(
                    $arguments,
                    $user,
                    $request,
                    $response,
                    $auth
                );

                // Check if the Account Information is Either Empty or has an Error Message
                if (!$accountInfo) {
                    return 'plus_no_switch1';
                } elseif (isset($accountInfo['error_message'])) {
                    return $accountInfo;
                }

                // Switch is Successful. Now Log the User into Powwownow
                $loginUser = Plus_Authenticate::logIn($contactData['email'], $arguments['password']);

                if ($loginUser) {
                    return 'plus_redirection_existing';
                }
            } else {
                $this->logMessage('Plus Logic :: User is Not Switchable', 'err');
                return 'plus_no_switch1';
            }
        } else {

            // New User - Full Plus Registrations
            if (isset($arguments['email'])) {
                $this->logMessage('Plus Logic :: User is a New User', 'err');
                $pageVar = ($this->getUser()->getAttribute('homePageCroVariation', null, 'cro')) ? $this->getUser(
                )->getAttribute('homePageCroVariation', null, 'cro') : 'Home';

                // locale set
                $browser = get_browser(null, true);

                $result = plusCommon::doPlusRegistration(
                    array(
                        'email'                => $arguments['email'],
                        'first_name'           => $arguments['first_name'],
                        'last_name'            => $arguments['last_name'],
                        'password'             => $arguments['password'],
                        'organisation'         => $arguments['company_name'],
                        'phone'                => $arguments['business_phone'],
                        'service_ref'          => 850,
                        'source'               => '/Plus_Reg_Success_' . $pageVar,
                        'country_code'         => isset($_SERVER['country']) ? $_SERVER['country'] : 'GBR',
                        'language_code'        => isset($_SERVER['language']) ? $_SERVER['language'] : 'en',
                        'timezone'             => date_default_timezone_get(),
                        'locale'               => $user->getCulture(),
                        'registration_ip'      => $_SERVER['REMOTE_ADDR'],
                        'registration_browser' => isset($browser['parent']) ? $browser['parent'] : '',
                        'registration_os'      => isset($browser['platform']) ? $browser['platform'] : '',
//                        'email_source'         => 'product_selector_tool'
                    )
                );

                $this->logMessage('Plus Registration Result: ' . print_r($result, true), 'err');

                if (!empty($result)) {
                    $goal = 'plus';
                    try {
                        PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                        PWN_Logger::log(
                            'pin:' . $result['pins']['chairman']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                            PWN_Logger::INFO,
                            'trackerLog'
                        );
                    } catch (Exception $e) {

                    }
                    PWN_Tracking_GoalReferrers::achieved('plus', $result['pins']['chairman']['pin_ref'], true);

                    // Authenticate and Login the User
                    $loginUser = Plus_Authenticate::logIn($arguments['email'], $arguments['password']);
                    $this->logMessage('Plus Logic :: Redirecting User', 'err');
                    return 'plus_redirection_new';
                } else {
                    $this->logMessage('Plus Logic :: User cannot switch', 'err');
                    return 'plus_no_switch1';
                }
            }
        }
        return sfView::NONE;
    }

    /**
     * Does Form Validation for the enablePlusAccountNewForm Method
     *
     * @param array $arguments
     * @param object $user
     * @param object $request
     * @param object $response
     * @param bool $auth
     * @return array
     */
    private function enablePlusAccountNewFormValidation($arguments, $user, $request, $response, $auth)
    {
        $errorMessages = array();

        if ($auth) {
            $firstName     = isset($arguments['first_name']) ? $arguments['first_name'] : '';
            $lastName      = isset($arguments['last_name']) ? $arguments['last_name'] : '';
            $businessPhone = isset($arguments['business_phone']) ? $arguments['business_phone'] : '';
            $companyName   = isset($arguments['company_name']) ? $arguments['company_name'] : '';

            if (empty($firstName)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_NO_FIRST_NAME', 'field_name' => 'first_name');
            } elseif (empty($lastName)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_NO_SURNAME', 'field_name' => 'last_name');
            } elseif (empty($businessPhone)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_FIELD_EMPTY', 'field_name' => 'business_phone');
            } elseif (empty($companyName)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_FIELD_EMPTY', 'field_name' => 'company_name');
            }
        } else {
            $firstName     = isset($arguments['first_name']) ? $arguments['first_name'] : '';
            $lastName      = isset($arguments['last_name']) ? $arguments['last_name'] : '';
            $businessPhone = isset($arguments['business_phone']) ? $arguments['business_phone'] : '';
            $companyName   = isset($arguments['company_name']) ? $arguments['company_name'] : '';
            $pass          = isset($arguments['password']) ? $arguments['password'] : '';
            $passConfirm   = isset($arguments['confirm_password']) ? $arguments['confirm_password'] : '';

            if (empty($firstName)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_NO_FIRST_NAME', 'field_name' => 'first_name');
            } elseif (empty($lastName)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_NO_SURNAME', 'field_name' => 'last_name');
            } elseif (empty($businessPhone)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_FIELD_EMPTY', 'field_name' => 'business_phone');
            } elseif (empty($companyName)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_FIELD_EMPTY', 'field_name' => 'company_name');
            } elseif (empty($pass)) {
                $errorMessages = array('message' => 'FORM_VALIDATION_PASSWORD_EMPTY', 'field_name' => 'password');
            } elseif (strlen($pass) < 6) {
                $errorMessages = array(
                    'message'    => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS',
                    'field_name' => 'password'
                );
            } elseif ($pass != $passConfirm) {
                $errorMessages = array(
                    'message'    => 'FORM_VALIDATION_PASSWORD_MISMATCH',
                    'field_name' => 'confirm_password'
                );
            }
        }

        if (!empty($errorMessages)) {
            $response->setStatusCode(500);
            return array('error_messages' => $errorMessages);
        }

        // Update Contact with New Information
        $contactArgs = array(
            'contact_ref'    => $arguments['contact_ref'],
            'first_name'     => $arguments['first_name'],
            'last_name'      => $arguments['last_name'],
            'business_phone' => $arguments['business_phone'],
            'organisation'   => $arguments['company_name']
        );
        if (!$auth) {
            $contactArgs['password'] = $arguments['password'];
        }

        $result = Common::updateContact($contactArgs);

        // Upgrade Account from Enhanced to Plus
        $source = new Source();

        $accountInfo = plusCommon::upgradeEnhancedToPlus(
            array(
                'contact_ref' => $arguments['contact_ref'],
                'service_ref' => 850,
                'source'      => $source->get(
                        'switch_to_plus',
                        $request->getReferer(),
                        $user->getAttribute('homePageCroVariation', null, 'cro')
                    ),
//            'email_source' => 'product_selector_tool'
            )
        );

        // Commented out until we add tracking to all possible places to upgrade
        //  PWN_Tracking_GoalReferrers::achieved('basic_to_plus', $accountInfo['pins']['chairman']['pin_ref'], true);

        return $accountInfo;
    }

    /**
     * The Plus Existing Customer Functionality will be dealt with here
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executePlusExistingCustomer(sfWebRequest $request)
    {
        // All Post Arguments
        $arguments = $request->getPostParameters();

        // Global Variables
        $response = $this->getResponse();
        $user     = $this->getUser();

        // Set Output Type
        $response->setContentType('application/json');

        // Form Validation
        $email    = isset($arguments['email']) ? $arguments['email'] : '';
        $password = isset($arguments['password']) ? $arguments['password'] : '';

        if (empty($email)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_NO_EMAIL', 'field_name' => 'email');
        } elseif (empty($password)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_PASSWORD_EMPTY', 'field_name' => 'password');
        } elseif (strlen($password) < 6) {
            $errorMessages = array(
                'message'    => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS',
                'field_name' => 'password'
            );
        }

        if (!empty($errorMessages)) {
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Authenticate the User, Logs them in or Returns an Error
        try {
            Plus_Authenticate::logIn($arguments['email'], $arguments['password']);
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'Authenticate Failed. Args: ' . print_r($arguments, true) . ', Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            switch ($e->getCode()) {
                case '000:011:002':
                    $errorMessage = array(
                        'message'    => 'FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_ADMIN',
                        'field_name' => 'email'
                    );
                    break;
                case '000:011:003':
                    $errorMessage = array(
                        'message'    => 'FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_USER',
                        'field_name' => 'email'
                    );
                    break;
                case '000:011:001':
                    $errorMessage = array(
                        'message'    => 'FORM_VALIDATION_INVALID_PASSWORD',
                        'field_name' => 'password'
                    );
                    break;
                default:
                    $errorMessage = array(
                        'message'    => 'FORM_VALIDATION_LOGIN_ERROR',
                        'field_name' => 'email'
                    );
            }
            echo json_encode(array('error_messages' => $errorMessage));
            return sfView::NONE;
        }

        // Get Contact Information
        $contactInfo = Common::getContact($arguments['email'], $user->getAttribute('service_ref'));

        // Decide which Message to show based on the Service User
        switch ($contactInfo['service_user']) {
            case 'POWWOWNOW':
                $html = 'plusExistingCustomerPowwownow';
                break;
            case 'PLUS_USER':
                $html = 'plusExistingCustomerPlusUser';
                break;
            case 'PLUS_ADMIN':
                $html = 'plusExistingCustomerPlusAdmin';
                break;
            case 'PREMIUM_USER':
                $html = 'plusExistingCustomerPremiumUser';
                break;
            case 'PREMIUM_ADMIN':
                $html = 'plusExistingCustomerPremiumAdmin';
                break;
            default:
                // Unknown
                $html = '';
        }

        echo json_encode(
            array(
                'template'   => $html,
                'email'      => $arguments['email'],
                'first_name' => $contactInfo['first_name']
            )
        );

        return sfView::NONE;
    }

    /**
     * Confirm details and register/switch user to Plus
     *
     * @author Asfer Tamimi
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeEnablePlusAccountUpgradeFromDashboard(sfWebRequest $request)
    {
        // All Post Arguments
        $arguments = $request->getPostParameters();
        $this->logMessage('Post Arguments: ' . print_r($arguments, true), 'err');

        // Set Globals
        $response     = $this->getResponse();
        $user         = $this->getUser();
        $isSwitchable = plusCommon::isSwitchableToPlus($arguments['email']);

        // User Information
        $userData = $user->getAttributeHolder()->getAll();

        // Set Output
        $response->setContentType('application/json');

        // Form Validation
        $email                 = isset($arguments['email']) ? $arguments['email'] : '';
        $fname                 = isset($arguments['first_name']) ? $arguments['first_name'] : '';
        $lname                 = isset($arguments['last_name']) ? $arguments['last_name'] : '';
        $company               = isset($arguments['company_name']) ? $arguments['company_name'] : '';
        $phone                 = isset($arguments['business_phone']) ? $arguments['business_phone'] : '';
        $confirm_password      = isset($arguments['confirm_password']) ? $arguments['confirm_password'] : '';
        $arguments['password'] = $confirm_password;
        $tAndc                 = isset($arguments['t_and_c']) ? $arguments['t_and_c'] : '';

        if (empty($email)) {
            $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
        } elseif (empty($fname)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_NO_FIRST_NAME', 'field_name' => 'first_name');
        } elseif (empty($lname)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_NO_SURNAME', 'field_name' => 'last_name');
        } elseif (empty($company)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_COMPANY_NAME_EMPTY', 'field_name' => 'company_name');
        } elseif (empty($phone)) {
            $errorMessages = array(
                'message'    => 'FORM_VALIDATION_INVALID_PHONE_NUMBER',
                'field_name' => 'business_phone'
            );
        } elseif (strlen($confirm_password) < 6) {
            $errorMessages = array(
                'message'    => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS',
                'field_name' => 'confirm_password'
            );
        } elseif (empty($confirm_password)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_PASSWORD_EMPTY', 'field_name' => 'confirm_password');
        } elseif (empty($tAndc)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_T_AND_C_EMPTY', 'field_name' => 't_and_c');
        }

        // Check the Password against the one saved in the database
        if (empty($errorMessages)) {
            $result       = Common::getContact($email, 801, false);
            $passwordHash = md5($confirm_password);
            if ($passwordHash != $result['password']) {
                $errorMessages = array(
                    'message'    => 'FORM_VALIDATION_WRONG_PASSWORD',
                    'field_name' => 'confirm_password'
                );
            }
        }

        if (!empty($errorMessages)) {
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        $result = $this->enablePlusAccountNewAuthenticated(
            $response,
            $user,
            $isSwitchable,
            $arguments,
            $request,
            true,
            $userData
        );

        // Check if the Result is an Error or Template
        if (is_array($result)) {
            echo json_encode($result);
        } else {
            echo json_encode(array('template' => $result));
        }

        return sfView::NONE;
    }


    /**
     * Display social registration landing page
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS | sfView::NONE | redirect
     *
     * @author Michal Macierzynski
     * //@TODO check if data was retrieved correctly otherwise redirect to home page or somewhere else
     */
    public function executeSocialRegister(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        if ($user->isAuthenticated()) {
            $this->redirect('@mypwn_index');
        }

        $this->setVar('media', $request->getParameter('media'));

        $socialUser = new PwnSocialUser($this->getVar('media'));

        try {
            $socialUser->retrieveDataFromSocialMedia();
        } catch (Exception $e) {
            return sfView::ERROR;
        }

        $this->setVar('socialUser', $socialUser->getSocialUserData());

        return sfView::SUCCESS;
    }

    /**
     * Unset token
     * myPowwownow
     *
     * @param sfWebRequest $request
     * @return mixed
     *
     * @author Michal Macierzynski
     */
    public function executeSocialCancel(sfWebRequest $request)
    {
        require_once(sfConfig::get('sf_lib_dir') . '/PwnSocialUser.php');

        $media = $media = $request->getParameter('media');
        $flow  = $request->getParameter('flow');

        $socialUser = new PwnSocialUser($media);

        try {
            $socialUser->retrieveDataFromSocialMedia();
        } catch (Exception $e) {
            return sfView::ERROR;
        }

        $socialUser->resetToken();

        if ($flow == 'change') {
            $this->redirect('@social_connect?media=' . $media);
        } else {
            $this->redirect('@homepage');
        }
        exit;
    }

    /**
     * Registers free or plus user and when succeeded then links with social media account and redirects to
     * myPowwownow
     *
     * @param sfWebRequest $request
     * @return mixed
     *
     * @author Michal Macierzynski
     */
    public function executeSocialRegisterAjax(sfWebRequest $request)
    {

        $response = new AJAXResponse($this->getResponse());

        // Assert AJAX request.
        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            $this->forward404();
        }

        require_once(sfConfig::get('sf_lib_dir') . '/PwnSocialUser.php');

        $this->media = $media = $request->getParameter('media');

        $socialUser = new PwnSocialUser($media);

        try {
            $socialUser->retrieveDataFromSocialMedia();
        } catch (Exception $e) {
            $response->addErrorMessage(array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'));
        }

        if ($response->hasErrors()) {
            return $response->outputResponse();
        }


        $socialUserData = $socialUser->getSocialUserData();

        $email          = $request->getParameter('email');
        $first_name     = $request->getParameter('first_name');
        $last_name      = $request->getParameter('last_name');
        $company_name   = $request->getParameter('company_name');
        $business_phone = $request->getParameter('business_phone');
        $password       = $request->getParameter('password');
        $service        = $request->getParameter('service');

        $pageVar  = 'Social';
        $browser  = get_browser(null, true);
        $language = (isset($_SERVER['language'])) ? $_SERVER['language'] : "en";
        $country  = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Plus Success - Redirection to regNew
        $plus_redirection_new = $this->getPartial('plusRedirectionNewSocialRegister', array());

        // Plus No Switch
        $registerFail = $this->getPartial('socialRegisterFail', array());

        if ($service == 'Free') {
            try {
                $response = Hermes_Client_Rest::call(
                    'doFullRegistration',
                    array(
                        'email'                => $email,
                        'first_name'           => $first_name,
                        'last_name'            => $last_name,
                        'password'             => $password,
                        'organisation'         => $company_name,
                        'phone'                => $business_phone,
                        'service_ref'          => 801,
                        'source'               => '/Free_Reg_Success_' . $pageVar,
                        'locale'               => 'en_GB',
                        'registration_ip'      => $_SERVER['REMOTE_ADDR'],
                        'registration_browser' => $browser['parent'],
                        'registration_os'      => $browser['platform']
                    )
                );

                $goal = 'free';
                try {
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                    PWN_Logger::log(
                        'pin:' . $response['pin']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                        PWN_Logger::INFO,
                        'trackerLog'
                    );
                } catch (Exception $e) {

                }
                PWN_Tracking_GoalReferrers::achieved('free', $response['pin']['pin_ref'], true);
                Hermes_Client_Rest::call(
                    'sendBasicRegistrationEmailDotmailer',
                    array(
                        'pin_ref' => $response['pin']['pin_ref'],
                    )
                );

                $loginUser = Plus_Authenticate::logIn($email, $password);
                $socialUser->linkAccount($response['contact']['contact_ref']);
                $socialUser->updateSocialUserData();

                // Shomei API And PinRefHash
                $pinRef       = $response['pin']['pin_ref'];
                $shomei       = new Shomei();
                $shomeiResult = $shomei->registerFreePin($pinRef);

                $free_redirection_new = $this->getPartial(
                    'freeRedirection',
                    array(
                        'pin'        => $response['pin']['pin'],
                        'pinRefHash' => $shomeiResult['apiResult']['responsiveBody']['transactionIdentifier']
                    )
                );
                echo $free_redirection_new;
                return sfView::NONE;
            } catch (Exception $e) {
                // this scenario can cover the case of having not 801 pin
                $this->logMessage('Error in doFullRegistration : ' . $e->getMessage(), 'err');
                echo $registerFail;
                return sfView::NONE;
            }
        } else {
            try {
                $response = Hermes_Client_Rest::call(
                    'doPlusRegistration',
                    array(
                        'email'                => $email,
                        'first_name'           => $first_name,
                        'last_name'            => $last_name,
                        'password'             => $password,
                        'organisation'         => $company_name,
                        'phone'                => $business_phone,
                        'service_ref'          => 850,
                        'source'               => '/Plus_Reg_Success_' . $pageVar,
                        'country_code'         => $country,
                        'language_code'        => $language,
                        'timezone'             => date_default_timezone_get(),
                        'locale'               => 'en_GB',
                        'registration_ip'      => $_SERVER['REMOTE_ADDR'],
                        'registration_browser' => $browser['parent'],
                        'registration_os'      => $browser['platform']
                    )
                );

                $goal = 'plus';
                try {
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                    PWN_Logger::log(
                        'pin:' . $response['pins']['chairman']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                        PWN_Logger::INFO,
                        'trackerLog'
                    );
                } catch (Exception $e) {

                }
                PWN_Tracking_GoalReferrers::achieved('plus', $response['pins']['chairman']['pin_ref'], true);

                $loginUser = Plus_Authenticate::logIn($email, $password);
                $socialUser->linkAccount($response['contact']['contact_ref']);
                $socialUser->updateSocialUserData();

                echo $plus_redirection_new;
                return sfView::NONE;
            } catch (Exception $e) {
                // this scenario can cover the case of having not 801 pin
                $this->logMessage('Error in doPlusRegistration : ' . $e->getMessage(), 'err');
                echo $registerFail;
                return sfView::NONE;
            }
        }
    }

    /**
     * Authenticate powwownow user and if it's succeeded then tries to link powwownow user with social media account
     * and update it's data then it redirects to myPowwownow with success or fail message
     *
     * @param sfWebRequest $request
     * @return mixed
     *
     * @author Michal Macierzynski
     */
    public function executeSocialPwnAuthAndLinkAjax(sfWebRequest $request)
    {
        require_once(sfConfig::get('sf_lib_dir') . '/PwnSocialUser.php');

        $email    = $request->getParameter('email');
        $password = $request->getParameter('password');
        $media    = $request->getParameter('media');

        try {
            $r = Plus_Authenticate::logIn($email, $password);
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'Authenticate Failed. email: ' . $email . ', Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            switch ($e->getCode()) {
                case '000:011:002':
                    $errorMessage = 'FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_ADMIN';
                    break;
                case '000:011:003':
                    $errorMessage = 'FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_USER';
                    break;
                case '000:011:001':
                    $errorMessage = 'Your email and password are not recognised.<br/> Please amend and try again';
                    break;
                default:
                    $errorMessage = 'Your email and password are not recognised.<br/> Please amend and try again';

            }
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => $errorMessage,
                        'field_name' => 'password'
                    )
                )
            );
            return sfView::NONE;
        }

        $socialUser = new PwnSocialUser($media);

        try {
            $socialUser->retrieveDataFromSocialMedia();
        } catch (Exception $e) {
            return sfView::ERROR;
        }

        if ($this->getUser()->isAuthenticated() == true) {
            // when it's authenticated with powwownow checks if this social media account is not linked with
            // other contact_ref, if find any redirects to mypowwownow page with error message
            $checkContactRef = $socialUser->getContactRefByOAuthUser();
            if (count($checkContactRef) > 0) {
                if ($checkContactRef['contact_ref'] > 0
                    && $checkContactRef['contact_ref'] <> $this->getUser()->getAttribute('contact_ref')
                ) {
                    $redirectionFailAlreadyConnected = $this->getPartial(
                        'loginAndLinkingRedirection',
                        array(
                            'social_connect' => 'failAlreadyConnected',
                            'media'          => $media,
                        )
                    );
                    echo $redirectionFailAlreadyConnected;
                    return sfView::NONE;
                }
            }

            // when it's not linked with any contact_ref then tries to link this social media user
            // and update social user data and redirects to mypowwownow page with success message
            $socialUser->linkAccount($this->getUser()->getAttribute('contact_ref'));
            $socialUser->updateSocialUserData();

            $redirectionSuccess = $this->getPartial(
                'loginAndLinkingRedirection',
                array(
                    'social_connect' => 'success',
                    'media'          => $media,
                )
            );
            echo $redirectionSuccess;
            return sfView::NONE;
        }

        return sfView::ERROR;
    }

    public function executeSocialLinkAnother(sfWebRequest $request)
    {
        require_once(sfConfig::get('sf_lib_dir') . '/PwnSocialUser.php');

        $media = $request->getParameter('media');
        // initialize Pwn Social User instance
        try {
            $socialUser = new PwnSocialUser($media);
        } catch (Exception $e) {
            return sfView::ERROR;
        }

        $socialUser->resetToken();
        $this->redirect('@social_connect?media=' . $media);
        exit;
    }

    /**
     * This action handle logic for OAuth, when it's called depends on state it performs different behaviour:
     * - When user is NOT AUTHENTICATED WITH OAUTH ( token is not retrieved or it's expired
     *   then it redirects to OAuth dialog
     * - When it's AUTHENTICATED WITH OAUTH then depends on conditions it does:
     * -- When user is AUTHENTICATED WITH MYPOWWOWNOW tries to link social media account
     *    with this user, checks if it's not connected with any other contact_ref and links it,
     *    update data from social media and redirects to @mypwn_index with success message
     *    otherwise redirects to @mypwn_index with error message
     * -- When user is NOT AUTHENTICATED WITH MYPOWWOWNOW then checks if this social media account
     *    is linked with any contact_ref and if finds then login and redirects to @mypwn_index and
     *    update data from social media
     * -- When user is NOT AUTHENTICATED WITH MYPOWWOWNOW and if it's not linked with any contact_ref
     *    redirects to @social_register with given social media
     *
     * Apart of that if user don't grant access with OAuth dialog it detects that and redirects to
     * @homepage or @mypwn_index
     *
     * @param sfWebRequest $request
     * @return mixed
     *
     * @author Michal Macierzynski
     */
    public function executeSocialConnect(sfWebRequest $request)
    {
        require_once(sfConfig::get('sf_lib_dir') . '/PwnSocialUser.php');

        $media = $request->getParameter('media');

        // checks there is no error parameter when users back from OAuth dialog ( ie: deny granting access )
        // if detects error redirects to home page when not authenticated with powwownow otherwise to mypowwownow
        $error = false;
        switch ($media) {
            case "LinkedIn":
                $error = $request->getParameter('oauth_problem', false);
                break;
            case "Google":
            case "Facebook":
                $error = $request->getParameter('error', false);
                break;
        }
        if ($error !== false) {
            if ($this->getUser()->isAuthenticated() == false) {
                $this->redirect('@homepage');
            } else {
                $this->redirect('@mypwn_index');
            }
            exit;
        }

        // initialize Pwn Social User instance
        try {
            $socialUser = new PwnSocialUser($media);
        } catch (Exception $e) {
            return sfView::ERROR;
        }

        // this is main call either to authenticate with OAuth or to retrieve data using api for given social media
        // inside this method there is exit point whenever oauth_client_class sends headers and set up exit property
        $success = false;
        try {
            $success = $socialUser->retrieveDataFromSocialMedia();
        } catch (Exception $e) {
            return sfView::ERROR;
        }

        // if successfully retrieved data we check for state of this social media user
        if ($success) {
            // when it's not authenticated with powwownow
            if ($this->getUser()->isAuthenticated() == false) {
                // we search for linked account with this social user
                $contact_ref = $socialUser->getContactRefByOAuthUser();

                // if not found redirects to registration page
                if (count($contact_ref) == 0) {
                    $this->redirect('@social_register?media=' . $media);
                    exit;
                } else {
                    // if found then tries to authenticate updates social user data
                    // and redirects to mypowwownow page
                    if (Plus_Authenticate::logInWithContactRef($contact_ref['email'], $contact_ref['contact_ref'])) {
                        $socialUser->updateSocialUserData();
                        $this->redirect('@mypwn_index');
                        exit;
                    }
                }
            } else {
                // when it's authenticated with powwownow checks if this social media account is not linked with
                // other contact_ref, if find any redirects to mypowwownow page with error message
                $checkContactRef = $socialUser->getContactRefByOAuthUser();
                if (count($checkContactRef) > 0) {
                    if ($checkContactRef['contact_ref'] > 0 && $checkContactRef['contact_ref'] <> $this->getUser(
                        )->getAttribute('contact_ref')
                    ) {
                        $this->redirect('@mypwn_index?social_connect=failAlreadyConnected&media=' . $media);
                        exit;
                    }
                }
                // when it's not linked with any contact_ref then tries to link this social media user
                // and update social user data and redirects to mypowwownow page with success message
                $socialUser->linkAccount($this->getUser()->getAttribute('contact_ref'));
                $socialUser->updateSocialUserData();
                $this->redirect('@mypwn_index?social_connect=success&media=' . $media);
                exit;
            }
        }

        // any other scenario throws server error
        return sfView::ERROR;
    }


    /**
     * Disconnects social account from myPowwownow account and delete oauth_user data
     *
     * @param sfWebRequest $request
     *
     * @author Michal Macierzynski
     */
    public function executeSocialDisconnect(sfWebRequest $request)
    {
        $user = $this->getUser();
        if ($user->isAuthenticated()) {
            $contactRef  = $user->getAttribute('contact_ref');
            $oAuthUserId = $request->getParameter('oauth_user_id');
            $validUser   = Hermes_Client_Rest::call(
                'Social.getOAuthUsersByContactRef',
                array(
                    'contact_ref'   => $contactRef,
                    'oauth_user_id' => $oAuthUserId,
                )
            );
            if (count($validUser) == 1) {
                $deactivate = Hermes_Client_Rest::call(
                    'Social.deleteOAuthUser',
                    array(
                        'oauth_user_id' => $oAuthUserId,
                    )
                );
            }
        }
        $this->redirect('@account_details#social-media-accounts');
        exit;
    }
}
