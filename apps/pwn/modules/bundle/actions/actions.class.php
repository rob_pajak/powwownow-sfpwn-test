<?php

/**
 * bundle actions.
 *
 * @package    powwownow
 * @subpackage bundle
 */
class bundleActions extends sfActions
{
    public function executeDisabledAutoTopUpPage()
    {
        $this->breadcrumbs = array(
            array('Account Details','/myPwn/Account-Details')
        );
    }
}
