<?php $sf_response->setTitle(__('Auto Top-up')); ?>

<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <!-- page header -->
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Auto Top-up'), 'headingSize' => 'm','breadcrumbs' => $breadcrumbs, 'disableBreadcrumbs' => true)); ?>

    <div class="grid_24">
        <p><?php echo __('Auto Top-Up is disabled for your account as you are a Post-Pay customer.') ?></p>
    </div>

    <!-- page footer -->
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
