<?php $sf_response->setTitle(__('Bundle Allowance')); ?>
<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Bundle Allowance'), 'headingSize' => 'l', 'disableBreadcrumbs' => true)); ?>

    <div class="grid_24 clearfix">

        <?php if ($hasBundle): ?>

            <?php if ($isAdmin): ?>
                <p>
                    <?php echo __('You currently have <b>%1%</b> assigned to your account.', array('%1%' => $bundleName));?>
                </p>
            <?php else: ?>
                <p>
                    <?php echo __('You currently have <b>%1%</b> assigned to your pin.', array('%1%' => $bundleName));?>
                </p>
            <?php endif; ?>

            <p>
                <?php echo __(
                    'You have <b>%1%</b> minutes of your <b>%2%</b> minute allowance remaining. Your new allowance will start on <b>%3%</b>.',
                    array('%1%' => $remainingMinutes, '%2%' => $monthlyAllowance, '%3%' => $nextAllowanceStartDate)
                );?>
            </p>

            <p><?php echo __('To view your call history %1%.', array('%1%' => link_to(_('click here'), 'call_history')));?></p>

        <?php else: ?>

            <?php if ($isAdmin): ?>
                <p>
                    <?php echo __(
                        'You do not have a bundle assigned to your account. To purchase a bundle visit the %1%.',
                        array('%1%' => link_to(_('Products Page'), 'products_select')));
                    ?>
                </p>
            <?php else: ?>
                <p>
                    <?php echo __('You do not have a bundle assigned to your pin. If you would like a Bundle please contact your account admin.');?>
                </p>
            <?php endif; ?>

        <?php endif; ?>

        <?php include_component('commonComponents', 'socialMediaFooter'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
