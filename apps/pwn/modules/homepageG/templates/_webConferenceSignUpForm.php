<div id="web-conferencing-container" class="<?php echo $formId; ?>-container">
    <script type="text/html" id="web-conferencing-template">


        <% if (! _.isUndefined(status) && status === 'success') { %>

            <div class="content color white web-conferencing-form-success">
                <h3 class="sub-header">Thank You for Registering</h3>
                <p class="sub-header-content color white">We will be sending you an email with details on getting started, but why not try it out now? Your PIN and voice conference dial-in number are listed here. Simply download and install the Powwownow Web application and share your PIN and the website with your participants.</p>

                <ul class="list">
                    <li>Your unique PIN is: <%=response.pin %> </li>
                    <li>Conference calling number: 0844 4 73 73 73</li>
                    <li>Link to share with participants: <a href="http://powwownow.yuuguu.com" class="more color white" target="_blank">powwownow.yuuguu.com</a></li>
                </ul>
            </div>

        <% } else { %>
            <div class="already-customer">
                <p class="color white content">Already an existing customer, you can simply download and install the web application for free now! <a href="/myPwn/Web-Conferencing" class="yellow button radius">GO TO DOWNLOAD</a></a></p>
            </div>
            <p class="color white content">Not an existing customer? Register below.</p>

            <?php echo form_tag(url_for('homepage_g_web_conferencing_signup_ajax'),array('id' => "web-conferencing-form", 'class' => "web-conferencing-form clearfix")); ?>

                <div class="form-input-container">
                    <div class="form-field">
                        <?php echo $form['first_name']->render(array('class' => 'input-text meduim radius height-small', 'autocomplete'=>'off','placeholder' => 'First Name *','maxlength'=>'255', 'data-trigger'=>'change' ,'data-required'=>'true', 'data-type'=>'alphanum','data-required-message'=>'Please enter first name')); ?>
                    </div>
                    <div class="form-field">
                        <?php echo $form['last_name']->render(array('class' => 'input-text meduim radius height-small', 'autocomplete'=>'off','placeholder' => 'Last Name *','maxlength'=>'255' ,'data-trigger'=>'change', 'data-required'=>'true"', 'data-type'=>'alphanum','data-required-message'=>'Please enter last name')); ?>
                    </div>
                </div>
                <div class="form-input-container">
                    <div class="form-field">
                        <?php echo $form['email']->render(array('class' => 'input-text meduim radius height-small', 'autocomplete'=>'off','placeholder' => 'Your Email *', 'data-required'=>'true', 'data-trigger'=>'change','data-error-message'=>'Please enter a valid email address.')); ?>
                    </div>
                    <div class="form-field">
                        <?php echo $form['password']->render(array('id'=>'web-conferencing-form-password-field','class' => 'input-text meduim radius height-small', 'autocomplete'=>'off','placeholder' => 'Password *','maxlength' => 20,'data-required'=>'true','data-required-message'=>'Please enter a password','data-minlength'=>'6','autocomplete' => 'off')); ?>
                    </div>
                </div>

                <div class="form-input-container">
                    <div class="form-field">
                        <?php echo $form['confirm_password']->render(array('class' => 'input-text meduim radius height-small', 'autocomplete'=>'off','placeholder' => 'Confirm Password *','maxlength' => 20,'data-required'=>'true', 'data-required-message'=>'Please retype password','data-equalto' => '#web-conferencing-form-password-field','autocomplete' => 'off')); ?>
                    </div>

                    <div class="form-field custom dropdown">
                        <?php echo $form['how_heard']->render(array('id' => '', 'class'=>'input-text input-dropdown meduim radius height-small')); ?>
                    </div>
                </div>

                <div class="form-input-container last">
                    <div class="form-action">
                        <span class="text-left color white form-legend-item content">* Required</span>
                        <input type="submit" class="small yellow button radius" value="Register">
                    </div>

                    <div class="form-field">
                        <div class="throbber loading green absolute hide"></div>
                    </div>
                </div>
            </form>
        <% } %>

    </script>
</div>


