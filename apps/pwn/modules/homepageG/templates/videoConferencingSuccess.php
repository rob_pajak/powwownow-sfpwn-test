<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>
<!-- Start of content -->
<div id="video-conferencing-landing-page" class="container video-conferencing-landing-page">
    <div class="row">
        <div class="twelve columns">
            <div class="inner-content">
                <h1 class="landing-main-title headers-bg-style-5">Video Conferencing</h1>
                <h3 class="sub-header color teal">Share a clearer vision.</h3>
                <p class="sub-header-content color grey">With the all new video conferencing service powered by Vidyo™ technology, you can see and interact with your meeting participants whilst sharing content as though you were literally in the same room.</p>
            </div>
            
            <div><a class="video-conferencing-image absolute text-hidden" href="#" data-reveal-id="modal-video-conferencing-engage">Play</a></div>
            
            <div class="inner-content alt-background engage-desc">
                <h3 class="absolute headers-bg-style-1 color white text-center">Get Video Now</h3>
                <p class="color white">Talk to our specialists on 0800 022 9900 (+44 (0) 20 3398 9900 for mobile and international calls) or <a href="<?php echo url_for('@contact_us'); ?>" class="yellow button radius">Email Us</a></p>
            </div>
        </div>
    </div>
</div>

<?php include_component('homepageG', 'whyPwn', array('videoConferenceContent' => true)) ?>

<?php include_component('homepageG', 'videoConferencingDescription') ?>

<?php //include_component('homepageG', 'whyChooseUs') ?>

<!-- End of content -->
<?php include_partial('homepageG/footerPartial');?>