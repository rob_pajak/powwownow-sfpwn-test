<div id="plus-landing-page-desc" class="container landing-page-desc">
    <div class="row">
        <div class="twelve columns">
            <div class="row">
                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Why Choose Plus?</h3>
                        <ul class="features color grey">
                            <li>
                                <span class="title">Free, local or global numbers</span>
                                <span class="content">Choose from a selection of phone numbers and rates for your conference calls. As well as the regular 0844 shared-cost numbers, you can also choose 0800 Freephone, local or international numbers. That means you can give your attendees free or low-cost landline and mobile access from over 100 countries around the world.</span>
                            </li>
                            <li>
                                <span class="title">Manage multiple users</span>
                                <span class="content">Plus gives you the ability to manage multiple users under one account - you can invite and create users in your myPowwownow account area. Having all of your users under one account gives you added security and the ability to keep track and manage how the account is being used.</span>
                            </li>
                            <li>
                                <span class="title">Unlimited conference calls</span>
                                <span class="content">With Plus, you can hold unlimited conference calls on demand, and with individual Chairperson and Participant PIN sets you can run as many secure conference calls at the same time as you like.</span>
                            </li>
                            <li>
                                <span class="title">Unbeatable quality</span>
                                <span class="content">Unlike other conference call services, Powwownow has its own fully-owned infrastructure, so call quality is as good as your landline. Our conference calls use the exact same fibre optic cabling as your landline, and along with BT, we're the only conference call provider to have this quality.</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Great Features</h3>
                        <ul class="features color grey">
                            <li>
                                <span class="title">Branded Welcome Message</span>
                                <span class="content">In today’s competitive environment it’s more important than ever to stand out from the crowd.  Customise your conference calls with your company branding and add it to dedicated UK Landline or Freephone dial-in numbers.</span>
                            </li>
                            <li>
                                <span class="title">Free instant web conferencing</span>
                                <span class="content">Integrate your conference calls with our free web conferencing tool; you can instantly share your computer screen with your call participants (perfect for sharing documents or presentations).  There is no need for call participants to download or install any software – all they need is internet access.</span>
                            </li>
                            <li>
                                <span class="title">Real-time reporting</span>
                                <span class="content">Monitor how your account is being used by running reports to monitor call Participants, see how many people were on particular calls and how long the calls were.</span>
                            </li>
                            <li>
                                <span class="title">Instant conference call recording</span>
                                <span class="content">Record your conference calls with the touch of a button. Then after your call you can review, download and share your recording with your colleagues and keep them for your records.</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>
