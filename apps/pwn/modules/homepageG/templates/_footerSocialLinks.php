<div class="container">
    <div class="row">
        <div class="twelve columns">
            
            <div id="footer-social-links" class="row footer-social-links">
                
                <script type="text/template" id="footer-social-links-template">
                <div class="three columns">
                    <div class="inner-content">
                        <h4 class="sprite blog">Powwownow Blog</h4>
                        <ul class="list-fading">
                            <%= blog %>
                        </ul>
                    </div>
                </div>

                <div class="three columns">
                    <div class="inner-content">
                        <h4 class="sprite twitter">@powwownow</h4>
                        <ul class="list-fading">
                            <%= twitter %>
                        </ul>
                    </div>
                </div>

                <div class="three columns">
                    <div class="inner-content">
                        <h4 class="sprite facebook">facebook.com/Powwownow</h4>
                        <ul class="list-fading">
                            <%= facebook %>
                        </ul>
                    </div>
                </div>
                <div class="three columns">
                    <div class="inner-content">
                        <h4 class="sprite rss">Latest News & Press</h4>
                        <ul class="list-fading">
                            <%= news %>
                        </ul>
                    </div>
                </div>
                </script>
            </div>
        </div>
    </div>
</div>