<div id="engage-landing-page-desc" class="container landing-page-desc">
    <div class="row">
        <div class="twelve columns">
            <div class="row">
                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Powwownow Operator Assisted Calls</h3>
                        <ul class="features color grey">
                            <li>
                                <span class="title">Professional Conference Operator </span>
                                <span class="content">Professional announcer to look after all your conferencing needs, they will kick off the call with a customised scripted meeting and speaker introductions.</span>
                            </li>
                            <li>
                                <span class="title">Multi-lingual Operators</span>
                                <span class="content">Conducting a global call, and want to offer people the choice of their native language? We offer multi-lingual operators to help with your call.</span>
                            </li>
                            <li>
                                <span class="title">Worldwide Access</span>
                                <span class="content">We offer access to conferences from around the world.</span>
                            </li>
                            <li>
                                <span class="title">Web Conferencing</span>
                                <span class="content">Fully integrated with our high-quality audio conferencing, your presentation is communicated over the phone and supported online by slides.</span>
                            </li>
                            <li>
                                <span class="title">Conference Call Recording</span>
                                <span class="content">Don’t spend ages trying to scribble down notes, simply record the call and play back later. Tight on time? We also offer a transcription service of the meeting, so you only need to concentrate on the important stuff!</span>
                            </li>
                            <li>
                                <span class="title">Dial-in Replay Service </span>
                                <span class="content">Missed the beginning of the call, we offer a dial-in replay service which allows you to catch up on important conversations that were had. </span>
                            </li>
                            <li>
                                <span class="title">Web Streaming And Webcasts</span>
                                <span class="content">Webcasting solutions are proven to drive higher levels of audience engagement using a highly interactive and scalable platform for visual communications. Both audio and video webcasting deliver a powerful message. </span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Powwownow Webcast Solution</h3>
                        <p class="color grey sub-content">Many of our customers use Operator Assisted Calls for Marketing Webinars, Investors Relations, Training and Development, Town Hall Events, Human Resources and more. </p>
                        <p class="color grey sub-content">Whether you are organising an online conference for thousands of people or need extra assistance for a smaller webinar, the Powwownow Event Team will help you every step of the way to deliver a first-class presentation. </p>
                        <p class="color grey sub-content">One key feature of the operator assisted service is the Event Manager. A dedicated Event Manager handles all your requested services before, during and after your meeting.</p>
                        <ul class="features color grey last">
                            <li>
                                <span class="title">No Landline Phone Is Required</span>
                                <span class="content">Audio can be played (streamed) via your PC.</span>
                            </li>
                            <li>
                                <span class="title">Live Feedback</span>
                                <span class="content">Get instant feedback from attendees with survey, polling and chat features.</span>
                            </li>
                            <li>
                                <span class="title">Full Customisation</span>
                                <span class="content">Full branding options to reinforce your message.</span>
                            </li>
                            <li>
                                <span class="title">Rich Multimedia Tool</span>
                                <span class="content">A high impact tool for reaching large audiences.</span>
                            </li>
                        </ul>
                        <p class="color grey sub-content">For more information on Operator Assisted Calls please download the PDFs below.</p>

                        <ul class="download-list">
                            <li><span class="sprite pdf"></span><a href="/sfpdf/en/Attended_Product_Sheet.pdf" target="_blank" class="more">Attended Product Sheet</a></li>
                            <li><span class="sprite pdf"> </span><a href="/sfpdf/en/Added_Security_for_your_Operator_Assited_Calls.pdf" class="more">Added Security for your Operator Assited Calls</a></li>
                            <li><span class="sprite pdf"> </span><a href="/sfpdf/en/Operator_Assisted_Best_Practices.pdf" target="_blank" class="more">Operator Assisted Best Practices</a></li>
                            <li><span class="sprite pdf"> </span><a href="/sfpdf/en/Operator_Assisted_FAQ.pdf" target="_blank" class="more">Operator Assisted FAQ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>
