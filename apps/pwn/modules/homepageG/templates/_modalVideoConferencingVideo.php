<?php use_helper('pwnVideo') ?>  

<div id="modal-video-conferencing-engage" class="reveal-modal modal-video-conferencing-engage">
    <div class="inner-content">
        <a class="close-reveal-modal close-button">&#215;</a>
        <h2 class="sub-header color teal">Video Conferencing Demo<h2>
        <?php echo embed_html5_video($webConferencingHtml5VideoData)  ?>
        <p class="content color grey">
            Powwownow Video delivers simple, affordable, high quality video meetings. It's as easy as 1, 2, 3!
        </p>
        <ol class="content color grey ordered-list">
            <li>Login, download and install the application.</li>
            <li>Search or invite your contacts.</li>
            <li>Start your Powwownow Video conference! </li>
        </ol>
        <p class="content color grey">To find out more press play on the video demo.</p>
    </div>
</div>