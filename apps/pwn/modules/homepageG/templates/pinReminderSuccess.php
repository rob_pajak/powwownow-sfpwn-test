<?php include_partial('homepageG/topPagePartial');?>
<!-- Start of content -->
<div id="pin-reminder" class="container pin-reminder">
    <div class="row">
        <div class="twelve column">
            <div class="inner-content">
                <h1 class="landing-main-title">PIN Reminder</h1>
                <p class="sub-content">Forgotten your PIN, but have a call? Enter your email and we will send you your PIN details. On the move? As well as your email address, enter your mobile number and we can text you your PIN details too.</p>
                <span class="absolute sprite reminder"></span>
            </div>
            <div class="inner-content">
                <h3 class="absolute speech-bubble headers-bg-style-1">Get your PIN</h3>
                <div class="inner-content alt-background">
                     <?php include_component('homepageG', 'pinReminderForm'); ?>
                </div>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>

<?php include_partial('homepageG/footerPartial');?>