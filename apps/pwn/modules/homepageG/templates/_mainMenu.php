<nav class="clearfix">
    <ul id="menu">
        <li class="icon-arrow-down">
            <a>Services</a>
            <ul>
                <li class="sub-menu"><a class="sub-menu-title">Conference call</a>
                    <ul class="submenu-items">
                        <li><a href="/">Powwownow</a></li>
                        <li><a href="<?php echo url_for('plus_service');?>">Powwownow Plus</a></li>
                        <li><a href="<?php echo url_for('@premium_service'); ?>">Powwownow Premium</a></li>
                        <li><a href="<?php echo url_for('engage_service')?>">Powwownow Engage</a></li>
                        <li><a href="<?php echo url_for('@event_conference_calls'); ?>">Event call</a></li>
                        <li><a href="<?php echo url_for('@compare_services'); ?>">Compare services</a></li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="sub-menu-title">Web conference</a>
                    <ul class="submenu-items">
                        <li><a href="<?php echo url_for('@web_conferencing'); ?>">Powwownow Web</a></li>
                        <li><a href="<?php echo url_for('@how_web_conferencing_works'); ?>">How it works</a></li>
                        <li><a href="<?php echo url_for('@web_conferencing_get_started'); ?>">Get started</a></li>
                        <li class="last-child"><a href="http://powwownow.yuuguu.com/" target="_blank">Join a session</a></li>
                    </ul>
                
                </li>
                <li class="sub-menu last-child"><a class="sub-menu-title">Video conference</a>
                    <ul class="submenu-items">
                        <li><a href="<?php echo url_for('@video_conferencing'); ?>">Powwownow Video</a></li>
                        <li class="last-child"><a href="<?php echo url_for('engage_service')?>">Powwownow Engage</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a>Why Choose Us</a>
            <ul>
                <li><a href="<?php echo url_for('@how_we_are_different'); ?>">How we are different</a></li>
                <li><a href="<?php echo url_for('@costs'); ?>">Cost comparison</a></li>
                <li><a href="<?php echo url_for('@international_number_rates'); ?>">International dial-in numbers</a></li>
                <li><a href="<?php echo url_for('@going_green'); ?>">Going green</a></li>
                <li><a href="<?php echo url_for('@business_efficiency'); ?>">Business efficiency</a></li>
                <li class="last-child"><a href="<?php echo url_for('@testimonials'); ?>">Testimonials</a></li>
            </ul>
        </li>
        <li>
            <a>How It Works</a>
            <ul>
                <li><a href="<?php echo url_for('@faqs'); ?>">FAQs</a></li>
                <li><a href="<?php echo url_for('@how_conference_calling_works'); ?>">Conference calling</a></li>
                <li class="last-child"><a href="<?php echo url_for('@how_web_conferencing_works'); ?>">Web conferencing</a></li>
            </ul>
        </li>
        <li>
            <a>Contact Us</a>
            <ul class="last-ul">
                <li><a class="chatlink">Live Chat</a></li>
                <li class="last-child"><a href="<?php echo url_for('@contact_us'); ?>">Email us</a></li>
            </ul>
        </li>
    </ul>
</nav>
