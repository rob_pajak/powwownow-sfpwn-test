   <div id="ca-container" class="ca-container">
        <div class="ca-wrapper">
            <div class="ca-item ca-item-1">
                <div class="ca-item-main">
                    <h2>How do our prices compare?</h2>
                    
                    <div class="inner-content">
                        <ul>
                            <li class="pwn-rate">&pound;2.58 <span class="logo"></span></li>
                            <li class="bt-rate">&pound;16.98 <span class="logo"></span></li>
                        </ul>
                    </div>
                    <a href="<?php echo url_for('@costs'); ?>" class="more">More on Pricing >></a>

                </div>
    
            </div>
            
            <div class="ca-item ca-item-2">
                <div class="ca-item-main">
                    <h2>How it works</h2>

                    <div class="inner-content">
                        <a href="#" data-reveal-id="modal-threeeasysteps">
                            <img src="<?php echo getDataURI("/homepageG/img/three-easy-steps-play.png") ?>" alt="3 Easy Steps"/>
                        </a>
                    </div>
                </div>
    
            </div>
            
            <div class="ca-item ca-item-3">
                <div class="ca-item-main">
                    <h2><span class="highlight">98&#65130;</span> of our customers would recommend our services.</h2>
                </div>
            </div>
            
            <div class="ca-item ca-item-4">
                <div class="ca-item-main">
                    <div class="advert">
                        <h4 class="hero-element">Powwownow Web Conferencing</h4>
                        <p class="sub-content">
                        Bring your conference calls to life with our <span class="color green uppercase">free</span>
                        desktop sharing tool.
                        </p>
                        <a href="<?php echo url_for('@web_conferencing'); ?>" class="small yellow button radius">Find out more</a>
                    </div>
                </div>
            </div>
            

            <div class="ca-item ca-item-5">
                <div class="ca-item-main">
                    <div class="inner-content">
                        <a href="#" data-reveal-id="modal-engage">
                            <img src="<?php echo getDataURI('/homepageG/img/engage-still-play.png') ?>" alt="Powwownow Engage"/>
                        </a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    