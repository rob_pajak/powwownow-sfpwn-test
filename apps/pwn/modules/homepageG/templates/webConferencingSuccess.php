<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>
<!-- Start of content -->
<div id="web-conferencing-landing-page" class="container web-conferencing-landing-page">
    <div class="row">
        <div class="twelve columns">
            <div class="inner-content">
                <h1 class="landing-main-title headers-bg-style-5">Web Conferencing</h1>
                <h3 class="sub-header color teal">Now everyone can see what you’re talking about.</h3>
                <p class="sub-header-content color grey">Add a new dimension to your conferencing with Powwownow Web, the free desktop sharing tool to use in conjunction with Powwownow’s telephone conferencing service. </p>
            </div>
            
            <div><a class="web-conference-image absolute text-hidden" href="#" data-reveal-id="web-conferencing-engage">Play</a></div>
            
            <div class="inner-content alt-background description">
                <h3 class="absolute headers-bg-style-1 color white">Get Started Today</h3>
                <?php include_component('homepageG', 'webConferenceSignUpForm') ?>
            </div>
        </div>
    </div>
</div>

<?php include_component('homepageG', 'whyPwn', array('webConference' => true)) ?>

<?php include_component('homepageG', 'webConferenceDescription') ?>

<?php //include_component('homepageG', 'whyChooseUs') ?>

<!-- End of content -->
<?php include_partial('homepageG/footerPartial');?>