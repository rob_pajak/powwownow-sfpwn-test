<?php
    $headerTitle = isset($headerTitle) ? $headerTitle : 'Powwownow';

    $landingPageDesc = isset($landingPageDesc) ? $landingPageDesc : '';

    $showBlock = isset($showBlock) ? $showBlock : false; //bool

    $class = 'nine';
    if (!$showBlock) {
        $class  = 'twelve';
    }


?>

<div class="powwownow-landing-page-desc">
    <div class="row">
        <div class="<?php echo $class?> columns content-box-one">
           <h3 class="sub-header color teal"><?php echo $headerTitle?></h3>
           <div class="inner-content">
               <p class="color grey content"><?php echo $landingPageDesc?></p>
           </div>
        </div>
        <?php if ($showBlock): ?>
        <div class="three columns content-box-two">
            <div class="inner-content small-bg-style-1 color white">
                <h3 class="sub-header strong text-center">Get Started Today</h3>
                <p class="text-center content">Talk to our specialists on <span class="strong">0800 022 9930</span> or <span class="strong"><a href="" class="color white">Email Us</a></span></p>
            </div>
        </div>
        <?php endif;?>
    </div>
</div>