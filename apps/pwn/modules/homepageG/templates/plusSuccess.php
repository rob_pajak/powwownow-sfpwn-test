<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>
<!-- Start of content -->
<div id="plus-landing-page" class="container plus-landing-page">
    <div class="row">
        <div class="twelve columns">
            <div class="inner-content">
                <h1 class="landing-main-title headers-bg-style-5">Powwownow Plus</h1>
                <h3 class="sub-header">Wondering how Plus can benefit you? </h3>
                <p class="sub-header-content">Not only do you get all the great features of our free service but switching to Plus now gives you that extra professional shine.</p>
                <p class="sub-header-content">An addition of a Chairperson PIN gives you complete control and security over your calls and product purchases, whilst managing multiple users under one account.</p>
                <p class="sub-header-content">Always on the go or calling abroad? Plus could save you money, giving you access to dial-in numbers that are most suited to your call requirements. 
                Don’t miss an opportunity for that personal touch; purchase a branded welcome message and dedicated number to make that first impression count.</p>
            </div>
            <div class="absolute plus-image"></div>
            
            <div class="inner-content alt-background plus-desc">
                <h3 class="absolute headers-bg-style-1">Get Started Today</h3>
                
                <?php include_component('homepageG', 'registerPlusForm', array('formId' => 'plus-register')); ?>
            
            </div>
        </div>
    </div>
</div>

<?php include_component('homepageG', 'whyPwn', array('plusContent' => true)) ?>

<?php include_component('homepageG', 'plusDescription') ?>
<!-- End of content -->
<?php include_partial('homepageG/footerPartial');?>