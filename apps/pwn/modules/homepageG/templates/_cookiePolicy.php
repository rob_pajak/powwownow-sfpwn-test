<?php if (sfContext::getInstance()->getRequest()->getCookie('cookie_policy_accepted', false) != true): ?>
<div id="cookie-policy">
    <div class="container">
        <div class="row">
            <div class="ten columns">
                <p class="sub-content text-left">We use cookies to give you the best online experience. By using our website you agree to our use of cookies in
                accordance with our cookie policy. <a class="more" href="<?php echo url_for('@privacy'); ?>#cookies">Learn more</a></p>
            </div>
            
            <div class="two columns">
                <p class="sub-content"><a class="close" title="Close">&#215;</a></p>
            </div>
        </div>
    </div>
</div>

<script>
    yepnope({
        test : $.cookie,
        nope : '/shared/foundation2/javascripts/jquery.cookie.js',
        callback : function(url, result, key){
            $('#cookie-policy').on('click','.close', function(e){
                e.preventDefault();
                $.cookie("cookie_policy_accepted", true, { expires: 1500, path:'/' });
                $('div#cookie-policy').addClass('hide');
            });
        }
    });
</script>
<?php else: ?>
<?php sfContext::getInstance()->getResponse()->setCookie('cookie_policy_accepted', true, date('Y-m-d h:i:s', strtotime('+1500 days')), '/'); ?>

<?php endif; ?>
