<div id="conference-call-with-pwn" class="container conference-call-with-pwn">
    <div class="row">
       
        <div class="nine columns">
           <h2 class="text-left section-title">Conference Call with Powwownow</h2>
           <div class="inner-content">
              <p>
                  Powwownow is the leading <a href="<?php echo url_for('@free_conference_call'); ?>" class="more">free conference call provider</a> in the UK.
              </p>
              <p>
                  At Powwownow, we don't believe in doing things the hard way. That's why we offer instant 
                  <a href="<?php echo url_for('@conference_call'); ?>" class="more">conference calling</a>,
                  available 24/7, with as many participants as required, wherever they are in the world. And because our service is free, 
                  callers only pay the cost of their own phone call, which is added to their standard telecoms bill - nothing more!
              </p>
              <p>
                  Plus, because we are a telecommunications company in our own right, our customers can be guaranteed the 
                  best quality calls with the maximum number of features for the lowest possible price. 
                  That's why <a href="<?php echo url_for('@how_we_are_different'); ?>" class="more">no one does conference calling like we do!</a>
              </p>
           </div>
        </div>
        <div id="get-my-pin-container" class="three columns get-my-pin-container">
            <h3 class="text-center">Get Started Today</h3>
            <p class="sub-content text-center">Enter your email to generate your PIN</p>
            
            <?php
                /**
                 * Include Get My Pin Partial
                 */
                include_component('homepageG','getMyPin', 
                    array('formId'=>'get-my-pin-small-bottom',
                          'inputFieldClasses' => 'extra-small',
                          'buttonFieldClasses' => 'small')
                ); 
            ?>
        </div>
    </div>
</div>