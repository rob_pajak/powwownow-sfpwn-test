<div id="<?php echo $formId; ?>-container" class="<?php echo $formId; ?>-container">
    <script type="text/html" id="<?php echo $formId; ?>-template">
        <?php echo form_tag(url_for('homepage_g_pin_reminder_ajax'), array('id' => 'pin-reminder-form', 'class' => 'pin-reminder-form clearfix')); ?>
            <div class="form-input-container">
                <div class="form-field email">
                    <label>Email me my PIN</label>
                    <?php echo $form['email']->render(array('class' => 'input-text medium radius height', 'autocomplete' => 'off','placeholder' => 'Your email address*', 'maxlength' => 50,'data-required'=>"true", 'data-type'=>'email','data-error-message'=>"Please enter a valid email address.")); ?>
                </div>
            </div>
            <div class="form-input-container">
                <div class="form-field mobile-number">
                    <label>Text me my PIN as well (optional)</label>
                    <?php echo $form['mobile_number']->render(array('class' => 'input-text medium radius height', 'autocomplete' => 'off','placeholder' => 'Your mobile number', 'maxlength' => 50)); ?>
                </div>
            </div>
            <div class="form-input-container button-wrapper">
                <div class="form-actions">
                    <input id="<?php echo $formId ?>-button" type="submit" class="yellow button radius get-my-pin" value="Get My Pin" />
                </div>
                <div class="form-field">
                    <div class="throbber loading absolute green hide"></div>
                </div>

            </div>
            <div class="form-legend absolute clearifx">
                <p class="text-left color white content">* Required</p>
            </div>
        </form>
    </script>
</div>