<div class="container">
    <div class="row">
        
        <div class="twelve columns footer-links-outer">
            <div id="footer-links" class="row footer-links">
                <div class="three columns">
                    <h4 class="color teal">Services</h4>
                    <ul>
                        <li><a class="more" href="<?php echo url_for('@conference_call'); ?>">Conference Call</a></li>
                        <li><a class="more" href="<?php echo url_for('@video_conferencing'); ?>">Video Conference</a></li>
                        <li><a class="more" href="<?php echo url_for('@web_conferencing'); ?>">Web Conference</a></li>
                        <li><a class="more" href="<?php echo url_for('@engage_service'); ?>">Powwownow Engage</a></li>
                        <li><a class="more" href="<?php echo url_for('@event_conference_calls'); ?>">Event calls</a></li>
                        <li><a class="more" href="<?php echo url_for('@compare_services'); ?>">Compare services</a></li>
                    </ul>
                </div>

                <div class="three columns">
                    <h4 class="color teal">Guides</h4>
                    <ul>
                        <li><a class="more" href="<?php echo url_for('@costs'); ?>">What it costs</a></li>
                        <li><a class="more" href="<?php echo url_for('@how_conference_calling_works'); ?>">How conference calling works</a></li>
                        <li><a class="more" href="<?php echo url_for('@how_web_conferencing_works'); ?>">How web conferencing works</a></li>
                        <li><a class="more" href="<?php echo url_for('@international_number_rates'); ?>">International numbers</a></li>
                        <li><a class="more" href="<?php echo url_for('@faqs'); ?>">FAQs</a></li>
                        <li><a class="more" href="<?php echo url_for('@contact_us'); ?>">Contact us</a></li>
                    </ul>
                </div>

                <div class="three columns">
                    <h4 class="color teal">Powwownow</h4>
                    <ul>
                        <li><a class="more" href="<?php echo url_for('@about_us'); ?>">About us</a></li>
                        <li><a class="more" href="<?php echo url_for('@how_we_are_different'); ?>">How we are different</a></li>
                        <li><a class="more" href="<?php echo url_for('@going_green'); ?>">Going green</a></li>
                        <li><a class="more" href="<?php echo url_for('@business_efficiency'); ?>">Business efficiency</a></li>
                        <li><a class="more" href="<?php echo url_for('@news'); ?>">Latest news and press</a></li>
                        <li><a class="more" href="/blog">Blog</a></li>
                    </ul>
                </div>
                <div class="three columns">
                    <h4 class="color teal">Useful Links</h4>
                    <ul>
                        <li><a class="more" href="<?php echo url_for('homepage_g_dial_in_numbers')?>">International dial-in numbers</a></li>
                        <li><a class="more" href="<?php echo url_for('homepage_g_conference_controls')?>">In-conference controls</a></li>
                        <li><a class="more" href="<?php echo url_for('homepage_g_pin_reminder')?>">PIN reminder</a></li>
                        <li><a class="more" href="<?php echo url_for('homepage_g_schedule_a_conference_call')?>">Schedule a call</a></li>
                        <li><a class="more" href="<?php echo url_for('homepage_g_pin_mobile_app')?>">Mobile App</a></li>
                        <li><a class="more" href="<?php echo url_for('@tell_a_friend'); ?>">Tell a friend</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
