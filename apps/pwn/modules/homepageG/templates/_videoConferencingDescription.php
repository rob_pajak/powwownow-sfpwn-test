<div id="video-conferencing-landing-page-desc" class="container landing-page-desc">
    <div class="row">
        <div class="twelve columns">
            <div class="row">
                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Why choose Powwownow Video?</h3>
                        <ul class="features color grey">
                            <li>
                                <span class="title">HD Video Calling</span>
                                <span class="content">HD video makes calls more personal, engaging and productive. Removes the frustration, distraction and delays of poor quality video calling, provides a ‘real meeting’ experience. Adaptive Video Layering™ dynamically optimises video streams for each endpoint device and network conditions.</span>
                            </li>
                            <li>
                                <span class="title">Flexible Multi-Person Display</span>
                                <span class="content">With up to 200 participants possible on a video call, you can display up to 8 participants at a time (plus self-view) - with customisable personal screen layout, including active speaker, continuous presence and content sharing layout.</span>
                            </li>
                            <li>
                                <span class="title">Screen-Sharing</span>
                                <span class="content">Sharing your screen with your video participants enables you and your colleagues to view the same document at the same time. The shared screen application window can also be undocked.</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="six columns">
                    <div class="inner-content">
                        
                        <ul class="features color grey last">
                            <li>
                                <span class="title">Works On Hardware and Devices You Already Own </span>
                                <span class="content">Vidyo™ Desktop software is built to run on Windows, Mac or Linux-based desktop or laptop computers. Just a simple download and you’ll be ready to have a face-to-face conversation with colleagues and business partners from the office, from home or on the move.</span>
                            </li>
                            <li>
                                <span class="title">Dedicated Customer Support</span>
                                <span class="content">excellent customer service team if you ever have an issue, you will also be assigned a dedicated Account Manager that will be on hand to help out with any queries you may have.</span>
                            </li>
                            <li>
                                <span class="title">Recording</span>
                                <span class="content">Now there is an easy way for you to record and webcast your meetings with one-click through the VidyoPortal. VidyoReplay is a simple easy-to-use webcasting and recording application that will enable large numbers of users to watch via their web browser, either in real-time or at any time after the conference.</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>
