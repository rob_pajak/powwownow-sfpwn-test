<div id="non-supported-browser" class="non-supported-browser alert-box">
    <div class="main">
        <div class="row">
            <div class="ten columns">
                <?php  if ($pwn_support_level == 12) : ?>
                <p><?php echo __('Should you be experiencing functionality and rendering issues please <a class="more" href="' . url_for('@faqs') . '">click here</a> for more information'); ?></p>
                <?php  elseif ($pwn_support_level == 10) : ?>
                <p><?php echo __('Should you be experiencing functionality and rendering issues please <a class="more" href="' . url_for('@faqs') . '">click here</a> for more information'); ?></p>
                <?php  else : ?>
                <p><?php echo __('Should you be experiencing functionality and rendering issues please <a class="more" href="' . url_for('@faqs') . '">click here</a> for more information'); ?></p>
                <?php endif; ?>
            </div>
<script>
    yepnope({
        test : $.cookie,
        nope : '/shared/foundation2/javascripts/jquery.cookie.js',
        callback : function(url, result, key){
            $('#non-supported-browser').on('click','.close', function(e){
                e.preventDefault();
                $.cookie("non_supported_closed", true, { path:'/' });
                $('div#non-supported-browser').addClass('hide');
            });
        }
    });
</script>
            <div class="two columns">
                <p><a class="close" title="Close">&times;</a></p>
            </div>
        </div>
    </div>
</div>
