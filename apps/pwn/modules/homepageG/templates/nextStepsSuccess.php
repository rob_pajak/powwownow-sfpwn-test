<?php include_partial('homepageG/topPagePartial');?>
<!--start Content-->

    <?php include_component('homepageG', 'generatedPin', array('id'=>'top')) ?>
    <div class="container next-steps">
        <div class="row">
            <div class="twelve columns">
                <h3 class="headers-bg-style-7">What to do next?</h3>
            </div>
        </div>
    </div>
    <div id="request-welcome-pack" class="container request-welcome-pack">
        <div class="row">
            <div class="twelve columns">
                <div class="inner-content rounded-corners">
                    
                    <div class="row">
                        <div class="nine columns">
                            
                            <div class="row">
                                <div class="three columns">
                                    <span class="general-welcome-pack"></span>
                                </div>
                                <div class="nine column">

                                    <h3 class="section-title">Request a FREE Welcome Pack</h3>

                                    <?php include_component('homepageG', 'requestWelcomePackForm') ?>

                                </div>
                            </div>
                        </div>
                        <div class="three columns request-welcome-pack-side">
                            <h3 class="section-title">Personalised with your PIN and dial-in numbers</h3>
                            
                            <ul class="what-you-get">
                                <li class="sprite black-arrow">Wallet card</li>
                                <li class="sprite black-arrow"> Computer stickers</li>
                                <li class="sprite black-arrow">Phone stickers</li>
                            </ul>
                            
                            <ul class="conference-info">
                                <li class="highlight">Dial-in Number:</li>
                                <li>0844 4 73 73 73</li>
                            </ul>
                            <ul class="conference-info">
                                <li class="highlight">UK Mobile Shortcode:</li>
                                <li>87373 <span class="shortcode-price">(12.5p a min + VAT)</span></li>
                            </ul>
                            <ul class="conference-info">
                                <li class="highlight">PIN:</li>
                                <li><?php echo $pin;?></li>
                            </ul>
                        </div>
                        
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>

    <div id="schedule-call" class="container schedule-call">
        <div class="row">
            <div class="twelve columns">
                <div class="inner-content rounded-corners">
                    
                    <div class="row">
                        <div class="twelve columns">
                            
                            <div class="row">
                                <div class="three columns">
                                    <span class="general-disk"></span>
                                </div>
                                <div class="nine column">
                                    <h3 class="section-title">Schedule a call</h3>
                                    
                                        <div class="row schedule-call-container">
                                            <div class="eight columns">
                                                <h4>Scheduler Tool</h4>
                                                <p>Powwownow is all about making things easy for you. So we thought we would invent an easy way to invite people to join your conference call. </p>
                                            </div>
                                            <div class="four columns">
                                                <div class="button-wrapper">
                                                    <a class="yellow button radius fixed-length" onclick="_gaq.push(['_trackEvent', 'link', 'schedule-a-call', 'G']);" href="http://myscheduler.powwownow.com/" target="_blank">Go to Scheduler</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row schedule-call-container">
                                            <div class="eight columns">
                                                <h4>Email Details</h4>
                                                <p>Simply send an email to your attendees to invite them to a call, all you need to do is fill in their email address and the time of the call.</p>
                                            </div>
                                            <div class="four columns">
                                               <div class="button-wrapper">
                                                   <?php include_component('commonComponents','shareMyPinLink', array(
                                                        'classes' => 'yellow button radius fixed-length',
                                                        'linkContent' => 'Email details',
                                                        'pin' => $pin,
                                                        'tracking' => "_gaq.push(['_trackEvent', 'link', 'email-details', 'G']);"
                                                    ));?>
                                               </div>
                                            </div>
                                        </div>
                                    
                                        <div class="row schedule-call-container">
                                            <div class="eight columns">
                                                <h4>Plugin for Outlook </h4>
                                                <p>Install this plugin and invite your attendees through Outlook, it’s a quick and easy way to schedule your conference call.</p>
                                            </div>
                                            <div class="four columns">
                                                <div class="button-wrapper">
                                                    <a class="yellow button radius fixed-length" onclick="_gaq.push(['_trackEvent', 'link', 'outlook-plugin', 'G']);" href="<?php echo url_for('homepage_g_schedule_a_conference_call')?>">Download Plugin</a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="print-conference-details" class="container print-conference-details hide">
        <div class="row">
            <div class="twelve columns">
                <div class="inner-content rounded-corners">
                    <div class="row">
                        <div class="twelve columns">
                            
                            <div class="row">
                                <div class="three columns">
                                    <span class="general-printer"></span>
                                </div>
                                <div class="nine column">
                                    <h3 class="section-title">Print your personal conference details</h3>

                                        <div class="button-wrapper">
                                            <a class="yellow button radius" onclick="_gaq.push(['_trackEvent', 'link', 'pin-details-print', 'G']);" href="#">Print your PIN details</a>
                                            <a class="more" href="http://get.adobe.com/uk/reader/" target="_blank">Download Adobe PDF Reader</a>
                                        </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <?php include_component('homepageG', 'generatedPin', array('id'=>'bottom')) ?>

<!-- end Content-->
<?php include_partial('homepageG/footerPartial');?>