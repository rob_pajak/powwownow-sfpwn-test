<div id="international-links" class="container international-links">
    <div class="row">
        <div class="twelve columns">
            <div class="inner-content">
                <p class="text-center">
                    <a href="http://www.powwownow.ie" class="more">Conference Call IE</a> | 
                    <a href="http://www.powwownow.com" class="more">Conference Call US</a> |
                    <a href="http://www.powwownow.de" class="more">Telefonkonferenz DE</a> | 
                    <a href="http://www.powwownow.fr" class="more">Conférence Téléphonique FR</a> |
                    <a href="http://www.powwownow.nl" class="more">Conference CallNL</a> | 
                    <a href="http://www.powwownow.at" class="more">Telefonkonferenz AT</a> | 
                    <a href="http://www.powwownow.be" class="more">Conférence Vocale BE</a>| 
                    <a href="http://www.powwownow.ch" class="more">Telefonkonferenz CH</a> | 
                    <a href="http://www.powwownow.it" class="more">Teleconferenza IT</a> |
                    <a href="http://www.powwownow.pl" class="more">Konferencja glosowa PL</a> | 
                    <a href="http://www.powwownow.es" class="more">Conferencia Telefónica ES</a> |
                    <a href="http://www.powwownow.se" class="more">Telefonkonferens SE</a> | 
                    <a href="http://www.powwownow.co.za" class="more">Conference Call ZA</a> | 
                    <a href="http://can.powwownow.com" class="more">Conference Call CA</a> |
                    <a href="http://www.andrewjpearce.com" class="more">Entrepreneur Blog</a>
                </p>  
            </div>
        </div>
    </div>
</div>

