<?php
    $content = isset($content) ? $content : 'default';
?>
<div id="modal-getmypinemailtaken" class="reveal-modal medium modal-getmypinemailtaken">
    <h3 class="sub-header color teal">Hi</h3>
    <div class="inner-content">
    <?php if ($content === 'webConferencing'):?>
       <p class="color grey"> It seems you already have an account with us. Click the <span class="strong">'Go To Download'</span> button in the green box below.</p>
    <?php elseif($content === 'default'):?>
        <p class="color grey">It seems you already have an account with us. If you have forgotten your PIN please use the <a href="<?php echo url_for('homepage_g_pin_reminder')?>" class="more">PIN reminder form</a>.</p>
    <?php endif;?>
    </div>
  <a class="close-reveal-modal close-button">&#215;</a>
</div>


