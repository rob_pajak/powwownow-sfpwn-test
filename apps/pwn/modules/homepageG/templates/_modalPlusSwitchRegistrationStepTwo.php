<div id="modal-plusSwitchRegistrationStepTwo" class="reveal-modal">
    <div class="inner-content">
        <?php include_component('homepageG', 'plusSwitchRegistrationStepTwoForm', array('formId' => 'plus-switch-registration'))?>
    </div>
    <a class="close-reveal-modal close-button">&#215;</a>
</div>