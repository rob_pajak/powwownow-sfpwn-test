<?php 
    /**
     * @todo Better method for loading partials on different pages.
     */
    $currentAction = sfContext::getInstance()->getController()->getActionStack()->getEntry(0)->getActionName();  
    
    $currentModule = sfContext::getInstance()->getController()->getActionStack()->getEntry(0)->getModuleName();    
    
//     echo "$currentAction $currentModule";

// Components to display on HomepageG Index e.g homepage

if ($currentModule === 'homepageCro') {
    // Will always be indexE
    // never indexG even
    if ($currentAction === 'indexE') {

        include_partial('homepageG/conferenceCallWithPwn');
        include_component('homepageG','modalCarousel3EasySteps');
        include_component('homepageG', 'modalGetMyPinEmailTaken');
        include_component('homepageG', 'modalEngageVideo');

    }

} elseif ($currentModule === 'homepageG') {

    switch($currentAction) {

        case 'plus':
            include_component('homepageG', 'modalPlusSwitchRegistrationStepTwo');
            include_component('homepageG', 'modalAlreadyAPlusCustomer');
            break;
        case 'engage':
            include_component('homepageG', 'modalEngageVideo');
            break;

        case 'webConferencing':
            include_component('homepageG', 'modalWebConferencingVideo');
            include_component('homepageG', 'modalGetMyPinEmailTaken');
            break;

        case 'videoConferencing':
            include_component('homepageG', 'modalVideoConferencingVideo');
            break;

        case 'scheduleConferenceCall':
            include_component('homepageG', 'modalDownloadOutlookPlugin');
            break;
        case 'pinReminder':
            include_component('homepageG', 'modalPleaseLogin');
            break;
    }

} elseif ($currentModule === 'pages') {
    /**
     * We Also need to check our variations just incase we
     * have been forwarded internally.
     */
    switch($currentAction) {
        case 'plusService':
            include_component('homepageG', 'modalPlusSwitchRegistrationStepTwo');
            include_component('homepageG', 'modalAlreadyAPlusCustomer');
            break;
        case 'engageService':
            include_component('homepageG', 'modalEngageVideo');
            break;
        case 'webConferencing':
            include_component('homepageG', 'modalWebConferencingVideo');
            include_component('homepageG', 'modalGetMyPinEmailTaken', array('content'=>'webConferencing'));
            break;
        case 'videoConferencing':
            include_component('homepageG', 'modalVideoConferencingVideo');
            break;
    }
}


include_component('homepageG','notSurePwn');
include_partial('homepageG/footerLinks');
include_component('homepageG', 'footerSocialLinks');
include_partial('homepageG/footer');
include_partial('homepageG/footerInternationalLinks');

if ($currentModule === 'homepageG') {
    switch($currentAction) {
        case 'nextSteps':
            include_component('homepageG','googleAdWords');
            break;
    }
}

?>



