<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>
<!-- Start of content -->
<div id="engage-landing-page" class="container engage-landing-page">
    <div class="row">
        <div class="twelve columns">
            <div class="inner-content">
                <h1 class="landing-main-title headers-bg-style-5">Powwownow Engage</h1>
                <h3 class="sub-header color teal">A smarter way for people to communicate and collaborate.</h3>
                <p class="sub-header-content color grey">We have combined the highest quality HD video conferencing, phone and VoIP calls, presence, instant messaging and more – all through one single intuitive interface.  There’s no simpler way for people to share their ideas, all at once. Bring your conference call to life.</p>
            </div>
            
            <div><a class="engage-image absolute text-hidden" href="#" data-reveal-id="modal-engage">Play</a></div>
            
            <div class="inner-content alt-background engage-desc">
                <h3 class="absolute headers-bg-style-1">Get started today!</h3>
                <p class="color white">Talk to our specialists on 0800 022 9900 (+44 (0) 20 3398 9900 for mobile and international calls) or <a href="<?php echo url_for('@contact_us'); ?>" class="yellow button radius">Email Us</a></p>
            </div>
        </div>
    </div>
</div>

<?php include_component('homepageG', 'whyPwn', array('engageContent' => true)) ?>

<?php include_component('homepageG', 'engageDescription') ?>

<?php // include_component('homepageG', 'whyChooseUs') ?>
<!-- End of content -->
<?php include_partial('homepageG/footerPartial');?>
