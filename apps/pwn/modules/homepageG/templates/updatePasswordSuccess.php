<?php include_partial('homepageG/topPagePartial');?>
<!-- Start of content -->
<div id="password-reset-first" class="container password-reset-first">
    <div class="row">
        <div class="twelve column">
            <div class="inner-content">
                <h1 class="landing-main-title">Update Password</h1>
                <p class="sub-header-content">Simply fill in the details below to a password more memorable and get going with all the great features of myPowwownow.</p>
                <div class="inner-content">
                    <h3 class="absolute headers-bg-style-1">Change your Password</h3>
                    <span class="absolute general-password-reminder"></span>
                    <?php include_component('homepageG', 'passwordResetForm'); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_partial('homepageG/footerPartial');?>