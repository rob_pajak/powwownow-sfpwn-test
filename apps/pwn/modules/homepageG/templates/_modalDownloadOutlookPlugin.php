
<div id="modal-downloadOutlookPlugin" class="reveal-modal modal-downloadOutlookPlugin">
    <div class="inner-content">
        <div id="download-outlook-plugin-container" class="download-outlook-plugin-container">
            <script type="text/html" id="<?php echo $id; ?>-template">
                <h2 class="sub-header color teal">Please provide us with a few details</h2>
                <form id="<?php echo $id; ?>" method="post" action="/Outlook-Plugin" data-focus="none">
                    <div class="form-field">
                        <input type="text" name="email_address" class="<?php echo $class;?>" placeholder="Email Address *" maxlength="255" data-trigger="change" data-type="email" data-required="true" autocomplete="off" data-required-message="Please enter Email Address" />
                    </div>
                    <div class="form-field">
                        <input type="text" name="job_title" class="<?php echo $class;?>" placeholder="Job Title" maxlength="255" autocomplete="off" />
                    </div>
                    <div class="form-field">
                        <input type="text" name="company" class="<?php echo $class;?>" placeholder="Company Name" maxlength="255 autocomplete="off" /">
                    </div>
                    <div class="form-action">
                        <p class="text-align-left color grey form-legend-item">* Required</p>
                        <input type="submit" class="medium yellow button radius" value="GO" />
                    </div>
                    <div class="form-field">
                        <div class="throbber loading grey hide"></div>
                    </div>
                </form>
            </script>
        </div>
    </div>
    <a class="close-reveal-modal close-button">×</a>
</div>