<?php $formId = isset($formId) ? $formId : 'default-form-id'.rand(1000,9999); ?>

<div id="<?php echo $formId; ?>-container" class="<?php echo $formId; ?>-container">
    <script type="text/html" id="<?php echo $formId; ?>-template">
        
        <% if ( !_.isUndefined(account_status)) { %>
        
            <div id="accountType" class="hidden" data-account-type=<%=account_status %> ></div>
            <div id="accountEmail" class="hidden" data-email=<%= email %> ></div>
            <div id="accountFirstname" class="hidden" data-firstname=<%=first_name %> ></div>
            <div id="accountLastname" class="hidden" data-lastname=<%=last_name %> ></div>

            <% if (account_status === 'complete') { %>
            
                <p>Please wait while we redirect you.</p>
                
            <% } else { %>
                
                <% if (account_status === 'ENHANCED_USER_WITH_ACCOUNT' && status !== 'pending' && status !== 'success'){ %>
                    <h2 class="sub-header color teal">Enter your myPowwownow password</h2>
                    <p class="sub-content color grey">All of your existing Powwownow account settings will be transferred on to your Plus account, so there is nothing to worry about!</p>
                    <?php echo form_tag('/ajax/mypwn/login', array('id' => "$formId-login", 'class' => "$formId-login clearfix")); ?>
                        <div class="form-field-hidden">
                            <input type="hidden" name="loginEmail" data-type="email" data-required="true" data-trigger="change" data-error-message="Please enter a valid email address." value=<%= email %> />
                        </div>

                        <div class="form-field">
                            <input type="password" name="loginPassword" placeholder="Enter Password *" data-trigger="change" data-required="true" data-error-message="Please enter a password." />
                        </div>

                        <div class="form-action">
                             <input type="submit" class="medium yellow button radius" value="Continue">
                        </div>
                        <div class="form-field">
                            <div class="throbber loading grey hide"></div>
                        </div>
                    </form>
                
                <% } else { %>
                    <h2 class="sub-header color teal">Please provide us with a few details</h2>
                    <?php echo form_tag(url_for('plus_signup_second_step'), array('id' => $formId, 'class' => $formId . ' clearfix')); ?>
                        <div class="form-field-hidden">
                            <input type="hidden" name="terms_and_conditions" value="1"/>
                            <input type="hidden" name="email" value='<%=email%>' />
                        </div>
                        <div class="form-field">
                            <input type="text" name="first_name" class="input-text medium radius" placeholder="Firstname *" value='<%=first_name %>' maxlength="255" data-trigger="change" data-required="true" data-type="alphanum" autocomplete="off" data-required-message="Please enter firstname" />
                        </div>
 
                        <div class="form-field">  
                            <input type="text" name="last_name" class="input-text medium radius" placeholder="Lastname *" value='<%= last_name %>' maxlength="255" data-trigger="change" data-required="true" data-type="alphanum" autocomplete="off" data-required-message="Please enter lastname"/>
                        </div>

                        <div class="form-field">  
                            <input type="text" name="company_name" class="input-text medium radius" placeholder="Company *" maxlength="255" data-trigger="change" data-required="true" autocomplete="off" data-required-message="Please enter Company name"/>
                        </div>

                        <div class="form-field">
                            <input type="text" name="business_phone" class="input-text meduim radius" placeholder="Business Phone *" data-trigger="change" data-type="number" data-required="true" data-required-message="Please enter Business Phone number"/>
                        </div>

                        <div class="form-field">

                            <input type ="text" name="email-disabled" class="input-text medium radius" disabled placeholder="Email *" maxlength ="50" autocomplete="off" value=<%=email %> />

                        </div>  

                        <% if (account_status === 'NEW_ENHANCED' || account_status === 'ENHANCED_USER_WITH_NOACCOUNT') { %>
                            <div class="form-field">
                                <?php echo $form['password']->render(array('id'=> 'password-form-field-input','class' => 'input-text medium radius', 'placeholder' => 'New password*', 'maxlength' => 20,'data-required'=>"true","data-required-message"=>"Please enter a password",'data-minlength'=>"6",'autocomplete' => 'off')); ?>
                            </div>

                            <div class="form-field">
                                <?php echo $form['confirm_password']->render(array('class' => 'input-text medium radius', 'placeholder' => 'Retype password*', 'maxlength' => 20,'data-required'=>"true", "data-required-message"=>"Please retype password",'data-equalto' => '#password-form-field-input','autocomplete' => 'off')); ?>
                            </div>
                        <% } %>
                        
                        
                        <div class="form-action">
                             <p class="text-align-left color grey form-legend-item">* Required</p>
                            <input type="submit" class="medium yellow button radius" value="Continue">
                        </div>
                        
                        <div class="form-field">
                            <div class="throbber loading grey hide"></div>
                        </div>
                        
                        
                    </form>
                    
                <% } %>
            
            <% } %>
            
        <% } %>
        
    </script>
</div>