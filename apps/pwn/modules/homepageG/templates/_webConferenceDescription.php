<div id="video-conferencing-landing-page-desc" class="container landing-page-desc">
    <div class="row">
        <div class="twelve columns">
            <div class="row">
                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Great Features</h3>
                        <ul class="features color grey">
                            <li>
                                <span class="title">Share Your Desktop</span>
                                <span class="content">If you’ve been on a call before and thought, wouldn’t it be good if I could see what they are talking about – well now you can. Share your screen and collaborate more easily. </span>
                            </li>
                            <li>
                                <span class="title">Present Documents</span>
                                <span class="content">Web conferencing allows you to share your screen and present documents to those on the call with you. </span>
                            </li>
                            <li>
                                <span class="title">Free To Use </span>
                                <span class="content">Unlike many other expensive web conferencing systems, Powwownow web is free to use and it has all the same features.</span>
                            </li>
                            <li>
                                <span class="title">No Participant Installation</span>
                                <span class="content">There is no need for your meeting attendees to download or install anything – all they need is an internet connection and a web browser and they can start seeing your screen..</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Powwownow Web in Action</h3>
                        <ul class="features color grey">
                            <li>
                                <span class="title">Present Company Updates</span>
                                <span class="content">No need to leave your office or all be in one place.</span>
                            </li>
                            <li>
                                <span class="title">On-demand</span>
                                <span class="content">Facilitate spontaneous, on-demand events or training plans.</span>
                            </li>
                            <li>
                                <span class="title">Stick To The Agenda</span>
                                <span class="content">Chairing a conference call is much easier when it's a web conference call.</span>
                            </li>
                            <li>
                                <span class="title">Training</span>
                                <span class="content"> Your workforce can learn efficiently - and remotely - by web conference.</span>
                            </li>
                            <li>
                                <span class="title">Recruitment</span>
                                <span class="content">Discuss CV's interactively. Decide together on the right person for the job.</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>
