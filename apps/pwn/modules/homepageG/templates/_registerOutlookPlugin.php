<?php if ($authenticated) : ?>
    <div class="button-wrapper">
        <a id="download-plugin" href="/sfdownload/PowwownowSetup.exe" target="_blank" onclick="_gaq.push(['_trackEvent', 'link', 'outlook-plugin', 'G-schedule']);" class="small yellow button radius center">Download Plugin for Outlook</a>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#download-plugin').click(function(){
                $.ajax({
                    url: '<?php echo url_for('schedule_a_call_ajax'); ?>'
                });
            });
        });
    </script>
<?php else: ?>
    <div class="button-wrapper">
        <a id="download-plugin" class="small yellow button radius center" data-reveal-id="modal-downloadOutlookPlugin" >Download Plugin for Outlook</a>
    </div>
<?php endif; ?>