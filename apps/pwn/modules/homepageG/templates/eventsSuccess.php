<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>
    <!-- Start of content -->
    <div id="events-call-landing-page" class="container events-call-landing-page">
        <div class="row">
            <div class="twelve columns">
                <div class="inner-content">
                    <h1 class="landing-main-title headers-bg-style-10">Powwownow <span class="uppercase font-goth-it">Event Calls</span></h1>
                    <h3 class="sub-header color teal">Quality of communication is vital in today's competitive environment.</h3>
                    <p class="sub-header-content color grey">Whether it’s announcing financial results to investors, launching a product or keeping staff informed, how you communicate makes a difference. Our Operator Assisted Service offers fully customised solutions, adapted to your specific requirements, depending on your business, department or profession.</p>
                </div>
                <div class="absolute events-call-image"></div>

                <div class="inner-content alt-background events-call-desc">
                    <h3 class="absolute headers-bg-style-11">Get Powwownow <span class="uppercase font-goth-it small">Event Calls</span></h3>
                    <p class="color white text-center">Talk to our specialists on 0800 229 0700</p>
                </div>

            </div>
        </div>
    </div>

<?php include_component('homepageG', 'whyPwn', array('eventsCall' => true)) ?>

<?php include_component('homepageG', 'eventsCallDescription') ?>
    <!-- End of content -->
<?php include_partial('homepageG/footerPartial');?>