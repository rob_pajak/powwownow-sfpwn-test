<?php use_helper('pwnVideo') ?>  
<?php
// Inc below code where ever this modal is needed so it can be triggered.
//<a href="#" data-reveal-id="modal-threeeasysteps">
//<img src="<?php echo getDataURI("/homepageG/img/three-easy-steps-play.png") \?\>" alt="3 Easy Steps"/>
//</a>
?>
<div id="modal-threeeasysteps" class="small reveal-modal">
    <div class="inner-content">
        <a class="close-reveal-modal">&#215;</a>
        <?php echo output_video (
            array (
                'config' => array(
                    'width' => 400,
                    //'height' => 260,
                    'autoplay' => false,
                    'controls' => true
                ),
                'video' => array(
                    'image' => '/sfimages/video-stills/3-steps-video.jpg',
                    'mp4'   => '/sfvideo/pwn_3steps460x260_LR.mp4',
                    'webm'  => '/sfvideo/pwn_3steps460x260_LR.webm',
                    'ogg'   => '/sfvideo/pwn_3steps460x260_LR.ogv'
                )
            )
        ); ?>
    </div>
</div>
