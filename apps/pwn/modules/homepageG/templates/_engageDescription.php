<div id="engage-landing-page-desc" class="container landing-page-desc">
    <div class="row">
        <div class="twelve columns">
            <div class="row">
                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">Why choose Powwownow Engage?</h3>
                        <ul class="features color grey">
                            <li>
                                <span class="title">HD Video Calling</span>
                                <span class="content">HD video conferencing makes calls more engaging, personal and more productive. HD quality provides a ‘real meeting’ experience, removing the frustration, distraction and delays of poor quality video calling. Video allows you to connect and collaborate with multiple users at the same time.</span>
                            </li>
                            <li>
                                <span class="title">Screen Sharing</span>
                                <span class="content">Sharing your screen with your contacts brings your conversation to life and enables you and your colleagues to work on the same document at the same time as if you were sitting next to them in the office.  So, whether you’re working from home, in the office or with colleagues or clients in other locations, you can collaborate from anywhere in the world.</span>
                            </li>
                            <li>
                                <span class="title">Phone and VoIP Conference Calling</span>
                                <span class="content">You and your call participants have the option to make and receive calls in the most convenient way, depending whether you’re working on the move or from the office.  VoIP calls are free of charge and made through a headset attached to your PC. Conference calls made through a telephone are charged at competitive Powwownow conference call rates.</span>
                            </li>
                            <li>
                                <span class="title">Instant Messaging</span>
                                <span class="content">Chat one-to-one or in a group, with the ability to run multiple chats at the same time, with alerts that notify you when a message is received.  Using Instant Message you can see when people are available or away, meaning you can get instant answers to your quick questions.</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="six columns">
                    <div class="inner-content">
                        <h3 class="sub-title">What It Costs</h3>
                        <p class="color grey sub-content">All you pay is a monthly subscription fee, there is no up-front investment and the service is simple to scale as your business requirements change.</p>
                        <ul class="features color grey">
                            <li>
                                <span class="title">How Powwownow Engage Saves You Money</span>
                                <span class="content">Real-time communication means an increase in productivity whilst minimising company overheads and employee travel expenses. Using integrated VoIP calls helps reduce your call costs.</span>
                            </li>
                            <li>
                                <span class="title">One Click Collaboration</span>
                                <span class="content">Multiple communication tools in an easy to use single application. No complex technology or on-going IT support required.</span>
                            </li>
                            <li>
                                <span class="title">Enable Remote IT Support</span>
                                <span class="content">Screen sharing technology enables your IT team to provide remote support to employees who work from multiple locations.</span>
                            </li>
                            <li>
                                <span class="title">Secure Communication</span>
                                <span class="content">When using video calling and contact-to-contact communications from within the interface, all data is 128-Bit AES Encrypted.</span>
                            </li>
                            <li>
                                <span class="title">Faster Communication</span>
                                <span class="content">Presence allows you to see when your contacts are available, so you know the most appropriate way to contact them. Set your own Presence status to inform contacts of an appropriate time to contact you.</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>
