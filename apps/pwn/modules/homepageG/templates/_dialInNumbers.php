<div class="inner-content">
    <div class="header-container">
        <h1 class="landing-main-title headers-bg-style-4">Dial-in Numbers</h1>
        <h3 class="sub-header color teal">Locate your caller's country below and send them the corresponding number ready for your international conference call. </h3>
        <p class="sub-header-content"><a target="_blank" href="/sfpdf/en/Powwownow-Dial-in-Numbers.pdf" class="more">Download</a> or <a href="mailto:?subject=Powwownow&amp;body=Please%20download%20Powwownow%27s%20international%20dial-in%20numbers%20from%20http://powwownow.co.uk/sfpdf/en/Powwownow-Dial-in-Numbers.pdf" class="more">email</a> our international dial-in numbers guide.</p>
    </div>
<?php if (count($dialinNumbers) == 0):?>
    <div class="alert-box error">
       <p>This is embarrassing, we are experiencing a technical issues. Please try refreshing the page. Thank you</p>
    </div>
<?php else: ?>
<?php $c = 0; ?>
<?php $loop = count($dialinNumbers); ?>
    <div class="dialin-numbers-container">
        <div class="inner-content">
            <div class='col four column'>
                <?php foreach ($dialinNumbers as $dialinNumber) : ?>
                    <?php if ($c < $loop): ?>
                        <div class="dialin-number">
                            <span class="sprite-flags <?php echo strtolower($dialinNumber['country_code'])?>"></span>
                            <span class="country"><?php echo $dialinNumber['country_en']?></span>
                            <span class="number"><?php echo ($dialinNumber['national_formatted']); ?></span>
                        </div>
                        
                        <?php if ($c === 4): ?>
                            </div><div class='col four column'>
                        <?php elseif($c === 9):?>
                            </div><div class='col four column last'>
                        <?php endif; ?>
                        <?php $c++;?>
                    <?php endif;?>   
                <?php endforeach; ?>

            </div>
        
        </div>
    </div>
<?php endif; ?>
    <div class="inner-content">
        <div class="dialin-numbers-container">
            <div class='col four column small'>
                <div class="dialin-number">
                    <span class="sprite-flags worldwide"></span>
                    <span class="country"><?php echo $worldwideNumber['title']?> &amp;</span>
                    <span class="number"><?php echo $worldwideNumber['number']?></span>
                 </div>
                <div class="dialin-number">
                    <span class="sprite-flags skypeout"></span>
                    <span class="country"><?php echo $skypeNumber['title']?></span>
                    <span class="number"><?php echo $skypeNumber['number']?></span>
                 </div>
            </div>

            <div class='col four column small last'>
                <div class="dialin-number">
                    <span class="sprite-flags uk-mobile"></span>
                    <span class="country"><?php echo $mobileShortCode['title']?></span>
                    <span class="number"><?php echo $mobileShortCode['number']?></span>
                    <br>
                    <span class="shortcode-price">12.5p a min + VAT</span>
                 </div>
            </div>
        </div>
    </div>
</div>