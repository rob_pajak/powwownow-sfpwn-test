<div id="generated-pin-<?php echo $id?>" class="container generated-pin">
    <div class="row ">
        <div class="twelve columns">            
            <div class="inner-content ">
                <h1 class="text-center main-title">Your conference PIN is ready to use</h1>
                <div class="row">
                    <div class="seven offset-by-one columns">
                        <div class="inner-content conference-details">
                            <p class="uppercase label text-left">Uk Dial-in number</p>
                            <p class="number"><?= $defaultDialInNumber; ?></p>
                        </div>
                    </div>
                    <div class="four columns">
                        <div class="inner-content conference-details">
                            <p class="uppercase label text-left">Pin</p>
                            <p class="pin"><?= $pin; ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="twelve columns">
                        <div class="inner-content">
                            <?php if ($id === 'top') :?>
                                <p class="small-text text-center login-info">Your account has been created and password emailed to you  <a class="yellow button radius" href="/myPwn" onclick="_gaq.push(['_trackEvent', 'link', 'myPowwownow', 'G']);">Enter myPowwownow</a></p>
                            <?php elseif ($id === 'bottom') : ?>
                                <div class="social-wrapper clearfix">
                                    <p class="social-text hide">Share how easy it is to start conference calling on </p>
                                    <ul class="hide">
                                        <li><a href="https://twitter.com/Powwownow" target="_blank" class="sprite twitter">Twitter</a></li>
                                        <li><a href="http://www.facebook.com/powwownow" target="_blank" class="sprite facebook">Facebook</a></li>
                                        <li><a href="http://www.linkedin.com/company/powwownow" target="_blank" class="sprite linkedin">Linkedin</a></li>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator"></div>
        </div>
    </div>
</div>