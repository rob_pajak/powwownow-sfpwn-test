<div id="why-choose-us" class="container why-choose-us">
    <div class="row">
        <div class="four columns">
            <div class="inner-content">
                <h3 class="color teal sub-header">What our Customers say</h3>
                <p class="color grey content">The Engage solution is a first class conferencing solution that offers a stable, reliable and user friendly platform with a superb support mechanism behind it… a pleasant experience, as well as a professional support relationship with Powwownow.</p>
                <p class="color light-grey content">Clive Sawkins, CEO at BCS Global</p>
            </div>
        </div>
        <div class="four columns how-we-compare">
            <div class="inner-content">
                <h3 class="color teal sub-header">How do we compare?</h3>
                <ul class="graph clearfix">
                    <li class="powwownow"><span class="legend pwn-meduim">Powwownow</span></li>
                    <li class="other"><span class="legend color grey text-center">Other leading provider</span></li>
                </ul>
            </div>
        </div>
        <div class="four columns">
            <div class="inner-content">
                <h3 class="color teal sub-header">Trust us</h3>
                <ul class="ratings">
                    <li class=""></li>
                    <li class=""></li>
                    <li class=""></li>
                    <li class=""></li>
                    <li class=""></li>
                </ul>
                <p class="color grey content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eius <a href="" class="more">More</a></p>
            </div>
        </div>
    </div>
</div>