<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>
<!-- Start of content -->
<div id="mobile-app" class="container mobile-app">
    <div class="row">
        <div class="twelve column">
            <div class="inner-content">
                <div class="header-container">
                    <h1 class="landing-main-title headers-bg-style-8">Mobile App</h1>
                    <h3 class="sub-header color teal">Download the Powwownow app to schedule conference calling on the go! </h3>
                    <p class="sub-header-content">Schedule conferences, set automatic call alerts, plus store your details, so joining calls is only ever one touch away. </p>
                    <ul class="pwn-app clearfix">
                        <li>The Powwownow app brings you free mobile conference calling at the touch of a button.</li>
                        <li>Schedule conferences, set automatic call alerts and store details, so joining calls is only ever one touch away.</li>
                        <li>Share the app with fellow conference call attendees and missed calls will become a thing of the past.</li>
                    </ul>
                    <span class="absolute mobile"></span>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row get-apps">
        <div class="six column">
            <div class="inner-content">
                <h3 class="color teal">iPhone App</h3>
                <p class="color grey first-child">Download the Powwownow app from the iTunes store. </p>
                <a href="http://app.powwownow.com/" target="_blank" class="small yellow button radius left">Download Iphone App</a>
                <span class="sprite apple"></span>
            </div>
        </div>
        <div class="six column">
            <div class="inner-content">
                <h3 class="color teal">Android App</h3>
                <p class="color grey">Download the Powwownow app from Google Play now to start scheduling your calls on the move. </p>
                <a href="https://market.android.com/details?id=com.grppl.android.shell.CMBpwn&feature=search_result" target="_blank" class="small yellow button radius left">Download Android App</a>
                <span class="sprite google-play"></span>
            </div>
        </div>
        
    </div>
</div>
<!-- End of content -->

<?php include_partial('homepageG/footerPartial');?>