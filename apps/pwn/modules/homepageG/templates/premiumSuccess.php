<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>
<!-- Start of content -->
<div id="premium-landing-page" class="container premium-landing-page">
    <div class="row">
        <div class="twelve columns">
            <div class="inner-content">
                <h1 class="landing-main-title headers-bg-style-10">Powwownow <span class="uppercase font-goth-it">Premium</span></h1>
                <h3 class="sub-header color teal">Premium Quality. Premium Service. Without Premium Price Tag.</h3>
                <p class="sub-header-content color grey">With Premium service, you'll get the same no-hassle conference calls as our regular service. On top, you'll get added features including unlimited conference calls and conference call PINs, personalised welcome messages and a choice of 0844, 0800, 0203 and international phone numbers.</p>
            </div>
            <div class="absolute premium-image">

            </div>
            
            <div class="inner-content alt-background premium-desc">
                <h3 class="absolute headers-bg-style-11">Get Powwownow <span class="uppercase font-goth-it small">Premuim</span></h3>
                
                
                <p class="color white">Talk to our specialists today on 0800 022 9781 (+44 (0) 20 3398 0102 for mobile and international calls) or <a href="<?php echo url_for('@contact_us'); ?>s" class="yellow button radius">Email Us</a></p>
            </div>

        </div>
    </div>
</div>

<?php include_component('homepageG', 'whyPwn', array('premiumContent' => true)) ?>

<?php include_component('homepageG', 'premiumDescription') ?>
<!-- End of content -->
<?php include_partial('homepageG/footerPartial');?>