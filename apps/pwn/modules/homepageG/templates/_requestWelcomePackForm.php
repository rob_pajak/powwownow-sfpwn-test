<?php ?>
<div id="<?php echo $formId; ?>-container" class="<?php echo $formId; ?>-container">
    <script type="text/html" id="<?php echo $formId; ?>-template">

        <% if ( ! _.isUndefined(welcomePackOrdered) && welcomePackOrdered === true) { %>
        <div class="inner-content">
            <h3 class="header-bg">Thanks for your request!</h3>
            <p class="sub-title">You have successfully requested a Powwownow Welcome Pack </p>
            <p class="sub-content">Your Welcome Pack is on its way! Why not schedule your next call or print your details below.</p>
        </div>
        <% } else {%>
        <?php echo form_tag(url_for('homepage_g_next_steps_ajax'), array('id' => $formId, 'data-focus'=>'none')); ?>
        <div class="input-container clearfix">
            <div class="form-field firstname">
                <?php echo $form['first_name']->render(array('class' => 'input-text small radius reduce height-small', 'placeholder' => 'First Name *', 'autocomplete'=>'off','maxlength' => 30, 'data-required'=>"true", 'data-error-message'=>'Please enter firstname.')); ?>
            </div>
            <div class="form-field lastname">
                <?php echo $form['last_name']->render(array('class' => 'input-text small radius reduce height-small', 'placeholder' => 'Last Name *', 'autocomplete'=>'off', 'maxlength' => 30, 'data-required'=>"true", 'data-trigger'=>'change','data-error-message'=>'Please enter last name.')); ?>
            </div>
        </div>
        <div class="input-container clearfix">
            <div class="form-field company">
                <?php echo $form['company']->render(array('class' => 'input-text small radius reduce height-small', 'placeholder' => 'Company', 'autocomplete'=>'off', 'maxlength' => 70)); ?>
            </div>
        </div>
        <div class="input-container clearfix">
            <div class="form-field address">
                <?php echo $form['address']->render(array('class' => 'input-text large radius reduce height-small', 'placeholder' => 'Address*', 'autocomplete'=>'off', 'maxlength' => 70, 'data-required'=>"true",  'data-trigger' => 'change','data-error-message'=>'Please enter address.')); ?>
            </div>
        </div>
        <div class="input-container clearfix">
            <div class="form-field town">
                <?php echo $form['town']->render(array('class' => 'input-text small radius reduce height-small', 'placeholder' => 'Town*', 'autocomplete'=>'off', 'maxlength' => 50, 'data-required'=>"true",  'data-trigger' => 'change','data-error-message'=>'Please enter town.')); ?>
            </div>
            <div class="form-field county">
                <?php echo $form['county']->render(array('class' => 'input-text small radius reduce height-small', 'placeholder' => 'County', 'autocomplete'=>'off', 'maxlength' => 50, 'data-trigger' => 'change')); ?>
            </div>
        </div>
        
        <div class="input-container clearfix">
            <div class="form-field postcode">
                <?php echo $form['postcode']->render(array('class' => 'input-text small radius reduce height-small', 'placeholder' => 'Postcode*', 'autocomplete'=>'off', 'maxlength' => 20, 'data-trigger' => 'change', 'data-required'=>"true", 'data-regexp' => '^([A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKS-UWa-hjks-uw])\ {0,1}[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|([Gg][Ii][Rr]\ 0[Aa][Aa])|([Ss][Aa][Nn]\ {0,1}[Tt][Aa]1)|([Bb][Ff][Pp][Oo]\ {0,1}([Cc]\/[Oo]\ )?[0-9]{1,4})|(([Aa][Ss][Cc][Nn]|[Bb][Bb][Nn][Dd]|[BFSbfs][Ii][Qq][Qq]|[Pp][Cc][Rr][Nn]|[Ss][Tt][Hh][Ll]|[Tt][Dd][Cc][Uu]|[Tt][Kk][Cc][Aa])\ {0,1}1[Zz][Zz]))$','data-error-message'=>'Please enter postcode.')); ?>
            </div>  
        </div>
        
        
        <div class="button-wrapper clearfix">
            <span class="required">* Required </span>
            <input id="<?php echo $formId ?>-button" type="submit" class="small yellow button radius left" value="Request welcome pack" />
        </div>
        
        <div class="form-field">
            <div class="throbber loading hide"></div>
        </div>
        
        </form>
        <% } %>
    </script>
</div>