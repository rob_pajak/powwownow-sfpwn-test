<?php if (count($websites)):?>

<ul id="international" class="international">
    <li class="text-left international-options"><a><span class="sprite-flags gbr"></span>EN</a>
        <ul>
            <?php foreach($websites as $website) :?>
            <li><a href="http://<?php echo $website['domain']?>"><span class="sprite-flags <?php echo strtolower($website['country_code']);?>"></span><?php echo $website['iso2']?></a></li>
            <?php endforeach; ?>
        </ul>
    </li>
</ul>

<?php endif;?>


