<?php use_helper('pwnVideo') ?>  

<div id="web-conferencing-engage" class="reveal-modal web-conferencing-engage">
    <div class="inner-content">
        <a class="close-reveal-modal close-button">&#215;</a>
        <h2 class="sub-header color teal">Web Conferencing Demo</h2>
        <?php echo embed_html5_video($webConferencingHtml5VideoData)  ?>
        <p class="content color grey">Powwownow Web is a free desktop sharing tool for you to use in conjunction with Powwownow's telephone service.
            Now everyone can see what you're talking about. Download your Web user guide <a href="http://www.powwownow.co.uk/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" class="more" target="_blank">here.</a></p>
    </div>
</div>