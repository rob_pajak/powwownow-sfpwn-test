
<?php
// Lets set some defaults to stop any warning errors.
$formId = isset($formId) ? $formId : rand();
$position = isset($position) ? $position : 'top';
?>


<div id="<?php echo $formId; ?>-container" class="<?php echo $formId; ?>-container clearfix">
    <script type="text/html" id="<?php echo $formId; ?>-template">
    <?php echo form_tag(url_for('basicRegistratonAjaxG'), array('id' => $formId, 'class' => $formId)); ?>

        <div class="form-field">
            <input id="<?php echo $formId ?>-email" class="email-address <?php echo $inputFieldClasses ?> radius input-text" type="text" name="email" placeholder="Your email address*" autocomplete="off" data-trigger="change" data-type="email" data-required="true" data-error-message="Please enter a valid email address." />
        </div>
        
        <?php if ($position === 'top'): ?>
        
            <div class="form-action">
                <input id="<?php echo $formId ?>-button" type="submit" class="<?php echo $buttonFieldClasses;?> yellow button radius top get-my-pin" value="Get My Pin" />
            </div>
        
        <?php endif;?>
        
        <div class="form-field tandc">
            <input id="<?php echo $formId ?>-tandc" type="checkbox" name="<?php echo $formId?>-tandc" class="<?php echo $formId?>-tandc" data-trigger="change" data-required="true" data-error-message="Please accept our terms & conditions." data-error-container="div.<?php echo $formId ?>-error-info" /><span>I agree to the <a href="<?php echo url_for('@terms_and_conditions'); ?>" target="_blank" class="more">terms & conditions</a></span>
            <div class="<?php echo $formId ?>-error-info"></div>
        </div>
        
        <?php if ($position !== 'top'): ?>
        
            <div class="form-action">
                <input id="<?php echo $formId ?>-button" type="submit" class="<?php echo $buttonFieldClasses;?> yellow button radius get-my-pin" value="Get My Pin" />
            </div>
        
        <?php endif;?>
        
        <div class="form-field">
             <div class="throbber loading green hide"></div>
        </div>
        
        
    </form>

    <% if ( typeof message !== 'undefined') { %>
        <div class="alert-box error">
            <%= message %>
        </div>
    <% } %>
    </script>
</div>

