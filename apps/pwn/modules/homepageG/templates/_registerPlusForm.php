
<?php $form = isset($form) ? $form : new stdClass; ?>

<?php if ($form instanceof PlusRegisterForm) :?>
    
    <?php $formId = isset($formId) ? $formId : 'default-form-'.rand(1000,9999); ?>

    <div id="<?php echo $formId; ?>-container" class="<?php echo $formId; ?>-container">
        <script type="text/html" id="<?php echo $formId; ?>-template">
            <?php echo form_tag(url_for('plus_signup_first_step'), array('id' => $formId .'-form', 'class' => $formId.'-form clearfix')); ?>

                <?php if (isset($user['email']) && $user['email'] !== '') :?>
                    <div class="form-field email hide">
                        <?php echo $form['email']->render(array('value'=> $user['email'])); ?>
                    </div>

                    <div class="form-field email">
                        <input type="email_disabled" value="<?php echo $user['email']?>"  class= 'input-text medium radius height' disabled  />
                    </div>

                <?php else : ?>
                    <div class="form-field email">
                        <?php echo $form['email']->render(array('value'=>  '','class' => 'input-text medium radius height', 'autocomplete' => 'off','placeholder' => 'Your email address*', 'maxlength' => 50,'data-required'=>"true", 'data-type'=>'email','data-error-message'=>'Please enter a valid email address.')); ?>
                    </div>
                <?php endif;?>

                <div class="form-actions">
                    <input id="<?php echo $formId ?>-button" type="submit" class="yellow button radius get-my-pin" value="Continue" />
                </div>

                <div class="form-field tandc">
                    <input id="<?php echo $formId ?>-tandc" type="checkbox" name="terms_and_conditions" class="terms-conditions" value="1" data-required="true" data-error-message="Please accept our terms & conditions." data-error-container="div.<?php echo $formId ?>-error-info"/><span class="terms-conditions">I agree to the <a href="<?php echo url_for('@terms_and_conditions'); ?>" class="more" target="_blank">terms & conditions</a></span>
                    <div class="<?php echo $formId ?>-error-info"></div>
                </div>

                <div class="form-field">
                    <div class="throbber loading green hide"></div>
                </div>

            </form>
        </script>
    </div>

<?php else :?>
    
    <?php $username = isset($user['firstname']) ? $user['firstname'] : ''?>

    <div class="already-powwownow">
        <p>You are already a Powwownow customer!</p>

        <p>Hi <?php echo $username;?></p>

        <p>We are unable to switch you to Plus as your email address is already registered for either our Plus or Premium service.</p>

        <p>For further assistance please contact our Customer Services on 0203 398 0398.</p>
    </div>

<?php endif; ?>
