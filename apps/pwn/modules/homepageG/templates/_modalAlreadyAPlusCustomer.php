<div id="modal-alreadyAPlusCustomers" class="reveal-modal modal-alreadyAPlusCustomers">
    <div class="inner-content">
        <h3 class="sub-header color teal">You are already a Powwownow customer! </h3>
        <p class="color grey">Hi</p>
        <p class="color grey">We are unable to switch you to Plus as your email address is already registered for either our Plus or Premium service.</p>
        <p class="color grey">For further assistance please contact our Customer Services on 0203 398 0398.</p>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</div>