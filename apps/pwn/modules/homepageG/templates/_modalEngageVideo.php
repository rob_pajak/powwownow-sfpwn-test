<?php use_helper('pwnVideo') ?>  

<div id="modal-engage" class="reveal-modal modal-engage">
    <div class="inner-content">
        <a class="close-reveal-modal">&#215;</a>
        <?php echo embed_html5_video($engageHtml5VideoData)  ?>
        <p class="color grey content">
            Powwownow Engage offers you HD video conferencing, phone and VoIP calls, presence, instant messaging and more, all through one single intuitive interface
        </p>


    </div>
</div>