<div id="top-panel" class="container">
        <div class="row">
            <div class="three columns">
                <div class="reminder">
                    <h3>PIN Reminder</h3>
                    <ul>
                        <li><a class="more" href="<?php echo url_for('homepage_g_pin_reminder')?>">Click here for a PIN reminder</a></li>
                    </ul>
                </div>
                <div class="conference-tools">
                    <h3>Conference Tools</h3>
                    <ul>
                        <li><a class="more" href="<?php echo url_for('homepage_g_dial_in_numbers')?>">Dial-in numbers</a></li>
                        <li><a class="more" href="<?php echo url_for('homepage_g_conference_controls')?>">In-conference controls</a></li>
                        <li><a class="more" href="<?php echo url_for('homepage_g_schedule_a_conference_call')?>">Schedule a call</a></li>
                        <li><a class="more" href="<?php echo url_for('homepage_g_pin_mobile_app')?>">Mobile app</a></li>
                    </ul>
                </div>
                <div class="dial-in-numbers">
                    <h3>Your UK dial-in number</h3>
                    <ul>
                        <li><?php echo $defaultDialInNumber; ?></li>
                    </ul>
                </div>
            </div>
            
            <div class="three columns no-margin-right">
                <div class="need-support">
                    <h3>Need Support?</h3>
                    <ul>
                        <li><a class="more" href="https://twitter.com/Powwownow_help" target="_blank">Send us a tweet</a></li>
                        <li><a class="more" href="<?php echo url_for('@contact_us'); ?>">Email us</a></li>
                        <li><a class="more chatlink">Live chat</a></li>
                        <li><a class="call-us ">Call us on: 0203 398 0398</a></li>
                    </ul>
                </div>
                <div class="follow-us">
                    <h3>Follow us!</h3>
                    <ul class="social-links clearfix">
                        <li><a href="https://twitter.com/Powwownow" target="_blank" title="Twitter" class="sprite twitter">Twitter</a></li>
                        <li><a href="http://www.facebook.com/powwownow"  target="_blank" title="Facebook" class="sprite facebook">Facebook</a></li>
                        <li><a href="http://www.linkedin.com/company/powwownow" target="_blank" title="Linkedin" class="sprite linkedin">Linkdn</a></li>
                        <li><a href="http://www.youtube.com/user/MyPowwownow" target="_blank" title="Youtube" class="sprite youtube">Youtube</a></li>
                        <li><a href="https://plus.google.com/102480333405705865805/posts" target="_blank" title="Google+" class="sprite google">Google+</a></li>
                    </ul>
                </div>
                <div class="mobile-short-code">
                    <h3>UK Mobile Shortcode</h3>
                    <ul>
                        <li>87373 <br><span class="shortcode-price">(12.5p a min + VAT)</span></li>
                    </ul>
                </div>
                
            </div>
            
            <div class="six columns no-margin-left">
                <div id="top-panel-login" class="row">
                    
                    <div class="six columns">
                        <?php if (isset($user)) :?>
                            <h3 class="top-panel-header">As a myPowwownow customer don’t forget you have all these great features at your finger tips:</h3>
                        <?php else: ?>
                            <h3 class="top-panel-header">When logged into myPowwownow you can:</h3>
                        <?php endif; ?>
                        <ul class="what-you-get">
                            <li>Change on hold music</li>
                            <li>Download free web conferencing</li>
                            <li>Access call recordings</li>
                            <li>Request welcome packs</li>
                            <li>Manage call settings</li>
                            <li>Use scheduler tool</li>
                        </ul>
                    </div>
                    <div id="login-form-container" class="six columns login-form-container">
                        <?php if (isset($user)):?>
                            <h3 class="top-panel-header right-header">Hello <?php echo $user['firstname']?> You are currently logged in</h3>
                            <div class="actions mypowwownow">
                                <a href="/myPwn" class="small yellow button radius"> enter myPowwownow</a>
                            </div>
                            <div class="actions logout">
                                <a href="<?php echo url_for('homepage_g_logout');?>" class="more">Logout</a>
                            </div>
                            <div class="advert rounded-corners">
                                <h4 class="hero-element">Powwownow Web Conferencing</h4>
                                <p class="sub-content">
                                Bring your conference calls to life with our <span class="color green uppercase">free</span>
                                desktop sharing tool.
                                </p>
                                <div class="set-a-small-large-pc"></div>
                                <a href="<?php echo url_for('@web_conferencing'); ?>" class="small yellow button radius">Find out more</a>
                            </div>
                        <?php else:?>
                            <?php include_component('homepageG','userLoginForm',array('id'=>'top-panel-login-form','class' => 'small',)) ?>
                        <?php endif;?>
                        
                    </div>
                    <?php if (!isset($user)):?>
                        <div class="eleven columns get-account">
                            <p>Not a Powwownow user? <a id="generate-pin-and-account" <?php echo (isset($getPinUrl) ? "href=$getPinUrl" : '');?> class="more">Generate a PIN and create an account</a></p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            
            
        </div>
    
</div>


<div id="top-panel-menu" class="container">
    <div class="row">
        
        <div class="ten columns">
        <?php $bd = sfContext::getInstance()->getUser()->getAttribute('browser_detection', array('data' => array('pwn_support_level' => 0))); ?>
        <?php $pwn_support_level = $bd['data']['pwn_support_level']; ?>
        <?php if ( $pwn_support_level != 20 ) :?>
            <?php if (sfContext::getInstance()->getRequest()->getCookie('non_supported_closed', false) != true): ?>
                <?php include_partial('homepageG/notSupportedBrowserOs', array('pwn_support_level' => $pwn_support_level)); ?>
            <?php endif;?>
        <?php endif;?>
        </div>
        <div class="two columns">
            <ul id="top-panel-login-menu-btn" class="top-panel-login-menu-btn">
                <?php if (isset($user)):?>
                    <li><a class="icon user"><span class="top-panel-btn welcome-back text-center"><?php echo $panelButtonText; ?></span></a></li>
                <?php else:?>
                    <li><a class="icon user"><span class="top-panel-btn existing-customer text-left">Existing Customers</span></a></li>
                <?php endif;?>
            </ul>
        </div>
    </div>
</div>
