
<div id="<?php echo $formId; ?>-container" class="<?php echo $formId; ?>-container">
    <script type="text/html" id="<?php echo $formId; ?>-template">
        
        <% if (_.isEmpty(status)) { %>
        <?php echo form_tag(url_for('homepage_g_update_password_ajax'), array('id' => $formId, 'class' => $formId . ' clearfix')); ?>
            <div class="form-field-container clearfix">
                <div class="form-field">
                    <label>Email*</label>
                    <?php echo $form['email']->render(array('class' => 'input-text medium radius height-small','maxlength' => 50,'data-required'=>"true", 'data-type'=>'email','data-trigger'=>'change', 'data-error-message'=>'Please enter a valid email address.','autocomplete' => 'off')); ?>
                </div>
                <div class="form-field">
                    <label>Current Password*</label>
                    <?php echo $form['existing_password']->render(array('class' => 'input-text medium radius height-small','maxlength' => 20,'data-required'=>'true', 'data-trigger'=>'change', 'data-error-message'=>'Please enter current password.', 'autocomplete' => 'off' )); ?>
                </div>
            </div>
            <div class="form-field-container clearfix">
                <div class="form-field">
                    <label>New Password*</label>
                    <?php echo $form['password']->render(array('id'=> 'password-form-field-input','class' => 'input-text medium radius height-small','maxlength' => 20,'data-required'=>"true",'data-trigger'=>'change', 'data-error-message'=>'Please enter new password.','autocomplete' => 'off')); ?>
                </div>
                <div class="form-field">
                    <label>Re-type Password*</label>
                    <?php echo $form['confirm_password']->render(array('class' => 'input-text medium radius height-small','maxlength' => 20,'data-required'=>"true",'data-trigger'=>'change', 'data-error-message'=>'Please re-type new password.','autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="form-legend clearifx">
                <p class="text-align-left">* Required</p>
            </div>
            <div class="form-action clearfix">
                <input type="submit" class="medium yellow button radius right" value="Update Password">
            </div>
            <div class="form-field">
                <div class="throbber loading green hide"></div>
            </div>
        </form>
        
       <% } else { %>
            <div class="password-reset-success">
                <p>Your password has successfully been updated!</p>
            </div>
       <% }%>
        
    </script>
</div>
