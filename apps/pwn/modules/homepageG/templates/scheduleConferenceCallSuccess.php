<?php include_partial('homepageG/topPagePartial');?>

<div id="schedule-conference" class="schedule-conference container">
    <div class="row">
        
        
        <div class="twelve columns">
            
                <div class="inner-content">
                    
                    <h1 class="landing-main-title">Schedule a Conference Call</h1>
                    <p class="sub-header-content">Powwownow is all about making things easy for you. Use either the Plugin for Outlook or our handy scheduler tool to send participants your conference call details.</p>
                    
                    <span class="absolute disc general-disk-large"></span>
                </div>
            
                <div class="inner-content alt-background">
                    <h3 class="absolute headers-bg-style-9">Scheduling Options</h3>
                    <div class="row">
                        <div class="six columns">
                            <div class="inner-content sub-container">
                                <h3 class="title">Using Microsoft Outlook</h3>
                                <p>If you use Microsoft&reg; Outlook&copy;, download the plugin to schedule 
                                conferences using your Outlook calendar and contacts (PC only).</p>
                                <?php include_component('homepageG', 'registerOutlookPlugin', array('formId' => 'download-outlook-plugin')); ?>
                            </div>
                        </div>

                        <div class="six columns">
                            <div class="inner-content no-background sub-container">
                                <h3 class="title">Not Using Microsoft Outlook</h3>
                                <p>If you use an alternative email client, please use the Powwownow scheduler tool.</p>
                                <div class="button-wrapper">
                                    <a href="http://myscheduler.powwownow.com/" target="_blank" onclick="_gaq.push(['_trackEvent', 'link', 'scheduler', 'G-schedule']);" class="small yellow button radius center">Powwownow Scheduler</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="separator"></div>
        </div>
        
        
        
    </div>
</div>


<?php include_partial('homepageG/footerPartial');?>

