<?php
/****
 *  @param String $id Form ID
 *  @param String $class classes to apply to input 
 *  hello world
 */
?>
<script type="text/html" id="<?php echo $id; ?>-template">
    <h3 class="top-panel-header right-header">myPowwownow Login</h3>
    <form id="<?php echo $id; ?>" method="post" action="/ajax/mypwn/login" data-focus="none">
        
        <div class="form-field">
            <input type="text" name="loginEmail" class="input-text <?php echo $class;?> radius" placeholder="Your email address" autocomplete="off" data-type="email" data-required="true" data-trigger="change" data-error-message="Please enter a valid email address." />
        </div>
        <div class="form-field">
            <input type="password" name="loginPassword" class="input-text <?php echo $class;?> radius" placeholder="Your password" autocomplete="off" data-trigger="change" data-required="true" data-minlength="6" data-error-message="Please enter a password." />
        </div>
        <a  class="more" href="<?php echo url_for('@forgotten_password'); ?>">Forgot your password</a>
        
        <div class="form-action clearfix">
            <input type="submit" class="small yellow button radius right" value="Login" />
        </div>
        
        <div class="form-field">
             <div class="throbber loading green hide"></div>
        </div>
        
    </form>
    <div class="alert-box error">
    <% if ( typeof message !== 'undefined') { %>
            <small><%= message %></small>
    <% } %>
    </div>
</script>

