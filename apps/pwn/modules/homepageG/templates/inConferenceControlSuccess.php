<?php include_partial('homepageG/topPagePartial');?>

<?php include_component('homepageG', 'shareThis') ?>

<!-- Start of content -->
<div id="in-conference-controls" class="container in-conference-controls">
    <div class="row">
        <div class="twelve columns">
            <div class="inner-content">
                <h1 class="landing-main-title headers-bg-style-3">In-conference Controls</h1>
                <h3 class="sub-header color teal">Keep in control during your conference call</h3>
                <p class="sub-header-content">During a conference call the following control keys are available.</p>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="nine columns">
            <div class="inner-content no-background">
                <ul class="conference-controls-guide">
                    <li><span class="ctrl">#</span><span class="short-desc">Skip Intro</span><span class="desc">Skips name recording and PIN playback when joining the call</span></li>
                    <li><span class="ctrl">#6</span><span class="short-desc">Mute</span><span class="desc">Mutes and unmutes your handset</span></li>
                    <li><span class="ctrl">#1</span><span class="short-desc">Head Count</span><span class="desc">Reviews the number of people on the call</span></li>
                    <li><span class="ctrl">#2</span><span class="short-desc">Roll Call</span><span class="desc">Replays the names recorded when the callers arrived on the conference</span></li>
                    <li><span class="ctrl">#3</span><span class="short-desc">Lock</span><span class="desc">Prevents anyone else joining the call</span></li>
                    <li><span class="ctrl">#8</span><span class="short-desc">Record</span><span class="desc">This allows you to record a conference</span></li>
                </ul>
            </div>
        </div>
        <div class="three columns">
            <div class="inner-content no-background">
                <h3>Download our user guides</h3>
                <ul class="conference-download-list">
                    <li><span class="sprite pdf"> </span><a href="/sfpdf/en/Powwownow-User-Guide.pdf" target="_blank" class="more">Conference Calling</a></li>
                    <li><span class="sprite pdf"> </span><a href="/sfpdf/en/Powwownow-Plus-User-Guide-For-Users.pdf" target="_blank" class="more">Powwownow Plus</a></li>
                    <li><span class="sprite pdf"> </span><a href="/sfpdf/en/Powwownow-Premium-User-Guide-For-Users.pdf" target="_blank" class="more">Powwownow Premium</a></li>
                    <li><span class="sprite pdf"> </span><a href="/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" target="_blank" class="more">Web Conferencing</a></li>
                </ul>
            </div>
        </div>
    </div>
    
</div>
<!-- End of content -->

<?php include_partial('homepageG/footerPartial');?>
