<div id="modal-PleaseLogin" class="reveal-modal">
    <div class="inner-content">
        In order to see your PIN(s) please log in to <a href="/Login">myPowwownow</a>. If you have forgotten your myPowwownow password, you can request a reminder <a href="<?php echo url_for('@forgotten_password'); ?>">click here</a>.
    </div>
    <a class="close-reveal-modal close-button">&#215;</a>
</div>