<?php 
    
    $items = array();

    if (isset($plusContent) && $plusContent === true) {
        
        $items = array(
            'item1' => array(
                'header' => 'No Fees',
                'content' => 'You only pay for what you need, nothing more!'
                ),
            'item2' => array(
                'header' => 'No Commitment',
                'content' => 'No monthly commitment, just use when you want and how often.'
            ),
            'item3' => array(
                'header' => 'Instant Setup',
                'content' => 'Simply register for your set of PINs and you can be talking in seconds.'
            ),
            'item4' => array(
                'header' => 'Superior Quality',
                'content' => 'Our conference calls use the exact same fibre optic cabling as your landline.'
            )  
        );
        
    } elseif (isset($engageContent) && $engageContent === true) { 
        
        
        $items = array(
            'item1' => array(
                'header' => 'Improved Productivity',
                'content' => 'Real-time communication increases productivity and reduces company overheads.'
            ),
            'item2' => array(
                'header' => 'Faster Decision Making',
                'content' => 'Share conversations, ideas, thoughts and make decisions instantly.'
            ),
            'item3' => array(
                'header' => 'Superior Quality',
                'content' => 'HD video eliminates delays, distortion and dropped calls for a natural meeting experience.'
            ),
            'item4' => array(
                'header' => 'Cost Effective',
                'content' => 'No up-front investment, all you pay is a monthly subscription.'
            ),
        );
    }elseif (isset($videoConferenceContent) && $videoConferenceContent === true) {
        
        $items = array(
            'item1' => array(
                'header' => 'Cost Effective',
                'content' => 'Simple to implement with no additional hardware required, all you pay is a monthly subscription.'
                ),
            'item2' => array(
                'header' => 'Security',
                'content' => 'Guest links and PIN codes make it easy to conduct secure meetings.'
            ),
            'item3' => array(
                'header' => 'Multi-device',
                'content' => 'Compatible with iOS or Android-based smart phones and tablet devices.'
            ),
            'item4' => array(
                'header' => 'Superior Quality',
                'content' => 'HD video eliminates delays, distortion and dropped calls for a natural meeting experience.'
            )
        );
       
    }elseif(isset($premiumContent) && $premiumContent === true){
        
        $items = array(
            'item1' => array(
                'header' => 'Tailor-made Solutions',
                'content' => 'A service that is tailored to meet your specific needs.'
                ),
            'item2' => array(
                'header' => 'Unbeatable Pricing',
                'content' => 'The best price available for the service you require. You only pay for what you use.'
            ),
            'item3' => array(
                'header' => 'Superior Quality',
                'content' => 'Our conference calls use the exact same fibre optic cabling as your landline.'
            ),
            'item4' => array(
                'header' => 'Unbeatable Extras',
                'content' => 'Added features to enhance your conferencing experience.'
            )  
        );
    } elseif(isset($eventsCall) && $eventsCall === true) {
        $items = array(
            'item1' => array(
                'header' => 'Easy Accessibility',
                'content' => 'Have all caller lines answered by an operator or enable pin-code access.'
            ),
            'item2' => array(
                'header' => 'Global Reach',
                'content' => 'Participants from any country can access the call with over 100 Local and Freephone numbers.'
            ),
            'item3' => array(
                'header' => 'Increased Productivity',
                'content' => 'Enable faster decision making by reaching a wide audience and gaining instant feedback.'
            ),
            'item4' => array(
                'header' => 'High-level support',
                'content' => 'With the planning, support and guidance of the Event Management Team.'
            )
        );
    } elseif (isset($webConference) && $webConference === true) {
        $items = array(
            'item1' => array(
                'header' => 'No Fees',
                'content' => 'Powwownow web conferencing is free to use. Get started today.'
            ),
            'item2' => array(
                'header' => 'No Commitment',
                'content' => 'No monthly commitment, you can use Powwownow on demand whenever you choose.'
            ),
            'item3' => array(
                'header' => 'Easy to Use',
                'content' => 'So simple to use, it is almost embarrassing.'
            ),
            'item4' => array(
                'header' => 'No Participant Installation',
                'content' => 'Your viewers don’t need to download or install anything. They just need a web browser and internet connection.'
            )
        );

    } else {
        
        $items = array(
            'item1' => array(
                'header' => 'No Fees',
                'content' => 'You only pay the cost of your own 0844 phone call, nothing more!'
                ),
            'item2' => array(
                'header' => 'No Commitment',
                'content' => 'No monthly commitment, you can use Powwownow on demand whenever you choose.'
            ),
            'item3' => array(
                'header' => 'Instant Setup',
                'content' => 'Simply create your PIN above and you can be talking in seconds!'
            ),
            'item4' => array(
                'header' => 'Superior Quality',
                'content' => 'Our conference calls use the exact same fibre optic cabling as your landline, unlike other conference call providers.'
            )  
        );

    }
?>
      
<div id="why-pwn" class="container why-pwn">
    <div class="row">
        
        <?php foreach($items as $item):?>
            
            <div class="three columns">
                <h3><?php echo $item['header']?></h3>
                <p><?php echo $item['content']?></p>
            </div>
        
        <?php endforeach; ?>
        
        
    </div>
</div>
