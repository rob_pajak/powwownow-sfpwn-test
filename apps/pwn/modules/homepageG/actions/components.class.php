<?php

class homepageGComponents extends sfComponents {
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executePwnLogo(sfWebRequest $request){}
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeCookiePolicy(sfWebRequest $request) {}
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeTopPanel(sfWebRequest $request) {
        
        $user = $this->getUser();

        if ($user->isAuthenticated()) {


            $this->user = array(
                'firstname' => $user->getAttribute('first_name', ''),
                'email' => $user->getAttribute('email', ''),
            );
        }
        
        $currentRoute = $this->getContext()->getRouting()->getCurrentInternalUri(true);
        
        // ID of registration form on homepage
        if ($currentRoute !== '@homepage') {
            $this->getPinUrl = '/#feature-panel';
        }
        
        $this->panelButtonText = 'Welcome Back';
        
        if ($this->getContext()->getActionName() === 'nextSteps') {
            $this->panelButtonText = 'Welcome';
        }
        
        $this->defaultDialInNumber = Common::getDefaultDialInNumber(
            801,
            (isset($_SERVER['locale'])) ? $_SERVER['locale'] : "en_GB"
        );

    }
    
    public function executeUserLoginForm(sfWebRequest $request) {
        
        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/homepageG/js/components/userLoginForm.js');
    }
    
    public function executeGetMyPin(sfWebRequest $request) {
        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/homepageG/js/components/getMyPin.js');
    }
    
    public function executeMainMenu(sfWebRequest $request) {}
    
    public function executeInternational(sfWebRequest $request) {
        
        $this->websites = array();
        
        try {
            $this->websites = Hermes_Client_Rest::call('Default.getWebsites', array(
                'locale' => 'en_GB'
            ));
        } catch(Exception $e) {
            $this->logMessage('Exception for getDialInNumbers: ' . $e->getMessage(), 'err');
        }
    }
    
    public function executeCarousel(sfWebRequest $request) {
        $this->getResponse()->addJavascript('/shared/foundation2/javascripts/jquery.easing.1.3.js');
        $this->getResponse()->addJavascript('/shared/foundation2/javascripts/jquery.contentcarousel.js');
    }
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeFooterSocialLinks(sfWebRequest $request) {
        $this->getResponse()->addJavascript('/homepageG/js/components/footerSocialLinks.js');
    }
    

    /**
     * Shows users pin and dial in number
     */
    public function executeGeneratedPin(sfWebRequest $request) {

        // Gets a single dial in number based on the service ref and locale
        $this->defaultDialInNumber = Common::getDefaultDialInNumber(
            $this->getUser()->getAttribute('service_ref', 801),
            $this->getUser()->getCulture()
        );

        // Get users pin
        $pins = PinHelper::getDefaultPins($this->getUser()->getAttribute('contact_ref'));
        $this->pin = $pins['pin'];

    }
    
    
    
    public function executeWhosOn(sfWebRequest $request) {
        
        $this->getResponse()->addJavascript('/homepageG/js/components/whosOn.js');
    }
    
    /**
     * Adds ShareThis widget to any page.
     * This has been turned off for now and will be deprecated in favour for 
     * a bespoke solution. Keep tuned.
     * @author Dav
     * @param sfWebRequest $request
     */
    public function executeShareThis(sfWebRequest $request) {
        
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/shareThis.css');
//        $this->getResponse()->addJavascript('http://w.sharethis.com/button/buttons.js');
//        $this->getResponse()->addJavascript('/homepageG/js/components/shareThis.js');
//        
//        $currentRoute = $this->getContext()->getRouting()->getCurrentInternalUri(true);
//         
//         switch($currentRoute) {
//             case '@homepage_g_conference_controls':
//                 $this->facebookMessage = 'Conference call with Powwownow? Here are some handy in-conference controls to help keep on top of your call';
//                 $this->twitterMessage = 'Conference call with @Powwownow? Here are some handy in-conference controls to help keep on top of your call';
//                 $this->googleMessage = 'Conference call with Powwownow? Here are some handy in-conference controls to help keep on top of your call';
//                 $this->linkedinMessage = 'Conference call with Powwownow? Here are some handy in-conference controls to help keep on top of your call';
//                 break;
//             case '@homepage_g_dial_in_numbers':
//                 $this->facebookMessage = 'Great international dial-in options for more than 15 countries with Powwownow';
//                 $this->twitterMessage = 'Great international dial-in options for more than 15 countries with @Powwownow';
//                 $this->googleMessage = 'Great international dial-in options for more than 15 countries with Powwownow';
//                 $this->linkedinMessage = 'Great international dial-in options for more than 15 countries with Powwownow';
//                 break;
//             case '@homepage_g_pin_mobile_app':
//                 $this->twitterMessage = '';
//                 break;
//             default:
//                 $this->twitterMessage;
//                 break;
//         }
    }
    
    public function executePasswordResetForm(sfWebRequest $request) {

        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/homepageG/js/components/passwordResetForm.js');

        $this->formId = 'password-reset-form';

        $this->form = new updatePasswordForm();

    }
    
    public function executeModalCarousel3EasySteps(sfWebRequest $request) {}
    
    public function executeModalEngageVideo(sfWebRequest $request) {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/modalEngageVideo.css');
        $this->engageHtml5VideoData = array(
            'title'     => 'Powwownow Engage',
            'width'     => '460',
            'height'    => '260',
            'url_mp4'   => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/pwn_engage.mp4',
            'url_webm'  => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/pwn_engage.webm',
            'url_ogv'   => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/pwn_engage.ogv',
            'url_image' => 'http://' . $_SERVER['SERVER_NAME']. '/sfimages/engage-still.jpg'
        );
    }
    
    public function executeModalWebConferencingVideo(sfWebRequest $request) {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/modalWebConferencingVideo.css');
        $this->webConferencingHtml5VideoData = array(
            'title'     => 'Powwownow Web Conferencing',
            'width'     => '460',
            'height'    => '260',
            'url_mp4'   => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/webConferencing/video.mp4',
            'url_webm'  => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/webConferencing/video.webm',
            'url_ogv'   => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/webConferencing/video.ogv',
            'url_image' => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/webConferencing/poster.png'
        );
    }
    
    public function executeModalVideoConferencingVideo(sfWebRequest $request) {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/modalVideoConferencingVideo.css');
        $this->webConferencingHtml5VideoData = array(
            'title'     => 'Powwownow Video Conferencing',
            'width'     => '460',
            'height'    => '260',
            'url_mp4'   => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/videoConferencing/video.mp4',
            'url_webm'  => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/videoConferencing/video.webm',
            'url_ogv'   => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/videoConferencing/video.ogv',
            'url_image' => 'http://' . $_SERVER['SERVER_NAME']. '/sfvideo/videoConferencing/poster.png'
        ); 
    }
    
    
    public function executeModalGetMyPinEmailTaken(sfWebRequest $request) {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/modalGetMyPinEmailTaken.css');
    }

    public function executeModalPlusSwitchRegistrationStepTwo(sfWebRequest $request) {}
    
    public function executeModalAlreadyAPlusCustomer(sfWebRequest $request){}

    public function executeModalPleaseLogin(sfWebRequest $request){}

    
    public function executePlusSwitchRegistrationStepTwoForm(sfWebRequest $request) {
        $this->form = new plusSwitchRegistrationForm();
    }

    public function executeRequestWelcomePackForm(sfWebRequest $request) {

        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/homepageG/js/components/requestWelcomePackForm.js');

        $this->formId = 'request-welcome-pack-form';
        $this->form = new requestWelcomePackBasicForm();

    }

    public function executePinReminderForm(sfWebRequest $request) {

        $this->formId = 'pin-reminder-form';
        $this->form = new pinReminderForm();

        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/homepageG/js/components/pinReminderForm.js');

    }

    public function executeRegisterPlusForm(sfWebRequest $request) {
        
        $user = $this->getUser();
        
        $this->form = false;
        
        $canSwitch = true;
        
        if ($user-> hasAttribute('service_user')) {
            if ($user->getAttribute('service_user') !== 'POWWOWNOW') {
                $canSwitch = false;
            }
        }
        
        if ($canSwitch) {
            
            $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
            $this->getResponse()->addJavascript('/homepageG/js/components/registerPlusForm.js');
            $this->form = new PlusRegisterForm();
        }
        
        $this->user = array(
            'firstname' => $user->getAttribute('first_name', ''),
            'email' => $user->getAttribute('email', ''),
        );
    }
    
    
    public function executeWhyPwn(sfWebRequest $request) {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/whyPwn.css');
    }
    
    public function executePlusDescription(sfWebRequest $request){
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/description.css');
    }
    
    public function executeEngageDescription(sfWebRequest $request) {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/description.css');
    }
    
    public function executeVideoConferencingDescription() {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/description.css');
    }

    public function executeWebConferenceDescription() {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/description.css');
    }

    public function executePremiumDescription() {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/description.css');
    }
    public function executeEventsCallDescription(){
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/description.css');
    }



    public function executeDialInNumbers(sfWebRequest $request) {        


        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');

        $pathInfo = sfContext::getInstance()->getRequest()->getPathInfoArray();

        $this->worldwideNumber = array('title' => 'Worldwide','number'=>'+44 844 4 73 73 73');
        $this->skypeNumber = array('title' => 'SkypeOut','number'=>'+49 1803 001 178');
        $this->mobileShortCode = array('title' => 'UK Mobile Shortcode','number'=>'87373');

        try {

            $this->dialinNumbers = hermesCallWithCaching('Default.getDialInNumbers', array(
                'service_ref' => 801,
                'locale' => ($pathInfo['locale']) ? $pathInfo['locale'] : 'en_GB'
            ),
            'array',
            86400 // TTL for cache (1 day)
            );

        } catch(Exception $e) {

            $this->logMessage('Exception for getDialInNumbers: ' . $e->getMessage(), 'err');
            $this->dialinNumbers = array();

        }

    }
    
    
    public function executeNotSurePwn(sfWebRequest $request){
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/notSurePwn.css');
    }
    
    public function executeWhyChooseUs() {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/whyChooseUs.css');
    }
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeLandingPageDesc(sfWebRequest $request) {
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/landingPageDesc.css');
    }
    
    public function executeWebConferenceSignUpForm() {

        $this->form = new webConferencingSignupForm();

        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/homepageG/js/components/webConferenceSignUpForm.js');
    }

    /**
     * @param sfWebRequest $request
     */
    public function executeRegisterOutlookPlugin(sfWebRequest $request) {
        // Is the User Authenticated
        $this->authenticated = $this->getUser()->isAuthenticated();
    }

    public function executeModalDownloadOutlookPlugin(sfWebRequest $request){
        // Backbone Files
        $this->getResponse()->addStyleSheet('/homepageG/stylesheets/components/modalDownloadOutlookPlugin.css');
        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/homepageG/js/components/downloadOutlookPluginForm.js');
        $this->id = 'download-outlook-plugin-form';
        $this->class = 'input-text medium radius';
    }


    public function executeGoogleAdWords(){}
    
    
}
