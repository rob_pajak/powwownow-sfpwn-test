<?php

/**
 * commonComponents actions.
 *
 * @package    Powwownow
 * @subpackage commonComponents
 * @author     Dav
 * @version    1
 */
class homepageGActions extends sfActions
{
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeDialInNumbers(sfWebRequest $request) {}

    /**
     *
     * @param sfWebRequest $request
     */
    public function executeDialInNumbersRedirect(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@international_number_rates'), 301);
    }
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeInConferenceControl(sfWebRequest $request) {}

    /**
     *
     * @param sfWebRequest $request
     */
    public function executeInConferenceControlRedirect(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@homepage'), 301);
    }

    /**
     * 
     * @param sfWebRequest $request
     */
    public function executePinReminder(sfWebRequest $request) {

    }

    /**
     *
     * @param sfWebRequest $request
     */
    public function executePinReminderRedirect(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@homepage'), 301);
    }

    /**
     * Validates and saves pin reminder form
     * 
     * @param sfWebRequest $request
     * @return type
     */
    public function executePinReminderAjax(sfWebRequest $request) {
        
        controllerHelper::jsonFormResponse($request, $this);

        $this->pinReminderForm = new pinReminderForm();

        $this->pinReminderForm->bind($request->getParameter($this->pinReminderForm->getName()));

        if (!$this->pinReminderForm->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $this->pinReminderForm->pwn_getErrors()));
        }

        try {
            $pathInfo = $request->getPathInfoArray();

            preg_match('/(http|https):\/\/.+?(\/.*)/', $request->getReferer(), $refRequestMatch);
            $refRequest = isset($refRequestMatch[2]) ? $refRequestMatch[2] : '';

            $this->pinReminderForm->save(
                array(
                    'locale' => ($pathInfo['locale']) ? $pathInfo['locale'] : 'en_GB',
                    'request_source' => $refRequest
                )
            );
        } catch (Exception $e) {
            
            $this->logMessage(
                $request->getUri() . ' An exception occurred calling pinReminderForm->save(): ' . serialize($e),
                'warning'
            );
            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $e->getErrorReasons()));
        }

        return $this->renderPartial('commonComponents/formSuccess', array('response' => array()));
    }
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeMobileApp(sfWebRequest $request) {}

    /**
     *
     * @param sfWebRequest $request
     */
    public function executeMobileAppRedirect(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@iphone_conference_app'), 301);
    }
    
    /**
     * @param sfWebRequest $request
     */
    public function executeScheduleConferenceCall(sfWebRequest $request) {}

    /**
     * @param sfWebRequest $request
     */
    public function executeScheduleConferenceCallRedirect(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@homepage'), 301);
    }

    /**
     * @param sfWebRequest $request
     */
    public function executeUpdatePassword(sfWebRequest $request) {}

    /**
     * @param sfWebRequest $request
     */
    public function executeUpdatePasswordRedirect(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@homepage'), 301);
    }
    
    public function executeUpdatePasswordAjax(sfWebRequest $request) {

        controllerHelper::jsonFormResponse($request, $this);

        $this->updatePasswordForm = new updatePasswordForm();

        $this->updatePasswordForm->bind($request->getParameter($this->updatePasswordForm->getName()));

        if (!$this->updatePasswordForm->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $this->updatePasswordForm->pwn_getErrors()));
        }

        try {
            $pathInfo = $request->getPathInfoArray();

            $this->updatePasswordForm->save(
                array('locale' => ($pathInfo['locale']) ? $pathInfo['locale'] : 'en_GB')
            );

            Hermes_Client_Rest::call(
                'DotMailer.sendPasswordChangeFreePin',
                array('email' => $this->updatePasswordForm->getValue('email'))
            );

            
        } catch (Exception $e) {

            $this->logMessage(
                $request->getUri() . ' An exception occurred calling updatePasswordForm->save(): ' . serialize($e),
                'warning'
            );

            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $e->getErrorReasons()));
        }
        
//
//        try {
//            Hermes_Client_Rest::call(
//                'DotMailer.sendPasswordChangeFreePin',
//                array('email' => $this->updatePasswordForm->getValue('email'))
//            );
//        } catch (Exception $e) {
//            $this->logMessage(
//                'DotMailer.sendPasswordChangeFreePin failed',
//                'error'
//            );
//        }
        
        return $this->renderPartial('commonComponents/formSuccess', array('response' => array()));
    }
    
    /**
     * Next steps component, this lets a user order a wallet card
     *
     * @param sfWebRequest $request
     */
    public function executeNextSteps(sfWebRequest $request) {
        $pins = PinHelper::getDefaultPins($this->getUser()->getAttribute('contact_ref'));
        $this->pin = $pins['pin'];
    }

    /**
     * Handles the 'next steps' component - this updates the users name and then orders them a wallet card
     *
     * Possible responses are:
     *     * {"status":"success"}
     *     * @see requestWelcomePackForm
     *     * @see Common::createWalletCardRequest for other responses
     *
     * @param sfWebRequest $request
     * @return type
     */
    public function executeNextStepsAjax(sfWebRequest $request)
    {

        controllerHelper::jsonFormResponse($request, $this);

        $defaultPin = PinHelper::getDefaultPins($this->getUser()->getAttribute('contact_ref'));

        // Use the request welcome pack form to do our validation
        $this->welcomePackForm = new requestWelcomePackBasicForm();

        $this->welcomePackForm ->bind($request->getParameter($this->welcomePackForm->getName()));

        if (!$this->welcomePackForm->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $this->welcomePackForm->pwn_getErrors()));
        }

        try {

            $pathInfo = $request->getPathInfoArray();

            $this->welcomePackForm->save(
                $this->getUser()->getAttribute('contact_ref'),
                $defaultPin['pin_ref'],
                ($pathInfo['country']) ? $pathInfo['country'] : 'GBR'
            );

        } catch (Exception $e) {

            $this->logMessage(
                $request->getUri() . ' An exception occurred calling requestWelcomePackBasicForm->save(): ' . serialize($e),
                'warning'
            );

            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $e->getErrorReasons()));
        }

        return $this->renderPartial('commonComponents/formSuccess', array('response' => array()));

    }
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executePlus(sfWebRequest $request) {}
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeEngage(sfWebRequest $request) {}

    /**
     *
     * @param sfWebRequest $request
     */
    public function executeEngageRedirect(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@homepage'), 301);
    }
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executePremium(sfWebRequest $request) {}
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeVideoConferencing(sfWebRequest $request) {}
    
    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeWebConferencing(sfWebRequest $request) {}

    /**
     *
     * @param sfWebRequest $request
     * @return view
     */
    public function executeWebConferencingAjax(sfWebRequest $request) {

        controllerHelper::jsonFormResponse($request, $this);

        $this->form = new webConferencingSignupForm();

        $this->form->bind($request->getParameter($this->form->getName()));

        if (!$this->form->isValid()) {

            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $this->form->pwn_getErrors()));
        }

        try {
            $pathInfo = $request->getPathInfoArray();

            $registrationResponse = $this->form->save(
                ($pathInfo['country']) ? $pathInfo['country'] : 'GBR',
                '/WebConferencing_reg_G'
            );

        } catch (Exception $e) {

            $this->logMessage(
                $request->getUri() . ' An exception occurred calling webConferencingSignupForm->save(): ' . serialize($e),
                'warning'
            );

            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial('commonComponents/formErrors', array('errors' => $e->getErrorReasons()));
        }

        // Log user in to mypowwownow
        try {
            Plus_Authenticate::logIn($this->form->getValue('email'), $this->form->getValue('password'));
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Unable to log user in after web conferencing registration ' . serialize($e));
        }

        return $this->renderPartial('commonComponents/formSuccess', array('response' => array('pin' => $registrationResponse['pin']['pin'])));
    }


    public function executeEvents(sfWebRequest $request){}


    /**
     * 
     * @param sfWebRequest $request
     */
    public function executeLogout(sfWebRequest $request) {
        
        try {
            Plus_Authenticate::logOut(); 
        } catch (Exception $e) {
            $this->logMessage('An Exception occurred trying to log a user in: ' . $e->getMessage(), 'err');
        }
        
        $this->redirect('/', 200);
    }
    
}
