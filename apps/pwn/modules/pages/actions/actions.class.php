<?php
/**
 * pages actions.
 *
 * @package    powwownow
 * @subpackage pages
 * @author     Vitaly
 * @author     Asfer Tamimi
 *
 */
class pagesActions extends sfActions
{
    /**
     * Web Conferencing [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeWebConferencing()
    {
        // Load Helpers
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        $webConferencingVideo = get_component(
            'commonComponents',
            'hTML5OutputVideo',
            array(
                'config' => array(
                    'width'    => 460,
                    'height'   => 267,
                    'autoplay' => false,
                    'controls' => true
                ),
                'video'  => array(
                    'image' => '/sfvideo/webConferencing/poster.png',
                    'mp4'   => '/sfvideo/webConferencing/video.mp4',
                    'webm'  => '/sfvideo/webConferencing/video.webm',
                    'ogg'   => '/sfvideo/webConferencing/video.ogv'
                ),
                'called' => 'script'
            )
        );

        $this->setVar('webConferencingVideo', str_replace(array("'", "\n"), array("\'", " "), $webConferencingVideo));
        $this->setVar('form', new webConferencingGetStartedForm());
        $this->setVar('fields', array(
            'first_name',
            'last_name',
            'company',
            'email',
            'confirm_email',
            'password',
            'confirm_password'
        ));
    }

    /**
     * Careers [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeCareers()
    {
    }

    /**
     * About-Us [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeAboutUs()
    {
    }

    /**
     * Premium-Service [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executePremiumService()
    {
    }

    /**
     * International-Number-Rates [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeInternationalNumberRates(sfWebRequest $request)
    {
        $lang          = substr($this->getUser()->getCulture(), 0, 2);
        $dialInNumbers = DialInNumbersHelper::getAllSharedCostDialInNumbers($lang);
        if (count($dialInNumbers) > 0) {
            $dialInNumbers = DialInNumbersHelper::cleanGetAllSharedCostDialInNumbers($dialInNumbers);
        }

        // Data Table Settings
        $this->dataTableOptions = array(
            'data'          => $dialInNumbers,
            'tableID'       => 'tableInternationalNumberRates',
            'sortOrder'     => 'false',
            'autoWidth'     => false,
            'tableEmpty'    => 'You currently do not have any International dial in numbers.',
            'displayLength' => -1,
            'tblColour'     => ''
        );

        $this->dataTable = array(
            array('title' => 'COUNTRY', 'map' => 'country_name_language'),
            array('title' => 'DIAL-IN NUMBER', 'map' => 'national_formatted'),
            array('title' => 'LANGUAGE', 'map' => 'international_formatted'),
            array('title' => 'COST PER MIN', 'map' => 'cpm_check'),
        );

        $this->defaultShortCode = '87373'; // This number is not in our database
    }

    /**
     * Going-Green [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeGoingGreen()
    {
    }

    /**
     * Business-Efficiency [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeBusinessEfficiency()
    {
    }

    /**
     * FAQs [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeFaqs()
    {
    }

    /**
     * How We Are Different [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeHowWeAreDifferent()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');

        // Table Information
        $tableArr   = array();
        $tableArr[] = array(
            'pwn'   => 'No registration forms to complete*',
            'other' => 'You have to register all your details'
        );
        $tableArr[] = array('pwn' => 'No contract - no ties*', 'other' => 'You have to sign a contract');
        $tableArr[] = array(
            'pwn'   => 'No booking required',
            'other' => 'You have to pre-book the date, time, duration and number of participants'
        );
        $tableArr[] = array(
            'pwn'   => '<a title="What it Costs" href="' . url_for('@costs') . '"><span class="grey">No fees or charges</span></a> from us. You just pay the cost of your own phone call*',
            'other' => 'Bridge fees or other charges usually payable. And check the cost of your phone call!'
        );
        $tableArr[] = array(
            'pwn'   => 'No invoice, no billing - ever! Charges appear on your phone bill in the same way as any other call.',
            'other' => 'You get a bill every month'
        );
        $tableArr[] = array(
            'pwn'   => 'Talk for as long as you like!',
            'other' => 'Exceed your limit and they might cut you off!'
        );
        $tableArr[] = array(
            'pwn'   => 'Offers a UK mobile shortcode (87373 <span class="shortcode-price">for 12.5p a min + VAT</span>) for cheaper mobile rates',
            'other' => 'Do not offer a UK mobile shortcode'
        );
        $tableArr[] = array(
            'pwn'   => 'Free <a title="Powwownow iPhone app" href="http://itunes.apple.com/us/app/powwownow/id359486614?mt=8" target="_blank"><span class="grey">mobile app</span></a>',
            'other' => 'Haven\'t developed their own apps'
        );
        $tableArr[] = array(
            'pwn'   => 'Free <a title="What it Costs" href="' . url_for('@web_conferencing') . '"><span class="grey">web conferencing</span></a>',
            'other' => 'You will be offered a paid-for product'
        );
        $tableArr[] = array(
            'pwn'   => 'Free call recordings',
            'other' => 'You have to pay for this featureYou will be offered a paid-for product'
        );
        $tableArr[] = array(
            'pwn'   => 'A choice of on-hold music and voice prompt language',
            'other' => 'You only get one set option'
        );
        $tableArr[] = array(
            'pwn'   => 'Our own network - to better monitor, manage and resolve service issues.',
            'other' => '3rd party telco networks are used'
        );
        $tableArr[] = array(
            'pwn'   => 'Handy wallet card',
            'other' => 'Only Powwownow think about the small details!'
        );
        $this->setVar('tableArr', $tableArr, true);
    }

    /**
     * Costs [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeCosts(sfWebRequest $request)
    {
    }

    /**
     * Event Conference Calls [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeEventConferenceCalls(sfWebRequest $request)
    {
    }

    /**
     * Plus Service [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executePlusService()
    {
        /** @var myUser $user */
        $user = $this->getUser();
        $this->setVar('authenticated', $user->isAuthenticated());
        $this->setVar('breadcrumbs', array());
    }

    /**
     * Engage Service [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Vitaly
     * @author Asfer Tamimi
     *
     */
    public function executeEngageService(sfWebRequest $request)
    {
        $this->setVar('authenticated', $this->getUser()->isAuthenticated());
        $this->setVar('videoOptions', array(

        ));
    }

    /**
     * Powwownow Quality [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executePowwownowQuality()
    {
        $this->setVar('statistics', array());
        try {
            $this->setVar('statistics', Hermes_Client_Rest::call('Quality.getSummary', array()));
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->logMessage("Quality.getSummary Hermes call has timed out", 'err');
        } catch (Hermes_Client_Exception $e) {
            $this->logMessage(
                'Quality.getSummary Client Exception Called: ' . $e->getMessage() . ', Code: ' . $e->getCode(),
                'err'
            );
        } catch (Exception $e) {
            $this->logMessage('Quality.getSummary Unknown Error Occurred: ' . $e->getMessage(), 'err');
        }
    }

    /**
     * Recommendations Registration Complete [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Vitaly
     *
     */
    public function executeRegistrationComplete(sfWebRequest $request)
    {
        // Load Helpers
        sfContext::getInstance()->getConfiguration()->loadHelpers(
            array('hermesCallWithCaching', 'dialInNumbers', 'Partial')
        );

        /** @var myUser $user */
        $user = $this->getUser();

        $country   = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";
        $this->setVar('pin', $request->getParameter('pin'));

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers      = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $user->getCulture(),
                'service_ref'    => $user->getAttribute('service_ref'),
                'country'        => $this->country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
        $this->setVar('dialInNumber', $dialInNumbers['local']);
        $this->setVar('mobile', $dialInNumbers['mobile']);

        // PIN
        $user->setAttribute('registered_pin', $this->getVar('pin'));

        // Email Message
        $emailMessage = "parent.location='mailto:?subject=" . rawurlencode('Powwownow Conference Call Details.');
        $emailMessage .= "&body=" . rawurlencode('Please join my conference call.') . "%0A%0A";
        $emailMessage .= rawurlencode(
                'Your dial-in number is: '
            ) . ' ' . $this->dialInNumber . ' (International Dial-in Numbers http://pdf.powwownow.com/pdf/GBR_en_Dial-In-Numbers.pdf) %0A';
        $emailMessage .= rawurlencode(
                'When dialling-in to conference calls using a mobile phone, use our mobile shortcode to help save you money: Dial '
            ) . $this->mobile . rawurlencode(
                ' and enter your conference PIN as usual to join your conference.'
            ) . '%0A';
        $emailMessage .= rawurlencode('Your PIN is: ') . ' ' . $this->getVar('pin') . "%0A%0A";
        $emailMessage .= rawurlencode(
            "Time of call:<br/><br/>Instructions:<br/>1. Call the dial-in number at the time indicated above<br/>2. Enter the PIN, when prompted<br/>3. Speak your full name when prompted and then you will join the conference. If you are the first person to arrive on the conference call, you will be placed on hold until the next person joins<br/>Find out how much easier your life could be with Powwownow's free conference calling service. Powwownow - get together whenever!<br/>http://www.powwownow.co.uk"
        );
        $this->setVar('emailMessage', str_replace(array('%3Cbr%20%2F%3E', '%26nbsp%3B'), array('%0A', '%20'), $emailMessage));
    }

    /**
     * Makes Sense [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Vitaly
     * @author Davinder C
     * @author Asfer Tamimi
     *
     */
    public function executeMakesSense(sfWebRequest $request)
    {
        //Redirect requested by Shahnaz by WR 9794 (Legal issues -> usage rights expiry for Cecil banner)
        $this->redirect("@its_your_call_a");

        // HTML5 video setup, use FQDN
        $this->videoOptions = array(
            'title'      => 'Powwownow Engage',
            'width'      => '460',
            'height'     => '260',
            'url_mp4'    => 'http://' . $_SERVER['SERVER_NAME'] . '/sfvideo/pwn_3steps460x260_LR.mp4',
            'url_webm'   => 'http://' . $_SERVER['SERVER_NAME'] . '/sfvideo/pwn_3steps460x260_LR.webm',
            'url_ogv'    => 'http://' . $_SERVER['SERVER_NAME'] . '/sfvideo/pwn_3steps460x260_LR.ogv',
            'url_image'  => 'http://' . $_SERVER['SERVER_NAME'] . '/sfimages/engage-still.jpg',
            'element_id' => 'powwownow_three_easy'
        );
    }

    /**
     * Testimonials [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executeTestimonials(sfWebRequest $request)
    {
    }

    /**
     * Mobile Phone Conference Call [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executeMobilePhoneConferenceCall(sfWebRequest $request)
    {
    }

    /**
     * Contact-Us [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeContactUs(sfWebRequest $request)
    {
        $this->form   = new contactUsForm();
        $this->fields = array('first_name', 'last_name', 'company', 'contact_number', 'email', 'confirm_email');
    }

    /**
     * Contact-Us [Ajax]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeContactUsAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new contactUsForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments               = Common::getArguments($request->getParameter($form->getName(), array()), array());
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(500);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        // Send Contact Us Email
        $response = Common::sendContactUsEmail(
            array(
                'first_name'     => $arguments['first_name'],
                'last_name'      => $arguments['last_name'],
                'email'          => $arguments['email'],
                'contact_number' => $arguments['contact_number'],
                'company'        => isset($arguments['company']) ? $arguments['company'] : null,
                'comments'       => $arguments['comments'],
            ),
            $_SERVER['SERVER_NAME']
        );

        // Check if there was an Error from the above Hermes Call
        if (isset($response['error'])) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        // Success Response
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
        echo json_encode(array('success_message' => get_partial('pages/contactusSuccessMessage', array())));
        return sfView::NONE;
    }

    /**
     * Im-Interested [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     * @modified Asfer Tamimi
     *
     */
    public function executeImInterested(sfWebRequest $request)
    {
        $this->form         = new callBackForm();
        $this->fields       = array('first_name', 'last_name', 'contact_number', 'email');
        $this->logos        = sfConfig::get('app_logos');
        $this->serviceUser  = $this->getUser()->getAttribute('service_user', 'UNAUTHENTICATED');
        $this->footerLinks  = sfConfig::get('app_footer_links');
        $this->socialLinks  = sfConfig::get('app_social_links');
        $this->utm_campaign = $request->getGetParameter('utm_campaign');
    }

    /**
     * Video Conferencing [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeVideoConferencing()
    {
    }

    /**
     * Audio Conferencing [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeAudioConferencing(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $this->redirect(url_for('@conference_call'), 301);
        //$this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Audio-Conferencing' : 'GBR-Audio-Conferencing';
    }

    /**
     * Better Conferencing [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeBetterConferencing(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $this->redirect(url_for('@conference_call'), 301);
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Better-Conferencing' : 'GBR-Better-Conferencing';
    }

    /**
     * Conference Call [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeConferenceCall(sfWebRequest $request)
    {
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Conference-Call' : 'GBR-Conference-Call';
    }

    /**
     * Conference Call Providers [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeConferenceCallProviders(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $this->redirect(url_for('@conference_call'), 301);
        //$this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Conference-Call-Providers' : 'GBR-Conference-Call-Providers';
    }

    /**
     * Conference Call Services [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeConferenceCallServices(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $this->redirect(url_for('@conference_call'), 301);
        //$this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Conference-Call-Services' : 'GBR-Conference-Call-Services';
    }

    /**
     * Free Conference Call [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeFreeConferenceCall(sfWebRequest $request)
    {
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Free-Conference-Call' : 'GBR-Free-Conference-Call';
    }

    /**
     * International Conference Call [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeInternationalConferenceCall(sfWebRequest $request)
    {
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-International-Conference-Call' : 'GBR-International-Conference-Call';
    }

    /**
     * Iphone Conference App [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeIphoneConferenceApp()
    {
    }

    /**
     * Teleconference [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeTeleconference(sfWebRequest $request)
    {
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Teleconference' : 'GBR-Teleconference';
    }

    /**
     * Teleconferencing Services [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeTeleconferencingServices(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $this->redirect(url_for('@teleconference'), 301);
        //$this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Teleconferencing-Services' : 'GBR-Teleconferencing-Services';
    }

    /**
     * Telephone Conferencing [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeTelephoneConferencing(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $this->redirect(url_for('@teleconference'), 301);
        //$this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Telephone-Conferencing' : 'GBR-Telephone-Conferencing';
    }

    /**
     * Voice Conferencing [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeVoiceConferencing(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $this->redirect(url_for('@conference_call'), 301);
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Voice-Conferencing' : 'GBR-Voice-Conferencing';
    }

    /**
     * Remote Working [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeRemoteWorking(sfWebRequest $request)
    {
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Remote-Working' : 'GBR-Remote-Working';
    }

    /**
     * Love Hate Travel [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeLoveHateTravel(sfWebRequest $request)
    {
        $this->registrationSource = (isset($_SERVER['country'])) ? $_SERVER['country'] . '-Love-Hate-Travel' : 'GBR-Love-Hate-Travel';
    }

    /**
     * This is the Initial Its-Your-Call-A Page [PAGE]
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeItsYourCallA(sfWebRequest $request)
    {
        $this->gtm = true;
    }

    /**
     * This is the Initial Its-Your-Call-A Page [AJAX]
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeItsYourCallAAjax(sfWebRequest $request)
    {
        $this->getResponse()->setHttpHeader('Content-type', 'application/json');

        // Email Validation
        try {
            $v     = new sfValidatorEmail();
            $email = $v->clean($request->getPostParameter('email'));
        } catch (sfValidatorError $e) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS',
                        'field_name' => 'email'
                    )
                )
            );
            return sfView::NONE;
        }

        // Get page variation from session, we send this to hermes upon registration, default to 'Home'
        $variation = $this->getUser()->getAttribute('homePageCroVariation', 'Home', 'cro');

        // Do the Basic Registration
        try {
            $response = Hermes_Client_Rest::call(
                'doBasicRegistration',
                array(
                    'email'  => $email,
                    'locale' => 'en_GB',
                    'source' => '/PWN-' . $variation . '-Reg'
                )
            );

            // Set Email and Contact Ref in Session
            $this->getUser()->setAttribute('email', $email);
            $this->getUser()->setAttribute('contact_ref', $response['contact']['contact_ref']);
            $this->getUser()->setAttribute('pin', $response['pin']['pin']);
            $this->getUser()->setAttribute('pin_ref', $response['pin']['pin_ref']);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'doBasicRegistration Hermes call has timed out. Contact Ref: ' . $this->getUser()->getAttribute(
                    'contract_ref'
                ),
                'err'
            );
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'doBasicRegistration Hermes Client Exception. ErrorCode: ' . $e->getCode(
                ) . ', Message: ' . $e->getMessage(),
                'err'
            );
            switch ($e->getCode()) {
                case '001:000:001':
                    echo json_encode(
                        array(
                            'error_messages' => array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_RESPONSIVE_OTHER',
                                'field_name' => 'email'
                            )
                        )
                    );
                    break;
                case '001:000:007':
                    echo json_encode(
                        array(
                            'error_messages' => array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE1',
                                'field_name' => 'alert'
                            )
                        )
                    );
                    break;
                default:
                    echo json_encode(
                        array(
                            'error_messages' => array(
                                'message'    => 'FORM_COMMUNICATION_ERROR',
                                'field_name' => 'alert'
                            )
                        )
                    );
                    break;
            }
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage('doBasicRegistration Unknown Error Occurred: ' . $e->getMessage(), 'err');
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        // PWN Tracking
        try {
            if (!is_null($this->getUser()->getAttribute('pin_ref'))) {
                $goal = 'conferencing';
                try {
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                    PWN_Logger::log(
                        'pin:' . $this->getUser()->getAttribute(
                            'pin_ref'
                        ) . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                        PWN_Logger::INFO,
                        'trackerLog'
                    );
                } catch (Exception $e) {

                }
                PWN_Tracking_GoalReferrers::achieved('conferencing', $this->getUser()->getAttribute('pin_ref'), true);
            } else {
                $this->logMessage('After a registration, pin ref was null: ' . serialize($response), 'err');
            }
        } catch (Exception $e) {
            $this->logMessage('Could not track goal referrers: ' . serialize($e), 'err');
        }

        echo json_encode(array('status' => 'success'));
        return sfView::NONE;
    }

    /**
     * This is the Second Step in the Its-Your-Call-A Route [PAGE]
     * Its used for inputting the Request Welcome Pack Details
     * @param sfWebRequest $request
     * @return sfView::None
     * @amend
     *  [DEV-1947] Lets Get It Done
     *      This step is NOW shared with the "lets-get-done" landing page.
     *      Marketing want the lozenge around the "card" a different colour for this landing page.
     *      Code Change - We check the registration type and pass a "CSS" class back to the view.
     *      Since the JS is also shared and Marketing wanted different URL we have to pass to the JS
     *      where to redirect.
     */
    public function executeItsYourCallStepA(sfWebRequest $request)
    {
        $email      = $this->getUser()->getAttribute('email', null);
        $contactRef = $this->getUser()->getAttribute('contact_ref', null);

        // Check the Email and the Contact Ref
        if (is_null($email) || is_null($contactRef)) {
            $this->logMessage('Redirecting... Email: ' . $email . ', contact_ref: ' . $contactRef, 'err');
            $this->redirect('its_your_call_a', 301);
        }

        // PIN Information
        $pins      = PinHelper::getDefaultPins($this->getUser()->getAttribute('contact_ref'));
        $this->pin = $pins['pin'];

        $registrationType = $this->getUser()->getAttribute('registration_type');

        switch ($registrationType) {
            case 'gid':
                $this->cssClass = 'gid';
                $this->redirectUrl = '@lets_get_it_done_step_b';
                break;

            default:
                $this->cssClass = 'standard';
                $this->redirectUrl = '@its_your_call_step_b';
        }

        // Request Welcome Pack Form
        $this->form = new requestWelcomePackFormItsYourCall();
    }

    /**
     * This is the Second Step in the Its-Your-Call-A Route [AJAX]
     * Its used for inputting the Request Welcome Pack Details
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeItsYourCallStepAAjax(sfWebRequest $request)
    {
        // Set Output Format
        $this->getResponse()->setContentType('application/json');

        // Request Welcome Pack Form Validation
        $form = new requestWelcomePackFormItsYourCall();
        $form->bind($request->getParameter($form->getName()));
        $arguments               = Common::getArguments(
            $request->getParameter($form->getName()),
            array('email' => $this->getUser()->getAttribute('email'))
        );
        $arguments['country']    = 'GBR'; // Replace the Default 'United Kingdom' Value given by default in the Forms
        $postFormValidationError = $form->doPostValidation($arguments);

        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        // Check the Optional Question
        if (!empty($arguments['question'])) {
            try {
                $response = Hermes_Client_Rest::call(
                    'saveRegistrationAnswer',
                    array(
                        'question_id' => '1',
                        'answer'      => $arguments['question'],
                        'contact_ref' => $this->getUser()->getAttribute('contact_ref')
                    )
                );
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage(
                    'saveRegistrationAnswer Hermes call has timed out. Contact Ref: ' . $this->getUser()->getAttribute(
                        'contract_ref'
                    ),
                    'err'
                );
                echo json_encode(
                    array(
                        'error_messages' => array(
                            array(
                                'message'    => 'FORM_COMMUNICATION_ERROR',
                                'field_name' => 'alert'
                            )
                        )
                    )
                );
                return sfView::NONE;
            } catch (Hermes_Client_Exception $e) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage(
                    'saveRegistrationAnswer Hermes Client Exception. ErrorCode: ' . $e->getCode(
                    ) . ', Message: ' . $e->getMessage(),
                    'err'
                );
                switch ($e->getCode()) {
                    case '001:000:001':
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'API_RESPONSE_EMAIL_TAKEN_AND_NON_STANDARD_PWN_RESPONSIVE',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                    case '001:000:005':
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'API_RESPONSE_ACCOUNT_ALREADY_CREATED_FOR_REQUESTED_DATA',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                    case '001:000:007':
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE1',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                    default:
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'FORM_COMMUNICATION_ERROR',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                }
                return sfView::NONE;
            } catch (Exception $e) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage('saveRegistrationAnswer Unknown Error Occurred: ' . $e->getMessage(), 'err');
                echo json_encode(
                    array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
                );
                return sfView::NONE;
            }
        }

        // Update the Contact to create a Normal Powwownow Account
        if (!empty($arguments['password'])) {
            $result = Common::doCreateOrUpdateAccount(
                array(
                    'email'      => $arguments['email'],
                    'first_name' => $arguments['first_name'],
                    'last_name'  => $arguments['last_name'],
                    'password'   => $arguments['password'],
                    'source'     => '/PWN-LpB1-Login-Created',
                    'locale'     => $this->getUser()->getCulture(),
                )
            );

            // Check if there was an Error from the above Hermes Call
            if (isset($result['error'])) {
                $this->getResponse()->setStatusCode(500);
                if ('email' == $result['error']['field_name']) {
                    $result['error']['field_name'] = 'alert';
                }
                $this->logMessage('doCreateOrUpdateAccount Hermes Error.' . print_r($result['error'], true), 'err');
                echo json_encode(array('error_messages' => $result['error']));
                return sfView::NONE;
            }
        }

        // Check if there is a Wallet Card Address Set (There should not be, since its a new user (Waste of step?)
        // Get the Existing Wallet Card Address + Create a Wallet Card Address
        try {
            $response = Hermes_Client_Rest::call(
                'getWalletCardAddress',
                array(
                    'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                    'pin_ref'     => $this->getUser()->getAttribute('pin_ref'),
                )
            );

            if (!empty($response)) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage('getWalletCardAddress Returned 0 Records', 'err');
                echo json_encode(
                    array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
                );
                return sfView::NONE;
            }
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'getWalletCardAddress Hermes call has timed out. Contact Ref: ' . $this->getUser()->getAttribute(
                    'contract_ref'
                ),
                'err'
            );
            echo json_encode(
                array(
                    'error_messages' => array(
                        array(
                            'message'    => 'FORM_COMMUNICATION_ERROR',
                            'field_name' => 'alert'
                        )
                    )
                )
            );
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            // There are no Exceptions in the actual Hermes Method, so show default Error Message
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'getWalletCardAddress Hermes Client Exception. ErrorCode: ' . $e->getCode(
                ) . ', Message: ' . $e->getMessage(),
                'err'
            );
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage('getWalletCardAddress Unknown Error Occurred: ' . $e->getMessage(), 'err');
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        $response = Common::createWalletCardRequest(
            array(
                // Wallet Card Arguments
                'pin_ref'     => $this->getUser()->getAttribute('pin_ref'),
                'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                'company'     => $arguments['company'],
                'street'      => $arguments['address'],
                'town'        => $arguments['town'],
                'county'      => $arguments['county'],
                'post_code'   => $arguments['postcode'],
                'country'     => $arguments['country'],
                // Additional Arguments
                'isAdmin'     => false,
            )
        );

        // Check if there was any Errors
        if (count($response['error']) > 0) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(array('error_messages' => $response['error']));
            return sfView::NONE;
        }

        // Authenticate the User, by logging them in.
        if (!empty($arguments['password'])) {
            try {
                $loginResponse = Plus_Authenticate::logIn(
                    $this->getUser()->getAttribute('email', $arguments['email']),
                    $arguments['password']
                );
            } catch (Exception $e) {
                $this->logMessage(
                    'Exception Called. Authentication Failed Or Unable to Login.' . $e->responseBody,
                    'err'
                );
                $loginResponse = false;
            }
        }

        if ((isset($loginResponse) && $loginResponse) || (!isset($loginResponse))) {
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'fail'));
        }

        return sfView::NONE;
    }

    /**
     * This is the Last Step in the Its-Your-Call-A Route [PAGE]
     * Its used for displaying information welcoming them to the Powwownow Service
     * They could either have completed a full Enhanced Login, or a Partial Login (Without Password)
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeItsYourCallStepB(sfWebRequest $request)
    {
        $email      = $this->getUser()->getAttribute('email', null);
        $contactRef = $this->getUser()->getAttribute('contact_ref', null);

        // Check the Email and the Contact Ref
        if (is_null($email) || is_null($contactRef)) {
            $this->logMessage('Redirecting... Email: ' . $email . ', contact_ref: ' . $contactRef, 'err');
            $this->redirect('its_your_call_a', 301);
        }

        // PIN Information
        $pins      = PinHelper::getDefaultPins($this->getUser()->getAttribute('contact_ref'));
        $this->pin = $pins['pin'];

        switch ($this->getContext()->getRouting()->getCurrentRouteName()) {
            case 'lets_get_it_done_step_b':
                $this->cssClass = 'gid';
                break;

            default:
                $this->cssClass = 'standard';
        }


        $this->isRegistered = $this->getUser()->hasCredential('POWWOWNOW');
    }

    /**
     * Homepage E [PAGE]
     * @author Asfer Tamimi, Dav C
     * @amend
     *  31/07/2014 - Removed "proemail" param, was not being used - Dav C
     *  31/07/2014 - Removed sfWebRequest as function args, not being used. Dav C
     *  31/07/2014 - Updated Doc Block.
     * @return string
     */
    public function executeIndexE(sfWebRequest $request)
    {
        return sfView::SUCCESS;
    }

    /**
     * Homepage E Full Registration. Used after the Basic Registration has been completed
     * @author Asfer
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeFullRegistrationEAjax(sfWebRequest $request)
    {
        // Set Output Format
        $this->getResponse()->setContentType('application/json');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Partial', 'DialInNumbers'));


        // Form Validation
        // @todo Move to Symfony Forms
        $form   = $request->getPostParameter('fullRegistration');
        $errors = array();

        if ($form['first_name'] == '') {
            $errors = array(
                'error_message' => 'FORM_VALIDATION_NO_FIRST_NAME',
                'field_name'    => 'fullRegistration["first_name"]'
            );
        } elseif ($form['last_name'] == '') {
            $errors = array(
                'error_message' => 'FORM_VALIDATION_NO_SURNAME',
                'field_name'    => 'fullRegistration["last_name"]'
            );
        } elseif ($form['password'] == '') {
            $errors = array(
                'error_message' => 'FORM_VALIDATION_NO_PASSWORD',
                'field_name'    => 'fullRegistration["password"]'
            );
        } elseif (strlen($form['password']) < 6) {
            $errors = array(
                'error_message' => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS',
                'field_name'    => 'fullRegistration["password"]'
            );
        } elseif ($form['password'] != $form['confirm_password']) {
            $errors = array(
                'error_message' => 'FORM_VALIDATION_PASSWORD_MISMATCH',
                'field_name'    => 'fullRegistration["confirm_password"]'
            );
        }

        if (!empty($errors)) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error_messages' => $errors));
            return sfView::NONE;
        }

        $arguments = $form;

        // Check the Optional Question
        if (!empty($arguments['question'])) {
            try {
                $response = Hermes_Client_Rest::call(
                    'saveRegistrationAnswer',
                    array(
                        'question_id' => '1',
                        'answer'      => $arguments['question'],
                        'contact_ref' => $this->getUser()->getAttribute('contact_ref')
                    )
                );
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage(
                    'saveRegistrationAnswer Hermes call has timed out. Contact Ref: ' . $this->getUser()->getAttribute(
                        'contract_ref'
                    ),
                    'err'
                );
                echo json_encode(
                    array(
                        'error_messages' => array(
                            array(
                                'message'    => 'FORM_COMMUNICATION_ERROR',
                                'field_name' => 'alert'
                            )
                        )
                    )
                );
                return sfView::NONE;
            } catch (Hermes_Client_Exception $e) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage(
                    'saveRegistrationAnswer Hermes Client Exception. ErrorCode: ' . $e->getCode(
                    ) . ', Message: ' . $e->getMessage(),
                    'err'
                );
                switch ($e->getCode()) {
                    case '001:000:001':
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'API_RESPONSE_EMAIL_TAKEN_AND_NON_STANDARD_PWN_RESPONSIVE',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                    case '001:000:005':
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'API_RESPONSE_ACCOUNT_ALREADY_CREATED_FOR_REQUESTED_DATA',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                    case '001:000:007':
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE1',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                    default:
                        echo json_encode(
                            array(
                                'error_messages' => array(
                                    'message'    => 'FORM_COMMUNICATION_ERROR',
                                    'field_name' => 'alert'
                                )
                            )
                        );
                        break;
                }
                return sfView::NONE;
            } catch (Exception $e) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage('saveRegistrationAnswer Unknown Error Occurred: ' . $e->getMessage(), 'err');
                echo json_encode(
                    array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
                );
                return sfView::NONE;
            }
        }

        // Update the Contact to create a Normal Powwownow Account
        if (!empty($arguments['password'])) {
            $result = Common::doCreateOrUpdateAccount(
                array(
                    'email'      => $this->getUser()->getAttribute('email'),
                    'first_name' => $arguments['first_name'],
                    'last_name'  => $arguments['last_name'],
                    'password'   => $arguments['password'],
                    'source'     => '/PWN-Login-Created-D',
                    'locale'     => $this->getUser()->getCulture(),
                )
            );

            // Check if there was an Error from the above Hermes Call
            if (isset($result['error'])) {
                $this->getResponse()->setStatusCode(500);
                if ('email' == $result['error']['field_name']) {
                    $result['error']['field_name'] = 'alert';
                }
                $this->logMessage('doCreateOrUpdateAccount Hermes Error.' . print_r($result['error'], true), 'err');
                echo json_encode(array('error_messages' => $result['error']));
                return sfView::NONE;
            }
        }

        // Authenticate the User, by logging them in.
        try {
            $loginResponse = Plus_Authenticate::logIn(
                $this->getUser()->getAttribute('email', $arguments['email']),
                $arguments['password']
            );
        } catch (Exception $e) {
            $this->logMessage('Exception Called. Authentication Failed Or Unable to Login.' . $e->responseBody, 'err');
            $loginResponse = false;
        }

        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumber = DialInNumbersHelper::getGetDialInNumbers();

        if ($loginResponse) {
            echo json_encode(
                array(
                    'html'   => get_partial('pages/fullRegistrationEAjax', array('dialInNumber'=> $dialInNumber)),
                    'status' => 'success'
                )
            );
        } else {
            // @todo Need to Catch this error correctly
            echo json_encode(
                array(
                    'html'   => get_partial('pages/fullRegistrationEAjax', array('dialInNumber'=> $dialInNumber)),
                    'status' => 'fail'
                )
            );
        }

        return sfView::NONE;
    }

    /**
     * Homepage E Request Welcome Pack Registration. Used after the Full Registration has been completed
     * @author Asfer
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeRequestWelcomePackEAjax(sfWebRequest $request)
    {
        // Set Output Format
        $this->getResponse()->setContentType('application/json');
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        // Form Validation
        // @todo Move to Symfony Forms
        $arguments = $request->getPostParameter('welcomePack');
        $errors    = array();

        if ($arguments['building'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_NO_ADDRESS', 'field_name' => 'welcomePack["building"]');
        } elseif ($arguments['town'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_NO_TOWN', 'field_name' => 'welcomePack["town"]');
        } elseif ($arguments['postal_code'] == '') {
            $errors = array(
                'error_message' => 'FORM_VALIDATION_INVALID_POSTCODE',
                'field_name'    => 'welcomePack["postal_code"]'
            );
        }

        if (!empty($errors)) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error_messages' => $errors));
            return sfView::NONE;
        }

        // Obtain the PIN Ref from the Contact Ref
        try {
            $pin_ref = null;
            $pins    = Hermes_Client_Rest::call(
                'getContactPins',
                array('contact_ref' => $this->getUser()->getAttribute('contact_ref'))
            );
            if (count($pins) == 1) {
                $pin_ref = $pins[0]['pin_ref'];
            }
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage("getContactPins Hermes call has timed out.", 'err');
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                "getContactPins Hermes call is faulty. Error message: " . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode(),
                'err'
            );
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                "getContactPins Failed. Error Message: " . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode(
                ),
                'err'
            );
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        // Check if there is a Wallet Card Address Set (There should not be, since its a new user (Waste of step?)
        // Get the Existing Wallet Card Address + Create a Wallet Card Address
        try {
            $response = Hermes_Client_Rest::call(
                'getWalletCardAddress',
                array(
                    'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                    'pin_ref'     => $pin_ref,
                )
            );

            if (!empty($response)) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage('getWalletCardAddress Returned 0 Records', 'err');
                echo json_encode(
                    array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
                );
                return sfView::NONE;
            }
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'getWalletCardAddress Hermes call has timed out. Contact Ref: ' . $this->getUser()->getAttribute(
                    'contract_ref'
                ),
                'err'
            );
            echo json_encode(
                array(
                    'error_messages' => array(
                        array(
                            'message'    => 'FORM_COMMUNICATION_ERROR',
                            'field_name' => 'alert'
                        )
                    )
                )
            );
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            // There are no Exceptions in the actual Hermes Method, so show default Error Message
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'getWalletCardAddress Hermes Client Exception. ErrorCode: ' . $e->getCode(
                ) . ', Message: ' . $e->getMessage(),
                'err'
            );
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage('getWalletCardAddress Unknown Error Occurred: ' . $e->getMessage(), 'err');
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        $arguments['address'] = (isset($arguments['building'])) ? $arguments['building'] : '';
        $arguments['address'] .= (isset($arguments['street'])) ? ', ' . $arguments['street'] : '';

        $response = Common::createWalletCardRequest(
            array(
                // Wallet Card Arguments
                'pin_ref'     => $pin_ref,
                'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                'company'     => (isset($arguments['company'])) ? $arguments['company'] : '',
                'street'      => (isset($arguments['address'])) ? $arguments['address'] : '',
                'town'        => (isset($arguments['town'])) ? $arguments['town'] : '',
                'county'      => (isset($arguments['county'])) ? $arguments['county'] : '',
                'post_code'   => (isset($arguments['postal_code'])) ? $arguments['postal_code'] : '',
                'country'     => (isset($arguments['country'])) ? $arguments['country'] : '',
                // Additional Arguments
                'isAdmin'     => false,
            )
        );

        // Check if there was any Errors
        if (count($response['error']) > 0) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(array('error_messages' => $response['error']));
            return sfView::NONE;
        }

        echo json_encode(
            array(
                'html'   => get_partial('pages/requestWelcomePackEAjax'),
                'status' => 'success'
            )
        );
        return sfView::NONE;
    }

    public function executePinReminder()
    {

    }


    /**
     * PIN Reminder Right Menu Ajax
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     */
    public function executePinReminderAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        $email  = $request->getPostParameter('email');
        $mobile = $request->getPostParameter('mobile_number');
        $locale = $this->getUser()->getCulture();
        $args   = array();

        // Email Validation
        try {
            $v             = new sfValidatorEmail();
            $email         = $v->clean($email);
            $args['email'] = $email;
        } catch (sfValidatorError $e) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS'));
            return sfView::NONE;
        }

        // Mobile Validation
        if (!is_null($mobile) || '' != $mobile) {
            $args['mobile_number'] = $mobile;
        }

        $args['locale'] = $locale;

        //Get request source of referrer page
        preg_match('/(http|https):\/\/.+?(\/.*)/', $request->getReferer(), $refRequestMatch);
        $refRequest             = isset($refRequestMatch[2]) ? $refRequestMatch[2] : '';
        $args['request_source'] = $refRequest;

        // Sent PIN Reminder Email
        try {
            $result = Hermes_Client_Rest::call('sendPinReminderEmail', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->getResponse()->setStatusCode(400);
            sfContext::getInstance()->getLogger()->err('sendPinReminderEmail Hermes call has timed out.');
            echo json_encode(
                array('error' => 'FORM_COMMUNICATION_ERROR'));
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'sendPinReminderEmail Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );

            $this->getResponse()->setStatusCode(400);
            switch ($e->getCode()) {
                case '001:000:000':
                    $errorMessages = 'API_RESPONSE_NO_ACCOUNT_FOR_EMAIL';
                    break;
                case '001:000:029':
                    $errorMessages = 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE';
                    break;
                case '001:000:030':
                case '001:000:031':
                    $errorMessages = 'API_RESPONSE_CONTACT_IS_NOT_STANDARD_POWWOWNOW_USER';
                    break;
                default:
                    $errorMessages = 'FORM_COMMUNICATION_ERROR';
                    break;
            }

            echo json_encode(array('error' => $errorMessages));
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(400);
            sfContext::getInstance()->getLogger()->err(
                'sendPinReminderEmail Failed. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
            echo json_encode(
                array('error' => 'FORM_COMMUNICATION_ERROR')
            );
        }
        echo json_encode(array());
        return sfView::NONE;
    }

    /**
     * Terms And Conditions [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeTermsAndConditions()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Partial'));
        $this->setVar(
            'termsAndConditionsList',
            array(
                1 => get_partial('pages/tandcPWNGeneral', array()),
                2 => get_partial('pages/tandcPWNService', array()),
                3 => get_partial('pages/tandcPWNPlusService', array()),
                4 => get_partial('pages/tandcPWNPremiumService', array()),
                5 => get_partial('pages/tandcPWNEngageService', array()),
                6 => get_partial('pages/tandcPWNVideoConferencingService', array()),
                7 => get_partial('pages/tandcPWNIMeet', array())
            ),
            true
        );
    }

    /**
     * How Conference Calling Works [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeHowConferenceCallingWorks(sfWebRequest $request)
    {
    }

    /**
     * Homepage PPC Short Term Testing [PAGE]
     * @param sfWebRequest $request
     * @author Asfer Tamimi
     * @return sfVIEW::SUCCESS
     */
    public function executeHomepagePPCShortTerm(sfWebRequest $request)
    {
        $this->promoemail = $request->getParameter('promoemail', null);
        if ($request->hasParameter('forceVar')) {
            $homePageCroVariation = $request->getGetParameter('forceVar');
            $homePageCroVariationArray = array('E', 'H1', 'H2');

            if (!in_array($homePageCroVariation, $homePageCroVariationArray)) {
                $homePageCroVariation = 'E';
            }
            $this->getUser()->setAttribute('homePageCroVariation', $homePageCroVariation, 'cro');

            switch ($homePageCroVariation) {
                case 'E':
                    apc_store('cx2_variant', 0);
                    break;
                case 'H1':
                    apc_store('cx2_variant', 1);
                    break;
                case 'H2':
                    apc_store('cx2_variant', 2);
                    break;
            }
        }
    }

    /**
     * Homepage PPC Long Term Testing [PAGE]
     * @param sfWebRequest $request
     * @author Asfer Tamimi
     * @return sfVIEW::SUCCESS
     */
    public function executeHomepagePPCLongTerm(sfWebRequest $request)
    {
        $this->promoemail = $request->getParameter('promoemail', null);
    }

    /**
     * Revalidate Pin [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     */
    public function executeRevalidatePin(sfWebRequest $request)
    {
        $result = PinHelper::getContactPinPairs($request->getParameter("contact_ref"), 'USER', false);
        $this->forward404If(count($result) != 1);
        $pinKeys              = array_keys($result);
        $this->pin            = $pinKeys[0];
        $this->logos          = sfConfig::get('app_logos');
        $this->serviceUser    = $this->getUser()->getAttribute('service_user', 'UNAUTHENTICATED');
        $this->regionLinks    = Common::getRegionalLinks($this->getUser()->getCulture());
        $this->currentCountry = (isset($_SERVER['country'])) ? $_SERVER['country'] : 'GBR';
    }

    /**
     * How Web Conferencing Works [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     * @author Dav C
     * @amend
     *  30/01/2014 - Dav C - Jira [DEV-2576]
     *      https://powwownow.atlassian.net/browse/DEV-2576
     *      Download button added with JS tracking
     *      CSS file added for new copy text changes
     */
    public function executeHowWebConferencingWorks(sfWebRequest $request)
    {
        $this->getResponse()->addStyleSheet('/sfcss/pages/howWebConferencingWorks.css');
        $this->getResponse()->addJavascript('/sfjs/pages/howWebConferencingWorks.js', 'first');

        if ($this->getUser()->isAuthenticated()) {
            $link = '@web_conferencing_auth';
        } else {
            $link = '/Login?d=1';
        }

        $this->setVar('link', $link);
    }

    /**
     * Meeting Cost Calculator [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Robert
     *
     */
    public function executeMeetingCostCalculator(sfWebRequest $request)
    {
    }

    /**
     * Glossary [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     *
     */
    public function executeGlossary(sfWebRequest $request)
    {
    }

    /**
     * Sitemap [PAGE]
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     *
     */
    public function executeSitemap(sfWebRequest $request)
    {
    }

    /**
     * Privacy [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executePrivacy(sfWebRequest $request)
    {
    }

    /**
     * Useful Links [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executeUsefulLinks(sfWebRequest $request)
    {
        $this->regionLinks = Common::getRegionalLinks($this->getUser()->getCulture());
    }

    /**
     * Top 10 Tips for Conference Calling [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executeTopTenTipsForConferenceCalling(sfWebRequest $request){
}

    /**
     * Top 10 Tips for Web Conferencing [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executeTopTenTipsForWebConferencing(sfWebRequest $request){}

    /**
     * How to Hold a Web Conference  [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executeHowToHoldAWebConference(sfWebRequest $request){}

    /**
     * Setting up A Web Conference [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer
     *
     */
    public function executeSetUpAWebConference(sfWebRequest $request){}

    /**
     * Let's Get It Done Landing Page
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Dav
     */
    public function executeLetsGetItDoneLandingPage(sfWebRequest $request){}

    /**
     * @param sfWebRequest $request
     */
    public function executeResetPassword(sfWebRequest $request)
    {
        $form = new ResetPasswordForm();
        $this->setVar('form', $form);

        // Presume token is invalid
        $tokenValid = false;

        // Validate token and contact ref
        $token      = $request->getGetParameter('token');
        $contactRef = $request->getGetParameter('contact');

        if (!empty($token) && !empty($contactRef)) {
            // Check the token is valid through API
            $tokenValid = Common::checkPasswordResetToken(array('contact_ref' => $contactRef, 'token' => $token));
        }

        $this->setVar('tokenValid', $tokenValid);
        $this->setVar('token', $token);
        $this->setVar('contactRef', $contactRef);
    }

    /**
     * Reset Password [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Alexander Farrow
     */
    public function executeResetPasswordAjax(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->logMessage('Forgotten Password Request, was not a POST Ajax Request.', 'err');
            $this->forward404();
        }
        $errorMessages = array();
        $token         = $request->getPostParameter('resetpassword[token]');
        $contact       = $request->getPostParameter('resetpassword[contact]');
        $password      = $request->getPostParameter('resetpassword[password]');

        // Check the supplied token was valid
        $tokenValid = Common::checkPasswordResetToken(array('contact_ref' => $contact, 'token' => $token));
        $passInvalid  = Common::checkifInvalidPassword($password);

        if (!$tokenValid) {
            $errorMessages = array('error' => 'FORM_VALIDATION_FORGOT_PASSWORD_EXPIRED_TOKEN');
        } elseif ($passInvalid) {
            $errorMessages = array('error' => $passInvalid);
        } elseif ($password !== $request->getPostParameter('resetpassword[confirm_password]')) {
            $errorMessages =  array('error' => 'FORM_VALIDATION_PASSWORD_MISMATCH');
        }

        if (!empty($errorMessages)) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode($errorMessages);
            return sfView::NONE;
        } else {
            //Reset pass
            $result = Common::updateContact(
                array(
                    'contact_ref' => $contact,
                    'password'    => $password
                )
            );

            if (isset($result['error_messages'])) {
                $this->getResponse()->setStatusCode(400);
                echo json_encode(array('error' => $result['error_messages']));
                return sfView::NONE;
            }
        }

        // Success Response
        echo json_encode(array());
        return sfView::NONE;
    }

    /**
     * Forgotten Password [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     * @author Jack Adams
     */
    public function executeForgottenPassword(sfWebRequest $request)
    {
        // getParameter returns an empty string if 'email' doesn't exist. Don't need to manually check.
        $this->setVar('userEmail', $request->getParameter('email'));
    }

    /**
     * Forgotten Password [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     */
    public function executeForgottenPasswordAjax(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->logMessage('Forgotten Password Request, was not a POST Ajax Request.', 'err');
            $this->forward404();
        }

        // Check Email Address
        if (!Common::isValidEmail($request->getPostParameter('email',''))) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error' => 'CLIENT_INVALID_EMAIL_ADDRESS'));
            return sfView::NONE;
        }

        // Initiate a Password Reset Process
        $result = Common::doPasswordReset(
            array(
                'email'  => $request->getPostParameter('email', ''),
                'locale' => $this->getUser()->getCulture(),
            )
        );

        // Check if an Error Occurred
        if (isset($result['error_messages'])) {

            if (array_key_exists('message', $result['error_messages'])) {
                $errorMessages = $result['error_messages']['message'];
            } else {
                $errorMessages = $result['error_messages'];
            }

            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error' => $errorMessages));
            return sfView::NONE;
        }

        // Success Response
        echo json_encode(array());
        return sfView::NONE;
    }

    /**
     * Compare Services
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeCompareServices(sfWebRequest $request)
    {
    }

    /**
     * Tell A Friend [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeTellAFriend(sfWebRequest $request)
    {
        $this->form   = new tellAFriendForm();
        $this->fields = array('your_name', 'your_email', 'friends_name', 'friends_email');
    }

    /**
     * Tell A Friend [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     */
    public function executeTellAFriendAjax(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->logMessage('TellAFriend Request, was not a POST Ajax Request.', 'err');
            $this->forward404();
        }

        $this->getResponse()->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new tellAFriendForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments = Common::getArguments($request->getParameter($form->getName(), array()), array());
        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(500);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form)
            );
        }

        // Send a Tell A Friend Email
        $result = Common::sendTellAFriendEmail(
            array(
                'from_name'   => $arguments['your_name'],
                'from_email'  => $arguments['your_email'],
                'to_name'     => $arguments['friends_name'],
                'to_email'    => $arguments['friends_email'],
                'locale'      => $this->getUser()->getCulture(),
                'service_ref' => 801
            )
        );

        // Check for an Error
        if (empty($result)) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(
                array('error_messages' => array('message' => 'TELL_A_FRIEND_FAILED_TO_SEND', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        // Success Response
        echo json_encode(array('status' => 'sent'));
        return sfView::NONE;
    }

    /**
     * About Us Security [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeSecurity(sfWebRequest $request)
    {
    }

    /**
     * Unsubscribe [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     */
    public function executeUnsubscribe(sfWebRequest $request)
    {
        $this->form = new unsubscribeForm();
        $this->fields = array('email');
    }

    /**
     * Unsubscribe AJAX [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Alexander Farrow
     */
    public function executeUnsubscribeAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        $errorMessages = array();
        // Email Validation
        try {
            $v     = new sfValidatorEmail();
            $email = $v->clean($request->getPostParameter('unsubscribe[email]'));
        } catch (sfValidatorError $e) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS',
                        'field_name' => 'unsubscribe[email]'
                    )
                )
            );
            return sfView::NONE;
        }
        $response = Common::doOptOutContact(array('email' => $email, 'type' => 'marketing'));

        if (empty($response) || isset($response['error_messages'])) {
            $this->getResponse()->setStatusCode('400');
            echo json_encode($response);
        } else {
            echo json_encode(array('status' => 'sent'));
        }

        return sfView::NONE;
    }

    /**
     * Unsubscribe Service [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     */
    public function executeUnsubscribeService(sfWebRequest $request)
    {
        $this->unsubscribeForm = new unsubscribeForm();
        $this->unsubscribeFields = array('email');
        $this->deletePinForm = new deletePinForm();
        $this->deletePinFields = array('email', 'pin');


    }

    /**
     * Delete Pin [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Alexander Farrow
     */
    public function executeDeletePinAjax(sfWebRequest $request)
    {
        // Assert AJAX request.
        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            $this->getLogger("not a Ajax request or not Posted - aborting", 'err');
            $this->forward404();
        }

        $email = $request->getPostParameter('deletepin[email]');
        $pin = $request->getPostParameter('deletepin[pin]');

        $this->getResponse()->setContentType('application/json');
        $errorMessages = array();
        if (empty($email) || !Common::isValidEmail($email)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'field_name' => 'deletepin[email]');
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        if (empty($pin)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_PIN', 'field_name' => 'deletepin[pin]');
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        $response = Common::doDeactivateAccount(array('email' => $email, 'pin' => $pin));

        // Check if there was an Error from the above Hermes Call
        if (isset($response['error_messages'])) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode($response);
            return sfView::NONE;
        }
        else {
            echo json_encode(array('status' => 'sent'));
        }
        return sfView::NONE;
    }

    /**
     * Conference Call Branding [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallBranding(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Charities [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallCharities(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Etiquette [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallEtiquette(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Headsets [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallHeadsets(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Time Zones [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallTimeZones(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Consultants [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallConsultants(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Finance [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallFinance(sfWebRequest $request)
    {
    }

    /**
     * Conference Call How To Conference Call [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallHowToConferenceCall(sfWebRequest $request)
    {
    }

    /**
     * Conference Call IT Services [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallITServices(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Manufacturing [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallManufacturing(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Pharmaceutical [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallPharmaceutical(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Public Sector [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallPublicSector(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Retail [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallRetail(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Services Powwownow And Skype [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallServicesPowwownowAndSkype(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Services Reporting [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallServicesReporting(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Set Up A Conference Call [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallSetUpAConferenceCall(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Solutions Live Events And Webinars [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallSolutionsLiveEventsAndWebinars(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Solutions Meetings [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallSolutionsMeetings(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Solutions Project And Product Launches [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallSolutionsProductAndProductLaunches(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Technical Support [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallSolutionsTechnicalSupport(sfWebRequest $request)
    {
    }

    /**
     * Conference Call Training [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallSolutionsTraining(sfWebRequest $request)
    {
    }

    /**
     * Conference Call What Is A Conference Call [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeConferenceCallWhatIsAConferenceCall(sfWebRequest $request)
    {
    }

    /**
     * The Workplace Can't [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     */
    public function executeTheWorkplaceCant(sfWebRequest $request)
    {
    }

    /**
     * The Workplace Can't (Phone) [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     */
    public function executeTheWorkplaceCantPhone(sfWebRequest $request)
    {
    }

    /**
     * The Workplace Can't (Tablet) [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Alexander Farrow
     */
    public function executeTheWorkplaceCantTablet(sfWebRequest $request)
    {
    }

    /**
     * Free Conference Call Service [PAGE]
     * @author Davinder
     * @author Asfer
     */
    public function executeFreeConferenceCallService()
    {
        $this->setVar('trustPilotReviews', $this->getTrustPilot5StartReviews());
    }

    /**
     * Conference Call One [PAGE]
     * @author Davinder
     * @author Asfer
     * @author Jack
     */
    public function executeConferenceCallOne()
    {
        $this->setVar('trustPilotReviews', $this->getTrustPilot5StartReviews());
    }

    /**
     * Teleconference One [PAGE]
     * @author Davinder
     * @author Asfer
     * @author Jack
     */
    public function executeTeleconferenceOne()
    {
        $this->setVar('trustPilotReviews', $this->getTrustPilot5StartReviews());
    }

    /**
     * @return array
     */
    private function getTrustPilot5StartReviews()
    {
        try {
            $trustPilot = TrustPilot::getFeed(true);
            $trustPilotReviews = $trustPilot->Reviews;
        } catch (Exception $e) {
            $trustPilotReviews = null;
        }

        if (is_array($trustPilotReviews)) {
            $reviews = array();

            foreach ($trustPilotReviews as $trustPilotReview) {
                if ($trustPilotReview->TrustScore->Stars === 5) {
                    $reviews[] =$trustPilotReview;
                }
            }

            return $reviews;
        }

        return array();
    }

    /**
     * Homepage CJ [PAGE]
     * @param sfWebRequest $request
     * @author Asfer Tamimi
     * @return sfVIEW::SUCCESS
     */
    public function executeHomepageCJ(sfWebRequest $request)
    {
        $this->setVar('promoemail', $request->getParameter('promoemail', null));
    }
}
