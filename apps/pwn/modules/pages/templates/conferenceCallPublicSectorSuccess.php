<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'Public sector')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Calls for the Public Sector'),'title' => __('Conference Calls for the Public Sector'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Calls for the Public Sector '), 'headingSize' => 'xxl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>Working on the public’s behalf can be a demanding job – whether you’re struggling under the weight of budget cuts or simply increased demand for your service, there’s always something to contend with. With free, reservationless <a title="conference call services" href="<?php echo url_for('@conference_call_services'); ?>">conference call services</a> life can be a lot easier, whether you’re liaising with external companies, other public bodies, or the public itself. Powwownow provide reliable teleconferencing services that are simple, efficient and cost-effective to use – a dream combination for the public sector!</p>
                <p>When you’re working in the public’s best interest, there can be a lot of voices wanting to be heard. Making sure they all get their say while sticking to a strict budget can seem like an impossible task, but <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference calling</a> through Powwownow can let you hold teleconferences with up to 1,000 participants for the cost of a regular telephone call. There’s no need to buy expensive conference call equipment or tie yourself into a lengthy contract. Additionally, conference calls of any size will never cost you more than a normal two-way call, so you can move public funds away from administration costs and back into action.</p>
                <p>We are working more closely than ever with our European friends and the Public sector feels this pull more than anyone. Unfortunately, this can mean a lot of expensive trips to the continent for decision makers and committee members which could be avoided by having a simple, reliable conference calling service in place. Powwownow’s <a title="international conference call" href="<?php echo url_for('@international_conference_call'); ?>">international conference call</a> service provides a fast, affordable way to get in touch with contacts on the continent and beyond and all you need to get started is a telephone, Powwownow’s local access international dial-in numbers and your unique PIN - and those long, pricey flights will be a thing of the past.</p>
                <p>Due to the Freedom of Information act, it’s important that public sector workers keep as many of their conversations “on the record” as possible; this may have compelled you to use email in place of going through the fuss of setting up a meeting and finding a minute-taker. Powwownow’s conference call recording service puts paid to this problem and allows you to record your conference calls for transcription at a later date. This service can also be invaluable if you have a language barrier to overcome, as you can go back and listen again for clarification on misunderstood sentences. Now you won’t have to ask your influential guests to repeat themselves if you don’t catch what they’re saying first time.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>