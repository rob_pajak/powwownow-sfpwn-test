<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSolutionsLeftNav', array('activeItem' => 'Technical support')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Solutions', 'Technical Support'),'title' => __('Technical Support'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Technical Support'), 'headingSize' => 'l', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                 <p>Giving your customer support team an edge over the competitors in this day and age is more important than ever and <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference call</a> services are just what every customer support team needs to put them ahead of the pack. In a world where the customer is always right, but needs an increasing amount of support throughout the purchase and aftercare process, providing your team with Powwownow’s conference call solutions will enable them to give the best support possible while driving down costs and increasing productivity.</p>
                <p>Powwownow is a leading <a title="conference call providers" href="<?php echo url_for('@conference_call_providers'); ?>">conference call provider</a> and no one offers conference calling for customer support teams like we do! Our conference call service means all your employees need is a telephone, our dial-in number and a conference call PIN number and they can do their job instantly and remotely from almost anywhere in the world. As with any good customer service system, our conference call service allows you to record calls for training purposes later on, as well as having more than one service representative on the call at any one time, but the added benefit of being able to hold these calls from a normal telephone with no additional conference call hardware required.</p>
                <p>For technical support employees, Powwownow supply <a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services'); ?>">teleconferencing services</a>, including web conferencing which allows you to share your desktop and documents with other conference call participants. This can be of huge benefit with in-house support across multiple office sites as technical support employees can share desktops to either demonstrate how a task is performed or can see problems a colleague is having displayed on their own desktop. Training documents and presentations can be easily shared through the conference call, making it an indispensable technical training tool, and all of this won’t cost you a penny more than a standard telephone call.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>