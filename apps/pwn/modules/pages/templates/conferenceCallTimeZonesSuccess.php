<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'Conference call time zones')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Call Time Zones'),'title' => __('Conference Call Time Zones'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Call Time Zones'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>Do you want to make use of Powwownow&rsquo;s fantastic&nbsp;<a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services');?>">teleconferencing services</a>, but are unsure as to what time you should be calling people overseas? After all you don&rsquo;t want to ring after office hours or, even worse, wake someone up in the middle of the night. Powwownow have put together an extensive list of the time zones of the world&rsquo;s &ldquo;global cities&rdquo;, along with the dial-in numbers your international participants will need to call to access your teleconference. <br /><br />That&rsquo;s not all Powwownow offer to make it easy for you to hold international conference calls.&nbsp; The free Powwownow Scheduler tool makes it simple for you to coordinate conference calls with participants across multiple time zones. Simply enter a list of your participants and the date and time you want to hold the call and the Scheduler will email all callers with the details in their local time and with the appropriate dial-in number for their location. The Scheduler will also send all participants reminders of the call time and their local dial-in details, so you can be sure no one will miss out.<br /><br />With Powwownow you have all the information you need to hold a first class international conference call. Now you can be sure that you&rsquo;re&nbsp;<a title="voice conferencing" href="<?php echo url_for('@voice_conferencing');?>">voice conferencing</a> at an hour to suit everyone and that you won&rsquo;t have that key participant falling asleep mid-sentence!</p>
                <ul class="chevron-green">
                    <li class="png pie_first-child"><strong>Buenos Aires, Argentina:</strong> GMT-3 (44 844 4 73 73 73)</li>
                    <li class="png"><strong>Sydney, Australia:</strong> GMT+10 (44 844 4 73 73 73)</li>
                    <li class="png"><strong>Vienna, Austria:</strong> GMT+1 (0820 4000 1503)</li>
                    <li class="png"><strong>Brussels, Belgium:</strong> GMT+1 (070 35 99 45)</li>
                    <li class="png"><strong>Toronto, Canada:</strong> GMT-5 (1 213 289 3444)</li>
                    <li class="png"><strong>Beijing, The People&rsquo;s Republic of China:</strong> GMT+8 (44 844 4 73 73 73)</li>
                    <li class="png"><strong>Paris, France:</strong> GMT+1 (0821 230 748)</li>
                    <li class="png"><strong>Berlin, Germany:</strong> GMT+1 (01803 001 178)</li>
                    <li class="png"><strong>Dublin, Ireland:</strong> GMT (0818 270 007)</li>
                    <li class="png"><strong>Rome, Italy:</strong> GMT+1 (848 390 166)</li>
                    <li class="png"><strong>Tokyo, Japan:</strong> GMT+9 (44 844 4 73 73 73)</li>
                    <li class="png"><strong>Mexico City, Mexico:</strong> GMT-6 (44 844 4 73 73 73)</li>
                    <li class="png"><strong>Amsterdam, Netherlands:</strong> GMT+1 (0870 001 901)</li>
                    <li class="png"><strong>Warsaw, Poland:</strong> GMT+1 (0801 003 543)</li>
                    <li class="png"><strong>Moscow, Russia:</strong> GMT+3 (44 844 4 73 73 73)</li>
                    <li class="png"><strong>Johannesburg, South Africa:</strong> GMT+2 (087 550 0375)</li>
                    <li class="png"><strong>Seoul, South Korea:</strong> GMT+9 (44 844 4 73 73 73)</li>
                    <li class="png"><strong>Barcelona, Spain:</strong> GMT+1 (902 885 318)</li>
                    <li class="png"><strong>Stockholm, Sweden:</strong> GMT+1 (0939 2066 300)</li>
                    <li class="png"><strong>Zurich, Switzerland:</strong> GMT+1 (0848 560 190)</li>
                    <li class="png"><strong>New York, United States:</strong> GMT-5 (1 415 363 0833)</li>
                    <li class="png"><strong>Chicago, United States:</strong> GMT-6 (1 415 363 0833)</li>
                    <li class="png"><strong>Los Angeles, United States:</strong> GMT-8(1 415 363 0833)</li>
                </ul>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>