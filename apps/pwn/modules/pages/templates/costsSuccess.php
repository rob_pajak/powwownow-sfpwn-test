<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('What It costs'),
                'headingTitle' => __('What It costs'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <p>It’s simple! Powwownow uses its own advanced technology and operates with low overhead costs. We truly believe in working smarter to make it easier (and more cost effective) for conference callers worldwide.</p>
            <p>That's why, with our standard service, <strong>we do not charge you any kind of fee</strong> - you just pay for your own phone call and these charges appear on your phone bill in the same way as any other call. That's it!</p>
            <p>With <a href="<?php echo url_for('@homepage'); ?>"><strong>Powwownow</strong></a>, there are no bridging costs. Each conference call participant, including the chairperson, pays just 4.3p per minute + VAT from a BT Landline - that’s it*. So the same one-hour call to an 0844 number using Powwownow would cost the chairperson a grand total of just <strong>£2.58 + VAT!</strong></p>
            <p>Always on the go and worried about expensive mobile charges?</p>
            <p>When dialling-in to conference calls using a mobile phone, use our mobile shortcode to help save you money: Dial <strong>87373</strong> and enter your conference PIN as usual and you will join your conference.  With some providers charging as much as 40p per minute, our mobile dial-in number is set at a standard <strong>12.5ppm + VAT</strong>.</p>
            <p><img title="Cost Comparison table" src="/sfimages/Cost-comparison.png" alt="Cost Comparison table"/></p>
        </div>
        <div class="clearfix">
            <div class="hr-spotted-top png content-seperator margin-top-10 clearfix"><!--Blank--></div>
            <div class="footnote">* Call charges may vary depending on your network provider.</div>
        </div>
        <div class="clearfix">
            <h3 class="lightgreen">A lot more for a little extra...</h3>
            <p>Our <a title="Powwownow Premium" href="<?php echo url_for('@premium_service'); ?>">Premium</a> business packages offer great value package deals to corporate customers who want additional features. Call us on <strong>0800 022 9922</strong> or <strong>020 3398 0398</strong> for rates that meet your business needs.</p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
