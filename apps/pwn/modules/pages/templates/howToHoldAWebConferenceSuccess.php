<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('How to Hold a Web Conference'),
                'headingTitle' => __('How to Hold a Web Conference'),
                'headingSize' => 'xxl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p>Holding a successful <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference call</a> is always a challenge, but with a web conference you are sitting people in front of a ready distraction – their computer! Don’t despair though, as holding the perfect web conference doesn't need to be an uphill struggle as long as you prepare yourself and your participants about what to expect.</p>
        <p>To hold a productive <a title="web conferencing" href="<?php echo url_for('@web_conferencing'); ?>">web conference</a> you will all have to think of it as if it were a regular meeting with a few added challenges. You should circulate an agenda for your web conference ahead of time, and if it is part of a series of meetings try and send along minutes from your previous meeting. Doing this will also give you a chance to send along any additional information your participants might need: if they’re new to web conferencing they will need to know how to connect to the conference and all participants will need your unique PIN , so be sure to include this.</p>
        <p>Start your web conference session ten to fifteen minutes before the web meeting is due to begin – this will give people time to arrive and familiarise themselves with the interface and features and also give you time to ensure everything is set up. Welcome each participant as they join and once the meeting begins ask all participants to introduce themselves. You should ask all participants to speak slowly and clearly to avoid miscommunication and this is particularly important if you have <a title="international conference call" href="<?php echo url_for('@international_conference_call'); ?>">international conference call</a> participants who might not understand local accents. Make sure everyone is able to see your desktop or presentation clearly and isn’t having any connection issues before beginning and keep the focus on your web conference by periodically asking your participants for input or questions.</p>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
