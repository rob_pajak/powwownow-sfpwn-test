<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">

        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
                'header' => '<span style="color:#e4242c;">Hate travel?</span> <span style="color:#384b51;">Try a conference call instead.</span>',
                'subHeader' => 'So you hate travel. Don\'t worry, you\'re not alone! Think of this as a cocoon of love for you, phone conference love. You\'re here because you hate travelling to meetings and want another option. Well we have it right here (and it\'s free)!',
                'hintBoxTitle' => "It's easy as 1, 2, 3!",
                'tabTitle' => 'Get started',
                'registrationSource' => $registrationSource,
            )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Conference Call with No Booking, No Billing, No Fuss</h3>
                <p style="text-align: justify;">Travelling to meetings can be frustrating and inefficient - so why not save time, money and the environment and have a conference call with Powwownow.</p>
                <p style="text-align: justify;"><strong>The Powwownow service is FREE!</strong> Callers only pay the cost of their own 0844 phone call which is added to their standard telecoms bill - nothing more.</p>
                <p style="text-align: justify;">Providing 24/7 availability and a host of free features including <a title="Powwownow Web" href="<?php url_for('@web_conferencing'); ?>">web conferencing</a>, call recording and a wide range of <a title="International Numbers and Rates" href="<?php url_for('@international_number_rates'); ?>">international access numbers</a>, there’s a reason why we're the UK’s leading free conference call provider.</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Love Hate Travel</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>