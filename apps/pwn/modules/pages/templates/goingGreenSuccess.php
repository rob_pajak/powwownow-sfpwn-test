<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Going Green'),
                'headingTitle' => __('Going Green'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <h3 class="lightgreen">Reduce your business’ carbon footprint with conference calls</h3>
            <p class="margin-bottom-20">Conference calling not only improves workplace efficiency and reduces costs for businesses; it also reduces CO2 emissions. By reducing the impact made on the environment by driving, flying and taking other forms of transport to meetings, conference calling is not only good for the environment, but good for business, as it allows you to promote your green credentials.</p>

            <h3 class="lightgreen">How exactly will going green help your business?</h3>
            <p>There is a huge awareness and a lot of buzz around the damage that humans are causing to the environment from CO2 emissions. Everyone wants to reduce their impact on the planet; therefore consumers are seeking out environmentally friendly businesses as it shows they hold similar values.</p>
            <p>By holding <a title="Conference Calls" href="<?php echo url_for('@conference_call'); ?>">conference calls</a> instead of face-to-face meetings, businesses can show their commitment to the environment and therefore, appeal more to their employees, clients, customers and stakeholders.</p>
            <p class="margin-bottom-20">So to avoid being left behind, improve your competitiveness by incorporating phone and web conferencing into your company policy. It will help strengthen your green credentials and support them with a solid sustainability foundation.</p>

            <h3 class="lightgreen">Calculate your carbon footprint</h3>
            <p>If you want to know how much carbon dioxide your company has generated in the past twelve months, use the Carbon Footprint Calculator. Just fill in the tabs giving details about your business consumption and the calculator will tell you how many tonnes of CO₂ you have emitted in the past year.</p>
            <p class="margin-bottom-10">Then try calculating your Carbon Footprint significantly reducing your flights and car/train/tube travel. From the moment you and your colleagues start using conference calls, you’ll clearly see the drop on your emissions!</p>
        </div>
        <div class="clearfix">
            <iframe style="overflow:scroll; margin:0; border:0;" src="http://calculator.carbonfootprint.com/calculator.aspx" width="710" height="740">
                <p>
                    Your browser doesn't support IFRAMES - please use the <a href="http://calculator.carbonfootprint.com/calculator.aspx" target="_blank">basic calculator</a> instead.
                </p>
            </iframe>
        </div>
        <div class="clearfix">
            <img title="SME Carbon Footprint" src="/sfimages/SME-Carbon-footprint.jpg" alt="SME Carbon Footprint" width="680" height="382"/>
        </div>
        <div class="clearfix">
            <h3 class="lightgreen">Follow in Powwownow’s footsteps</h3>
            <p>Powwownow's green commitment extends to other day-to-day operations as well. These are some of the things we do in our Richmond office:</p>
            <p>Like any company, Powwownow generates a significant amount of cardboard and paper waste. None of this waste goes to a landfill; rather it is collected by a recycling company - along with drinks cans, food tins, glass bottles, drinks cartons, and even plastic bottles. Printer components such as exhausted toner cartridges and image drums are notoriously hazardous for the environment. Powwownow are careful to only use printers whose manufacturers offer a recycling service for used parts, which are returned to the manufacturer rather than thrown away.</p>
            <p>Decommissioned hardware poses an awkward waste disposal problem. Ironically, the very servers which help to reduce CO2 emissions by making <a title="Audio Conferencing" href="<?php echo url_for('@audio_conferencing'); ?>">conference calling</a> possible pose a potential threat to the environment once they are replaced with more up-to-date technology and become surplus to requirements. Their unwanted servers and network equipment are given to Secure IT Recycling, one of the few companies licensed by the Environment Agency to collect and recycle WEEE (Waste Electrical and Electronic Equipment). After securely wiping all data, SITR resell any working equipment which is not obsolete and pass on a percentage of the resale value to the donating organisation. Anything else is broken down into its component materials for recycling.</p>
            <p>"Reduce, reuse, recycle" is a mantra that is drilled into school children across the country. This mantra is important to Powwownow: recycling is a complicated and labour-intensive process, so if something can be put to another use first and its working life extended, then it should be. This is the principle behind Freecycle (and, in a sense, eBay). Why should a perfectly good desk go to landfill when somebody is setting off (with that inevitable sense of dread) down the Purley Way to Ikea to buy a new desk? With this in mind, Powwownow came up with an ingenious solution to the growing stockpile of redundant desktops in the office. Rather than have them collected for a fee by SITR, we decided to give them to Powwownow employees in return for donations to Cancer Research UK via JustGiving. Through this scheme, Powwownow have so far given away six second-hand desktops at very reasonable prices, and raised GBP110 for charity. <a title="Web Conferencing" href="<?php echo url_for('@web_conferencing'); ?>">Web conferencing</a> provider Powwownow strives to continue its progress in lessening their environmental impact whilst helping good causes. They are a shining example of how to limit one's carbon footprint and other companies would do well to follow suit.</p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>