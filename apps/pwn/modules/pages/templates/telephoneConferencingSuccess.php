<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">


        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
            'header' => 'A Free Telephone Conferencing Service',
            'subHeader' => 'Powwownow\'s phone conferencing service is free - participants only pay the cost of their own call, which is added to their standard telecoms bill.',
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Powwownow Telephone Conferencing!</h3>
                <p style="text-align: justify;">Powwownow’s telephone conferencing service is free, easy to use and offers a host of features with no additional charges!&nbsp; These features include call recordings, a scheduler tool and web conferencing; enabling you to <a title="Free Web Conferencing" href="<?php echo url_for('@web_conferencing'); ?>">share your desktop</a> and present documents to conference participants.</p>
                <p style="text-align: justify;">As Powwownow is the only telecommunications provider in the UK offering a FREE telephone conferencing service, you can be guaranteed the best call quality at the lowest possible rate when connecting with clients and colleagues all over the world.</p>
                <p style="text-align: justify;">Need to conference on the go? Download our free <a title="iPhone Conference App" href="http://itunes.apple.com/us/app/powwownow/id359486614?mt=8" target="_blank">iPhone app</a> which allows you to schedule calls, set call reminders and join calls at the touch of a button from wherever you are.</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Telephone Conferencing</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>