<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Useful Links'),
                'headingTitle' => __('Useful Links'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <h3 class="rockwell">Visit our International sites</h3>
        <ul id="international-websites">
            <?php foreach ($regionLinks as $id => $region) : ?>
            <li><?php include_partial('pages/usefulLinksWebsiteList',array('website' => $region)); ?></li>
            <?php endforeach; ?>
        </ul>
        <h3 class="rockwell">Other links</h3>
        <ul id="other-websites">
            <li>
                <a target="_blank" href="http://www.allconferencecalls.co.uk/" class="flag-logo-all-conference-calls sprite-flag">
                    <img src="/sfimages/sprites/sprite-flags-round.png" alt="All Conference Calls"/>
                </a>&nbsp;
                <a class="grey flag-label" target="_blank" href="http://www.allconferencecalls.co.uk/">allconferencecalls UK</a>
            </li>
            <li>
                <a target="_blank" href="http://www.easytz.com/" class="flag-logo-easytz sprite-flag">
                    <img src="/sfimages/sprites/sprite-flags-round.png" alt="EasyTz"/>
                </a>&nbsp;
                <a class="grey flag-label" target="_blank" href="http://www.easytz.com/">EasyTZ</a>
            </li>
            <li>
                <a target="_blank" href="http://www.yuuguu.com/" class="flag-logo-yuuguu sprite-flag">
                    <img src="/sfimages/sprites/sprite-flags-round.png" alt="Yuuguu"/>
                </a>&nbsp;
                <a class="grey flag-label" target="_blank" href="http://www.yuuguu.com/">Yuuguu</a>
            </li>
            <li>
                <a target="_blank" href="http://www.startupbritain.org/">
                    <img class="flag-logo-non-sprite sprite-flag" src="/sfimages/start-up-britain.gif" alt="Start Up Britain"/>
                </a>&nbsp;
                <a class="grey flag-label" target="_blank" href="http://www.startupbritain.org/">Start Up Britain</a>
            </li>
        </ul>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
