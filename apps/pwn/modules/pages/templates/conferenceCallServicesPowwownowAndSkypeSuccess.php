<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'Skype')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Services', 'Skype'),'title' => __('Skype'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Skype'), 'headingSize' => 's', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>If you work in a modern office, you might find yourself constantly sat in front of a computer screen and not always within easy reach of a telephone. This can make conference calls impractical or downright impossible at times. Unfortunately many <a title="Conference Call Providers" href="<?php echo url_for('@conference_call_providers'); ?>">conference call providers</a> simply don’t have a solution to this problem, but thankfully Powwownow does! We have been partnered with Skype for a number of years and were the first conference call provider in Europe to do so. This means we are able to provide SkypeOut dial-in for our teleconferences, opening up our fantastic conference call service to the world.</p>
                <p>Using Skype’s VOIP (voice over internet protocol) solution, anyone with access to a computer with a microphone, an internet connection and Skype can dial-in to our <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>">conference calls</a>. You won’t need any additional teleconferencing equipment and the only programme you will need to have installed is the freely available Skype. Participants joining the conference call then simply have to type the SkypeOut dial-in number to the Skype keypad and enter the secure PIN, just like a regular telephone. This method also works for Smartphone users who have downloaded the Skype app for their handset. It simply couldn't be easier.</p>
                <p>Aside from being useful where there is limited access to landline phones, this solution is ideal for technical support employees who are using web conferencing to share desktops with customers or colleagues, for chairpersons who needs to share documents or presentations during a webinar, for international colleagues or clients who regularly need to call head office and for training purposes when new employees need walking through computer processes.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>