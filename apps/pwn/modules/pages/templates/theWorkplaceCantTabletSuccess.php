<div class="clearfix" id="page"><!-- column -->
<div class="position_content" id="page_position_content">
<div class="clearfix colelem" id="pu2430"><!-- group -->
    <div class="clearfix browser_width grpelem" id="u2430"><!-- column -->
        <div class="clearfix colelem" id="pu2644"><!-- group -->
            <a class="nonblock nontext clip_frame grpelem no-decoration" id="u2644" href="http://www.powwownow.co.uk/"><!-- image --><img class="block" id="u2644_img" src="/sfimages/the-workplace-cant/logo-big.png" alt="" width="197" height="59"/></a>
            <div class="pointer_cursor clearfix grpelem" id="u2646-9"><!-- content -->
                <a class="block" href="<?php echo url_for('@homepage'); ?>" target="_blank"></a>
                <p>Home&nbsp;&nbsp; |&nbsp;&nbsp; <a class="nonblock" href="<?php echo url_for('@conference_call') ?>" target="_blank">Conference Call </a>&nbsp; |&nbsp;&nbsp; <a class="nonblock" href="<?php echo url_for('@contact_us'); ?>" target="_blank">Contact Us</a></p>
            </div>
        </div>
        <img class="ose_pre_init colelem" id="u2432-4" src="/sfimages/the-workplace-cant/u2432-4.png" alt="The identity of the workplace" width="578" height="72"/><!-- rasterized frame -->
    </div>
    <div class="browser_width grpelem" id="u2431"><!-- simple frame --></div>
    <div class="clearfix grpelem" id="u2439"><!-- column -->
        <img class="colelem" id="u2442-4" src="/sfimages/the-workplace-cant/u2442-4.png" alt="SCROLL DOWN" width="350" height="27"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u2440"><!-- image -->
            <img class="block" id="u2440_img" src="/sfimages/the-workplace-cant/scroll-down-arrow.png" alt="" width="17" height="15"/>
        </div>
    </div>
</div>
<div class="clearfix colelem" id="pu2435"><!-- group -->
    <div class="clip_frame mse_pre_init" id="u2435"><!-- image -->
        <img class="block" id="u2435_img" src="/sfimages/the-workplace-cant/sarah-homepage.png" alt="" width="153" height="366"/>
    </div>
    <div class="clip_frame mse_pre_init" id="u2433"><!-- image -->
        <img class="block" id="u2433_img" src="/sfimages/the-workplace-cant/cant-word-bubble.png" alt="" width="532" height="223"/>
    </div>
    <img class="mse_pre_init ose_pre_init" id="u2437-4" src="/sfimages/the-workplace-cant/u2437-4.png" alt="An analysis of the most obstructive and unsympathetic people in the Great British workplace, based on national research in conjunction with OnePoll." width="415" height="108"/><!-- rasterized frame -->
</div>
<div class="clearfix colelem" id="pu2359"><!-- group -->
    <div class="clearfix browser_width grpelem" id="u2359"><!-- column -->
        <img class="colelem" id="u2360-7" src="/sfimages/the-workplace-cant/u2360-7.png" alt="The anatomy of a workplace ‘Can’t’ (i.e. an apathetic and unhelpful colleague) according to a poll of 2,000 UK&#45;based respondents: " width="536" height="196"/><!-- rasterized frame -->
        <div class="clearfix colelem" id="ppu3039"><!-- group -->
            <div class="clearfix grpelem" id="pu3039"><!-- column -->
                <div class="clearfix colelem" id="u3039"><!-- group -->
                    <img class="grpelem" id="u3023-4" src="/sfimages/the-workplace-cant/u3023-4.png" alt="Sarah" width="234" height="58"/><!-- rasterized frame -->
                    <img class="grpelem" id="u3024-6" src="/sfimages/the-workplace-cant/u3024-6.png" alt="The most common  Workplace ‘Can’t’ name" width="234" height="44"/><!-- rasterized frame -->
                </div>
                <div class="clip_frame colelem" id="u3032"><!-- image -->
                    <img class="block" id="u3032_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
                </div>
                <div class="clearfix colelem" id="u3112"><!-- group -->
                    <img class="grpelem" id="u3001-4" src="/sfimages/the-workplace-cant/u3001-4.png" alt="5’7" width="234" height="66"/><!-- rasterized frame -->
                    <img class="grpelem" id="u3006-6" src="/sfimages/the-workplace-cant/u3006-6.png" alt="Average height of  Workplace Can’t" width="235" height="57"/><!-- rasterized frame -->
                </div>
                <div class="clip_frame colelem" id="u3011"><!-- image -->
                    <img class="block" id="u3011_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
                </div>
                <div class="clearfix colelem" id="u3113"><!-- group -->
                    <img class="grpelem" id="u3002-4" src="/sfimages/the-workplace-cant/u3002-4.png" alt="31%" width="234" height="66"/><!-- rasterized frame -->
                    <img class="grpelem" id="u3007-4" src="/sfimages/the-workplace-cant/u3007-4.png" alt="Cited her as brunette" width="171" height="57"/><!-- rasterized frame -->
                </div>
            </div>
            <div class="clip_frame grpelem" id="u2361"><!-- image -->
                <img class="block" id="u2361_img" src="/sfimages/the-workplace-cant/sarah-slide-2.png" alt="" width="163" height="387"/>
            </div>
            <div class="clearfix grpelem" id="pu3117"><!-- column -->
                <div class="clearfix colelem" id="u3117"><!-- group -->
                    <img class="grpelem" id="u3003-4" src="/sfimages/the-workplace-cant/u3003-4.png" alt="44" width="160" height="67"/><!-- rasterized frame -->
                    <img class="grpelem" id="u3008-6" src="/sfimages/the-workplace-cant/u3008-6.png" alt="Average age of  Workplace Can’t" width="160" height="57"/><!-- rasterized frame -->
                </div>
                <div class="clip_frame colelem" id="u3015"><!-- image -->
                    <img class="block" id="u3015_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
                </div>
                <div class="clearfix colelem" id="u3126"><!-- group -->
                    <img class="grpelem" id="u3004-4" src="/sfimages/the-workplace-cant/u3004-4.png" alt="19%" width="160" height="66"/><!-- rasterized frame -->
                    <img class="grpelem" id="u3009-4" src="/sfimages/the-workplace-cant/u3009-4.png" alt="Based in HR Department" width="160" height="57"/><!-- rasterized frame -->
                </div>
                <div class="clip_frame colelem" id="u3017"><!-- image -->
                    <img class="block" id="u3017_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
                </div>
                <div class="clearfix colelem" id="u3150"><!-- group -->
                    <img class="grpelem" id="u3005-4" src="/sfimages/the-workplace-cant/u3005-4.png" alt="54%" width="160" height="67"/><!-- rasterized frame -->
                    <img class="grpelem" id="u3010-4" src="/sfimages/the-workplace-cant/u3010-4.png" alt="Said they were female" width="198" height="57"/><!-- rasterized frame -->
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix browser_width grpelem" id="u3163"><!-- column -->
        <img class="colelem" id="u3164-4" src="/sfimages/the-workplace-cant/u3164-4.png" alt="David" width="320" height="58"/><!-- rasterized frame -->
        <div class="clearfix colelem" id="ppu3166-4"><!-- group -->
            <div class="clearfix grpelem" id="pu3166-4"><!-- column -->
                <img class="colelem" id="u3166-4" src="/sfimages/the-workplace-cant/u3166-4.png" alt="25%" width="223" height="66"/><!-- rasterized frame -->
                <div class="clip_frame colelem" id="u3174"><!-- image -->
                    <img class="block" id="u3174_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
                </div>
                <img class="colelem" id="u3167-4" src="/sfimages/the-workplace-cant/u3167-4.png" alt="16%" width="223" height="66"/><!-- rasterized frame -->
            </div>
            <div class="clip_frame grpelem" id="u3194"><!-- image -->
                <img class="block" id="u3194_img" src="/sfimages/the-workplace-cant/david-2.png" alt="" width="156" height="388"/>
            </div>
            <div class="clearfix grpelem" id="pu3168-4"><!-- column -->
                <img class="colelem" id="u3168-4" src="/sfimages/the-workplace-cant/u3168-4.png" alt="13%" width="223" height="67"/><!-- rasterized frame -->
                <div class="clip_frame colelem" id="u3178"><!-- image -->
                    <img class="block" id="u3178_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
                </div>
                <img class="colelem" id="u3169-4" src="/sfimages/the-workplace-cant/u3169-4.png" alt="33%" width="223" height="66"/><!-- rasterized frame -->
            </div>
        </div>
    </div>
    <img class="grpelem" id="u3165-6" src="/sfimages/the-workplace-cant/u3165-6.png" alt="The most common name  for a male workplace Can’t" width="320" height="44"/><!-- rasterized frame -->
    <img class="grpelem" id="u3170-6" src="/sfimages/the-workplace-cant/u3170-6.png" alt="Cited him as having  grey hair" width="223" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u3171-6" src="/sfimages/the-workplace-cant/u3171-6.png" alt="Estimated his height as  being between 5'9 and 5'11" width="223" height="66"/><!-- rasterized frame -->
    <img class="grpelem" id="u3172-4" src="/sfimages/the-workplace-cant/u3172-4.png" alt="The majority said that he worked in their employer’s HR department" width="223" height="88"/><!-- rasterized frame -->
    <img class="grpelem" id="u3173-6" src="/sfimages/the-workplace-cant/u3173-6.png" alt="Guessed that he is aged  between 45 and 54" width="223" height="57"/><!-- rasterized frame -->
</div>
<div class="clearfix colelem" id="pu2443"><!-- group -->
    <div class="clearfix browser_width grpelem" id="u2443"><!-- group -->
        <img class="grpelem" id="u2444-5" src="/sfimages/the-workplace-cant/u2444-5.png" alt="The workplace ‘Can’t’ might be seen with: " width="671" height="126"/><!-- rasterized frame -->
    </div>
    <div class="clearfix browser_width grpelem" id="u2470"><!-- column -->
        <img class="colelem" id="u2471-4" src="/sfimages/the-workplace-cant/u2471-4.png" alt="From a list of personality traits, the top five qualities of the workplace ‘Can’t’ were: " width="668" height="180"/><!-- rasterized frame -->
        <div class="clearfix colelem" id="pu3342"><!-- group -->
            <div class="clearfix grpelem" id="u3342"><!-- column -->
                <div class="clip_frame ose_pre_init colelem" id="u2477"><!-- image -->
                    <img class="block" id="u2477_img" src="/sfimages/the-workplace-cant/47-piechart.png" alt="" width="160" height="159"/>
                </div>
                <div class="clip_frame ose_pre_init colelem" id="u2492"><!-- image -->
                    <img class="block" id="u2492_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
                </div>
                <img class="ose_pre_init colelem" id="u2485-4" src="/sfimages/the-workplace-cant/u2485-4.png" alt="Deceitful" width="107" height="34"/><!-- rasterized frame -->
            </div>
            <div class="clearfix grpelem" id="u3347"><!-- column -->
                <div class="clip_frame ose_pre_init colelem" id="u2479"><!-- image -->
                    <img class="block" id="u2479_img" src="/sfimages/the-workplace-cant/46-piechart.png" alt="" width="160" height="159"/>
                </div>
                <div class="clip_frame ose_pre_init colelem" id="u2494"><!-- image -->
                    <img class="block" id="u2494_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
                </div>
                <img class="ose_pre_init colelem" id="u2486-4" src="/sfimages/the-workplace-cant/u2486-4.png" alt="Unproductive" width="150" height="34"/><!-- rasterized frame -->
            </div>
            <div class="clearfix grpelem" id="u3348"><!-- column -->
                <div class="clip_frame ose_pre_init colelem" id="u2475"><!-- image -->
                    <img class="block" id="u2475_img" src="/sfimages/the-workplace-cant/59-piechart.png" alt="" width="160" height="159"/>
                </div>
                <div class="clip_frame ose_pre_init colelem" id="u2490"><!-- image -->
                    <img class="block" id="u2490_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
                </div>
                <img class="ose_pre_init colelem" id="u2484-4" src="/sfimages/the-workplace-cant/u2484-4.png" alt="Confident" width="108" height="34"/><!-- rasterized frame -->
            </div>
        </div>
        <div class="clearfix colelem" id="pu3374"><!-- group -->
            <div class="clearfix grpelem" id="u3374"><!-- column -->
                <div class="clip_frame ose_pre_init colelem" id="u2473"><!-- image -->
                    <img class="block" id="u2473_img" src="/sfimages/the-workplace-cant/65-piechart.png" alt="" width="160" height="159"/>
                </div>
                <div class="clip_frame ose_pre_init colelem" id="u2488"><!-- image -->
                    <img class="block" id="u2488_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
                </div>
                <img class="ose_pre_init colelem" id="u2483-4" src="/sfimages/the-workplace-cant/u2483-4.png" alt="Argumentative" width="167" height="34"/><!-- rasterized frame -->
            </div>
            <div class="clearfix grpelem" id="u3373"><!-- column -->
                <div class="clip_frame ose_pre_init colelem" id="u2481"><!-- image -->
                    <img class="block" id="u2481_img" src="/sfimages/the-workplace-cant/41-piechart.png" alt="" width="160" height="159"/>
                </div>
                <div class="clip_frame ose_pre_init colelem" id="u2496"><!-- image -->
                    <img class="block" id="u2496_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
                </div>
                <img class="ose_pre_init colelem" id="u2487-4" src="/sfimages/the-workplace-cant/u2487-4.png" alt="Committed " width="121" height="34"/><!-- rasterized frame -->
            </div>
        </div>
    </div>
</div>
<div class="clearfix colelem" id="pu2462"><!-- group -->
    <div class="clearfix mse_pre_init" id="u2462"><!-- column -->
        <div class="clip_frame colelem" id="u2463"><!-- image -->
            <img class="block" id="u2463_img" src="/sfimages/the-workplace-cant/sandals.png" alt="" width="219" height="208"/>
        </div>
        <img class="colelem" id="u2465-6" src="/sfimages/the-workplace-cant/u2465-6.png" alt="Sandals all  year round" width="213" height="50"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2445"><!-- column -->
        <div class="clip_frame colelem" id="u2447"><!-- image -->
            <img class="block" id="u2447_img" src="/sfimages/the-workplace-cant/horn-glasses.png" alt="" width="213" height="214"/>
        </div>
        <img class="colelem" id="u2446-4" src="/sfimages/the-workplace-cant/u2446-4.png" alt="Horn Rim Glasses" width="213" height="70"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2458"><!-- column -->
        <div class="clip_frame colelem" id="u2460"><!-- image -->
            <img class="block" id="u2460_img" src="/sfimages/the-workplace-cant/red-face.png" alt="" width="209" height="225"/>
        </div>
        <img class="colelem" id="u2459-4" src="/sfimages/the-workplace-cant/u2459-4.png" alt="Red Face" width="207" height="70"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2449"><!-- column -->
        <div class="clip_frame colelem" id="u2450"><!-- image -->
            <img class="block" id="u2450_img" src="/sfimages/the-workplace-cant/big-nose.png" alt="" width="209" height="222"/>
        </div>
        <img class="colelem" id="u2452-4" src="/sfimages/the-workplace-cant/u2452-4.png" alt="Large Nose" width="209" height="39"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2453"><!-- column -->
        <div class="clip_frame colelem" id="u2454"><!-- image -->
            <img class="block" id="u2454_img" src="/sfimages/the-workplace-cant/moustache.png" alt="" width="208" height="223"/>
        </div>
        <img class="colelem" id="u2456-4" src="/sfimages/the-workplace-cant/u2456-4.png" alt="Moustache" width="208" height="39"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2466"><!-- column -->
        <div class="clip_frame colelem" id="u2468"><!-- image -->
            <img class="block" id="u2468_img" src="/sfimages/the-workplace-cant/tattoo.png" alt="" width="208" height="208"/>
        </div>
        <img class="colelem" id="u2467-4" src="/sfimages/the-workplace-cant/u2467-4.png" alt="Tattoos" width="208" height="39"/><!-- rasterized frame -->
    </div>
</div>
<div class="clearfix colelem" id="pu2500"><!-- group -->
    <div class="clearfix browser_width grpelem" id="u2500"><!-- column -->
        <img class="colelem" id="u2501-5" src="/sfimages/the-workplace-cant/u2501-5.png" alt="A ‘Can’t’ Watchers Guide" width="587" height="170"/><!-- rasterized frame -->
        <img class="colelem" id="u2502-6" src="/sfimages/the-workplace-cant/u2502-6.png" alt="According to respondents in each area, the identities  of common workplace Can’ts across the UK." width="687" height="62"/><!-- rasterized frame -->
    </div>
    <div class="clearfix browser_width grpelem" id="u2593"><!-- column -->
        <img class="colelem" id="u2943-4" src="/sfimages/the-workplace-cant/u2943-4.png" alt="How us Brits consider dealing with the workplace Can’t:" width="619" height="162"/><!-- rasterized frame -->
        <div class="clearfix colelem" id="u3748"><!-- column -->
            <div class="clearfix colelem" id="u3687"><!-- group -->
                <div class="clip_frame grpelem" id="u3688"><!-- image -->
                    <img class="block" id="u3688_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3690-6" src="/sfimages/the-workplace-cant/u3690-6.png" alt="40%
Scream and shout silently or in private, out of sheer frustration" width="394" height="144"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3691"><!-- group -->
                <div class="clip_frame grpelem" id="u3692"><!-- image -->
                    <img class="block" id="u3692_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3694-10" src="/sfimages/the-workplace-cant/u3694-10.png" alt="39%
Change the workplace  seating plan to be further  away from the Can’t " width="396" height="155"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3695"><!-- group -->
                <div class="clip_frame grpelem" id="u3697"><!-- image -->
                    <img class="block" id="u3697_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3696-6" src="/sfimages/the-workplace-cant/u3696-6.png" alt="36%
Seek new employment " width="400" height="143"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3699"><!-- group -->
                <div class="clip_frame grpelem" id="u3700"><!-- image -->
                    <img class="block" id="u3700_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3702-10" src="/sfimages/the-workplace-cant/u3702-10.png" alt="30%
Approach the Can’ts superior  in an attempt to cut him/her out  of work processes and protocol " width="396" height="155"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3703"><!-- group -->
                <div class="clip_frame grpelem" id="u3705"><!-- image -->
                    <img class="block" id="u3705_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3704-8" src="/sfimages/the-workplace-cant/u3704-8.png" alt="22%
Change profession or  career path entirely " width="396" height="143"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3707"><!-- group -->
                <div class="clip_frame grpelem" id="u3708"><!-- image -->
                    <img class="block" id="u3708_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3710-8" src="/sfimages/the-workplace-cant/u3710-8.png" alt="22%
Compete with that  person professionally " width="400" height="143"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3711"><!-- group -->
                <div class="clip_frame grpelem" id="u3712"><!-- image -->
                    <img class="block" id="u3712_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3714-8" src="/sfimages/the-workplace-cant/u3714-8.png" alt="22%
Consider working from  home/remote working " width="396" height="143"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3715"><!-- group -->
                <div class="clip_frame grpelem" id="u3717"><!-- image -->
                    <img class="block" id="u3717_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3716-8" src="/sfimages/the-workplace-cant/u3716-8.png" alt="20%
Call in sick in a bid to avoid  a meeting with the Can’t " width="395" height="143"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3719"><!-- group -->
                <div class="clip_frame grpelem" id="u3720"><!-- image -->
                    <img class="block" id="u3720_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3722-8" src="/sfimages/the-workplace-cant/u3722-8.png" alt="18%
Seek advice or buy tools/instructional material in a  bid to ‘bust stress’ " width="396" height="155"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3723"><!-- group -->
                <div class="clip_frame grpelem" id="u3724"><!-- image -->
                    <img class="block" id="u3724_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3726-6" src="/sfimages/the-workplace-cant/u3726-6.png" alt="15%
Consider counselling " width="396" height="119"/><!-- rasterized frame -->
            </div>
            <div class="clearfix colelem" id="u3727"><!-- group -->
                <div class="clip_frame grpelem" id="u3728"><!-- image -->
                    <img class="block" id="u3728_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
                </div>
                <img class="grpelem" id="u3730-6" src="/sfimages/the-workplace-cant/u3730-6.png" alt="12%
Relocate or move away " width="396" height="119"/><!-- rasterized frame -->
            </div>
        </div>
    </div>
    <div class="clearfix browser_width grpelem" id="u2648"><!-- column -->
        <div class="clip_frame colelem" id="u2649"><!-- image -->
            <img class="block" id="u2649_img" src="/sfimages/the-workplace-cant/big-logo.png" alt="" width="357" height="107"/>
        </div>
        <a class="nonblock nontext colelem no-decoration" id="u2651-4" href="http://www.powwownow.co.uk/"><!-- rasterized frame --><img id="u2651-4_img" src="/sfimages/the-workplace-cant/u2651-4.png" alt="www.powwownow.co.uk" width="677" height="117"/></a>
        <div class="colelem" id="u3502"><!-- custom html -->
            <span class="st_facebook_large">
                <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                    <a href="http://www.facebook.com/sharer.php?u=www.powwownow.co.uk/Conference-Call/The-Workplace-Cant" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - Facebook/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/facebook_32.png" alt="Facebook Share"/></a>
                </span>
            </span>
            <span class="st_twitter_large">
                <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                    <a href="http://twitter.com/share?text=The%20identity%20of%20the%20workplace%20Can%27t%20via%20%40Powwownow%20%23iamacan" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - Twitter/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/twitter_32.png" alt="Twitter Share"/></a>
                </span>
            </span>
            <span class="st_linkedin_large">
                <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fwww.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant&amp;title=The%20Workplace%20Can%E2%80%99t%20%E2%80%93%20Be%20a%20Can%20with%20Powwownow%2C%20Let%E2%80%99s%20Get%20it%20Done&amp;summary=We%20completed%20a%20survey%20in%20association%20with%20OnePoll%20to%20find%20out%20the%20identity%20of%20%E2%80%98Can%E2%80%99ts%E2%80%99%20within%20the%20UK.%20Find%20out%20the%20characteristics%20of%20a%20Can%E2%80%99t%20and%20where%20in%20the%20UK%20to%20find%20them" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - LinkedIn/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/linkedin_32.png" alt="LinkedIn Share"/></a>
                </span>
            </span>
            <span class="st_googleplus_large">
                <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                    <a href="https://plus.google.com/share?url=www.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - GooglePlus/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/googleplus_32.png" alt="Google+ Share"/></a>
                </span>
            </span>
            <span class="st_email_large">
                <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                    <a href="mailto:?subject=The%20Workplace%20Can%E2%80%99t%20%E2%80%93%20Be%20a%20Can%20with%20Powwownow%2C%20Let%E2%80%99s%20Get%20it%20Done&amp;body=www.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant" onclick="dataLayer.push({'event': 'WorkplaceCant - Share - Email/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/email_32.png" alt="Email Share"/></a>
                </span>
            </span>
        </div>
    </div>
</div>
<div class="clearfix colelem" id="pu2881"><!-- group -->
    <div class="clearfix mse_pre_init" id="u2881"><!-- column -->
        <img class="colelem" id="u2882-4" src="/sfimages/the-workplace-cant/u2882-4.png" alt="Northern Ireland" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2883-6" src="/sfimages/the-workplace-cant/u2883-6.png" alt="Male, named Peter/Thomas  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2875"><!-- column -->
        <img class="colelem" id="u2876-4" src="/sfimages/the-workplace-cant/u2876-4.png" alt="North East" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2877-4" src="/sfimages/the-workplace-cant/u2877-4.png" alt="Female, named Sophie/Elizabeth/Victoria (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2878"><!-- column -->
        <img class="colelem" id="u2880-4" src="/sfimages/the-workplace-cant/u2880-4.png" alt="North West" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2879-6" src="/sfimages/the-workplace-cant/u2879-6.png" alt="Female, named Emily,  working in Customer Services" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2872"><!-- column -->
        <img class="colelem" id="u2873-4" src="/sfimages/the-workplace-cant/u2873-4.png" alt="Scotland" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2874-6" src="/sfimages/the-workplace-cant/u2874-6.png" alt="Female, named Elizabeth,  working in Customer Services" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2884"><!-- column -->
        <img class="colelem" id="u2885-6" src="/sfimages/the-workplace-cant/u2885-6.png" alt="Yorkshire
and Humberside" width="320" height="72"/><!-- rasterized frame -->
        <img class="colelem" id="u2886-6" src="/sfimages/the-workplace-cant/u2886-6.png" alt="Male, named Paul/John  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2887"><!-- column -->
        <img class="colelem" id="u2889-4" src="/sfimages/the-workplace-cant/u2889-4.png" alt="West Midlands" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2888-6" src="/sfimages/the-workplace-cant/u2888-6.png" alt="Female, named Julia,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clip_frame mse_pre_init" id="u2594"><!-- image -->
        <img class="block" id="u2594_img" src="/sfimages/the-workplace-cant/office-cant-slide-7.png" alt="" width="144" height="269"/>
    </div>
    <div class="clip_frame mse_pre_init" id="u2504"><!-- image -->
        <img class="block" id="u2504_img" src="/sfimages/the-workplace-cant/blank-map-2.png" alt="" width="588" height="711"/>
    </div>
    <div class="clip_frame mse_pre_init" id="u2596"><!-- image -->
        <img class="block" id="u2596_img" src="/sfimages/the-workplace-cant/office-scene225x264.png" alt="" width="225" height="263"/>
    </div>
    <div class="clearfix mse_pre_init" id="u2863"><!-- column -->
        <img class="colelem" id="u2865-4" src="/sfimages/the-workplace-cant/u2865-4.png" alt="London" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2864-6" src="/sfimages/the-workplace-cant/u2864-6.png" alt="Female, named Julia,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2857"><!-- column -->
        <img class="colelem" id="u2859-4" src="/sfimages/the-workplace-cant/u2859-4.png" alt="Wales" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2858-6" src="/sfimages/the-workplace-cant/u2858-6.png" alt="Female, named Sarah/Beth/Lucy  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2860"><!-- column -->
        <img class="colelem" id="u2862-4" src="/sfimages/the-workplace-cant/u2862-4.png" alt="East Anglia" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2861-6" src="/sfimages/the-workplace-cant/u2861-6.png" alt="Female, named Sarah,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2854"><!-- column -->
        <img class="colelem" id="u2855-4" src="/sfimages/the-workplace-cant/u2855-4.png" alt="East Midlands" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2856-6" src="/sfimages/the-workplace-cant/u2856-6.png" alt="Male, named David, working  in Customer Services" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2866"><!-- column -->
        <img class="colelem" id="u2867-4" src="/sfimages/the-workplace-cant/u2867-4.png" alt="South East" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2868-6" src="/sfimages/the-workplace-cant/u2868-6.png" alt="Female, named Emma,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix mse_pre_init" id="u2869"><!-- column -->
        <img class="colelem" id="u2870-4" src="/sfimages/the-workplace-cant/u2870-4.png" alt="South West" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u2871-6" src="/sfimages/the-workplace-cant/u2871-6.png" alt="Female, named Emily/Sarah  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
</div>
<div class="verticalspacer"></div>
<div class="clearfix ose_pre_init mse_pre_init" id="u2503-3"><!-- content -->
    <p>&nbsp;</p>
</div>
</div>
</div>
<!-- JS includes -->
<script type="text/javascript">
    if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
<script type="text/javascript">
    window.jQuery || document.write('\x3Cscript src="/sfjs/pages/the-workplace-cant/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
<script src="/sfjs/pages/the-workplace-cant/museutils.js?4291592202" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.scrolleffects.js?4006931061" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.tobrowserwidth.js?3842421675" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.watch.js?4068933136" type="text/javascript"></script>
<!-- Other scripts -->
<script type="text/javascript">
    $(document).ready(function() { try {
        Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
        $('.browser_width').toBrowserWidth();/* browser width elements */
        Muse.Utils.prepHyperlinks(true);/* body */
        $('#u2432-4').registerOpacityScrollEffect([{"in":[-Infinity,0],"opacity":0,"fade":50},{"in":[0,0],"opacity":100},{"in":[0,Infinity],"opacity":0,"fade":50}]);/* scroll effect */
        $('#u2435').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[0,0]},{"in":[0,Infinity],"speed":[-1,0]}]);/* scroll effect */
        $('#u2433').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[0,0]},{"in":[0,Infinity],"speed":[1,0]}]);/* scroll effect */
        $('#u2437-4').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[0,0]},{"in":[0,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u2437-4').registerOpacityScrollEffect([{"in":[-Infinity,0],"opacity":0,"fade":353},{"in":[0,0],"opacity":100},{"in":[0,Infinity],"opacity":0,"fade":130}]);/* scroll effect */
        $('#u2477').registerOpacityScrollEffect([{"in":[-Infinity,5233.2],"opacity":0,"fade":297.2},{"in":[5233.2,5233.2],"opacity":100},{"in":[5233.2,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u2492').registerOpacityScrollEffect([{"in":[-Infinity,5234.5],"opacity":0,"fade":298.5},{"in":[5234.5,5234.5],"opacity":100},{"in":[5234.5,Infinity],"opacity":100,"fade":215.15}]);/* scroll effect */
        $('#u2485-4').registerOpacityScrollEffect([{"in":[-Infinity,5222.55],"opacity":0,"fade":286.55},{"in":[5222.55,5222.55],"opacity":100},{"in":[5222.55,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u2479').registerOpacityScrollEffect([{"in":[-Infinity,5233.2],"opacity":0,"fade":297.2},{"in":[5233.2,5233.2],"opacity":100},{"in":[5233.2,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u2494').registerOpacityScrollEffect([{"in":[-Infinity,5234.5],"opacity":0,"fade":298.5},{"in":[5234.5,5234.5],"opacity":100},{"in":[5234.5,Infinity],"opacity":100,"fade":215.15}]);/* scroll effect */
        $('#u2486-4').registerOpacityScrollEffect([{"in":[-Infinity,5222.55],"opacity":0,"fade":286.55},{"in":[5222.55,5222.55],"opacity":100},{"in":[5222.55,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u2475').registerOpacityScrollEffect([{"in":[-Infinity,5233.2],"opacity":0,"fade":297.2},{"in":[5233.2,5233.2],"opacity":100},{"in":[5233.2,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u2490').registerOpacityScrollEffect([{"in":[-Infinity,5234.5],"opacity":0,"fade":298.5},{"in":[5234.5,5234.5],"opacity":100},{"in":[5234.5,Infinity],"opacity":100,"fade":215.15}]);/* scroll effect */
        $('#u2484-4').registerOpacityScrollEffect([{"in":[-Infinity,5222.55],"opacity":0,"fade":286.55},{"in":[5222.55,5222.55],"opacity":100},{"in":[5222.55,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u2473').registerOpacityScrollEffect([{"in":[-Infinity,5477.2],"opacity":0,"fade":165.2},{"in":[5477.2,5477.2],"opacity":100},{"in":[5477.2,Infinity],"opacity":100,"fade":134.8}]);/* scroll effect */
        $('#u2488').registerOpacityScrollEffect([{"in":[-Infinity,5610.7],"opacity":0,"fade":298.7},{"in":[5610.7,5610.7],"opacity":100},{"in":[5610.7,Infinity],"opacity":100,"fade":1.3}]);/* scroll effect */
        $('#u2483-4').registerOpacityScrollEffect([{"in":[-Infinity,5600.3],"opacity":0,"fade":288.3},{"in":[5600.3,5600.3],"opacity":100},{"in":[5600.3,Infinity],"opacity":100,"fade":11.7}]);/* scroll effect */
        $('#u2481').registerOpacityScrollEffect([{"in":[-Infinity,5477.2],"opacity":0,"fade":165.2},{"in":[5477.2,5477.2],"opacity":100},{"in":[5477.2,Infinity],"opacity":100,"fade":134.8}]);/* scroll effect */
        $('#u2496').registerOpacityScrollEffect([{"in":[-Infinity,5612],"opacity":0,"fade":305.85},{"in":[5612,5612],"opacity":100},{"in":[5612,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u2487-4').registerOpacityScrollEffect([{"in":[-Infinity,5600.45],"opacity":0,"fade":288.45},{"in":[5600.45,5600.45],"opacity":100},{"in":[5600.45,Infinity],"opacity":100,"fade":11.55}]);/* scroll effect */
        $('#u2462').registerPositionScrollEffect([{"in":[-Infinity,4132.3],"speed":[0.8,0]},{"in":[4132.3,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2445').registerPositionScrollEffect([{"in":[-Infinity,3345.5],"speed":[0.8,0]},{"in":[3345.5,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2458').registerPositionScrollEffect([{"in":[-Infinity,3738.9],"speed":[0.8,0]},{"in":[3738.9,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2449').registerPositionScrollEffect([{"in":[-Infinity,3345.5],"speed":[-0.8,0]},{"in":[3345.5,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2453').registerPositionScrollEffect([{"in":[-Infinity,3738.9],"speed":[-0.8,0]},{"in":[3738.9,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2466').registerPositionScrollEffect([{"in":[-Infinity,4132.3],"speed":[-0.8,0]},{"in":[4132.3,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2881').registerPositionScrollEffect([{"in":[-Infinity,7600],"speed":[1,0]},{"in":[7600,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2875').registerPositionScrollEffect([{"in":[-Infinity,7807.72],"speed":[1,0]},{"in":[7807.72,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2878').registerPositionScrollEffect([{"in":[-Infinity,8015.44],"speed":[1,0]},{"in":[8015.44,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2872').registerPositionScrollEffect([{"in":[-Infinity,8223.16],"speed":[1,0]},{"in":[8223.16,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2884').registerPositionScrollEffect([{"in":[-Infinity,8430.88],"speed":[1,0]},{"in":[8430.88,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2887').registerPositionScrollEffect([{"in":[-Infinity,8638.6],"speed":[1,0]},{"in":[8638.6,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2594').registerPositionScrollEffect([{"in":[-Infinity,10563.92],"speed":[1,0]},{"in":[10563.92,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u2504').registerPositionScrollEffect([{"in":[-Infinity,7179.4],"speed":[0,1]},{"in":[7179.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u2596').registerPositionScrollEffect([{"in":[-Infinity,10218.49],"speed":[1.5,0]},{"in":[10218.49,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u2863').registerPositionScrollEffect([{"in":[-Infinity,7600],"speed":[-1,0]},{"in":[7600,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2857').registerPositionScrollEffect([{"in":[-Infinity,7807.72],"speed":[-1,0]},{"in":[7807.72,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2860').registerPositionScrollEffect([{"in":[-Infinity,8015.44],"speed":[-1,0]},{"in":[8015.44,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2854').registerPositionScrollEffect([{"in":[-Infinity,8223.16],"speed":[-1,0]},{"in":[8223.16,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2866').registerPositionScrollEffect([{"in":[-Infinity,8430.88],"speed":[-1,0]},{"in":[8430.88,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2869').registerPositionScrollEffect([{"in":[-Infinity,8638.6],"speed":[-1,0]},{"in":[8638.6,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2503-3').registerPositionScrollEffect([{"in":[-Infinity,35383.77],"speed":[1,0]},{"in":[35383.77,Infinity],"speed":[0,1]}]);/* scroll effect */
        $('#u2503-3').registerOpacityScrollEffect([{"in":[-Infinity,35383.77],"opacity":0,"fade":50},{"in":[35383.77,35383.77],"opacity":100},{"in":[35383.77,Infinity],"opacity":0,"fade":50}]);/* scroll effect */
        Muse.Utils.fullPage('#page');/* 100% height page */
        Muse.Utils.showWidgetsWhenReady();/* body */
        Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
    } catch(e) { Muse.Assert.fail('Error calling selector function:' + e); }});
</script>