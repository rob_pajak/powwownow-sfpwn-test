<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('How We Are Different'),
                'headingTitle' => __('Easy, Low-Cost Conference Calling'),
                'headingSize' => 'xxl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <h3 class="lightgreen">There has never been a product like it</h3>
                <ul class="chevron-green">
                    <li class="png">Affordable. Only pay the cost of your own phone call - that's it! No contracts and no bills.</li>
                    <li class="png">Simple to use. Don't worry about explaining to people how to use it. The simple 3-stage process makes it all perfectly clear.</li>
                    <li class="png">It saves so much time. Why travel to a meeting when you can do it over the phone? Don't set a meeting for next week. Have a Powwow - now!</li>
                </ul>
            <h3 class="lightgreen">For business people with more sense than money...</h3>
            <p>At Powwownow, we don't want our customers doing things the hard way. That's why we offer you <a title="Conference Call Service" href="<?php echo url_for('@conference_call_services'); ?>">immediate conference calls</a>, available 24/7, with as many people as you want speaking at an affordable price - we don't even need to know your name. Here are some of the ways we are different from the competition:</p>
        </div>
        <div class="clearfix">
            <table class="rockwell" id="tableHowWeAreDifferent">
                <thead>
                    <tr>
                        <th>POWWOWNOW</th>
                        <th>OTHER PROVIDERS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count = 1; ?>
                    <?php foreach ($tableArr as $info) : ?>
                        <tr class="<?php echo (1==$count) ? 'odd' : 'even'; ?>">
                            <td><?php echo $info['pwn']; ?></td>
                            <td><?php echo $info['other']; ?></td>
                        </tr>
                        <?php $count = (2==$count) ? 1 : 2 ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix">
            <p>*These do not apply to Powwownow <a title="Powwownow Premium" href="<?php echo url_for('@premium_service'); ?>">business packages</a> where customers opt for a competitively priced contract service</p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
