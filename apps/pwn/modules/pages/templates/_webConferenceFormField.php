<div class="grid_4">
    <?php echo $form[$field]->renderLabel(); ?>
</div>
<?php if ($field == 'company'):  ?>
<div class="grid_17">
<?php else: ?>
<div class="grid_7">
<?php endif; ?>
    <span class="mypwn-input-container"<?php if ($field == 'email') echo ' style="z-index:1000;"';?>>
        <?php echo $form[$field]->render(array('class' => 'input-medium font-small mypwn-input')); ?>
    </span>
</div>
