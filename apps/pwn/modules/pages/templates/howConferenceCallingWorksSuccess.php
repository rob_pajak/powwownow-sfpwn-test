<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('How Conference Calling Works'),
                'headingTitle' => __('How Conference Calling Works'),
                'headingSize' => 'xl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <h3 class="green rockwell">Try it now in 3 easy steps</h3>
            <div id="pwn-vid-3-steps">
                <?php include_component(
                    'commonComponents',
                    'hTML5OutputVideo',
                    array(
                        'config' => array(
                            'width'    => 460,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true
                        ),
                        'video'  => array(
                            'image' => '/sfimages/video-stills/3_easy_steps_video_still.jpg',
                            'mp4'   => '/sfvideo/pwn_3steps460x260_LR.mp4',
                            'webm'  => '/sfvideo/pwn_3steps460x260_LR.webm',
                            'ogg'   => '/sfvideo/pwn_3steps460x260_LR.ogv'
                        ),
                        'called' => 'html'
                    )
                ); ?>
            </div>
            <p>To Hold a conference call with <a title="conference calls" href="<?php echo url_for('@conference_call'); ?>">Powwownow</a>, follow these simple instructions:</p>

            <h3 class="green rockwell">Getting started</h3>
            <ol class="getting-started-list">
                <li>
                    <p>Generate your PIN. Just type in your email address and one will be instantly generated for you.</p>
                </li>
                <li>
                    <p>Tell your conference call participants what they need to know:</p>
                    <ul class="chevron-green">
                        <li>The time of the call</li>
                        <li>The Powwownow dial-in number</li>
                        <li>Your PIN</li>
                    </ul>
                </li>
                <li>
                    <p>Have a Powwow!</p>
                </li>
            </ol>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="Holding-A-Powwownow">Holding a Powwownow</h3>
            <ol class="holding-powwownow-list">
                <li>
                    <p>
                        At the agreed time, all dial 0844 4 73 73 73, or from a UK mobile use the shortcode 87373,
                        <span>for 12.5p a min + VAT</span>.
                        (<a title="International Dial-in Numbers" href="http://pdf.powwownow.com/pdf/GBR_en_Dial-In-Numbers.pdf">Download</a> international
                        dial-in numbers)
                    </p>
                </li>
                <li>
                    <p>
                        You will be asked for your PIN and then your name. If you are the first person to arrive on the conference call, you will hear music. As others arrive on the call, you will hear them being announced. When there are at least two people on the call you are ready to start talking!
                    </p>
                </li>
                <li>
                    <p>
                        When you have finished your conference call, simply hang up. As each person hangs up, you will hear their name announced. When the last person hangs up, the conference call ends.
                    </p>
                </li>
            </ol>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="In-Conference-Controls">In-conference controls</h3>
            <div id="pwn-vid-conference-controls">
                <?php include_component(
                    'commonComponents',
                    'hTML5OutputVideo',
                    array(
                        'config' => array(
                            'width'    => 460,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true
                        ),
                        'video'  => array(
                            'image' => '/sfimages/video-stills/In_Conf_Controls_Video_Still.jpg',
                            'mp4'   => '/sfvideo/pwn_inConfCont460x260_LR.mp4',
                            'webm'  => '/sfvideo/pwn_inConfCont460x260_LR.webm',
                            'ogg'   => '/sfvideo/pwn_inConfCont460x260_LR.ogv'
                        ),
                        'called' => 'html'
                    )
                ); ?>
            </div>
            <p>Take advantage of the following control keys available during your conference:</p>
            <p><strong># = Skip Intro:</strong><br/>Skips name recording and PIN playback when joining the call to make the process faster</p>
            <p><strong>#6 = Mute:</strong><br/>By pressing #6, an individual can mute and un-mute his/her handset</p>
            <p><strong>#1 = Head Count:</strong><br/>Allows you to review the number of people on the conference call</p>
            <p><strong>#2 = Roll Call:</strong><br/>This is a replay of all names recorded when people arrived on the conference call. All participants will hear the number of people and the roll call.</p>
            <p><strong>#3 = Lock:</strong><br/>Allows you to lock and unlock a conference call</p>
            <p><strong>#8 = Record:</strong><br/>Allows you to record a conference. To start the recording, press #8. (You will be asked to confirm this by pressing 1). To stop and save the recording, press #8 again and confirm or just hang up the phone.</p>
            <p>Also available to Powwownow <a title="premium" href="<?php echo url_for('@premium_service'); ?>">Premium</a> users:</p>
            <p>Your saved recordings will appear in <a title="myRecordings" href="<?php echo url_for('@recordings'); ?>">myPowwownow</a> a few minutes after your call ends, identified by the conference date and time. Here you can play the recordings or publish and share them with whoever you wish. We'll hold recordings for 60 days, or up to 6 months if published.</p>
            <p><strong>## = Mute All:</strong><br/>Enables the Chairperson to mute/un-mute all their participants</p>
            <p><strong>#7 = Private Roll Call:</strong><br/>Allows the Chairperson to hear who is on the call without participants hearing</p>
            <p><strong>#9 = Private Head Count:</strong><br/>Allows the Chairperson to hear how many people are on the call without the participants hearing</p>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="myPowwownow">myPowwownow</h3>
            <div id="pwn-vid-mypowwownow">
                <?php include_component(
                    'commonComponents',
                    'hTML5OutputVideo',
                    array(
                        'config' => array(
                            'width'    => 460,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true
                        ),
                        'video'  => array(
                            'image' => '/sfimages/video-stills/myPowwownow_Video_Still.jpg',
                            'mp4'   => '/sfvideo/pwn_myPWN460x260_LR.mp4',
                            'webm'  => '/sfvideo/pwn_myPWN460x260_LR.webm',
                            'ogg'   => '/sfvideo/pwn_myPWN460x260_LR.ogv'
                        ),
                        'called' => 'html'
                    )
                ); ?>
            </div>
            <p>myPowwownow is your personal account area where you can manage your call settings, use the scheduler tool, access call recordings and much more.</p>
            <p>To access myPowwownow simply
                <a title="Login" href="<?php echo url_for('@login'); ?>">log in</a> with your registered email address and password. If you don't have a password yet,
                <a title="Create a Login" href="<?php echo url_for('@create_a_login'); ?>">click here</a>.
                If you have forgotten your password, you can
                <a title="Forgotten Password" href="<?php echo url_for('@forgotten_password'); ?>">reset it here</a>.
            </p>
            <p>In myPowwownow you will be able to:</p>
            <ul class="chevron-green">
                <li>Request a free wallet-sized reminder card with your details on it: PIN, dial-in numbers etc</li>
                <li>Schedule a conference using the
                    <?php include_component(
                        'commonComponents',
                        'outlookPluginTooltip',
                        array(
                            'title' => 'Outlook Plugin',
                            'id'    => 'bottom-outlook-link',
                            'css'   => array(
                                'left'       => '135px',
                                'margin-top' => '10px'
                            )
                        )
                    ); ?>
                    or the <a title="myScheduler" href="http://myscheduler.powwownow.com/">Scheduler Tool</a> - handy
                    tools to organize your conference calls and invite your participants
                </li>
                <li>Manage your call settings such as voice prompt language and the music on hold</li>
                <li>Access recordings - listen, download and share your saved conference call recordings</li>
                <li>Download Powwownow Web, so you can share your desktop and present documents while on your conference call</li>
                <li>Change your hold music - Bored of your on hold music? Log in to myPowwownow and select your preferred tune!</li>
                <li>Plus much more!</li>
            </ul>
            <p>Also available to <a title="Powwownow Premium" href="<?php echo url_for('@premium_service'); ?>">Powwownow Premium</a> users:</p>
            <ul class="chevron-green">
                <li>Reports &ndash; generate reports on all PINs created and used plus on calls made on particular dates</li>
                <li>Time-limited PINs - for booking high level or sensitive audio conferences</li>
            </ul>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="User-Guides">User guides</h3>
            <p class="margin-bottom-10">For detailed step-by-step user guides, simply click on the links below for printer-friendly summaries that you can keep to hand.</p>
            <ul class="chevron-green">
                <li><a title="Powwownow User Guide" href="/sfpdf/en/Powwownow-User-Guide.pdf" target="_blank">Conference calling user guide</a></li>
                <li><a title="Premium Admin User Guide" href="/sfpdf/en/Powwownow-Premium-User-Guide-For-Admins.pdf" target="_blank">Premium administrator user guide</a></li>
                <li><a title="Premium User Guide" href="/sfpdf/en/Powwownow-Premium-User-Guide-For-Users.pdf" target="_blank">Premium user guide</a></li>
                <li><a title="International Dial-in Numbers" href="/sfpdf/en/Powwownow-Dial-in-Numbers.pdf" target="_blank">International dial in numbers</a></li>
            </ul>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell">Tips:</h3>
            <p class="margin-bottom-10">Here are some helpful tips to enhance your conference calls.</p>
            <ul class="chevron-green">
                <li><a title="Top 10 Tips for a Conference Call" href="<?php echo url_for('@top_ten_tips_for_conference_calling'); ?>">Top 10 tips for a conference call</a></li>
                <li><a title="Conference Call Headsets" href="<?php echo url_for('@conference_call_headsets'); ?>">Conference call headsets</a></li>
                <li><a title="Conference Call Etiquette" href="<?php echo url_for('@conference_call_etiquette'); ?>">Conference call etiquette</a></li>
                <li><a title="How to Conference Call" href="<?php echo url_for('@conference_call_how_to_conference_call'); ?>">How to conference call</a></li>
                <li><a title="Set up a Conference Call" href="<?php echo url_for('@conference_call_set_up_a_conference_call'); ?>">Set up a conference call</a></li>
                <li><a title="Conference Call Time Zones" href="<?php echo url_for('@conference_call_time_zones'); ?>">Conference call time zones</a></li>
            </ul>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
