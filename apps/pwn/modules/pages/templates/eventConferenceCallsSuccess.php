<?php use_helper('DataUri') ?>
<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Event Conference Calls'),
                'headingTitle' => __('Powwownow Event Calls'),
                'headingSize' => 'l',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <h3 class="green rockwell">Quality of communication is vital in today's competitive environment.</h3>
            <p>Whether it’s announcing financial results to investors, launching a product or keeping staff informed, how you communicate makes a difference.
               Our Operator Assisted Service offers fully customised solutions, adapted to your specific requirements, depending on your business department
               or profession.
            </p>
        </div>

        <div class="margin-top-15px">
            <div class="lozenge-green rockwell">
                <p>Talk to our specialists on 0800 229 0700</p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Easy Accessibility</span><br/>
                    Have all caller lines answered by an operator or enable pin-code access.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Global Reach</span><br>
                    Participants from any country can access the call with over 100 Local and Freephone numbers.
                </p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Increased Productivity</span><br>
                    Enable faster decision making by reaching a wide audience and gaining instant feedback.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">High-level support</span><br>
                    With the planning, support and guidance of the Event Management Team.
                </p>
            </div>
        </div>

        <div class="margin-top-32px">
            <?php include_component(
                'commonComponents',
                'genericHeading',
                array(
                    'title' => __('What would I use this service for?'),
                    'headingSize' => 'xl',
                    'user_type' => 'powwownow')
            ); ?>
        </div>

        <div class="clearfix">
            <p>
                Many of our customers use Operator Assisted Calls for Marketing Webinars, Investors Relations,
                Training and Development, Town Hall Events, Human Resource and more.
            </p>
            <p>
                Whether you are organising an online conference for thousands of people or need extra assistance for a smaller webinar, the
                Powwownow Event Team will help you every step of the way to deliver a first-class presentation. One key feature of the operator
                assisted service is the Event Manager.  A dedicated Event Manager handles all your requested services before, during and
                after your meeting.
            </p>
        </div>

        <div class="margin-top-32px">
            <?php include_component(
                'commonComponents',
                'genericHeading',
                array(
                    'title' => __('Powwownow Operator Assisted Calls'),
                    'headingSize' => 'xxl',
                    'user_type' => 'powwownow')
            ); ?>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Professional Conference Operator</span><br>
                    Professional announcer to look after all your conferencing needs, they will kick off the call with a customised scripted meeting and speaker introductions.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Multi-lingual operators</span><br>
                    Conducting a global call, and want to offer people the choice of their native language? We offer multi-lingual operators to help with your call.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Worldwide access</span><br>
                    We offer access to conferences from around the world.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Web conferencing</span><br>
                    Fully integrated with our high-quality audio conferencing, your presentation is communicated over the phone and supported online by slides.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Conference call recording</span><br>
                    Don’t spend ages trying to scribble down notes, simply record the call and play back later. Tight on time? We also offer a transcription service of the meeting, so you only need to concentrate on the important stuff!
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Dial-in replay service</span><br>
                    Missed the beginning of the call, we offer a dial-in replay service which allows you to catch up on important conversations that were had.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Web streaming and webcasts</span><br>
                    Webcasting solutions are proven to drive higher levels of audience engagement using a highly interactive and scalable platform for visual communications. Both audio and video webcasting deliver a powerful message.
                </p>
            </div>
        </div>

        <div class="margin-top-32px">
            <?php include_component(
                'commonComponents',
                'genericHeading',
                array(
                    'title' => __('Powwownow Webcast Solution'),
                    'headingSize' => 'xl',
                    'user_type' => 'powwownow')
            ); ?>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">No landline phone is required</span><br>
                    Audio can be played (streamed) via your PC.
                </p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Live feedback</span><br>
                    Get instant feedback from attendees with survey, polling and chat features.
                </p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Full customisation</span><br>
                    Full branding options to reinforce your message.
                </p>
            </div>
        </div>

        <div class="clearfix">
            <div class="grid_sub_2"><img alt="Arrow" src="<?php echo getDataURI('/sfimages/list-yellow-arrow.png');?>"></div>
            <div class="grid_sub_22">
                <p>
                    <span class="rockwell font-bigger">Rich multimedia tool</span><br>
                    A high impact tool for reaching large audiences.
                </p>
            </div>
        </div>

        <div class="margin-top-15px">
            <div class="lozenge-green rockwell">
                <p>Talk to our specialists on 0800 229 0700</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
