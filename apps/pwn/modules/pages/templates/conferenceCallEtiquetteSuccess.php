<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'Conference call etiquette')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Call Etiquette'),'title' => __('Conference Call Etiquette'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Call Etiquette'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>The use of <a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services');?>">teleconferencing services</a> has become increasingly popular in recent years and many people use conference calls to replace face-to-face meetings. Like any meeting there is certain etiquette than can help you run a polite, effective and pleasant call. This is mostly common sense, but be sure to be aware of the &ldquo;cardinal sins of conference calling&rdquo;; consider this your personal teleconferencing finishing school!</p>
                <ol class="conferencecalletiquette">
                    <li>
                        <span>Be on time for the call &ndash; you wouldn&rsquo;t show up late for a face-to-face meeting, so show your conference calling colleagues the same courtesy.</span>
                    </li>
                    <li>
                        <span>Avoid the hold button &ndash; many phones have music that automatically plays while the phone is on hold, this can be very disruptive to the other callers. If you have to step away, leave the conference call and dial back in when you&rsquo;re free again. Tell the other participants and use mute if you don&rsquo;t want to leave the call, but have to go away for a short period of time.</span>
                    </li>
                    <li>
                        <span>Mind your manners &ndash; don&rsquo;t eat your dinner while you&rsquo;re on a <a title="conference call " href="<?php echo url_for('@conference_call');?>">conference call</a>, reply to your emails, or take the telephone into noisy communal areas. These noises can be picked up by the phone and they will disrupt everyone else on the call!</span>
                    </li>
                    <li>
                        <span>Your dog is not invited &ndash; if you have a noisy pet (or even a noisy child) in the vicinity of where you&rsquo;re holding the teleconference, close the door or put your pet outside. Unless their name is on the attendees list they shouldn&rsquo;t be heard on the call.</span>
                    </li>
                    <li>
                        <span>Don&rsquo;t talk over other participants &ndash; this is rude in any situation, but with <a title="telephone conferencing" href="<?php echo url_for('@telephone_conferencing');?>">telephone conferencing</a> no one will be able to make out what either you, or the person you have spoken over, just said. Be patient and wait your turn to speak.</span>
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>