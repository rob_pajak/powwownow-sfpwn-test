<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'IT services')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Calls for IT Services'),'title' => __('Conference Calls for IT Services'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Calls for IT Services'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>In IT services your main responsibility is to support other people and the software that supports them, so you don&rsquo;t want be stuck with a <a title="conference call providers" href="<?php echo url_for('@conference_call_providers');?>">conference call provider</a> that gives you yet more problems to solve. Powwownow understands this and we aim to provide you with an excellent teleconferencing solution. Whether you&rsquo;re providing technical support to colleagues or walking a client half the world away through an installation, Powwownow&rsquo;s telephone conference call service offers a free, quick and easy way for you to get in touch without the hassle of restrictive contracts, reserving calls or unnecessary conference calling hardware.</p>
                <p>Business is global these days and there are few industries where this is felt more than in IT services &ndash; a lot of IT support can now be provided over the phone or the web and this has meant that IT companies are no longer location based when courting clients. If you&rsquo;re trying to grab that vital contact with a client in Cape Town or San Diego, Powwownow&rsquo;s <a title="international conference call" href="<?php echo url_for('@international_conference_call');?>">international conference call</a> service can put you a cut above the rest by giving you instant access to conference calling at a moment&rsquo;s notice. All you and your clients will need to get started is a dial-in number, a telephone and your Powwownow PIN &ndash; it couldn&rsquo;t be easier!</p>
                <p>No one does <a title="conference call" href="<?php echo url_for('@conference_call');?>">conference calls</a> quite like Powwownow do, and our fantastic web conferencing service proves it! Unlike some other conference call providers we don&rsquo;t charge you for using our web conferencing software and it will only cost you as much as a standard telephone call. On a Powwownow web conference you can share your desktop with colleagues struggling with IT problems, or presentations with potential clients to give your proposal that winning edge. Only the call chairperson needs to install our software &ndash; all your viewers need is a web browser and an internet connection.</p>
                <p>We understand that the reliability of your conferencing services is incredibly important in the IT services industry. At Powwownow we are 100 per cent reliable as there are no third party providers between you and our services. We are a telecommunications company in our own right, as well as a conference call provider, and we operate with a fully owned infrastructure meaning all our servers, data and networks are owned solely by us. As we operate independently our customers can always be sure who has access to their data and we always know that we&rsquo;re providing them with the best service possible.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>