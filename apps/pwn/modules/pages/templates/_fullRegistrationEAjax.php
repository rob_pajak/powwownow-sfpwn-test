<div style="position:relative; color: #FFFFFF; width: 50%; float: left;">
    <h2 class="rockwell white" style="margin-top:5px">Order your free wallet card</h2>
    <form id="index-welcomepack-form" style="background: transparent" enctype="application/x-www-form-urlencoded" method="post" action="<?php echo url_for("@request_welcome_pack_homepage_e"); ?>" name="mypins-walletcard-form">
        <div>
            <label for="welcomePack_company">Company (optional)</label>
            <div style="position: relative" class="fields">
                <span class="mypwn-input-container">
                    <input type="text" id="welcomePack_company" name="welcomePack[company]" class="mypwn-input input-large font-small" autocomplete="off"/>
                </span>
            </div>

            <label for="welcomePack_building">Address *</label>
            <div style="position: relative" class="fields">
                <span class="mypwn-input-container">
                    <input type="text" id="welcomePack_building" name="welcomePack[building]" class="mypwn-input input-large font-small required" autocomplete="off"/>
                </span>
                <span class="mypwn-input-container">
                    <input type="text" id="welcomePack_street" name="welcomePack[street]" class="mypwn-input input-large font-small" autocomplete="off"/>
                </span>
            </div>

            <label for="welcomePack_town">Town *</label>
            <div style="position: relative" class="fields">
                <span class="mypwn-input-container">
                    <input type="text" id="welcomePack_town" name="welcomePack[town]" class="mypwn-input input-large font-small required" autocomplete="off"/>
                </span>
            </div>

            <label for="welcomePack_postal_code">Postcode *</label>
            <div style="position: relative" class="fields">
                <span class="mypwn-input-container">
                    <input type="text" id="welcomePack_postal_code" name="welcomePack[postal_code]" class="mypwn-input input-large font-small required" autocomplete="off" pattern="^([A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKS-UWa-hjks-uw])\ {0,1}[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|([Gg][Ii][Rr]\ 0[Aa][Aa])|([Ss][Aa][Nn]\ {0,1}[Tt][Aa]1)|([Bb][Ff][Pp][Oo]\ {0,1}([Cc]\/[Oo]\ )?[0-9]{1,4})|(([Aa][Ss][Cc][Nn]|[Bb][Bb][Nn][Dd]|[BFSbfs][Ii][Qq][Qq]|[Pp][Cc][Rr][Nn]|[Ss][Tt][Hh][Ll]|[Tt][Dd][Cc][Uu]|[Tt][Kk][Cc][Aa])\ {0,1}1[Zz][Zz]))$"/>
                </span>
            </div>

            <label for="welcomePack_country">Country *</label>
            <div style="position: relative" class="fields">
                <span class="mypwn-input-container">
                    <input type="text" id="welcomePack_country" value="United Kingdom" name="welcomePack[country]" class="mypwn-input input-large font-small" readonly="readonly"/>
                </span>
            </div>

            <input type="hidden" value="GBR" id="welcomePack_country_code" name="welcomePack[country_code]"/><br/>

            <div class="margin-top-10">
                <button class="button-orange" type="submit" id="mypwn-mypins-walletcard-send"><span>SEND WALLET CARD</span></button>
                <div class="clear"><!--Blank--></div>
                <span><p>* Mandatory Fields</p></span>
            </div>
        </div>
    </form>
</div>

<div style="position:relative; color: #FFFFFF; width: 50%; float: left; margin-top:0px;">
    <div style="height: 470px; background: url('/sfimages/step3-bg.png') no-repeat; color: #ffffff; padding: 30px 35px 0 20px">
        <div style="text-align: center">
            <img src="/sfimages/walletCard/wallet-card-option.png" width="260" alt="wallet card" />
            <div class="mypwn-customer">
                <div>
                    <span>Name: </span><?php echo $first_name .' '. $last_name?>
                </div>
                <div>
                    <span>Dial-in Number: </span><?php echo $dialInNumber; ?>
                </div>
                <div>
                    <span>PIN: </span><?php echo $pin;?>
                </div>
            </div>
        </div>

        <div style="margin-top: 10px; margin-right:20px;">
            <i>"Your wallet was very innovative and made me smile"</i><br />
            <div style="text-align: right; font-weight: bold">Yvonne, BMW</div>
        </div><br/><br/>

        <ul class="chevron">
            <li><b>Wallet Card</b><br/><span style="color: #515151">With your personal dial-in numbers and PIN</span></li>
            <li><b>Stickers</b><br/><span style="color: #515151">For your computer and phone</span></li>
            <li><b>Free delivery</b><br/><span style="color: #515151">We'll post your card and stickers within 3 working days</span></li>
        </ul>
    </div>
</div>
<script type="text/javascript">pwnApp.homepageE.RequestWelcomePackRegistration();</script>
