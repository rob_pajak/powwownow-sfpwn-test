<div class="row">
    <div class="small-12 medium-12 large-12 columns">
        <header class="logo">
            <a href="<?php echo url_for('@homepage');?>">
                <img src="/cx2/img/mobile/340_logo_strap_line_white.png" width="200" alt="Powwownow">
            </a>
        </header>
    </div>
</div>
<div class="row">
    <div class="small-12 medium-8 large-7 medium-centered large-centered columns">
        <section class="pin-reminder-container">
            <div id="js.ko.pin.reminder.container" data-bind="template: {name: 'pin.reminder.container.data' }"></div>
            <script type="text/html" id="pin.reminder.container.data">

                <!-- ko ifnot: ui.success.displaySuccess -->
                <h2 class="section-title">Forgotten your PIN?</h2>
                <p class="section-text">
                    No worries! If you've a FREE account with us, get a reminder below. Otherwise if you're a Plus or a
                    Premium customer <a href="<?php echo url_for('@login')?>">click here</a> to log in and view all
                    of your PINs.
                </p>

                <form class="cx_form pin-reminder-form" data-bind="submit: submit" novalidate>

                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <input class="pseudo-input" type='email'
                                   data-bind="initBind: { attr: 'value', property: model.email, binding: 'textInput' }"
                                   placeholder="Enter your email address" />
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <p class="section-text">We can also text it to you if you'd like?</p>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <input class="pseudo-input" type='text'
                                   data-bind="initBind: { attr: 'value', property: model.mobileNumber, binding: 'textInput' }"
                                   placeholder="Enter your mobile number" />
                        </div>
                    </div>

                    <div class="row error-message" data-bind="fadeVisible: ui.errors.message().length > 0">
                        <div class="small-12 medium-12 large-12 columns">
                            <p data-bind="html: ui.errors.message()"></p>
                        </div>
                    </div>

                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <img id="loader" src="/cx2/img/throbber.gif" alt="loading..." class="loader" data-bind="visible: ui.isEnabled.loader"/>
                            <button type="submit" class="right button-blue-set-a" data-bind="enable: ui.isEnabled.submit">
                                Send me a reminder
                            </button>
                        </div>
                    </div>

                </form>
                <!-- /ko -->

                <!-- ko if: ui.success.displaySuccess -->
                    <h2 class="section-title">On its way...</h2>
                    <p class="section-text">We've sent a PIN reminder to <strong  data-bind="text: model.email()"></strong></p>
                    <p class="section-text">You'll get a text from us too if you added in your mobile number.</p>
                    <p class="section-text">Hopefully now you have everything you need to start calling again, but if
                        not just give us a call on 0203 398 0398 or <a href="<?php echo url_for('@contact_us')?>">contact us</a> . </p>
                <!-- /ko -->
            </script>
        </section>
    </div>
</div>
