<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'Consultants')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Calls for Consultants'),'title' => __('Conference Calls for Consultants'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Calls for Consultants'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>As a consultant, you will already be well aware of the value of being able to stay in touch. Being able to get people on the phone at the right time can give you a crucial business edge. When you have <a title="conference call" href="<?php echo url_for('@conference_call');?>">conference call</a> provider Powwownow by your side, you can rest assured that you have a conference call service that will be up and running whenever and wherever you are!</p>
                <p>When you work in consulting keeping costs to a minimum is a top priority. This can put investing in a teleconferencing service at the bottom of your priorities list &ndash; after all, why tie up money in a watertight contract with a conference call provider when you don&rsquo;t have to? Powwownow agree! We don&rsquo;t think you should be paying expensive fees for a dependable teleconference service and with our <a title="free conference call" href="<?php echo url_for('@free_conference_call');?>">free conference call</a> service you can have as many people on a call as you like, for as long as you like, from wherever you like, at a price you like: the cost of a normal telephone call. Most of our conference calls will only cost the same as a two-way call normally would and with Powwownow Premium&rsquo;s choice of dial-in numbers and rates the cost of international and mobile calls can be reduced significantly for both you and your client! You won&rsquo;t need to tie yourself into a contract or buy conference calling equipment, all you need to get going is a telephone, your dial-in number and a Powwownow PIN.</p>
                <p>When you&rsquo;re marketing your skills to a global audience, you need to be sure you can keep in touch with your clients whenever they need you. Powwownow offers reservationless conference calling services that mean you can set up a conference call at any time of the day, or night, with no waiting around. Our <a title="international conference call" href="<?php echo url_for('@international_conference_call');?>">international conference call</a> service is available in over 100 countries worldwide and our fantastic free web conferencing tool allows you to add visual presentations to your conference calls over the internet so you can put forth your new business proposal in the best way possible, no matter where your client is. Powwownow&rsquo;s services can help your business to grow globally!</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>