<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Mobile Conference Call'),
                'headingTitle' => __('Mobile Conference Call'),
                'headingSize' => 'l',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p><a title="conference call" href="<?php echo url_for('@conference_call'); ?>">Conference call</a> services give you that almost magical ability to make your presence felt half the world away without leaving your home or office. For those who spend a lot of time in the office, it’s easy to pick up the phone and have a high quality teleconference with anyone, anywhere in the world, but with many conference call providers this stops at landlines. You may have been told that it’s impossible to have a high quality conference call using a mobile phone, and that there’s no free conference call provider that could possibly promise otherwise, but Powwownow is here to prove that none of this is true!</p>
        <p>We’re a telecommunications company in our own right and so we can guarantee our customers the best quality <a title="voice conferencing" href="<?php echo url_for('@voice_conferencing'); ?>">voice conferencing</a>, with amazing features at the lowest price possible. We do all this without charging you a penny – you’ll only pay the cost of the telephone call no matter what device you’re using to dial in. We understand that you can’t always be tied to your office phone, so we offer mobile phone users all the same features on our conference calls that we offer to our landline users. We promise your calls will always be of the highest quality possible for your mobile phone handset.</p>
        <p>If you’re lucky enough to own an iPhone and love our services, our <a title="iphone conference app" href="<?php echo url_for('@iphone_conference_app'); ?>">conference call iPhone</a> app could be your teleconferencing dream come true! Our iPhone app works with any iPhone with iOS 3.0 or higher and will give you all of the functionality of myPowwownow while on the move. This app will schedule teleconferences, invite participants from the phone’s contact list and send automatic call reminder alerts via email and push notifications. This clever little thing can also geo-locate you to determine the best dial-in number for you! What more could you ever want from a conference calling app? Our latest version is also multi-platform compatible, meaning that the same app is now also available for your Android, Blackberry or Nokia smartphone.</p>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
