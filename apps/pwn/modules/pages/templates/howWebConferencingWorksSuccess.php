<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix how-web-conferencing-works">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('How Web Conferencing Works'),
                'headingTitle' => __('How Web Conferencing Works'),
                'headingSize' => 'xl',
                'headingUserType' => 'powwownow'
            )
        ); ?>

        <div class="clearfix">
            <p class="intro-paragraph">
                Powwownow <a title="Web Conferencing" href="<?php echo url_for('@web_conferencing'); ?>">web conferencing</a>
                allows you to chat, present documents and share your desktop with your conference call participants.
                Now everyone can see what you’re talking about!
            </p>

            <div class="grid_10">
                <?php include_component(
                    'commonComponents',
                    'hTML5OutputVideo',
                    array(
                        'config' => array(
                            'width'    => 355,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true
                        ),
                        'video'  => array(
                            'mp4'  => '/sfvideo/howWebConferencingWorks.mp4',
                            'webm' => '/sfvideo/howWebConferencingWorks.webm',
                            'ogg'  => '/sfvideo/howWebConferencingWorks.ogv',
                        ),
                        'called' => 'html'
                    )
                ); ?>
            </div>
            <div class="grid_14 download-instructions">
                <p>If you are already registered with Powwownow simply download the software applications.</p>

                <div class="text-align-center">
                    <button class="button-orange" id="how-web-conferencing-works-download"
                            data-link="<?php echo url_for($link); ?>">
                        <span>DOWNLOAD NOW</span>
                    </button>
                </div>
                <p>
                    If you don't have a Powwownow account <a
                        href="<?php echo url_for('@web_conferencing_get_started'); ?>">register here</a> and we will
                    send you a PIN
                    and password which can be used for both voice and web conferencing.
                </p>
            </div>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell">Getting started</h3>
            <ol class="getting-started-list">
                <li><p>Are you a Powwownow user?</p>
                    <ul class="chevron-green">
                        <li>
                            If you are already registered with Powwownow simply
                            <a title="Web Conferencing" href="<?php echo url_for('@login'); ?>">download</a>
                            the software application
                        </li>
                        <li>
                            If you don't have a Powwownow account
                            <a href="<?php echo url_for('@web_conferencing_get_started'); ?>">register here</a>
                            and we will send you a PIN and password which can be used for both voice and web
                            conferencing
                        </li>
                    </ul>
                </li>
                <li>
                    <p>
                        <a title="Web Conferencing" href="<?php echo url_for('@web_conferencing_auth'); ?>">Download </a>
                        and install the web application
                    </p>
                </li>
                <li><p>Once the application is installed, the start up window will appear</p></li>
                <li><p>Log in to the application with your email address and password</p></li>
            </ol>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="Hold-A-Web-Conference-Instantly">Hold a web conference instantly</h3>
            <div class="grid_sub_16">
                <ol class="getting-started-list">
                    <li><p>Start your web conference by clicking <strong>'Web Conference'</strong> in the Get Started screen</p></li>
                    <li><p><strong>‘Copy’</strong> the website link and your unique PIN and send this to your participants via email</li>
                    <li><p>Click on the <strong>'Screen share'</strong> button to start showing your screen</p></li>
                    <li><p>Participants join using a standard web browser and by visiting <a href="http://powwownow.yuuguu.com/">http://powwownow.yuuguu.com/</a> where they enter your unique PIN and their name</p></li>
                    <li><p>When participants join, you will see them appearing on your Powwownow Web chat window. Participants are now viewing your screen.</p></li>
                </ol>
            </div>
            <div class="grid_sub_8">
                <img src="/sfimages/Web-Conferencing-Application-Getting-Started.jpg" alt="Powwownow Web Application" height="180" style="float: right; margin-left: 20px; margin-bottom: 20px">
            </div>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="Schedule-A-Meeting">Schedule a meeting</h3>
            <div class="grid_sub_16">
                <p>In the Get Started screen you can organise your next web or audio conference, by clicking <strong>'Schedule a Meeting'</strong>.</p>
                <p>You can schedule your next meeting by phone or email, simply give the details displayed to your attendees or send a calendar invite using Outlook or Google Calendar.</p>
                <p>Once you have scheduled your meeting click <strong>'Done'</strong> and this will return you to the Get Started screen.</p>
            </div>
            <div class="grid_sub_8">
                <img src="/sfimages/Schedule-A-Call.png" alt="Schedule a Meeting" height="180" style="float: right; margin-left: 20px; margin-bottom: 20px">
            </div>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="Web-Conference-Controls">Web conference controls</h3>
            <ul class="chevron-green">
                <li>
                    <strong>Pause:</strong> Allows you to pause your screen share so that your participants will see the last image frozen. Click <strong>'Restart'</strong> when you're ready to continue sharing your screen.
                </li>
                <li>
                    <strong>Change quality:</strong> Allows you to select from 'Fastest/Low colour' to 'Slowest/Full colour', depending on your preferences
                </li>
                <li>
                    <strong>Remote control:</strong> Allows another user to control your keyboard and mouse
                    <ol class="decimal getting-started-list">
                        <li><p>Your participant need to click the <strong>'Request Control'</strong> button in their screen sharing window</p></li>
                        <li><p>You need to accept the request by clicking <strong>'share'</strong> in the pop-up (permission is always required)</p></li>
                        <li><p>The <strong>'Release Control'</strong> button releases their control over your computer</p></li>
                        <li><p>The <strong>'Re-take Control'</strong> button can be clicked at any time by you in order to regain control</p></li>
                    </ol>
                <li><strong>End Web Conference:</strong> Allows you to end your web conference</li>
            </ul>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="Powwownow-Premium">Powwownow Premium</h3>
            <p><strong>Adding users</strong></p>
            <ol class="getting-started-list">
                <li><p>Click on 'Find contacts' in the start button drop down in your screen sharing window</p></li>
                <li><p>Add contacts from your email and instant messaging accounts, or simply enter their email address</li>
                <li><p>Follow the simple steps and when you're finished we'll instantly send them an email with details of how to download and sign in to Powwownow web conferencing</p></li>
            </ol>
            <p><strong>Chatting and sharing your screen with your contacts</strong></p>
            <ul class="chevron-green">
                <li>Chat and share your screen individually with each of your contacts by clicking their name.</li>
                <li>Add other users to your conversation by clicking the 'Add Contacts' button on your chat window.</li>
                <li>To show your screen to various contacts at the same time, click on the 'Start' arrow, 'Start Sharing your screen' and choose the contacts you want to show your screen to. Your participants will be notified when you have started screen-sharing.</li>
            </ul>
            <p><strong>Combine multiple IM account</strong></p>
            <p>With Powwownow web conferencing, you can have more than one instant Messenger account running at once. To add a MSN, Yahoo, AIM, etc. account to you Powwownow account click on 'Settings' and then on the 'External Accounts' tab.</p>
            <p>All your contacts from your other IM accounts will appear in your Powwownow contact list.</p>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell" id="User-Guides">User guides</h3>
            <p class="margin-bottom-10">For detailed step-by-step user guides, simply click on the links below for printer-friendly summaries that you can keep to hand.</p>
            <ul class="chevron-green">
                <li><a title="Web Conferencing User Guide" href="/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" target="_blank">Web Conferencing user guide</a></li>
            </ul>
        </div>

        <div class="margin-top-10 clearfix">
            <div class="hr-spotted-top png"></div>
        </div>

        <div class="clearfix">
            <h3 class="green rockwell">Tips:</h3>
            <p class="margin-bottom-10">Here are some helpful tips to enhance your web conference.</p>
            <ul class="chevron-green">
                <li><a title="Top 10 Tips for a Conference Call" href="<?php echo url_for('@top_ten_tips_for_web_conferencing'); ?>">Top 10 tips for a web conference</a></li>
                <li><a title="How to Conference Call" href="<?php echo url_for('@how_to_hold_a_web_conference'); ?>">How to web conference</a></li>
                <li><a title="Set up a Conference Call" href="<?php echo url_for('@set_up_a_web_conference'); ?>">Set up a web conference</a></li>
            </ul>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>