<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Sitemap'),
                'headingTitle' => __('Sitemap'),
                'headingSize' => 's',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div id="sitemap-page">
            <h2 class="sitemap-page">
                <a href="<?php echo url_for('@homepage'); ?>">Conference Call Homepage</a>
            </h2>
            <span>Powwownow - the free conference call service with no booking, no billing, no fuss!</span>
            <ul class="chevron-green">
                <li>
                    <a href="<?php echo url_for('@homepage'); ?>">Powwownow</a><br/>
                    Powwownow offers you free, easy and instant access to conference calling
                </li>
                <li>
                    <a href="<?php echo url_for('@plus_service'); ?>">Powwownow Plus</a><br/>
                    Powwownow Plus offers unlimited chairperson and participant PIN sets plus the option to purchase extra products
                </li>
                <li>
                    <a href="<?php echo url_for('@premium_service'); ?>">Powwownow Premium</a><br/>
                    Powwownow Premium offers
                    tailor-made business solutions with superior features and specially negotiated pricing
                </li>
                <li>
                    <a href="<?php echo url_for('@compare_services'); ?>">Compare Services</a><br/>
                    Compare Powwownow's services: Powwownow, Powwownow Plus and Powwownow Premium</li>
                <li>
                    <a href="<?php echo url_for('@event_conference_calls'); ?>">Event Calls</a><br/>
                    For conference calls with up to 1,000 people
                </li>
                <li>
                    <a href="<?php echo url_for('@international_number_rates'); ?>">International-Dial-in Numbers</a>
                    <br/>International numbers to use Powwownow from abroad
                </li>
                <li>
                    <a href="<?php echo url_for('@in_conference_controls'); ?>">In-conference Controls</a><br/>
                    In-conference control keys to enhance your conference call experience
                </li>
                <li>
                    <a href="<?php echo url_for('@homepage'); ?>">PIN Reminder</a><br/>
                    Forgotten your PIN? No problem, you can request a PIN reminder here
                </li>
                <li><a href="<?php echo url_for('@how_we_are_different'); ?>">How We Are Different</a><br/>Compare
                    Powwownow with other audio conference providers
                </li>
                <li>
                    <a href="<?php echo url_for('@international_number_rates'); ?>">International Numbers and Rates</a><br/>
                    International dial-in numbers to use Powwownow from abroad, including local rates in over 14 countries.
                </li>
                <li>
                    <a href="<?php echo url_for('@going_green'); ?>">Going Green</a><br/>
                    How Powwownow conferencing calling can help your business go green &amp; reduce your carbon footprint
                </li>
                <li>
                    <a href="<?php echo url_for('@business_efficiency'); ?>">Business Efficiency</a><br/>Save your
                    business time, money, stress &amp; guilt with Powwownow
                </li>
                <li>
                    <a href="<?php echo url_for('@costs'); ?>">What It Costs</a><br/>Find out more about what is
                    costs to use Powwownow and compare it to BTMeetMe!
                </li>
                <li>
                    <a href="<?php echo url_for('@faqs'); ?>">FAQs</a><br/>Frequently asked questions about the
                    Powwownow service
                </li>
                <li>
                    <a href="<?php echo url_for('@how_conference_calling_works'); ?>">How Conference Calling Works</a>
                    <br/>The 3 easy steps on how to holding conference call with Powwownow, plus handy tips
                </li>
                <li>
                    <a href="<?php echo url_for('@how_web_conferencing_works'); ?>">How Web Conferencing Works</a>
                    <br/>The step by step guide to how our web conferencing &amp; screen sharing platform works
                </li>
                <li>
                    <a href="<?php echo url_for('@contact_us'); ?>">Contact Us</a><br/>
                    Find all our contact information here or drop us a line using the form available<br/>
                </li>
                <li>
                    <a href="<?php echo url_for('@news'); ?>">Latest News and Press</a><br/>
                    The latest news from Powwownow and media coverage received over the years
                </li>
                <li>
                    <a href="<?php echo url_for('@privacy'); ?>">Privacy</a><br/>
                    Learn more about Powwownow's privacy policy
                </li>
                <li>
                    <a href="<?php echo url_for('@terms_and_conditions'); ?>">Terms and Conditions</a><br/>
                    Powwownow's terms and conditions of use
                </li>
                <li>
                    <a href="<?php echo url_for('@about_us'); ?>">About Us</a><br/>
                    Who we are. All about Powwownow and the people behind the company.
                </li>
                <li>
                    <a href="<?php echo url_for('@careers'); ?>">Careers</a><br/>
                    Career opportunities at Powwownow
                </li>
                <li>
                    <a href="<?php echo url_for('@useful_links'); ?>">Useful Links</a><br/>
                    Useful links to our international websites and sister companies
                </li>
                <li>
                    <a href="<?php echo url_for('@login'); ?>">Login</a><br/>
                    Log in to myPowwownow to manage your call settings, use the scheduler tool, access recordings and much more!
                </li>
                <li>
                    <a href="<?php echo url_for('@create_a_login'); ?>">Create a Login</a><br/>
                    Do you have a PIN but no access to myPowwownow? Create a login to unlock your features and benefits.
                </li>
            </ul>

            <h2 class="sitemap-page">
                <a href="<?php echo url_for('@web_conferencing'); ?>">Web Conferencing</a>
            </h2>

            <span>Free web conferencing service for desktop sharing... No booking, no billing, no fuss!</span>

            <ul class="chevron-green">
                <li>
                    <a href="<?php echo url_for('@web_conferencing'); ?>">Screen Share</a><br/>
                    Download Powwownow web to start using our web conferencing service in conjunction with our telephone service
                </li>
                <li>
                    <a href="<?php echo url_for('@web_conferencing_demo'); ?>">Demo</a><br/>
                    Watch and learn how easy it is to use our desktop-sharing service with this short demo
                </li>
                <li>
                    <a href="<?php echo url_for('@web_conferencing_get_started'); ?>">Get Started</a><br/>
                    Register here for our free web conferencing service
                </li>
            </ul>

            <h2 class="sitemap-page">
                <a href="<?php echo url_for('@video_conferencing'); ?>">Video Conferencing</a>
            </h2>

            <span>High quality, video conferencing service powered by Vidyo technology</span>

            <ul class="chevron-green">
                <li>
                    <a href="<?php echo url_for('@video_conferencing_with_tabs?tabName=Demo'); ?>">Demo</a><br/>
                    Watch and discover how easy and effectively your business can benefit from video conferencing
                </li>
                <li><a href="<?php echo url_for('@video_conferencing_with_tabs?tabName=Get-Started'); ?>">Get Started</a><br/>
                    Talk to our video specialists to get started today
                </li>
                <li>
                    <a href="<?php echo url_for('@audio_conferencing'); ?>">Audio Conferencing</a><br/>
                    See why we are the free conference calling service with a difference
                </li>
                <li>
                    <a href="<?php echo url_for('@better_conferencing'); ?>">Better Conferencing</a><br/>
                    Powwownow is better conferencing as the service is easy, free and there's no booking, no billing, no fuss!
                </li>
                <li>
                    <a href="<?php echo url_for('@conference_call_providers'); ?>">Conference Call Providers</a><br/>
                    Have an conference call with Europe's fastest growing free conference call provider
                </li>
                <li>
                    <a href="<?php echo url_for('@conference_call_services'); ?>">Conference Call Services</a><br/>
                    Get instant access to conference calls at a low cost call rate of 4.3p/min+VAT.
                </li>
                <li>
                    <a href="<?php echo url_for('@conference_call'); ?>">Conference Call</a><br/>
                    Conference calls with whoever, whenever: International teleconferencing 24/7, 365 days a year.
                </li>
                <li>
                    <a href="<?php echo url_for('@free_conference_call'); ?>">Free Conference Call</a><br/>
                    Learn about Powwownow's free service and register to reserve your unique PIN.
                </li>
                <li>
                    <a href="<?php echo url_for('@international_conference_call'); ?>">International Conference Call</a><br/>
                    Hold low-cost international conference calls and get local call rates in over 14 worldwide destinations.
                </li>
                <li>
                    <a href="<?php echo url_for('@iphone_conference_app'); ?>">iPhone Conference App</a><br/>
                    iPhone conferencing app, to join conference calls at the touch of a button.
                </li>
                <li>
                    <a href="<?php echo url_for('@teleconference'); ?>">Teleconference</a><br/>
                    Powwownow's service is free, easy, flexible and comes with a great range of features to choose from.
                </li>
                <li>
                    <a href="<?php echo url_for('@telephone_conferencing'); ?>">Telephone Conferencing</a><br/>
                    Free audio-conferencing service, try it now in 3 easy steps!
                </li>
                <li>
                    <a href="<?php echo url_for('@teleconferencing_services'); ?>">Teleconferencing Services</a><br/>
                    Why meet when you can have a Powwow for free! Register for Powwownow teleconferencing today.
                </li>
                <li>
                    <a href="<?php echo url_for('@voice_conferencing'); ?>">Voice Conferencing</a><br/>
                    Enter your email to generate you PIN and start your PowwowNOW. It's as easy as 1-2-3!
                </li>
            </ul>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
