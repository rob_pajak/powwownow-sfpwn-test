<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'Finance')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Calls for Financial Services'),'title' => __('Conference Calls for Financial Services'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Calls for Financial Services'), 'headingSize' => 'xxl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>The financial services and investment industry never sleeps, and to be at the top of your game you need to know that you can speak to contacts across the world at any time of the day or night at the drop of a hat. Many conference call providers will tie you into a contract that will charge you through the nose for an <a title="international conference call" href="<?php echo url_for('@international_conference_call');?>">international conference call</a> and require you to make a reservation in advance of any calls you make, which can seriously hamper communication lines. However, with reservationless, free conference calling from Powwownow you can touch base with those clients in New York, Germany and Japan with no waiting around for a call slot.</p>
                <p>Financial workers who have to collaborate on large projects will know the difficulty of keeping up-to-date on the latest goings on when you have hundreds of people working on the same task. <a title="voice conferencing" href="<?php echo url_for('@voice_conferencing');?>">Voice conferencing</a> can alleviate some of this issue, but most conference call solutions restrict the number of participants you can have on any one call which can make updating large numbers of staff spread across multiple work sites problematic. With Powwownow, you can have up to 1,000 participants on a teleconference call at any one time making project, investor and shareholder meetings a snap. All your participants need is a telephone, your dial-in number and a Powwownow PIN to get started and with Powwownow's Premium service you can set these PINs to be time-limited.&nbsp; This gives an extra level of security for calls where you're relaying sensitive finiancial information to investors or shareholders. Best of all there&rsquo;s no contracts or restrictions giving your conference calls a truly &lsquo;unlimited&rsquo; feel.</p>
                <p>You might think a <a title="conference call" href="<?php echo url_for('@conference_call');?>">conference calling</a> service like this would cost an arm and a leg, require a lot of expensive conference calling hardware or require you to sign a lengthy and restrictive contract, but you&rsquo;ll be glad to know that Powwownow&rsquo;s teleconference services are all about freedom. Our conference calls will only set you back the same amount as a normal phone call would and all you need to hold one is a telephone, your dial-in number and a Powwownow PIN and you&rsquo;re ready to go.</p>
                <p>I also think it would be appropriate to talk about our time-limited PINs feature which offers extra security for high level</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>