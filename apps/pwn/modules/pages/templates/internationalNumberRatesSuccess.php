<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('International Numbers & Rates'),
                'headingTitle' => __('International Numbers & Rates'),
                'headingSize' => 'xl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix margin-bottom-20">
            <p>Now even more international callers can dial into your conference calls at much lower rates. Simply locate your caller’s country below and send them the corresponding number for your next international conference call.</p>
            <p>For lower cost calls from your UK mobile, dial <strong><?php echo $defaultShortCode; ?></strong> to join your conference and enter your PIN as usual.</p>
            <p><a title="Powwownow Dial-in Numbers Guide" href="/sfpdf/en/Powwownow-Dial-in-Numbers.pdf" target="_blank">Download</a> our international dial-in numbers guide.</p>

            <div class="clearfix" style="position: relative">
                <?php include_component('mypwn', 'createDataTable', array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)); ?>
            </div>
            <div class="clearfix internationalNumberRatesFooter">
                <p>If a dial-in number is not listed for the country you require, please dial +44 844 4 73 73 73 / +49 1803 001 178 (calls charged at international rates).</p>
                <ol>
                    <li>The dial-in numbers in these countries are not available for use from mobiles. In these countries, mobile callers should continue to dial +44 844 4 73 73 73 / +49 1803 001 178 (calls charged at international rates).</li>
                    <li>The dial-in numbers for these countries can only be dialled from within the country. International callers should continue to dial +44 844 4 73 73 73 / +49 1803 001 178 (calls charged at international rates).</li>
                    <li>The UK short code can be dialled in place of a shared cost number and only from a UK mobile. It costs 12.5p per minute + VAT from any UK mobile network provider.</li>
                </ol>
                <p>Powwownow does not set the cost of calls – some networks may vary from these prices – for cost confirmation please contact your network provider.</p>
                <p>The Polish number range is available for account holders and users of the Telefonia Dialog network and TP S.A., Exatel, GTS Energis, Netia, DataCOM, ST Zbaszyn and Multimedia Polska S.A. For calls from mobile phones, the number range is only available for Orange network users.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
