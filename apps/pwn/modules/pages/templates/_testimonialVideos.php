<div class="jcarousel-wrapper">
    <div class="jcarousel">
        <ul>
            <li>
                <?php include_component(
                    'commonComponents',
                    'outputCrossBrowserHtml5AndFallbackVideo',
                    array (
                        'config' => array(
                            'width'    => 460,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true,
                            'preload'  => 'none'
                        ),
                        'video' => array(
                            'mp4'   => '/sfvideo/Brocklebank_Pen_Final.mp4',
                            'webm'  => '/sfvideo/Brocklebank_Pen_Final.webm',
                            'ogg'   => '/sfvideo/Brocklebank_Pen_Final.ogv',
                            'image' => '/sfimages/video-stills/Brocklebank_Pen_Final.png'
                        ),
                    )
                ); ?>
                <p class="videoCaption rockwell">John Pollard - Director at Brocklebank Penn</p>
            </li>
            <li>
                <?php include_component(
                    'commonComponents',
                    'outputCrossBrowserHtml5AndFallbackVideo',
                    array (
                        'config' => array(
                            'width'    => 460,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true,
                            'preload'  => 'none'
                        ),
                        'video' => array(
                            'mp4'   => '/sfvideo/testimonial_vdeo_sandpit_new.mp4',
                            'webm'  => '/sfvideo/testimonial_vdeo_sandpit_new.webm',
                            'ogg'   => '/sfvideo/testimonial_vdeo_sandpit_new.ogv',
                            'image' => '/sfimages/testimonials/testimonial_vdeo_sandpit_new.png'
                        ),
                    )
                ); ?>
                <p class="videoCaption rockwell">Simon Campbell - Founder at The Sandpit</p>
            </li>
            <li>
                <?php include_component(
                    'commonComponents',
                    'outputCrossBrowserHtml5AndFallbackVideo',
                    array (
                        'config' => array(
                            'width'    => 460,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true,
                            'preload'  => 'none'
                        ),
                        'video' => array(
                            'mp4'   => '/sfvideo/PWN-DC.mp4',
                            'webm'  => '/sfvideo/PWN-DC.webm',
                            'ogg'   => '/sfvideo/PWN-DC.ogv',
                            'image' => '/sfimages/PWN-DC.png',
                        ),
                    )
                ); ?>
                <p class="videoCaption rockwell">Duncan Cheatle - StartUp Britain</p>
            </li>
            <li>
                <?php include_component(
                    'commonComponents',
                    'outputCrossBrowserHtml5AndFallbackVideo',
                    array (
                        'config' => array(
                            'width'    => 460,
                            'height'   => 260,
                            'autoplay' => false,
                            'controls' => true,
                            'preload'  => 'none'
                        ),
                        'video' => array(
                            'mp4'   => '/sfvideo/pwn-ms.mp4',
                            'webm'  => '/sfvideo/pwn-ms.webm',
                            'ogg'   => '/sfvideo/pwn-ms.ogv',
                            'image' => '/sfimages/pwn-ms.png'
                        ),
                    )
                ); ?>
                <p class="videoCaption rockwell">Mark Shaw - Engagement Index</p>
            </li>
        </ul>
    </div>

    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
</div>
<script>
(function($) {
    'use strict';
    $(function() {
        testimonialsInitialisation();
    });
})(jQuery);
</script>
