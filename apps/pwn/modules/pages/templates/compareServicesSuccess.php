<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Compare Services'),
                'headingTitle' => __('Compare Services'),
                'headingSize' => 'l',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <table class="rockwell">
                <tbody>
                    <tr>
                        <th class="compare-features-col">Features</th>
                        <th class="compare-pwn-col">Powwownow</th>
                        <th class="compare-plus-col">Plus</th>
                        <th class="compare-premium-col">Premium</th>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Instant availability</td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col">&nbsp;</td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Secure access and your own unique PIN</td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Unlimited call duration</td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Low-call rate numbers in 15 countries (e.g. 0844 or 87373 on UK
                            mobile)
                        </td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Web conferencing solution</td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Powwownow conference scheduling tools</td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Instant conference call recording system</td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Choice of call announcements, language and music on-hold</td>
                        <td class="compare-pwn-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Multiple users with Chairperson and Participant PINs</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Landline numbers for 50+ countries (e.g. 0203)</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Freephone numbers for 70+ countries (e.g. 0800)</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Real time reporting of calls</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Company branding</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Negotiable number rates</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col">&nbsp;</td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="odd">
                        <td class="compare-features-col">Dedicated Account Manager</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col">&nbsp;</td>
                        <td class="compare-premium-col"><img alt="+" src="/sfimages/table-tick.png"></td>
                    </tr>
                    <tr class="even">
                        <td class="compare-features-col">Number of call participants</td>
                        <td class="compare-pwn-col">50*</td>
                        <td class="compare-plus-col">50*</td>
                        <td class="compare-premium-col">Up to 1000</td>
                    </tr>
                    <tr class="table-bottom">
                        <td class="compare-features-col">&nbsp;</td>
                        <td class="compare-pwn-col">&nbsp;</td>
                        <td class="compare-plus-col">&nbsp;</td>
                        <td class="compare-premium-col">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            *Speak with up to 1000 callers with <strong><a title="Event Conference Calls" href="<?php echo url_for('@event_conference_calls'); ?>">Event Calls</a></strong>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>