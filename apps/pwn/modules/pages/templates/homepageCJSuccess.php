<?php slot('page_metaproperties') ?>
<meta property="og:site_name" content="Powwownow.co.uk" />
<meta property="og:title" content="Leading free conference call service provider &#124; Powwownow"/>
<meta property="og:url" content="http://www.powwownow.co.uk/" />
<meta property="og:description" content="Powwownow, the UK's leading free conference call service provider. Offering audio and video conferencing, online collaboration solutions, as well as free screen sharing, call recording and scheduling tools" />
<meta property="og:image" content="http://www.powwownow.co.uk/sfimages/powwownow_big_logo.png" />
<meta property="og:image:width" content="1200"/>
<meta property="og:image:height" content="592"/>
<meta property="og:image" content="http://www.powwownow.co.uk/sfimages/powwownow_logo_square.png" />
<meta property="og:image:width" content="1200"/>
<meta property="og:image:height" content="1200"/>
<meta property="og:locale" content="en_GB" />
<?php end_slot(); ?>

<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix home">
    <div class="grid_24">
        <h1 class="tab-h1">Instant, contract-free, hassle-free conference calling</h1>
    </div>

    <div id="tabs-grid" class="grid_18">
        <div id="pwn-before-registration">
            <p class="mB15">With Powwownow you only pay the cost of your own 0844 phone call – nothing more! <a class="grey" href="#cost-compare">View cost comparison</a></p>

            <?php include_component('commonComponents', 'howItWorks'); ?>

            <section class="register_box">
                <h2 class="white">Our one-step sign-up means you can start conference calling right now</h2>
                <?php include_component(
                    'commonComponents',
                    'jsKoCxRegistrationTemplate',
                    array(
                        'koTemplateId' => 'ko.cj.registration.container',
                        'koTemplateDataId' => 'ko.cj.registration.data',
                        'buttonText' => 'Generate Pin',
                        'isLegacyCss' => true
                    )
                ); ?>
            </section>

            <div class="mT20">
                <div class="column2 first">
                    <img src="/sfimages/customer-support.gif" alt="Customer Support">
                    <p class="title">Instant conference calls</p>
                    <p>There's no need to book your conference calls. You can use Powwownow on demand, 24/7.</p>
                </div>
                <div class="column2 last">
                    <img src="/sfimages/call-quality.gif" alt="Call Quality">
                    <p class="title">Unbeatable call quality</p>
                    <p>Unlike almost all conference call providers, most of our conference calls use the exact same fibre optic cabling as your landline.</p>
                </div>
            </div>
        </div>
    </div>

    <!--Ajax Code Replacement-->
    <div id="tab-pwn-pin-registered" class="grid_18 white"></div>

    <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

    <?php include_component('commonComponents', 'choosePlan', array('plusTrackingName' => 'Homepage-ChoosePlan')); ?>

    <?php include_component('commonComponents', 'costsCompare'); ?>

    <div class="grid_12 alpha" id="index-information">
        <div id="left-content-container">
            <h3 class="rockwell blue">Conference Call with Powwownow</h3>
            <p>Powwownow was founded in 2004 and is now the leading <a title="Free Conference Call" href="<?php echo url_for('@free_conference_call'); ?>">free conference call</a> provider in the UK.</p>
            <p>At Powwownow, we don't believe in doing things the hard way. That's why we offer instant <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>">conference calling</a>, available 24/7, with as many participants as required, wherever they are in the world. And because our service is free, callers only pay the cost of their own phone call, which is added to their standard telecoms bill - nothing more!</p>
            <p>Plus, because we are a telecommunications company in our own right, our customers can be guaranteed the best quality calls with the maximum number of features for the lowest possible price. That's why <a title="How We Are Different" href="<?php echo url_for('@how_we_are_different'); ?>">no one does conference calling like we do</a>!</p>
        </div>
    </div>

    <?php include_component('commonComponents', 'trustPilotBanner'); ?>
    <?php include_component('commonComponents', 'trustPilotFeed'); ?>

    <div class="grid_24">
        <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
    </div>

    <div class="grid_24" id="videos-for-signup">
        <?php include_partial('homepageCro/videoCarousel'); ?>
        <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
    </div>

    <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
