<div class="terms-conditions-container-item item-1">
    <h2>Powwownow general terms and conditions</h2>
    <p>Please read these General Terms and Conditions and any applicable Additional Terms and Conditions carefully before using our Website and/or any Powwownow Services.</p>
    <ol>
        <li>Introduction
            <ul>
                <li>1.1 We are Via-Vox Limited, a limited company registered in England (company number 04646978) trading under the name "Powwownow" (<strong>Powwownow </strong>or <strong>we </strong>or <strong>us </strong>or <strong>our</strong>). Our business is based in England and our registered office is at Vectra House, 36 Paradise Road, Richmond, Surrey, TW9 1SE. Our VAT registration number is 813 0934 52.</li>
                <li>1.2 We are a telecommunications company and a leading provider of conference call services. Our Website is provided for the benefit of customers interested in or Registered and/or subscribed to use our Services (as applicable). We administer the Website from England and through our service providers who may or may not be based in England.</li>
                <li>1.3 These general terms and conditions (<strong>General Terms and Conditions</strong>) set out our commitment to you (<strong>you </strong>or <strong>your</strong>) and your commitment to us in respect of your use of this Website and/or the Services that we provide (as applicable). Each of the Services set out below are governed by: (i) these General Terms and Conditions; (ii) and the additional terms and conditions that apply in respect of each particular Service (<strong>Additional Terms and Conditions</strong>). A hyperlink to the Additional Terms and Conditions applicable to each Service is set out below.
                    <ul>
                        <li><strong>- Powwownow Service</strong> (please <a class="terms-and-conditions-contents" title="Powwownow Service Terms and Conditions" href="#" data-id="2">click here</a> to see the Additional Terms and Conditions that apply to this Service)</li>
                        <li><strong>- Powwownow Plus Service</strong> (please <a class="terms-and-conditions-contents" title="Powwownow Plus Service Terms and Conditions" href="#" data-id="3">click here</a> to see the Additional Terms and Conditions that apply to this service)</li>
                        <li><strong>- Powwownow Premium Service </strong>(please <a class="terms-and-conditions-contents" title="Premium Service Terms and Conditions" href="#" data-id="4">click here</a> to see the Additional Terms and Conditions that apply to this Service)</li>
                        <li><strong>- Powwownow Engage Service</strong> (please <a class="terms-and-conditions-contents" title="Powwownow&nbsp;Engage Service" href="#" data-id="5">click here</a> to see the Additional Terms and Conditions that apply to this Service)</li>
                        <li><strong>- Powwownow Video Conferencing Service</strong> (please <a class="terms-and-conditions-contents" title="Powwownow Video Conferencing Service" href="#" data-id="6">click here</a> to see the Additional Terms and Conditions that apply to this Service)</li>
                        <li><strong>- Powwownow iMEET Service</strong> (please <a class="terms-and-conditions-contents" title="Powwownow iMEET Service" href="#" data-id="7">click here</a> to see the Additional Terms and Conditions that apply to this Service)</li>
                    </ul>
                </li>
                <li>1.4 We also offer an additional web conferencing Service. This Service is not a standalone Service, and may only be used in conjunction with the Powwownow Service or Premium Service. Please see Paragraph 4 for more information.</li>
                <li>1.5 Nothing in any Additional Terms and Conditions will increase our legal liability or change your relationship with us (unless we expressly state, in the Additional Terms and Conditions, our intention to modify the terms of these General Terms and Conditions).</li>
                <li>1.6 If there is any contradiction between the provisions of any applicable Additional Terms and Conditions and these General Terms and Conditions, then the Additional Terms and Conditions shall take precedence only in relation to the particular Service to which they specifically apply.</li>
                <li>1.7 These General Terms and Conditions, together with any applicable Additional Terms and Conditions (together, the <strong>Terms</strong>) form a legally binding agreement between you and us in relation to the use of the Services. By using this Website, any Services, Registering and/or subscribing to use any Services, or by clicking a box that states that you accept and/or agree to these General Terms and Conditions, you signify your agreement to these Terms. If you do not agree to these Terms, you are not permitted to use this Website and/or any Services. It is therefore important that you take the time to read them carefully.</li>
                <li>1.8 We may change these General Terms and Conditions and/or any of the Additional Terms and Conditions at any time without notice. We publish the current version of our General Terms and Conditions and our Additional Terms and Conditions on our Website.</li>
            </ul>
        </li>
        <li>Definitions
            <p>In these Terms:</p>
            <ul>
                <li>2.1 Unless the context otherwise requires, the following definitions apply:
                    <ul>
                        <li>- <strong>"Additional Terms and Conditions"</strong> has the meaning given to that term in paragraph 1.</li>
                        <li>- <strong>"Administrator"</strong> has the meaning given to that term in paragraph 3.2;</li>
                        <li>- <strong>"General Terms and Conditions"</strong> has the meaning given to that term in paragraph 1.3;</li>
                        <li>- <strong>"Network Operator Call Charges"</strong> means the price charged to the caller by the network operator;</li>
                        <li>- <strong>"Participant"</strong> has the meaning given to that term in paragraph 7.3;</li>
                        <li>- <strong>"PIN"</strong> means any personal identification number that we give to you in connection with your use of the Services;</li>
                        <li>- <strong>"Powwownow"</strong> or <strong>'we' </strong>or <strong>'us' </strong>or <strong>'our' </strong>have the meaning given to those terms in paragraph 1.1;</li>
                        <li>- <strong>"Powwownow Call Charges"</strong> has the meaning given to that term in paragraph 4.5;</li>
                        <li>- <strong>"Privacy Policy"</strong> means our privacy policy which is available to view at <a title="Privacy Policy" href="<?php echo url_for('@privacy'); ?>">www.powwownow.co.uk/Privacy</a>;</li>
                        <li>- <strong>"Prohibited Purpose"</strong> shall have the meaning given to that term in Paragraph 7.4;</li>
                        <li>- <strong>"Registration"</strong> or <strong>"Registering"</strong> have the meaning given to those terms in paragraph 3.1;</li>
                        <li>- <strong>"Services"</strong> means (i) the 'Powwownow Service'; (ii) the 'Powwownow Premium Service' conference call services (as each more fully described in the Additional Terms and Conditions applicable to each Service (please click on the hyperlinks at Paragraph 1.3 for more information)); and (iv) the additional web conferencing service (as more fully described at paragraph 4), and <strong>"Service"</strong> means any one of them;<br></li>
                        <li>- <strong>"Software"</strong> has the meaning given to that term in paragraph 4.1;</li>
                        <li>- <strong>"Terms"</strong> has the meaning given to that term in paragraph 1.7;</li>
                        <li>- <strong>"Website"</strong> means our Website found at <a title="Powwownow" href="/">http://www.powwownow.co.uk</a>; and</li>
                        <li>- <strong>"you"</strong> and <strong>"your" </strong>have the meaning given to those terms in paragraph 1.3;</li>
                    </ul>
                </li>
                <li>2.2 Words importing the singular shall include the plural and vice versa, words importing a gender shall include all genders and words importing persons shall include bodies corporate, unincorporated associations and partnerships;</li>
                <li>2.3 Any reference to a statute, statutory provision or subordinate legislation is a reference to such legislation as amended and in force from time to time and to any legislation which re-enacts or consolidates (with or without modification) any such legislation;</li>
                <li>2.4 References to Paragraphs are (unless expressly stated otherwise) references to Paragraphs of these General Terms and Conditions; and</li>
                <li>2.5 Headings are included for ease of reference only and shall not affect the interpretation or construction of these Terms.</li>
            </ul>
        </li>
        <li>Registration, accepting these Terms and our contract with you
            <ul>
                <li>3.1 You can browse this Website without registering, but if you wish to use any of our Services then you will need to register with us by completing the registration process applicable to the relevant Service(s) (<strong>Registration</strong>). The Registration process applicable to each Service that we provide is explained more fully in the specific Additional Terms and Conditions applicable to each Service.</li>
                <li>3.2 You may be required to provide information about yourself (such as identification or contact details, for example an email address), or as part of your ongoing use of the Services. You warrant and represent to us that the details you provide to us during your Registration with us are accurate, complete and up-to-date. We will hold and treat such information in accordance with the terms of our Privacy Policy. If you or your Participants use a mobile telephone to access a Service, we may send occasional SMS messages. You may opt out of this service by contacting us at the address or telephone number shown in Paragraph 25.</li>
                <li>3.3 You agree to immediately notify us of any changes to the information that you provided to us during the Registration process and to ensure that your contact information is accurate and is kept up to date.</li>
                <li>3.4 You will be asked during the applicable Registration process to confirm that you accept and agree to be bound by these Terms. You may not use the Services if you do not accept these Terms and your use of any of our Services is deemed for all purposes to be acceptance of these Terms by you and you acknowledge and agree that we will treat your use of the Services as acceptance of these Terms from the point that you commence using the Services onwards.</li>
                <li>3.5 By applying to Register with us as a Registered user of the Website and/or entering into an agreement for us to provide Services to you, you undertake, represent and warrant to us that: (i) you have the capacity to understand, accept and comply with these terms and conditions; (ii) where you are acting on behalf of a business, you are authorised to act on behalf of that business for the purposes of registration, procurement and subsequent use of our Services; and (iii) you are eighteen (18) years of age or older.</li>
                <li>3.6 You may not use the Services and may not accept these Terms if you (i) are a person barred from receiving the Services under the laws of the United Kingdom or other countries including the country in which you are resident or from which you are using or purporting to use the Services; or (ii) not of legal age in England to form a binding contract with us (18 years of age and older).</li>
                <li>3.7 Our contract with you begins at the point specified in the Additional Terms and Conditions applicable to the Service that you are Registering to use.</li>
                <li>3.8 Before you continue, you should print off or save a copy of these General Terms and Conditions (together with any applicable Additional Terms and Conditions) for your records.</li>
            </ul>
        </li>
        <li>Additional Services: the web conferencing facility
            <ul>
                <li>4.1 We offer additional web conferencing Services that allow you to screen share and web conference when making conference calls using the Powwownow Service, the Premium Service or the iMeet Service (individually the “Service” and collectively the “Services”). To use these additional Services you don´t need to download any software, however if you wish to: (i) share your screen or have a Windows or Mac desktop App (as available) or; (ii) for use of iMeet on your desktop, smartphone or tablet; then you will need to download the software from our Website (i) for Powwownow Service or Premium Service at <a title="Powwownow Web" href="<?php echo url_for('@web_conferencing'); ?>">www.powwownow.co.uk/Web-Conferencing</a> and (ii) for iMeet at <a title="iMeet Tools" href="http://imeet.powwownow.com/tools">imeet.powwownow.com/tools</a>, at an iTunes store (IOS) or at PlayStore (Android).</li>
                <li>4.2 Web conferencing is not a standalone Service, but an additional Service that you may use once you have Registered and entered into an agreement with us to use either the Powwownow Service, the Premium Service or the PWN-iMeet Service. If you have not Registered with us to use either of these Services, you will have to do so before you can use the web conferencing Service. If you have already Registered with us, you will need to confirm your email address and password. A user guide is available on our Website (at <a title="Powwownow Web" href="<?php echo url_for('@web_conferencing'); ?>">www.powwownow.co.uk/Web-Conferencing</a>) that explains how you can download if needed and use the Service.</li>
                <li>4.3 When you download, install and use the Powwownow screen sharing software, your use of the software and the Service will be governed by the terms and conditions of our end user software licence agreement, (a copy of which is available at <a title="Yuuguu License Agreement" href="http://www.yuuguu.com/user_license">www.yuuguu.com/user_license</a>) for Powwownow Service and Premium Service or at <a title="iMeet Terms and Conditions" href="#" data-id="7" class="terms-and-conditions-contents">Powwownow iMeet Service additional terms and conditions</a> for iMeet Service) in addition to these Terms.</li>
                <li>4.4 Before you download, install or in any way use the web conferencing software from our Website, please ensure that you have read and agree to the terms and conditions set out in the end user software license agreement. When you browse and click on the following link, you will be subject to the terms and conditions of their website (available at <a title="Yuuguu License Agreement" href="http://www.yuuguu.com/user_license">www.yuuguu.com/user_license</a> and at <a title="iMeet Terms and Conditions" href="#" data-id="7" class="terms-and-conditions-contents">Powwownow iMeet Service additional terms and conditions</a>).</li>
                <li>4.5 You agree that, in addition to the terms of the web conferencing end user software licence, you shall not (and you shall not permit anyone else, including any Participant, to) copy, translate, merge, modify, create a derivative work of, reverse engineer, decompile or otherwise attempt to extract the source code of the software or any part thereof, unless this is expressly permitted or is otherwise required or permitted by law (and then, only to the extent so permitted or required), unless you have obtained our express prior written consent.</li>
                <li>4.6 You agree to comply promptly with any reasonable instructions given by us from time to time in connection with the use and operation of the web conferencing software.</li>
                <li>4.7 Please be aware that from time to time, the web conferencing software may automatically download and install, or request or require you to download updates from us or our third party supplier. These updates are designed to improve, enhance and further develop the Services and may take the form of bug fixes, enhanced functions, new software modules and completely new versions of the software. By accepting these Terms you agree to receive such updates and permit us to deliver these to you as part of your use of the Services.</li>
                <li>4.8 We reserve the right to discontinue the provision of the web conferencing service at any time.</li>
                <li>4.9 You acknowledge and agree that our affiliated third party supplier may contact you by email in the event that we are to discontinue the provision of the web conferencing Service (so that they may offer to provide you with an alternative web conferencing service, or to see how they may improve the performance of the product).</li>
                <li>4.10 You agree and undertake to stop using the web conferencing Service (and to uninstall the software) if at any time you terminate your agreement with us to use the Powwownow Service, Premium Service or iMeet Service.</li>
            </ul>
        </li>
        <li>myPowwownow
            <ul>
                <li>5.1 When you register for the Powwownow Service, Premium Service or iMeet Service, you may choose to open a myPowwownow account, an online account that allows you to view and change certain details about the Service.</li>
                <li>5.2 When you register for a myPowwownow account you will be given an access password.</li>
            </ul>
        </li>
        <li>The provision of Services to you by Powwownow
            <ul>
                <li>6.1 We will provide the Services to you from the point specified in the Additional Terms and Conditions applicable to the Service that you have Registered to use.</li>
                <li>6.2 The provision of our Services is subject always to available capacity and we do not guarantee that the number of connections required by you will be available at any given time.</li>
                <li>6.3 We will provide the Services with the reasonable skill and care of a competent telecommunications service provider. We cannot guarantee however a fault-free and/or uninterrupted Service, and from time to time faults and/or interruptions may occur. We will repair faults and rectify interruptions as quickly as reasonably possible.</li>
                <li>6.4 Our Services are available 24 hours a day seven days a week except:
                    <ul>
                        <li>- <strong>(i) </strong> in the event of scheduled planned maintenance, in which case the Service may not be available between 2am and 5am Sunday morning (UK time);</li>
                        <li>- <strong>(ii)</strong> in the event of unplanned or emergency maintenance, we may, if the need arises, have to carry out work that may affect the Service. In this event calls may be truncated or may not connect. If we have to interrupt the Service, we will make every effort to restore it within a reasonable time; or</li>
                        <li>- <strong>(iii)</strong> in the event of circumstances beyond our reasonable control.</li>
                    </ul>
                </li>
                <li>6.5 We shall also be entitled to suspend the provision of the Services during any technical failure and/or in the event that it is necessary to safeguard the security and integrity of our Services.</li>
                <li>6.6 Occasionally we may: (i) change the PIN code or phone number or the technical specification of the Service for operational reasons; or (ii) give you instructions that we believe are necessary for security, health or safety, or for the quality of the Services that we supply to you or to our other customers and you agree to observe them; but before doing so, we will give you as much notice as we can.</li>
                <li>6.7 By accepting these Terms you acknowledge and agree that the form and nature of the Services may change from time to time without prior notice to you. As part of your acceptance of our right to change and develop the Services, you acknowledge and agree that we may stop (permanently or temporarily) providing some or all of the Services (or any features within the Services) to you at our sole discretion, without prior notice to you.</li>
            </ul>
        </li>
        <li>Your use of the Services
            <ul>
                <li>7.1 You agree to use the Services only for purposes that are permitted by the applicable Terms. You must also use the Services in accordance with all reasonable instructions we may give you from time to time and in accordance with all applicable laws and/or regulations.</li>
                <li>7.2 Access to an operative telephone line is required to use our Services. Access to the Internet and an e-mail address are required in order to use this Website and some of elements of our Services. You must make your own arrangements for Internet and/or telephone line connection, and you are responsible for any telephone costs and/or charges which may become due in relation to accessing and/or using (as applicable) the Website and/or any of our Services.</li>
                <li>7.3 You and any person that you invite or allow to use the Services (a <strong>Participant</strong>) must use tone-dialling telephones to dial in to any telephone based element of the Services.</li>
                <li>7.4 You agree that you will not, and you will not permit or engage any Participant or any other person to:
                    <ul>
                        <li>- <strong>(i)</strong> engage in any activity that interferes with or disrupts the Services (or the servers and networks which are used in or in connection with the provision of the Services) or is in any way unlawful or not in accordance with all applicable laws and/or regulations;</li>
                        <li>- <strong>(ii)</strong> reproduce, duplicate, copy, sell, trade or resell any of the Services for any purpose;</li>
                        <li>- <strong>(iii)</strong> send, use or reuse any material that is illegal, offensive, pornographic, abusive, indecent, defamatory, immoral, obscene or menacing; or in breach of copyright, trademark, confidentiality, privacy or any other right; or is otherwise injurious to third parties; or objectionable; or which consists of or contains software viruses, commercial solicitation, chain letters, mass mailings or any "spam"; or</li>
                        <li>- <strong>(iv)</strong> harass, annoy, inconvenience or cause needless anxiety to any person (including, without limitation, to make or attempt to make 'hoax' or 'prank' calls), (together, <strong>Prohibited Purposes</strong>).</li>
                    </ul>
                </li>
                <li>7.5 We reserve the right to record calls made through our Services and to use such recordings for the sole purpose of investigating any suspected, alleged or actual use of our Services and/or the underlying telecommunication systems or networks for Prohibited Purposes.</li>
                <li>7.6 You acknowledge and agree that we and/or our licensors own all legal rights, title and interest in and to the Services, including any intellectual property rights which subsist in the Services (whether those rights happen to be registered or not, and wherever in the world those rights may exist).</li>
            </ul>
        </li>
        <li>Telephone access number(s), PIN code(s) and account security
            <ul>
                <li>8.1 We will allocate any PIN code(s) to you at the time detailed in the specific Additional Terms and Conditions applicable to each Service.</li>
                <li>8.2 You shall not acquire any rights in relation to any telephone numbers or any other PIN code or number or passwords allocated by us in connection with the Services. You must access our Services using only the phone numbers and/or PIN provided by us to you.</li>
                <li>8.3 You are responsible for the security, safe keeping and proper use of the PIN code (and any other passwords) once you have received it from us. Other than as expressly permitted by these Terms, you have no right to sell or agree to transfer the PIN code(s) or passwords provided to you for use with the Services and you understand that you shall not do so.</li>
                <li>8.4 You are not permitted to advertise any phone number or PIN code or password provided to you for use with any of the Services without our prior written consent, and you undertake to ensure that this does not happen.</li>
                <li>8.5 You are responsible for maintaining the confidentiality and security of any PIN code(s), telephone numbers and/or passwords that we provide to you. In the event that you become aware of or suspect that your account may be subject to unauthorised use, you agree to notify us immediately at <a title="Powwownow Customer Services" onclick="dataLayer.push({'event': 'Terms and Conditions - Mail Customer Services/onClick'});" href="mailto:customer.services@powwownow.com">customer.services@powwownow.com</a></li>
                <li>8.6 We shall exercise all reasonable efforts to ensure the security of your communications, but we cannot guarantee that all communications shall be completely secure. There remains a risk that, for reasons outside our control, your communications may be intercepted or accessed, whether lawfully or unlawfully, by persons or entities other than the intended recipient.</li>
            </ul>
        </li>
        <li>Charges and payment for the Services
            <ul>
                <li>9.1 Details of the charges and payment terms and/or mechanisms applicable to your use of the Services (where applicable) are set out more fully in the specific Additional Terms and Conditions applicable to each Service. Where a particular Service (or part thereof) is chargeable, you agree to make payment against such charges in accordance with the Additional Terms and Conditions applicable to that Service.</li>
                <li>9.2 Charges (where applicable) exclude (unless expressly stated otherwise) VAT which will be added at the prevailing rate at the time at which we issue an invoice to you.</li>
                <li>9.3 We reserve the right to charge interest at a rate of 5% above the Bank of England base rate on any charges (where applicable) not paid within 30 days of the date of the applicable invoice sent by us to you in accordance with the Additional Terms and Conditions applicable to the relevant Service.</li>
            </ul>
        </li>
        <li>Your use of the Website
            <ul>
                <li>10.1 The Website is accessed via the Internet. You are responsible for providing a suitable computer or mobile device to access and use the Website and for any telecommunications costs you incur in connection with your use of the Website. Please note that the quality of your computer and the quality of your connection will affect your use of the Website and the online services available through the Website (for example, the Website may seem slow if you have a poor connection).</li>
                <li>10.2 You should also check that your computer has suitable protection, such as virus protection. We are not responsible for any computer virus or bug that affects your computer, mobile device or data as a result of your use of the Website or the downloading of any materials from the Website.</li>
                <li>10.3 We do not allow any illegal activities to take place on the Website. You agree to use the Website for lawful purposes only and in a manner which is consistent with any and all applicable laws and regulations in the country in which you access the Website. Your use of the Website must not infringe the rights of, or restrict or inhibit the use and enjoyment of the Website by, any other person. You agree not to take any action that imposes an unreasonable, or disproportionately large, load on the infrastructure of the Website.</li>
                <li>10.4 Although we take pride in the Website and aim to keep it up to date, please note that information we post on the Website may, at times, be incomplete, out of date or inaccurate. If you wish to rely on any information we post then we recommend that you first confirm with us that the information you wish to rely on is correct. The content of the Website is subject to change at any time.</li>
                <li>10.5 We are the owner or the licensee of all intellectual property rights used and/or displayed on our Website, and in the material published on our Website. This material is protected by copyright laws and treaties around the world. You may print off one copy, and may download extracts, of any page(s) from our Website for your personal reference and you may draw the attention of others within your organisation to material posted on our Website. You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.</li>
                <li>10.6 Our status (and that of any identified contributors) as the authors of material on our Website must always be acknowledged. You must not use any part of the materials on our Website for commercial purposes without obtaining a written licence from us or our licensors to do so. If you print off, copy or download any part of our Website in breach of these Terms, your right to use our Website will cease immediately and you must, at our option, return or destroy any copies of the materials which you have made.</li>
                <li>10.7 We may also claim trade mark and service mark rights in marks contained within the pages of this Website. Other trade marks and names may be used in this Website to refer to the entities claiming the marks and names of their products and/or services. We disclaim any proprietary interest in the marks and names of others.</li>
                <li>10.8 Where we provide hypertext links to third party Websites or contacts we do so for information purposes only. We are not responsible for any products, services or materials found on linked third party sites. You use such links entirely at your own risk and we accept no responsibility for the content or use of such Websites or for the information contained on such sites (including any Website through which you may have gained access to the Website). You may not link to this Website, nor frame it, without our prior written permission.</li>
                <li>10.9 Our aim is to make the Website available for use at all times, but we cannot, and do not, guarantee availability either generally or at any particular time. There will be times when the Website or your MyPowwownow account (where you have signed up for a MyPowwownow account) is unavailable. Such unavailability may be planned (for example, where we are carrying out planned maintenance or upgrades) or unplanned (for example, where there is a hardware or software failure). You acknowledge and understand that you will not be able to use the Website or your MyPowwownow account (if applicable) when it is unavailable. We will try to keep unavailability to a minimum, but we accept no liability for any loss or damage you may suffer as a result of the Website or your MyPowwownow account being unavailable. We reserve the right to add to or change the Website and/or the functionality of your MyPowwownow account (where applicable) at any time without notice or explanation and without incurring any liability to you.</li>
            </ul>
        </li>
        <li>Termination or suspension of the Services by us
            <ul>
                <li>11.1 We may suspend access to or use of all or part of the Services and/or terminate our agreement with you immediately if: (i) you materially breach these Terms; (ii) we believe that the Service is being used in a way forbidden or otherwise not permitted by these Terms (this applies even if you do not know that the Services are being used in such a way); (iii) you fail to pay any charges (where applicable) within 30 days of the date of our invoice to you; or (iv) if you become insolvent (or suffer or incur any event or circumstances analogous to insolvency).</li>
                <li>11.2 We will inform you of such suspension and/or termination as soon as reasonably possible and explain why we have taken this action.</li>
                <li>11.3 If we suspend your access to the Services (or any part thereof), access will not be restored until you satisfy us that you will only use the Service in accordance with these Terms.</li>
                <li>11.4 You acknowledge and agree that if we suspend access to your user account, you may be prevented from accessing the Services, your account details or any files or other content which is contained in your account.</li>
            </ul>
        </li>
        <li>Termination or suspension of the Services by you
            <ul>
                <li>12.1 You may terminate your agreement with us at any time by giving us 30 days' written notice of your intention to terminate this agreement. You should contact us to let us know using the contact details out at paragraph 25.</li>
                <li>12.2 Nothing in this paragraph 12 limits or affects any right that you may have to cancel this agreement pursuant to the Additional Terms and Conditions that apply to the Service that you are using.</li>
            </ul>
        </li>
        <li>Limitation of liability
            <ul>
                <li>13.1 Nothing in these Terms shall exclude or limit our liability for death or personal injury arising from our negligence; fraud or fraudulent misrepresentation; or any other liability that cannot be limited or excluded by law. This provision overrides all other provisions of these Terms.</li>
                <li>13.2 The warranties and conditions set out in these Terms are in lieu of all other conditions, warranties or terms which might but for this Paragraph 13 be implied or incorporated into any dealings between you and us in respect of our Contract and/or the Services by statute, common law or otherwise, all of which are excluded to the extent permitted by applicable law.</li>
                <li>13.3 We do not recommend use of the Services where the risk of non-connection or loss of connection carries a material risk. Accordingly you may only use the Service if you accept that all such risk is yours and you should insure against such risks accordingly.</li>
                <li>13.4 Subject to Paragraph 13.1, we will not be liable to you under any statute or in contract, tort or otherwise for any: (i) loss of profits, business revenue, business opportunity, contracts, goodwill and/or anticipated savings; (ii) indirect or consequential loss or damage; or (iii) loss suffered that is avoidable through your reasonable conduct including (where applicable) you backing up all data available and following our reasonable advice in relation to any Services we provide to you, which arises out of or in relation to these Terms.</li>
                <li>13.5 Subject to Paragraph 13.1, our total aggregate liability to you under or in connection with these Terms (whether such liability arises under any statute or in contract, tort or otherwise) shall be limited to: (i) where, pursuant to the applicable Additional Terms and Conditions a charge applies to relevant Service(s), an amount not exceeding the value of charges paid by you to us; or (ii) where, pursuant to the applicable Additional Terms and Conditions no charge applies to relevant Service(s), an amount not exceeding the costs of the call charges incurred for the call in question.</li>
            </ul>
        </li>
        <li>Indemnity
            <p>You shall defend, indemnify and hold harmless us and our respective officers, and directors against all losses, costs, damages, and expenses (including legal costs and disbursements on a solicitor and client basis) suffered or incurred and arising out of or in connection with any claim or demand made or threatened arising out of or in relation to:</p>
            <ul>
                <li>- <strong>(i)</strong> the use by you or any of your Participants of the Services (including the use of any Software) in breach of these Terms;</li>
                <li>- <strong>(ii) </strong> any breach or alleged breach of any representation, warranty or obligation given by you;</li>
                <li>- <strong>(iii)</strong> or any claims or actions brought against us arising out of or related to the use of the Services by you or your Participants.</li>
            </ul>
        </li>
        <li>Variation
            <ul>
                <li>15.1 By accepting these Terms you acknowledge and agree that we may make changes to these Terms from time to time. When these changes are made, we will make a new copy of these General Terms and Conditions and/or Additional Terms and Conditions available at <a title="Powwownow Terms and Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>">www.powwownow.co.uk/Terms-and-Conditions</a>.</li>
                <li>15.2 By accepting these Terms you acknowledge and agree that if you use the Services after the date on which these Terms have changed, we will treat your continued use of the Services as acceptance of the amended and updated Terms.</li>
            </ul>
        </li>
        <li>Your privacy and personal information
            <p>For information about our data protection practices, please read our Privacy Policy which can be viewed at <a title="Privacy Policy" href="<?php echo url_for('@privacy'); ?>">www.powwownow.co.uk/Privacy</a>. The Privacy Policy explains how we process your personal information and protects your privacy when you use our Services and submit your information or data to us. You will be asked to confirm your acceptance of the terms of our Privacy Policy. By reading and accepting the terms of the Privacy Policy you agree to the use of your data in accordance with our privacy practices. By using our Website, you consent to such processing and you warrant, represent and undertake that all data provided by you is accurate, up to date and complete.</p>
        </li>
        <li>Confidential information
            <p>You shall hold in confidence all information concerning our business and affairs that we provide to you and which is designated as confidential or which by its nature is confidential. You shall not disclose such information to any third party and shall, immediately following our request, either destroy or return all such information to us.</p>
        </li>
        <li>Circumstances beyond our control
            <p>We will not be liable to you for any breach of or delay in the performance of our obligations under these Terms to the extent that the breach is directly or indirectly due to circumstances beyond our reasonable control, which shall include fire, flood, storm, other natural event, act of God, explosion, lock-out, strikes, civil disturbance or war.</p>
        </li>
        <li>Third party rights
            <p>No third party shall have any rights under or in connection with these Terms by virtue of the Contracts (Rights of Third Parties) Act 1999.</p>
        </li>
        <li>Waiver
            <p>No delay or failure by us in exercising or enforcing any right or remedy under these Terms will be deemed to be a waiver of any such right or remedy, nor will that failure operate to bar the exercise or enforcement of such right or remedy at any future time.</p>
        </li>
        <li>Severability
            <p>If at any time any provision of these Terms is or becomes illegal, invalid or unenforceable in any respect under the law of any jurisdiction, that shall not affect or impair the legality, validity or enforceability in that jurisdiction of any other provision of these Terms, or the legality, validity or enforceability under the law of any other jurisdiction of that or any other provision of these Terms.</p>
        </li>
        <li>Governing law
            <ul>
                <li>22.1 These Terms are governed by and shall be construed in accordance with English law and, except as set out in Paragraph 22.2, you and we hereby submit to the exclusive jurisdiction of the English courts.</li>
                <li>22.2 If you fail to pay us on time for any monies due to us under these Terms then you acknowledge and agree that we may bring a claim against you for non-payment in any jurisdiction in which you or your assets are located.</li>
                <li>22.3 We reserve the right to seek interim relief against you (such as an injunction) through the courts of England and Wales and any other jurisdiction to protect our rights and interests, or to enforce any of your obligations arising pursuant to these Terms.</li>
            </ul>
        </li>
        <li>Disputes
            <p>We always try to give you the best service we can so if you have a grievance with us please let us know and we will try to sort it out quickly and amicably. All grievances should be written down by you and sent to us at the our registered office at Via-Vox Limited, Vectra House, 36 Paradise Road, Richmond, Surrey TW9 1S.</p>
        </li>
        <li>Keeping these Terms
            <p>We do not separately file these General Terms and Conditions or any Additional Terms and Conditions. All our contracts are concluded in English. You can always access the latest version of these General Terms and Conditions at this page, and the Additional Terms and Conditions applicable to each particular Service by clicking on the hyperlinks provided at Paragraph 1.3. Please make a durable copy of these General Terms and Conditions and any applicable Additional Terms and Conditions by printing and/or saving a downloaded copy on your own computer.</p>
        </li>
        <li>Contacting us
            <p>To contact us please phone us on +44 (0)20 3398 0398 or email us at customer.services@powwownow.com. Our registered address is at Via-Vox Limited, Vectra House, 36 Paradise Road, Richmond, Surrey TW9 1SE.</p>
        </li>
    </ol>
    <p>VERSION September 2012</p>
</div>