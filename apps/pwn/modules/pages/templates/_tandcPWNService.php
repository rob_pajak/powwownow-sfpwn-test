<div class="terms-conditions-container-item item-2">
    <h2>Powwownow Service additional terms and conditions</h2>
    <p>These Additional Terms and Conditions apply in respect of our Powwownow Service, and apply in addition to our General Terms and Conditions. Please read these Additional Terms and Conditions together with our <a title="Powwownow Terms and Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>">General Terms and Conditions</a> carefully before Registering for and/or using our Powwownow Service.</p>
    <ol>
        <li>Definitions
            <ul>
                <li>1.1 Unless the context otherwise requires and except for those words and expressions defined in these Additional Terms and Conditions, words and expressions defined in the General Terms and Conditions shall have the same meanings where used in these Additional Terms and Conditions.</li>
                <li>1.2 References to Paragraph numbers in these Powwownow Service Additional Terms and Conditions refer, unless expressly stated otherwise, to Paragraphs contained in these Powwownow Service Additional Terms and Conditions.</li>
                <li>1.3 In these Powwownow Service Additional Terms and Conditions, unless the context otherwise requires, "Network Operator Call Charge" means the price charged to the caller by the network operator.</li>
            </ul>
        </li>
        <li>Scope of the Powwownow Service
            <ul>
                <li>2.1 Our Powwownow Service gives you the ability to have simultaneous telephone calls with other Participants via the telephone network.</li>
                <li>2.2 As more particularly described in these Powwownow Service Additional Terms and Conditions, the Powwownow Service is a free and easy to use conference call service with no booking or billing fees and Powwownow does not charge you any kind of fee to register or use the service. You just pay for the Network Operator Call Charges, which appear on your phone bill in the same way as any other call. Once you have Registered on our Website we will provide you with a PIN and dial in number.</li>
                <li>2.3 You may then share the PIN, dial-in number and start time of the conference call with your other call Participants and at the agreed time, you and your Participants may dial-in, enter the PIN and start conference calling.</li>
            </ul>
        </li>
        <li>Registration process, when our contract with you begins, and your right to cancel
            <ul>
                <li>3.1 If you wish to use our Powwownow Service you must Register with us online at <a title="Powwownow" href="/">http://www.powwownow.co.uk</a>.</li>
                <li>3.2 To Register, you must provide a current valid email address in the registration box shown on screen. Please then review our General Terms and Conditions and these Powwownow Service Additional Terms and Conditions (together with the Additional Terms and Conditions applicable to any other Service that you wish to use) and, providing that you accept and agree to be bound by such Terms, check the ["I have read and agree to the terms and conditions and the privacy policy"] box and then click on the ["generate my PIN"] button.</li>
                <li>3.3 Once you have submitted your Registration details we will issue you with a PIN. This will be displayed on our Website in the ["Your access PIN is"] box. The dial-in number that you must use for the Powwownow Service will also be displayed on the Website. It is at this point that our contract with you begins, and we will provide the Powwownow Service to you from the point that we confirm your PIN.</li>
                <li>3.4 We will also send you a confirmatory email setting out your user details.</li>
                <li>3.5 The email address that you provide to us will be used by us to communicate service messages and for marketing purposes (in accordance with the terms of our Privacy Policy and General Terms and Conditions).</li>
            </ul>
        </li>
        <li>Your right to cancel the Powwownow Service at any time
            <ul>
                <li>You may stop using the Powwownow Service at any time, and cancel your contract with us without liability by contacting us using the contact information provided at Paragraph 25 of our General Terms and Conditions.</li>
            </ul>
        </li>
        <li>Charges for the Powwownow Service
            <ul>
                <li>5.1 We do not charge you directly for the use of the Powwownow Service.</li>
                <li>5.2 Each user of the Service, including you and your Participant(s), will be charged the prevailing Network Operator Call Charge rate for calls to the dial-in number applicable to the Service that you use.</li>
                <li>5.3 All users (including your invited Participants) will be invoiced for the Network Operator Call Charge on their standard telephone bill issued by their telephone network operator at the prevailing Network Operator Call Charge rate for calls to the dial-in number.</li>
                <li>5.4 We always advise that you should check with your telephone network operator to confirm the applicable Network Operator Call Charge rate for the dial-in number that we give to you.</li>
                <li>5.5 There are no cancellation or booking charges.</li>
            </ul>
        </li>
        <li>Refund Policy for the Plus Service
            <ul>
                <li>Credit refunds will be dealt with by contacting our Customer Services department on 020 3398 0398.</li>
            </ul>
        </li>
    </ol>
    <p>VERSION March 2011</p>
</div>