<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'How to conference call')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'How To Conference Call'),'title' => __('How To Conference Call'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('How To Conference Call'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>If this is your first time chairing a conference call meeting, or you’re new to <a title="voice conferencing" href="<?php echo url_for('@voice_conferencing');?>">voice conferencing</a> altogether, you will be extra-eager to ensure that you have a great first teleconference. You might think that participants are more focussed in a face-to-face meeting and that these are always the best option, but a structured conference call is nearly always more productive than a face-to-face meeting as long as you do a little forward planning!</p>
                <p>If you’re totally new to <a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services');?>">teleconferencing services</a> familiarise yourself with the medium before scheduling the meeting. You want to be sure that you know how to work the service and that the call quality will be high – luckily Powwownow’s conference call service is easy to use, reliable and, because we are a telecommunications company in our own right, our call quality is second to none. Once you’re comfortable with the service you’ll be able to assist any other conference calling newbies who join the discussion.</p>
                <p>When arranging your conference call, make sure that all your participants are aware of the date and time of the meeting and have their dial-in number and PIN. You can do this quickly and easily using the Powwownow telephone conference Scheduler. If you have <a title="international conference call" href="<?php echo url_for('@international_conference_call');?>">international conference call</a> participants, the Scheduler invitation tool will make sure they have the appropriate dial-in number for their country.</p>
                <p>To get the best results, you should be thinking of your teleconference as if it was any other meeting, so prepare an agenda and take notes throughout the discussion or record the call for distribution later. Make sure you stick to the agenda and don’t let your participants wander off course, as this can be easy for them to do on a telephone meeting when there’s no body language to indicate it’s time to move on.</p>
                <p>If you follow these simple steps you’ll be on your way to a successful phone conference call in no time at all!</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>