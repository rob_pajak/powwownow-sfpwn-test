<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Top 10 Tips for Web Conferencing'),
                'headingTitle' => __('Top 10 Tips for Web Conferencing'),
                'headingSize' => 'xxl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <p>For those more used to holding telephone <a title="conference call" href="<?php echo url_for('@conference_call');?>">conference calls</a> or face-to-face meetings, the ins and outs of web conferencing can be a little daunting. Figuring out how to hold a successful online meeting can be a challenge without a helping hand and that’s where Powwownow steps in! With just a little bit of preparation, a web conference can prove even more productive than any face-to-face meeting ever could, so here’s some tips to keep in mind for your next web meeting.</p>
            <ol class="top_ten_tips_for_conference_calling">
                <li><span>Open your web meeting earlier than its scheduled start time - this can allow participants who are used to traditional <a title="conference call services" href="<?php echo url_for('@conference_call_services');?>">conference calls services</a> to get to grips with the interface.</span></li>
                <li><span>Be a courteous host – welcome each participant and once everyone has arrived go over the purpose of the meeting and the agenda if you have one.</span></li>
                <li><span>Keep it clean – close all unnecessary programmes, non-work related internet tabs or confidential files and folders.</span></li>
                <li><span>Don’t be a bandwidth hog – close all bandwidth hogging background applications. There’s nothing worse than timing out everyone’s connections mid-meeting because you didn't bother to do this!</span></li>
                <li><span>Change your resolution – a resolution of 1024x786 should be viewable on all modern monitors.</span></li>
                <li><span>Don’t move too fast – no matter how fast your internet connection is, there will always be a difference between what you see and what other people are seeing. Work around 50% slower to combat latency issues.</span></li>
                <li><span>This is relevant for web conferencing as you still need to use a phone in conjunction with a computer for audio. You could maybe talk about the benefits of headsets or handsfree.</span></li>
                <li><span>Treat the next meeting like any other meeting – prepare in advance and give the web call your full attention.</span></li>
                <li><span>Consider your surroundings – if you’re at home, close the door and don’t let children or animals run riot in the background. At the office, try to take the web conference somewhere quiet.</span></li>
                <li><span>Customise and brand your web conference – if you’re meeting with a client include their logo in your <a title="web conferencing" href="<?php echo url_for('@web_conferencing'); ?>">web conferencing</a> branding.</span></li>
            </ol>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>