<div class="clearfix" id="page"><!-- column -->
<div class="position_content" id="page_position_content">
<div class="clearfix colelem" id="pu1899"><!-- group -->
    <div class="clearfix browser_width grpelem" id="u1899"><!-- column -->
        <img class="colelem" id="u1905-4" src="/sfimages/the-workplace-cant/u1905-4.png" alt="David" width="320" height="58"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1940"><!-- image -->
            <img class="block" id="u1940_img" src="/sfimages/the-workplace-cant/david-mobile.png" alt="" width="130" height="323"/>
        </div>
        <img class="colelem" id="u1909-4" src="/sfimages/the-workplace-cant/u1909-4.png" alt="25%" width="320" height="66"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1928"><!-- image -->
            <img class="block" id="u1928_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
        </div>
        <img class="colelem" id="u1911-4" src="/sfimages/the-workplace-cant/u1911-4.png" alt="16%" width="320" height="66"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1932"><!-- image -->
            <img class="block" id="u1932_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
        </div>
        <img class="colelem" id="u1913-4" src="/sfimages/the-workplace-cant/u1913-4.png" alt="13%" width="320" height="67"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1936"><!-- image -->
            <img class="block" id="u1936_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
        </div>
        <img class="colelem" id="u1915-4" src="/sfimages/the-workplace-cant/u1915-4.png" alt="33%" width="320" height="66"/><!-- rasterized frame -->
    </div>
    <div class="clearfix browser_width grpelem" id="u1900"><!-- column -->
        <img class="colelem" id="u1903-6" src="/sfimages/the-workplace-cant/u1903-6.png" alt="The anatomy of a Workplace ‘Can’t’
(i.e. an apathetic and unhelpful colleague) according to a poll of 2,000 UK&#45;based respondents: " width="320" height="250"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1901"><!-- image -->
            <img class="block" id="u1901_img" src="/sfimages/the-workplace-cant/sarah-stats-mobile.png" alt="" width="133" height="317"/>
        </div>
        <img class="colelem" id="u1908-4" src="/sfimages/the-workplace-cant/u1908-4.png" alt="5’7" width="320" height="66"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1926"><!-- image -->
            <img class="block" id="u1926_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
        </div>
        <img class="colelem" id="u1910-4" src="/sfimages/the-workplace-cant/u1910-4.png" alt="31%" width="320" height="66"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1930"><!-- image -->
            <img class="block" id="u1930_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
        </div>
        <img class="colelem" id="u1912-4" src="/sfimages/the-workplace-cant/u1912-4.png" alt="44" width="320" height="67"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1934"><!-- image -->
            <img class="block" id="u1934_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
        </div>
        <img class="colelem" id="u1914-4" src="/sfimages/the-workplace-cant/u1914-4.png" alt="19%" width="320" height="66"/><!-- rasterized frame -->
        <div class="clip_frame colelem" id="u1938"><!-- image -->
            <img class="block" id="u1938_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
        </div>
        <img class="colelem" id="u1916-4" src="/sfimages/the-workplace-cant/u1916-4.png" alt="54%" width="320" height="67"/><!-- rasterized frame -->
    </div>
    <img class="grpelem" id="u1904-4" src="/sfimages/the-workplace-cant/u1904-4.png" alt="Sarah" width="320" height="58"/><!-- rasterized frame -->
    <img class="grpelem" id="u1906-6" src="/sfimages/the-workplace-cant/u1906-6.png" alt="The most common  Workplace ‘Can’t’ name" width="320" height="44"/><!-- rasterized frame -->
    <img class="grpelem" id="u1907-6" src="/sfimages/the-workplace-cant/u1907-6.png" alt="The most common name  for a male workplace Can’t" width="320" height="44"/><!-- rasterized frame -->
    <img class="grpelem" id="u1917-6" src="/sfimages/the-workplace-cant/u1917-6.png" alt="Average height of  Workplace Can’t" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1918-6" src="/sfimages/the-workplace-cant/u1918-6.png" alt="Cited him as having  grey hair" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1919-4" src="/sfimages/the-workplace-cant/u1919-4.png" alt="Cited her as brunette" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1920-6" src="/sfimages/the-workplace-cant/u1920-6.png" alt="Estimated his height as  being between 5'9 and 5'11" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1921-6" src="/sfimages/the-workplace-cant/u1921-6.png" alt="Average age of  Workplace Can’t" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1922-4" src="/sfimages/the-workplace-cant/u1922-4.png" alt="The majority said that he worked in their employer’s HR department" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1923-4" src="/sfimages/the-workplace-cant/u1923-4.png" alt="Based in HR Department" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1924-6" src="/sfimages/the-workplace-cant/u1924-6.png" alt="Guessed that he is aged  between 45 and 54" width="320" height="57"/><!-- rasterized frame -->
    <img class="grpelem" id="u1925-4" src="/sfimages/the-workplace-cant/u1925-4.png" alt="Said they were female" width="320" height="57"/><!-- rasterized frame -->
    <div class="clearfix browser_width grpelem" id="u1942"><!-- column -->
        <div class="clip_frame colelem" id="u1949"><!-- image -->
            <img class="block" id="u1949_img" src="/sfimages/the-workplace-cant/logo.png" alt="" width="197" height="59"/>
        </div>
        <img class="colelem" id="u1943-4" src="/sfimages/the-workplace-cant/u1943-4.png" alt="The identity of the Workplace" width="317" height="41"/><!-- rasterized frame -->
    </div>
    <div class="clip_frame grpelem" id="u1944"><!-- image -->
        <img class="block" id="u1944_img" src="/sfimages/the-workplace-cant/bubble-mobile.png" alt="" width="270" height="114"/>
    </div>
    <div class="clip_frame grpelem" id="u1946"><!-- image -->
        <img class="block" id="u1946_img" src="/sfimages/the-workplace-cant/sarah-mobile.png" alt="" width="103" height="246"/>
    </div>
    <img class="grpelem" id="u1948-4" src="/sfimages/the-workplace-cant/u1948-4.png" alt="An analysis of the most obstructive and unsympathetic people in the Great British workplace, based on national research in conjunction with OnePoll." width="217" height="127"/><!-- rasterized frame -->
</div>
<div class="clearfix colelem" id="pu1751"><!-- group -->
<div class="browser_width grpelem" id="u1751"><!-- simple frame --></div>
<div class="clearfix browser_width grpelem" id="u1752"><!-- column -->
    <div class="position_content" id="u1752_position_content">
        <img class="colelem" id="u1753-8" src="/sfimages/the-workplace-cant/u1753-8.png" alt="How us Brits consider dealing with the  workplace Can’t: " width="320" height="188"/><!-- rasterized frame -->
        <div class="clearfix colelem" id="u1758"><!-- group -->
            <div class="clip_frame grpelem" id="u1759"><!-- image -->
                <img class="block" id="u1759_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1761-6" src="/sfimages/the-workplace-cant/u1761-6.png" alt="40%
Scream and shout silently or in private, out of sheer frustration" width="308" height="122"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1766"><!-- group -->
            <div class="clip_frame grpelem" id="u1768"><!-- image -->
                <img class="block" id="u1768_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1767-8" src="/sfimages/the-workplace-cant/u1767-8.png" alt="39%
Change the workplace seating plan  to be further away from the Can’t " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1774"><!-- group -->
            <div class="clip_frame grpelem" id="u1776"><!-- image -->
                <img class="block" id="u1776_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1775-6" src="/sfimages/the-workplace-cant/u1775-6.png" alt="36%
Seek new employment " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1778"><!-- group -->
            <div class="clip_frame grpelem" id="u1780"><!-- image -->
                <img class="block" id="u1780_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1779-10" src="/sfimages/the-workplace-cant/u1779-10.png" alt="30%
Approach the Can’ts superior  in an attempt to cut him/her out  of work processes and protocol " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1786"><!-- group -->
            <div class="clip_frame grpelem" id="u1788"><!-- image -->
                <img class="block" id="u1788_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1787-10" src="/sfimages/the-workplace-cant/u1787-10.png" alt="22%
Change profession or  career path entirely " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1799"><!-- group -->
            <div class="clip_frame grpelem" id="u1800"><!-- image -->
                <img class="block" id="u1800_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1802-8" src="/sfimages/the-workplace-cant/u1802-8.png" alt="22%
Compete with that  person professionally " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1762"><!-- group -->
            <div class="clip_frame grpelem" id="u1763"><!-- image -->
                <img class="block" id="u1763_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1765-8" src="/sfimages/the-workplace-cant/u1765-8.png" alt="22%
Consider working from  home/remote working " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1770"><!-- group -->
            <div class="clip_frame grpelem" id="u1771"><!-- image -->
                <img class="block" id="u1771_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1773-10" src="/sfimages/the-workplace-cant/u1773-10.png" alt="20%
Call in sick in a bid to avoid  a meeting with the Can’t " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1782"><!-- group -->
            <div class="clip_frame grpelem" id="u1784"><!-- image -->
                <img class="block" id="u1784_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1783-10" src="/sfimages/the-workplace-cant/u1783-10.png" alt="18%
Seek advice or buy tools/ instructional material in a  bid to ‘bust stress’ " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1790"><!-- group -->
            <div class="clip_frame grpelem" id="u1791"><!-- image -->
                <img class="block" id="u1791_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1793-6" src="/sfimages/the-workplace-cant/u1793-6.png" alt="15%
Consider counselling " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clearfix colelem" id="u1795"><!-- group -->
            <div class="clip_frame grpelem" id="u1796"><!-- image -->
                <img class="block" id="u1796_img" src="/sfimages/the-workplace-cant/bottom-bubble-mobile.png" alt="" width="308" height="167"/>
            </div>
            <img class="grpelem" id="u1798-6" src="/sfimages/the-workplace-cant/u1798-6.png" alt="12%
Relocate or move away " width="308" height="121"/><!-- rasterized frame -->
        </div>
        <div class="clip_frame colelem" id="u1754"><!-- image -->
            <img class="block" id="u1754_img" src="/sfimages/the-workplace-cant/cant-mobile-bottom.png" alt="" width="129" height="241"/>
        </div>
    </div>
</div>
<div class="clip_frame grpelem" id="u1756"><!-- image -->
    <img class="block" id="u1756_img" src="/sfimages/the-workplace-cant/office-bottom.png" alt="" width="218" height="256"/>
</div>
<div class="clearfix browser_width grpelem" id="u1806"><!-- column -->
    <img class="colelem" id="u1807-8" src="/sfimages/the-workplace-cant/u1807-8.png" alt="A ‘Can’t’ Watchers Guide" width="320" height="84"/><!-- rasterized frame -->
    <div class="clearfix colelem" id="u1827"><!-- column -->
        <img class="colelem" id="u1828-4" src="/sfimages/the-workplace-cant/u1828-4.png" alt="Scotland" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1829-6" src="/sfimages/the-workplace-cant/u1829-6.png" alt="Female, named Elizabeth,  working in Customer Services" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1830"><!-- column -->
        <img class="colelem" id="u1832-4" src="/sfimages/the-workplace-cant/u1832-4.png" alt="North East" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1831-4" src="/sfimages/the-workplace-cant/u1831-4.png" alt="Female, named Sophie/Elizabeth/Victoria (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1833"><!-- column -->
        <img class="colelem" id="u1834-4" src="/sfimages/the-workplace-cant/u1834-4.png" alt="North West" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1835-6" src="/sfimages/the-workplace-cant/u1835-6.png" alt="Female, named Emily,  working in Customer Services" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1836"><!-- column -->
        <img class="colelem" id="u1837-4" src="/sfimages/the-workplace-cant/u1837-4.png" alt="Northern Ireland" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1838-6" src="/sfimages/the-workplace-cant/u1838-6.png" alt="Male, named Peter/Thomas  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1839"><!-- column -->
        <img class="colelem" id="u1841-6" src="/sfimages/the-workplace-cant/u1841-6.png" alt="Yorkshire
and Humberside" width="320" height="72"/><!-- rasterized frame -->
        <img class="colelem" id="u1840-6" src="/sfimages/the-workplace-cant/u1840-6.png" alt="Male, named Paul/John  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1842"><!-- column -->
        <img class="colelem" id="u1843-4" src="/sfimages/the-workplace-cant/u1843-4.png" alt="West Midlands" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1844-6" src="/sfimages/the-workplace-cant/u1844-6.png" alt="Female, named Julia,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1809"><!-- column -->
        <img class="colelem" id="u1811-4" src="/sfimages/the-workplace-cant/u1811-4.png" alt="East Midlands" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1810-6" src="/sfimages/the-workplace-cant/u1810-6.png" alt="Male, named David, working  in Customer Services" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1812"><!-- column -->
        <img class="colelem" id="u1814-4" src="/sfimages/the-workplace-cant/u1814-4.png" alt="Wales" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1813-6" src="/sfimages/the-workplace-cant/u1813-6.png" alt="Female, named Sarah/Beth/Lucy  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1815"><!-- column -->
        <img class="colelem" id="u1816-4" src="/sfimages/the-workplace-cant/u1816-4.png" alt="East Anglia" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1817-6" src="/sfimages/the-workplace-cant/u1817-6.png" alt="Female, named Sarah,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1818"><!-- column -->
        <img class="colelem" id="u1819-4" src="/sfimages/the-workplace-cant/u1819-4.png" alt="London" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1820-6" src="/sfimages/the-workplace-cant/u1820-6.png" alt="Female, named Julia,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1821"><!-- column -->
        <img class="colelem" id="u1823-4" src="/sfimages/the-workplace-cant/u1823-4.png" alt="South East" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1822-6" src="/sfimages/the-workplace-cant/u1822-6.png" alt="Female, named Emma,  working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1824"><!-- column -->
        <img class="colelem" id="u1825-4" src="/sfimages/the-workplace-cant/u1825-4.png" alt="South West" width="320" height="42"/><!-- rasterized frame -->
        <img class="colelem" id="u1826-6" src="/sfimages/the-workplace-cant/u1826-6.png" alt="Female, named Emily/Sarah  (tied scores), working in HR" width="320" height="71"/><!-- rasterized frame -->
    </div>
</div>
<img class="grpelem" id="u1808-8" src="/sfimages/the-workplace-cant/u1808-8.png" alt="According to respondents in  each area, the identities of common workplace Can’ts  across the UK." width="320" height="114"/><!-- rasterized frame -->
<div class="clearfix browser_width grpelem" id="u1845"><!-- column -->
    <img class="colelem" id="u1846-4" src="/sfimages/the-workplace-cant/u1846-4.png" alt="From a list of personality traits, the top five qualities of the workplace ‘Can’t’ were: " width="320" height="255"/><!-- rasterized frame -->
    <div class="clip_frame colelem" id="u1871"><!-- image -->
        <img class="block" id="u1871_img" src="/sfimages/the-workplace-cant/65-piechart.png" alt="" width="160" height="159"/>
    </div>
    <div class="clip_frame colelem" id="u1849"><!-- image -->
        <img class="block" id="u1849_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
    </div>
    <img class="colelem" id="u1859-4" src="/sfimages/the-workplace-cant/u1859-4.png" alt="Argumentative" width="320" height="65"/><!-- rasterized frame -->
    <div class="clip_frame colelem" id="u1869"><!-- image -->
        <img class="block" id="u1869_img" src="/sfimages/the-workplace-cant/59-piechart.png" alt="" width="160" height="159"/>
    </div>
    <div class="clip_frame colelem" id="u1851"><!-- image -->
        <img class="block" id="u1851_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
    </div>
    <img class="colelem" id="u1860-4" src="/sfimages/the-workplace-cant/u1860-4.png" alt="Confident" width="320" height="65"/><!-- rasterized frame -->
    <div class="clip_frame colelem" id="u1847"><!-- image -->
        <img class="block" id="u1847_img" src="/sfimages/the-workplace-cant/47-piechart.png" alt="" width="160" height="159"/>
    </div>
    <div class="clip_frame colelem" id="u1853"><!-- image -->
        <img class="block" id="u1853_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
    </div>
    <img class="colelem" id="u1861-4" src="/sfimages/the-workplace-cant/u1861-4.png" alt="Deceitful" width="320" height="65"/><!-- rasterized frame -->
    <div class="clip_frame colelem" id="u1867"><!-- image -->
        <img class="block" id="u1867_img" src="/sfimages/the-workplace-cant/46-piechart.png" alt="" width="160" height="159"/>
    </div>
    <div class="clip_frame colelem" id="u1855"><!-- image -->
        <img class="block" id="u1855_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
    </div>
    <img class="colelem" id="u1862-4" src="/sfimages/the-workplace-cant/u1862-4.png" alt="Unproductive" width="320" height="65"/><!-- rasterized frame -->
    <div class="clip_frame colelem" id="u1865"><!-- image -->
        <img class="block" id="u1865_img" src="/sfimages/the-workplace-cant/41-piechart.png" alt="" width="160" height="159"/>
    </div>
    <div class="clip_frame colelem" id="u1857"><!-- image -->
        <img class="block" id="u1857_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
    </div>
    <img class="colelem" id="u1863-4" src="/sfimages/the-workplace-cant/u1863-4.png" alt="Committed " width="320" height="65"/><!-- rasterized frame -->
</div>
<div class="clearfix browser_width grpelem" id="u1873"><!-- column -->
    <img class="colelem" id="u1874-4" src="/sfimages/the-workplace-cant/u1874-4.png" alt="The workplace ‘Can’t’ might be seen with:" width="319" height="181"/><!-- rasterized frame -->
    <div class="clearfix colelem" id="u1875"><!-- column -->
        <div class="clip_frame ose_pre_init colelem" id="u1877"><!-- image -->
            <img class="block" id="u1877_img" src="/sfimages/the-workplace-cant/horn-glasses.png" alt="" width="213" height="214"/>
        </div>
        <img class="ose_pre_init colelem" id="u1876-4" src="/sfimages/the-workplace-cant/u1876-4.png" alt="Horn Rim Glasses" width="213" height="70"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1879"><!-- column -->
        <div class="clip_frame ose_pre_init colelem" id="u1881"><!-- image -->
            <img class="block" id="u1881_img" src="/sfimages/the-workplace-cant/big-nose.png" alt="" width="209" height="222"/>
        </div>
        <img class="ose_pre_init colelem" id="u1880-4" src="/sfimages/the-workplace-cant/u1880-4.png" alt="Large Nose" width="209" height="39"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1883"><!-- column -->
        <div class="clip_frame ose_pre_init colelem" id="u1884"><!-- image -->
            <img class="block" id="u1884_img" src="/sfimages/the-workplace-cant/moustache.png" alt="" width="208" height="223"/>
        </div>
        <img class="ose_pre_init colelem" id="u1886-4" src="/sfimages/the-workplace-cant/u1886-4.png" alt="Moustache" width="208" height="39"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1887"><!-- column -->
        <div class="clip_frame ose_pre_init colelem" id="u1888"><!-- image -->
            <img class="block" id="u1888_img" src="/sfimages/the-workplace-cant/sandals.png" alt="" width="219" height="208"/>
        </div>
        <img class="ose_pre_init colelem" id="u1890-6" src="/sfimages/the-workplace-cant/u1890-6.png" alt="Sandals all  year round" width="213" height="50"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1891"><!-- column -->
        <div class="clip_frame ose_pre_init colelem" id="u1892"><!-- image -->
            <img class="block" id="u1892_img" src="/sfimages/the-workplace-cant/red-face.png" alt="" width="209" height="225"/>
        </div>
        <img class="ose_pre_init colelem" id="u1894-4" src="/sfimages/the-workplace-cant/u1894-4.png" alt="Red Face" width="207" height="70"/><!-- rasterized frame -->
    </div>
    <div class="clearfix colelem" id="u1895"><!-- column -->
        <div class="clip_frame ose_pre_init colelem" id="u1896"><!-- image -->
            <img class="block" id="u1896_img" src="/sfimages/the-workplace-cant/tattoo.png" alt="" width="208" height="208"/>
        </div>
        <img class="ose_pre_init colelem" id="u1898-4" src="/sfimages/the-workplace-cant/u1898-4.png" alt="Tattoos" width="208" height="39"/><!-- rasterized frame -->
    </div>
</div>
</div>
<div class="clearfix browser_width colelem" id="u1794"><!-- column -->
    <div class="clip_frame colelem" id="u1803"><!-- image -->
        <img class="block" id="u1803_img" src="/sfimages/the-workplace-cant/logo.png" alt="" width="197" height="59"/>
    </div>
    <div class="pointer_cursor clearfix colelem" id="u1805-9"><!-- content -->
        <a class="block" href="<?php echo url_for('@homepage'); ?>" target="_blank"></a>
        <p>Home&nbsp;&nbsp; |&nbsp;&nbsp; <a class="nonblock" href="<?php echo url_for('@conference_call') ?>" target="_blank">Conference Call </a>&nbsp; |&nbsp;&nbsp; <a class="nonblock" href="<?php echo url_for('@contact_us'); ?>" target="_blank">Contact Us</a></p>
    </div>
    <div class="colelem" id="u2052"><!-- custom html -->
        <span class="st_facebook_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="http://www.facebook.com/sharer.php?u=www.powwownow.co.uk/Conference-Call/The-Workplace-Cant" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - Facebook/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/facebook_32.png" alt="Facebook Share"/></a>
            </span>
        </span>
        <span class="st_twitter_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="http://twitter.com/share?text=The%20identity%20of%20the%20workplace%20Can%27t%20via%20%40Powwownow%20%23iamacan" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - Twitter/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/twitter_32.png" alt="Twitter Share"/></a>
            </span>
        </span>
        <span class="st_linkedin_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fwww.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant&amp;title=The%20Workplace%20Can%E2%80%99t%20%E2%80%93%20Be%20a%20Can%20with%20Powwownow%2C%20Let%E2%80%99s%20Get%20it%20Done&amp;summary=We%20completed%20a%20survey%20in%20association%20with%20OnePoll%20to%20find%20out%20the%20identity%20of%20%E2%80%98Can%E2%80%99ts%E2%80%99%20within%20the%20UK.%20Find%20out%20the%20characteristics%20of%20a%20Can%E2%80%99t%20and%20where%20in%20the%20UK%20to%20find%20them" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - LinkedIn/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/linkedin_32.png" alt="LinkedIn Share"/></a>
            </span>
        </span>
        <span class="st_googleplus_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="https://plus.google.com/share?url=www.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - GooglePlus/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/googleplus_32.png" alt="Google+ Share"/></a>
            </span>
        </span>
        <span class="st_email_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="mailto:?subject=The%20Workplace%20Can%E2%80%99t%20%E2%80%93%20Be%20a%20Can%20with%20Powwownow%2C%20Let%E2%80%99s%20Get%20it%20Done&amp;body=www.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant" onclick="dataLayer.push({'event': 'WorkplaceCant - Share - Email/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/email_32.png" alt="Email Share"/></a>
            </span>
        </span>
    </div>
</div>
<div class="verticalspacer"></div>
</div>
</div>
<!-- JS includes -->
<script type="text/javascript">
    if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
<script type="text/javascript">
    window.jQuery || document.write('\x3Cscript src="/sfjs/pages/the-workplace-cant/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
<script src="/sfjs/pages/the-workplace-cant/museutils.js?4291592202" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.tobrowserwidth.js?3842421675" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.scrolleffects.js?4006931061" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.watch.js?4068933136" type="text/javascript"></script>
<!-- Other scripts -->
<script type="text/javascript">
    $(document).ready(function() { try {
        Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
        $('.browser_width').toBrowserWidth();/* browser width elements */
        Muse.Utils.prepHyperlinks(true);/* body */
        $('#u1877').registerOpacityScrollEffect([{"in":[-Infinity,2924.77],"opacity":0,"fade":189},{"in":[2924.77,2924.77],"opacity":100},{"in":[2924.77,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1876-4').registerOpacityScrollEffect([{"in":[-Infinity,2924.77],"opacity":0,"fade":189},{"in":[2924.77,2924.77],"opacity":100},{"in":[2924.77,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1881').registerOpacityScrollEffect([{"in":[-Infinity,3248.17],"opacity":0,"fade":188.82},{"in":[3248.17,3248.17],"opacity":100},{"in":[3248.17,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1880-4').registerOpacityScrollEffect([{"in":[-Infinity,3248.17],"opacity":0,"fade":188.82},{"in":[3248.17,3248.17],"opacity":100},{"in":[3248.17,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1884').registerOpacityScrollEffect([{"in":[-Infinity,3550.57],"opacity":0,"fade":188.65},{"in":[3550.57,3550.57],"opacity":100},{"in":[3550.57,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1886-4').registerOpacityScrollEffect([{"in":[-Infinity,3550.57],"opacity":0,"fade":188.65},{"in":[3550.57,3550.57],"opacity":100},{"in":[3550.57,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1888').registerOpacityScrollEffect([{"in":[-Infinity,3792.88],"opacity":0,"fade":138.98},{"in":[3792.88,3792.88],"opacity":100},{"in":[3792.88,Infinity],"opacity":100,"fade":50.87}]);/* scroll effect */
        $('#u1890-6').registerOpacityScrollEffect([{"in":[-Infinity,3792.88],"opacity":0,"fade":138.98},{"in":[3792.88,3792.88],"opacity":100},{"in":[3792.88,Infinity],"opacity":100,"fade":50.87}]);/* scroll effect */
        $('#u1892').registerOpacityScrollEffect([{"in":[-Infinity,4008.04],"opacity":0,"fade":479.93},{"in":[4008.04,4008.04],"opacity":100},{"in":[4008.04,Infinity],"opacity":100,"fade":170}]);/* scroll effect */
        $('#u1894-4').registerOpacityScrollEffect([{"in":[-Infinity,4008.04],"opacity":0,"fade":479.93},{"in":[4008.04,4008.04],"opacity":100},{"in":[4008.04,Infinity],"opacity":100,"fade":170}]);/* scroll effect */
        $('#u1896').registerOpacityScrollEffect([{"in":[-Infinity,4424.98],"opacity":0,"fade":599.93},{"in":[4424.98,4424.98],"opacity":100},{"in":[4424.98,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u1898-4').registerOpacityScrollEffect([{"in":[-Infinity,4424.98],"opacity":0,"fade":599.93},{"in":[4424.98,4424.98],"opacity":100},{"in":[4424.98,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        Muse.Utils.fullPage('#page');/* 100% height page */
        Muse.Utils.showWidgetsWhenReady();/* body */
        Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
    } catch(e) { Muse.Assert.fail('Error calling selector function:' + e); }});
</script>