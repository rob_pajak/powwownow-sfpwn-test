<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'What is a conference call')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'What is a Conference Call'),'title' => __('What is a Conference Call'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('What is a Conference Call'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>Face-to-face business meetings have been carried out for centuries, but <a title="telephone conferencing" href="<?php echo url_for('@telephone_conferencing'); ?>">telephone conferencing</a> is the new, exciting kid on the block. With the medium still being fairly new some people are still fairly unfamiliar with conference calling, and the ins and outs of this fantastic and efficient means of communication.</p>
                <p>A <a title="free conference call" href="<?php echo url_for('@free_conference_call'); ?>">conference call</a> is a meeting, conducted over the phone using audio, between two or more people and usually for the purposes of discussing a particular topic. There are many different terms used other than “conference call” which mean the same thing, such as teleconference, telephone conferencing, telephone meeting, voice conference, audio conference, or conferencing call. Although you can use a landline telephone to hold a Powwownow conference call, you can also use a mobile phone or a computer with an internet connection and Skype.</p>
                <p>Businesses use conference calls instead of face-to-face meetings for a variety of reasons. Voice calls are often used for purely practical reasons such as trying to save on time and money by keeping staff in the office or at home rather than travelling to meetings, dealing with international clients who they cannot meet face-to-face, and increasing efficiency by conducting focused discussions. However, <a title="audio conferencing" href="<?php echo url_for('@audio_conferencing'); ?>">audio conferencing</a> and web conferencing are also frequently used to hold press conferences, make company-wide announcements, conduct focus groups, provide technical support to customers and colleagues, and for staff training purposes. Charities also use them to connect their aid workers during times of crisis and for training up staff in remote locations, so it’s not just businesses that can benefit from teleconferences! There are all sorts of reasons you might choose to use conference calling over a face-to-face meeting. So no matter what business you’re in, or who you need to communicate with, having access to a great free conference call service will always be a significant advantage!</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>