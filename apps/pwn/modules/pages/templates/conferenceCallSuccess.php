<?php slot('page_gcx') ?>
<?php include_component('commonComponents', 'googleContentExperiment', array('experimentId' => '90448072-1')); ?>
<?php end_slot(); ?>

<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsERefactored', array(
            'header' => 'Free Conference Call Service',
            'subHeader' => array("Powwownow is the UK's leading free conference call service provider. Whether it's web conferencing or audio phone conferencing you’re after... our instant conference call service is available 24/7, with as many participants as required. There is no booking, no billing, no fuss!"),
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource,
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Conferencing Call with Powwownow!</h3>
                <p style="text-align: justify;">Holding a <strong>conference call</strong> should be simple and affordable! Here at Powwownow we offer just that. Our conference call service surpasses all other competition and saves you time and money. With excellent call quality and loads of extra features for free, there really isn’t a better option.</p>
                <p style="text-align: justify;">Features include call recordings, a scheduler tool, <a title="Free Web Conferencing" href="<?php echo url_for('@web_conferencing'); ?>">screen share</a> and a large range of low-cost international dial-in numbers. Plus all participants pay is the cost of their own phone call which is added to their standard telecoms bill, so no hidden charges!</p>
                <p style="text-align: justify;">At Powwownow we are truly committed to meeting all of our customers’ voice conferencing needs.  Our dedicated customer service team pride themselves on taking a friendly, personal and focused approach - that’s why 97% of our customers would recommend us!</p>
                <div style="text-align: justify; font-weight: bold;">
                    <?php include_component(
                        'commonComponents',
                        'trustPilotFeed',
                        array('showReviews' => true, 'name' => 'conference call')
                    ); ?>
                </div>
            </div>
        </div>

        <?php include_component('commonComponents', 'trustPilotBanner'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Conference Call</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>