<div class="row">
    <div class="small-12 medium-12 large-12 columns">
        <header class="logo">
            <a href="<?php echo url_for('@homepage');?>">
                <img src="/cx2/img/mobile/340_logo_strap_line_white.png" width="200" alt="Powwownow">
            </a>
        </header>
    </div>
</div>
<div class="row">
    <div class="small-12 medium-8 large-7 medium-centered large-centered columns">
        <section class="forgotten-password-container">
            <div id="js.ko.forgotten.password.container" data-bind="template: {name: 'forgotten.password.container.data' }"></div>
            <script type="text/html" id="forgotten.password.container.data">

            <!-- ko ifnot: ui.success.displaySuccess -->
                <h2 class="section-title">Trouble signing in?</h2>
                <p class="section-text">
                    Don't worry, we all forget things from time to time. Give us your email address below and we'll send
                    you a helpful email to reset your password.
                </p>
                <form class='cx_form' data-bind="submit: doLogin" novalidate>

                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <input class="email-input" type='email' data-bind="initBind: { attr: 'value', property: model.email, binding: 'textInput' }" placeholder="Enter your email address" value="<?php echo $userEmail; ?>" />
                        </div>
                    </div>
                    <div class="row error-message" data-bind="fadeVisible: ui.errors.message().length > 0">
                        <div class="small-12 medium-12 large-12 columns">
                            <p data-bind="html: ui.errors.message()"></p>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-12 medium-12 large-12 columns">
                            <img id="loader" src="/cx2/img/throbber.gif" alt="loading..." class="loader" data-bind="visible: ui.isEnabled.loader"/>
                            <button type="submit" class="right button-blue-set-a" data-bind="enable: ui.isEnabled.submit">Get a new password</button>
                        </div>
                    </div>
                </form>
            <!-- /ko -->

            <!-- ko if: ui.success.displaySuccess -->
                <h2 class="section-title">On it’s way...</h2>
                <p class="section-text">You'll receive an email in a few moments with a link to help you reset your password.</p>
            <!-- /ko -->
            </script>
        </section>
    </div>
</div>
<div class="row">
    <div class="small-12 medium-8 large-7 medium-centered large-centered columns">
        <section class="new-customer-container">
            <p class="section-text"><span class="enlarge font_green">New Customer?</span> <a href="/?register=true">Click here</a> to generate your PIN</p>
        </section>
    </div>
</div>