<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Careers'),
                'headingTitle' => __('Careers'),
                'headingSize' => 's',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <h3 class="margin-bottom-10 lightgreen">What are you looking for?</h3>
        <p class="margin-bottom-10"><strong>A good salary?</strong><br/>How about a basic of £ 16k, with OTE of £ 30k in your first year?</p>
        <p class="margin-bottom-10"><strong>Career progression and training?</strong><br/>Powwownow is growing - and fast. And we want you to move up to that senior role as soon as possible, and will support and train you all the way.</p>
        <p class="margin-bottom-10"><strong>Fulfilment?</strong><br/>We love what we do and get enormous satisfaction from doing it well. Get ready to love Mondays.</p>
        <p class="margin-bottom-20"><strong>Location?</strong><br/>In the heart of leafy Richmond, the centre of social and commercial life in West London.</p>

        <h3 class="margin-bottom-10 lightgreen">What are we looking for?</h3>
        <p class="margin-bottom-10"><strong>Drive.</strong><br/>If you're determined to succeed, we're determined to help you.</p>
        <p class="margin-bottom-10"><strong>Intelligence.</strong><br/>If you're going to do well, you need your wits about you. You don't need a double-first from Oxford, but if you think fast, you'll progress faster.</p>
        <p class="margin-bottom-20"><strong>Willingness to learn.</strong><br/>A good attitude and keenness to learn is essential.  We like positive people. The Powwownow glass is always half-full.</p>

        <h3 class="margin-bottom-10 lightgreen">That little extra...</h3>
        <p class="margin-bottom-20">Impress us by sending us a CV that marks you out as the kind of person who deserves an opportunity like this.</p>

        <h3 class="margin-bottom-10 lightgreen">What's the next step?</h3>
        <p class="margin-bottom-20">Why not call us on 0203 398 0398? We would be delighted to hear from you, although you can of course email us your CV at <strong><a onclick="dataLayer.push({'event': 'Careers - Mail/onClick'});" href="mailto:recruitment@powwownow.com">recruitment@powwownow.com</a></strong>.</p>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
