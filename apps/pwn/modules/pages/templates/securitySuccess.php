<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(link_to('About Us', url_for('@about_us')), 'Security'),
                'breadcrumbsTitle' => __('Security'),
                'headingTitle' => __('Security'),
                'headingSize' => 's',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p>Security is extremely important to us at Powwownow and we know that it’s a top priority for our customers as well. We know that you want to conduct your <a title="free conference call" href="<?php echo url_for('@free_conference_call'); ?>">free conference call</a> safe in the knowledge that no one is going to be listening in on your conversation without your knowledge or consent and that your call recordings will be in safe hands. That’s why we provide a number of features that help protect your conference calls from prying eyes and ears.</p>
        <p>Firstly there’s our secure PIN system – no one can enter your teleconference without a PIN, which you will need to provide to each call participant. PINs are stored on our secure database and are only accessed by Powwownow’s staff if and when you’re having issues accessing your calls.</p>
        <p>Every participant on a Powwownow conference call is asked to state their name before entering the meeting – this is recorded and played back when they enter the call. This means you can hear whenever anyone enters or leaves the conference call. If you’re ever in any doubt, you can easily request a “head count” which will review the number of participants on the call. Powwownow also gives you the power to lock your telephone conference calls once all your participants arrive which stops anyone else joining the call, PIN or no PIN!</p>
        <p>Our Premium <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference calling</a> service offers dual PINs allowing the Chairperson tighter control over a call it also offers time-limited PINs which will expire after a call ends. This gives you ultimate control over who can access your confidential conference calls.</p>
        <p>If you choose to record your free conference call, you can rest assured that the only person who will hear your recording, if you choose, is you. Your recordings are only accessible via myPowwownow using your email and password, and as responsible<a title="conference call providers" href="<?php echo url_for('@conference_call_providers'); ?>"> conference call providers</a>, Powwownow store them on a secure server that is only ever accessed automatically. Technical staff at Powwownow cannot listen in on a call in progress and will only ever enter a conference call with your explicit consent.</p>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
