<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'Branding')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Branded Conference Calls'),'title' => __('Branded Conference Calls'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Branding'), 'headingSize' => 's', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>If conference calls are a major part of how you communicate with your employees, clients or the media, you'll want to go the extra mile to make every teleconference you hold seem just that little bit special. With Powwownow's Business Bundle <a title="conference call services" href="<?php echo url_for('@conference_call_services');?>">conference call services</a> you can customise your conference call to better represent your brand.</p>
                <p>As a regular user of Powwownow, you will have the option to customise your conference calls, such as creating your own branded welcome message to your calls that will allow you to introduce participants to your brand, your new product or even simply the subject of your teleconference; you can also add personal branding to Powwownow web conferences. In today's competitive marketplace it's more important than ever to stand out from the crowd and with Powwownow it's as easy as one, two, three to add that personal touch to your <a title="free conference call" href="<?php echo url_for('@free_conference_call');?>">free conference call</a>.</p>
                <p>Bored of the same old same old on-hold music and want to kick it up a notch for your next conference call? Powwownow will put the rhythm back into your step with an array of fantastic on-hold music choices to fit your company's personality. Whether you like cool and classical or something a bit more contemporary, we're sure to have the perfect tune to compliment your brand and add something extra to your <a title="conference call" href="<?php echo url_for('@conference_call');?>">conference call</a>!</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>