<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Meeting Cost Calculator'),
                'headingTitle' => __('Meeting Cost Calculator'),
                'headingSize' => 'xxl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <p>Conference calling not only improves workplace efficiency but also saves companies lots of money per year. We figured you'd think we're all talk, so we did a bit of digging to reveal just how much meetings cost companies.</p>
            <p>In our 2013 survey we found that the average UK business person spends £3,433 on travel to meetings every year. They also spend 45 hours travelling to meetings per year. That's the equivalent of a full working week!</p>
            <p>Sure, face-to-face meetings may be a necessity sometimes, but we found that 41 hours a year are spent just on niceties. How very British!</p>
        </div>
        <?php include_partial('commonComponents/meetingCostCalculator');?>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
