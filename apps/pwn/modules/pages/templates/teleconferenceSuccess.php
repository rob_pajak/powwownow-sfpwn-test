<?php slot('page_gcx') ?>
<?php include_component('commonComponents', 'googleContentExperiment', array('experimentId' => '90448072-4')); ?>
<?php end_slot(); ?>

<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsERefactored', array(
            'header' => "The Free Teleconference Service",
            'subHeader' => array("Powwownow's telephone conference service is free - participants only pay the cost of their own call, which is added to their standard telecoms bill."),
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Teleconferencing with Powwownow</h3>
                <p style="text-align: justify;">Travelling to meetings can be painfully inefficient so why not save time, costs and the <a title="Going Green" href="<?php echo url_for("@going_green"); ?>">environment</a> and have a teleconference with Powwownow.</p>
                <p style="text-align: justify;">The Powwownow telephone conferencing service is free and easy to use so you’ll be talking with your colleagues and clients in the UK or around the globe in no time. We offer quality and reliable teleconferencing services and all each participant pays is the cost of their own call - nothing more.</p>
                <p style="text-align: justify;">Registration is simple and we offer a host of extra features all for free, including a wide range of <a title="International Numbers and Rates" href="<?php echo url_for("@international_number_rates"); ?>">international access numbers</a>, a <a  title="Screen Sharing" href="<?php echo url_for("@web_conferencing"); ?>">screen sharing facility</a>, call recording and a handy conference scheduler tool.&nbsp; There’s a reason why we’re Europe’s fastest growing teleconferencing provider.</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Teleconference</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>