<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'Charities')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Calls for Charities'),'title' => __('Conference Calls for Charities'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Calls for Charities'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>When you're running a charity every penny counts and the last thing you want is unnecessary expenditure. Unlike other <a title="conference call providers" href="<?php echo url_for('@conference_call_providers');?>">conference call providers</a>, Powwownow doesn't charge for their teleconferencing service and won't lock you into an airtight contract or force you to buy unnecessary conference call equipment. All your charity needs to start conference calling with Powwownow is a standard telephone, a dial-in number and a Powwownow PIN - that's it! You won't be charged costly bridging fees either and the only cost to your organisation is the standard cost of a two-way telephone call.</p>
                <p>Working with an uncertain income stream is a constant challenge for charities, teleconferencing gives you the opportunity to hold quick, cheap meetings as and when they are needed. If you have international workers or benefactors, our <a title="international conference call" href="<?php echo url_for('@international_conference_call');?>">international conference calls</a> can facilitate meetings without the organisation having to divert funds away from their intended purpose to fund travel, accommodation or catering costs. If you have workers in disaster zones or remote locations, our global dial-in numbers and SkypeOut partnership can let you conduct conference calls without pulling your staff out of the places they are most needed. Our reservationless teleconferences also mean they can get in touch immediately if they need arises, which can be a huge benefit for humanitarian organisations.</p>
                <p><a title="telephone conferencing" href="<?php echo url_for('@telephone_conferencing');?>">Telephone conferencing</a> generally uses up less staff time than face-to-face meetings, this can be a significant help to under-manned organisations. Conference calls encourage discussion, co-operation and the sharing of ideas, moving away from the traditional meeting structure which can be frustrating to charity workers who want to spend more time on the ground and less time in the office. A teleconference will allow your staff to stay at the coalface and spend less time being pulled away from the cause they care about and more time devoted to your charity's mission.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>