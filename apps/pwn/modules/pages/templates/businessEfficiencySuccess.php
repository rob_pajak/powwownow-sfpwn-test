<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Business Efficiency'),
                'headingTitle' => __('Business Efficiency'),
                'headingSize' => 'l',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <h3 class="lightgreen">Conference calling..... saving you time, money, stress and guilt!</h3>
            <p class="margin-bottom-20">So we all know that sometimes you can’t beat a face-to-face meeting, but when it comes to enhancing the operation of your business, improving the productivity and effectiveness of your staff and achieving greater business efficiency <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>">conference calls</a> are the perfect solution. Whether internally or client facing businesses, small or large, should be able to deliver messages to co-workers, staff, suppliers, prospective and current clients clearly and on time. Plus any steps that can be taken to cut costs while maintaining - or even boosting - productivity should be grasped with both hands.</p>

            <h3 class="lightgreen">Save costs</h3>
            <p>Conference calling, unlike face-to-face meetings, allows businesses to instantly save on costs such as travel, venue hire, accommodation and other direct costs such as biscuits, which were previously a big drain on company budgets.</p>
            <p>In addition to saving on costs associated with meetings, free conference calling services available through conference call providers like Powwownow, further reduce the cost of meetings by eliminating booking and bridging fees. The caller only pays the cost of their own call which appears on their standard telephone bill – nothing more! This is now even the case when on the go; simply use the UK mobile shortcode (87373 <span>for 12.5p a min + VAT</span>) instead of the 0844 number and start benefiting from our exclusive reduced call rates. Furthermore, international callers can dial-in using <a title="International Number Rates" href="<?php echo url_for('@international_number_rates'); ?>">local access numbers</a> which are cheaper than a long-distance call, even if there are just two people on the call!</p>
            <p class="margin-bottom-20"><a title="Powwownow Costs" href="<?php echo url_for('@costs'); ?>"><strong>What Powwownow costs</strong></a></p>

            <h3 class="lightgreen">Save time</h3>
            <p>Time; the most valuable resource that a company can have and it should be optimised by all means.</p>
            <p>The time your business can save on travel can be priceless. Assuming you had 50 people participating in a <a title="Meetings" href="<?php echo url_for('@conference_call_solutions_meetings'); ?>">meeting</a> (which is not likely, but great as an example) and travelling time totalled 2 hours per person for a return trip, your business could effectively save 100 hours by having a conference call!</p>
            <p class="margin-bottom-20">The time saving benefits of teleconferencing are clear; business people can attend meetings without the unnecessary stress of travelling and have more time to focus on the preparation and delivery of the meeting content.</p>

            <h3 class="lightgreen">Enhance competency</h3>
            <p class="margin-bottom-20">Conference calling can also give you that competitive advantage you need to move quickly in the ever-changing marketplace and stay ahead of the competition:</p>
            <ul class="chevron-green">
                <li class="png">Discuss problems and share ideas, regardless of geographical distance</li>
                <li class="png">Get group buy-in to make urgent decisions at very short notice</li>
                <li class="png">Improve energy levels by avoiding commutes</li>
                <li class="png">Carry out effective minute taking with <a title="Security" href="<?php echo url_for('@security'); ?>">call recording</a></li>
                <li class="png">Make presentations and conduct <a title="Training" href="<?php echo url_for('@conference_call_solutions_training'); ?>">training</a> and product demonstrations in conjunction with <a title="Web Conferencing" href="<?php echo url_for('@web_conferencing'); ?>">web conferencing</a></li>
                <li class="png">Deliver sales pitches quickly, clearly and efficiently</li>
                <li class="png">Communicate much more accurately than via e-mail, using the tone and inflection in voice</li>
            </ul>

            <h3 class="lightgreen">Enhance security</h3>
            <p>Unlike face-to-face meetings and conference calls over the web, ‘tapping’ into conference calls is impossible due to the <a title="Security" href="<?php echo url_for('@security'); ?>">secure PIN process</a>. Only those supplied with the unique PIN will be able to access the call.</p>
            <p class="margin-bottom-20">Furthermore, most conference call providers – Powwownow included – have a ‘lock’ feature which enables the call to be locked once all participants have dialled in. Plus the ‘roll call’ feature will let the chairperson know who is definitely on the call, just to double check there are no unwanted gatecrashers!</p>

            <h3 class="lightgreen">Go green</h3>
            <p>Conference calling not only improves workplace efficiency and reduces costs for businesses; it also <a title="Going Green" href="<?php echo url_for('@going_green'); ?>">reduces CO2 emissions</a>. By reducing the impact made on the environment by driving, flying and taking other forms of transport to meetings, conference calling is not only good for the environment, but good for business, as it allows you to promote your green credentials.</p>
            <p class="margin-bottom-20">Click to review how you can reduce your <a title="Going Green" href="<?php echo url_for('@going_green'); ?>">Carbon Footprint</a></p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>