<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
            'header' => 'Better Conferencing with Powwownow',
            'subHeader' => 'Experience better conferencing with Powwownow’s free and easy conference call service. No one offers free conferencing like we do.',
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource,
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Better Conferencing with Powwownow!</h3>
                <p style="text-align: justify;">Try better conference calling with better quality at a better price. Powwownow’s voice conferencing service is quick to sign up to, easy to use and offers you a great range of free features which most of our competitors charge for. Each participant only pays the cost of their own phone call - nothing more! What can be better than that?</p>
                <p style="text-align: justify;">Some of the many features and benefits of Powwownow’s free conference call service include low-cost <a title="International Numbers Rates" href="<?php echo url_for('@international_number_rates'); ?>">international dial-in numbers</a>, free call recordings, a <a title="Free Mobile App" href="http://itunes.apple.com/us/app/powwownow/id359486614?mt=8">free mobile app</a> and a free web conferencing facility which makes screen sharing, online presentations and training a breeze. There’s a reason why we’re the fastest growing conference call provider in Europe!</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Better Conferencing</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>