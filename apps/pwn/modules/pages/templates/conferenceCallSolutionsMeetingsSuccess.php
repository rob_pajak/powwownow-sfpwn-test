<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSolutionsLeftNav', array('activeItem' => 'Meetings')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Solutions', 'Employee and Client Meetings'),'title' => __('Employee and Client Meetings'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Employee and Client Meetings'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>If you need to bring people together quickly then a conference call can be just the ticket. Powwownow’s free <a title="conference call services" href="<?php echo url_for('@conference_call_services'); ?>">conference call services</a> are a simple, cost-effective and reliable method of getting your key decision makers together at short notice whenever and wherever with international access from over 100 countries across the world. With Powwownow’s reservationless conference calls, all you and your colleagues will need to get started is a PIN, dial-in number and a telephone – that’s it! There’s no fancy, overpriced equipment to worry about, or need to wait for an allocated call time, and you’re guaranteed access to an easy and dependable conference calling service.</p>
                <p>Worried about the time and cost of travelling to a potential international client’s offices? Powwownow’s low cost <a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services'); ?>">teleconferencing services</a> allow you to hold those all important meetings without the hassle and frustration of a trek across the globe. For the small price of a phone call, you can meet with prospective clients to establish the strength of the lead without having to leave the office and later allow you to keep in touch more frequently than face to face meetings would allow. When you work in a company where every penny counts, international conference calls enable you and your clients to keep up- to- date with all the latest news and developments at the drop of a hat whilst saving you a bundle in travel costs.</p>
                <p>Face-to-face contact is important for any business, but when it’s not possible <a title="audio conferencing" href="<?php echo url_for('@audio_conferencing'); ?>">audio conferencing</a> with Powwownow will keep you connected to those who matter when it matters most. Whether you’re chasing sales leads, liaising with investors, holding meetings with your existing employees or training up new ones; if you choose Powwownow as your conference call provider you’ll never have to worry about keeping in touch again.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>