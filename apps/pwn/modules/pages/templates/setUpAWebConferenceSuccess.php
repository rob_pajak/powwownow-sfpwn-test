<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Setting up a Web conference'),
                'headingTitle' => __('Setting up a Web conference'),
                'headingSize' => 'xl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p>Being able to support your conference calls through presentations and screen share can be invaluable and web conferencing with Powwownow makes adding people to your <a title="free conference call" href="<?php echo url_for('@free_conference_call'); ?>">free conference calls</a> as easy as one, two, three! By following these simple steps you can have your free web conference up and running in next to no time.</p>
        <p>To start using Powwownow web conferencing, you will need to sign up to receive a Powwownow PIN. If you’re already registered then you can skip this part and jump right into downloading our web conferencing programme. Once installed, open the application and enter your registration details to sign in – now you’re nearly ready to go!</p>
        <p>Once signed in, starting a web conference couldn't be easier. Simply click the blue “Start” button to open up the web conference options box. This will contain the meeting room details that your participants will need to join the conversation. Simply click “Copy” beside the details to copy them to your clipboard and then paste them into an email to your participants. Participants join the web meeting via&nbsp;<a title="yuuguu" href="http://powwownow.yuuguu.com">http://powwownow.yuuguu.com</a> and will be asked to enter the <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference call</a> PIN and their name. Once this is done they will automatically be brought into your net meeting room and can now view your screen.</p>
        <p>Although by default only you will be able to control your desktop, participants can request remote control by clicking the “Request Access” button at the top of the screen. A pop up will then appear on your screen and you will be prompted to accept or decline access – while the participant is in control of your desktop you can still assume control by using the keyboard or mouse and can withdraw their access at any time. Once you’re finished with <a title="web conferencing" href="<?php echo url_for('@web_conferencing'); ?>">web conferencing</a>, simply close the conference window to end the session.</p>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
