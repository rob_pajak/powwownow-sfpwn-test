<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(
                    array('About Us', '@about_us'),
                    'Testimonials'
                ),
                'breadcrumbsTitle' => 'Testimonials',
                'headingTitle' => 'Testimonials',
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <p>At Powwownow we pride ourselves on being able to deliver one of the best <a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services'); ?>">teleconferencing services</a> in the UK, but we also aim to deliver excellent customer service on top of this. We&rsquo;re honoured to be able to say that <strong>98 per cent of our customers would recommend us</strong> to others, and we continue to work towards making that 100 per cent. To that end, we regularly conduct surveys and talk to our customers to make sure they&rsquo;re completely satisfied with the service we&rsquo;re providing. Here&rsquo;s just a few of the things people have to say about us:</p>
        </div>
        <div class="clearfix">
            <?php include_partial('pages/testimonialVideos', array()); ?>
        </div>
        <?php include_partial('pages/testimonialTestimonials', array()); ?>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>