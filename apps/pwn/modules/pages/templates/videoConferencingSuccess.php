<?php include_component('commonComponents', 'header', array('headerHint' => true)); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div id="tabs-grid" style="position: relative" class="grid_18">
            <h1>Video Conferencing</h1>
            <div class="subheading">Experience high quality HD video conferencing with Powwownow. Bring your meetings to life with a video conference.</div>
            <div class="homepage-tab-1 tab-1-green png active" id="tab-1">
                <div class="rockwell font-large white png">Video</div>
                <div style="display:none" class="tab-shadow png"></div>
            </div>
            <div class="homepage-tab-2 tab-2-grey png" id="tab-2">
                <div class="rockwell font-large white">Demo</div>
                <div class="tab-shadow png"></div>
            </div>
            <div class="homepage-tab-3 tab-3-grey png" id="tab-3">
                <div class="rockwell font-large white">Get Started</div>
                <div class="tab-shadow png"></div>
            </div>
            <div>
                <div id="tabs-container">
                    <div class="white" id="tab-web-conferencing-showtime">
                        <h2 class="rockwell white">Powwownow Video Conferencing</h2>
                        <p>If you can&apos;t get together in person, a Powwownow Video Conference is the next best thing, <a title="Conference Call" class="white" href="<?php echo url_for('@conference_call');?>">taking conference calling to the next level.</a></p>
                        <p>Powwownow video conferencing offers crystal-clear HD visuals, rock-solid connections and a host of flexible features making it simple to hold meetings, presentations or interviews regardless of location.</p>
                        <p>No new hardware is required. Powwownow video works on PC, Mac, phone and tablet with a standard internet connection. Welcome 25 participants with eight on screen at once.</p>
                        <ul class="web-conferencing-showtime-icons-list">
                            <li>
                                <span class="icon icon-white"><span class="icon-question-mark-grey png"></span></span>
                                <a class="white tooltip-showtime-in-action-link" href="#">What can you use it for?</a>
                                <div id="tooltip-showtime-in-action" class="tooltip">
                                    <div>
                                        <div class="tooltip-arrow-left png"></div>
                                        <a class="sprite tooltip-close floatright tooltip-showtime-in-action-close" href="#"><span class="sprite-cross-white png"></span></a>
                                        <span class="icon icon-grey floatleft"><span class="icon-question-mark-white png"></span></span>
                                        <h4 style="margin-left: 5px; position:relative; top:6px;" class="rockwell grey-dark floatleft">Powwownow Video in action</h4>
                                        <div class="clear"></div>
                                        <ul style="margin-top:20px;" class="chevron-green grey-dark">
                                            <li class="png">Deliver effective training to individuals without having to travel</li>
                                            <li class="png">Present to customers and collegues without the need to leave your office</li>
                                            <li class="png">Stick to the Agenda: keep meetings brief and more focused than face-to-face meetings</li>
                                            <li class="png">Conduct interviews regardless of the location of the candidate</li>
                                            <li class="png">Hold impromptu meetings to deliver company announcements</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <img src="/sfimages/vidyo-logo-small.jpg" alt="VidYo" width="100" style="position: absolute; right: 10px; bottom: 10px"/>
                    </div>
                    <div class="white" id="tab-web-conferencing-demo">
                        <div class="grid_sub_17">
                            <?php include_component(
                                'commonComponents',
                                'hTML5OutputVideo',
                                array(
                                    'config' => array(
                                        'width'    => 460,
                                        'height'   => 267,
                                        'autoplay' => false,
                                        'controls' => true
                                    ),
                                    'video'  => array(
                                        'image' => '/sfvideo/webConferencing/poster.png',
                                        'mp4'   => '/sfvideo/WelcomeToVidyo.mp4',
                                        'webm'  => '/sfvideo/WelcomeToVidyo.webm',
                                        'ogg'   => '/sfvideo/WelcomeToVidyo.ogv'
                                    ),
                                    'called' => 'html'
                                )
                            ); ?>
                        </div>
                        <div class="grid_sub_7">
                            <p style="text-align: left;">Powwownow Video delivers simple, affordable, high quality video meetings.</p>
                            <p style="text-align: left;">It's as easy as 1, 2, 3!</p>
                            <ul class="features-list-one-column">
                                <li class="png pie_first-child">Login, download and install the application </li>
                                <li class="png">Search or invite your contacts</li>
                                <li class="png">Start your Powwownow Video conference!</li>
                            </ul>
                            To find out more press play on the video demo.
                        </div>
                    </div>
                    <div class="white" id="tab-web-conferencing-get-started">
                        <div id="tab-web-conferencing-get-started-initial-content">
                            <h2 class="rockwell white">Talk to our video specialists<br/></h2>
                            <p style="text-align: left;">If you're looking for a simple, reliable, high quality video conferencing solution to save on time and costs associated with business travel, whilst still meeting face-to-face, contact us today.</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">Powwownow is offering Vidyo Desktop supporting PCs and Macs and also making use of the newly launched VidyoMobile platform, which will provide multi-point video conferencing on iOS or Android-based smart phones and tablet devices.</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">Powwownow Video will deliver the highest quality video conferencing over the Internet, wireless LAN, 3G networks and Internet connections without any boundaries.</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <h2 class="rockwell white" style="text-align: left;">Call us on 0800 022 9930 or +44 (0)20 3398 0900<br /></h2>
                            <p style="text-align: left;">to request a demo or a free 14 day trial or to simply sign up and get started.</p>
                            <p style="text-align: left;">&nbsp;</p>
                        </div>
                        <div id="tab-web-conferencing-get-started-form-response-placeholder"></div>
                    </div>
                </div>
            </div>
        </div>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div id="element4">
            <div class="grid_12 alpha">
                <div id="left-content-container">
                    <h3 class="rockwell grey-dark">Welcome to Powwownow</h3>
                    <br/>
                    <div style="text-align: justify;">
                        Powwownow was founded in 2004 and is now the leading <a title="Free Conference Call" href="<?php echo url_for('@free_conference_call'); ?>">free conference call</a> provider in the UK.
                        <br/>
                        <br/>
                        At Powwownow, we don't believe in doing things the hard way. That's why we offer instant <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>">conference calling</a>, available 24/7, with as many participants as required, wherever they are in the world. And because our service is free, callers only pay the cost of their own phone call, which is added to their standard telecoms bill - nothing more!
                        <br/><br/>
                        Plus, because we are a telecommunications company in our own right, our customers can experience excellent quality calls with the maximum number of features for a low price. That&rsquo;s why <a title="How We Are Different" href="<?php echo url_for('@how_we_are_different'); ?>">no one does conference calling like we do</a>!
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function() { videoConferencingTabs_Init(); } );</script>
        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>
