<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Privacy'),
                'headingTitle' => __('Privacy'),
                'headingSize' => 's',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <ul class="privacy-list">
            <li>
                <h2>1. Introduction</h2>
                <p>This website is owned and run by Via-Vox Limited.</p>
                <p>This privacy policy applies to all of Powwownow´s websites and sets out Via-Vox Limited's commitment to you in respect of how we handle your personal information that we collect or you give us through your use of this website and our services.</p>
                <p>For the purposes of this privacy policy "Via-Vox" means Via-Vox Limited (company number: 04646978) of:<br><br>Vectra House<br>36 Paradise Road<br>Richmond<br>Surrey<br>TW9 1SE</p>
                <p>Telephone Number: +44 (0) 203 398 0398<br>Fax Number: +44 (0) 20 3393 9358</p>
                <p>This privacy policy includes Via-Vox's general privacy terms that apply to all our dealings with you in any capacity. We may amend this privacy policy at any time. Any changes we make to this privacy policy in the future will be posted on this page and will take effect from the date on which the change is posted. Your use of this website and our services after the date a change takes effect will be subject to this privacy policy as amended by the change. We will take your continued use as your acceptance of the change, so if an amendment is not acceptable to you then you should immediately stop using the website and our services.</p>
            </li>
        </ul>
        <div class="privacy-heading">YOUR INFORMATION</div>
        <ul class="privacy-list">
            <li>
                <h2>2. How do we collect information about you?</h2>
            </li>
            <li>
                <p>The personal information we collect and maintain about you is gathered both indirectly (for example, through the website's technology) and directly (for example, when you register to use our services). It is made up of information you give us through your use of our website and through our other communications with you such as by telephone, through our customer service agents and specialist sales consultants or in person. This includes information obtained when you register with us to set up a teleconferencing account or use other products or services offered by us.</p>
                <p>It is solely your choice whether or not you provide this information either by direct or indirect means. We do not share, license or sell information concerning <a href="<?php echo url_for('@homepage'); ?>">www.powwownow.co.uk</a> users with or to third parties in any way other than as disclosed in this privacy policy.</p>
            </li>
            <li>
                <h2>3. What information do we collect about you?</h2>
                <p>Personal information is any information that can be used to identify an individual and the personal information that we collect from you depends on the user package you subscribe to, and may include your name, address, telephone numbers, email address, and payment card number.  We do not collect sensitive personal information from you. You retain the right to withhold any information from us but please be aware that some of our products and services may not be available to you if this information is not provided by you to us.</p>
            </li>
        </ul>
        <div class="privacy-heading">OUR USE OF YOUR INFORMATION</div>
        <ul class="privacy-list">
            <li>
                <h2>4. How do we use your information?</h2>
                <p>By using our website, you consent to your personal information being used by us for the following main purposes:</p>
                <ul>
                    <li>To provide you with the website and our services;</li>
                    <li>To assist in processing your requests for products and services offered by us;</li>
                    <li>To complete your registration for any conferencing packages offered by us, and to communicate with you in relation to those packages;</li>
                    <li>For identification and verification purposes in connection with any of the services or products that may be supplied to you;</li>
                    <li>To contact you regarding your enquiries;</li>
                    <li>To provide you with customer support;</li>
                    <li>For internal analysis and research (in particular, to help us improve the website and our services);</li>
                    <li>To communicate with you regarding any competitions, promotions or other offers which may be of interest to you and for which you may be eligible;</li>
                    <li>To comply with any applicable laws and regulations, including the transfer of personal information to regulatory and government authorities; and</li>
                    <li>To help detect and deal with crime and unsavoury behaviour.</li>
                </ul>
                <p>We may retain and store some of your personal information, including your customer profile information (such as your name and contact details) and transactional information (such as your teleconferencing history) to improve customer satisfaction and to tailor our services to meet your individual needs.</p>
                <p>We may also use and disclose information in aggregate (so that no individuals are identified) for marketing and strategic development purposes.</p>
            </li>
        </ul>
        <div class="privacy-heading">PROTECTING YOUR INFORMATION</div>
        <ul class="privacy-list">
            <li>
                <h2>5. How do we keep your online information secure?</h2>
                <p>We take security issues seriously. We have implemented appropriate steps to help maintain the security of our information systems, processes and website and prevent the accidental destruction, loss or unauthorised disclosure of the information we collect.</p>
                <p>However, whilst we continue to work hard to protect the security and integrity of your personal information, no data transmission over the internet can be guaranteed to be 100% secure and we cannot ensure or warrant the security of any information you transmit to us over the internet. Transmitting personal information over the internet is done at your own risk and you should bear this in mind when using the website and our products and services.</p>
                <p>In order to protect key account and financial information you provide us with online, we use a secure server known as Secure Server Layer (SSL). When you submit this information to our website it is encrypted to provide greater protection. Although we take every measure to protect this information, we make no warranty and offer no undertaking regarding the effectiveness of this encryption.</p>
            </li>
            <li>
                <h2><a name="cookies"></a>6. Does our website use cookies?</h2>
                <p>Yes, our website does use cookies to enhance the site experience.</p>
                <p>A cookie is a simple text file that is stored on your computer or mobile device by a website’s server. This will contain some basic information such as a unique identifier and the site name, along with some digits and numbers. Only that server will be able to retrieve or read the contents of the cookie, with each being unique to your web browser. Using cookies allows for a better site experience as they allow a website to remember things like your preferences or what’s in your shopping basket. Cookies also enable login information to be accessed and stored, allowing you to remain logged into the myPowwownow system when browsing between pages.</p>
                <p>If you are uncomfortable with our use of cookies you can disable them by changing the browser settings. However, please note that if you do disable the use of cookies, it may affect your ability to properly use the website. You can find out more about the way cookies work and how to disable them here: <a href="http://www.cookiecentral.com">http://www.cookiecentral.com</a>.</p>
                <p>There are a variety of different types of cookies used across our website to enhance user experience, either by us or by third-party sites.</p>
                <p><strong>First-party cookies </strong><br>We use cookies to monitor how visitors find and use our website. We report on this activity in aggregate, and use this information solely for the purposes of monitoring and improving our website, services and products. These cookies are only readable by the Powwownow site.</p>
                <p><strong>Third-party cookies</strong><br>We use a variety of widely-used third-party plugins as marketing tools for our own business and to provide convenience tools for our visitors. In our belief the use of these plugins represents current "best practice". We have no direct control over the content of the cookies they create, the use made of any data collected, or the security of that data. These third parties may collect information about your visit to our website, and they may use this with other data in order to serve advertising which may be more relevant to you.</p>
                <p><strong>Session cookies</strong><br>This type of cookie is only temporarily stored on your browser and its deleted from the computer or mobile device once the website is closed.</p>
                <p><strong>Persistent cookies</strong><br>Persistent cookies remain after the web browser is closed, being saved to your device for a longer period of time (usually for at least a year). These are used for repeat visits to remember data that its relevant to more than one browsing sessions, for example, user preferences.</p>
                <p><strong>Flash cookies</strong><br>Adobe Flash Player is commonly used on websites to play video and game content. For this, Adobe uses its own cookies. These work in a different way to others, instead using only one cookie per website to store all relevant information. Although not able to control what information is stored in this one cookie, you can control how much data is remembered. Adobe cookies are manageable through the Flash Player, not a web browser.</p>
            </li>
        </ul>
        <div class="privacy-heading">DISCLOSING YOUR INFORMATION TO THIRD PARTIES</div>
        <ul class="privacy-list">
            <li>
                <h2>7. Information</h2>
            </li>
            <li><br>
                <p><strong>7.1 To whom may we disclose your information?</strong></p>
                <p>We may use third party agents and service providers to collect, hold and process on our behalf your personal information for the purposes set out in this privacy policy. These agents and service providers act on our instructions and will only use your information as we tell them to.</p>
                <p>We may disclose your information to third parties, including the police and other governmental bodies, as required by law or if we think the disclosure may help to detect and deal with crime and/or unsavoury behaviour.</p>
                <p>We may give your information to third parties (such as our subsidiaries, associate companies and/or business associates) to allow them to market their goods and services to you, but only with your express consent.</p>
                <p>We will not sell your information to a third party for marketing purposes, except in the unlikely event that we sell our business, or a substantial part of it; in which case we may transfer your information as part of the sale in order to allow the purchaser to carry on providing some or all of our services to you.</p>
                <p>Other than as set out in this privacy policy, we will not disclose your personal information without your consent unless disclosure is either:</p>
            </li>
            <li>
                <ul>
                    <li>Necessary to prevent a threat to life or health;</li>
                    <li>Necessary to stop or prevent a breach of the website's terms and conditions or any of its policies or usage guidelines for specific products or services;</li>
                    <li>Necessary to prevent, investigate, detect or prosecute attacks on the technical integrity of the website or its network;</li>
                    <li>Necessary in order to protect the rights, property, or safety of Via-Vox or its employees, the users of its website, or the public;</li>
                    <li>Authorised or required by law;</li>
                    <li>Reasonably necessary to enforce the law; or</li>
                    <li>Necessary to investigate any suspected unlawful activity or unsavoury behaviour.</li>
                </ul>
                <p><strong>7.2 Linking to third party sites from the Powwownow website</strong></p>
                <p>You may be able to link to third party sites through our website (for example, to social-networking sites and other information-sharing sites). Other sites that are or may be accessible through our website may have their own privacy and data collection policies, use of data and data disclosure practices. Once you connect to these sites, you should carefully read the security and privacy policies of each of these sites.</p>
                <p>This privacy policy only applies to information collected on or through our website and we are not responsible for the collection of data and the related data and privacy policies and practices of third parties.</p>
            </li>
        </ul>
        <div class="privacy-heading">OTHER TERMS</div>
        <ul class="privacy-list">
            <li>
                <h2>8. Can you see and update the information we hold about you?</h2>
                <p>You have the right to ask for a copy of the information we hold about you (for which we may charge a small fee). If you find at any time that any of the information we hold about you is incorrect then you should promptly notify us and we will correct the inaccuracy. You have the right to ask that your personal information is deleted from our files. We make good faith efforts to honour your reasonable request to access and correct your data if it is inaccurate or delete the data if we are not required to retain it by law or for legitimate purposes. We will respond to your request to access within 30 days.</p>
            </li>
            <li>
                <h2>9. Do our privacy practices apply to disclosures you make to third parties?</h2>
                <p>Please note that this privacy policy addresses only the use and disclosure of information we collect from you. If you give your information to others, whether they are third parties that we introduce you to or third party sites that you visit by clicking on a link on one of our websites, different rules may apply to their use or disclosure of the information you give to them. We do not control the privacy policies of third parties, and you are subject to the privacy policies of those third parties where applicable. We strongly encourage you to ask questions before you disclose your personal information to others.</p>
            </li>
            <li>
                <h2>10. How can you contact us about privacy issues?</h2>
                <p>You can contact us about privacy issues or comment or complain about our privacy practices by contacting us at <a href="mailto:customer.services@powwownow.com">customer.services@powwownow.com</a></p>
            </li>
            <li>
                <h2>11. Law</h2>
                <p>This privacy policy is governed by and shall be construed in accordance with the laws of England and Wales and any disputes which arise under or in connection with this privacy policy shall be determined exclusively by the English Courts sitting in London, United Kingdom.</p>
            </li>
        </ul>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
