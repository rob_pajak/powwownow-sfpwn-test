<?php use_helper('DataUri') ?>

<?php include_component('commonComponents','responsiveLogo');?>

<?php include_component('commonComponents','responsiveCanvas',array('layers'=>array('It\'s your call.','And you\'re about to make the right one.')));?>

<div class="row">
    <div class="small-12 columns">
        <section id="generic-panel-1" class="generic-panel-1 panel border-green rounded-corners">
            <p class="paragraph-font-12">You’re just one step away from getting started with Powwownow’s free and easy conference calling service. That’s right, the service is free – all you pay for is the price of a phone call. Enter your email below and you’re ready to go!</p>
        </section>
    </div>
</div>

<?php include_component('commonComponents','responsiveHowItWorks',array('classes'=>'show-for-large-up'));?>

<div class="row">
    <div class="small-12 columns">
        <section id="its-your-call-get-pin-container" class="its-your-call panel green rounded-corners">
            <p class="font-white form-intro">Our one-step sign-up means you can start conference calling right now</p>
            <form id="its-your-call-get-pin-form" class="its-your-call-get-pin-form parsley" method="post" action="<?php echo url_for("@free_pin_registration"); ?>">
                <div class="form-field">
                    <input id="its-your-call-get-pin-form-email" type="text" name="email" placeholder="Enter email address" autocomplete="off" data-trigger="change" data-type="email" data-required="true" data-error-message="Please enter a valid email address.">
                    <input type="hidden" id="agree-tick" name="agree_tick" value="agreed" />
                    <input type="hidden" id="registration_source" name="registration_source" value="GBR-Its-Your-Call" />
                    <input type="hidden" id="registration_type" name="registration_type" value="iyc" />
                </div>
                <div class="form-action">
                    <input id="its-your-call-email-button" type="submit" class="small yellow button radius" value="Get My Pin" onclick="dataLayer.push({'event': 'Generate Pin - Its-Your-Call/onClick'});">
                </div>
                <div class="form-field">
                    <div class="throbber loading hide"></div>
                </div>
            </form>
            <p class="paragraph-font-11 font-white"> By clicking Get My PIN, you agree to our <a href="<?php echo url_for("@terms_and_conditions"); ?>" class="link">Terms &amp; Conditions</a></p>
        </section>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        registration.basic('its-your-call-get-pin-form', 'its-your-call-get-pin-form-email', 'Its-Your-Call', false, true);
    });
</script>
<div class="row">
    <div class="large-12 large-centered columns">
        <div class="clients hide-for-medium-down">
            <img src="<?php echo getDataURI('/sfimages/cro-lp/recommend.gif'); ?>" alt="98% of our customers recommend us, including:" />
            <img src="<?php echo getDataURI('/sfimages/cro-lp/accenture.jpg'); ?>" alt="Accenture" />
            <img src="<?php echo getDataURI('/sfimages/cro-lp/motorla-solutions.jpg'); ?>" alt="Motorola Solutions" class="margin-left-25px" />
            <img src="<?php echo getDataURI('/sfimages/cro-lp/network-marketing.jpg'); ?>" alt="Network Marketing" class="margin-left-25px"/>
            <img src="<?php echo getDataURI('/sfimages/cro-lp/university-brighton.jpg'); ?>" alt="University of Brighton" class="margin-left-25px"/>
        </div>
    </div>
</div>

<div class="row">
    <div class="small-12 columns">
        <h2 class="hozline"><span class="hozline-text">What You Get</span></h2>
        <section id="what-you-get" class="what-you-get panel border-green rounded-corners">

            <ul class="large-block-grid-2 small-block-grid-1 list-font-12 what-you-get-list clearfix">

                <li class="hide-for-small">
                    <span class="sprite set-a-call-quality"></span>
                    <span class="title">Unbeatable call quality</span>
                    <span class="content">Unlike almost all conference call providers, most of our conference calls use the exact same fibre optic cabling as your landline.</span>
                </li>

                <li>
                    <span class="sprite set-a-scheduling"></span>
                    <span class="title">Free scheduling</span>
                    <span class="content">With Powwownow, you don’t have to book or schedule your conference calls. But if you want to, you can download our plugin for Outlook or use our scheduler tool to organise your calls and invite participants.</span>
                </li>

                <li>
                    <span class="sprite set-a-low-cost"></span>
                    <span class="title">Low-cost international access</span>
                    <span class="content">Access Powwownow from <a href="<?php echo url_for('@international_number_rates'); ?>" target="_blank">over 14 countries</a> around the world - for the cost of a local-rate phone call.</span>
                </li>

                <li>
                    <span class="sprite set-a-web-conference"></span>
                    <span class="title">Free instant web conferencing</span>
                    <span class="content">Share your computer screen instantly. The attendees don't need to download or install anything – all they need is internet access.</span>
                </li>

                <li class="hide-for-small">
                    <span class="sprite set-a-customer-support"></span>
                    <span class="title">Fantastic customer support</span>
                    <span class="content">Got a question? We're fanatical about customer support. (That's probably why 98% of our customers would recommend us.) Call us on <b>020 3398 0398</b>.</span>
                </li>

                <li class="hide-for-small">
                    <span class="sprite set-a-call-recording"></span>
                    <span class="title">Instant call recording</span>
                    <span class="content">Record your conference calls with one touch. Then you can review, download and share them with your colleagues, or keep them for your records.</span>
                </li>
            </ul>

        </section>
    </div>
</div>

<div class="row">
    <div class="small-12 columns">
        <h2 class="hozline"><span class="hozline-text">What It Costs</span></h2>

        <section class="what-it-costs">
            <div class="large-4 columns">
                <ul class="list-font-12 what-it-costs-list">
                    <li class="list-item">
                        <span class="title-a"><strong>You just pay the cost of a local 0844 call</strong></span>
                        <span class="content">That's it - there are no other fees</span>
                    </li>
                    <li class="list-item">
                        <span class="title-a"><strong>There’s no contract or minimum usage</strong></span>
                        <span class="content">You can start and stop as you like</span>
                    </li>
                    <li class="list-item">
                        <span class="title-a"><strong>There’s no monthly billing</strong></span>
                        <span class="content">The local call charges show up on your phone bill</span>
                    </li>
                </ul>
            </div>
            <div class="large-8 columns hide-for-small">
                <h3>How our costs compare</h3>
                <p class="paragraph-font-12">This is what you’d pay for a 1hr conference with 4 participants:</p>
                <img style="height:115px" alt="Unlike other conference call providers, Powwownow doesn't charge you a 'bridging fee' - everyone just pays the same local-rate call" src="<?php echo getDataURI('/sfimages/cro-lp/cost-compare.gif')?>" />
            </div>

        </section>
    </div>
</div>

<?php include_component('commonComponents','responsiveFooter');?>

<?php include_component('homepageG','modalCarousel3EasySteps');?>
<div id="response"></div>
