<?php include_component('commonComponents', 'header'); ?>

<div class="container_24 clearfix">
    <div class="grid_24 clearfix">


        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
            'header' => 'Free Audio Conferencing Service',
            'subHeader' => 'Audio Conferencing with Powwownow is easy with no booking, no billing and no fuss!',
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource,
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Audio Conferencing with Powwownow!</h3>
                <p style="text-align: justify;">In many instances a <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>">conference call</a> can be more productive than a meeting and Powwownow’s free audio conferencing service is the easiest, most cost-effective and reliable way to communicate with several people at the same time. Each participant only pays the cost of their own call, nothing more!</p>
                <p style="text-align: justify;">As well as being a high quality, affordable service, Powwownow also offers you a host of other features for FREE! These include call recordings, a scheduler tool and web conferencing; allowing you to chat, present documents and share your desktop with your fellow conference callers.</p>
                <p style="text-align: justify;">Audio conferencing on the move? Powwownow is always one step ahead of the competition; now providing a <a title="Free Mobile App" href="http://itunes.apple.com/us/app/powwownow/id359486614?mt=8">free mobile app</a> so you can join calls at the touch of a button from wherever you are.</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Audio Conferencing</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>