<?php use_helper('pwnVideo') ?>
<?php use_helper('DataUri') ?>
<?php $logo = getDataURI('/letsGetItDone/gridpak/img/powwownow.png'); ?>
<?php $logoWhite = getDataURI('/letsGetItDone/gridpak/img/powwownow_white.png'); ?>
<div id="get-it-done" class="container get-it-done">
<header class="row inner-container">
    <div class="logo">
        <a href="<?php echo url_for('@homepage')?>" target="_blank"><img src="<?php echo $logo?>" alt="Powwownow"></a>
    </div>
</header>

<article class="row hero-element">
    <section class="inner-container">
        <h1 class="col">Instant, contract-free, hassle-free conference calling.</h1>

        <div class="col mouthy">
            <img alt="I am can" src="letsGetItDone/gridpak/img/mouthy.png" data-interchange="
                    [letsGetItDone/gridpak/img/mouthy_mobile.png, (only screen and (min-width: 1px))],
                    [letsGetItDone/gridpak/img/mouthy.png, (only screen and (min-width: 675px))]
                ">
        </div>

        <div class="col hero-form">
            <p>Start conference calling right now with our one-step sign-up.</p>
            <form id="register-top" class="registration-form" action="<?php echo url_for('@free_pin_registration');?>" method="post">
                <input type="checkbox" name="agree_tick" value="agreed" class="hide" checked>
                <input id="registration_source" name="registration_source" value="GBR-Get-It-Done" class="hide">
                <input id="registration_type" name="registration_type" value="gid" class="hide">
                <input id="lets-get-it-done-registration-email" type="text" name="email" class="xlarge-input" placeholder="Email Address">
                <input id="submit_button" type="submit" value="Get it done" class="button">
                <div class="terms-conditions">
                    <p>
                        In registering for our service you are agreeing to our
                        <a href="<?php echo url_for('@terms_and_conditions');?>" target="_blank">terms and conditions</a>.
                    </p>
                </div>
                <div id="error" class="error">
                    <!--                        Please enter email address-->
                </div>
                <div class="loading hide"></div>
            </form>
        </div>
        <div class="col hero-benefits">
            <ul>
                <li>Only pay 4.3p per minute (depending on your network provider). Nothing else.</li>
                <li>Crystal clear quality delivered through fibre optic cabling (oooooooh!).</li>
                <li>No waiting, no booking.<br /> Available 24/7.</li>
                <li>Record, playback and share your calls.</li>
            </ul>
        </div>
    </section>
</article>

<article class="row get-started hide-for-small hide-for-smaller">
    <section class="inner-container">
        <h2 class="text-center">Get started instantly</h2>
        <?php echo output_video (
            array (
                'config' => array(
                    'width' => 400,
                    //'height' => 260,
                    'autoplay' => false,
                    'controls' => true
                ),
                'video' => array(
                    'image' => '/sfimages/video-stills/3-steps-video.jpg',
                    'mp4'   => '/sfvideo/pwn_3steps460x260_LR.mp4',
                    'webm'  => '/sfvideo/pwn_3steps460x260_LR.webm',
                    'ogg'   => '/sfvideo/pwn_3steps460x260_LR.ogv'
                )
            )
        ); ?>
    </section>
</article>

<article class="row service-details">
    <div class="cityscape"></div>
    <section class="service-details-main">
        <h2 class="text-center">The service in more detail...</h2>
        <ul class="row inner-container">
            <li>
                <span class="sprite icons-audio"></span>
                <span class="title">Unbeatable call quality</span>
                    <span class="body hide-for-small hide-for-smaller">
                        Unlike almost all conference call providers, most of our conference calls use the exact
                        same fibre optic cabling as your landline.
                    </span>
            </li>

            <li>
                <span class="sprite icons-chat"></span>
                <span class="title">Low-cost international access</span>
                    <span class="body hide-for-small hide-for-smaller">
                        Access Powwownow from over 14 countries around the world - for the cost of a local-rate phone
                        call.
                    </span>
            </li>

            <li>
                <span class="sprite icons-im"></span>
                <span class="title">Fantastic customer support</span>
                    <span class="body hide-for-small hide-for-smaller">
                        Got a question? We're fanatical about customer support. (That's why 98% of our
                        customers would recommend us.)
                        Call us on 020 3398 0398
                    </span>
            </li>

            <li>
                <span class="sprite icons-address"></span>
                <span class="title">Free Scheduling</span>
                    <span class="body hide-for-small hide-for-smaller">
                        You don't have to book or schedule your conference calls. But if you want to,
                        you can download our plugin for Outlook or use our scheduler tool to organise your calls and
                        invite participants.
                    </span>
            </li>

            <li>
                <span class="sprite icons-desktop"></span>
                <span class="title">Free instant web conferencing</span>
                    <span class="body hide-for-small hide-for-smaller">
                        Share your computer screen instantly. The attendees don't need to download or install anything -
                        all they need is internet access.
                    </span>
            </li>

            <li>
                <span class="sprite icons-cursor"></span>
                <span class="title">Instant call recording</span>
                    <span class="body hide-for-small hide-for-smaller">
                        Record your conference calls with one touch. Then you can review, download and share them with
                        your colleagues, or keep them for your records.
                    </span>
            </li>
        </ul>
    </section>
</article>

<article class="row clients">
    <section class="inner-container">
        <h2 class="text-center">Who we do it for... <br> and what they say about us. <br />
            <small>(All their own words, honest!)</small>
        </h2>

        <div class="col client-list">
            <div class="sprite pwn-ethicaltea">Ethical Tea Partnership</div>
            <div class="sprite pwn-libdems">Liberal Democrats</div>
            <div class="sprite pwn-met">Met Office</div>
            <div class="sprite pwn-nhs">NHS</div>
            <div class="sprite pwn-bbc">bbc</div>
        </div>

        <h3 class="col text-center">&#8220;We are really impressed with the excellent customer service.&#8221; <br />
            <small class="text-center"><strong>Martin Clark</strong>, MD Mullbery Group</small>
        </h3>

    </section>
</article>

<article class="row footer">
    <div class="inner-container">
        <section class="about">
            <p>
                Powwownow was founded in 2004 and is now the leading free conference call provider in the UK.
            </p>
            <p>
                At Powwownow, we don't believe in doing things the hard way. That's why we offer instant conference
                calling, available 24/7, with as many participants as required, wherever they are in the world. And
                because our service is free, callers only pay the cost of their own phone call, which is added to
                their standard telecoms bill - nothing more.
            </p>
            <p>
                Plus, because we are a telecommunications company in our own right, our customers can be guaranteed
                the best quality calls with the maximum number of features for the lowest price. That's why no one does
                conference calling like we do!
            </p>
        </section>

        <section class="social">
            <img alt="I am can" src="letsGetItDone/gridpak/img/mouth_98_mobile.png" data-interchange="
                        [letsGetItDone/gridpak/img/mouth_98_mobile.png, (only screen and (min-width: 1px))],
                        [letsGetItDone/gridpak/img/mouthy_98.png, (only screen and (min-width: 780px))]
                    ">
            <ul class="social-links css3">
                <li class="social-Twitter"><a href="https://twitter.com/powwownow" target="_blank"><strong>Twitter</strong></a></li>
                <li class="social-facebook"><a href="https://www.facebook.com/powwownow" target="_blank"><strong>Facebook</strong></a></li>
                <li class="social-linkedin"><a href="http://www.linkedin.com/company/powwownow" target="_blank"><strong>Linkedin</strong></a></li>
                <li class="social-googleplus"><a href="https://plus.google.com/+powwownow/videos" target="_blank"><strong>Google</strong></a></li>
                <li class="social-youtube"><a href="http://www.youtube.com/user/MyPowwownow" target="_blank"><strong>Youtube</strong></a></li>
            </ul>
        </section>

        <section class="col misc">
            <div class="logo">
                <a href="<?php echo url_for('@homepage')?>" target="_blank">
                    <img alt="Powwownow" src="<?php echo $logoWhite;?>">
                </a>
            </div>

            <div class="hashtag">
                #IAMACAN
            </div>
        </section>

        <section class="links">
            <ul class="css3">
                <li><a href="<?php echo url_for('@privacy'); ?>" target="_blank">Privacy</a></li>
                <li><a href="<?php echo url_for('@terms_and_conditions');?>" target="_blank">Terms & Conditions</a></li>
                <li><a href="<?php echo url_for('@about_us');?>" target="_blank">About Us</a></li>
                <li><a href="<?php echo url_for('@contact_us')?>" target="_blank">Contact Us</a></li>
                <li><a href="/Glossary" target="_blank">Glossary</a></li>
                <li><a href="<?php echo url_for('@useful_links'); ?>" target="_blank">Useful Links</a></li>
                <li><a href="/Sitemap" target="_blank">Site Map</a></li>
            </ul>
        </section>
    </div>
</article>
</div>
