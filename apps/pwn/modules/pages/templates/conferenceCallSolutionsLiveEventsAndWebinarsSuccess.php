<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSolutionsLeftNav', array('activeItem' => 'Events and webinars')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Solutions', 'Live Events and Webinars'),'title' => __('Live Events and Webinars'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Live Events and Webinars'), 'headingSize' => 'l', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>If you want to get the word out about your latest innovation, service or product, holding a live event can be a great way to do this, but this comes along with a hefty price tag and can be frustrating to organise. So why go to your audience when you can bring your audience to you using live event calls and webinars? Holding a live webinar using Powwownow’s free <a title="voice conferencing" href="<?php echo url_for('@voice_conferencing'); ?>">voice conferencing</a> service is a fantastic way to reach out to your audience without the need to tear them away from their work place or pour time, energy and money into organising a face-to-face event.</p>
                <p>Making sure the information about your company announcement gets heard by all the right people can be difficult and stressful, but it doesn't have to be if you use Powwownow’s conference calling services. With a Powwownow <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference call</a> you’re not limited to a handful of participants – you can quickly and easily pull an audience of up to a thousand together without anyone having to leave their home or office. Our conference call service allows you to make sure your voice is heard by the people who matter most to your organisation, be they your employees, the press or your stockholders, when it matters most.</p>
                <p>As effective as audio conferencing is, sometimes words alone just aren't enough to convey complex new ideas, explain ground breaking original products or give depth your impressive sales figures. Accompanying your announcements with a visual presentation through using Powwownow’s free <a title="web conferencing" href="<?php echo url_for('@web_conferencing'); ?>">web conferencing</a> services can make your announcement really pop and create a lasting impression on your audience that will endure long after the webinar is over. Whether you’re using a powerpoint presentation to show off your new product or walking audience members through your new website, our web conferencing services will let you put together a webinar worthy of applause.</p>
                <p>It doesn't even matter where in the world your audience is – whether they’re in New York, London or Shanghai you can reach them using our simple, low-cost and reliable <a title="international conference call" href="<?php echo url_for('@international_conference_call'); ?>">international conference call</a> service. There’s no special equipment needed and joining the call is as easy as one, two, three – all you need to do is send your conference call participants their international dial-in number and the PIN and that’s it! For the cost of a single phone call you can speak to your entire international audience in one fast and easy step. Don’t keep your good news all to yourselves – go global with live events and webinars!</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>