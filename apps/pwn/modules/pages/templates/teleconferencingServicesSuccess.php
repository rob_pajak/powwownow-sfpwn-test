<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">


        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
            'header' => 'Teleconferencing Services',
            'subHeader' => 'Teleconferencing allows you to meet with colleagues or clients wherever they are in the world without setting foot outside the home or office.',
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource,
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Powwownow Teleconferencing Services</h3>
                <p style="text-align: justify;">Travelling to meetings can be frustrating and inefficient so why not save time, costs and the environment and <a title="Free Teleconferencing" href="/Teleconferencing">teleconference</a> with Powwownow.</p>
                <p style="text-align: justify;">At Powwownow, we don't want our customers doing things the hard way. That's why we offer instant teleconferencing, available 24/7, with as many participants as required, speaking at an <a title="What it Costs" href="<?php echo url_for('@costs'); ?>">affordable price</a>. Callers only pay the cost of their own phone call which is added to their standard telecoms bill - nothing more.</p>
                <p style="text-align: justify;">With over 100,000 satisfied customers worldwide we must be doing something right. So get together, whenever and have a Powwow-now!</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Teleconferencing Services</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>