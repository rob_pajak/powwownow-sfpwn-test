<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'Manufacturing')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Calls for Manufacturing'),'title' => __('Conference Calls for Manufacturing'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Calls for Manufacturing'), 'headingSize' => 'xxl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>When you work in manufacturing, you&rsquo;re always working to a deadline and every moment counts. Whether you&rsquo;re putting together the next big thing in time for its launch, or manufacturing daily essentials that the customer expects to constantly be on the shelves, you need to know that you can get in touch with your clients, contractors and suppliers in the heat of the moment and not have your <a title="conference call " href="<?php echo url_for('@conference_call');?>">conference call</a> service fail you. If you&rsquo;re relying on the conference call provider Powwownow, such worries will be a thing of the past as you can count on us to keep you in touch whenever you need to be!</p>
                <p>When you&rsquo;re working with large orders your clients will often want to touch base on all the latest developments throughout the manufacturing cycle and ensure everything stays on schedule. With Powwownow&rsquo;s reservationless conference calls you can keep in touch with your clients with ease as there&rsquo;s no need to book your teleconference in advance. Best of all it&rsquo;s entirely free &ndash; there&rsquo;s no expensive bridging costs, contracts or complicated <a title="audio conferencing" href="<?php echo url_for('@audio_conferencing');?>">audio conferencing</a> hardware. All you and your clients need are the Powwownow dial-in number and your call PIN and you&rsquo;re ready to go. The only expense is the standard cost of a telephone call, no matter how many participants sit in.</p>
                <p>If you&rsquo;re working on a technical product, you might find yourself working alongside manufacturers across the world when putting together a final product. With multiple time zones to contend with and expensive international calling rates, the price in time and money of keeping in touch with them all can quickly mount up. Powwownow&rsquo;s <a title="international conference call" href="<?php echo url_for('@international_conference_call');?>">international conference call</a> service allows you to hold audio conference calls with over 100 countries worldwide, at any time of the day or night, and it won&rsquo;t cost any more than the standard call. The free Powwownow Scheduler tool makes it easy to schedule conference calls with callers across multiple time zones &ndash; the Scheduler will email all of your participants with all of the details for the conference call, showing the meeting time in their local time. This means you can easily keep in touch with your partners from Manchester to Detroit to Hong Kong without any confusion over the call start time.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>