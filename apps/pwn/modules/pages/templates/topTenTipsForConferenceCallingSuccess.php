<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Top 10 Tips for Conference Calling'),
                'headingTitle' => __('Top 10 Tips for Conference Calling'),
                'headingSize' => 'xxl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p>Some people seem to think that it’s more difficult to have a successful conference call meeting than a face-to-face meeting, but with a little forward planning <a title="telephone conferencing" href="<?php url_for('@telephone_conferencing'); ?>">telephone conferencing</a> can be a lot more productive and a more effective use of your time. Here are a few tips to bear in mind for your next teleconference to make sure you get the most out of it.</p>
        <ol class="top_ten_tips_for_conference_calling">
            <li><span>Go somewhere quiet – no one wants to hear a noisy office in the background. If you can’t take the call somewhere quiet, be sure to make judicious use of the mute button.</span></li>
            <li><span>Give the call your full attention – you never know when you might be called upon for your opinion and if you haven’t been paying attention you’ll be left red in the face!</span></li>
            <li><span>Treat a teleconference like any other meeting – be sure to take notes and read, or circulate, the agenda ahead of time.</span></li>
            <li><span>Introduce yourself – not only does everyone’s voice sounds different over the phone, there may be guests on the call who don’t know you.</span></li>
            <li><span>Be expressive – with <a title="conference call" href="<?php echo url_for('@conference_call');?>">conference calling</a> no one can see your body language and you will need to make up for this with your voice, so don’t be afraid to be vocal.</span></li>
            <li><span>Speak clearly and slowly – this ensures there are no misunderstandings and it’s much easier for the person taking notes. If you have an accent and are speaking with international colleagues or clients this also ensures you will be understood.</span></li>
            <li><span>Record the call – some <a title="conference call providers" href="<?php echo url_for('@conference_call_providers');?>">conference call providers</a>, including Powwownow, provide the facility for you to record your conference calls. This can be useful if you need to check back on what was said later on.</span></li>
            <li><span>Use proper <a title="conference call etiquette" href="<?php echo url_for('@conference_call_etiquette'); ?>">conference call etiquette</a> – don’t eat, play with your kids, or watch TV while in a teleconference!</span></li>
            <li><span>Use an appropriate phone – you can join a Powwownow conference call from any phone, but a good headset is nearly always the better option.</span></li>
            <li><span>Follow up on the call by email – thank everyone for the meeting and follow up on any lingering questions you might have.</span></li>
        </ol>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
