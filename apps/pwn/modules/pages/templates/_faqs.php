<h3><a href="#">What is the maximum number of participants I can have on a call?</a></h3>
<div><p>You can have up to 50 participants on a call at any one time without having to book with us in advance. We can allow more participants if needed through our Event Call facility. Please contact us on 0203 398 0398 or <a title="Contact Us" href="<?php echo url_for('@contact_us'); ?>">click here</a> if you require more information about this service.</p></div>

<h3><a href="#">What will it cost me?</a></h3>
<div><p>There is no charge for using the Powwownow service. Everyone calling into a teleconference, including the host, simply pays for their own phone call, which typically costs 4.3p/min+VAT from landlines anywhere in the UK. The call costs will be shown on your phone bill in the same way as any other call. <a title="International Number Rates" href="<?php echo url_for('@international_number_rates'); ?>">Click here</a> for international call charges.</p></div>

<h3><a href="#">How much does it cost to call from a mobile phone?</a></h3>
<div><p>When in the UK, use Powwownow’s exclusive mobile shortcode (87373) instead of the regular 0844 number and benefit from a fixed flat rate of 12.5p per minute + VAT. When abroad, the cost of calls from a mobile phone may vary depending on the network provider. Please contact the appropriate network provider for a confirmed price. Please also note that some numbers are not available for use from mobiles.&nbsp; <a title="International Number Rates" href="<?php echo url_for('@international_number_rates'); ?>">Click here</a> for more details.</p></div>

<h3><a href="#">How much does it cost to call your numbers from abroad?</a></h3>
<div><p>Powwownow offers local access numbers in 15 countries worldwide so callers can save on international call rates. For countries without a local access number, the cost of international conference calls vary depending on the country and the network provider. Please contact your network provider for a confirmed price. Please also note that some numbers cannot be dialled internationally. <a title="International Number Rates" href="<?php echo url_for('@international_number_rates'); ?>">Click here</a> for more details.</p></div>

<h3><a href="#">Which services are charged for when registering?</a></h3>
<div><p>It is free to register for the main Powwownow service; all you pay for is your own call charges.</p><p>It is free to register or switch to Powwownow Plus, however should you wish to use either Freephone or Landline numbers you need to purchase credit to do so.</p><p>Powwownow Premium comes with a subscription fee which is individually tailored based on your company needs.</p></div>

<h3><a href="#">How does Powwownow make money?</a></h3>
<div><p>We make our money in the form of a rebate which we receive from our telecoms partners.</p></div>

<h3><a href="#">Do I need to book my conference call?</a></h3>
<div><p>No, there is no need to book. You can hold a telephone conference whenever you like - 24x7x365.</p></div>

<h3><a href="#">Is your service secure?</a></h3>
<div><p>Yes, every participant entering the conference is announced, ensuring you will know exactly who is on your conference call. You have the ability to lock the conference room and even do a head count or roll call of the participants. <a title="How Conference Calling Works" href="<?php echo url_for('@how_conference_calling_works'); ?>">Click here</a> for more in-conference controls.&nbsp;</p><p>For further details about how secure our service is <a title="Security" href="<?php echo url_for('@security'); ?>">click here</a>.</p></div>

<h3><a href="#">Is there a limit to how long my conference call can last?</a></h3>
<div><p>No, there is no limit to the duration of a conference call.</p></div>

<h3><a href="#">Can I use the service from abroad?</a></h3>
<div><p>Yes, we offer local access numbers in 15 countries worldwide and many of these numbers can also be dialled internationally for when you're located in countries without a local access number. Please <a title="International Number Rates" href="<?php echo url_for('@international_number_rates'); ?>">click here</a> for international dial-in numbers.<br>Contact us on 0203 398 0398 for a full list of your dedicated international dial-in numbers.</p></div>

<h3><a href="#">Does the dial-in number change?</a></h3>
<div><p>No, the dial-in number remains the same, however if there are any changes, we will ensure you are made aware of them.</p></div>

<h3><a href="#">Can I use my PIN more than once?</a></h3>
<div><p>Yes, all of our services are designed so PINs can be used again and again.</p></div>

<h3><a href="#">Can someone have the same PIN as me?</a></h3>
<div><p>No, registered customers have unique PINs that are reserved for each customer's use.</p></div>

<h3><a href="#">Can I change my PIN?</a></h3>
<div><p>In order for us to change your PIN we would have to delete your account from the system so that you can re-register and be issued with a new PIN.</p></div>

<h3><a href="#">I am able to dial-in on your number but the system is ignoring my PIN. Why?</a></h3>
<div><p>This problem is most likely linked to an issue with your handset. It may be that your telephone is not sending out the correct DTMF tones, hence preventing our system from identifying the keys pressed. It might be advisable to try another handset for your next call and to contact us again if this problem persists.</p></div>

<h3><a href="#">What is the difference between PIN and password?</a></h3>
<div><p>Your PIN enables you to access your voice conference and is also required to hold a web conference.<br>Your password is required to access <a title="Login to myPowwownow" href="<?php echo url_for('@login'); ?>">myPowwownow</a>, where you can manage your call settings, use Scheduler, retrieve recordings and purchase products. It is also required to download our web conferencing application.</p></div>

<h3><a href="#">Where do I find my password?</a></h3>
<div><p>If you are a Powwownow user, you can <a title="Create a Password" href="<?php echo url_for('@create_a_login'); ?>">create a password</a> to log in to myPowwownow and unlock all your features and benefits. If you have forgotten your password you can <a title="Forgotten Password" href="<?php echo url_for('@forgotten_password'); ?>">reset it here</a>.</p></div>

<h3><a href="#">Do I need any special equipment?</a></h3>
<div><p>No, you can use Powwownow from any touchtone phone, a landline, pay phone, mobile and even Skype - in the same way as you would make any other phone call.</p></div>

<h3><a href="#">Can I record my calls?</a></h3>
<div><p>Yes, you can record, play and share your call recordings with whoever you wish. Just press #8 on your telephone keypad to get started. Your saved recordings will appear in <a title="Login to myPowwownow" href="<?php echo url_for('@login'); ?>">myPowwownow</a> a few minutes after your call ends.</p></div>

<h3><a href="#">Can I change my on-hold music and other call settings?</a></h3>
<div><p>Yes, we have a choice of on-hold music for you to choose from. You can also change various call settings including the voice prompt language. Just log in to <a title="Login to myPowwownow" href="<?php echo url_for('@login'); ?>">myPowwownow</a> and go to myPIN(s) and click the 'Edit' button under Call Preferences. If you don't have a login, you can <a title="Create a Login" href="<?php echo url_for('@create_a_login'); ?>">create one here</a>.</p></div>

<h3><a href="#">Why have I been issued 2 PINs?</a></h3>
<div><p>As a Plus or Premium user we have issued you dual PINs for additional security. Your Chairperson PIN is the PIN you use to start,&nbsp;join and control&nbsp;a conference call.&nbsp; Your participant PIN is the PIN you need to share with your call participants every time you&nbsp;invite them to conference call.</p></div>

<h3><a href="#">Can I use the Plus service with just one PIN?</a></h3>
<div>Yes, you can continue to use your participant PIN in exactly the same way as with the Free Powwownow service.</div>

<h3><a href="#">Where do I find my purchased Landline and Freephone numbers?</a></h3>
<div>Once you have assigned products to your account a list of your available numbers can be found in your <a title="International Dial-in Numbers" href="http://www.powwownow.co.uk/Login?d=3">international dial-in numbers</a> list in your myPowwownow account area.<br></div>

<h3><a href="#">How does Auto Top-up work?</a></h3>
<div>Auto top-up works by automatically topping up your credit when your balance falls below £5.00.&nbsp; The system will use the same payment method and amount as your last transaction. You can disable auto top-up on your account whenever you like and enable it the next time you purchase credit.</div>

<h3><a href="#">What is the minimum amount of credit I need to make a conference call?</a></h3>
<div>The minimum amount of credit required is £5.00.&nbsp; Should your account run in to debit during a conference call, we will not cut you off.&nbsp; However you will need to top up your account immediately to bring it back into credit to make another conference call.</div>

<h3><a href="#">What does it cost to purchase Branded Welcome Messages?</a></h3>
<div>Recording of&nbsp;your company's personalised message&nbsp;is a one-off charge of £50.&nbsp; For every dedicated dial-in number you add to this recording there is an annual charge of £60 per number.&nbsp;</div>

<h3><a href="#">How long does my Branded Welcome Message take to set up?</a></h3>
<div>Once you have created your branded welcome message, it will take up to 10 working days for your personalised message to be live.&nbsp; Your dedicated dial-in numbers will be available to use as soon as they appear on your account and in your international dial-in numbers list.</div>

<h3><a href="#">What are dedicated dial-in numbers?</a></h3>
<div>Dedicated dial-in numbers are specially allocated numbers which can be used by&nbsp;your business and conference&nbsp;call participants.</div>

<h3><a href="#">What is your refund policy?</a></h3>
<div>Refunds will be given at the discretion of the management by contacting customer services on 0203 398 0398.&nbsp; Our full terms and conditions can be viewed <a title="Terms and Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>">here</a> .</div>

<h3><a href="#">Do you provide video conferencing?</a></h3>
<div><p>Yes, however&nbsp;at the moment this service is only available to Premium customers.&nbsp; For more information please contact our video specialists on 0800 022 9930 or +44 (0)20 3398 0900.</p></div>

<h3><a href="#">How secure is your web conferencing service?</a></h3>
<div><p>When registering for our web conferencing service, you are provided with a PIN that is totally unique to you. When arranging a web conference be&nbsp;certain to only share your PIN with the&nbsp;participants that you wish to join your web conference. Once&nbsp;your web conference has started all participants that join&nbsp;will appear in your chat window, ensuring you know exactly who is on your web conference.</p></div>

<h3><a href="#">What is the minimum amount of credit I need to make a conference call?</a></h3>
<div>The minimum amount of credit required to make a conference call&nbsp;is £5.00.&nbsp; Should your account run in to debit during a conference call, we will not cut you off.&nbsp; However you will need to top up your account immediately to bring it back into credit to make another conference call.</div>

<h3><a href="#">How safe is sharing my screen?</a></h3>
<div>When using&nbsp;Powwownow with other&nbsp;Powwownow contacts, all data is 128-bit SSL encrypted end to end which is the highest level of encryption generally available. When you share your screen using Powwownow, screen sharing data is never stored on our servers.</div>

<h3><a href="#">Does Powwownow keep any of my data?</a></h3>
<div>Yes. Chat message history, contacts and&nbsp;Powwownow login details are all stored on our secured servers.<br></div>

<h3><a href="#">What does Powwownow do with my data?</a></h3>
<div>Powwownow&nbsp;uses information for the following general purposes: Improve our services, contact you, conduct research, and provide data analysis and reporting for internal usage.<br></div>

<h3><a href="#">Is my Browser supported</a></h3>
<div class="link-browsers">
    <p>To keep life simple, the Powwownow website is compatible with almost any Web browser. This includes Internet Explorer, Mozilla Firefox, Apple Safari and Chrome, as well as most other Web browsers on computers running Windows, Apple Macintosh or UNIX.</p>
    <p>However, to make sure you’re benefiting from all the latest wizardry the Powwownow site can offer, the following browsers are recommended for computers running Windows XP, Windows 2003, Windows Vista, or Windows 7:</p>
    <ul>
        <li>Internet Explorer 8 and later versions.</li>
        <li>Firefox 4 and later versions.</li>
        <li>Chrome 7 and later versions.</li>
    </ul>
    <p>For those using a computer running Mac OS X 10.5 and later versions:</p>
    <ul>
        <li>Safari 3.1 and later versions.</li>
        <li>Chrome 7 and later versions.</li>
    </ul>
    <p>For computers running Linux:</p>
    <ul>
        <li>Firefox 4 and later versions.</li>
        <li>Chrome 7 and later versions.</li>
    </ul>
    <p>The following browsers are recommended for those of you using a mobile device; for iPhone and iPad:</p>
    <ul>
        <li>Safari 3.1 and later versions.</li>
    </ul>
    <p>And for Android users:</p>
    <ul>
        <li>Chrome 7 and later versions.</li>
    </ul>
    <p>If you are unable to use any of the above Web browsers, do not fear, a few features may just be a little less smooth than usual.</p>
    <p>To ensure that you’re receiving the best experience possible, check your Web browser’s options to verify the following settings:</p>
    <ul>
        <li>Enable JavaScript.</li>
        <li>Check pop-up blocker settings.</li>
        <li>Enable session cookies – for full information on our cookies policy, see section 7 on our <a title="Privacy Page" href="<?php echo url_for('@privacy'); ?>">Privacy page</a>.</li>
    </ul>
</div>

<h3><a href="#">Can participants abroad connect to my conference via numbers local to them?</a></h3>
<div><p>Yes, as long as your overseas participants are calling from any of the Powwownow <a title="International Number Rates" href="<?php echo url_for('@international_number_rates'); ?>">international dial-in numbers</a> and the same PIN or PIN set is used.</p></div>

<h3><a href="#">How do I request a welcome pack and/or reminder stickers?</a></h3>
<div><p>Simply log in to <a title="Login to myPowwownow" href="<?php echo url_for('@login'); ?>">myPowwownow</a> and click the 'Request Welcome Pack' button under My PIN(s). If you don't have a login, you can <a title="Create a Login" href="<?php echo url_for('@create_a_login'); ?>">create one here</a>.</p></div>

<h3><a href="#">Can you please send me more than one welcome pack?</a></h3>
<div><p>Unfortunately, we are only able to issue one welcome pack per user at a time. We can however, send you more than one set of stickers for you to pass around. Please contact us on 0203 398 0398 if you require more stickers.</p></div>