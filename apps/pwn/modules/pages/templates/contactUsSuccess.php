<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Contact Us'),
                'headingTitle' => __('Contact Us'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <div class="left-col-contact-us">
                <h3 class="lightgreen">Powwownow</h3>
                <p>1st Floor, Vectra House<br/>36 Paradise Road<br/>Richmond, Surrey<br/>TW9 1SE</p>
                <p>Phone: 0203 398 0398<br/>Fax: +44 (0)203 355 4262</p>
                <p><a title="Find Powwownow On Google Maps" href="http://maps.google.co.uk/maps?cid=6627778478432420610&amp;q=Vectra+House,+36+Paradise+Road&amp;sll=51.460258,-0.30184&amp;sspn=0.002757,0.006974&amp;ie=UTF8&amp;ll=51.46042,-0.301456&amp;spn=0.001379,0.003487&amp;t=h&amp;z=19&amp;iwloc=A" target="_blank">Find us on Google Maps</a></p>
            </div>
            <div class="right-col-contact-us">
                <p>
                    <a class="chatlink" title="Live Chat!" href="">
                        <img src="/sfimages/contact-us-live-chat.png" alt="Live Chat!" width="230" height="138">
                    </a>
                </p>
            </div>
        </div>
        <div class="clearfix">
            <p>
                We welcome your feedback about our service, so if you have something on your mind but don't feel like talking, please drop us a line using the form below.
            </p>
        </div>
        <div class="contactus-form clearfix">
            <?php echo form_tag(url_for('contact_us_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-contact-us', 'class' => 'pwnform clearfix')); ?>
                <?php $count = 1; ?>
                <?php foreach ($fields as $field) : ?>
                    <?php include_partial('contactUsFormField', array(
                        'field' => $field,
                        'form'  => $form,
                        'count' => $count
                    )); ?>
                    <?php $count = (2==$count) ? 1 : 2 ?>
                <?php endforeach; ?>
                <div class="form-field clearfix">
                    <?php echo $form['comments']->renderLabel(); ?>
                    <span class="mypwn-input-container">
                        <?php echo $form['comments']->render(array('class' => 'input-large font-small mypwn-input')); ?>
                    </span>
                </div>
                <div class="clearfix">
                    <div class="form-action" style="position: relative">
                        <p style="color: white;">* Required fields</p>
                        <button class="button-orange" type="submit" id="contactus-submit"><span>SEND</span></button>
                    </div>
                </div>

            </form>
        </div>
        <div class="clearfix">
            <p>
                Your details will be stored securely by Via-Vox Ltd and will only be accessed by Via-Vox Ltd -
                we will not under any circumstances pass your details on to any third party. You are also agreeing to
                be bound by our <a title="Terms And Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>">terms and conditions</a>.
            </p>
        </div>
        <script type="text/javascript">
            (function($) {
                'use strict';
                $(function() {
                    pwnApp.contactus.init('#frm-contact-us');
                });
            })(jQuery);
        </script>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
