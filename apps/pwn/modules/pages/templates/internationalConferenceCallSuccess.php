<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
            'header' => 'International Conference Call',
            'subHeader' => 'With local access dial-in numbers in 15 countries worldwide, low-cost international conference calling has never looked better!',
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource,
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">International Conference Calls with Powwownow!</h3>
                <p style="text-align: justify;">At Powwownow we offer our customers an affordable <strong>international conference call service</strong>, available 24/7. We have a wide range of low-cost <a title="International Number Rates" href="<?php echo url_for('@international_number_rates'); ?>">international access numbers</a> making it easy for you to keep connected with clients and colleagues around the world.</p>
                <p style="text-align: justify;">Each participant dials the in-country number where they are located and will only pay the cost of a national rate call, no hidden or extra charges! What could be simpler?</p>
                <p style="text-align: justify;">We also offer a host of free features and benefits such as <a title="Powwownow Web Conferencing" href="<?php echo url_for('@web_conferencing'); ?>">web conferencing</a> and call recording.&nbsp; No wonder 97% of our customers would recommend us!</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; International Conference Call</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>