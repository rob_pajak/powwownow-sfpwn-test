<?php include_component('commonComponents', 'header', array('headerHint' => false)); ?>
<div class="container_24 clearfix">
    <div class="grid_24">
        <h1 class="tab-h1" style="margin: 50px 0 25px 15px; font-size: 29px; line-height: 100%">You are ready to start conference calling</h1>
    </div>
    <div class="grid_18">
        <div class="green-rounded-box clearfix" style="height: 225px; color: white;">
            <h2 class="rockwell white">Start your conference call now...</h2>
            <div class="subheading2">These are your key details for every time you need to conference call:</div>
            <div class="grid_sub_11" style="position: relative;">
                <div class="sprite-new sprite-phone-white floatleft png" style="position:relative; top:-6px;"></div>
                <div class="floatleft">Your UK dial-in number is:</div>
                <p style="font-size:29px; clear:both;"><?php echo $dialInNumber; ?></p>
            </div>
            <div class="grid_sub_7" style="position: relative;">
                <span class="rockwell" style="font-size: 18px;">***</span> Your PIN is:
                <div class="pin-container pin-container-response png" id="generated_pin"><?php echo $pin; ?></div>
            </div>
            <div class="grid_sub_6" style="height:100px; position:relative; padding-top: 42px; padding-left: 27px; z-index:2000">
                <div class="tooltip" style="height: 80px; position: absolute; top: -70px; width: 155px; left: 0px; padding: 10px; color: #515151">
                    <div style="background: url('/sfimages/sprites/sprite-tooltip.png') no-repeat scroll -29px top transparent; height: 11px;  position: absolute; top: 100px; left: 30px; width: 29px;"></div>
                    <p style="font-size: 10px; line-height: 110%; margin: 0; padding: 0 0 10px 0">In a hurry? You can start your first conference call right now.</p>
                    <p style="font-size: 10px; line-height: 110%; margin: 0; padding: 0">Just share the dial-in number and PIN with the other callers and you're off!</p>
                </div>
                <?php include_component('ajaxResponses', 'shareMyPinLink',array(
                        'linkContent' => '<button id="share-details" class="button-orange margin-top-10" type="submit"><span>SHARE DETAILS</span></button>',
                        'pin' => $pin
                    )); ?>
            </div>
        </div>
        <div class="green-rounded-box clearfix" style="height: auto; padding-bottom: 15px">
            <?php include_component('commonComponents', 'whatToDoNow');?>
        </div>
    </div>

    <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

    <div class="grid_24">
        <div class="hr-spotted-top png content-seperator"></div>
    </div>

    <?php include_component('commonComponents', 'videoCarousel'); ?>

    <div class="grid_24">
        <div class="hr-spotted-top png content-seperator"></div>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
