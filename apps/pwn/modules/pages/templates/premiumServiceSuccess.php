<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Powwownow Premium'),
                'headingTitle' => __('Premium at a Glance'),
                'headingSize' => 'l',
                'headingUserType' => 'premium'
            )
        ); ?>
        <div class="bubble-blue2 white png">
            <div>
                <p>With Premium, you'll get the same <b>no-hassle conference calls</b> as our regular service. On top, you'll get added features including unlimited conference calls and conference call PINs, personalised welcome messages and a choice of 0844, 0800, local geographical and international phone numbers.</p>
                <ul id="features-list">
                    <li class="png"><p><b>Tailor-made solutions with superior features</b></p></li>
                    <li class="png"><p><b>Unbeatable pricing</b></p></li>
                    <li class="png"><p><b>Trusted by brands like:</b></p></li>
                </ul>
                <!-- keep images no bigger than 100px by 45px -->
                <img class="tmp16" src="/sfimages/testimonials/testimonial-logos_nhs.gif" alt="NHS"/>
                <img class="tmp17" src="/sfimages/testimonials/testimonial-logos_ethical.gif" alt="Ethical Tea Partnership"/>
                <img class="tmp18" src="/sfimages/testimonials/testimonial-logos_met.gif" alt="Met Office"/>
                <img class="tmp19" src="/sfimages/testimonials/testimonial-logos_arriva.gif" alt="Arriva"/>
                <img class="tmp20" src="/sfimages/testimonials/testimonial-logos_lib.gif" alt="Lib Dems"/>
                <img class="tmp21" src="/sfimages/testimonials/testimonial-logos_marriott.gif" alt="Marriott"/>
                <p class="tmp22">To find out more, call us now on <span class="green-light rockwell font-large">0800 022 9781</span> (or +44 (0) 20 3398 0102 for mobile and international calls).</p>
                <p>We're open Monday-Friday 8.30am-5.30pm.</p>
            </div>
        </div>

        <h3 class="bubble-blue rockwell png">Why Choose Premium?</h3>

        <p class="rockwell font-bigger">Easy to use conference calls and web conferences</p>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-phone.png" alt="Phone"/></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">Unlimited conference calls - on demand</p>
                <p>With Premium, you'll get <b>unlimited conference calls</b>, plus unlimited chairperson and participant PINs.</p>
                <p>That means you can run as many conference calls as you like - at the same time.</p>
                <p>And just like our regular service, conference calls can be scheduled or on demand.</p>
            </div>
        </div>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-speaker.png" alt="Speaker"/></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">Unbeatable quality</p>
                <p>Unlike other conference call providers, Powwownow has a fully-owned infrastructure <b>ensuring call quality is as good as your landline</b>.</p>
                <p>That's because almost all our conference calls use the exact same fibre optic cabling as your landline. And along with BT, <b>we're the only conference call provider to have this quality</b>.</p>
            </div>
        </div>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-globe.png" alt="Globe"/></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">Free, local or global numbers</p>
                <p>Choose from a selection of phone numbers and rates for your conference calls. As well as the regular 0844 shared-cost numbers, you can also choose 0800 freephone, local or international numbers. That means you can give your attendees <b>free or low-cost landline and mobile access from over 100 countries around the world</b>.</p>
            </div>
        </div>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-pointer.png" alt="Pointer"/></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">Free instant web conferencing</p>
                <p>You can now integrate your conference calls with web conferencing - and it's completely free.</p>
                <p>That means you can <b>instantly share your computer screen with your call's attendees</b> (it's perfect for presentations). The attendees don't need to download or install anything - all they need is internet access.</p>
            </div>
        </div>

        <div class="grid_sub_16">
            <h4>Unbeatable extras</h4>
            <div class="grid_sub_24">
                <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt="Arrow"/></div>
                <div class="grid_sub_21">
                    <p><span class="rockwell font-bigger">Branded welcome messages</span><br>When you sign up for Premium, you can customise your conference calls' welcome message to match your brand.</p>
                </div>
            </div>
            <div class="grid_sub_24">
                <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt="Arrow"/></div>
                <div class="grid_sub_21">
                    <p><span class="rockwell font-bigger">Real-time reporting</span><br>Run reports to monitor call participants joining and leaving conference calls, see how many people were on particular calls, what number they dialled to access the call and how long the call was for.</p>
                </div>
            </div>
            <div class="grid_sub_24">
                <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt="Arrow"/></div>
                <div class="grid_sub_21">
                    <p><span class="rockwell font-bigger">Instant call recording</span><br>Record your conference calls with one touch. Then you can review, download and share them with your colleagues, or keep them for your records.</p>
                </div>
            </div>
            <div class="grid_sub_24">
                <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt="Arrow"/></div>
                <div class="grid_sub_21">
                    <p><span class="rockwell font-bigger">Dedicated customer support</span><br>We're proud to have some of the best customer support in the industry. That's why 98% of our customers would recommend us.</p>
                </div>
            </div>
            <div class="clearfix"><div class="hr-spotted-top png content-seperator"></div></div>

            <h3 class="bubble-blue-small rockwell png">What it Costs</h3>
            <p>The pricing of Powwownow Premium is customised to your exact needs. That way, you get everything you need - and don't pay for anything you don't.</p>
            <p><b>Call us now on 0800 022 9781 or 020 3398 0102.</b> One of our specialist consultants will build the right package for you and help you get started.</p>
            <h4>How Powwownow Premium saves you money</h4>
            <p>Powwownow is different from other conference call providers. That's because - unlike other services - we don't charge a "bridging fee". And because we are a telecommunications company in our right, we don't pass on any third-party supplier fees.</p>
            <div class="clearfix"><div class="hr-spotted-top png content-seperator"></div></div>
            <h4 class="margin-top-5">Your choice of dial-in numbers and rates</h4>
            <p>With Premium, you can choose from:</p>
            <div class="grid_sub_24">
                <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt="Arrow"/></div>
                <div class="grid_sub_21">
                    <p><span class="rockwell font-bigger">0844 shared cost numbers</span><br>Low call rate, all callers pay their own call charges of 4.3p/min if dialling from a BT landline.</p>
                </div>
            </div>
            <div class="grid_sub_24">
                <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt="Arrow"/></div>
                <div class="grid_sub_21">
                    <p><span class="rockwell font-bigger">0800 freephone numbers</span><br>Free to callers, host will be invoiced monthly for all calls to this number.</p>
                </div>
            </div>
            <div class="grid_sub_24">
                <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt="Arrow"/></div>
                <div class="grid_sub_21">
                    <p><span class="rockwell font-bigger">Geographical numbers (eg 03...)</span><br>Perfect for mobile phone users.</p>
                </div>
            </div>
            <div class="grid_sub_24">
                <div class="bubble-blue3 margin-top-20 png">
                    <div>
                        <p class="white clearfix">Let's talk about how Powwownow Premium can help you:</p>
                        <p><span class="green-light rockwell font-huge">0800 022 9781</span> or <span class="green-light rockwell font-huge">020 3398 0102</span></p>
                        <p><strong>We're open Monday-Friday 8.30am-5.30pm</strong></p>
                    </div>
                    <div class="bubble-blue3-bookmark png"></div>
                </div>
            </div>
        </div>
        <div class="grid_sub_7 margin-top-20 floatright">
            <div class="bubble-orange png">
                <div class="rockwell font-large">Find out why 98% of our customers recommend us.</div>
            </div>
            <p class="margin-top-20"><span class="green rockwell font-bigger">&ldquo;Powwownow is very easy to use and very convenient and I would recommend other businesses use it&rdquo;</span><br/>Lindsay Tarrant (Business Management, NHS)</p>
            <p class="margin-top-20"><span class="green rockwell font-bigger">&ldquo;[The Powwownow team] understand the core values of our business and have gone the extra mile to make sure we can communicate on demand.&rdquo;</span><br/>EMEA Sales Director, Global e-Learning Provider</p>
            <p class="margin-top-20"><span class="green rockwell font-bigger">&ldquo;Since using Powwownow, I have seen a real improvement in customer interaction and retention.&rdquo;</span><br/>Principal - Financial Services</p>
            <p class="margin-top-20"><span class="green rockwell font-bigger">&ldquo;Powwownow has made communicating with our clients straight forward and stress free. The branding of the interface gives a thoroughly professional feel for both our clients and our own staff around the globe.&rdquo;</span><br/>Rob Smith (Group IT Manager, Start Creative Limited)</p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
