<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Glossary'),
                'headingTitle' => __('Conference Call Glossar'),
                'headingSize' => 'l',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <table class="rockwell">
            <tbody>
                <tr>
                    <th>Term</th>
                    <th>Definition</th>
                </tr>
                <tr class="odd">
                    <td><a title="audio conferencing" href="<?php echo url_for('@audio_conferencing'); ?>">Audio Conferencing</a></td>
                    <td>Conference calling that utilises nothing but audio, or voice. This is the most common type of conference call.</span></td>
                </tr>
                <tr class="even">
                    <td><a title="branding" href="<?php echo url_for('@conference_call_branding'); ?>">Branded Welcome Message</a></td>
                    <td>A personalised greeting that is recorded and played to participants when dialling into a conference call, instead of the default Powwownow introduction.</td>
                </tr>
                <tr class="odd">
                    <td>Chairperson</td>
                    <td>The host of the conference call. With many conference call providers, the Chairperson pays an additional fee on top of the cost of the phone call for each additional participant, but a Chairperson with Powwownow only pays the basic cost of the call: the same as all other participants.</td>
                </tr>
                <tr class="even">
                    <td>Chairperson</td>
                    <td>The host of the conference call. With many conference call providers, the Chairperson pays an additional fee on top of the cost of the phone call for each additional participant, but a Chairperson with Powwownow only pays the basic cost of the call: the same as all other participants.</td>
                </tr>
                <tr class="odd">
                    <td>Conference Bridge</td>
                    <td>A facility within a service provider that connects all participants of a conference call together and supports the call whilst it is ongoing. With most providers you may be charged a bridging fee, but Powwownow doesn’t charge any additional fees for bridging your calls.</td>
                </tr>
                <tr class="even">
                    <td>Conference Call</td>
                    <td>A conference meeting held via the use of telephones or telephonic devices between two or more people.</td>
                </tr>
                <tr class="odd">
                    <td><a title="conference call providers" href="<?php echo url_for('@conference_call_providers'); ?>">Conference Call Provider</a></td>
                    <td>The company, or individual, who provides the facility or hardware to conduct a conference call. Powwownow is a (great!) conference call provider.</td>
                </tr>
                <tr class="even">
                    <td>Dial-in Number</td>
                    <td>The number you dial to join the conference call. It differs from country to country and will also be different depending on the method you choose to join the call with, whether it’s a landline, Skype or mobile.</td>
                </tr>
                <tr class="odd">
                    <td>Participant</td>
                    <td>A person who takes part in the conference call. Simple as that!</td>
                </tr>
                <tr class="even">
                    <td>PIN</td>
                    <td>“Personal identification number”. You might recognise the term as what you use at a cash point to identify yourself along with your card, but it’s also the number you use to access a Powwownow conference call.</td>
                </tr>
                <tr class="odd">
                    <td>Reservationless Conference Call</td>
                    <td>A conference call that does not require advanced reservation. Powwownow’s conference calls are reservationless: we don’t require you to book the time and date of the call in advance, meaning you can hold a conference call whenever you like, 24 hours a day, seven days a week.</td>
                </tr>
                <tr class="even">
                    <td><a title="telephone conferencing" href="<?php echo url_for('@telephone_conferencing'); ?>">Telephone Conference</a></td>
                    <td>Also known as a Teleconference, this is a meeting among two or more participants through a telephone, mobile phone or Skype and involving conference calling hardware or services, such as Powwownow.</td>
                </tr>
                <tr class="odd">
                    <td>Scheduler</td>
                    <td>A tool that allows you to set the time and date of your conference call, along with your list of participants, and have Powwownow email them the details of the call, making it quicker and easier to get these details out to participants.</td>
                </tr>
                <tr class="even">
                    <td>Web Conferencing</td>
                    <td>A meeting or event held via the use of telephones and the web to share desktop applications between two or more people, usually from remote locations.</td>
                </tr>
                <tr class="odd">
                    <td>Webinar</td>
                    <td>A seminar conducted over the internet.</td>
                </tr>
            </tbody>
        </table>
        *Speak with up to 1000 callers with <strong><a title="Event Conference Calls" href="<?php echo url_for('@event_conference_calls'); ?>">Event Calls</a></strong>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
