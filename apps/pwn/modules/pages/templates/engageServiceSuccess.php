<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Powwownow Engage at a Glance'),
                'headingTitle' => __('Powwownow Engage at a Glance'),
                'headingSize' => 'xl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <?php include_component(
                'commonComponents',
                'hTML5OutputVideo',
                array(
                    'config' => array(
                        'width'    => 460,
                        'height'   => 260,
                        'autoplay' => false,
                        'controls' => true
                    ),
                    'video'  => array(
                        'mp4'  => '/sfvideo/pwn_engage.mp4',
                        'webm' => '/sfvideo/pwn_engage.webm',
                        'ogg'  => '/sfvideo/pwn_engage.ogv',
                        'image' => '/sfimages/engage-still.jpg'
                    ),
                    'called' => 'html'
                )
            ); ?>
        </div>
        <div class="margin-top-20">
            <?php include_component('commonComponents', 'genericHeading', array('title' => __('Why Powwownow Engage?'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
        </div>
        <p class="rockwell font-bigger">It’s a straight forward, smarter and easier way to communicate with colleagues, business partners and customers.</p>
        <p>Powwownow, The Smarter Connection: Click <a href="/sfpdf/en/Whitepaper-The-Smarter-Connection-November-2012.pdf" target="_blank">here</a> to download our whitepaper.</p>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-hd.png" alt=""></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">HD Video Calling</p>
                <p>HD video conferencing makes calls more engaging, personal and more productive. HD quality provides a ‘real meeting’ experience, removing the frustration, distraction and delays of poor quality video calling.</p>
                <p>Video allows you to connect and collaborate with multiple users at the same time.</p>
            </div>
        </div>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-arrow.png" alt=""></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">Screen Sharing</p>
                <p>Sharing your screen with your contacts brings your conversation to life and enables you and your colleagues to work on the same document at the same time as if you were sitting next to them in the office.</p>
                <p>So, whether you’re working from home, in the office or with colleagues or clients in other locations, you can collaborate from anywhere in the world.</p>
            </div>
        </div>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-voip.png" alt=""></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">Phone and VoIP Conference Calling</p>
                <p>You and your call participants have the option to make and receive calls in the most convenient way, depending whether you’re working on the move or from the office.</p>
                <p>VoIP calls are free of charge and made through a headset attached to your PC. Conference calls made through a telephone are charged at competitive Powwownow conference call rates.</p>
            </div>
        </div>
        <div class="bubble-gray clearfix">
            <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-speech.png" alt=""></div>
            <div class="grid_sub_19 text grey">
                <p class="rockwell font-bigger green-dark">Instant Messaging</p>
                <p>Chat one-to-one or in a group, with the ability to run multiple chats at the same time, with alerts that notify you when a message is received.</p>
                <p>Using Instant Message you can see when people are available or away, meaning you can  get instant answers to your quick questions.</p>
            </div>
        </div>

        <h4 class="rockwell"><strong>Extra benefits</strong></h4>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">One Click Collaboration</span><br/>Multiple communication tools in an easy to use single application. No complex technology or on-going IT support required.</p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Enable Remote IT Support</span><br/>Screen sharing technology enables your IT team to provide remote support to employees who work from multiple locations.</p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Secure Communication</span><br/>When using video calling and contact-to- contact communications from within the interface, all data is 128-Bit AES Encrypted.</p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Faster Communication</span><br/>Presence allows you to see when your contacts are available, so you know the most appropriate way to contact them. Set your own Presence status to inform contacts of an appropriate time to contact you.</p>
            </div>
        </div>

        <div class="hr-spotted-top png content-seperator"></div>

        <div class="margin-top-10">
            <?php include_component('commonComponents', 'genericHeading', array('title' => __('What It Costs'), 'headingSize' => 'm', 'user_type' => 'powwownow')); ?>
        </div>

        <p>All you pay is a monthly subscription fee, there is no up-front investment and the service is simple to scale as your business requirements change.</p>
        <h4>How Powwownow Engage saves you money</h4>
        <p>Real-time communication means an increase in productivity whilst minimising company overheads and employee travel expenses. Using integrated VoIP calls helps reduce your call costs.</p>
        <p>Unlike traditional video conferencing room systems no additional hardware is require, you can simply use your desktop PC, laptop with a camera and a headset or microphone.</p>


        <div class="hr-spotted-top png content-seperator"></div>

        <div class="bubble-green3 margin-top-20 png">
            <div>
                <span class="emphasise">To find out more about how Powwownow Engage can benefit your business, call us today:</span><br/><br/>
                <span class="font-bigger rockwell white" style="line-height: 140%">
                    0800 0<!-- AntiSkype comment -->22 9900 or +44 (0) 2<!-- AntiSkype comment -->0 3398 9900
                </span>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>