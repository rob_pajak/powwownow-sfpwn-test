<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('About Us'),
                'headingTitle' => __('About Us'),
                'headingSize' => 's',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <p>Powwownow was founded in 2004, offering customers low-cost conference calling facilities with the ethos of no booking, no billing, no fuss. There is no need for a customer to book a conference room and they never receive a bill from Powwownow. They only pay the cost of their own phone call, which is added to their standard telecoms bill. </p>
            <p>Powwownow is Europe’s fastest growing free conference call provider and operates in 15 countries including the UK, Germany and France.</p>
            <p>Powwownow and Audio-cast are trading names of Via-Vox Limited, which is a UK incorporated company registered at Companies House (Company Number: 04646978, VAT REG: 813 0934 52).</p>
            <p class="margin-bottom-10"><strong>Find out what our customers say about us </strong><a title="Testimonials" href="<?php echo url_for('@testimonials'); ?>">here</a><strong>.</strong></p>
        </div>
        <div class="margin-bottom-20">
            <p style="font-size: 13px;"><strong>Postal address:</strong></p>
            <p>Via-Vox Limited<br/>1st Floor, Vectra House<br/>36 Paradise Road<br/>Richmond<br/>Surrey<br/>TW9 1SE</p>
            <p>Telephone Number: 0203 398 0398<br/>Fax Number: +44 (0)20 3355 4262</p>
            <p>In 2013 Powwownow was acquired by PGi, a global leader in collaboration and virtual meetings for over 20 years and now the second largest independent conferencing provider in Europe. <a href="http://www.prnewswire.co.uk/news-releases/pgi-acquires-powwownow-uks-leading-conferencing-and-collaboration-provider-for-smbs-234368541.html" target="_blank">Press release</a>.</p>
            <p><img src="/sfimages/a_PGi_company.png" alt="PGI logo" width="111" height="32" /></p>
        </div>
        <div class="clearfix">
            <h2 class="rockwell light green margin-bottom-20">People at Powwownow</h2>
            <p class="margin-bottom-20 clearfix">
                <img title="Paul Lees" src="/sfimages/people/pl-photo3.jpg" alt="Paul Lees" width="128" height="190" class="margin-bottom-10" style="float: left; margin-right: 10px;" />
                <strong>Paul Lees, Founder</strong><br/><br/>
                Paul founded Powwownow in 2004. Paul came from ViewsCast as Technical Director and Product Architect. In his role at ViewsCast, Paul helped to develop an advanced CTI / Internet platform producing real-time measurement of customer opinions over the telephone and internet for clients including BT, Norwich Union and British Gas.<br/><br/>
                Prior to his pioneering work with ViewsCast, Paul had 15 years' experience in the computer industry, including BT, TNT, Maritz and Research International, in systems design, development and programme implementation.
            </p>
            <p class="margin-bottom-20 clearfix">
                <img title="Jason Downes" src="/sfimages/people/jd-photo3.jpg" alt="Jason Downes" width="128" height="190" class="margin-bottom-10" style="float: left; margin-right: 10px;" />
                <strong>Jason Downes, General Manager</strong><br/><br/>
                Jason came to Powwownow from Staples Advantage where he was UK Managing Director. Jason was responsible for the leadership and running of Staples Advantage, which provides office products and services within the B2B sector and generates sales in excess of £124 million with 450 employees.<br/><br/>
                Jason has a wealth of more than 18 years’ experience managing large teams across the UK and in the business sector with big brands such as Office Depot and Rentokil Initial.
            </p>
            <p class="margin-bottom-20 clearfix">
                <img title="Chris Martin" src="/sfimages/people/cm-photo3.jpg" alt="Chris Martin" width="128" height="190" class="margin-bottom-10" style="float: left; margin-right: 10px;" />
                <strong>Chris Martin, Chief Technology Officer</strong><br/><br/>
                Chris brings over 20 years of experience in building and leading technology teams, to deliver innovative and scalable technology solutions for business and consumer sectors, to Powwownow. In his previous role as CTO at Cheapflights Media, Chris was responsible for architecting, building and supporting the technology platform to satisfy over 140 million global annual visits and the gathering of millions of daily travel deals. <br/><br/>
                Prior to his role at Cheapflights Media, Chris was CTO of eBuyer.com, a leading UK e-commerce web site, where he was responsible for the website and all e-commerce and back-office systems to support the business. Chris has also led Production and Engineering teams for the renowned web search business AskJeeves in Europe.
            </p>
            <p class="margin-bottom-20 clearfix">
                <img title="Robert Gorby" src="/sfimages/people/rg-photo3.jpg" alt="Robert Gorby" width="128" height="190" class="margin-bottom-10" style="float: left; margin-right: 10px;" />
                <strong>Robert Gorby, Marketing Director</strong><br/><br/>
                Robert joined Powwownow from AVG Technologies where he was Global Head of SMB Marketing. Specialising in B2B marketing, Robert has more than 15 years of international marketing experience across a number of industries including technology, telecommunications, media and publishing in a range of senior roles.<br/><br/>
                A native of Dublin, Robert has a B.Sc. in Marketing Management and a MA Arts from Trinity College, Dublin. He has spent a number of years living in France and is fluent in both French and Italian.
            </p>
            <p class="margin-bottom-20 clearfix">
                <img title="Andrew Johnson" src="/sfimages/people/aj-photo3.jpg" alt="Anderew Johnson" width="128" height="190" class="margin-bottom-10" style="float: left; margin-right: 10px;" />
                <strong>Andrew Johnson, Finance Director</strong><br/><br/>
                Andrew has 11 years of finance experience from the aviation, shipping and property sectors including four years within telecommunications. Andrew joined Powwownow from Urban Wimax Networks PLC where he was Financial Controller, and worked as a key member of their senior management team.<br/><br/>
                Andrew is a chartered accountant and also has a degree in accounting and finance.
            </p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
