<div class="contactusSuccess">
    <h2>Thank you for your email!</h2>
    <p>We will reply to your request shortly. In the meantime, please check out these useful links:</p>
    <br/>
    <ul class="chevron">
        <li class="png"><a title="PIN Reminder" href="/PIN-Reminder">PIN Reminder</a></li>
        <li class="png"><a title="Frequently Asked Questions" href="<?php echo url_for('@faqs'); ?>">FAQs</a></li>
        <li class="png"><a title="How Conference Calling Works" href="<?php echo url_for('@how_conference_calling_works'); ?>">How Conference Calling works</a></li>
        <li class="png"><a title="How Web Conferencing Works" href="<?php echo url_for('@how_web_conferencing_works'); ?>">How Web Conferencing works</a></li>
        <li class="png"><a title="International Numbers and Rates" href="<?php echo url_for('@international_number_rates'); ?>">International numbers &amp; rates</a></li>
        <li class="png"><a title="Powwownow Costs" href="<?php echo url_for('@costs'); ?>">What it costs</a></li>
        <li class="png"><a title="About Powwownow" href="<?php echo url_for('@about_us'); ?>">About us</a></li>
    </ul>
</div>