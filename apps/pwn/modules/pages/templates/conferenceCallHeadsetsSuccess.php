<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'Conference call headsets')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Conference Call Headsets'),'title' => __('Conference Call Headsets'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Call Headsets'), 'headingSize' => 'l', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>When you&rsquo;re dealing with <a title="voice conferencing" href="<?php echo url_for('@voice_conferencing');?>">voice conferencing</a>, ensuring that everything that&rsquo;s said is clear and understandable to everyone on the call is vital. The best way to do this is to ditch the traditional telephone handset and invest in a conference calling headset. The headset you choose will need to fit your individual needs, but Powwownow have a few favourites that we recommend to our customers.</p>
                <h3 class="lightgreen rockwell">Plantronics TriStar H81 Headset</h3>
                <p>This is an old favourite that has gone through a number of different iterations over the years. It&rsquo;s very popular with contact centre workers due to its excellent acoustics, background noise reduction and feedback cancelling features. Great if you&rsquo;re <a title="telephone conferencing" href="<?php echo url_for('@telephone_conferencing');?>">telephone conferencing</a> with a participant who can&rsquo;t find the mute button!</p>
                <h3 class="lightgreen rockwell" style="padding-top: 20px;">Jabra 9350E Wireless Headset</h3>
                <p>If you hate being constrained by wires, this headset could be a winner for you. The high quality sound and consistent volume level means you&rsquo;ll never struggle to hear your conference call participants, while its future proof design ensures that it will continue to work when you upgrade your telephone system. The noise cancelling microphone ensures that your voice is always clear and crisp.</p>
                <h3 class="lightgreen rockwell" style="padding-top: 20px;">Sennheiser BW 900 Bluetooth Headset</h3>
                <p>This 'business class' headset is exceedingly trendy looking as well as very clever - it will work with your landline, PDA, VoIP-enabled computer and any type of mobile phone (it works great with our <a title="iphone conference app" href="<?php echo url_for('@iphone_conference_app');?>">conference call iphone app</a> too! The BW 900 has has incredible sound quality and it&rsquo;s great if you teleconference on the move a lot and want to go mobile!</p>
                <h3 class="lightgreen rockwell" style="padding-top: 20px;">Plantronics Audio 995 Wireless Stereo Headset</h3>
                <p>If you access your Powwownow conference calls via skype, this headset could be ideal for you. Unlike many computer headsets, the Audio 995 comes with a dongle that allows you to go wireless. The noise cancelling microphone acts as a "fast mic mute" that mutes your microphone when the boom is raised. It is also incredibly comfortable and you won't mind wearing it all day.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>