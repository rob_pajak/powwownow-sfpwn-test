<div class="grid_24 clearfix">
    <div class="grid_sub_19 floatleft">
        <p><strong>Clive Sawkins, CEO at BCS Global</strong><br/>What we found with the Powwownow team and the new Engage product is a first class conferencing solution that offers a stable, reliable and user friendly platform with a superb support mechanism behind it&hellip; we are encouraged to utilise business conferencing tools to maintain contact with both our customers and the BCS team. This can only be achieved successfully if the quality of the engagement is of a high enough standard and the experience can satisfy the users. We have achieved both, a pleasant experience with the product as well as a professional support relationship with Powwownow and I anticipate that this will  become a long and lucrative partnership.</p>
    </div>
    <div class="grid_sub_5 clearfix floatright">
        <img src="/sfimages/testimonials/BCS_global.jpg" alt="BCS Global" width="135" height="60"/>
    </div>
</div>
<div class="grid_24 clearfix">
    <div class="grid_sub_19 floatleft">
        <p><strong>Jason Johur, EMEA Business Strategist at Motorola Solutions</strong><br/>Powwownow's iPhone application just makes it easy for me and my colleagues to enter a conference call without needing to know any of the conference details, just simple, highly recommended.</p>
    </div>
    <div class="grid_sub_5 clearfix floatright">
        <img src="/sfimages/testimonials/Motorolalogo.png" alt="Motorola Logo" width="135"/>
    </div>
</div>
<div class="grid_24 clearfix">
    <div class="grid_sub_19 floatleft">
        <p><strong>Martin Chick, Managing Director at Mulberry Group</strong><br/>We are really impressed with the excellent customer service that we have received from Powwownow when we had questions about setting up the account and going through all the instructions of the service. We have used many other conference call providers and have left them because of poor customer service, so we are so happy that we got to experience such a high level professionalism from Powwownow.</p>
    </div>
    <div class="grid_sub_5 clearfix floatright">
        <img src="/sfimages/testimonials/Mulberrylogo.jpg" alt="Mulberry Logo" width="135"/>
    </div>
</div>
<div class="grid_24 clearfix">
    <div class="grid_sub_19 floatleft">
        <p><strong>Johnny Morris, Managing Director at Cognac UK Ltd</strong><br/>Before I was introduced to Powwownow, I used a broad range of conference calling options. Now Powwownow's service is the only one. So simple to use. Nobody does it better!</p>
    </div>
    <div class="grid_sub_5 clearfix floatright">
        <img src="/sfimages/testimonials/Congaclogo.jpg" alt="Congac Logo" width="135"/>
    </div>
</div>
<div class="grid_24 clearfix">
    <div class="grid_sub_19 floatleft">
        <p><strong>Andrew Jones, Managing Director at IonField Systems Limited</strong><br/>It is not often (in fact this is a first) that I feel compelled to compliment a company on a great service, but the quality of the product offered by Powwownow is exceptional. Simple to use, cost effective and innovative. As a start up company working with companies throughout Europe and the US, your service has allowed me to present a professional face to the customer with an extremely reliable way for me to conference and importantly, it is simple and low cost for the customer to join. The iPhone app is also excellent. Looking forward to seeing how Powwownow develops.</p>
    </div>
    <div class="grid_sub_5 clearfix floatright">
        <img src="/sfimages/testimonials/Ionfieldlogo.jpg" alt="Ionfield Logo" width="135"/>
    </div>
</div>
<div class="grid_24 clearfix">
    <div class="grid_sub_19 floatleft">
        <p><strong>Richard Burden, Director at RLB Project Management Ltd</strong><br/>I've used Powwownow for 7 years as it is the easiest of all the conference call services I've come across. Now I've started using Enhanced Access. No need to set anything up or register your call with anyone and you even get to choose your own PIN. It couldn't be simpler. I've encouraged all my business contacts to use it.</p>
    </div>
    <div class="grid_sub_5 clearfix floatright">
        <img src="/sfimages/testimonials/riblogo.jpg" alt="RIB Logo" width="135"/>
    </div>
</div>