<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'Pharmaceutical')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Pharmaceutical Conference Calls'),'title' => __('Pharmaceutical Conference Calls'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Pharmaceutical Conference Calls'), 'headingSize' => 'xxl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>Working in the pharmaceutical industry can leave you feeling like you&rsquo;re being tugged in a million different directions: from liaising with consulting physicians, to training internal teams, bargaining with vendors, and charming the media, there&rsquo;s always someone whose ear you need to bend. The beauty of Powwownow&rsquo;s free, no fuss <a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services');?>">teleconferencing services</a> is that they are entirely reservationless and allow you to hold a conference call whenever the need arises.</p>
                <p>With ever fluctuating government and drug regulating body mandates, it can be hard to make sure your staff stay up to date with the latest changes. If something big is announced, you can quickly and easily pull together your entire staff on a <a title="free conference call" href="<?php echo url_for('@free_conference_call');?>">free conference call</a> from Powwownow. This simple teleconference system ensures that everyone knows to conform to new regulations without having to leave their desks or travel to head office. This can also be hugely advantageous when bringing together marketing focus groups, as you don&rsquo;t need to pay for travel expenses to bring the group members to your office and you can conduct these telephone meetings from whatever location suits best. When you need to get the public opinion pinned down fast, there is no better way to get started than with a conference call!</p>
                <p>Travelling to and from sales appointments, medical seminars and press conferences can become expensive. Similarly, constantly having staff travelling can take a lot of time away from driving up sales. You can get your staff off the road and back to work by using our&nbsp;<a title="international conference call" href="<?php echo url_for('@international_conference_call');?>">international conference call</a> service!</p>
                <p>To hold a Powwownow conference call all your staff will need is a telephone, a dial-in number and a Powwownow PIN and they can easily contact interested parties, put together sales pitches and speak to the media in no time at all. By signing up to Powwownow&rsquo;s Premium package you can add personalised company branding to your conference calls which allows you to add a customisable welcome message to your calls and implement personal branding to your web conferences. These little touches can set you apart from the crowd and ensure your sales pitch is more memorable than your competitor&rsquo;s.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>