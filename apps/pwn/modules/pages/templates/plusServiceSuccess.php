<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Plus at a Glance'),
                'headingTitle' => __('Plus at a Glance'),
                'headingSize' => 'l',
                'headingUserType' => 'plus'
            )
        ); ?>
        <div class="bubble-blue2 white png">
            <div>
                <p>Powwownow Plus offers you all the great features of our free conference calling service plus additional products and international dial-in numbers.</p>
                <strong>Additional benefits of Plus:</strong>
                <ul id="features-list">
                    <li><p>Save even more money on your conference calls with one of our <a class="white" href="<?php url_for('@call_minute_bundles'); ?>">money-saving minute bundles</a></p></li>
                    <li><p>The option to purchase Pay As You Go Call Credit to use Worldwide Freephone and Landline numbers</p></li>
                    <li><p>Greater security with the addition of a Chairperson PIN. Keep this PIN for yourself and share the other one with your participants</p></li>
                    <li><p>The opportunity to purchase a Branded Welcome Message for a dedicated conference call number</p></li>
                    <li><p>The ability to add and manage multiple users under one account</p></li>
                </ul>
                <p>Remember, signing up is free and easy, so why not discover the benefits today?</p>
                <button data-tracking-id="Plus-Service-Top" class="button-orange get-plus floatright" style="position: relative;" onclick="dataLayer.push({'event': 'Plus - Get Started - Top/onClick'});"><span>Get Started Now</span></button>
            </div>
        </div>

        <?php include_component('commonComponents', 'genericHeading', array('title' => __('Why choose Plus?'), 'headingSize' => 'l', 'user_type' => 'plus')); ?>

        <p class="rockwell font-bigger">This service is free, offering additional security and control over your account.</p>

        <?php include_component('commonComponents', 'whyPlus', array('page' => 'plusService')); ?>

        <h4>Extra features</h4>
        <?php include_component('commonComponents', 'plusFeatures', array('page' => 'plusService')); ?>

        <div class="hr-spotted-top png content-seperator"></div>
        <div class="margin-top-10">
            <?php include_component('commonComponents', 'genericHeading', array('title' => __('What It Costs'), 'headingSize' => 'm', 'user_type' => 'plus')); ?>
        </div>

        <p>Powwownow Plus is free to sign up to.</p>

        <p>Products are purchased independently and Pay As You Go Call Credit can be bought in order to use the Freephone and Landline numbers. Credit can be set up as an auto top-up, removing the hassle of making continual purchases.</p>

        <p>For those that make regular conference calls, we offer a range of money-saving monthly minute Bundles. Depending on the Bundle, this can be assigned either to a single or multiple account user.There is no set-up or cancellation fee so things really couldn’t be simpler!</p>

        <h4 class="margin-top-5">Dial-in numbers and rates</h4>
        <p>With Plus, you can choose from:</p>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" /></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Shared Cost numbers (e.g. 0844...)</span><br/>All callers pay their own call charges of 4.3p/min if dialling from a BT landline, other providers may vary.  These numbers are automatically enabled when you sign up for Plus, so you can start conference calling straight away.</p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" /></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Freephone numbers (e.g. 0800...)</span><br/>Free to Participants, but the Chairperson will be charged for all calls to this number, starting from 7p/min.</p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" /></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Landline numbers (e.g. 03...)</span><br/>Many network providers include these numbers in their home, business and mobile call packages, therefore calls may be free or very low cost. The Chairperson will pay an additional per minute call charge for all callers, starting from 4.3p/min.</p>
            </div>
        </div>

        <div class="bubble-blue3 margin-top-20 png">
            <div>
                <span class="font-bigger rockwell white">Register for free and only pay for what you need.</span>
                <button data-tracking-id="Plus-Service-Bottom" class="button-orange get-plus floatright" style="position: relative;" onclick="dataLayer.push({'event': 'Plus - Get Started - Bottom/onClick'});">
                    <span>Get Started Now</span>
                </button>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
