<div class="row">
    <div class="small-12 medium-12 large-12 columns">
        <header class="logo">
            <a href="<?php echo url_for('@homepage');?>">
                <img src="/cx2/img/mobile/340_logo_strap_line_white.png" width="200" alt="Powwownow">
            </a>
        </header>
    </div>
</div>
<div class="row">
    <div class="small-12 medium-8 large-7 medium-centered large-centered columns">

        <section class="reset-password-container">
            <?php if (!$tokenValid) : ?>
                <h2 class="section-title">You’re too late...</h2>
                <p class="section-text">To protect your account from nasty hackers, reset password links only work for 24 hours. </p>
                <p class="section-text"><a href="<?php echo url_for('@forgotten_password'); ?>">Click here</a> to request a new one and access your account</p>
            <?php else : ?>
                <h2 class="section-title">Reset your password</h2>
                <div id="js.ko.reset.password.container" data-bind="template: {name: 'reset.password.container.data', afterRender : initialise('<?php echo $token; ?>', '<?php echo $contactRef; ?>') }"></div>
                <script type="text/html" id="reset.password.container.data">
                    <!-- ko ifnot: ui.success.displaySuccess -->
                        <p class="section-text">Almost there! All that's left to do is choose a new password and you're all set to get back to business.</p>
                        <form class='cx_form' data-bind="submit: doResetPassword" novalidate>
                            <div class="row collapse">
                                <div class="small-12 medium-12 large-12 columns">
                                    <input class="password-input" type='password' data-bind='textInput: model.resetpassword["password"]' placeholder="new password" />
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="small-12 medium-12 large-12 columns">
                                    <input class="password-input" type='password' data-bind='textInput: model.resetpassword["confirm_password"]' placeholder="confirm password" />
                                </div>
                            </div>
                            <div class="row error-message" data-bind="fadeVisible: ui.errors.message().length > 0">
                                <div class="small-12 medium-12 large-12 columns">
                                    <p data-bind="html: ui.errors.message()"></p>
                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="small-12 medium-12 large-12 columns">
                                    <img id="loader" src="/cx2/img/throbber.gif" alt="loading..." class="loader" data-bind="visible: ui.isEnabled.loader"/>
                                    <button type="submit" class="right button-blue-set-a" data-bind="enable: ui.isEnabled.submit">Update my password</button>
                                </div>
                            </div>
                        </form>
                    <!-- /ko -->
                    <!-- ko if: ui.success.displaySuccess -->
                        <p class="section-text">Success! Your password has been updated and you can now use it the next time you visit myPowwownow.</p>
                        <a class="right button-blue-set-a" href="<?php echo url_for('@mypwn_index')?>">Go to myPowwownow</a>
                    <!-- /ko -->
                </script>
            <?php endif; ?>
        </section>
    </div>
</div>







