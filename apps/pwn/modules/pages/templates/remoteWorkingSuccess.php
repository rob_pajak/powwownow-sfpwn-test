<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">

        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
                'header' => 'Remote working',
                'subHeader' => 'Cost of travel to meetings mounting up? Or simply looking to save on time? Having a <a title="Conference Call" href="' .url_for('@conference_call') . '">conference call</a>, <a title="Web Conferencing" href="' . url_for('@web_conferencing') . '">web conference</a> or <a title="Video Conferencing" href="' . url_for('@video_conferencing') . '">video conference</a> instead can save you both.',
                'hintBoxTitle' => "It's easy as 1, 2, 3!",
                'tabTitle' => 'Get started',
                'registrationSource' => $registrationSource,
            )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Remote working – the benefits</h3>
                <p style="text-align: justify;"><strong>Greater efficiency</strong><br/>There is no need to travel to meetings with congestion holding you up.</p>
                <p style="text-align: justify;"><strong>Improved morale</strong><br/>Remote working is a great motivator leading to increased staff morale, and better quality of work produced.</p>
                <p style="text-align: justify;"><strong>Reduced absenteeism</strong><br/>Commuter stress could lead to an increase in absenteeism. Remote working allows employees to still be able to work from home instead.</p>
                <p style="text-align: justify;"><strong>Reduction in costs</strong><br/>Businesses can save hugely on office running costs by implementing remote working.</p>
                <p style="text-align: justify;">Some businesses may feel sceptical about having remote workers, but with Powwownow there is no need to worry.</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Remote Working</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>