<div class="clearfix" id="page"><!-- column -->
<div class="position_content" id="page_position_content">
<div class="browser_width colelem" id="u78"><!-- simple frame --></div>
<div class="clearfix colelem" id="pu1462"><!-- column -->
    <a class="nonblock nontext clip_frame pinned-colelem" id="u1462" style="border-bottom: 0px;" href="http://www.powwownow.co.uk/"><!-- image --><img class="block" id="u1462_img" src="/sfimages/the-workplace-cant/logo-big.png" alt="" width="197" height="59"/></a>
    <div class="pointer_cursor clearfix pinned-colelem" id="u812-9"><!-- content -->
        <a class="block" href="<?php echo url_for('@homepage'); ?>" target="_blank"></a>
        <p>Home&nbsp;&nbsp; |&nbsp;&nbsp; <a class="nonblock" href="<?php echo url_for('@conference_call') ?>" target="_blank">Conference Call </a>&nbsp; |&nbsp;&nbsp; <a class="nonblock" href="<?php echo url_for('@contact_us'); ?>" target="_blank">Contact Us</a></p>
    </div>
    <img class="mse_pre_init" id="u106-4" src="/sfimages/the-workplace-cant/u106-4.png" alt="The identity of the workplace" width="578" height="72"/><!-- rasterized frame -->
    <div class="clip_frame mse_pre_init" id="u108"><!-- image -->
        <img class="block" id="u108_img" src="/sfimages/the-workplace-cant/cant-word-bubble.png" alt="" width="532" height="223"/>
    </div>
    <div class="pinned-colelem" id="u1298"><!-- custom html -->
        <span class="st_facebook_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="http://www.facebook.com/sharer.php?u=www.powwownow.co.uk/Conference-Call/The-Workplace-Cant" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - Facebook/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/facebook_32.png" alt="Facebook Share"/></a>
            </span>
        </span><br />
        <span class="st_twitter_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="http://twitter.com/share?text=The%20identity%20of%20the%20workplace%20Can%27t%20http%3A%2F%2Fwww.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant%20via%20%40Powwownow%20%23iamacan" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - Twitter/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/twitter_32.png" alt="Twitter Share"/></a>
            </span>
        </span><br />
        <span class="st_linkedin_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fwww.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant&amp;title=The%20Workplace%20Can%E2%80%99t%20%E2%80%93%20Be%20a%20Can%20with%20Powwownow%2C%20Let%E2%80%99s%20Get%20it%20Done&amp;summary=We%20completed%20a%20survey%20in%20association%20with%20OnePoll%20to%20find%20out%20the%20identity%20of%20%E2%80%98Can%E2%80%99ts%E2%80%99%20within%20the%20UK.%20Find%20out%20the%20characteristics%20of%20a%20Can%E2%80%99t%20and%20where%20in%20the%20UK%20to%20find%20them" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - LinkedIn/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/linkedin_32.png" alt="LinkedIn Share"/></a>
            </span>
        </span><br />
        <span class="st_googleplus_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a href="https://plus.google.com/share?url=www.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant" onclick="window.open(this.href,'window','width=626,height=436,resizable,scrollbars,toolbar,menubar'); dataLayer.push({'event': 'WorkplaceCant - Share - GooglePlus/onClick'}); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/googleplus_32.png" alt="Google+ Share"/></a>
            </span><br />
        </span>
        <span class="st_email_large">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton">
                <a id="mailto_link" href="mailto:?subject=The%20Workplace%20Can%E2%80%99t%20%E2%80%93%20Be%20a%20Can%20with%20Powwownow%2C%20Let%E2%80%99s%20Get%20it%20Done&amp;body=www.powwownow.co.uk%2FConference-Call%2FThe-Workplace-Cant" onclick="dataLayer.push({'event': 'WorkplaceCant - Share - Email/onClick'}); setTimeout(function() {window.location = $('#mailto_link').attr('href');}, 1000); return false;" style="border: 0px;"><img src="/sfimages/the-workplace-cant/email_32.png" alt="Email Share"/></a>
            </span>
        </span>
    </div>

    <div class="clip_frame mse_pre_init" id="u101"><!-- image -->
        <img class="block" id="u101_img" src="/sfimages/the-workplace-cant/sarah-homepage.png" alt="" width="153" height="366"/>
    </div>
    <img class="mse_pre_init ose_pre_init" id="u116-4" src="/sfimages/the-workplace-cant/u116-4.png" alt="An analysis of the most obstructive and unsympathetic people in the Great British workplace, based on national research in conjunction with OnePoll." width="415" height="108"/><!-- rasterized frame -->
    <div class="browser_width mse_pre_init" id="u99"><!-- simple frame --></div>
    <div class="clearfix mse_pre_init" id="u1451"><!-- column -->
        <a class="nonblock nontext anim_swing colelem no-decoration" id="u119-4" href="index.html#woman"><!-- rasterized frame --><img id="u119-4_img" src="/sfimages/the-workplace-cant/u119-4.png" alt="SCROLL DOWN" width="350" height="27"/></a>
        <a class="nonblock nontext anim_swing clip_frame colelem no-decoration" id="u120" href="index.html#woman"><!-- image --><img class="block" id="u120_img" src="/sfimages/the-workplace-cant/scroll-down-arrow.png" alt="" width="17" height="15"/></a>
    </div>
</div>
<div class="browser_width mse_pre_init" id="u142"><!-- simple frame --></div>
<a class="anchor_item colelem" id="anatomy"></a>
<img class="mse_pre_init ose_pre_init" id="u135-6" src="/sfimages/the-workplace-cant/u135-6.png" alt="The anatomy of a workplace ‘Can’t’
(i.e. an apathetic and unhelpful colleague) according to a poll of 2,000 UK&#45;based respondents: " width="953" height="93"/><!-- rasterized frame -->
<div class="clip_frame mse_pre_init" id="u136"><!-- image -->
    <img class="block" id="u136_img" src="/sfimages/the-workplace-cant/sarah-slide-2.png" alt="" width="163" height="387"/>
</div>
<div class="clearfix mse_pre_init" id="u1382"><!-- column -->
    <a class="nonblock nontext anim_swing ose_pre_init colelem" id="u1378-4" href="index.html#man"><!-- rasterized frame --><img id="u1378-4_img" src="/sfimages/the-workplace-cant/u1378-4.png" alt="SCROLL DOWN" width="350" height="27"/></a>
    <a class="nonblock nontext anim_swing clip_frame ose_pre_init colelem" id="u1379" href="index.html#man"><!-- image --><img class="block" id="u1379_img" src="/sfimages/the-workplace-cant/scroll-down-arrow.png" alt="" width="17" height="15"/></a>
</div>
<div class="clearfix mse_pre_init" id="u1485"><!-- group -->
    <div class="clip_frame grpelem" id="u147"><!-- image -->
        <img class="block" id="u147_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
    </div>
    <div class="clearfix grpelem" id="pu157-4"><!-- column -->
        <img class="colelem" id="u157-4" src="/sfimages/the-workplace-cant/u157-4.png" alt="Sarah" width="158" height="54"/><!-- rasterized frame -->
        <img class="colelem" id="u156-4" src="/sfimages/the-workplace-cant/u156-4.png" alt="The most common workplace ‘Can’t’ name" width="239" height="66"/><!-- rasterized frame -->
    </div>
</div>
<div class="clearfix mse_pre_init" id="u832"><!-- group -->
    <div class="clearfix grpelem" id="pu144-4"><!-- column -->
        <img class="colelem" id="u144-4" src="/sfimages/the-workplace-cant/u144-4.png" alt="5’7" width="103" height="54"/><!-- rasterized frame -->
        <img class="colelem" id="u146-4" src="/sfimages/the-workplace-cant/u146-4.png" alt="Average height of workplace Can’t" width="205" height="57"/><!-- rasterized frame -->
    </div>
    <div class="clip_frame grpelem" id="u175"><!-- image -->
        <img class="block" id="u175_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
    </div>
</div>
<div class="clearfix mse_pre_init" id="u828"><!-- group -->
    <div class="clip_frame grpelem" id="u163"><!-- image -->
        <img class="block" id="u163_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
    </div>
    <div class="clearfix grpelem" id="pu160-4"><!-- column -->
        <img class="colelem" id="u160-4" src="/sfimages/the-workplace-cant/u160-4.png" alt="31%" width="158" height="54"/><!-- rasterized frame -->
        <img class="colelem" id="u159-4" src="/sfimages/the-workplace-cant/u159-4.png" alt="Cited her as brunette" width="239" height="26"/><!-- rasterized frame -->
    </div>
</div>
<div class="clearfix mse_pre_init" id="u830"><!-- group -->
    <div class="clearfix grpelem" id="pu155-4"><!-- column -->
        <img class="colelem" id="u155-4" src="/sfimages/the-workplace-cant/u155-4.png" alt="44" width="103" height="55"/><!-- rasterized frame -->
        <img class="colelem" id="u154-4" src="/sfimages/the-workplace-cant/u154-4.png" alt="Average age of workplace Can’t" width="215" height="51"/><!-- rasterized frame -->
    </div>
    <div class="clip_frame grpelem" id="u169"><!-- image -->
        <img class="block" id="u169_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
    </div>
</div>
<div class="clearfix colelem" id="u264-3"><!-- content -->
    <p>&nbsp;</p>
</div>
<div class="clearfix mse_pre_init" id="u829"><!-- group -->
    <div class="clip_frame grpelem" id="u166"><!-- image -->
        <img class="block" id="u166_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
    </div>
    <div class="clearfix grpelem" id="pu161-4"><!-- column -->
        <img class="colelem" id="u161-4" src="/sfimages/the-workplace-cant/u161-4.png" alt="19%" width="158" height="54"/><!-- rasterized frame -->
        <img class="colelem" id="u162-4" src="/sfimages/the-workplace-cant/u162-4.png" alt="Based in HR Department" width="239" height="66"/><!-- rasterized frame -->
    </div>
</div>
<a class="anchor_item colelem" id="woman"></a>
<div class="clearfix mse_pre_init" id="u831"><!-- group -->
    <div class="clearfix grpelem" id="pu152-4"><!-- column -->
        <img class="colelem" id="u152-4" src="/sfimages/the-workplace-cant/u152-4.png" alt="54%" width="103" height="55"/><!-- rasterized frame -->
        <img class="colelem" id="u153-4" src="/sfimages/the-workplace-cant/u153-4.png" alt="Said they were female" width="224" height="29"/><!-- rasterized frame -->
    </div>
    <div class="clip_frame grpelem" id="u172"><!-- image -->
        <img class="block" id="u172_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
    </div>
</div>
<div class="browser_width colelem" id="u324"><!-- group -->
    <div class="clearfix" id="u324_align_to_page">
        <a class="anchor_item grpelem" id="man"></a>
    </div>
</div>
<div class="clearfix colelem" id="pu189-5"><!-- column -->
    <div class="position_content" id="pu189-5_position_content">
        <img class="mse_pre_init" id="u189-5" src="/sfimages/the-workplace-cant/u189-5.png" alt="From those who said their " width="953" height="66"/><!-- rasterized frame -->
        <img class="mse_pre_init" id="u853-4" src="/sfimages/the-workplace-cant/u853-4.png" alt="workplace Can't was a man:" width="953" height="74"/><!-- rasterized frame -->
        <div class="clip_frame mse_pre_init" id="u1366"><!-- image -->
            <img class="block" id="u1366_img" src="/sfimages/the-workplace-cant/david-2.png" alt="" width="156" height="388"/>
        </div>
        <div class="clearfix mse_pre_init" id="u879"><!-- group -->
            <div class="clearfix grpelem" id="pu210-4"><!-- column -->
                <img class="colelem" id="u210-4" src="/sfimages/the-workplace-cant/u210-4.png" alt="David" width="158" height="54"/><!-- rasterized frame -->
                <img class="colelem" id="u854-4" src="/sfimages/the-workplace-cant/u854-4.png" alt="The most common name for a male workplace Can’t" width="255" height="55"/><!-- rasterized frame -->
            </div>
            <div class="clip_frame grpelem" id="u866"><!-- image -->
                <img class="block" id="u866_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
            </div>
        </div>
        <div class="clearfix mse_pre_init" id="u877"><!-- group -->
            <div class="clip_frame grpelem" id="u863"><!-- image -->
                <img class="block" id="u863_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
            </div>
            <div class="clearfix grpelem" id="pu191-4"><!-- column -->
                <img class="colelem" id="u191-4" src="/sfimages/the-workplace-cant/u191-4.png" alt="25%" width="103" height="55"/><!-- rasterized frame -->
                <img class="colelem" id="u193-4" src="/sfimages/the-workplace-cant/u193-4.png" alt="Cited him as having grey hair" width="158" height="52"/><!-- rasterized frame -->
            </div>
        </div>
        <div class="clearfix mse_pre_init" id="u874"><!-- group -->
            <div class="clearfix grpelem" id="pu203-4"><!-- column -->
                <img class="colelem" id="u203-4" src="/sfimages/the-workplace-cant/u203-4.png" alt="16%" width="102" height="55"/><!-- rasterized frame -->
                <img class="colelem" id="u216-4" src="/sfimages/the-workplace-cant/u216-4.png" alt="Estimated his height as being between 5'9 and 5'11" width="252" height="67"/><!-- rasterized frame -->
            </div>
            <div class="clip_frame grpelem" id="u194"><!-- image -->
                <img class="block" id="u194_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
            </div>
        </div>
        <div class="clearfix mse_pre_init" id="u876"><!-- group -->
            <div class="clip_frame grpelem" id="u860"><!-- image -->
                <img class="block" id="u860_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
            </div>
            <div class="clearfix grpelem" id="pu856-4"><!-- column -->
                <img class="colelem" id="u856-4" src="/sfimages/the-workplace-cant/u856-4.png" alt="13%" width="103" height="55"/><!-- rasterized frame -->
                <img class="colelem" id="u855-4" src="/sfimages/the-workplace-cant/u855-4.png" alt="The majority said that he worked in their employer’s HR department" width="252" height="67"/><!-- rasterized frame -->
            </div>
        </div>
        <div class="clearfix mse_pre_init" id="u875"><!-- group -->
            <div class="clearfix grpelem" id="pu190-4"><!-- column -->
                <img class="colelem" id="u190-4" src="/sfimages/the-workplace-cant/u190-4.png" alt="33%" width="103" height="54"/><!-- rasterized frame -->
                <img class="colelem" id="u192-4" src="/sfimages/the-workplace-cant/u192-4.png" alt="Guessed that he is aged between 45 and 54" width="219" height="80"/><!-- rasterized frame -->
            </div>
            <div class="clip_frame grpelem" id="u857"><!-- image -->
                <img class="block" id="u857_img" src="/sfimages/the-workplace-cant/slide-2-rule.png" alt="" width="127" height="1"/>
            </div>
        </div>
    </div>
</div>
<div class="browser_width mse_pre_init" id="u272"><!-- simple frame --></div>
<img class="mse_pre_init ose_pre_init" id="u274-5" src="/sfimages/the-workplace-cant/u274-5.png" alt="The workplace ‘Can’t’ might be seen with: " width="953" height="78"/><!-- rasterized frame -->
<div class="clearfix mse_pre_init" id="u1241"><!-- column -->
    <div class="clip_frame ose_pre_init colelem" id="u275"><!-- image -->
        <img class="block" id="u275_img" src="/sfimages/the-workplace-cant/horn-glasses.png" alt="" width="213" height="214"/>
    </div>
    <img class="ose_pre_init colelem" id="u308-4" src="/sfimages/the-workplace-cant/u308-4.png" alt="Horn Rim Glasses" width="213" height="70"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1245"><!-- column -->
    <div class="clip_frame ose_pre_init colelem" id="u280"><!-- image -->
        <img class="block" id="u280_img" src="/sfimages/the-workplace-cant/big-nose.png" alt="" width="209" height="222"/>
    </div>
    <img class="ose_pre_init colelem" id="u309-4" src="/sfimages/the-workplace-cant/u309-4.png" alt="Large Nose" width="209" height="39"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1246"><!-- column -->
    <div class="clip_frame ose_pre_init colelem" id="u285"><!-- image -->
        <img class="block" id="u285_img" src="/sfimages/the-workplace-cant/moustache.png" alt="" width="208" height="223"/>
    </div>
    <img class="ose_pre_init colelem" id="u310-4" src="/sfimages/the-workplace-cant/u310-4.png" alt="Moustache" width="208" height="39"/><!-- rasterized frame -->
</div>
<div class="ose_pre_init browser_width mse_pre_init" id="u1335"><!-- simple frame --></div>
<div class="clearfix mse_pre_init" id="u1346"><!-- column -->
    <div class="clip_frame ose_pre_init colelem" id="u295"><!-- image -->
        <img class="block" id="u295_img" src="/sfimages/the-workplace-cant/sandals.png" alt="" width="219" height="208"/>
    </div>
    <img class="ose_pre_init colelem" id="u312-6" src="/sfimages/the-workplace-cant/u312-6.png" alt="Sandals all  year round" width="213" height="50"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1347"><!-- column -->
    <div class="clip_frame ose_pre_init colelem" id="u290"><!-- image -->
        <img class="block" id="u290_img" src="/sfimages/the-workplace-cant/red-face.png" alt="" width="209" height="225"/>
    </div>
    <img class="ose_pre_init colelem" id="u311-4" src="/sfimages/the-workplace-cant/u311-4.png" alt="Red Face" width="207" height="70"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1348"><!-- column -->
    <div class="clip_frame ose_pre_init colelem" id="u300"><!-- image -->
        <img class="block" id="u300_img" src="/sfimages/the-workplace-cant/tattoo.png" alt="" width="208" height="208"/>
    </div>
    <img class="ose_pre_init colelem" id="u313-4" src="/sfimages/the-workplace-cant/u313-4.png" alt="Tattoos" width="208" height="39"/><!-- rasterized frame -->
</div>
<div class="clearfix colelem" id="pu322"><!-- group -->
    <div class="browser_width grpelem" id="u322"><!-- group -->
        <div class="clearfix" id="u322_align_to_page">
            <a class="anchor_item grpelem" id="charts"></a>
        </div>
    </div>
    <div class="clearfix grpelem" id="u1386-3"><!-- content -->
        <p>&nbsp;</p>
    </div>
</div>
<div class="clearfix colelem" id="pu323-4"><!-- column -->
    <div class="position_content" id="pu323-4_position_content">
        <img class="mse_pre_init ose_pre_init" id="u323-4" src="/sfimages/the-workplace-cant/u323-4.png" alt="From a list of personality traits, the top five " width="953" height="60"/><!-- rasterized frame -->
        <img class="mse_pre_init ose_pre_init" id="u895-4" src="/sfimages/the-workplace-cant/u895-4.png" alt="qualities of the workplace ‘Can’t’ were:" width="953" height="60"/><!-- rasterized frame -->
        <div class="clip_frame mse_pre_init" id="u340"><!-- image -->
            <img class="block" id="u340_img" src="/sfimages/the-workplace-cant/47-piechart.png" alt="" width="160" height="159"/>
        </div>
        <div class="clip_frame ose_pre_init mse_pre_init" id="u364"><!-- image -->
            <img class="block" id="u364_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
        </div>
        <img class="mse_pre_init" id="u374-4" src="/sfimages/the-workplace-cant/u374-4.png" alt="Deceitful" width="107" height="34"/><!-- rasterized frame -->
        <div class="clip_frame mse_pre_init" id="u345"><!-- image -->
            <img class="block" id="u345_img" src="/sfimages/the-workplace-cant/46-piechart.png" alt="" width="160" height="159"/>
        </div>
        <div class="clip_frame ose_pre_init mse_pre_init" id="u367"><!-- image -->
            <img class="block" id="u367_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
        </div>
        <img class="mse_pre_init" id="u375-4" src="/sfimages/the-workplace-cant/u375-4.png" alt="Unproductive" width="150" height="34"/><!-- rasterized frame -->
        <div class="clip_frame mse_pre_init" id="u334"><!-- image -->
            <img class="block" id="u334_img" src="/sfimages/the-workplace-cant/59-piechart.png" alt="" width="160" height="159"/>
        </div>
        <div class="clip_frame ose_pre_init mse_pre_init" id="u361"><!-- image -->
            <img class="block" id="u361_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
        </div>
        <img class="mse_pre_init" id="u373-4" src="/sfimages/the-workplace-cant/u373-4.png" alt="Confident" width="108" height="34"/><!-- rasterized frame -->
        <div class="clip_frame mse_pre_init" id="u350"><!-- image -->
            <img class="block" id="u350_img" src="/sfimages/the-workplace-cant/41-piechart.png" alt="" width="160" height="159"/>
        </div>
        <div class="clip_frame ose_pre_init mse_pre_init" id="u370"><!-- image -->
            <img class="block" id="u370_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
        </div>
        <img class="mse_pre_init" id="u376-4" src="/sfimages/the-workplace-cant/u376-4.png" alt="Committed " width="122" height="34"/><!-- rasterized frame -->
        <div class="clip_frame mse_pre_init" id="u329"><!-- image -->
            <img class="block" id="u329_img" src="/sfimages/the-workplace-cant/65-piechart.png" alt="" width="160" height="159"/>
        </div>
        <div class="clip_frame ose_pre_init mse_pre_init" id="u356"><!-- image -->
            <img class="block" id="u356_img" src="/sfimages/the-workplace-cant/piechart-shadow.png" alt="" width="111" height="15"/>
        </div>
        <img class="mse_pre_init" id="u355-4" src="/sfimages/the-workplace-cant/u355-4.png" alt="Argumentative" width="167" height="34"/><!-- rasterized frame -->
        <div class="browser_width mse_pre_init" id="u384"><!-- simple frame --></div>
    </div>
</div>
<img class="mse_pre_init" id="u385-5" src="/sfimages/the-workplace-cant/u385-5.png" alt="A ‘Can’t’ Watchers Guide" width="953" height="85"/><!-- rasterized frame -->
<img class="mse_pre_init" id="u1116-6" src="/sfimages/the-workplace-cant/u1116-6.png" alt="According to respondents in each area, the identities  of common workplace Can’ts across the UK." width="953" height="62"/><!-- rasterized frame -->
<div class="clip_frame mse_pre_init" id="u1311"><!-- image -->
    <img class="block" id="u1311_img" src="/sfimages/the-workplace-cant/blank-map-2.png" alt="" width="735" height="888"/>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u952"><!-- image -->
    <img class="block" id="u952_img" src="/sfimages/the-workplace-cant/scotland.png" alt="" width="385" height="442"/>
</div>
<div class="clearfix mse_pre_init" id="u1058"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u1052"><!-- image -->
        <img class="block" id="u1052_img" src="/sfimages/the-workplace-cant/scotland-rule.png" alt="" width="202" height="8"/>
    </div>
    <div class="clearfix grpelem" id="u996"><!-- column -->
        <img class="ose_pre_init colelem" id="u520-4" src="/sfimages/the-workplace-cant/u520-4.png" alt="Scotland" width="161" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u521-4" src="/sfimages/the-workplace-cant/u521-4.png" alt="Female, named Elizabeth, working in Customer Services" width="252" height="72"/><!-- rasterized frame -->
    </div>
</div>
<div class="clearfix mse_pre_init" id="u995"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u537"><!-- image -->
        <img class="block" id="u537_img" src="/sfimages/the-workplace-cant/north-east-rule.png" alt="" width="152" height="8"/>
    </div>
    <div class="clearfix grpelem" id="u994"><!-- column -->
        <img class="ose_pre_init colelem" id="u533-4" src="/sfimages/the-workplace-cant/u533-4.png" alt="North East" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u532-4" src="/sfimages/the-workplace-cant/u532-4.png" alt="Female, named Sophie/Elizabeth/Victoria (tied scores), working in HR" width="298" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u957"><!-- image -->
    <img class="block" id="u957_img" src="/sfimages/the-workplace-cant/north-east.png" alt="" width="100" height="141"/>
</div>
<div class="clearfix mse_pre_init" id="u998"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u547"><!-- image -->
        <img class="block" id="u547_img" src="/sfimages/the-workplace-cant/north-west-rule.png" alt="" width="222" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu543-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u543-4" src="/sfimages/the-workplace-cant/u543-4.png" alt="North West" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u542-6" src="/sfimages/the-workplace-cant/u542-6.png" alt="Female, named Emily,  working in Customer Services" width="298" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u974"><!-- image -->
    <img class="block" id="u974_img" src="/sfimages/the-workplace-cant/north-west.png" alt="" width="88" height="221"/>
</div>
<div class="clearfix colelem" id="u386-3"><!-- content -->
    <p>&nbsp;</p>
</div>
<div class="clearfix mse_pre_init" id="u1090"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u1083"><!-- image -->
        <img class="block" id="u1083_img" src="/sfimages/the-workplace-cant/northern-ireland-rule.png" alt="" width="402" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu1076-4"><!-- group -->
        <img class="ose_pre_init grpelem" id="u1076-4" src="/sfimages/the-workplace-cant/u1076-4.png" alt="Northern Ireland" width="317" height="50"/><!-- rasterized frame -->
        <img class="ose_pre_init grpelem" id="u1075-6" src="/sfimages/the-workplace-cant/u1075-6.png" alt="Male, named Peter/Thomas  (tied scores), working in HR" width="298" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1078"><!-- image -->
    <img class="block" id="u1078_img" src="/sfimages/the-workplace-cant/norther-ireland.png" alt="" width="153" height="124"/>
</div>
<div class="clearfix mse_pre_init" id="u1402"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u557"><!-- image -->
        <img class="block" id="u557_img" src="/sfimages/the-workplace-cant/north-east-rule.png" alt="" width="152" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu553-6"><!-- group -->
        <img class="ose_pre_init grpelem" id="u553-6" src="/sfimages/the-workplace-cant/u553-6.png" alt="Yorkshire
and Humberside" width="287" height="83"/><!-- rasterized frame -->
        <img class="ose_pre_init grpelem" id="u552-6" src="/sfimages/the-workplace-cant/u552-6.png" alt="Male, named Paul/John  (tied scores), working in HR" width="298" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u999"><!-- image -->
    <img class="block" id="u999_img" src="/sfimages/the-workplace-cant/yorkshire.png" alt="" width="150" height="130"/>
</div>
<div class="clearfix mse_pre_init" id="u1015"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u562"><!-- image -->
        <img class="block" id="u562_img" src="/sfimages/the-workplace-cant/north-west-rule.png" alt="" width="222" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu560-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u560-4" src="/sfimages/the-workplace-cant/u560-4.png" alt="West Midlands" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u561-6" src="/sfimages/the-workplace-cant/u561-6.png" alt="Female, named Julia,  working in HR" width="298" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1005"><!-- image -->
    <img class="block" id="u1005_img" src="/sfimages/the-workplace-cant/west-midlands.png" alt="" width="123" height="131"/>
</div>
<div class="clearfix mse_pre_init" id="u1016"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u570"><!-- image -->
        <img class="block" id="u570_img" src="/sfimages/the-workplace-cant/east-midlands-rule.png" alt="" width="102" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu566-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u566-4" src="/sfimages/the-workplace-cant/u566-4.png" alt="East Midlands" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u565-6" src="/sfimages/the-workplace-cant/u565-6.png" alt="Male, named David, working  in Customer Services" width="298" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1017"><!-- image -->
    <img class="block" id="u1017_img" src="/sfimages/the-workplace-cant/east-midlands.png" alt="" width="141" height="161"/>
</div>
<div class="clearfix mse_pre_init" id="u1022"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u580"><!-- image -->
        <img class="block" id="u580_img" src="/sfimages/the-workplace-cant/wales-rule.png" alt="" width="302" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu575-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u575-4" src="/sfimages/the-workplace-cant/u575-4.png" alt="Wales" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u576-4" src="/sfimages/the-workplace-cant/u576-4.png" alt="Female, named Sarah/Beth/Lucy (tied scores), working in HR" width="298" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1023"><!-- image -->
    <img class="block" id="u1023_img" src="/sfimages/the-workplace-cant/wales.png" alt="" width="175" height="212"/>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1028"><!-- image -->
    <img class="block" id="u1028_img" src="/sfimages/the-workplace-cant/east-anglia.png" alt="" width="162" height="150"/>
</div>
<div class="clearfix mse_pre_init" id="u1033"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u587"><!-- image -->
        <img class="block" id="u587_img" src="/sfimages/the-workplace-cant/east-midlands-rule.png" alt="" width="102" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu585-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u585-4" src="/sfimages/the-workplace-cant/u585-4.png" alt="East Anglia" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u586-6" src="/sfimages/the-workplace-cant/u586-6.png" alt="Female, named Sarah,  working in HR" width="218" height="45"/><!-- rasterized frame -->
    </div>
</div>
<div class="clearfix mse_pre_init" id="u1034"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u610"><!-- image -->
        <img class="block" id="u610_img" src="/sfimages/the-workplace-cant/london-rule.png" alt="" width="132" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu590-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u590-4" src="/sfimages/the-workplace-cant/u590-4.png" alt="London" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u591-4" src="/sfimages/the-workplace-cant/u591-4.png" alt="Female, named Julia, working in HR" width="218" height="45"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1185"><!-- image -->
    <img class="block" id="u1185_img" src="/sfimages/the-workplace-cant/london.png" alt="" width="55" height="45"/>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1041"><!-- image -->
    <img class="block" id="u1041_img" src="/sfimages/the-workplace-cant/south-east.png" alt="" width="214" height="158"/>
</div>
<div class="clearfix mse_pre_init" id="u1040"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u620"><!-- image -->
        <img class="block" id="u620_img" src="/sfimages/the-workplace-cant/east-midlands-rule.png" alt="" width="102" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu615-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u615-4" src="/sfimages/the-workplace-cant/u615-4.png" alt="South East" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u616-4" src="/sfimages/the-workplace-cant/u616-4.png" alt="Female, named Emma, working in HR" width="218" height="45"/><!-- rasterized frame -->
    </div>
</div>
<div class="clip_frame ose_pre_init mse_pre_init" id="u1047"><!-- image -->
    <img class="block" id="u1047_img" src="/sfimages/the-workplace-cant/south-west.png" alt="" width="336" height="176"/>
</div>
<div class="clearfix mse_pre_init" id="u1046"><!-- group -->
    <div class="clip_frame ose_pre_init grpelem" id="u631"><!-- image -->
        <img class="block" id="u631_img" src="/sfimages/the-workplace-cant/south-west-rule.png" alt="" width="302" height="8"/>
    </div>
    <div class="clearfix grpelem" id="pu623-4"><!-- column -->
        <img class="ose_pre_init colelem" id="u623-4" src="/sfimages/the-workplace-cant/u623-4.png" alt="South West" width="237" height="36"/><!-- rasterized frame -->
        <img class="ose_pre_init colelem" id="u624-4" src="/sfimages/the-workplace-cant/u624-4.png" alt="Female, named Emily/Sarah (tied scores), working in HR" width="218" height="57"/><!-- rasterized frame -->
    </div>
</div>
<div class="browser_width mse_pre_init" id="u643"><!-- simple frame --></div>
<img class="mse_pre_init ose_pre_init" id="u661-4" src="/sfimages/the-workplace-cant/u661-4.png" alt="How us Brits consider dealing " width="953" height="110"/><!-- rasterized frame -->
<img class="mse_pre_init ose_pre_init" id="u1131-4" src="/sfimages/the-workplace-cant/u1131-4.png" alt="with the workplace Can’t:" width="953" height="110"/><!-- rasterized frame -->
<div class="browser_width mse_pre_init" id="u1133"><!-- simple frame --></div>
<div class="clip_frame mse_pre_init" id="u650"><!-- image -->
    <img class="block" id="u650_img" src="/sfimages/the-workplace-cant/office-scene.png" alt="" width="336" height="394"/>
</div>
<div class="clip_frame mse_pre_init" id="u645"><!-- image -->
    <img class="block" id="u645_img" src="/sfimages/the-workplace-cant/office-cant-slide-7.png" alt="" width="199" height="371"/>
</div>
<div class="clearfix mse_pre_init" id="u1350"><!-- group -->
    <div class="clip_frame grpelem" id="u655"><!-- image -->
        <img class="block" id="u655_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u663-6" src="/sfimages/the-workplace-cant/u663-6.png" alt="40%
Scream and shout silently or in private, out of sheer frustration" width="394" height="144"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1129"><!-- group -->
    <div class="clip_frame grpelem" id="u678"><!-- image -->
        <img class="block" id="u678_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u681-10" src="/sfimages/the-workplace-cant/u681-10.png" alt="39%
Change the workplace  seating plan to be further  away from the Can’t " width="396" height="155"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1128"><!-- group -->
    <div class="clip_frame grpelem" id="u689"><!-- image -->
        <img class="block" id="u689_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u692-6" src="/sfimages/the-workplace-cant/u692-6.png" alt="36%
Seek new employment " width="400" height="143"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1127"><!-- group -->
    <div class="clip_frame grpelem" id="u701"><!-- image -->
        <img class="block" id="u701_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u700-10" src="/sfimages/the-workplace-cant/u700-10.png" alt="30%
Approach the Can’ts superior  in an attempt to cut him/her out  of work processes and protocol " width="396" height="155"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1126"><!-- group -->
    <div class="clip_frame grpelem" id="u711"><!-- image -->
        <img class="block" id="u711_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u714-8" src="/sfimages/the-workplace-cant/u714-8.png" alt="22%
Change profession or  career path entirely " width="396" height="143"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1125"><!-- group -->
    <div class="clip_frame grpelem" id="u734"><!-- image -->
        <img class="block" id="u734_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u733-8" src="/sfimages/the-workplace-cant/u733-8.png" alt="22%
Compete with that  person professionally " width="400" height="143"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1120"><!-- group -->
    <div class="clip_frame grpelem" id="u730"><!-- image -->
        <img class="block" id="u730_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u729-8" src="/sfimages/the-workplace-cant/u729-8.png" alt="22%
Consider working from  home/remote working " width="396" height="143"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1124"><!-- group -->
    <div class="clip_frame grpelem" id="u752"><!-- image -->
        <img class="block" id="u752_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u751-8" src="/sfimages/the-workplace-cant/u751-8.png" alt="20%
Call in sick in a bid to avoid  a meeting with the Can’t " width="395" height="143"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1123"><!-- group -->
    <div class="clip_frame grpelem" id="u756"><!-- image -->
        <img class="block" id="u756_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u755-8" src="/sfimages/the-workplace-cant/u755-8.png" alt="18%
Seek advice or buy tools/instructional material in a  bid to ‘bust stress’ " width="396" height="155"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1122"><!-- group -->
    <div class="clip_frame grpelem" id="u766"><!-- image -->
        <img class="block" id="u766_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u769-6" src="/sfimages/the-workplace-cant/u769-6.png" alt="15%
Consider counselling " width="396" height="119"/><!-- rasterized frame -->
</div>
<div class="clearfix mse_pre_init" id="u1121"><!-- group -->
    <div class="clip_frame grpelem" id="u778"><!-- image -->
        <img class="block" id="u778_img" src="/sfimages/the-workplace-cant/blue-bubble.png" alt="" width="401" height="218"/>
    </div>
    <img class="grpelem" id="u777-6" src="/sfimages/the-workplace-cant/u777-6.png" alt="12%
Relocate or move away " width="396" height="119"/><!-- rasterized frame -->
</div>
<div class="browser_width mse_pre_init" id="u1167"><!-- simple frame --></div>
<div class="clip_frame mse_pre_init" id="u1171"><!-- image -->
    <img class="block" id="u1171_img" src="/sfimages/the-workplace-cant/big-logo.png" alt="" width="357" height="107"/>
</div>
<a class="nonblock nontext mse_pre_init no-decoration" id="u1179-4" href="http://www.powwownow.co.uk/"><!-- rasterized frame --><img id="u1179-4_img" src="/sfimages/the-workplace-cant/u1179-4.png" alt="www.powwownow.co.uk" width="677" height="117"/></a>
<div class="verticalspacer"></div>
</div>
</div>
<!-- JS includes -->
<script type="text/javascript">
    if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
<script type="text/javascript">
    window.jQuery || document.write('\x3Cscript src="/sfjs/pages/the-workplace-cant/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
<script src="/sfjs/pages/the-workplace-cant/museutils.js?4291592202" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.tobrowserwidth.js?3842421675" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.scrolleffects.js?4006931061" type="text/javascript"></script>
<script src="/sfjs/pages/the-workplace-cant/jquery.watch.js?4068933136" type="text/javascript"></script>
<!-- Other scripts -->
<script type="text/javascript">
    $(document).ready(function() { try {
        Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
        $('.browser_width').toBrowserWidth();/* browser width elements */
        Muse.Utils.prepHyperlinks(true);/* body */
        $('#u106-4').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[1,0]},{"in":[0,Infinity],"speed":[-1,0]}]);/* scroll effect */
        $('#u108').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[0,0]},{"in":[0,Infinity],"speed":[1,0]}]);/* scroll effect */
        $('#u101').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[0,0]},{"in":[0,Infinity],"speed":[-1,0]}]);/* scroll effect */
        $('#u116-4').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[0,0]},{"in":[0,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u116-4').registerOpacityScrollEffect([{"in":[-Infinity,0],"opacity":0,"fade":353},{"in":[0,0],"opacity":100},{"in":[0,Infinity],"opacity":0,"fade":130}]);/* scroll effect */
        $('#u99').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[null,0]},{"in":[0,Infinity],"speed":[null,-0.5]}]);/* scroll effect */
        $('#u1451').registerPositionScrollEffect([{"in":[-Infinity,0],"speed":[0,0]},{"in":[0,Infinity],"speed":[0,-1]}]);/* scroll effect */
        $('#u142').registerPositionScrollEffect([{"in":[-Infinity,1905],"speed":[null,0.5]},{"in":[1905,Infinity],"speed":[null,0]}]);/* scroll effect */
        $('#u135-6').registerPositionScrollEffect([{"in":[-Infinity,2327.7],"speed":[0,0]},{"in":[2327.7,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u135-6').registerOpacityScrollEffect([{"in":[-Infinity,2327.7],"opacity":0,"fade":95.4},{"in":[2327.7,2327.7],"opacity":100},{"in":[2327.7,Infinity],"opacity":100,"fade":95.6}]);/* scroll effect */
        $('#u136').registerPositionScrollEffect([{"in":[-Infinity,2867],"speed":[0,1]},{"in":[2867,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1378-4').registerOpacityScrollEffect([{"in":[-Infinity,2866],"opacity":0,"fade":266},{"in":[2866,2866],"opacity":100},{"in":[2866,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1379').registerOpacityScrollEffect([{"in":[-Infinity,2866],"opacity":0,"fade":266},{"in":[2866,2866],"opacity":100},{"in":[2866,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1382').registerPositionScrollEffect([{"in":[-Infinity,2866.4],"speed":[0,0]},{"in":[2866.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1485').registerPositionScrollEffect([{"in":[-Infinity,3720.4],"speed":[-1,0]},{"in":[3720.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u832').registerPositionScrollEffect([{"in":[-Infinity,4370.65],"speed":[2,0]},{"in":[4370.65,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u828').registerPositionScrollEffect([{"in":[-Infinity,4878.65],"speed":[-2,0]},{"in":[4878.65,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u830').registerPositionScrollEffect([{"in":[-Infinity,5519.15],"speed":[2,0]},{"in":[5519.15,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u829').registerPositionScrollEffect([{"in":[-Infinity,6661.4],"speed":[-2,0]},{"in":[6661.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u831').registerPositionScrollEffect([{"in":[-Infinity,7322.9],"speed":[2,0]},{"in":[7322.9,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u189-5').registerPositionScrollEffect([{"in":[-Infinity,11005.2],"speed":[2,0]},{"in":[11005.2,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u853-4').registerPositionScrollEffect([{"in":[-Infinity,11005.9],"speed":[-2,0]},{"in":[11005.9,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1366').registerPositionScrollEffect([{"in":[-Infinity,12566.55],"speed":[0,0.5]},{"in":[12566.55,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u879').registerPositionScrollEffect([{"in":[-Infinity,13690.1],"speed":[2,0]},{"in":[13690.1,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u877').registerPositionScrollEffect([{"in":[-Infinity,14940.95],"speed":[-2,0]},{"in":[14940.95,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u874').registerPositionScrollEffect([{"in":[-Infinity,16042.45],"speed":[2,0]},{"in":[16042.45,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u876').registerPositionScrollEffect([{"in":[-Infinity,17260.25],"speed":[-2,0]},{"in":[17260.25,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u875').registerPositionScrollEffect([{"in":[-Infinity,18393.55],"speed":[2,0]},{"in":[18393.55,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u272').registerPositionScrollEffect([{"in":[-Infinity,23801.95],"speed":[null,0.2]},{"in":[23801.95,Infinity],"speed":[null,0]}]);/* scroll effect */
        $('#u274-5').registerPositionScrollEffect([{"in":[-Infinity,24237.15],"speed":[0,0.1]},{"in":[24237.15,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u274-5').registerOpacityScrollEffect([{"in":[-Infinity,24237.15],"opacity":0,"fade":789.45},{"in":[24237.15,24237.15],"opacity":100},{"in":[24237.15,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u275').registerOpacityScrollEffect([{"in":[-Infinity,24371.85],"opacity":0,"fade":189},{"in":[24371.85,24371.85],"opacity":100},{"in":[24371.85,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u308-4').registerOpacityScrollEffect([{"in":[-Infinity,24371.85],"opacity":0,"fade":189},{"in":[24371.85,24371.85],"opacity":100},{"in":[24371.85,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1241').registerPositionScrollEffect([{"in":[-Infinity,24542.3],"speed":[0,0.8]},{"in":[24542.3,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u280').registerOpacityScrollEffect([{"in":[-Infinity,24871.67],"opacity":0,"fade":188.82},{"in":[24871.67,24871.67],"opacity":100},{"in":[24871.67,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u309-4').registerOpacityScrollEffect([{"in":[-Infinity,24871.68],"opacity":0,"fade":188.82},{"in":[24871.68,24871.68],"opacity":100},{"in":[24871.68,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1245').registerPositionScrollEffect([{"in":[-Infinity,25041.95],"speed":[0,0.8]},{"in":[25041.95,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u285').registerOpacityScrollEffect([{"in":[-Infinity,25353.5],"opacity":0,"fade":188.65},{"in":[25353.5,25353.5],"opacity":100},{"in":[25353.5,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u310-4').registerOpacityScrollEffect([{"in":[-Infinity,25353.5],"opacity":0,"fade":188.65},{"in":[25353.5,25353.5],"opacity":100},{"in":[25353.5,Infinity],"opacity":100,"fade":0}]);/* scroll effect */
        $('#u1246').registerPositionScrollEffect([{"in":[-Infinity,25523.6],"speed":[0,0.8]},{"in":[25523.6,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1335').registerPositionScrollEffect([{"in":[-Infinity,26400.55],"speed":[null,1]},{"in":[26400.55,Infinity],"speed":[null,0]}]);/* scroll effect */
        $('#u1335').registerOpacityScrollEffect([{"in":[-Infinity,26400.55],"opacity":0,"fade":76},{"in":[26400.55,26400.55],"opacity":0},{"in":[26400.55,Infinity],"opacity":100,"fade":688.95}]);/* scroll effect */
        $('#u295').registerOpacityScrollEffect([{"in":[-Infinity,27907.83],"opacity":0,"fade":138.98},{"in":[27907.83,27907.83],"opacity":100},{"in":[27907.83,Infinity],"opacity":100,"fade":50.87}]);/* scroll effect */
        $('#u312-6').registerOpacityScrollEffect([{"in":[-Infinity,27907.83],"opacity":0,"fade":138.98},{"in":[27907.83,27907.83],"opacity":100},{"in":[27907.83,Infinity],"opacity":100,"fade":50.87}]);/* scroll effect */
        $('#u1346').registerPositionScrollEffect([{"in":[-Infinity,28128.1],"speed":[0,0.8]},{"in":[28128.1,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u290').registerOpacityScrollEffect([{"in":[-Infinity,28424.5],"opacity":0,"fade":479.93},{"in":[28424.5,28424.5],"opacity":100},{"in":[28424.5,Infinity],"opacity":100,"fade":170}]);/* scroll effect */
        $('#u311-4').registerOpacityScrollEffect([{"in":[-Infinity,28424.5],"opacity":0,"fade":479.93},{"in":[28424.5,28424.5],"opacity":100},{"in":[28424.5,Infinity],"opacity":100,"fade":170}]);/* scroll effect */
        $('#u1347').registerPositionScrollEffect([{"in":[-Infinity,28763.5],"speed":[0,0.8]},{"in":[28763.5,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u300').registerOpacityScrollEffect([{"in":[-Infinity,29189.93],"opacity":0,"fade":599.93},{"in":[29189.93,29189.93],"opacity":100},{"in":[29189.93,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u313-4').registerOpacityScrollEffect([{"in":[-Infinity,29189.93],"opacity":0,"fade":599.93},{"in":[29189.93,29189.93],"opacity":100},{"in":[29189.93,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u1348').registerPositionScrollEffect([{"in":[-Infinity,29409.9],"speed":[0,0.8]},{"in":[29409.9,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u323-4').registerPositionScrollEffect([{"in":[-Infinity,32809.9],"speed":[0,0]},{"in":[32809.9,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u323-4').registerOpacityScrollEffect([{"in":[-Infinity,32809.9],"opacity":0,"fade":301.05},{"in":[32809.9,32809.9],"opacity":100},{"in":[32809.9,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u895-4').registerPositionScrollEffect([{"in":[-Infinity,32854.25],"speed":[0,0]},{"in":[32854.25,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u895-4').registerOpacityScrollEffect([{"in":[-Infinity,32854.25],"opacity":0,"fade":115.4},{"in":[32854.25,32854.25],"opacity":100},{"in":[32854.25,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u340').registerPositionScrollEffect([{"in":[-Infinity,34118.4],"speed":[0,1]},{"in":[34118.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u364').registerPositionScrollEffect([{"in":[-Infinity,34509.4],"speed":[0,0]},{"in":[34509.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u364').registerOpacityScrollEffect([{"in":[-Infinity,34509.4],"opacity":0,"fade":176.85},{"in":[34509.4,34509.4],"opacity":100},{"in":[34509.4,Infinity],"opacity":100,"fade":215.15}]);/* scroll effect */
        $('#u374-4').registerPositionScrollEffect([{"in":[-Infinity,34852.4],"speed":[0,2]},{"in":[34852.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u345').registerPositionScrollEffect([{"in":[-Infinity,35606.4],"speed":[-1,0]},{"in":[35606.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u367').registerPositionScrollEffect([{"in":[-Infinity,35997.4],"speed":[0,0]},{"in":[35997.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u367').registerOpacityScrollEffect([{"in":[-Infinity,35997.4],"opacity":0,"fade":176.85},{"in":[35997.4,35997.4],"opacity":100},{"in":[35997.4,Infinity],"opacity":100,"fade":215.15}]);/* scroll effect */
        $('#u375-4').registerPositionScrollEffect([{"in":[-Infinity,36340.4],"speed":[-2,0]},{"in":[36340.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u334').registerPositionScrollEffect([{"in":[-Infinity,37094.4],"speed":[1,0]},{"in":[37094.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u361').registerPositionScrollEffect([{"in":[-Infinity,37485.4],"speed":[0,0]},{"in":[37485.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u361').registerOpacityScrollEffect([{"in":[-Infinity,37485.4],"opacity":0,"fade":176.85},{"in":[37485.4,37485.4],"opacity":100},{"in":[37485.4,Infinity],"opacity":100,"fade":215.15}]);/* scroll effect */
        $('#u373-4').registerPositionScrollEffect([{"in":[-Infinity,37828.4],"speed":[2,0]},{"in":[37828.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u350').registerPositionScrollEffect([{"in":[-Infinity,38582.4],"speed":[-1,0]},{"in":[38582.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u370').registerPositionScrollEffect([{"in":[-Infinity,38980.4],"speed":[0,0]},{"in":[38980.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u370').registerOpacityScrollEffect([{"in":[-Infinity,38980.4],"opacity":0,"fade":176.85},{"in":[38980.4,38980.4],"opacity":100},{"in":[38980.4,Infinity],"opacity":100,"fade":215.25}]);/* scroll effect */
        $('#u376-4').registerPositionScrollEffect([{"in":[-Infinity,39316.4],"speed":[-2,0]},{"in":[39316.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u329').registerPositionScrollEffect([{"in":[-Infinity,40070.4],"speed":[1,0]},{"in":[40070.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u356').registerPositionScrollEffect([{"in":[-Infinity,40461.4],"speed":[0,0]},{"in":[40461.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u356').registerOpacityScrollEffect([{"in":[-Infinity,40461.4],"opacity":0,"fade":176.85},{"in":[40461.4,40461.4],"opacity":100},{"in":[40461.4,Infinity],"opacity":100,"fade":215.15}]);/* scroll effect */
        $('#u355-4').registerPositionScrollEffect([{"in":[-Infinity,40804.4],"speed":[2,0]},{"in":[40804.4,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u384').registerPositionScrollEffect([{"in":[-Infinity,46764.95],"speed":[null,0.2]},{"in":[46764.95,Infinity],"speed":[null,0]}]);/* scroll effect */
        $('#u385-5').registerPositionScrollEffect([{"in":[-Infinity,50098.85],"speed":[0.8,0]},{"in":[50098.85,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1116-6').registerPositionScrollEffect([{"in":[-Infinity,50098.85],"speed":[-0.8,0]},{"in":[50098.85,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1311').registerPositionScrollEffect([{"in":[-Infinity,53792],"speed":[0,0.05]},{"in":[53792,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u952').registerPositionScrollEffect([{"in":[-Infinity,53817.75],"speed":[0,0.05]},{"in":[53817.75,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u952').registerOpacityScrollEffect([{"in":[-Infinity,53817.75],"opacity":0,"fade":0},{"in":[53817.75,53817.75],"opacity":100},{"in":[53817.75,Infinity],"opacity":0,"fade":1999.45}]);/* scroll effect */
        $('#u1052').registerOpacityScrollEffect([{"in":[-Infinity,53830.75],"opacity":0,"fade":13},{"in":[53830.75,53830.75],"opacity":100},{"in":[53830.75,Infinity],"opacity":0,"fade":1986.45}]);/* scroll effect */
        $('#u520-4').registerOpacityScrollEffect([{"in":[-Infinity,53830.75],"opacity":0,"fade":13},{"in":[53830.75,53830.75],"opacity":100},{"in":[53830.75,Infinity],"opacity":0,"fade":1986.45}]);/* scroll effect */
        $('#u521-4').registerOpacityScrollEffect([{"in":[-Infinity,53830.75],"opacity":0,"fade":13},{"in":[53830.75,53830.75],"opacity":100},{"in":[53830.75,Infinity],"opacity":0,"fade":1986.45}]);/* scroll effect */
        $('#u1058').registerPositionScrollEffect([{"in":[-Infinity,53817.55],"speed":[0,0.05]},{"in":[53817.55,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u537').registerOpacityScrollEffect([{"in":[-Infinity,55888.2],"opacity":0,"fade":0},{"in":[55888.2,55888.2],"opacity":100},{"in":[55888.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u533-4').registerOpacityScrollEffect([{"in":[-Infinity,55888.2],"opacity":0,"fade":0},{"in":[55888.2,55888.2],"opacity":100},{"in":[55888.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u532-4').registerOpacityScrollEffect([{"in":[-Infinity,55888.2],"opacity":0,"fade":0},{"in":[55888.2,55888.2],"opacity":100},{"in":[55888.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u995').registerPositionScrollEffect([{"in":[-Infinity,55888.55],"speed":[0,0.05]},{"in":[55888.55,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u957').registerPositionScrollEffect([{"in":[-Infinity,55888.2],"speed":[0,0.05]},{"in":[55888.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u957').registerOpacityScrollEffect([{"in":[-Infinity,55888.2],"opacity":0,"fade":0},{"in":[55888.2,55888.2],"opacity":100},{"in":[55888.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u547').registerOpacityScrollEffect([{"in":[-Infinity,58006.17],"opacity":0,"fade":121.97},{"in":[58006.17,58006.17],"opacity":100},{"in":[58006.17,Infinity],"opacity":0,"fade":1878.03}]);/* scroll effect */
        $('#u543-4').registerOpacityScrollEffect([{"in":[-Infinity,58006.17],"opacity":0,"fade":121.97},{"in":[58006.17,58006.17],"opacity":100},{"in":[58006.17,Infinity],"opacity":0,"fade":1878.03}]);/* scroll effect */
        $('#u542-6').registerOpacityScrollEffect([{"in":[-Infinity,58006.17],"opacity":0,"fade":121.97},{"in":[58006.17,58006.17],"opacity":100},{"in":[58006.17,Infinity],"opacity":0,"fade":1878.03}]);/* scroll effect */
        $('#u998').registerPositionScrollEffect([{"in":[-Infinity,57883.8],"speed":[0,0.05]},{"in":[57883.8,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u974').registerPositionScrollEffect([{"in":[-Infinity,57884.2],"speed":[0,0.05]},{"in":[57884.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u974').registerOpacityScrollEffect([{"in":[-Infinity,57884.2],"opacity":0,"fade":0},{"in":[57884.2,57884.2],"opacity":100},{"in":[57884.2,Infinity],"opacity":0,"fade":2000.2}]);/* scroll effect */
        $('#u1083').registerOpacityScrollEffect([{"in":[-Infinity,60004.43],"opacity":0,"fade":110.23},{"in":[60004.43,60004.43],"opacity":100},{"in":[60004.43,Infinity],"opacity":0,"fade":1889.77}]);/* scroll effect */
        $('#u1076-4').registerOpacityScrollEffect([{"in":[-Infinity,59894.2],"opacity":0,"fade":0},{"in":[59894.2,59894.2],"opacity":100},{"in":[59894.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u1075-6').registerOpacityScrollEffect([{"in":[-Infinity,59894.2],"opacity":0,"fade":0},{"in":[59894.2,59894.2],"opacity":100},{"in":[59894.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u1090').registerPositionScrollEffect([{"in":[-Infinity,59894.2],"speed":[0,0.05]},{"in":[59894.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1078').registerPositionScrollEffect([{"in":[-Infinity,59894.2],"speed":[0,0.05]},{"in":[59894.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1078').registerOpacityScrollEffect([{"in":[-Infinity,59894.2],"opacity":0,"fade":0},{"in":[59894.2,59894.2],"opacity":100},{"in":[59894.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u557').registerOpacityScrollEffect([{"in":[-Infinity,61907.2],"opacity":0,"fade":0},{"in":[61907.2,61907.2],"opacity":100},{"in":[61907.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u553-6').registerOpacityScrollEffect([{"in":[-Infinity,61907.2],"opacity":0,"fade":0},{"in":[61907.2,61907.2],"opacity":100},{"in":[61907.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u552-6').registerOpacityScrollEffect([{"in":[-Infinity,61907.2],"opacity":0,"fade":0},{"in":[61907.2,61907.2],"opacity":100},{"in":[61907.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u1402').registerPositionScrollEffect([{"in":[-Infinity,61907.2],"speed":[0,0.05]},{"in":[61907.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u999').registerPositionScrollEffect([{"in":[-Infinity,61907],"speed":[0,0.05]},{"in":[61907,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u999').registerOpacityScrollEffect([{"in":[-Infinity,61907],"opacity":0,"fade":0},{"in":[61907,61907],"opacity":100},{"in":[61907,Infinity],"opacity":0,"fade":2000.2}]);/* scroll effect */
        $('#u562').registerOpacityScrollEffect([{"in":[-Infinity,63947.2],"opacity":0,"fade":0},{"in":[63947.2,63947.2],"opacity":100},{"in":[63947.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u560-4').registerOpacityScrollEffect([{"in":[-Infinity,63947.2],"opacity":0,"fade":0},{"in":[63947.2,63947.2],"opacity":100},{"in":[63947.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u561-6').registerOpacityScrollEffect([{"in":[-Infinity,63947.2],"opacity":0,"fade":0},{"in":[63947.2,63947.2],"opacity":100},{"in":[63947.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u1015').registerPositionScrollEffect([{"in":[-Infinity,63947.2],"speed":[0,0.05]},{"in":[63947.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1005').registerPositionScrollEffect([{"in":[-Infinity,63947.2],"speed":[0,0.05]},{"in":[63947.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1005').registerOpacityScrollEffect([{"in":[-Infinity,63947.2],"opacity":0,"fade":0},{"in":[63947.2,63947.2],"opacity":100},{"in":[63947.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u570').registerOpacityScrollEffect([{"in":[-Infinity,66001.2],"opacity":0,"fade":0},{"in":[66001.2,66001.2],"opacity":100},{"in":[66001.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u566-4').registerOpacityScrollEffect([{"in":[-Infinity,66001.2],"opacity":0,"fade":0},{"in":[66001.2,66001.2],"opacity":100},{"in":[66001.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u565-6').registerOpacityScrollEffect([{"in":[-Infinity,66001.2],"opacity":0,"fade":0},{"in":[66001.2,66001.2],"opacity":100},{"in":[66001.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u1016').registerPositionScrollEffect([{"in":[-Infinity,66001.2],"speed":[0,0.05]},{"in":[66001.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1017').registerPositionScrollEffect([{"in":[-Infinity,66001.2],"speed":[0,0.05]},{"in":[66001.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1017').registerOpacityScrollEffect([{"in":[-Infinity,66001.2],"opacity":0,"fade":0},{"in":[66001.2,66001.2],"opacity":100},{"in":[66001.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u580').registerOpacityScrollEffect([{"in":[-Infinity,68020.2],"opacity":0,"fade":0},{"in":[68020.2,68020.2],"opacity":100},{"in":[68020.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u575-4').registerOpacityScrollEffect([{"in":[-Infinity,68020.2],"opacity":0,"fade":0},{"in":[68020.2,68020.2],"opacity":100},{"in":[68020.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u576-4').registerOpacityScrollEffect([{"in":[-Infinity,68020.2],"opacity":0,"fade":0},{"in":[68020.2,68020.2],"opacity":100},{"in":[68020.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u1022').registerPositionScrollEffect([{"in":[-Infinity,68019.85],"speed":[0,0.05]},{"in":[68019.85,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1023').registerPositionScrollEffect([{"in":[-Infinity,68020.2],"speed":[0,0.05]},{"in":[68020.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1023').registerOpacityScrollEffect([{"in":[-Infinity,68020.2],"opacity":0,"fade":0},{"in":[68020.2,68020.2],"opacity":100},{"in":[68020.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u1028').registerPositionScrollEffect([{"in":[-Infinity,70093.2],"speed":[0,0.05]},{"in":[70093.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1028').registerOpacityScrollEffect([{"in":[-Infinity,70093.2],"opacity":0,"fade":0},{"in":[70093.2,70093.2],"opacity":100},{"in":[70093.2,Infinity],"opacity":0,"fade":2000}]);/* scroll effect */
        $('#u587').registerOpacityScrollEffect([{"in":[-Infinity,70173.2],"opacity":0,"fade":80},{"in":[70173.2,70173.2],"opacity":100},{"in":[70173.2,Infinity],"opacity":0,"fade":1920}]);/* scroll effect */
        $('#u585-4').registerOpacityScrollEffect([{"in":[-Infinity,70173.2],"opacity":0,"fade":80},{"in":[70173.2,70173.2],"opacity":100},{"in":[70173.2,Infinity],"opacity":0,"fade":1920}]);/* scroll effect */
        $('#u586-6').registerOpacityScrollEffect([{"in":[-Infinity,70173.2],"opacity":0,"fade":80},{"in":[70173.2,70173.2],"opacity":100},{"in":[70173.2,Infinity],"opacity":0,"fade":1920}]);/* scroll effect */
        $('#u1033').registerPositionScrollEffect([{"in":[-Infinity,70093.2],"speed":[0,0.05]},{"in":[70093.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u610').registerOpacityScrollEffect([{"in":[-Infinity,72116.2],"opacity":0,"fade":0},{"in":[72116.2,72116.2],"opacity":100},{"in":[72116.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u590-4').registerOpacityScrollEffect([{"in":[-Infinity,72116.2],"opacity":0,"fade":0},{"in":[72116.2,72116.2],"opacity":100},{"in":[72116.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u591-4').registerOpacityScrollEffect([{"in":[-Infinity,72116.2],"opacity":0,"fade":0},{"in":[72116.2,72116.2],"opacity":100},{"in":[72116.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u1034').registerPositionScrollEffect([{"in":[-Infinity,72116.45],"speed":[0,0.05]},{"in":[72116.45,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1185').registerPositionScrollEffect([{"in":[-Infinity,72116.2],"speed":[0,0.05]},{"in":[72116.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1185').registerOpacityScrollEffect([{"in":[-Infinity,72116.2],"opacity":0,"fade":0},{"in":[72116.2,72116.2],"opacity":100},{"in":[72116.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u1041').registerPositionScrollEffect([{"in":[-Infinity,73119.2],"speed":[0,0.05]},{"in":[73119.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1041').registerOpacityScrollEffect([{"in":[-Infinity,73119.2],"opacity":0,"fade":0},{"in":[73119.2,73119.2],"opacity":100},{"in":[73119.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u620').registerOpacityScrollEffect([{"in":[-Infinity,73159.2],"opacity":0,"fade":0},{"in":[73159.2,73159.2],"opacity":100},{"in":[73159.2,Infinity],"opacity":0,"fade":960}]);/* scroll effect */
        $('#u615-4').registerOpacityScrollEffect([{"in":[-Infinity,73159.2],"opacity":0,"fade":0},{"in":[73159.2,73159.2],"opacity":100},{"in":[73159.2,Infinity],"opacity":0,"fade":960}]);/* scroll effect */
        $('#u616-4').registerOpacityScrollEffect([{"in":[-Infinity,73159.2],"opacity":0,"fade":0},{"in":[73159.2,73159.2],"opacity":100},{"in":[73159.2,Infinity],"opacity":0,"fade":960}]);/* scroll effect */
        $('#u1040').registerPositionScrollEffect([{"in":[-Infinity,73119.15],"speed":[0,0.05]},{"in":[73119.15,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1047').registerPositionScrollEffect([{"in":[-Infinity,74125.2],"speed":[0,0.05]},{"in":[74125.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u1047').registerOpacityScrollEffect([{"in":[-Infinity,74125.2],"opacity":0,"fade":0},{"in":[74125.2,74125.2],"opacity":100},{"in":[74125.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u631').registerOpacityScrollEffect([{"in":[-Infinity,74125.2],"opacity":0,"fade":0},{"in":[74125.2,74125.2],"opacity":100},{"in":[74125.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u623-4').registerOpacityScrollEffect([{"in":[-Infinity,74125.2],"opacity":0,"fade":0},{"in":[74125.2,74125.2],"opacity":100},{"in":[74125.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u624-4').registerOpacityScrollEffect([{"in":[-Infinity,74125.2],"opacity":0,"fade":0},{"in":[74125.2,74125.2],"opacity":100},{"in":[74125.2,Infinity],"opacity":0,"fade":1000}]);/* scroll effect */
        $('#u1046').registerPositionScrollEffect([{"in":[-Infinity,74125.2],"speed":[0,0.05]},{"in":[74125.2,Infinity],"speed":[0,0.05]}]);/* scroll effect */
        $('#u643').registerPositionScrollEffect([{"in":[-Infinity,76250.1],"speed":[null,1]},{"in":[76250.1,Infinity],"speed":[null,0]}]);/* scroll effect */
        $('#u661-4').registerPositionScrollEffect([{"in":[-Infinity,76919.5],"speed":[0,0]},{"in":[76919.5,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u661-4').registerOpacityScrollEffect([{"in":[-Infinity,76919.5],"opacity":0,"fade":310.8},{"in":[76919.5,76919.5],"opacity":100},{"in":[76919.5,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u1131-4').registerPositionScrollEffect([{"in":[-Infinity,77149.85],"speed":[0,0.5]},{"in":[77149.85,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1131-4').registerOpacityScrollEffect([{"in":[-Infinity,77149.85],"opacity":0,"fade":0},{"in":[77149.85,77149.85],"opacity":100},{"in":[77149.85,Infinity],"opacity":100,"fade":50}]);/* scroll effect */
        $('#u1133').registerPositionScrollEffect([{"in":[-Infinity,77723.5],"speed":[null,0.7]},{"in":[77723.5,Infinity],"speed":[null,0]}]);/* scroll effect */
        $('#u650').registerPositionScrollEffect([{"in":[-Infinity,78072.05],"speed":[0,0.5]},{"in":[78072.05,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u645').registerPositionScrollEffect([{"in":[-Infinity,78072.05],"speed":[0,0.5]},{"in":[78072.05,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1350').registerPositionScrollEffect([{"in":[-Infinity,79440.94],"speed":[0,0.5]},{"in":[79440.94,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1129').registerPositionScrollEffect([{"in":[-Infinity,80577.05],"speed":[0,0.5]},{"in":[80577.05,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1128').registerPositionScrollEffect([{"in":[-Infinity,81713.15],"speed":[0,0.5]},{"in":[81713.15,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1127').registerPositionScrollEffect([{"in":[-Infinity,82849.26],"speed":[0,0.5]},{"in":[82849.26,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1126').registerPositionScrollEffect([{"in":[-Infinity,83985.36],"speed":[0,0.5]},{"in":[83985.36,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1125').registerPositionScrollEffect([{"in":[-Infinity,85121.47],"speed":[0,0.5]},{"in":[85121.47,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1120').registerPositionScrollEffect([{"in":[-Infinity,86257.58],"speed":[0,0.5]},{"in":[86257.58,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1124').registerPositionScrollEffect([{"in":[-Infinity,87393.68],"speed":[0,0.5]},{"in":[87393.68,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1123').registerPositionScrollEffect([{"in":[-Infinity,88529.79],"speed":[0,0.5]},{"in":[88529.79,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1122').registerPositionScrollEffect([{"in":[-Infinity,89665.89],"speed":[0,0.5]},{"in":[89665.89,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1121').registerPositionScrollEffect([{"in":[-Infinity,90803.85],"speed":[0,0.5]},{"in":[90803.85,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1167').registerPositionScrollEffect([{"in":[-Infinity,93138.3],"speed":[null,1]},{"in":[93138.3,Infinity],"speed":[null,0]}]);/* scroll effect */
        $('#u1171').registerPositionScrollEffect([{"in":[-Infinity,93640.6],"speed":[-1,0]},{"in":[93640.6,Infinity],"speed":[0,0]}]);/* scroll effect */
        $('#u1179-4').registerPositionScrollEffect([{"in":[-Infinity,93877.2],"speed":[1,0]},{"in":[93877.2,Infinity],"speed":[0,0]}]);/* scroll effect */
        Muse.Utils.fullPage('#page');/* 100% height page */
        Muse.Utils.showWidgetsWhenReady();/* body */
        Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
    } catch(e) { Muse.Assert.fail('Error calling selector function:' + e); }});
</script>
