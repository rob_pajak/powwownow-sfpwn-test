<div class="terms-conditions-container-item item-5">
    <h2>Powwownow Engage service additional terms and conditions</h2>
    <p>Terms and Conditions apply in respect of our Powwownow Engage Service, and apply in addition to our General Terms and Conditions. Please read these Powwownow Engage Service Additional Terms and Conditions together with our General Terms and Conditions carefully before Registering for and/or using our Powwownow Engage Service.</p>
    <ol>
        <li>Definitions
            <ul>
                <li>1.1 Unless the context otherwise requires and except for those words and expressions defined in these Additional Terms and Conditions, words and expressions defined in the General Terms and Conditions shall have the same meanings where used in these Additional Terms and Conditions.</li>
                <li>1.2 References to Paragraph numbers in these Powwownow Engage Service Additional Terms and Conditions refer, unless expressly stated otherwise, to Paragraphs contained in these Powwownow Engage Service Additional Terms and Conditions.</li>
                <li>1.3 In these Powwownow Engage Service Additional Terms and Conditions, unless the context otherwise requires:
                    <ul>
                        <li>1.3.1 "Hardware" means a bundle containing a webcam, a headset and a speaker box;</li>
                        <li>1.3.2 "Network Operator Call Charges" means the price charged to the caller by the network operator;</li>
                        <li>1.3.3 "Subscription Form" means the business subscription service order form which we will prepare and send to you following your telephone enquiry regarding the Powwownow Engage Services; and</li>
                        <li>1.3.4 "Users" means the number of your employees or representatives who are registered to use the Powwownow Engage Services.</li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>Scope of the Powwownow Engage Service
            <ul>
                <li>2.1 Our Powwownow Engage service is a standalone Unified Collaboration Service which provides presence, instant messaging, HD video conferencing, audio conferencing, VOIP calling, screen sharing and web conferencing functionality.</li>
                <li>2.2 The number of Users you register to use the Powwownow Engage Service will be specified in the Subscription Form.</li>
            </ul>
        </li>
        <li>Registration, subscribing to Powwownow Engage, and when our contract with you begins
            <ul>
                <li>3.1 If you wish to use our Powwownow Engage Service, you must Register with us by phone on 0800 022 9930 or +44 (0)20 3398 0900. Calls will be charged at your telephone provider's standard rate for each of these numbers.</li>
                <li>3.2 When you register, you must provide a current valid email address and such additional information as we may require in order to provide a quote to you, and to set up your customer account (for example, how you would like to pay, your telephone number, email addresses of the Users you wish to register etc). We will use email addresses to communicate Service messages to you and your Users and for marketing purposes (in accordance with the terms of our Privacy Policy and our General Terms and Conditions).</li>
                <li>3.3 Following the call, we will send you an email which attaches our Subscription Form which will set out our proposal of the key terms of the agreement between you and us for the Services. If you are happy with the Subscription Form, please arrange for an authorised signatory to sign the same to confirm that you have read and agree the terms of our proposal, the General Terms and Conditions, the Powwownow Engage Additional Terms and Conditions and our Privacy Policy. Unless we expressly agree otherwise, our proposal is valid for a period of 30 days.</li>
                <li>3.4 By signing and returning the Subscription Form, you warrant and represent to us that: (i) the form has been signed by an authorised signatory who is authorised by you to make the request to us to provide the Services and to place an order for the Services for and on your behalf; (ii) the information provided to us as part of the Registration process is accurate, complete and up-to-date; and (iii) you are entitled to purchase the Services using the payment details that you have provided.</li>
                <li>3.5 Your purchase of the Powwownow Engage Service is offered by us on an "invitation to treat" basis only. By signing and accepting the Subscription Form, you are making an offer to us to purchase the Services. Your offer will only be accepted by us and a contract formed when we have confirmed that we have accepted your request in accordance with Paragraph 3.6</li>
                <li>3.6 We accept your offer to purchase the Powwownow Engage Service at the time we send confirmation to you by email that we accept the request and your order has been successful and it is at this moment that a contract is made between you and us in respect of the provision of the Powwownow Engage Service. Our confirmation email will attach a PDF copy of the applicable Terms and a summary of the Powwownow Engage Service that we will supply to you, together with a summary of the agreed pricing, billing and contact details. We may decline your order request for the Powwownow Engage Service at our absolute discretion, and we are not obliged to provide the Powwownow Engage Service to you until we have confirmed our acceptance to you.</li>
                <li>3.7 We will provide the Powwownow Engage Services to you from the date of our confirmatory email, although you will not be able to use the Services until installation is complete.</li>
            </ul>
        </li>
        <li>Installation and third party software
            <ul>
                <li>4.1 Once we have sent to you the acceptance email in accordance with Paragraph 3.6, we will set up the product for you and assist you with the installation in your business.</li>
                <li>4.2 The Powwownow Engage Services have been developed by us in conjunction with affiliated third party suppliers. These affiliated third party suppliers have given us the right to distribute this software to you. During the installation process, you will receive an email from us with a link to some software which you will need to download to use the Services. When you download, install and use the software, your use of the software and the Services will be governed by the terms and conditions of our third party supplier’s end user software licence agreement and their Terms of Use (a copy of which is available from their website at</li>
                <li>4.3 A user guide is available on our website that explains how to use the Services once the software has been installed.</li>
                <li>4.4 Before you download, install or in any way use the Services, please ensure that you have read and agree to both these Additional Terms and Conditions, the General Terms and Conditions, our Website Terms of Use, our Privacy Policy and the terms and conditions set out in the end user software licence agreement. When you browse our third party supplier’s website, you will be subject to the terms and conditions of their website (available at www.yuuguu.com/terms_of_use and http://www.vidyo.com/terms-of-use/). We are not responsible for any products, services or materials found on our third party supplier’s site, your use of their site is entirely at your own risk, and we accept no responsibility for the content of their site.</li>
                <li>4.5 In addition to the terms of the third party end user software licence, you agree that you shall not (and you shall not permit anyone else, including any Participant, to) copy, translate, merge, modify, create a derivative work of, reverse engineer, decompile or otherwise attempt to extract the source code of the software or any part thereof, unless this is expressly permitted or is otherwise required or permitted by law (and then, only to the extent so permitted or required), unless you have obtained our express prior written consent.</li>
                <li>4.6 You agree to comply promptly with any reasonable instructions given by us from time to time in connection with the use and operation of the Services and/or the software required to operate the Services.</li>
                <li>4.7 Please be aware that from time to time, the Powwownow Engage software may automatically download and install, or request or require you to download updates from us or our third party supplier. These updates are designed to improve, enhance and further develop the Services and may take the form of bug fixes, enhanced functions, new software modules and completely new versions of the software. By accepting the Terms you agree to receive such updates and permit us to deliver these to you as part of your use of the Services.</li>
                <li>4.8 We reserve the right to discontinue the provision of the Powwownow Engage service at any time.</li>
                <li>4.9 You acknowledge and agree that our affiliated third party suppliers may contact you by email in the event that we are to discontinue the provision of the Powwownow Engage Services (so that they may offer to provide you with an alternative video or web conferencing service or to see how they may improve the performance of the product).</li>
                <li>4.10 You agree and undertake to stop using the Powwownow Engage Services (and to uninstall the software) if at any time your agreement with us terminates (for whatever reason).</li>
            </ul>
        </li>
        <li>Payment
            <ul>
                <li>5.1 Our charges are based on the number of Users. The costs per month are set out in the Subscription Form.</li>
                <li>5.2 Our licence fee charges for the Services are payable monthly in advance. We will send you an invoice at the beginning of each month.</li>
                <li>5.3 All invoices are payable within 30 days of issue. You may pay by BACS transfer, by cheque made payable to Via-Vox Ltd or by direct debit. If you pay by way of BACS transfer, you will need to ensure that payment reaches us within 30 days of the date of our invoice. If you pay by cheque, we must receive the cheque at least five business days before the expiry the 30 days after the date of the invoice. Where you have arranged to pay by way of direct debit, payment will be taken no less than ten (10) days after the date of our invoice.</li>
                <li>5.4 If we do not receive payment from you, we may immediately terminate or suspend your use of the Powwownow Engage Services.</li>
                <li>5.5 If you have any queries in respect of your invoice, please contact us.</li>
                <li>5.6 Please be aware that you and your invited Participants will also have to pay (where applicable) for any Network Operator Call Charges incurred in respect of telephone conference calls made using the Powwownow Engage Service. Any Network Operator Call Charges will be invoiced on the standard telephone bill issued by your (or their, as applicable) telephone network operator at the prevailing Network Operator Call Charge rate for calls to the relevant dial-in number. We always advise that you and each of your Participants should check with the relevant telephone network operator to confirm the applicable Network Operator Call Charge rate.</li>
            </ul>
        </li>
        <li>Hardware
            <ul>
                <li>6.1 In certain circumstances, we may provide Hardware to you for each of your Users. Hardware is manufactured and supplied by a third party supplier.</li>
                <li>6.2 Delivery of the Hardware will be completed when the Hardware is delivered to the address you gave us. The Hardware will be your responsibility from the completion of delivery.</li>
                <li>6.3 You must inspect the Hardware on delivery and inform us immediately if there are any problems.</li>
                <li>6.4 The Hardware may come with a manufacturer's guarantee. For details of the applicable terms and conditions, please refer to the manufacturer's guarantee provided with the Hardware.</li>
                <li>6.5 The Hardware is supplied for internal use by your business only, and you agree not to use the Hardware for any re-sale purposes.</li>
                <li>6.6 Except as expressly stated in these Additional Terms and Conditions, we do not give any representation, warranties or undertakings in relation to the Hardware. Any representation, condition or warranty which might be implied or incorporated into these Additional Terms and Conditions by statute, common law or otherwise is excluded to the fullest extent permitted by law. In particular, we will not be responsible for ensuring that the Hardware is suitable for your purposes.</li>
                <li>6.7 Provided the term of the contract between you and us is for 12 months or more and you have 30 or more Users, if we agree in writing to supply the Hardware to you for each User, we will do so without charge.</li>
                <li>6.8 If, following the supply of Hardware in accordance with Paragraph 6.7, the contract between you and us for the Powwownow Engage Service is terminated (for whatever reason) within 12 months of the contract being formed, we will charge you £50 per Hardware supplied. For example, if you have 34 Users and the contract is terminated before the expiry of the initial 12 months of the contract, we will invoice you for £50 x 34 Users = £1,700.</li>
                <li>6.9 If you have fewer than 30 Users and we agree in writing to supply the Hardware to you for each User, we will charge you £5 per User per month in advance for the first 12 months of the contract. Invoices raised by us in accordance with this Paragraph 6.9 shall be payable by you in accordance with Paragraph 5 above.</li>
                <li>6.10 If, following the supply of Hardware in accordance with Paragraph 6.9, our contract for the Powwownow Engage Service is terminated (for whatever reason) within 12 months of the contract being formed, we will, on termination, invoice you for an amount equal to the amount which would have been invoiced for the Hardware from the date of termination until the expiry of the initial 12 months. For example, if you have 20 Users and the contract is terminated after 8 months, we will invoice you for £5 x 20 Users x 4 months remaining = £400.</li>
                <li>6.11 Invoices raised in accordance with Paragraphs 6.8 and 6.10 shall be payable immediately.</li>
            </ul>
        </li>
        <li>Our agreement with you
            <ul>
                <li>7.1 Your agreement with us will continue until terminated by either you or us in accordance with Paragraph 7.2 below.</li>
                <li>7.2 You may terminate your agreement with us at any time by giving us not less than four weeks' written notice of your intention to terminate this agreement.</li>
                <li>7.3 We may contact you throughout the duration of our agreement with you to see if we can improve the services, or offer you a more competitive package. If we do agree any changes to your Powwownow Engage Service or the terms of our agreement with you, we will send you a new proposal and we will enter into a new agreement with you following the steps set out in Paragraph 3. Upon the conclusion of a www.yuuguu.com/user_license and at http://www.vidyo.com/terms-of-use) in addition to the Terms.</li>
            </ul>
        </li>
    </ol>
    <p>VERSION September 2012</p>
</div>