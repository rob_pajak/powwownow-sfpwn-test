<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => 'Terms and Conditions',
                'headingTitle' => 'Terms and Conditions',
                'headingSize' => 'l',
                'headingUserType' => 'powwownow'
            )
        ); ?>

        <ul class="terms-and-conditions-menu">
            <li><a class="terms-and-conditions-contents" href="#" data-id="1">Powwownow general terms and conditions</a></li>
            <li><a class="terms-and-conditions-contents" href="#" data-id="2">Powwownow Service additional terms and conditions</a></li>
            <li><a class="terms-and-conditions-contents" href="#" data-id="3">Powwownow Plus Service additional terms and conditions</a></li>
            <li><a class="terms-and-conditions-contents" href="#" data-id="4">Powwownow Premium Service additional terms and conditions</a></li>
            <li><a class="terms-and-conditions-contents" href="#" data-id="5">Powwownow Engage Service additional terms and conditions</a></li>
            <li><a class="terms-and-conditions-contents" href="#" data-id="6">Powwownow Video Conferencing Service additional terms and conditions</a></li>
            <li><a class="terms-and-conditions-contents" href="#" data-id="7">Powwownow iMeet Service additional terms and conditions</a></li>
        </ul>
        <div class="clearfix terms-conditions-container">
            <?php foreach ($termsAndConditionsList as $k => $partial) : ?>
                <?php echo $partial; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
