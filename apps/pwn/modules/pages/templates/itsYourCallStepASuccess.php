<?php include_component('commonComponents','responsiveLogo');?>

<?php include_component('commonComponents','responsiveHeaderNavigation');?>

<div class="row <?php echo $cssClass?>">
    <div class="large-9 columns custom-grid-12">
        <h1 class="header-style-1 text-center">You are ready to start conference calling</h1>

        <?php include_component('commonComponents','responsiveCallingCard',array('pin'=>$pin));?>

        <div class="large-12 columns">
            <div class="panel green rounded-corners-10 clearfix">
                <h2 class="header-style-1">Where should we send your free welcome pack?</h2>

                <section id="its-your-call-request-welcome-pack-container" class="large-5 columns">

                    <script type="text/template" id="its-your-call-request-welcome-pack-template">

                        <div id="attrs" class="hide" data-redirect="<?php echo url_for($redirectUrl);?>"></div>

                        <form id="its-your-call-request-welcome-pack-form" class="its-your-call-request-welcome-pack-form custom" method="post" action="<?php echo url_for('@its_your_call_step_a_ajax'); ?>">

                            <div class="row show-for-small">
                                <div class="large-12 columns">
                                    <ul class="chevron list-font-11 font-grey">
                                        <li>
                                            <strong>Personalised stickers</strong><br>
                                            For your computer and phone
                                        </li>
                                        <li>
                                            <strong>Wallet Card</strong><br>
                                            With your personal dial-in numbers and PIN
                                        </li>
                                        <li>
                                            <strong>Free delivery</strong><br>
                                            We'll post your card and stickers within 2 working days
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['first_name']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['first_name']->render(array('id'=>'its-your-call-request-welcome-pack-form-first-name','class' => 'rounded-corners', 'placeholder' => 'First Name *', 'autocomplete'=>'off','maxlength' => 30, 'data-required'=>"true", 'data-error-message'=>'Please enter firstname.')); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['last_name']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['last_name']->render(array('id'=>'its-your-call-request-welcome-pack-form-last-name','class' => 'rounded-corners', 'placeholder' => 'Last Name *', 'autocomplete' => 'off', 'data-required' => 'true', 'data-error-message' => 'Please enter your Last Name'))?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                     <?php echo $form['company']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                     <?php echo $form['company']->render(array('id'=>'its-your-call-request-welcome-pack-form-company','class' => 'rounded-corners', 'placeholder' => 'Company', 'autocomplete' => 'off'));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['address']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['address']->render(array('id'=>'its-your-call-request-welcome-pack-form-address','class' => 'rounded-corners', 'placeholder' => 'Address *', 'autocomplete' => 'off', 'data-required' => 'true', 'data-error-message' => 'Please enter your Address'));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['town']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['town']->render(array('id'=>'its-your-call-request-welcome-pack-form-first-town','class' => 'rounded-corners', 'placeholder' => 'Town *', 'autocomplete' => 'off', 'data-required' => 'true', 'data-error-message' => 'Please enter your Town'));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['county']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['county']->render(array('id'=>'its-your-call-request-welcome-pack-form-first-county','class' => 'rounded-corners', 'placeholder' => 'County *', 'autocomplete' => 'off', 'data-required' => 'true', 'data-error-message' => 'Please enter your County'))?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['postcode']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['postcode']->render(array('id'=>'its-your-call-request-welcome-pack-form-postcode','class' => 'rounded-corners', 'placeholder' => 'Postcode*', 'autocomplete'=>'off', 'maxlength' => 20, 'data-trigger' => 'change', 'data-required'=>"true", 'data-regexp' => '^([A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKS-UWa-hjks-uw])\ {0,1}[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|([Gg][Ii][Rr]\ 0[Aa][Aa])|([Ss][Aa][Nn]\ {0,1}[Tt][Aa]1)|([Bb][Ff][Pp][Oo]\ {0,1}([Cc]\/[Oo]\ )?[0-9]{1,4})|(([Aa][Ss][Cc][Nn]|[Bb][Bb][Nn][Dd]|[BFSbfs][Ii][Qq][Qq]|[Pp][Cc][Rr][Nn]|[Ss][Tt][Hh][Ll]|[Tt][Dd][Cc][Uu]|[Tt][Kk][Cc][Aa])\ {0,1}1[Zz][Zz]))$','data-error-message'=>'Please enter postcode.')); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['country']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['country']->render(array('id'=>'its-your-call-request-welcome-pack-form-country','class' => 'rounded-corners', 'placeholder' => 'Country *', 'autocomplete' => 'off', 'data-required' => 'true', 'data-error-message' => 'Please enter your Country', 'readonly' => 'readonly', 'value' => 'United Kingdom'));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['password']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['password']->render(array('id'=>'its-your-call-request-welcome-pack-form-first-password','class' => 'rounded-corners', 'placeholder' => 'Password (Optional)'));?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['password_retype']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['password_retype']->render(array('id'=>'its-your-call-request-welcome-pack-form-password_retype','class' => 'rounded-corners', 'placeholder' => 'Retype your password'));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <?php echo $form['question']->renderLabel(null,array('class'=>'paragraph-font-12'));?>
                                    <?php echo $form['question']->render(array('class'=> 'custom-pwn select margin-bottom-25px'));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input id="its-your-call-email-button" type="submit" class="yellow small button radius get-my-pin" value="Submit"/>
                                </div>
                            </div>
                            <div class="form-field">
                                <div class="throbber loading green hide"></div>
                            </div>
                        </form>

                    </script>
                </section>


                <section class="large-7 columns hide-for-small order-your-free-welcome-pack">
                    <div class="lozenge lozenge-style-1">
                        <h2 class="header-style-1 text-center">Your free welcome pack</h2>
                        <img class="welcome-pack-img" width="260" height="194" alt="Powwownow Welcome Pack" src="<?php echo getDataURI('/sfimages/walletCard/welcome-pack.jpg');?>" />
                        <p class="paragraph-font-11 font-white text-center"><i>"Your wallet was very innovative and made me smile"</i>&nbsp;<strong>Yvonne, BMW</strong></p>

                        <ul class="chevron margin-left-45px list-font-11 font-grey">
                            <li>
                                <strong>Personalised stickers</strong><br>
                                For your computer and phone
                            </li>
                            <li>
                                <strong>Wallet Card</strong><br>
                                With your personal dial-in numbers and PIN
                            </li>
                            <li>
                                <strong>Free delivery</strong><br>
                                We'll post your card and stickers within 2 working days
                            </li>
                        </ul>
                    </div>

                    <div class="lozenge lozenge-style-1">
                        <h2 class="header-style-1 text-center">Get all this free</h2>
                        <p class="paragraph-font-11 text-center hide-for-medium-down">When you create a password, you'll be able to use: </p>

                        <ul class="what-you-get-pwn list-font-11">
                            <li>
						        <span class="icon icon-blue">
							        <span class="icon-phone-small-white png"></span>
						        </span>
                                <strong>Call settings</strong><br>
                                <span>Change your hold music, voice prompt language and announcements</span>
                            </li>
                            <li>
						        <span class="icon icon-blue">
							        <span class="icon-arrow-white png"></span>
						        </span>
                                <strong>Web conferencing</strong><br>
                                <span>Share your screen with the participants on your call</span>
                            </li>
                            <li>
						        <span class="icon icon-blue">
							        <span class="icon-speaker-white png"></span>
						        </span>
                                <strong>Recordings</strong><br>
                                <span>Record, download and share your conference calls</span>
                            </li>
                            <li>
						        <span class="icon icon-blue">
							        <span class="icon-calender-white png"></span>
						        </span>
                                <strong>Scheduler</strong><br>
                                <span>Organise your conference calls and invite participants</span>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>

    </div>

    <?php include_component('commonComponents','responsiveRightMenu', array('gtm' => true));?>

</div>

<?php include_component('commonComponents','responsiveFooter');?>
