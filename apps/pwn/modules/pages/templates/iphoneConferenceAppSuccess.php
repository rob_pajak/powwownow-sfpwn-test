<?php include_component('commonComponents', 'header'); ?>

<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_18" style="height: 470px;">
            <h1 class="tab_page_heading">Introducing the iPhone Conference App</h1>
            <div style="padding-left: 15px">
                <p>Powwownow's iPhone Conference Call App brings you <a title="Mobile Phone Conference Call" href="<?php echo url_for('@mobile_phone_conference_call'); ?>">mobile conference calling</a> at the touch of a button.</p>
                <p>The first of its kind, the App will schedule conferences, invite participants from your phone’s contact lists, send automatic call reminder alerts via email and push notifications, plus geo-locate you to determine the best dial-in number for you wherever you are in the world.&nbsp;</p>
                <p>On top of this, the Powwownow App automatically dials you in to your call and inputs the PIN leaving you to get talking! <a title="walkthrough demo" href="/sfswf/iPhoneWalkthrough.swf" target="_blank">View the Conference Call App DEMO here</a>.</p>
                <p>App Features Include:</p>
                <ul class="chevron-green">
                    <li class="png pie_first-child" style="width: 250px; float: left;">One Touch Call Connection</li>
                    <li class="png" style="float:left;">Conference Call Scheduler</li>
                    <li class="png" style="width: 250px; float: left; clear: both;">Reminder Alerts for all users</li>
                    <li class="png" style="float:left;">Geolocator to determine the best dial-in numbers</li>
                </ul>
                <h2 style="clear:both; padding-top:18px; font-size: medium; color: #6e91a4;">Start your iPhone conference call now, it's as easy as 1-2-3!</h2>
            </div>
            <ul id="mobileapp_3steps">
                <li class="step1">
                    <div class="step-number rockwell">1</div>
                    <h2 class="rockwell">Download the iPhone App to register</h2>
                    <div style="text-align:center; position:relative; z-index:999;">
                        <button class="button-orange" type="button" onclick="window.open('http://app.powwownow.com')">
                            <span>Download the App</span>
                        </button>
                    </div>
                </li>
                <li class="step2">
                    <div class="step-number rockwell">2</div>
                    <h2 class="rockwell">Take a walk through how it works</h2>
                    <div style="text-align:center; position:relative; z-index:999;">
                        <button class="button-orange" type="button" onclick="window.open('/sfswf/iPhoneWalkthrough.swf')">
                            <span>View the Demo</span>
                        </button>
                    </div>
                </li>
                <li class="step3">
                    <div class="step-number rockwell">3</div>
                    <h2 class="rockwell">Easily schedule and join your first mobile conference call</h2>
                </li>
            </ul>
        </div>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>
        <script type="text/javascript">$(document).ready(function() {$('#right-menu-container').css('margin-top','20px');});</script>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Welcome to Powwownow</h3>
                <p style="text-align: justify;">Powwownow was founded in 2004 and is now the leading <a title="Free Conference Call" href="<?php echo url_for('@free_conference_call'); ?>">free conference call</a> provider in the UK.</p>
                <p style="text-align: justify;">At Powwownow, we don't believe in doing things the hard way. That's why we offer instant <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>">conference calling</a>, available 24/7, with as many participants as required, wherever they are in the world. And because our service is free, callers only pay the cost of their own phone call, which is added to their standard telecoms bill - nothing more!</p>
                <p style="text-align: justify;">Plus, because we are a telecommunications company in our own right, our customers can be guaranteed the best quality calls with the maximum number of features for the lowest possible price. That’s why <a title="How We Are Different" href="<?php echo url_for('@how_we_are_different'); ?>">no one does conference calling like we do</a>!</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; iPhone Conference App</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>