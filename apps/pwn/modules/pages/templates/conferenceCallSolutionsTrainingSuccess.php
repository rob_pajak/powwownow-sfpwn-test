<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSolutionsLeftNav', array('activeItem' => 'Training')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Solutions', 'Employee and Client Training'),'title' => __('Employee and Client Training'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Employee and Client Training'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                 <p>Having well trained employees is important to any business’ success, but if you have a number of new employees across multiple work sites, remote workers spread out across the country or globally, or if you yourself don’t spend much time in the office, finding the time and resources to train these all up can seem impossible. Now the impossible can quickly, easily and cheaply be made possible using Powwownow’s free <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference call</a> services. Our conference call service can help you make sure your employees are the best employees they can be by giving you the ability to train them up wherever and whenever you like.</p>
                <p>In the past you used to have to set aside an entire day of you and your staff’s time to devote to training and even pull them into the office from all corners of the earth, but when training with conference calls your staff can stay in their local offices or homes and will only need to take as much time away from work as the conference call requires, so they can quickly get back to business once the call wraps up. Best of all, our <a title="audio conferencing" href="<?php echo url_for('@audio_conferencing'); ?>">audio conference calls</a> allow you to invite up to 1,000 people so you can train up hundreds of employees at the same time.</p>
                <p>As a leading provider of conference call services, Powwownow offers more than just the standard conference call experience, but without any of the hassle or expense of buying additional conference call hardware. All you and your participants need is a phone, your dial-in number and your secure PIN number and you’re set to go. Our international dial-in options also mean it doesn’t matter where in the world your employees are: as long as they have access to a telephone, they have access to a <a title="telephone conferencing" href="<?php echo url_for('@telephone_conferencing'); ?>">telephone conference</a>!</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>