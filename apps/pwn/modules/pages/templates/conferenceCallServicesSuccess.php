<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
            'header' => 'Conference Call Services',
            'subHeader' => 'As a supplier of free conference call services, all participants pay is the cost of their own call which is added to their standard telecoms bill - so no hidden charges!',
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource,
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Powwownow Conference Call Services</h3>
                <p style="text-align: justify;">At Powwownow we like to keep things simple for our customers, that's why we offer a <strong>free conference call service</strong> in 3 simple steps: generate your PIN; share the PIN and dial-in number with your participants; then at the agreed time all dial-in, enter the PIN and start talking!</p>
                <p style="text-align: justify;">As Europe's fastest growing conference call provider, our service is now available in over 15 countries worldwide, making it easier to <a title="What it Costs" href="<?php echo url_for('@costs'); ?>">connect with clients for less</a> wherever they are in the world!</p>
                <p style="text-align: justify;">Get started today and find out just how easy staying connected can be.</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Conference Call Services</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>