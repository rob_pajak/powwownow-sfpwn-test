<?php use_helper('pwnVideo') ?>
<?php include_component('commonComponents', 'header'); ?>

<div class="container_24 clearfix">
    <div class="grid_24 clearfix">

        <div id="tabs-grid" style="position: relative;" class="grid_18">
            <h1>Web Conferencing &amp; Screen Share</h1>
            <div class="subheading">Free and simple desktop sharing allows you to share your desktop and documents with call participants.</div>
            <div class="homepage-tab-1 tab-1-green active png" id="tab-1">
                <div class="rockwell font-large white png">Screen Share</div>
                <div style="display:none" class="tab-shadow png"></div>
            </div>
            <div class="homepage-tab-2 tab-2-grey png " id="tab-2">
                <div class="rockwell font-large white">Demo</div>
                <div class="tab-shadow png"></div>
            </div>
            <div class="homepage-tab-3 tab-3-grey png" id="tab-3">
                <div class="rockwell font-large white">Get Started</div>
                <div class="tab-shadow png"></div>
            </div>
            <div>
                <div id="tabs-container">
                    <div class="white" id="tab-web-conferencing-showtime">
                        <div class="compare-button-container">
                            <button id="join-session-button" class="button button-green compare-button" onClick="window.open('http://powwownow.yuuguu.com')">
                                <span>JOIN A SESSION NOW</span>
                            </button>
                        </div>
                        <h2 class="rockwell white">Web Conferencing & Screen Share with Powwownow</h2>
                        <div class="subheading3">Sometimes you need more than just a <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>" class="white">conference call.</a> Powwownow&apos;s free web conferencing allows you to share documents and your desktop whilst you chat. </div>
                        <p>With its screen sharing functionality you can share whatever is on your screen with whoever is on your call, allowing you to work on the same document at the same time. There is no need to buy expensive hardware, all you need is to download the screen share tool and your participants don&apos;t need to download anything, they simply need only a web browser and internet connection.</p>
                        <p>Ideal for presentations, sharing updates, training sessions and collaborative working. To get started, simply download the free screen share tool above.</p>
                        <ul class="web-conferencing-showtime-icons-list">
                            <li>
                                <span class="icon icon-white"><span class="icon-question-mark-grey png"></span></span>
                                <a class="white tooltip-showtime-in-action-link" href="#">What can you use it for?</a>
                                <div id="tooltip-showtime-in-action" class="tooltip">
                                    <div>
                                        <div class="tooltip-arrow-left png"></div>
                                        <a class="sprite tooltip-close floatright tooltip-showtime-in-action-close" href="#"><span class="sprite-cross-white png"></span></a>
                                        <span class="icon icon-grey floatleft"><span class="icon-question-mark-white png"></span></span>
                                        <h4 style="margin-left: 5px; position:relative; top:6px;" class="rockwell grey-dark floatleft">Powwownow Web in action</h4>
                                        <div class="clear"></div>
                                        <ul style="margin-top:20px;" class="chevron-green grey-dark">
                                            <li class="png">Present company updates without the need to leave your office</li>
                                            <li class="png">Facilitate spontaneous, on-demand events or training plans</li>
                                            <li class="png">Run through sales presentations with your sales team wherever they are</li>
                                            <li class="png">Use it as a motivation tool to show employees how well the company is doing</li>
                                            <li class="png">Recruitment: discuss CV's interactively. Decide together on the right person for the job.</li>
                                            <li class="png">Stick to the Agenda: chairing a conference call is much easier when it's a web conference call</li>
                                            <li class="png">Training: Your workforce can learn efficiently - and remotely - by web conference</li>
                                            <li class="png">Promotional Marketing: A web conference does your ideas justice</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="icon icon-white"><span class="icon-arrow-down-grey png"></span></span>
                                <a class="white" href="<?php echo url_for('@web_conferencing_auth'); ?>">Existing user? Download Powwownow Web!</a>
                            </li>
                        </ul>

                        <button onclick="showContentForWebAndVideoConferencingTags(2); return false;" style="margin-right:10px;" class="button-orange floatright" type="submit">
                            <span>VIEW DEMO</span>
                        </button>
                    </div>

                    <div class="white" id="tab-web-conferencing-demo">
                        <div class="grid_sub_17">
                            <img class="videoThumb" src="/sfvideo/webConferencing/poster.png" alt="Web Conferencing"/>
                            <div id="demo-engage-video" title="Web Conferencing"></div>
                            <script>
                                $(function() {
                                    var engageVideo = $("#demo-engage-video");

                                    engageVideo.dialog({
                                        autoOpen: false,
                                        width: "auto",
                                        height: "auto",
                                        modal: true,
                                        dialogClass: "demo-engage-video-dialog",
                                        close: function(event, ui) {
                                            engageVideo.find('video').trigger("stop");
                                            engageVideo.html('');
                                        }
                                    });

                                    $(".videoThumb").on("click", function() {
                                        engageVideo.html('<?php echo html_entity_decode($webConferencingVideo); ?>');
                                        engageVideo.dialog("open");
                                        engageVideo.find('video').trigger("play");
                                        engageVideo.find('video').on("ended", function(){
                                            engageVideo.find('video')[0].currentTime=1;
                                        });
                                    });

                                    $(document).on("click tap touch", '.ui-widget-overlay', function () {
                                        $("#demo-engage-video").dialog("close");
                                    });
                                });
                            </script>
                        </div>
                        <div class="grid_sub_7">
                            <p>Powwownow Web is a free desktop sharing tool for you to use in conjunction with Powwownow's telephone service.</p>
                            <p>Now everyone can see what you're talking about. Download your Web user guide
                                <a class="white" title="Web Conferencing User Guide" href="/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" target="_blank">here</a>.
                            </p>
                            <button onclick="showContentForWebAndVideoConferencingTags(3); return false;" style="margin-top: 15px" class="button-orange" type="submit">
                                <span>REGISTER HERE</span>
                            </button>
                        </div>
                    </div>
                    <div class="white register_box" id="tab-web-conferencing-get-started">
                        <div id="tab-web-conferencing-get-started-initial-content">
                            <h2 class="rockwell white">Existing Powwownow user?</h2>
                            <p>Download and install the Powwownow Web application for free now!
                                <button class="button-orange pie_first-child" style="margin-left:5px;" onclick="dataLayer.push({'event': 'Web-Conferencing - Go to Download/onClick'}); window.location.href='/Login?d=1'">
                                    <span>Go to download</span>
                                </button>
                            </p>
                            <h2 class="rockwell white"><span style="color: #888888;"><span style="color: #ffffff;">Don't have a Powwownow PIN? </span><br /></span></h2>
                            <?php include_component(
                                'commonComponents',
                                'jsKoCxRegistrationTemplate',
                                array(
                                    'koTemplateId' => 'ko.webConference.registration.container',
                                    'koTemplateDataId' => 'ko.webConference.registration.data',
                                    'buttonText' => 'Generate Pin',
                                    'tcText' => 'By clicking Generate PIN, you agree to the <a target="_blank" href="'.url_for('@terms_and_conditions').'">terms and conditions</a>.',
                                    'isLegacyCss' => true
                                )
                            ); ?>
                        </div>
                        <div id="tab-web-conferencing-get-started-form-response-placeholder"></div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function() { webConferencingTabs_Init('webConferencingForm');});</script>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div id="element4">
            <div class="grid_12 alpha">
                <div id="left-content-container">
                    <h3 class="rockwell grey-dark">Welcome to Powwownow</h3>
                    <p style="text-align: justify;">Powwownow was founded in 2004 and is now the leading <a title="Free Conference Call" href="<?php echo url_for('@free_conference_call'); ?>">free conference call</a> provider in the UK.</p>
                    <p style="text-align: justify;">At Powwownow, we don't believe in doing things the hard way. That's why we offer instant <a title="Conference Call" href="<?php echo url_for('@conference_call'); ?>">conference calling</a>, available 24/7, with as many participants as required, wherever they are in the world. And because our service is free, callers only pay the cost of their own phone call, which is added to their standard telecoms bill - nothing more!</p>
                    <p style="text-align: justify;">Plus, because we are a telecommunications company in our own right, our customers can experience excellent quality calls with the maximum number of features for a low price. That&rsquo;s why <a title="How We Are Different" href="<?php echo url_for('@how_we_are_different'); ?>">no one does conference calling like we do</a>!</p>
                </div>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component(
        'commonComponents',
        'videoCarousel',
        array(
            'heading'    => 'Web conferencing made easy',
            'subHeading' => 'Need help with setting up your Powwownow? Check out these handy tips on how to get the most out of your web and audio conferencing.',
            'videoOrder' => array('WebConferencing', 'myPowwownow', '3Steps', 'InConferenceControls')
        )
    ); ?>
</div>