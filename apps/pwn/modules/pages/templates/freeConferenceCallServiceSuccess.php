<?php include_component('commonComponents', 'header'); ?>
<main class="container_16 clearfix">
    <section class="registration_unit clearfix" id="register_now_anchor">
        <section class="registration_description">
            <p class="registration_title">Instant,<br>contract-free,<br>hassle-free<br>conference calling</p>
            <p class="registration_sub_title">With Powwownow you only pay<br>the cost of your own 0844 phone<br>call - nothing more!</p>
            <p class="registration_link"><a href="<?php echo url_for('@costs'); ?>">view cost comparison</a></p>
        </section>
        <section class="registration_form">
            <?php include_component(
                'commonComponents',
                'jsKoCxRegistrationTemplate',
                array(
                    'koTemplateId' => 'ko.main.registration.container',
                    'koTemplateDataId' => 'ko.main.registration.data',
                    'buttonText' => 'Generate PIN',
                    'buttonCssClass' => 'cx_grad_button cx_grad_button_orange_green register_now_button',
                    'seoPage' => true,
                    'tcText' => 'In registering for our service you are agreeing to our <a target="_blank" href="'.url_for('@terms_and_conditions').'">terms and conditions</a>.',
                    'isLegacyCss' => false
                )
            ); ?>
        </section>
    </section>
</main>

<?php include_component('commonComponents', 'whatYouGetFreeConferenceCallService'); ?>

<?php if (isset($trustPilotReviews)): ?>
<section class="container_16 trust_pilot clearfix">
    <section class="trust_pilot_container clearfix">
        <div class="trust_pilot_inner_container clearfix">
            <h2>5 Star rating from customers on </h2>

            <?php foreach ($trustPilotReviews as $trustPilotReview): ?>
            <div class="customer_reviews">
                <div class="quote">
                    &lsquo;<?php echo $trustPilotReview->Title?>&rsquo;
                </div>
                <div class="author">
                    <?php echo $trustPilotReview->User->Name?>
                </div>
            </div>
            <?php endforeach?>
            <div class="read_more">
                <a target="_blank" href="http://www.trustpilot.co.uk/review/powwownow.co.uk">read more reviews</a>
            </div>
        </div>
    </section>
</section>
<?php endif;?>

<section class="container_16 generic_section clearfix">
    <h1 class="header font_blue_g">The instant, contract-free, hassle-free conference call service</h1>
    <div class="grid_5">
        <h3 class="header header_h3 font_green">Ready when you are</h3>
        <p class="font_blue_e">
            Conference calling anytime, anyplace. Our voice conferencing service is available 365 days a year,
            24 hours a day, with as many participants as required whenever and wherever you need it.
        </p>
    </div>
    <div class="grid_5">
        <h3 class="header header_h3 font_green">Easy to use</h3>
        <p class="font_blue_e">
            We believe in making things simple, life is hard enough already – and that’s why we make conference calling simple too.
            Our conference calling is easy, instant and contract-free.
        </p>
    </div>
    <div class="grid_5">
        <h3 class="header header_h3 font_green">More than just audio</h3>
        <p class="font_blue_e">Our free service also includes free call recordings, screen share and a scheduler tool.
            We’ve also a range of on hold music to suit any taste and a large range of low-cost international dial-in numbers.
        </p>
    </div>
</section>

<section class="container_16 three_easy_steps clearfix">
    <div class="trapezoid_bg">
        <div class="trapezoid trapezoid_one">
            <p class="title">Sign Up</p>
            <p class="body">Our one-click sign up makes it easy to get your very own PIN just by providing your email.</p>
        </div>
        <div class="trapezoid trapezoid_two">
            <p class="title">Share <br />your <br />PIN</p>
            <p class="body">Give participants your PIN and the dial-in number.</p>
        </div>
        <div class="trapezoid trapezoid_three">
            <p class="title">Start talking</p>
            <p class="body">At the agreed time, everyone calls up and starts talking.</p>
        </div>
    </div>
    <h3 class="header header_h3 font_blue_e center">Just 3 steps to making your first call</h3>
</section>

<section class="container_16 customer_service clearfix">
    <div class="grid_16">
        <div class="bubble_container">
            <div class="bubble">
                Still need a hand? Contact our <a class="chatlink">Live Chat</a> team for help.
            </div>
        </div>
        <div class="customer_service_text">or</div>
        <div class="register_now_link">
            <a class="register_now_anchor cx_grad_button cx_grad_button_orange_green register_now_button" href="#register_now_anchor" id="register_now_dl">Register NOW</a>
        </div>
        <div class="customer_service_text customer_service_text_font_normal">and make your first call</div>
        <div class="customer_service_line_break">&nbsp;</div>
    </div>
</section>

<section class="container_16 getting_started clearfix">
    <div class="grid_8">
        <h3 class="header header_h3 font_blue_e">Conference Call With Powwownow</h3>
        <p class="font_blue_e">Need to make a hassle-free, instant conference call? Don’t have time for long forms or contracts?
           Then the Powwownow free conference call service is for you. Start conference calling right now with our
           simple one-step sign-up. Simply enter your email address above to get going on your audio conference.
           We believe that smarter working should be accessible to all. That means no hassle,
           hold-ups or complications and definitely no price barriers.</p>
    </div>
    <div class="grid_8">
        <h3 class="header header_h3 font_blue_e center">Getting Started</h3>
        <div class="video_player_popout">
            <img alt="Three Easy Steps" data-video-id="#three_easy_steps" data-auto-play="true" src="/cx2/img/freeConferenceCallingService/macbook.png">
        </div>

        <div id="three_easy_steps" class="pop_out_modal_video_player">
            <div>
                <a href="#close" title="Close" class="close">&#10006;</a>
                <?php include_component(
                    'commonComponents',
                    'hTML5OutputVideo',
                    array(
                        'config' => array(
                            'width'    => 925,
                            'height'   => 365,
                            'autoplay' => false,
                            'controls' => true
                        ),
                        'video'  => array(
                            'image' => '/sfimages/video-stills/3_easy_steps_video_still.jpg',
                            'mp4'   => '/sfvideo/pwn_3steps460x260_LR.mp4',
                            'webm'  => '/sfvideo/pwn_3steps460x260_LR.webm',
                            'ogg'   => '/sfvideo/pwn_3steps460x260_LR.ogv'
                        ),
                        'called' => 'html'
                    )
                ); ?>
            </div>
        </div>
    </div>
</section>
<footer class="container_24 clearfix">
    <?php include_component('commonComponents', 'footer'); ?>
</footer>
