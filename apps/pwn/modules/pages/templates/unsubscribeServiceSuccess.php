<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Unsubscribe'),
                'headingTitle' => __('Unsubscribe'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p>As part of the conditions of use of our service you have agreed to receive emails informing you of important changes and enhancements to our service.</p>
        <p> If you would like to be removed from our promotional email list, please enter your email address below, however you will continue to receive service-related emails from time to time.  Please note it can take up to 24hours for your email address to be unsubscribed.</p>
        <div class="clearfix">
            <p>To confirm that you no longer wish to receive marketing emails, please enter your email address below.</p>
            <div class="margin-top-10">
                <?php echo form_tag(url_for('unsubscribe_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-unsubscribe')); ?>
                    <div class="unsubscribe-form-container">
                        <?php $count = 1; ?>
                        <?php foreach ($unsubscribeFields as $field) : ?>
                            <?php include_partial(
                                'unsubscribeFormField',
                                array(
                                    'field' => $field,
                                    'form'  => $unsubscribeForm,
                                    'count' => $count
                                )
                            ); ?>
                            <?php $count = (2 == $count) ? 1 : 2 ?>
                        <?php endforeach; ?>
                        <input type="hidden" name="origin" id="origin" value="N/A">

                        <div class="floatleft">
                            <button class="button-orange" type="submit"><span>Unsubscribe</span></button>
                        </div>
                    </div>
                    <div class="white unsubscribe-response" style="display: none;">
                        <h2><span style="font-size: 16px;">We&rsquo;ll miss talking to you...</span></h2>
                        <p>You have successfully unsubscribed from receiving any future Powwownow promotional emails and newsletters.</p>
                        <p>To manage your call preferences, use the Scheduler, and much more, login to <a class="white" title="Login to myPowwownow" href="<?php echo url_for('@pins'); ?>">myPowwownow</a>.</p>
                    </div>
                </form>
            </div>
            <div class="margin-top-20">
                <h2 class="green">Not using Powwownow anymore?</h2>
                <p>If you've had problems with the service please <a href="<?php echo url_for('@contact_us'); ?>" title="Contact Us">contact us</a>.</p>
                <p>If you definitely wish to unsubscribe from the Powwownow service and delete your PIN from our database, please enter your email address and PIN below. Please note that it can take up to 24hours for your email address to be unsubscribed.</p>
                <?php echo form_tag(url_for('@delete_pin_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-delete-pin')); ?>
                    <div class="delete-pin-form-container">
                        <?php $count = 1; ?>
                        <?php foreach ($deletePinFields as $field) : ?>
                            <?php include_partial(
                                'deletePinFormField',
                                array(
                                    'field' => $field,
                                    'form'  => $deletePinForm,
                                    'count' => $count
                                )
                            ); ?>
                            <?php $count = (2 == $count) ? 1 : 2 ?>
                        <?php endforeach; ?>
                        <input type="hidden" name="origin" id="origin" value="N/A">
                        <div class="floatleft">
                            <button class="button-orange" type="submit"><span>Unsubscribe</span></button>
                        </div>
                    </div>
                    <div class="white delete-pin-response" style="display: none;">
                        <h2>It was great while it lasted...</h2>
                        <p>You have successfully unsubscribed from the Powwownow service. If you are interested in getting further information on alternative services, check out our <a class="white" title="Powwownow" href="/">Powwownow</a>, <a class="white" title="Powwownow Plus" href="/Plus">Powwownow Plus</a>, <a class="white" title="Powwownow Premium" href="/Premium">Powwownow Premium</a> or <a class="white" title="Web Conferencing" href="/Web-Conferencing">Web Conferencing</a> service.</p>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            (function ($) {
                'use strict';
                $(function () {
                    pwnApp.unsubscribe.init('#form-unsubscribe');
                    pwnApp.deletePin.init('#form-delete-pin');
                });
            })(jQuery);
        </script>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
