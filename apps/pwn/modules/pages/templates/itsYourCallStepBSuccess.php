<?php include_component('commonComponents','responsiveLogo');?>

<?php include_component('commonComponents','responsiveHeaderNavigation');?>

<div class="row <?php echo $cssClass?>">
    <div class="large-9 columns custom-grid-12">
        <h1 class="header-style-1 text-center">You are ready to start conference calling</h1>

        <?php include_component('commonComponents','responsiveCallingCard',array('pin'=>$pin));?>

        <div class="large-12 columns">
            <section class="panel green rounded-corners-10">
                <h2 class="header-style-1">Your welcome pack is in the post!</h2>

                <p class="font-white paragraph-font-11">In myPowwownow you can:</p>

                <ul class="list-font-11 what-you-get-pwn">

                    <li>
                        <span class="icon icon-blue hide-for-medium-down">
		                    <span class="icon-phone-small-white"></span>
	                    </span>
                        <strong>Manage Your Call Settings</strong><br>
                        <span>Change your hold music, voice prompt language and announcements.</span>
                    </li>

                    <li>
	                    <span class="icon icon-blue hide-for-medium-down">
		                    <span class="icon-outlook-white"></span>
	                    </span>
                        <strong>Download The Plugin For Outlook</strong><br>
                        <span>Powwownow's Plugin for Outlook makes it easy to add conference access information to your emails and appointments.</span>
                    </li>
                    <li>
	                    <span class="icon icon-blue hide-for-medium-down">
		                    <span class="icon-calender-white"></span>
	                    </span>
                        <strong>Use Scheduler</strong><br>
                        <span>With Powwownow, you don't have to schedule conference calls. But if you want to, just use this handy tool to organise calls and invite participants.</span>
                    </li>
                    <li>
	                    <span class="icon icon-blue hide-for-medium-down">
		                    <span class="icon-arrow-white"></span>
	                    </span>
                        <strong>Use Web Conferencing</strong><br>
                        <span>Access Powwownow's web conferencing tool, which allows you to share your screen with your call participants.</span>
                    </li>
                    <li>
	                    <span class="icon icon-blue hide-for-medium-down">
		                    <span class="icon-speaker-white"></span>
	                    </span>
                        <strong>Access Recordings</strong><br>
                        <span>Listen, download and share your saved conference call recordings.</span>
                    </li>
                </ul>
                <?php if ($isRegistered):?>
                    <a href="/mypwn" class="yellow small button rounded-corners">Enter MYPOWWOWNOW</a>
                <?php else:?>
                    <a href="/Create-A-Login" class="yellow small button rounded-corners">Create Account</a>
                <?php endif;?>
            </section>
        </div>

    </div>
    <?php include_component('commonComponents','responsiveRightMenu', array('gtm' => true));?>
</div>




<?php include_component('commonComponents','responsiveFooter');?>
