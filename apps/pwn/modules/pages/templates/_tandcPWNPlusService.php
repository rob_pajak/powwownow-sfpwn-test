<div class="terms-conditions-container-item item-3">
    <h2>Powwownow Plus service additional terms and conditions</h2>
    <p>These Additional Terms and Conditions apply in respect of our Powwownow Plus Service, and apply in addition to our General Terms and Conditions. Please read these Powwownow Plus Additional Terms and Conditions together with our <a title="Powwownow Terms and Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>">General Terms and Conditions</a> carefully before Registering for and/or using our Powwownow Plus Service.</p>
    <ol>
        <li>Definitions
            <ul>
                <li>1.1 Unless the context otherwise requires and except for those words and expressions defined in these Additional Terms and Conditions, words and expressions defined in the General Terms and Conditions shall have the same meanings where used in these Additional Terms and Conditions.</li>
                <li>1.2 References to Paragraph numbers in these Powwownow Plus Service Additional Terms and Conditions refer, unless expressly stated otherwise, to Paragraphs contained in these Powwownow Service Additional Terms and Conditions.</li>
                <li>1.3 In these Powwownow Plus Service Additional Terms and Conditions, unless the context otherwise requires:
                    <ul>
                        <li>1.3.1 "Administrator" has the meaning given to that term in Paragraph 3.2;</li>
                        <li>1.3.2 "Branded Welcome Message" means a welcome message which includes your organisation's name, a contact name and a contact telephone number;</li>
                        <li>1.3.3 "BWM Commencement Date" has the meaning given to that term in Paragraph 4.5;</li>
                        <li>1.3.4 "BWM Set Up Charge" has the meaning given to that term in Paragraph 5.15.1;</li>
                        <li>1.3.5 "Dedicated Number Charge" has the meaning given to that term in Paragraph 5.15.2;</li>
                        <li>1.3.6 "First Alert Email" has the meaning given to that term in Paragraph 5.8.1;</li>
                        <li>1.3.7 "International Add-On" means a package which allows international call minutes to be used as part of a Money Saving Minute Bundle;</li>
                        <li>1.3.8 "Network Operator Call Charges" means the price charged to the caller by the network operator;</li>
                        <li>1.3.9 "Money Saving Minute Bundles" means any of the Landline 1000, Landline 2500, Landline 5000 and All You Can Meet call bundles and "Money Saving Minute Bundle" means any one of them;</li>
                        <li>1.3.10 “PAYG Call Credit” means credit purchased by you for Powwownow Call Charges (different amounts of credit can be purchased, please see the Products page for further details);</li>
                        <li>1.3.11 "PAYG Call Credit Cancellation Date" has the meaning given to that term in Paragraph 5.8.1;</li>
                        <li>1.3.12 "Powwownow Call Charges" has the meaning given to that term in Paragraph 5.9;</li>
                        <li>1.3.13 "Powwownow Plus Product" means any additional product connected with the Powwownow Plus Service offered to you by us from time to time including, but not limited to, the Branded Welcome Message, the Money Saving Minute Bundles and an International Add-On;</li>
                        <li>1.3.14 "Powwownow Product Charges" means the applicable charges for the Powwownow Plus Products; and</li>
                        <li>1.3.15 "Second Alert Email" has the meaning given to that term in Paragraph 5.8.2.</li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>Scope of the Powwownow Plus Service
            <ul>
                <li>2.1 Our Powwownow Plus Service gives you the ability to have simultaneous telephone calls with other Participants via the telephone network.</li>
                <li>2.2 As more fully described in these Powwownow Plus Service Additional Terms and Conditions, Powwownow Plus Service is a global audio and web conferencing service, which is delivered via a telephone or the internet and accessed through unique PINs.</li>
                <li>2.3 The particular specification applicable to your Powwownow Plus Service will be specified on your My Products page.</li>
                <li>2.4 The pricing applicable to your Powwownow Plus Service and any purchased Powwownow Plus Products is set out in the details of those packages and products on the Products page.</li>
                <li>2.5 You will be required to purchase PAYG Call Credit from which we will deduct the Powwownow Call Charges displayed as applicable to your chosen Powwownow Plus Service.</li>
                <li>2.6 You (and potentially any Participants to your calls) will be charged Network Operator Call Charges, which appear on your phone bill in the same way as any other call.</li>
            </ul>
        </li>
        <li>Registration, subscribing to Powwownow Plus, and when our contract with you begins
            <ul>
                <li>3.1 If you wish to use our Powwownow Plus Service, you must Register with us online at <a href="http://www.powwownow.co.uk/">www.powwownow.co.uk</a>. If you have previously used our Powwownow Service, Registering for the Powwownow Plus Service will terminate your agreement with us in relation to the Powwownow Service with immediate effect.</li>
                <li>3.2 To Register, you must complete the registration details on the registration page and provide a current valid email address in the registration box shown on screen. You will be designated as the "Administrator". You must then review our General Terms and Conditions, our Privacy Policy and these Powwownow Plus Service Additional Terms and Conditions and, providing that you accept and agree to be bound by such terms, tick the boxes to confirm that you have read and agree to these Additional Terms and Conditions, the General Terms and Conditions and the Privacy Policy. You may then click on the "continue" button to confirm this acceptance.</li>
                <li>3.3 Your purchase of the Powwownow Plus Service is offered by us on this Website (and in any documentation that we submit to you or that you view on our Website) on an "invitation to treat" basis only. By clicking on the "continue" button, you are making an offer to us to purchase the Services. Your offer will only be accepted by us and a contract formed when we have confirmed that we have accepted your request in accordance with Paragraph 3.5 and we issue a set of PINs to you.</li>
                <li>3.4 By clicking on the "continue" button, you warrant and represent to us that:
                    <ul>
                        <li>3.4.1 where you are acting on behalf of a business, you are authorised by the business to request the Services from us and to place and order for the Powwownow Plus Service for and on its behalf; and</li>
                        <li>3.4.2 the information you have provided to us as part of the Registration process is accurate, complete and up-to-date.</li>
                    </ul>
                </li>
                <li>3.5 Once your details have been submitted to us and we have accepted them, we will issue a set of PINs to you. This will be displayed on every page when you are logged in to the Powwownow Plus Service. It is at this point that our contract with you begins. You will then be directed to a homepage explaining more about the Powwownow Plus Service. We may decline your request for the Powwownow Plus Service at our absolute discretion and we are not obliged to provide the Powwownow Plus Service to you until we have confirmed our acceptance to you.</li>
                <li>3.6 Additional users from your business may be added to your Powwownow Plus Service – each person will need to be registered with us by the Administrator and we will issue each additional user with their own PINs.</li>
                <li>3.7 The Administrator may assign and change the Powwownow Plus Products and features available to each PIN pair registered for the business on the Manage Users page.</li>
            </ul>
        </li>
        <li><a name="Powwownow-Plus-Products" class="blank-link">Powwownow Plus Products</a>
            <ul>
                <li>4.1 Once you are registered to use the Powwownow Plus Service, you may purchase additional Powwownow Plus Products. Details of the Powwownow Plus Products available to you are displayed on the Products page.</li>
                <li>4.2 Your purchase of a Powwownow Plus Product is offered by us on this Website (and in any documentation that we submit to you or that you view on our Website) on an "invitation to treat" basis only. By clicking on the "continue" button, you are making an offer to us to purchase the Powwownow Plus Product. Your offer will only be accepted by us and a contract for the Powwownow Plus Product formed when we have confirmed that we have accepted your request.</li>
                <li>
                    <span class="underline-title">Branded Welcome Message</span>
                    4.3 The Branded Welcome Message is a Powwownow Plus Product offered by us for a term of 12 months, renewable thereafter for further terms of 12 months.
                </li>
                <li>4.4 In order to add a Branded Welcome Message, you must:
                    <ul>
                        <li>4.4.1 complete and submit the information on the Products page for the Branded Welcome Message product;</li>
                        <li>4.4.2 select one or more dedicated telephone numbers to which the Branded Welcome Message is to be added; and</li>
                        <li>4.4.3 pay the Powwownow Product Charges as set out on the Basket page. If your Powwownow Plus Service credit is less than £5, a further £5 will be added to the charges on the Basket page and such £5 will be credit available for you to use for Powwownow Call Charges in accordance with Paragraph 5. Payment for the Powwownow Product Charges and any necessary additional credit must be made immediately using WorldPay by following the directions on screen.</li>
                    </ul>
                </li>
                <li>4.5 Once your details for the Branded Welcome Message have been submitted to us, you have paid the relevant Powwownow Product Charges and we have accepted and received the same, the Branded Welcome Message will be uploaded to the selected dedicated telephone number(s) within ten working days, and the "BWM Commencement Date" will be the date on which the Branded Welcome Message is uploaded to your dedicated telephone number(s).</li>
                <li>4.6 You may, by contacting customer services at any time, request changes to the Branded Welcome Message or request an additional telephone number to which the Branded Welcome Message is assigned. We reserve the right to charge the following for any such changes and additions:
                    <ul>
                        <li>4.6.1 the BWM Set Up Charge, for any request to make changes to the Branded Welcome Message; and</li>
                        <li>4.6.2 the Dedicated Number Charge (or a pro-rata proportion thereof), for any additional dedicated telephone numbers to which the Branded Welcome Message is to be assigned.</li>
                    </ul>
                </li>
                <li>
                    <span class="underline-title"><a name="Money-Saving-Minute-Bundles" class="blank-link">Money Saving Minute Bundles</a></span>
                    4.7 The Money Saving Minute Bundles are Powwownow Plus Products offered by us on a monthly rolling basis for a minimum period of one month. At the end of each month, the Money Saving Minute Bundle will automatically renew. You can change or cancel your chosen Money Saving Minute Bundle at any time by contacting Customer Service and giving not less than 7 days notice, such notice to expire on or before the renewal date (the 1st of the month). Provided at least 7 days notice is given before the 1st of the following month, the variation or cancellation will take effect from the 1st of the following month.
                </li>
                <li>4.8 In order to add a Money Saving Minute Bundle (and an International Add-On, if required), you must:
                    <ul>
                        <li>4.8.1 select the relevant Money Saving Minute Bundle (and an International Add-On, if required); and</li>
                        <li>4.8.2 pay the Powwownow Product Charges for the first month as set out on the Basket Page, by credit or debit card using WorldPay by</li>
                    </ul>
                </li>
                <li>4.9 Once you have selected the Money Saving Minute Bundle and, where applicable, the International Add-On, you have paid the Powwownow Product Charges for the first month and we have acknowledged to you by email that we have accepted and received the same, the Money Saving Minute Bundle (and any International Add-On) will be added to your Powwownow Plus Service.</li>
                <li>4.10 The number of minutes included in each Money Saving Minute Bundle is set out on the Products Page. The minutes included in the Money Saving Minute Bundles:
                    <ul>
                        <li>4.10.1 can only be used in respect of UK landline geographic numbers; and</li>
                        <li>4.10.2 do not apply to shared cost, freephone and non-geographic numbers.</li>
                    </ul>
                </li>
                <li>4.11 You may add Worldwide landline numbers to a Money Saving Minute Bundle by purchasing an International Add-On, further details of which are set out on the Products Page. An International Add-On is only available where you also purchase a Money Saving Minute Bundle. All numbers included in an International Add-On must be equal to a call rate of 25p per minute or less.</li>
                <li>4.12 You cannot roll over any unused minutes to the next month. Any minutes used in excess of the minutes included in the Money Saving Minute Bundle will be charged in accordance with Paragraphs 5.10 and 5.11 below.</li>
                <li>4.13 Save in respect of All You Can Meet (where the Money Saving Minute Bundle is assigned to a single user and this cannot be reassigned without contacting our customer services team), the minutes included in the Money Saving Minute Bundle will be assigned to all users on your account. The Administrator can at any time change the users to whom the Money Saving Minute Bundle is applied on the Assign-Products Page.</li>
                <li>4.14 We will send a courtesy email to your Administrator:
                    <ul>
                        <li>4.14.1 when there are only 10% of the minutes remaining of your Money Saving Minute Bundle for that month; and</li>
                        <li>4.14.2 when the minutes included in your Money Saving Minute Bundle have been used for that month.</li>
                    </ul>
                </li>
                <li>
                    <span class="underline-title">Fair Usage Policy</span>
                    4.15 The purpose of the Money Saving Minute Bundles is to enable customers to get the best call package at the lowest possible price and therefore the Money Saving Minute Bundles must not be used by you for telemarketing or call centre operations and you must not re-sell the minutes. Breach of this paragraph will be considered a material breach of the Terms.
                </li>
                <li>4.16 If you purchase an International Add-On and the number of international minutes in any month exceeds 50% of the minutes included in your Money Saving Minute Bundle, we may write to you to request that you moderate your usage. If the number of international minutes for a second month in any 6 month period exceeds 50% of the minutes included in your Money Saving Minute Bundle, we may contact you again asking you to moderate your usage and to warn you that we may terminate your Money Saving Minute Bundle. If the number of international minutes for a third month in any 12 month period exceeds 50% of the minutes included in your Money Saving Minute Bundle, we reserve the right to terminate your Money Saving Minute Bundle.</li>
            </ul>
        </li>
        <li>Payment
            <ul>
                <li>5.1 Once you have added to the Basket the Powwownow Plus Service and/or any Powwownow Plus Products that you would like to purchase, you will be directed to the Payment page. We will invoice you for your order of the Powwownow Plus Service, for your order of any Powwownow Plus Products and for your chosen amount of PAYG Call Credit (if any) and this invoice will be displayed for the Administrator on the Purchase Confirmation Page. Payment must be made immediately using WorldPay by following the directions on screen.</li>
                <li>5.2 You can pay using a debit card or credit card and payments are processed by WorldPay. You will be directed from our site to the WorldPay secure site for payment.</li>
                <li>
                    <span class="underline-title">PAYG Call Credit</span>
                    5.3 You can select an option to automatically order and top-up PAYG Call Credit for your Powwownow Plus Service account when it drops below a certain threshold. When the automatic top-up is triggered, we will invoice you for that top-up amount and this invoice will be displayed for the Administrator on the Account Statement Page. Payment will be automatically taken by WorldPay for such PAYG Call Credit.
                </li>
                <li>5.4 The PAYG Call Credit you purchase will be displayed in the top right hand corner when you log into the Powwownow Plus Service section of our website.</li>
                <li>5.5 Where you have purchased any PAYG Call Credit, we reserve the right to deduct any Powwownow Product Charges from the PAYG Call Credit you have purchased.</li>
                <li>5.6 If you use the auto top-up facility you must ensure your credit card expiry date is still valid to ensure that your Powwownow Plus Service can be topped up. If your credit card has expired we cannot top-up your Powwownow Plus Service:
                    <ul>
                        <li>5.6.1 You will not be able to use the Powwownow Plus Service until further PAYG Call Credit is purchased; and</li>
                        <li>5.6.2 we may immediately terminate or suspend your use of the Powwownow Plus Service.</li>
                    </ul>
                </li>
                <li>
                    <p>To avoid this, you should ensure that you have a valid credit card registered on myPowwownow. We may also send you an email informing you that your registered credit card is about to expire and you might like to consider registering an alternative card to enable the automatic top-up facility.</p>
                    5.7 If at any time there is insufficient credit to pay for the Powwownow Call Charges you have incurred:
                    <ul>
                        <li>5.7.1 you must immediately purchase PAYG Call Credit for your account equal to or greater than the amount of the Powwownow Call Charges incurred;</li>
                        <li>5.7.2 the PAYG Call Credit you purchase in accordance with Paragraph 5.7.1 will be used:
                            <ul>
                                <li>5.7.2.1 firstly to pay the previous Powwownow Call Charge incurred; and</li>
                                <li>5.7.2.2 the balance shall be available for future Powwownow Call Charges; and</li>
                            </ul>
                        </li>
                        <li>5.7.3 if you do not purchase PAYG Call Credit in accordance with this Paragraph 5.7 within 7 days of having insufficient PAYG Call Credit to pay for any Powwownow Call Charges incurred, in addition to our rights in Paragraph 5.6 above:
                            <ul>
                                <li>5.7.3.1 we will to send you an invoice for the outstanding Powwownow Call Charges incurred; and</li>
                                <li>5.7.3.2 such invoice must be paid by you in full cleared funds within 30 days of the date of the invoice by BAC transfer or by credit or debit card (further details of how to pay will be on the invoice).</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <span class="underline-title">Unused PAYG Call Charges</span>
                    5.8 If at any time any or all of your purchased PAYG Call Credit has not been used for 30 days:
                    <ul>
                        <li>5.8.1 we will send you an email (the "First Alert Email") to let you know that if such unused PAYG Call Credit is not used in whole or in part by the date which is 180 days after the First Alert Email (the "PAYG Call Credit Cancellation Date"), such credit will be cancelled;</li>
                        <li>5.8.2 seven days before the PAYG Call Credit Cancellation Date, we will send you a second email (the "Second Alert Email") stating that your unused PAYG Call Credit is about to be cancelled;</li>
                        <li>5.8.3 if you have still not used your PAYG Call Credit by the PAYG Call Credit Cancellation Date, such PAYG Call Credit will be cancelled; and</li>
                        <li>
                            5.8.4  if you make a conference call using any of your PAYG Call Credit:
                            <ul>
                                <li>5.8.4.1 after the First Alert Email but before the Second Alert Email, the 30 day period referred to in this Paragraph 5.8 starts again the day after such conference call; and</li>
                                <li>5.8.4.2 after the Second Alert Email, the 180 day period referred to in Paragraph 5.8.1 starts again the day after such conference call.</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <span class="underline-title">Powwownow Call Charges</span>
                    5.9 You will be charged at the agreed rate for your usage (and, where applicable, the usage of your invited Participants) of the dial-in numbers at the prices set out on the Products page (the "Powwownow Call Charges"), provided always that any minutes included in any Money Saving Minute Bundle will not be charged.
                </li>
                <li>5.10 Save as set out in 5.11 below, if in any month you have used all of the minutes included in your Money Saving Minute Bundle for that month, additional calls will be charged at the standard rate, as varied from time to time and as set out on the Dial-In-Numbers Page.</li>
                <li>5.11 If you have purchased the Landline 5000 Money Saving Minute Bundle and you use more than the 5000 minutes included in the Money Saving Minute Bundle per month, the additional minutes used that the month will be charged at the rate per minute set out on the Products Page for Landline 5000 (as varied from time to time).</li>
                <li>5.12 Save as set out in Paragraph 5.13 below, the Powwownow Call Charges applicable to your use of the Powwownow Plus Service will be deducted from the PAYG Call Credit you have purchased within half an hour of the last Participant leaving the call.</li>
                <li>5.13 Where you have purchased a Money Saving Minute Bundle, any Powwownow Call Charges incurred in accordance with Paragraphs 5.10 and 5.11 above will be invoiced at the start of the following month and this invoice will be displayed for the Administrator on the Account-Statement Page. Payment must be made within 7 days of the date of the invoice using WorldPay by following the directions on screen.</li>
                <li>
                    <span class="underline-title">Powwownow Product Charges</span>
                    5.14 The Powwownow Product Charges payable for the Branded Welcome Message are payable in advance and are non-refundable in the event of termination by you prior to any anniversary of the BWM Commencement Date.
                </li>
                <li>5.15 The Powwownow Product Charges for the Branded Welcome Message are:
                    <ul>
                        <li>5.15.1 a fee for the set up of the Branded Welcome Message, as set out in the Basket page (the "BWM Set Up Charge"); and</li>
                        <li>5.15.2 an annual fee for each dedicated telephone number to which the Branded Welcome Message is assigned, as set out in the Basket page (the "Dedicated Number Charge").</li>
                    </ul>
                </li>
                <li>5.16 One month before each anniversary of the BWM Commencement Date, we will send the Administrator an email giving directions regarding the payment of the Dedicated Number Charge for the following 12 months. If such payment is not received by us on or before the relevant anniversary of the BWM Commencement Date, we may immediately terminate or suspend your use of the Branded Welcome Message product and/or the dedicated telephone numbers.</li>
                <li>5.17 The Powwownow Product Charges for the Money Saving Minute Bundles and any International Add-Ons are monthly charges as set out in the Basket Page and are payable monthly in advance by credit or debit card using WorldPay.</li>
                <li>5.18 Without prejudice to any other rights we may have, if payment of the Powwownow Product Charges for a Money Saving Minute Bundle and (if applicable) an International Add-On for any month is not received on or before the 7th day of that month, we may immediately terminate or suspend your use of the Money Saving Minute Bundle and any relevant International Add-On.</li>
                <li>
                    <span class="underline-title">Other Charges</span>
                    5.19 Please be aware that, where the telephone number used by you under the Powwownow Plus Service is not a freephone number, you and your invited Participants will also have to pay (where applicable) for any Network Operator Call Charges incurred in respect of calls made using the Powwownow Plus Service.  Any Network Operator Call Charges will be invoiced on the standard telephone bill issued by your (or their, as applicable) telephone network operator at the prevailing Network Operator Call Charges rate for calls to the relevant dial-in number.  We always advise that you and each of your Participants should check with the relevant telephone network operator to confirm the applicable Network Operator Call Charges rate.
                </li>
                <li>5.20 There are no cancellation or booking charges.</li>
            </ul>
        </li>
        <li>Your right to cancel
            <ul>
                <li>6.1 Subject to Paragraph 6.2 and (in connection with the cancellation of a Powwownow Plus Product only) Paragraph 6.3, you have the right to cancel your agreement with us for the Powwownow Plus Service and/or cancel the purchase of any Powwownow Plus Products without liability at any time within seven working days from the first working day following the day that you sign up for the Powwownow Plus Service (or the day on which we issue PINs, whichever is the later) or (in relation to the cancellation of a Powwownow Plus Product only) within seven working days from the first working day following the day on which you place an order for the purchase of a Powwownow Plus Product. If you chose to cancel during this 7 working day period, we will provide a refund to you of any PAYG Call Credit that you have purchased and/or a refund of the Powwownow Product Charges paid by you in connection with that Powwownow Plus Product.</li>
                <li>6.2 If you use any of the Powwownow Plus Services within the cancellation period referred to in Paragraph 6.1 and then cancel your agreement with us in accordance with Paragraph 6.1, we will deduct any charges incurred in respect of your use of the Powwownow Plus Services from your PAYG Call Credit.</li>
                <li>6.3 If you use the Powwownow Plus Product within the cancellation period referred to in Paragraph 6.1 and then cancel the purchase of that Powwownow Plus Product in accordance with Paragraph 6.1, we will deduct from the refund of Powwownow Product Charges any costs and expenses incurred by us in respect of your use of the Powwownow Plus Product.</li>
            </ul>
        </li>
        <li>Our agreement with you
            <ul>
                <li>7.1 Your agreement with us will continue until terminated by either you or us in accordance with Paragraphs 11 and 12 of our General terms and Conditions.</li>
                <li>7.2 We may, within 30 days of termination of your agreement with us, at our absolute discretion and following a written request from you, return to you the balance of any PAYG Call Credit purchased by you after deduction of all of Powwownow Call Charges incurred.</li>
            </ul>
        </li>
    </ol>
    <p>VERSION March 2014</p>
</div>
