<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('FAQs'),
                'headingTitle' => __('FAQs'),
                'headingSize' => 's',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <p>I'd like to know more about...</p>
        </div>
        <div id="faqs">
            <?php include_partial('pages/faqs',array()); ?>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
