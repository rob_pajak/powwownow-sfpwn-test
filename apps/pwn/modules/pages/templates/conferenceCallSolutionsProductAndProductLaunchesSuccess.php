<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSolutionsLeftNav', array('activeItem' => 'Project and product launches')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Solutions', 'Project and Product Launches'),'title' => __('Project and Product Launches'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Project and Product Launches'), 'headingSize' => 'xl', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>In the run up to a new product launch, or a large project, keeping the lines of communication open can be critical to its success. When you need a solid service behind you to keep the wheels in motion you can turn to <a title="conference call providers" href="<?php echo url_for('@conference_call_providers'); ?>">conference call provider</a> Powwownow! Our free conference call service will help you stay in touch when it really counts, quickly, easily and at a moment’s notice so that you can ensure everyone working on your project stays on the same page.</p>
                <p>When you’re working to tight operating budgets being able to get a premium teleconferencing service without a premium cost can be a fantastic thing, but what’s even better is if you can get one for free! A <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference call</a> through Powwownow will only set you back the basic cost of the telephone call, no matter how many participants join. So when you need to get the word out quickly you can have hundreds of employees or contractors sitting in on the conference call without any hidden costs. You don’t need to buy any pricey conference call hardware or lock yourself into an expensive contract – all you’ll need is a regular telephone, Powwownow’s dial-in number and a PIN and you’re ready to launch wherever, whenever.</p>
                <p>After all the hard work is done and you’re ready to release your fantastic new product, a conference call can make it quick and simple for you to announce its launch. By using Powwownow <a title="voice conferencing" href="<?php echo url_for('@voice_conferencing'); ?>">voice conferencing</a>, you can hold press conferences without anyone needing to leave the office: just enter the details of the press members you want to invite into the Powwownow Scheduler, set a date and time for your conference call and we will do the rest so you can focus on delivering the perfect pitch. Powwownow’s conference call facilities are available in over 100 countries internationally and at all times of the day, so you can be sure that your voice will be heard the world over.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>