<?php use_helper('getQualityService'); ?>
<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Quality Status'),
                'headingTitle' => __('Quality Status'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>

        <h2><?php echo __('Summary of the state-of-the-art quality of our technology'); ?></h2>
        <p><?php echo __('Our core video and voice conferencing systems are based on robust and powerful, industry standard based media servers that are designed to deliver the full range of video, voice and media services solutions.');?></p>
        <p><?php echo __('Our customer service, provisioning and reporting functions are web-based, and fully integrated with our voice conferencing system, ensuring the latest system information is available online.');?></p>
        <p><?php echo __('All of our production hardware is housed in Tier 3+ datacentres, providing the highest level of services and redundancy for power, cooling and connectivity available within the UK. Our systems have been implemented with at least N+1 redundancy to ensure continuity of service in the unlikely event of a failure.');?></p>
        <p><?php echo __('We configure our systems to provide maximum availability, minimum maintenance and the safety option of remote, 24x7 management. We proactively monitor the status of our servers, network and telephone lines, and we send any alarms instantly to our technical support staff.');?></p>
        <br/>

        <h2><?php echo __('Summary of our critical incident procedure'); ?></h2>
        <p><?php echo __('Our key objective within the incident procedure is to restore a normal state of operations as rapidly as possible. Our customer service desk is the first point of call for all incidents and they manage all communications. Once the incident has been classified, a work request will pass to the operations team who will work to resolve the issue. We aim to resolve critical incidents within 1 hour and high incidents within 24 hours. Our escalation procedure includes the Operations Manager and CTO.');?></p>
        <p><?php echo __('After any incident, a post mortem is carried out to fully understand what went wrong and to identify ways in which to reduce instances of these issues re-occurring.');?></p>
        <br/>

        <h2><?php echo __('Service availability statistics'); ?></h2>
        <table id="tbl_service_availability_stats" class="mypwn tblqualitystats">
            <tr><th>Service Type</th><th>Month / Year</th><th>Availablity</th></tr>
            <?php $odd = 0; ?>
            <?php foreach ($statistics['historical_statistics'] as $k => $v) : ?>
                <tr class="<?php echo ($odd>1) ? 'hiddenstats' : NULL; ?> <?php echo ($odd++%2==1) ? 'odd' : NULL; ?>"><td><?php echo __('Conference call Service availability');?></td><td><?php echo getDateRangeFormatting($v['date_from'],$v['date_to']); ?></td><td><?php echo getTrafficLightImg($v['audio_availability']*100); ?></td></tr>
                <tr class="<?php echo ($odd>1) ? 'hiddenstats' : NULL; ?> <?php echo ($odd++%2==1) ? 'odd' : NULL; ?>"><td><?php echo __('Website availability');?></td><td><?php echo getDateRangeFormatting($v['date_from'],$v['date_to']); ?></td><td><?php echo getTrafficLightImg($v['web_availability']*100); ?></td></tr>
            <?php endforeach; ?>
            <tr class="odd"><td colspan="3" class="showstats">Show more...</td></tr>
        </table><br/>

        <h2><?php echo __('Summary of customer service SLAs'); ?></h2>
        <ul>
            <li><?php echo __('All telephone calls are answered within 3 rings.'); ?></li>
            <li><?php echo __('Emails and website enquiries received during working hours are responded to within 2 hours.'); ?></li>
            <li><?php echo __('All claims escalated to the Technical Department for further investigation should be resolved within 48 hours. If the investigation requires more than 48hours or if the claim needs to be escalated to a third party (supplier), the customer is kept informed at every stage, either by phone or email, of the status of their claim until the enquiry is resolved.'); ?></li>
            <li><?php echo __('Whenever customers are experiencing difficulties that would prevent them from hosting a planned conference call, we provide alternative means so that the customer experiences minimum inconvenience and can conduct business as normal.'); ?></li>
            <li><?php echo __('Provide on-going, support to customers across all areas of their account.'); ?></li>
        </ul><br/>

        <h2><?php echo __('Average call answer time'); ?></h2>
        <table id="tbl_call_answer_stats" class="mypwn tblqualitystats">
            <tr><th><?php echo __('Date');?></th><th><?php echo __('Average call answer time (seconds)');?></th></tr>
            <?php $odd = 0; ?>
            <?php foreach ($statistics['historical_statistics'] as $k => $v) : ?>
                <tr class="<?php echo ($odd++%2==1) ? 'odd' : NULL; ?><?php echo ($odd>1) ? ' hiddenstats' : NULL; ?>"><td><?php echo getDateRangeFormatting($v['date_from'],$v['date_to']); ?></td><td><?php echo round($v['call_answer_time'],1) . ' Seconds'; ?></td></tr>
            <?php endforeach; ?>
            <tr class="odd"><td colspan="2" class="showstats">Show more...</td></tr>
        </table><br/>

        <h2><?php echo __("Customer enquiry / query resolution"); ?></h2>
        <table id="tbl_customer_case_resolution_time" class="mypwn tblqualitystats">
            <tr><th><?php echo __('Date');?></th><th><?php echo __('Customer enquiry / query resolution');?></th></tr>
            <?php $odd = 0; ?>
            <?php foreach ($statistics['historical_statistics'] as $k => $v) : ?>
                <tr class="<?php echo ($odd++%2==1) ? 'odd' : NULL; ?><?php echo ($odd>1) ? ' hiddenstats' : NULL; ?>"><td><?php echo getDateRangeFormatting($v['date_from'],$v['date_to']); ?></td><td><?php echo getHMS($v['customer_case_resolution_time']); ?></td></tr>
            <?php endforeach; ?>
            <tr class="odd"><td colspan="2" class="showstats">Show more...</td></tr>
        </table><br/>

        <h2><?php echo __('Customer satisfaction'); ?></h2>
        <table id="tbl_feedback_score" class="mypwn tblqualitystats">
            <tr><th><?php echo __('Date');?></th><th><?php echo __('Customer satisfaction');?></th></tr>
            <?php $odd = 0; ?>
            <?php foreach ($statistics['historical_statistics'] as $k => $v) : ?>
                <tr class="<?php echo ($odd++%2==1) ? 'odd' : NULL; ?><?php echo ($odd>1) ? ' hiddenstats' : NULL; ?>"><td><?php echo getDateRangeFormatting($v['date_from'],$v['date_to']); ?></td><td><?php echo $v['customer_service_feedback_score'] . ' / 5'; ?></td></tr>
            <?php endforeach; ?>
            <tr class="odd"><td colspan="2" class="showstats">Show more...</td></tr>
        </table><br/>
        <script>
        $(function () {
            powwownowQuality.initialize();
        });
        </script>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
