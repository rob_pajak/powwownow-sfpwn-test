<?php use_helper('DataUri') ?>
<div class="container_24 clearfix">
    <div class="grid_5 clearfix">
        <?php include_partial('commonComponents/headerLogos', array('logos' => $logos, 'serviceUser' => $serviceUser, 'publicPage' => 'true')); ?>
    </div>
</div>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="margin-top-32">
            <?php include_component('commonComponents', 'genericHeading', array('title' => __('Get your 100 days free!'), 'headingSize' => 'l', 'user_type' => 'powwownow')); ?>
        </div>
        <div class="margin-top-20">
            <p><?php echo __('Enter your email address below and we’ll send you more information about our offer.<br/>Alternatively you can call Angela on +44 (0)20 3398 1131 or Customer Services on +44 (0)20 3398 0398.'); ?></p>
        </div>
        <div class="contactus-form clearfix">
            <?php echo form_tag(url_for('im_interested_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-call-back', 'class' => 'pwnform clearfix')); ?>
            <?php $count = 1; ?>
            <?php foreach ($fields as $field) : ?>
                <?php include_partial('callBackFormField', array(
                    'field' => $field,
                    'form'  => $form,
                    'count' => $count
                )); ?>
                <?php $count = (2==$count) ? 1 : 2 ?>
            <?php endforeach; ?>
            <input type="hidden" name="contactus[utm_campaign]" value="<?php echo $utm_campaign; ?>" />
            <div class="form-field grid_sub_18">
                <?php echo $form['comments']->renderLabel(null, array('class' => 'comments')); ?><br />
                <div class="mypwn-input-container comments">
                    <?php echo $form['comments']->render(array('class' => 'input-large font-small mypwn-input')); ?>
                </div>
            </div>
            <div class="grid_sub_6 clearfix margin-top-20">
                <div class="required-field">
                    * Required fields
                </div>
                <div>
                    <button class="button-orange floatright" type="submit">
                        <span>Find Out More</span>
                    </button>
                </div>
            </div>
            </form>
        </div>
        <div class="hr-spotted-top png content-seperator margin-top-20"><!--Blank--></div>
        <div class="recommended-images">
            <img src="<?php echo getDataURI('/sfimages/cro-lp/recommend.gif'); ?>" alt="98% of our customers recommend us, including:" style="margin-left: 0px;" />
            <img src="<?php echo getDataURI('/sfimages/imInterested/bbc.jpg'); ?>" alt="BBC"/>
            <img src="<?php echo getDataURI('/sfimages/imInterested/bentley.png'); ?>" alt="Bentley"/>
            <img src="<?php echo getDataURI('/sfimages/imInterested/ethical_tea_partnership.png'); ?>" alt="Ethical Tea Partnership"/>
            <img src="<?php echo getDataURI('/sfimages/imInterested/nhs.png'); ?>" alt="National Health Service"/>
        </div>
        <div class="hr-spotted-top png content-seperator margin-top-20"><!--Blank--></div>
        <div class="what-you-get">
            <ul class="clearfix">
                <li>
                    <span class="sprite set-a-call-quality"></span>
                    <span class="title">Unbeatable call quality</span>
                    <span class="content">Unlike almost all conference call providers, most of our conference calls use the exact same fibre optic cabling as your landline.</span>
                </li>
                <li>
                    <span class="sprite set-a-scheduling"></span>
                    <span class="title">Free scheduling</span>
                    <span class="content">With Powwownow, you don’t have to book or schedule your conference calls. But if you want to, you can download our plugin for Outlook or use our scheduler tool to organise your calls and invite participants.</span>
                </li>
                <li>
                    <span class="sprite set-a-low-cost"></span>
                    <span class="title">Low-cost international access</span>
                    <span class="content">Access Powwownow from around the world - for the cost of a local-rate phone call.</span>
                </li>

                <li>
                    <span class="sprite set-a-web-conference"></span>
                    <span class="title">Free instant web conferencing</span>
                    <span class="content">Share your computer screen instantly. The attendees don't need to download or install anything – all they need is internet access.</span>
                </li>

                <li>
                    <span class="sprite set-a-customer-support"></span>
                    <span class="title">Fantastic customer support</span>
                    <span class="content">Got a question? We're fanatical about customer support. (That's probably why 98% of our customers would recommend us.) Call us on <b>020 3398 0398</b>.</span>
                </li>

                <li>
                    <span class="sprite set-a-call-recording"></span>
                    <span class="title">Instant call recording</span>
                    <span class="content">Record your conference calls with one touch. Then you can review, download and share them with your colleagues, or keep them for your records.</span>
                </li>
            </ul>
        </div>

        <script type="text/javascript">
            (function($) {
                'use strict';
                $(function() {
                    pwnApp.contactus.init('#frm-call-back');
                });
            })(jQuery);
        </script>
    </div>
    <?php include_component('commonComponents', 'footer', array('showInternational' => false)); ?>
</div>