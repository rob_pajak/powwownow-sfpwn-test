<?php use_helper('pwnVideo') ?>
<div id="container" class="makes-sense">
    <header>
        <div class="inside clearfix">
            <a id="logo" href="http://www.powwownow.co.uk" class="ir">powwownow Get Together Whenever</a>
            <ul id="breadcrumb">
                <li>You are in: <a href="http://www.powwownow.co.uk">Home</a></li>
                <li class="separator">></li>
                <li>Register Now</li>
            </ul>
        </div>
    </header>
    <div id="main">
        <div class="inside">
            <img id="feature" class="feature_img" src="/sfimages/banners/makes_sense.jpeg" alt="The Free conference Calling Service For Business People With More Sense Than Money" />

            <div class="how-it-works-box" style="margin-bottom:15px;">
                <p>
                   With Powwownow conference calling, the service is free. That means no contract, no monthly billing and
                   no hidden fees. All you pay for is the price of a phone call. As you’ve clearly got more sense than money,
                   enter your email below and get talking!
                </p>
            </div>

            <div class="how-it-works-box" style="margin-bottom:15px;">
                <div class="how-it-works-arrow"></div>

                <a href="#" id="how-to-get-started" class="hiw-video-co" title="How to get started with conference calling.">
                   <img src="/sfimages/video-stills/3_easy_steps_video_still_homepage.jpg" alt="3 easy steps" style="float: right; margin-bottom: 10px; width: 230px; margin-right: 10px; border: 4px solid #A2A0A1; margin-top:0;" />
                </a>

                <h3 class="rockwell blue" style="margin-bottom: 5px; margin-top:0; margin-left:7px;">How it works</h3>
                <ul class="how-it-works-list">
                    <li class="first">Enter your email to generate your PIN</li>
                    <li class="second">Share your PIN, dial-in number and time of the call with your call participants</li>
                    <li class="third">At the agreed time, all dial in, enter your PIN and start talking!</li>
                </ul>
                <div class="clear"></div>

            </div>

            <div id="signup" class="clearfix" style="padding:10px 20px; position: relative">
                <p>Our one-step sign-up means you can start conference calling right now</p>
                <form name="signup" id="form-generate-pin" enctype="application/x-www-form-urlencoded" action="/ajax/form-cro-lpCG-1.php" method="post">
                    <input type="text" placeholder="Enter email address here" name="email" id="register_email" />
                    <input type="submit" value="Generate PIN" />
                </form>
            </div>
            <div id="clients">
                <img src="/sfimages/cro-lp/recommend.gif" alt="98% of our customers recommend us, including:" style="width:330px;height:79px" />
                <img src="/sfimages/cro-lp/accenture.jpg" alt="accenture" style="width:140px;height:79px" />
                <img src="/sfimages/cro-lp/motorla-solutions.jpg" alt="Motorola Solutions" style="width:178px;height:79px" />
                <img src="/sfimages/cro-lp/network-marketing.jpg" alt="networkmarketing" style="width:120px;height:79px" />
                <img src="/sfimages/cro-lp/university-brighton.jpg" alt="University of Brighton" style="width:97px;height:79px" />
            </div>
            <h2 class="hozLine"><span>What you get</span></h2>
            <div class="lightBox clearfix">
                <h3>With Powwownow, you get all this FREE</h3>
                <ul id="freeStuff">
                    <li><img src="/sfimages/cro-lp/call-quality.gif" alt="" style="height:63px;width:64px" /><p class="title">Unbeatable call quality</p><p>Unlike almost all confrence call providers, most of our confrence calls use the exact same fibre optic cabling as your landline.</p></li>
                    <li class="last"><img src="/sfimages/cro-lp/scheduling.gif" alt="" style="height:63px;width:64px" /><p class="title">Free scheduling</p><p>With Powwownow, you don’t have to book or schedule your conference calls. But if you want to, you can download our plugin for Outlook or use our scheduler tool to organise your calls and invite participants.</p></li>
                    <li><img src="/sfimages/cro-lp/low-cost.gif" alt="" style="height:63px;width:64px" /><p class="title">Low-cost international access</p><p>Access Powwownow from <a href="<?php echo url_for('@international_number_rates'); ?>" target="_blank">over 14 countries</a> around the world - for the cost of a local-rate phone call.</p></li>
                    <li class="last"><img src="/sfimages/cro-lp/web-conference.gif" alt="" style="height:63px;width:63px" /><p class="title">Free instant web conferencing</p><p>Share your computer screen instantly. The attendees don't need to download or install anything – all they need is internet access.</p></li>
                    <li class="bottom"><img src="/sfimages/cro-lp/customer-support.gif" alt="" style="height:63px;width:63px" /><p class="title">Fantastic customer support</p><p>Got a question? We're fanatical about customer support. (That's probably why 98% of our customers would recommend us.) Call us on <b>020 3398 0398</b>.</p></li>
                    <li class="last bottom"><img src="/sfimages/cro-lp/call-recording.gif" alt="" style="height:63px;width:64px" /><p class="title">Instant call recording</p><p>Record your conference calls with one touch. Then you can review, download and share them with your colleagues, or keep them for your records.</p></li>
                </ul>
            </div>
            <h2 class="hozLine"><span>What it costs</span></h2>
            <div class="clearfix">
                <div class="whatCosts column1">
                    <ul>
                        <li><strong>You just pay the cost of a local 0844 call</strong>Thats it - there are no other fees</li>
                        <li><strong>There’s no contract or minimum usage</strong>You can start and stop as you like</li>
                        <li><strong>There’s no monthly billing</strong>The local call charges show up on your phone bill</li>
                    </ul>
                </div>
                <div class="whatCosts column2">
                    <h3>How our costs compare</h3>
                    <p>This is what you’d pay for a 1hr conference with 4 participants:</p>
                    <img src="/sfimages/cro-lp/cost-compare.gif" alt="Unlike other conference call providers, Powwownow doesn't charge you a 'bridging fee' - everyone just pays the same local-rate call" style="width:750px;height:145px" />
                </div>
            </div>

        </div>
    </div>
    <div class="push"></div>
</div>

<footer>
    <div class="inside clearfix">
        <ul>
            <li><a href="<?php echo url_for('@privacy'); ?>">Privacy</a></li>
            <li>|</li>
            <li><a href="<?php echo url_for('@terms_and_conditions'); ?>">Terms &amp; Conditions</a></li>
            <li>|</li>
            <li><a href="<?php echo url_for('@about_us'); ?>">About Us</a></li>
            <li>|</li>
            <li><a href="<?php echo url_for('@contact_us'); ?>">Contact Us</a></li>
            <li>|</li>
            <li><a href="/Glossary">Glossary</a></li>
            <li>|</li>
            <li><a href="<?php echo url_for('@useful_links'); ?>">Useful Links</a></li>
            <li>|</li>
            <li class="break"><a href="http://www.powwownow.co.uk/Sitemap">Site Map</a></li>
            <li class="social"><a href="http://www.powwownow.co.uk/blog"><img src="/sfimages/cro-lp/blog.gif" alt="Blog" style="width:35px;height:21px" /></a></li>
            <li class="social"><a href="http://www.facebook.com/powwownow"><img src="/sfimages/cro-lp/facebook.gif" alt="Facebook" style="width:15px;height:21px" /></a></li>
            <li class="social"><a href="http://twitter.com/powwownow"><img src="/sfimages/cro-lp/twitter.gif" alt="Twitter" style="width:28px;height:21px" /></a></li>
            <li class="social"><a href="http://www.linkedin.com/company/powwownow"><img src="/sfimages/cro-lp/linkedin.gif" alt="LinkedIn" style="width:29px;height:21px" /></a></li>
            <li class="social"><a href="http://www.youtube.com/user/MyPowwownow"><img src="/sfimages/cro-lp/youtube.gif" alt="Youtube" style="width:49px;height:21px" /></a></li>
        </ul>
        <p id="copyright">2012 Powwownow</p>
    </div>
</footer>
    
<div id="hiw-video-modal" class="reveal-modal">
    <div class="modal-container">
        <h3>3 Easy Steps</h3>
        <div class="modal-video">
            <?php echo embed_html5_video($sf_data->getRaw('videoOptions'))  ?>
        </div>
        <p>How to get started with conference calling.</p>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>
<script>
	$(function () {
	    if (window.PIE) {
	        $("#howWorks li em, #signup input[type=submit], #signup, .lightBox, #signup input[type=text], footer .inside").each(function () {
	            PIE.attach(this)
	        })
	    }
	    if ($("html").hasClass("ie7") || $("html").hasClass("ie8")) {
	        $("[placeholder]").each(function () {
	            $(this).val($(this).attr("placeholder"))
	        }).click(function () {
	            if ($(this).val() === $(this).attr("placeholder")) $(this).val("")
	        }).focusout(function () {
	            if ($(this).val() === "") $(this).val($(this).attr("placeholder"))
	        })
	    }
	    $("footer .social a").click(function (a) {
	        a.preventDefault();
	        window.open(this.href)
	    });
	});
    
    var ModalEvent = (function () {
        'use strict';
        function ModalEvent(document) {
            this.VideoElement = document.getElementById('powwownow_three_easy');
        }

        ModalEvent.prototype.player = function() {
            if (this.VideoElement.paused) {
                this.VideoElement.play();
            } else if (!this.VideoElement.paused) {
                this.VideoElement.pause();
            }
        };
        
        ModalEvent.prototype.render = function($) {
            var that = this;

            $('#how-to-get-started').click(function(e) {
                e.preventDefault();
                $('#hiw-video-modal').reveal({
                    closeonbackgroundclick: false
                });
                that.player();
            });
            $('.video .close-reveal-modal').click(function(e) {
                that.player();
            });
        };
        
        return ModalEvent;
    })();
    
	$(document).ready(function () {
	    if (!$.support.placeholder) {
	        var active = document.activeElement;
	        $('input[type="text"]').focus(function () {
	            if ($(this).attr('placeholder') !== '' && $(this).val() === $(this).attr('placeholder')) {
	                $(this).val('').removeClass('hasPlaceholder');
	            }
	        }).blur(function () {
	            if ($(this).attr('placeholder') !== '' && ($(this).val() === '' || $(this).val() === $(this).attr('placeholder'))) {
	                $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
	            }
	        });
	        $('input[type="text"]').blur();
	        $(active).focus();
	        $('form').submit(function () {
	            $(this).find('.hasPlaceholder').each(function () {
	                $(this).val('');
	            });
	        });
	    }
        
        var $modalEvent = new ModalEvent(document);
        $modalEvent.render($);
        
	})

    formGeneratePin_lp_Init('makes-sense');
    formAdvancedRegistration_var7_Init();
</script>