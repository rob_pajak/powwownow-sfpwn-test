<div class="form-field grid_sub_10<?php echo (2==$count) ? ' clearfix' : ''; ?>" style="z-index: <?php echo (1==$count) ? 350 : 300; ?>">
    <?php echo $form[$field]->renderLabel(); ?>
    <span class="mypwn-input-container">
        <?php echo $form[$field]->render(array('class' => 'input-large font-small mypwn-input')); ?>
    </span>
</div>