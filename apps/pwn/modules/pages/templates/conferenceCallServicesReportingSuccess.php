<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPagesLeftNav', array('activeItem' => 'Reporting')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Services', 'Conference Call Reports'),'title' => __('Conference Call Reports'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Conference Call Reports'), 'headingSize' => 'l', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>If you or your company chair a lot of conference calls, it always pays to be able to review the effectiveness of your calls, but this can be difficult as many <a title="conference calls providers" href="<?php echo url_for('@conference_call_providers'); ?>">conference call providers</a> simply don’t store this information for you, or if they do store it they may not make it accessible to their users. Powwownow is completely different in this respect – no one does conference calling like we do and no one does conference call reporting like we do either! We are delighted if you want to take a deeper look into your teleconferences, so to help you do this we have several reporting options available through myPowwownow!</p>
                <p>myPowwownow is a secure online portal that allows you to run reports on your organisation’s calls so you can get the skinny on what’s going on behind the talk while you’re <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference calling</a>. The chief chairperson or company administrator can run these reports to display a variety of facts and figures. With myPowwownow you can quickly and easily monitor participants joining and leaving conference calls so you can make sure that no one is missing out on important conversations. You can also find out who is regularly joining scheduled conference calls late, which can help you plan your meeting times more efficiently. With our reporting services you’ll always know what phone number each participant used to dial in to the call with, which can give you an overview of the methods they are using to connect. You can also see how many people were on a particular call and how long the call lasted at a glance, allowing you to make an accurate assessment of your conference call’s effectiveness.</p>
                <p>Access to myPowwownow is totally free and makes reporting on your <a title="audio conferencing" href="<?php echo url_for('@audio_conferencing'); ?>">audio conferencing</a> sessions a breeze. Now you’ll always be sure that your voice is being heard at the right time, in the right place and by the right people!</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>