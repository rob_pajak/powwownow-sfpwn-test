<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">


        <?php include_component('commonComponents', 'containerOldLandingPagesWithGreenTabsE', array(
            'header' => 'Voice Conferencing',
            'subHeader' => 'In three simple steps you can be talking with colleagues and customers wherever they are in the UK or around the world.',
            'hintBoxTitle' => "It's easy as 1, 2, 3!",
            'tabTitle' => 'Get started',
            'registrationSource' => $registrationSource,
        )); ?>

        <?php include_component('commonComponents', 'rightLandingPageMenu'); ?>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <div class="grid_12 alpha">
            <div id="left-content-container">
                <h3 class="rockwell grey-dark">Powwownow Voice Conferencing</h3>
                <p style="text-align: justify;">Why travel to meetings when you can meet over the phone at a time that suits you? Powwownow’s <strong>voice conferencing</strong> service is available 24/7, making it easy to connect with colleagues and clients in the UK and around the world.</p>
                <p style="text-align: justify;">At Powwownow we don’t want our customers doing things the hard way, that’s why we offer you free voice conferencing in 3 simple steps: generate your PIN; share the PIN and dial-in number with your participants; then at the agreed time all dial-in, enter the PIN and start talking!</p>
                <p style="text-align: justify;">Powwownow's Customer Services Team is also on hand to offer their renowned and respected support, so <a title="Telephone Conferencing" href="<?php echo url_for('@telephone_conferencing'); ?>">telephone conferencing</a> has never been easier. No wonder 97% of our customers would recommend us!</p>
            </div>
        </div>

        <?php include_component('commonComponents', 'rotatingPromotialLinksE'); ?>

        <div class="grid_24">
            <div class="breadcrumb-landing-page">You are in: <a href="/">Home</a> &gt; Voice Conferencing</div>
        </div>

        <div class="grid_24">
            <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
        </div>

        <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<div style="display: none;">
    <?php include_component('commonComponents', 'videoCarousel', array()); ?>
</div>