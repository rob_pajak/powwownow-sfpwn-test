<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Unsubscribe'),
                'headingTitle' => __('Unsubscribe'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p id="confirmBlurb">To confirm that you no longer wish to receive marketing emails, please enter your email address below.</p>
        <div class="margin-top-10">
            <form id="form-unsubscribe" method="post" action="<?php echo url_for('unsubscribe_ajax'); ?>" class="pwnform h5form clearfix">
                <div class="unsubscribe-form-container">
                    <?php $count = 1; ?>
                    <?php foreach ($fields as $field) : ?>
                        <?php include_partial(
                            'unsubscribeFormField',
                            array(
                                'field' => $field,
                                'form'  => $form,
                                'count' => $count
                            )
                        ); ?>
                        <?php $count = (2 == $count) ? 1 : 2 ?>
                    <?php endforeach; ?>
                    <input type="hidden" name="origin" id="origin" value="N/A">
                    <div class="floatleft">
                        <button class="button-orange" type="submit"><span>Unsubscribe</span></button>
                    </div>
                </div>
                <div class="white unsubscribe-response" style="display: none;">
                    <h2><span style="font-size: 16px;">We&rsquo;ll miss talking to you...</span></h2>
                    <p>You have successfully unsubscribed from receiving any future Powwownow promotional emails and newsletters.</p>
                    <p>To manage your call preferences, use the Scheduler, and much more, login to <a class="white" title="Login to myPowwownow" href="<?php echo url_for('@pins'); ?>">myPowwownow</a>.</p>
                </div>
            </form>
        </div>
        <p>Please be advised that you will no longer receive newsletters and promotional emails from us, however as part of the conditions of use of our service you will continue to receive emails informing you of important changes and enhancements to our service.</p>
    </div>
    <script type="text/javascript">
        (function ($) {
            'use strict';
            $(function () {
                pwnApp.unsubscribe.init('#form-unsubscribe');
            });
        })(jQuery);
    </script>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
