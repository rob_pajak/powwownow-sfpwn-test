<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">

            <div class="grid_sub_24 margin-top-10">
                <h1 class="heavy">Congratulations!</h1>
            </div>


    <div class="grid_24 clearfix">
        <div class="grid_24 margin-top-20">
            <p>Your PIN (<span class="heavy pin"><?php echo $pin; ?></span>) has been re-validated and we’ve sent you an email confirmation of this.</p>

            <p>Your dial-in number is <span class="heavy pin">0844 4 73 73 73</span>.</p>

            <p>Time is the one thing we never seem to have enough of and in the email, we’ve included some smart ways
            for you to save time in your daily life – just so you can do the things that you really need to do!</p><br />

            <p>If there are any aspects of conference calling that you aren’t sure of, we have a whole host of resources
            that can help you:</p><br />
            <ul class="no-bullets-list">
                <li>
                    <p><a href="#" onclick="dataLayer.push({'event': 'Powwownow Blog/onClick'}); setTimeout(function(){window.location.href = '/blog';}, 1000);">Powwownow Blog</a> – informative, educational and often fun, our blog offers articles such as how to save
                    money on mobile calls, tips to successful remote working, how screen sharing and video calling works...
                    and so much more.</p>
                </li>
                <li>
                    <p><a href="/sfpdf/en/Powwownow-User-Guide.pdf" onclick="dataLayer.push({'event': 'Revalidate Pin - Download Powwownow-User-Guide/onClick'});" target="_blank">Conference Calling User Guide</a> – as easy as 1, 2, 3... download this user guide to break down conference
                    calling to help you on your way.</p>
                </li>
                <li>
                    <p><a href="#" onclick="dataLayer.push({'event': 'Top 10 Tips for Conference Calling/onClick'}); setTimeout(function(){window.location.href = <?php echo url_for('@top_ten_tips_for_conference_calling'); ?>;}, 1000);">Top 10 Tips for Conference Calling</a> – make the most of your conference calls with these handy top tips!</p>
                </li>
            </ul>
            <p>Why not schedule your next conference call today with our Outlook Plugin?</p>
            <ul class="no-bullets-list">
                <li class="padding-bottom-10">
                    <a href="/Outlook_Plugin/current/setup-powwownow-plugin-2003-2007.exe" onclick="dataLayer.push({'event': 'Outlook Plugin - 2003/onClick'});" target="_blank">Download Plugin for Outlook 2003-2007</a>
                </li>
                <li>
                    <a href="/Outlook_Plugin/current/setup-powwownow-plugin-2010-2013.exe" onclick="dataLayer.push({'event': 'Outlook Plugin - 2010/onClick'});" target="_blank">Download Plugin for Outlook 2010-2013</a>
                </li>
            </ul>
            <p class="margin-top-20">Got a question?  <a class="chatlink heavy" title="Live Chat!" href="javascript:void(0);" onclick="dataLayer.push({'event': 'Live Chat/onClick'});">Live chat now</a> with one of our Customer Service Team or call 0203 398 0398</p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>