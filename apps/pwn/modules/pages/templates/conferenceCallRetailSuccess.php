<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_6">
            <?php include_component('commonComponents', 'conferenceCallSubPages2LeftNav', array('activeItem' => 'Retail')); ?>
        </div>
        <div class="grid_sub_18 floatright">
            <div style="margin-top:15px;">
                <?php include_partial('commonComponents/genericBreadcrumbs',array('breadcrumbs' => array('Conference Call', 'Retail Conference Call'),'title' => __('Retail Conference Call'))) ?>
            </div>
            <div style="margin-top:32px;">
                <?php include_component('commonComponents', 'genericHeading', array('title' => __('Retail Conference Call'), 'headingSize' => 'l', 'user_type' => 'powwownow')); ?>
            </div>
            <div class="grid_24 clearfix">
                <p>In an ever more crowded retail marketplace, making sure your business meets the needs of the consumer better than any of your competitors can be a constant challenge – whether you’re trying to keep up-to-date with the latest developments in fashion or trying to secure supply of that new coveted gadget, keeping strong lines of communication open between you, your franchisees, outlets and suppliers can make all the difference between success and disaster. Having <a title="teleconferencing services" href="<?php echo url_for('@teleconferencing_services'); ?>">teleconferencing services</a> you can rely on means you have one less thing to worry about and can get on with what really matters – making sure your customers are the happiest shoppers on the high street.</p>
                <p>That's where conference call provider Powwownow steps in. We provide a conference calling service that will be there whenever and wherever you need it and it’s totally free. Whether you’re pounding the streets of Paris and Milan in search of the latest celebrated fashion must-have, circling the floor at a London trade show or negotiating design prices in Shanghai, your international <a title="conference calls" href="<?php echo url_for('@conference_call'); ?>">conference call</a> from your mobile will not cost you the world with Powwownow Premium. When you're out and about and conference calling from a mobile phone, you'll want to avoid the high call charges usually associated with dialling 0844 numbers - our Premium service allows you to dial-in to your call using a standard landline telephone number, making conference calls on the move much more cost effective.</p>
                <p>With Powwownow you don’t pay a penny more for your audio conference than the cost of your own call, so now you can connect to clients, colleagues, customers and contractors in all four corners of the world without leaving home which can net you a considerable saving in travel costs alone.</p>
                <p>Powwownow Premium also allows your client to dial-in to calls on our dedicated Freephone number which means you will pay for the cost of their call for them - what potential client wouldn't be impressed by you trying to save them money?</p>
                <p>Our teleconferencing service is entirely reservationless, meaning that you can hold your <a title="free conference call" href="<?php echo url_for('@free_conference_call'); ?>">free conference call</a> at any time of the day or night without having to pre-book your call. When the news of the new must-have product hits the stands, being able to immediately pick up the phone and hold an ad hoc conference call with all your key decision makers can put you ahead of your competitors and make sure you’re the first to get your hands on the next big thing.</p>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>