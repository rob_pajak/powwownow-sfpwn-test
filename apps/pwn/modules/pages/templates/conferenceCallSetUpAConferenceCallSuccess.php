<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array('Conference Call', 'Set Up A Conference Call'),
                'breadcrumbsTitle' => __('Set Up A Conference Call'),
                'headingTitle' => __('Set Up A Conference Call'),
                'headingSize' => 'xl',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <p>The ability to hold <a title="conference call" href="<?php echo url_for('@conference_call'); ?>">conference calls</a> at a moment’s notice is becoming increasingly valuable in the modern working world. With many companies spread out across several sites and a market that has become truly global, being able to hold large meetings without the need to travel is essential to most businesses. If you’re unfamiliar with conference calling services, you might be uneasy about the process of setting up your first call, but with Powwownow it couldn't be simpler!</p>
            <p>To begin, head to Powwownow.co.uk and enter your email address into the box beneath “Get started now!”; then read and accept the terms and conditions and click the “Generate PIN” button. This will quickly and easily register you to use our free <a title="voice conferencing" href="<?php echo url_for('@voice_conferencing'); ?>">voice conferencing</a> services – we’ll email you a UK dial-in number and your secure PIN. The only other thing you’ll need to get started is a telephone, preferably with a conference calling headset. You won’t need to buy any additional conference call hardware or sign any contracts – just grab your PIN, your dial-in number, a phone and you’re good to go!</p>
            <p>With your dial-in number and PIN in hand, share these with your participants and let them know the time and date of the teleconference. If you have international participants attending your phone meeting, you will need to give them the&nbsp;<a title="international conference call" href="<?php echo url_for('@international_conference_call'); ?>">international conference call</a> dial-in number appropriate to their location.</p>
            <p>Another quick and easy way to do all of this is to use the Powwownow conference call Scheduler, where you can enter the details of your call, a message explaining what the meeting will be about and your list of attendees. We’ll then automatically email them the dial-in number and PIN for the call and send a reminder message to all of your participants at a time of your choosing.</p>
        </div>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
