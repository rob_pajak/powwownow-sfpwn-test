<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('Tell A Friend'),
                'headingTitle' => __('Tell A Friend'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix">
            <h2 class="rockwell light green margin-bottom-20">Conference calling worth shouting about!</h2>
            <p>Do you have a friend or colleague that might be interested in free and <a title="Easy Conference Calls" href="<?php echo url_for('@free_conference_call'); ?>">easy conference calls</a> using Powwownow?</p>
            <p>That's great! Just enter your details and your friend's details and we'll do the rest.</p>
            <p class="margin-bottom-10">Thank you for spreading the word!</p>
        </div>
        <div class="form-tell-a-friend-container clearfix">
            <?php echo form_tag(url_for('@tell_a_friend_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-tell-a-friend', 'class' => 'pwnform clearfix')); ?>
                <?php $count = 1; ?>
                <?php foreach ($fields as $field) : ?>
                    <?php include_partial('tellAFriendFormField', array(
                            'field' => $field,
                            'form'  => $form,
                            'count' => $count
                        )); ?>
                    <?php $count = (2==$count) ? 1 : 2 ?>
                <?php endforeach; ?>
                <div class="grid_sub_24 clearfix">
                    <div class="form-action">
                        <p class="white">* Required fields</p>
                        <button id="btn-tellafriend" type="submit" class="button-orange">
                            <span>Send</span>
                        </button>
                    </div>
                </div>
            </form>
            <div class="tell-a-friend-success-message">
                <h2 class="white">Thank you</h2>
                <p class="white">Your email has been sent. If you wish to tell another friend about us, please click <a href="" onclick="window.reload();" class="white">here</a>.</p>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($) {
            'use strict';
            $(function() {
                pwnApp.tellAFriend.init('#form-tell-a-friend');
            });
        })(jQuery);
    </script>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
