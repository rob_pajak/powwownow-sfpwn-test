<?php
/**
 * pages actions.
 *
 * @package    powwownow
 * @subpackage imeet
 * @author     Jack Adams
 *
 */
class imeetActions extends sfActions
{
    /**
     * index [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Jack Adams
     */
    public function executeIndex(sfWebRequest $request) {
        $this->url = $request->getParameter('url');
        $this->password = $request->getParameter('password');

        if(urldecode(base64_decode($this->password)) == 1) {
            $this->url = "MQ%3D%3D";
        }
    }
}