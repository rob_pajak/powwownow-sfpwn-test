<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
<div class="grid_24 clearfix imeet" id="imeet">
<section class="grid_24 video-cont" id="video_cont">
    <div class="grid_13">
        <h1 class="header-title">Free Screen Sharing and<br />Video Calling with iMeet</h1>
        <h3 class="header-sub"><strong>It's the smart and easy way to connect and collaborate</strong></h3>
        <h3 class="header-sub">Visit your unique meeting room and follow these 3 simple steps:</h3>
        <div class="grid_24 head-list">
            <div class="grid_22 prefix_2 row1">
                <h3 class="item"><strong>Connect</strong> to your conference by dialling in with your phone</h3>
            </div>
            <div class="grid_22 prefix_2 row2">
                <h3 class="item"><strong>Invite</strong> participants to join your room</h3>
            </div>
            <div class="grid_22 prefix_2 row3">
                <h3 class="item"><strong>Share</strong> your screen with your participants.</h3>
            </div>

        </div>
    </div>
    <div class="grid_11">
        <img src="/sfimages/imeet/iMeet-video-placeholder.jpg" class="imeet-video-trigger" id="imeet-video" alt="Click to view video" title="Click to view video" />
        <?php if(urldecode(base64_decode($url)) != 1):
            include_partial('tryImeet', array('url' => $url, 'password' => $password));
        endif; ?>
    </div>
</section>

<section class="grid_24 imeet-cta" id="share_url">No Downloads for Participants. Just Share your Room URL.</section>

<section class="grid_24 share-screen" id="share_screen">
    <div class="grid_12">
        <img src="/sfimages/imeet/imeet-screen-sharing.png" style="margin: 5% 0; width: 95%;" alt="iMeet Screen Sharing" />
    </div>
    <div class="grid_12 content">
        <h1 class="content-header">Screen sharing brings your calls to life </h1>
        <h3 class="content-sub">Collaborate and share documents and presentations in real-time with anyone, anywhere in the world.</h3>
        <p class="content-list"><strong>Simple to use: </strong> A simple download and everyone can see your screen and what you're talking about.</p>
        <p class="content-list"><strong>Crystal clear picture: </strong> Guests will see everything clearly, keeping you all on the same page and your meeting flowing.</p>

        <div class="grid_18 did-you-know">
            <div class="grid_4">
                <img src="/sfimages/imeet/imeet-screen-share-icon.svg" class="imeet-share-icon" onerror="this.src='/sfimages/imeet/imeet-screen-share-icon.png';this.onerror=null;" alt="Share" />
            </div>
            <div class="prefix_1 grid_18 suffix_1">
                <h3>Did you know?</h3>
                <p>You can edit and work on the same document, as if you were in the same room.</p>
            </div>
        </div>
    </div>
</section>

<section class="grid_24 on-the-go" id="on_the_go">
    <div class="grid_12">
        <h1 class="content-header2">Use it on the go</h1>
        <p>iMeet is available on Apple and Android devices as well as desktop, meaning you can jump on a call wherever you are.</p>
        <div class="grid_8" style="margin-left: 10px;">
            <a href="https://itunes.apple.com/gb/app/imeet/id456757257" target="_blank" title="App Store">
                <img src="/sfimages/imeet/download-app-store.svg" onerror="this.src='/sfimages/imeet/download-app-store.png';this.onerror=null;" alt="App Store" />
            </a>
        </div>
        <div class="grid_8">
            <a href="https://play.google.com/store/apps/details?id=com.imeet" target="_blank" title="Google Play">
                <img src="/sfimages/imeet/download-google-play.svg" onerror="this.src='/sfimages/imeet/download-google-play.png';this.onerror=null;" alt="Google Play" />
            </a>
        </div>
    </div>
    <div class="grid_12">
        <img src="/sfimages/imeet/imeet-mobile-devices.png" style="margin: 0 5%" alt="iMeet is available on mobile devices" />
    </div>
</section>

<section class="grid_24 video-calling" id="video_calling">
    <div class="grid_12">
        <img src="/sfimages/imeet/imeet-video-boxes.jpg" style="margin: 5% 20% 5% 0; width: 80%;" alt="iMeet profiles" />
    </div>
    <div class="grid_12 content">
        <h1 class="content-header">Video makes calls more engaging</h1>
        <h3 class="content-sub">Improve productivity by collaborating face-to-face with one or multiple guests as if you were in the same room.</h3>
        <p class="content-list"><strong>Easy to set up: </strong> In just one click your webcam will be activated.</p>
        <p class="content-list"><strong>HD Quality: </strong> See everyone clearly, without the interruptions of poor quality video.</p>

        <div class="grid_18 did-you-know">
            <div class="grid_4">
                <img src="/sfimages/imeet/imeet-video-icon.svg" class="imeet-video-icon" onerror="this.src='/sfimages/imeet/imeet-video-icon.png';this.onerror=null;" alt="iMeet Video" />
            </div>
            <div class="prefix_1 grid_18 suffix_1">
                <h3>Did you know?</h3>
                <p>All you need is a webcam and you can video call with up to 5 guests.</p>
            </div>
        </div>
    </div>
</section>

<section class="grid_24 take-notes" id="take_notes">
    <div class="grid_15 bubble">
        <h1 class="content-header2">Take notes during your call</h1>
        <p>Capture notes collectively whilst on the call, these can then be emailed or exported to Evernote and shared with your meeting participants.</p>
    </div>
    <div class="grid_9">
        <img src="/sfimages/imeet/notes-image.png" alt="iMeet Notes" />
    </div>
</section>



<section class="grid_24 share-files" id="share_files">
    <div class="grid_12 content">
        <h1 class="content-header">File sharing is simple, safe and forever</h1>
        <h3 class="content-sub">Upload, share and present files to your guests safe in the knowledge that your room is unique to you.</h3>
        <p class="content-list"><strong>250MB storage: </strong> There's plenty of space to store your files and documents and they're always available.</p>
        <p class="content-list"><strong>Complete control: </strong> You decide what people see and when, so no one can skip ahead.</p>
        <div class="grid_18 did-you-know">
            <div class="grid_4">
                <img src="/sfimages/imeet/imeet-pen-icon.svg" class="imeet-pen-icon" onerror="this.src='/sfimages/imeet/imeet-pen-icon.png';this.onerror=null;" alt="iMeet Pen Tool" />
            </div>
            <div class="prefix_1 grid_18 suffix_1">
                <h3>Did you know?</h3>
                <p>You can annotate documents and files using the pen tool.</p>
            </div>
        </div>
    </div>

    <div class="grid_12">
        <img src="/sfimages/imeet/imeet-file-sharing.jpg" style="margin: 6% 0 0 3%; width: 97%;" alt="iMeet File Sharing" />
        <?php if(urldecode(base64_decode($url)) != 1):
            include_partial('tryImeet', array('url' => $url, 'password' => $password));
        endif; ?>
    </div>
</section>

<section class="grid_24 imeet-cta" id="contact_us">Need a hand? Email us at <a href="mailto:imeet@powwownow.com" target="_blank" title="Email Us">imeet@powwownow.com</a> to speak to one of our iMeet specialists.</section>

<?php include_component('commonComponents', 'footer'); ?>
</div>
</div>

<div class="youtube-modal" id="youtube_modal">
    <iframe class="youtube-modal-iframe" width="853" height="480" src="about:blank" data-src="//www.youtube.com/embed/8qRGKeiaidA?rel=0&amp;autoplay=1" allowfullscreen>Please enable iFrames to view this video</iframe>
    <div id="youtube_modal_close" class="close"></div>
</div>