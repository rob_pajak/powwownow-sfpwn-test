<div class="grid-24 try-imeet-container">
    <h3 class="try-imeet-header">Try iMeet for free</h3>
    <article class="grid_24 try-imeet">
        <p class="try-imeet-details"><strong>Enter your iMeet room: </strong><a href="<?php echo urldecode(base64_decode($url)); ?>" target="_blank" title="Enter your iMeet room">click here</a></p>
        <p class="try-imeet-details"><strong>Password:</strong> <?php echo urldecode(base64_decode($password)); ?></p>
    </article>
</div>