<div class="grid_24 clearfix meeting-calculator">
    <div class="meeting-calculator-first-layout clearfix">
        <div class="header">
            <h1>Meeting Cost Calculator</h1>
            <p>If you want to know just how much face-to-face meetings cost your company, why not use our handy Meeting Cost Calculator. In just six simple steps, the Meeting Cost Calculator will tell you how much each meeting costs your company and how much could have been saved using conference calls. All you need to do is input information about yourself and a meeting you've recently attended and hey presto, all is revealed.</p>
        </div>
        <div class="grid_4 meeting-calculator-progress">
            <h2>
                Total Cost
                <strong>&pound;<span class="total-cost"></span></strong>
            </h2>
            <div class="meeting-calculator-progress-div">
                <span class="progress-bar">Progress</span>
            </div>
        </div>
        <div class="grid_19 meeting-calculator-form">
            <form action="#">
                <fieldset class="annual-wage-container">
                    <h2>Your annual salary</h2>
                    <p>Add your annual salary, plus any colleagues in attendance, into the box below</p>
                    <p>
                        <input type="text" class="annual-wage-input" name="" placeholder="0" />
                        <button class="add-colleague">Add colleague</button>
                        <label class="error">Please enter your annual salary</label>
                    </p>
                </fieldset>
                <fieldset>
                    <h2>Time spent in a meeting</h2>
                    <div class="margin-top-20">
                        <div id="hourMeetingSlider" class="meeting-calculator-slider"></div>
                        <span><span class="meeting-hours"></span> hour<span class="meeting-hour-plural"></span></span>
                    </div>
                    <h2>Time spent travelling to meeting</h2>
                    <div class="margin-top-20">
                        <div id="hourTravelSlider" class="meeting-calculator-slider"></div>
                        <span><span class="travel-hours"></span> min</span>
                    </div>
                    <h2>Cost of travel</h2>
                    <p>Train, taxi, bus, car, commercial plane, hovercraft etc</p>
                    <div>
                        <div id="costTravelSlider" class="meeting-calculator-slider"></div>
                        <span>&pound;<span class="travel-cost"></span></span>
                    </div>
                    <h2>Cost of food and drink</h2>
                    <p>Catering for meeting, restaurant, café etc</p>
                    <div>
                        <div id="cateringCostSlider" class="meeting-calculator-slider"></div>
                        <span>&pound;<span class="catering-cost"></span></span>
                    </div>
                </fieldset>
                <fieldset>
                    <h2>Additional expenses</h2>
                    <ul>
                        <li>
                            <label for="coffee-tea">
                                <input type="checkbox" name="coffee-tea" id="coffee-tea" data-cost="2" />
                                Coffee/tea on-the-go
                            </label>
                        </li>
                        <li>
                            <label for="magazine">
                                <input type="checkbox" name="magazine" id="magazine" data-cost="2" />
                                Magazine
                            </label>
                        </li>
                        <li>
                            <label for="cold-drink">
                                <input type="checkbox" name="cold-drink" id="cold-drink" data-cost="1.5" />
                                Cold drink on-the-go
                            </label>
                        </li>
                        <li>
                            <label for="newspaper">
                                <input type="checkbox" name="newspaper" id="newspaper" data-cost="1" />
                                Newspaper
                            </label>
                        </li>
                        <li>
                            <label for="chocolate">
                                <input type="checkbox" name="chocolate" id="chocolate" data-cost="1" />
                                Chocolate/snacks
                            </label>
                        </li>
                        <li>
                            <label for="chewing-gum">
                                <input type="checkbox" name="chewing-gum" id="chewing-gum" data-cost="0.6" />
                                Chewing gum
                            </label>
                        </li>
                        <li>
                            <label for="stationary">
                                <input type="checkbox" name="stationary" id="stationary" data-cost="3" />
                                Stationary
                            </label>
                        </li>
                        <li>
                            <label for="umbrella">
                                <input type="checkbox" name="umbrella" id="umbrella" data-cost="5" />
                                Umbrella
                            </label>
                        </li>
                    </ul>
                </fieldset>
                <button class="calculate-savings-btn">CALCULATE SAVINGS</button>
            </form>
        </div>
    </div>
    <div class="meeting-calculator-second-layout">
        <div class="header">
            <h1>Meeting Cost Calculator</h1>
            <p>Voila! All those cups of coffee and time spent out off the office add up, don't they? So, avoid traffic jams and missed trains and the dreaded piles of receipts by incorporating conference calling and screen sharing into your day! It will help save you time and money… we promise.</p>
        </div>
        <div class="meeting-calculator-results">
            <h2>Your meeting cost</h2>
            <p>
                <strong class="pound">&pound;<span class="total-cost"></span></strong>
            </p>
            <div class="savings">
                <h2>You could have saved </h2>
                <p>
                    <strong class="pound">&pound;<span class="total-saved"></span></strong><br />
                    by using <strong>Powwownow</strong> where you only pay the cost of an 0844 call <br> (based on 4.3p per minute + VAT from a BT Landline – that’s all*)
                </p>
            </div>
            <div class="footer">
                <h3>Save money on your next meeting</h3>
                <a class="generate-pin-btn" href="<?php echo url_for("@homepage"); ?>#register">GENERATE YOUR PIN NOW</a>
                <p>
                    <a href="#" class="calculate-another-meeting">Calculate another meeting</a>
                </p>
                <p>
                    <small>
                        *Call charges may vary depending on your network provider
                    </small>
                </p>
        </div>
        </div>
    </div>
</div>
