<a href="<?php echo url_for('@homepage'); ?>" id="logolink"><?php
    if ($publicPage) :
        include_partial('commonComponents/headerLogo', array('logo' => $logos['UNAUTHENTICATED']));
    elseif (!empty($logos[$serviceUser])) :
        include_partial('commonComponents/headerLogo', array('logo' => $logos[$serviceUser]));
    endif;
?></a>
