<?php if (sfContext::getInstance()->getRequest()->getCookie('cookie_policy_accepted', false) != true): ?>
    <?php $template = isset($template) ? $template : 'default'; ?>
    <?php if ($template === 'default'): ?>
        <div id="cookie-policy">
            <style scoped>
                .cookie-policy {
                    padding: 0 15px 10px 15px;
                    margin: 0 0 10px 0;
                    background: #A6CF5A;
                }
                .cookie-policy .close {
                    color: #ffffff;
                    border: none;
                    float: right;
                    cursor: pointer;
                    font-size: 14px;
                }
            </style>
            <div class="cookie-policy clearfix">
                <div class="container_24">
                    <div class="grid_23">
                        <p class="text-left white">We use cookies to give you the best online experience. By using our website you agree to our use of cookies in accordance with our cookie policy. <a class="white" href="<?php echo url_for('@privacy'); ?>#cookies">Learn more</a>.</p>
                    </div>
                    <div class="grid_1">
                        <p class="white"><a class="close" title="Close">&#215;</a></p>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($template === 'foundation-5-layout'):?>
        <section id="cookie-policy" class="cookie-container">
            <div class="row">
                <div class="small-12 medium-12 large-12 columns">
                    <p class="text-left white">We use cookies to give you the best online experience. By using our
                        website you agree to our use of cookies in accordance with our cookie policy.
                        <a class="white" href="<?php echo url_for('@privacy'); ?>#cookies">Learn more</a>.
                        <a class="close" title="Close">&#215;</a>
                    </p>
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php else: ?>
    <?php sfContext::getInstance()->getResponse()->setCookie('cookie_policy_accepted', true, date('Y-m-d h:i:s', strtotime('+1500 days')), '/'); ?>
<?php endif; ?>