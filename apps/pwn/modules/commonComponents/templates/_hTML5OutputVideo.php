<?php if (!$showBlank) : ?>
<script type="text/javascript" src="/shared/SWFObject/swfobject.js"><?php echo ($called === 'html') ? "</script>" : "<\/script>"; ?>
<script type="text/javascript">
    var flashvars = {skin: "/sfswf/mySkin.swf", video: "<?php echo $video['mp4']; ?>", play: "true"};
    var params = {};
    var attributes = {};
    attributes.id = "flashContent";
    swfobject.embedSWF("/sfswf/player.swf?v1.3.5", "<?php echo $config['playerID']; ?>", "<?php echo $config['width']; ?>", "<?php echo $config['height']; ?>", "10.0.0", false, flashvars, params, attributes);
<?php echo ($called === 'html') ? "</script>" : "<\/script>"; ?>

<video class="pwn-video-helper" width="<?php echo $config['width']; ?>" height="<?php echo $config['height']; ?>" <?php echo ('' !== $video['image']) ? 'poster="' . $video['image'] . '"' : '' ; ?> <?php echo $config['controls']; ?> <?php echo $config['autoplay']; ?> preload="<?php echo $config['preload']; ?>">
    <source type="video/mp4" src="<?php echo $video['mp4']; ?>">
    <source type="video/webm" src="<?php echo $video['webm']; ?>">
    <source type="video/ogg" src="<?php echo $video['ogg']; ?>">

    <div id="<?php echo $config['playerID']; ?>">
        <p>
            <strong>You do not have Flash installed, or it is older than the required 10.0.0.</strong><br>
            <strong>Click below to install the latest version and then try again.</strong><br>
            <a target="_blank" href="//www.adobe.com/go/getflashplayer">
                <img src="//www.adobe.com/misc/images/160x41_get_flashplayer.gif" width="112" height="33" alt="Get Adobe Flash player">
            </a>
        </p>
    </div>
</video>
<?php endif; ?>