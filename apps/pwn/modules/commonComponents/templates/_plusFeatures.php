<?php if ($page == 'plusService') : ?>
    <div class="clearfix">
        <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
        <div class="grid_sub_21">
            <p>
                <span class="rockwell font-bigger">Money-saving minute Bundles</span>
                <br/>Save money on regular conference calls with our range of <?php echo link_to('monthly minute Bundles','productSelectorTool/callMinuteBundles'); ?>. We’ve a variety of packages available so have one to suit every business!
            </p>
        </div>
    </div>
    <div class="clearfix">
        <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
        <div class="grid_sub_21">
            <p>
                <span class="rockwell font-bigger">A personal touch</span>
                <br/>Individually recorded to your specifications, there's no better way to give your conference calls a professional shine than by adding a Branded Welcome Message.
            </p>
        </div>
    </div>
    <div class="clearfix">
        <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
        <div class="grid_sub_21">
            <p>
                <span class="rockwell font-bigger">Real-time reporting</span>
                <br/>Monitor how your account is being used by running reports to monitor call Participants, see how many people were on particular calls and how long the calls were.
            </p>
        </div>
    </div>
    <div class="clearfix">
        <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
        <div class="grid_sub_21">
            <p>
                <span class="rockwell font-bigger">Instant conference call recording</span>
                <br/>Record your conference calls with the touch of a button. Then after your call you can review, download and share your recording with your colleagues and keep them for your records.
            </p>
        </div>
    </div>
    <div class="clearfix">
        <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
        <div class="grid_sub_21">
            <p>
                <span class="rockwell font-bigger">Dedicated customer support</span>
                <br/>We are proud to have some of the best customer support in the industry. That's why 98% of our customers would recommend us.
            </p>
        </div>
    </div>
    <div class="clearfix">
        <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
        <div class="grid_sub_21">
            <p>
                <span class="rockwell font-bigger">Free instant web conferencing</span>
                <br/>You can now integrate your conference calls with web conferencing and it is completely free. This means you can instantly share your computer screen with your call Participants (perfect for sharing documents or presentations). There is no need for call Participants to download or install any software – all they need is internet access.
            </p>
        </div>
    </div>
<?php elseif ($page == 'callMinuteBundle') : ?>
    <div class="grid_24 clearfix margin-top-10">
        <h4 class="font-bigger">Still need convincing...</h4>
    </div>
    <div class="grid_24">
        <div class="grid_24  margin-top-10">
            <div class="grid_sub_12">
                <div class="grid_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
                <div class="grid_21">
                    <p style="text-align: justify;">
                        <span class="rockwell font-bigger">Money saving</span>
                        <br/>
                        Using a Landline Bundle can make individuals and businesses amazing savings, with rates working out as low as 1.4pence per minute!
                    </p>
                </div>
            </div>
            <div class="grid_sub_12">
                <div class="grid_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
                <div class="grid_21">
                    <p style="text-align: justify;">
                        <span class="rockwell font-bigger">No long contract or cancellation fee!</span>
                        <br/>All contracts are on a monthly-rolling basis with no cancellation fee.  This makes it possible to upgrade or downgrade between Bundles.
                    </p>
                </div>
            </div>
        </div>
        <div class="grid_sub_12">
            <div class="grid_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
            <div class="grid_21">
                <p style="text-align: justify;">
                    <span class="rockwell font-bigger">Immediate activation</span>
                    <br/>There’s no waiting around to use a Bundle. Simply purchase and start saving money on your calls immediately.
                </p>
            </div>
        </div>

        <div class="grid_sub_12 clearfix margin-top-10">
            <div class="grid_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
            <div class="grid_21">
                <p style="text-align: justify;">
                    <span class="rockwell font-bigger">Need international numbers? </span>
                    <br/>
                    Simply purchase our International Add-On to gain access to over 50 worldwide Landline numbers to include in your Bundle minute allowance.
                </p>
            </div>
        </div>

        <div class="grid_sub_12 margin-top-10">
            <div class="grid_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
            <div class="grid_21">
                <p style="text-align: justify;">
                    <span class="rockwell font-bigger">What is a Landline (eg 03…) number?</span>
                    <br/>
                    Landline numbers are perfect for conference calling on the move as they’re included in most mobile contracts.
                    These numbers are often included in landline call packages, allowing free or low cost calls.
                </p>
            </div>
        </div>
        <div class="grid_sub_12 clearfix margin-top-10">
            <div class="grid_3"><img src="/sfimages/list-yellow-arrow.png" alt=""/></div>
            <div class="grid_21">
                <p style="text-align: justify;">
                    <span class="rockwell font-bigger">Don’t want to pay for your participants calls?</span>
                    <br/>
                    As the chairperson of the call you dial the Landline (eg 03…) number and share the Shared Cost (0844…) number with your participants, so they pay for their own call.
                </p>
            </div>
        </div>
    </div>
<?php endif; ?>