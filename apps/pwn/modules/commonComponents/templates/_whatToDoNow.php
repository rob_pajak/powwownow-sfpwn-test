<?php use_stylesheet('/sfcss/components/commonComponents.whatToDoNow.css'); ?>
<?php use_javascript('/sfjs/mobile-detect.min.js'); ?>
<?php use_javascript('/sfjs/components/commonComponents.rightLandingPageMenu.js'); ?>
<div class="what-to-do-now">
    <h2 class="rockwell white">What do you want to do now?</h2>

    <ul id="do-now-list" class="do-now-list" style="height:auto">
        <li class="png">
            <span class="icon icon-grey no-shadow png"><span class="icon-outlook-white"></span></span>
            <?php include_component(
                'commonComponents','outlookPluginTooltip',
                array(
                    'title' => 'Outlook Plugin',
                    'id' => 'top-outlook-plugin',
                    'classes' => array(
                        'font' => 'white',
                    ),
                    'css' => array(
                        'left' => '-75px',
                        'color' => '#515151'
                    )
                )
            ); ?>
        </li>
        <li class="png">
            <span class="icon icon-grey no-shadow png"><span class="icon-phone-small-white"></span></span>
            <a href="<?php echo url_for('@call_settings'); ?>" class="white" target="_self">Manage my call settings</a>
        </li>

        <li class="png">
            <span class="icon icon-grey no-shadow png"><span class="icon-play-pause-white"></span></span>
            <a href="<?php echo url_for('@in_conference_controls'); ?>" class="white" target="_self">View in-conference controls</a>
        </li>
        <li class="png">
            <span class="icon icon-grey no-shadow png"><span class="icon-calender-white"></span></span>
            <a href="<?php echo url_for('@schedule_a_call'); ?>" class="white" target="_self">Scheduler tool</a>
        </li>
        <li class="png">
            <span class="icon icon-grey no-shadow png"><span class="icon-arrow-white"></span></span>
            <a href="<?php echo url_for('@web_conferencing_auth'); ?>" class="white" target="_self">Hold a web conference</a>
        </li>

        <li class="png">
            <span class="icon icon-grey no-shadow png"><span class="icon-globe-white"></span></span>
            <a href="<?php echo url_for('@dial_in_numbers'); ?>" class="white" target="_self">Discover international access</a>
        </li>
    </ul>
</div>
