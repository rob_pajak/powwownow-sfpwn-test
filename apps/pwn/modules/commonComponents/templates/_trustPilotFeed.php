<?php if (is_object($trustPilotFeed)) : ?>
<div itemtype="http://schema.org/Product" itemscope=""<?php echo ($showReviews) ? '' : ' style="display: none;"'; ?>>
    <span itemprop="name"<?php echo (!$showReviews) ? '' : ' style="display: none;"'; ?>><?php echo $name; ?></span>
    <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        Powwownow is rated <span itemprop="ratingValue"><?php echo number_format($trustPilotFeed->TrustScore->Score / 20, 1); ?>/5 based on <span itemprop="reviewCount"><?php echo $maxReviews; ?> reviews from the last 12 months.</span></span>
        <a href="http://www.google.co.uk/shopping/seller?q=powwownow.co.uk" target="_self" title="Read our reviews">Read our reviews</a>.
        <a href="https://plus.google.com/103373815892911340998?rel=author"<?php echo (!$showReviews) ? '' : ' style="display: none;"'; ?>>Google</a>
    </div>
</div>
<?php endif; ?>
