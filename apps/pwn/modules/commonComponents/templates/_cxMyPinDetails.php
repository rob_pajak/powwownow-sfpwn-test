<section class="my_pin_details">
    <div class="background background_blue clearfix">
        <div class="floating_element_or">OR</div>
        <ul class="pwn_list_cx_style first">
            <li class="title">Call from <br />a UK landline</li>
            <li class="body" id="cx_dial_in_number"><?php echo $dialInNumber;?></li>
            <li class="small_print">Everyone pays from 4.3p per minute from a BT landline using the 0844 number.</li>
        </ul>

        <ul class="pwn_list_cx_style">
            <li class="title">Call from a <br /> UK Mobile</li>
            <li class="body" id="cx_mobile_dial_in_number"><?php echo $mobileDialInNumber;?></li>
            <li class="small_print">Everyone from a UK mobile using 87373 pays only 12.5p per minute.</li>
        </ul>

        <ul class="pwn_list_cx_style last">
            <li class="title">And enter your PIN</li>
            <li class="body" id="cx_generated_pin"><?php echo $pin; ?></li>
            <li class="small_print small_print_rockwell">(Keep me safe...<br /> I'm yours forever.)</li>
        </ul>

        <div class="international clearfix">
            <p>
                <span class="icons-sprite icons-phone"></span>
                <span class="sprite_text">Are you calling from abroad? <a target="_blank" href="<?php echo url_for('@dial_in_numbers');?>">Get your international numbers here</a></span>
            </p>
        </div>

        <div class="grid_7 my_account">
            <ul class="pwn_list_basic">
                <li>
                    <span class="font_green font_heading">Email:</span>
                    <span id="cx_email"><?php echo $email; ?></span></li>
                <li><span class="font_green font_heading">Password:</span> <a target="_blank" href="<?php echo url_for('@account_details')?>">Change it now</a></li>
            </ul>
        </div>
        <div class="grid_4 miscellaneous">
            <p>
                Don't worry we've just sent you an email with your username and password, however we know you're a
                'get-it-done' kind of person so you can change it now.
            </p>
        </div>
    </div>
</section>