<!--Header - Start -->
<?php if (!$sf_user->isAuthenticated()):?>
    <!-- CX ONE CLICK REGISTRATION FORM START -->
    <section id="cx_registration" class="pwn_custom_grid_header clearfix" data-registration-element-state="closed">
        <section class="container_24 clearfix cx_registration">
            <div class="grid_sub_8">
                <h2 class="cx_header_1">One click sign-up</h2>
                <p>to start conference calling...</p>
            </div>
            <div class="grid_sub_16">
                <?php include_component(
                    'commonComponents',
                    'jsKoCxRegistrationTemplate',
                    array(
                        'koTemplateId' => 'ko.header.registration.container',
                        'koTemplateDataId' => 'ko.header.registration.data',
                        'isLegacyCss' => false
                    )
                ); ?>
            </div>
            <span id="close_cx_registration" class="close" title="Close">&#215;</span>
        </section>
    </section>
    <!-- CX ONE CLICK REGISTRATION FORM END -->
<?php endif; ?>

<header class="container_24 clearfix header">
    <!-- LOGOS START -->
    <section class="grid_sub_10 logos">
        <?php if ($isPremiumUser && isset($customerLogo)):?>
            <div class="customer_logo">
                <a href="<?php echo url_for('@homepage')?>">
                    <?php echo $sf_data->getRaw('customerLogo'); ?>
                </a>
            </div>
        <?php else:?>
            <a href="<?php echo url_for('@homepage')?>">
                <img src="/cx2/img/mobile/640_logo_strap_line.png" width="200" alt="Powwownow" />
            </a>
        <?php endif?>
    </section>
    <!-- LOGOS END -->

    <section class="grid_sub_14">
        <section class="pwn_authentication_widget">
            <?php if ($isPublicPage):?>
                <?php include_component('commonComponents', 'widgetRegionSelector'); ?>
            <?php endif; ?>
            <?php if ($sf_user->isAuthenticated()):?>
                <div class="<?php echo ($isPremiumUser ? 'premium_user' : '');?> <?php echo ($isPublicPage ? 'authenticated_public' : 'authenticated_secure');?>">
                    <?php if ($isPublicPage):?>
                        <p class="hello public_page"></p>
                        <ul class="mypowwownow_actions">
                            <li>
                                <a href="<?php echo url_for('@mypwn_index')?>" class="cx_button cx_button_blue_c">myPowwownow</a>
                            </li>
                            <li>
                                <a href="<?php echo url_for('@authenticate_logout_new')?>" class="logout">Logout</a>
                            </li>
                        </ul>
                    <?php else: ?>
                        <p class="hello">
                            <?php if ($sf_user->getAttribute('first_name') !== '') : ?>
                                Hello <span class="font_verdana"><i><?php echo $sf_user->getAttribute('first_name') ?></i> and
                            <?php else:?>
                                Hello and <span class="font_verdana">
                            <?php endif?>
                            </span>
                            welcome to<br />
                            <span class="mypowwownow_branding">myPowwownow</span>
                        </p>
                        <p>
                            <a href="<?php echo url_for('@authenticate_logout_new')?>" class="logout">Logout</a>
                        </p>
                    <?php endif?>
                </div>
            <?php else: ?>
                <ul class="mypowwownow_actions">
                    <li>
                        <button id="cx_sign_up_button" class="cx_button cx_button_orange">Sign up FREE</button>
                    </li>
                    <li>
                        <a class="login" href="<?php echo url_for('@login')?>">Login</a>
                    </li>
                </ul>
            <?php endif; ?>
        </section>
    </section>
    <section class="navigation grid_sub_24 clearfix">
        <?php include_partial('commonComponents/headerNavigation', $headerNavigationArgs); ?>
    </section>
</header>

<div class="container_24 clearfix">
    <?php
    if (isset($showPinBar)) {
        /**
         * The below bit of logic is a dirty hack. New CX2 requires us hiding this pinPairBar component.
         * Simply turning $publicPage to TRUE does not work, as we lose styling related to that type of
         * customer. To make this work correctly is epic.
         */
        if ($showPinBar === false) {
            $isPublicPage = true;
        }
    }
    ?>

    <?php if (!$isPublicPage) : ?>
        <!--Sub Header / PIN Pair Bar-->
        <?php include_component('commonComponents', 'pinPairBar'); ?>
    <?php endif; ?>
</div>


