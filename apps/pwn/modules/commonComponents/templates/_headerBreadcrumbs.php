<?php
   $c[] = link_to('Home', '/myPwn/');

   $breadcrumbs = $sf_data->getRaw('breadcrumbs');

   if (is_array($breadcrumbs)) {
      foreach ($breadcrumbs AS $b) {
         $c[] = is_array($b) ? link_to($b[0], $b[1]) : $b;
      }
   }

   if (substr($c[count($c) - 1], 1, 1) == 'a') {
      $c[] = $title;
   }

    $fullClasses = array();
    if (isset($classes)) {
        $fullClasses[] = $classes;
    }
    $gridSize = 12;
    if (isset($grid)) {
        $gridSize = $grid;
    }
    $fullClasses[] = "grid_$gridSize";

    $classes = implode(' ', $fullClasses);
?>
<div class="<?php echo $classes ?>" style="<?php echo isset($style) ? $style : 'text-align:right;' ?>">
   You are in: <?php echo implode(' > ', $c); ?>
</div>