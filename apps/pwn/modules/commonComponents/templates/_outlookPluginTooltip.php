<div class="display-inline-block href-container">
    <a id="<?php echo $elementId?>-link" class="<?php echo $fontClass?>" href="#" onclick="dataLayer.push({'event': 'Schedule a call/onClick'});"><?php echo $hrefTitle ?></a>
</div>
<div id="<?php echo $elementId?>" class="tooltip <?php echo $elementId?>-class tooltip-outlook">
    <style scoped>
        .<?php echo $elementId?>-class {
        <?php foreach ($css as $key => $value) :?>
        <?php echo "$key:$value;\n"; ?>
        <?php endforeach; ?>
        }
        .<?php echo $elementId?>-class ul {
            margin-left: 0;
            padding: 0;
        }
        .<?php echo $elementId?>-class ul li {
            list-style: none;
            margin-left: 0;
            background: none;
            padding: 0;
        }

        .<?php echo $elementId?>-class .pwnform {
            padding: 0;
            background: #ffffff;
        }
    </style>
    <div class="tooltip-arrow-top png"><!--Blank--></div>
    <a id="<?php echo $elementId?>-close" class="floatright sprite tooltip-close" href="#">
        <span class="sprite-cross-white png"><!--Blank--></span>
    </a>
    <h4 class="rockwell">Schedule a call</h4>
    <p>
        If you use Microsoft<sup>&reg;</sup> Outlook<sup>&reg;</sup>,
        download our new plugin to schedule your next conference using your Outlook calendar and contacts (PC Only).
        <br/><br/>
        Already using our Plugin? Then upgrade now for new and improved features.
    </p>
    <div class="display-inline-block outlook-container-form">
        <?php include_component(
            'commonComponents',
            'outlookPlugin',
            array(
                'formID'=> $elementId.'-outlook-plugin-form'
            )
        );?>
    </div>
</div>
