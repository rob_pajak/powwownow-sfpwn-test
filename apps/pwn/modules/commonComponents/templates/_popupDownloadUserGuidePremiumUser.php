<div class="mypwn_footer_title title-<?php echo $service; ?>">
    <div style="display:none;" class="tooltip-footer-user-guides tooltip">
        <a class="tooltip-footer-user-guides-close floatright sprite tooltip-close" href="#"><span class="sprite-cross-white png"></span></a>
        <p>
            <a title="Powwownow Premium User Guide" href="/sfpdf/en/Powwownow-Premium-User-Guide-For-Users.pdf" target="_blank" style="font-weight:bold;">Powwownow Premium User Guide</a>
        </p>
        <a title="Web Conferencing Guide" href="/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" target="_blank" style="font-weight:bold;">Web Conferencing Guide</a>
        <div class="tooltip-arrow-bottom png"></div>
    </div>
</div>