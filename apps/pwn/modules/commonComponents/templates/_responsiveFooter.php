<div class="row">
    <div class="small-12 columns">
        <footer class="rounded-corners clearfix">
            <ul class="site-links">
                <li><a class="more" href="<?php echo url_for('@privacy'); ?>">Privacy</a></li>
                <li><a class="more" href="<?php echo url_for('@terms_and_conditions'); ?>">Terms & Conditions</a></li>
                <li><a class="more" href="<?php echo url_for('@about_us'); ?>">About Us</a></li>
                <li><a class="more" href="<?php echo url_for('@contact_us'); ?>">Contact Us</a></li>
                <li><a class="more" href="<?php echo url_for('@glossary'); ?>">Glossary</a></li>
                <li><a class="more" href="<?php echo url_for('@useful_links'); ?>">Useful Links</a></li>
                <li><a class="more" href="<?php echo url_for('@sitemap'); ?>">Site Map</a></li>
            </ul>


            <ul class="social-media-links">
                <li><a class="sprite blog" title="Blog" href="/blog">Blog</a></li>
                <li><a class="sprite twitter" title="Twitter" href="https://twitter.com/powwownow" target="_blank">Twitter</a></li>
                <li><a class="sprite facebook" title="Facebook" href="https://www.facebook.com/powwownow" target="_blank">Facebook</a></li>
                <li><a class="sprite linkedin" title="Linkedin" href="http://www.linkedin.com/company/powwownow" target="_blank" >Linkdin</a></li>
                <li><a class="sprite youtube" title="Youtube" href="http://www.youtube.com/user/MyPowwownow" target="_blank">Youtube</a></li>
                <li><a class="sprite google" title="Google+" href="https://plus.google.com/102480333405705865805/posts" target="_blank">Google+</a></li>
            </ul>


            <ul class="legal">
                <li class="text-center powwownow-copyright"><?php echo date('Y'); ?> Powwownow</li>
            </ul>
        </footer>
    </div>
</div>
