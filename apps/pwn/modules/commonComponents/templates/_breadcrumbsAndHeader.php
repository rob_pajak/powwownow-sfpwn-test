
<div class="header_breadcrumbs">
    <?php include_partial(
        'commonComponents/genericBreadcrumbs',
        array(
            'breadcrumbs' => $breadcrumbs,
            'title' => __($breadcrumbsTitle)
        )
    ); ?>
</div>
<div class="header_heading">
    <?php include_component(
        'commonComponents',
        'genericHeading',
        array(
            'title' => __($headingTitle),
            'headingSize' => $headingSize,
            'user_type' => $headingUserType
        )
    ); ?>
</div>
