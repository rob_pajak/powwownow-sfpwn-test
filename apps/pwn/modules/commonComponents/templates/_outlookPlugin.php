<?php if ($authenticated) : ?>
    <ul class="scheduler-links">
        <li><a id="download-plugin" class="dotted grey">Powwownow Plugin for Outlook</a></li>
        <li>Select which version of the Outlook plugin to download<small>*</small>:</li>
        <li class="button-list">
            <button type="submit" class="button-stroked button-orange" onclick="window.open('<?php echo $outlookBtnData['2003']['href']?>','_blank'); dataLayer.push({'event': 'Outlook Plugin - 2003/onClick'});">
                <span><?php echo $outlookBtnData['2003']['text']?></span>
            </button>

            <button type="submit" class="button-stroked button-orange" onclick="window.open('<?php echo $outlookBtnData['2010']['href']?>','_blank'); dataLayer.push({'event': 'Outlook Plugin - 2010/onClick'});">
                <span><?php echo $outlookBtnData['2010']['text']?></span>
            </button>
        </li>
        <li class="small-text">*To find out which version of Outlook you are using go into Outlook and select File, Help.</li>
    </ul>
    <div class="display-inline-block"><p>If you do not use Outlook, please use the Powwownow scheduler tool.</p></div>
    <ul class="scheduler-links">
        <li><a class="dotted grey" onclick="dataLayer.push({'event': 'Schedule a call - Scheduler Tool/onClick'});" href="http://myscheduler.powwownow.com/" target="_blank">Scheduler tool</a></li>
    </ul>
    <script type="text/javascript">
        (function($) {
            'use strict';
            $(function() {
                $('#download-plugin').click(function(){
                    $.ajax({
                        url: '<?php echo url_for('schedule_a_call_ajax'); ?>'
                    });
                });
            });
        })(jQuery);
    </script>

<?php else : ?>

    <ul class="scheduler-links margin-bottom-20">
        <li>
            <a class="dotted grey toggle-plugin" onclick="dataLayer.push({'event': 'Schedule a call - Plugin for Outlook - Click/onClick'}); $('#<?php echo $formID ?>').toggle();" style="cursor: pointer;">Powwownow Plugin for Outlook</a>
            <?php echo form_tag(url_for('outlook_plugin'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => $formID, 'class' => 'pwnform clearfix', 'style' => 'display:none;')); ?>
                <div class="grid_24 clearfix">
                    <div class="form-field">
                        <label for="email_address_<?php echo $formHash; ?>">Email Address *</label>
                        <span class="mypwn-input-container">
                            <input class="input-large font-small mypwn-input" type="text" name="email_address" value="" id="email_address_<?php echo $formHash; ?>"/>
                        </span>
                    </div>
                </div>
                <div class="grid_24 clearfix">
                    <div class="form-field">
                        <label for="job_title_<?php echo $formHash; ?>">Job Title</label>
                        <span class="mypwn-input-container">
                            <input class="input-large font-small mypwn-input" type="text" name="job_title" value="" id="job_title_<?php echo $formHash; ?>"/>
                        </span>
                    </div>
                </div>
                <div class="grid_24 clearfix">
                    <div class="form-field">
                        <label for="company_<?php echo $formHash; ?>">Company Name</label>
                        <span class="mypwn-input-container">
                            <input class="input-large font-small mypwn-input" type="text" name="company" value="" id="company_<?php echo $formHash; ?>"/>
                        </span>
                    </div>
                </div>
                <div class="grid_24 clearfix">
                    <div class="form-action">
                        <p class="nopadding">* Required fields</p>
                        <button class="button-orange" type="submit"><span>GO</span></button>
                    </div>
                </div>
            </form>
            <?php
            /**
             * Do not change the ID on this
             * pwnApp.outlookPlugin.formInit in pwn.js looks for this id to remove hidden class
             * after user has successfully submitted form.
             */
            ?>
            <ul class="hidden outlook-plugin-download-links">
                <li>Select which version of the Outlook plugin to download<small>*</small>:</li>
                <li class="button-list">
                    <button type="submit" class="button-stroked button-orange" onclick="window.open('<?php echo $outlookBtnData['2003']['href']?>','_blank'); dataLayer.push({'event': 'Outlook Plugin - 2003/onClick'});">
                        <span><?php echo $outlookBtnData['2003']['text']?></span>
                    </button>

                    <button type="submit" class="button-stroked button-orange" onclick="window.open('<?php echo $outlookBtnData['2010']['href']?>','_blank'); dataLayer.push({'event': 'Outlook Plugin - 2010/onClick'});">
                        <span><?php echo $outlookBtnData['2010']['text']?></span>
                    </button>
                </li>
                <li class="small-text">*To find out which version of Outlook you are using go into Outlook and select File, Help.</li>
            </ul>
        </li>
        <li>
            <div class="display-inline-block"><p>If you do not use Outlook, please use the Powwownow scheduler tool.</p><br/></div>
            <a class="dotted grey margin-bottom-20" onclick="dataLayer.push({'event': 'Schedule a call - Scheduler Tool/onClick'});" href="http://myscheduler.powwownow.com/" target="_blank">Scheduler tool</a>
        </li>
    </ul>
    <script type="text/javascript">
        (function($) {
            'use strict';
            $(function() {
                pwnApp.outlookPlugin.formInit('#<?php echo $formID?>');
            });
        })(jQuery);
    </script>
<?php endif; ?>
