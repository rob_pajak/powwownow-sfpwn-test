<div class="grid_12 omega" id="right-content-container" style="float:right">
    <ul id="rotating-promotional-links" class="circles floatleft">
        <li class="filled"><a onmouseover="promotionalFadeShowSlide(0, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(1, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(2, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(3, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(4, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(5, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(6, true);" href="#" onclick="return false;"><!--Blank--></a></li>
    </ul>
    <div id="rotating-promotional-container">
        <ul id="rotating-promotional-list">
            <li title="" class="rotating-promotional-list-item item-1 item-start get-plus" data-tracking-id="Promo-Rotator"></li>
            <li title="" class="rotating-promotional-list-item item-2"></li>
            <li title="" class="rotating-promotional-list-item item-3">
                <div class="promotion-content rockwell">
                    <div class="title-line">
                        <span class="title-1">TECH TRACK</span>
                        <span class="title-2">100</span>
                    </div>
                    <div class="body-line">Powwownow are</div>
                    <div class="body-line">proud to be included</div>
                    <div class="body-line">in The Sunday Times</div>
                    <div class="body-line">Tech Track 100, 2011<br/><br/></div>
                    <div class="button-line">
                        <button class="button-orange">
                            <span>read the full story here</span>
                        </button>
                    </div>
                </div>
            </li>
            <li title="" class="rotating-promotional-list-item item-4"></li>
            <li title="" class="rotating-promotional-list-item item-5">
                <div class="item-5-content rockwell">
                    <div class="title-1">COST</div>
                    <div class="title-2">SAVINGS</div>
                    <div><div class="hr-spotted-top png" id="content-seperator"><!--Blank--></div></div>
                    <div class="body-line">How on earth do we</div>
                    <div class="body-line">manage to save you so</div>
                    <div class="body-line">much money?</div>
                    <div class="button-line">
                        <button class="button-green">
                            <span>We spell it all out here</span>
                        </button>
                    </div>
                </div>
            </li>
            <li title="" class="rotating-promotional-list-item item-6">
                <div class="item-6-content rockwell">
                    <div class="title-line">INTERNATIONAL</div>
                    <div class="title-line">DIAL-IN</div>
                    <div class="title-line">NUMBERS</div>
                    <div class="body-line">Keeping you</div>
                    <div class="body-line">better connected</div>
                    <div class="body-line margin-bottom-20">for less</div>
                    <div class="button-line">
                        <button class="button-orange">
                            <span>view numbers and rates</span>
                        </button>
                    </div>
                </div>
            </li>
            <li title="" class="rotating-promotional-list-item item-7">
                <div class="item-7-content rockwell">
                    <div class="title-line">FOR CONFERENCE</div>
                    <div class="title-line">CALLS ON THE MOVE,</div>
                    <div class="title-line">DOWNLOAD OUR</div>
                    <div class="title-line margin-bottom-10">FREE APP</div>
                    <div class="body-line">Join conference calls at the</div>
                    <div class="body-line margin-bottom-10">touch of a button</div>
                    <div class="button-line">
                        <button class="item-7-iphone button-green">
                            <span>iPhone</span>
                        </button>
                        <button class="item-7-android button-green">
                            <span>Android</span>
                        </button>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
