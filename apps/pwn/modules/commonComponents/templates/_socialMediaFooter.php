<div class="grid_24" style="margin-top: 20px; margin-bottom: 10px;">
    <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    socialMediaFooterInitialise(<?php echo $sf_data->getRaw('popUpNames'); ?>);
});
</script>
<div class="mypwn_footer_main">
    <div class="mypwn_footer_main">
        <?php foreach ($items[$service_user] as $item) : ?>
            <div class="mypwn_footer_sub">
                <span class="mypwn_footer_icon icon-<?php echo ('powwownow' == $service) ? 'enchanced' : $service; ?>">
                    <span class="<?php echo $item['icon_class']; ?>"></span>
                </span>
                <?php $popUp = $item->getRaw('popUp'); ?>
                <?php if ($popUp) { echo $popUp; }?>
                <span class="mypwn_footer_title title-<?php echo ('powwownow' == $service) ? 'enchanced' : $service; ?>">
                    <a href="<?php echo $item['link']; ?>" target="_blank" class="<?php echo $item['popUpName']; ?>-link">
                        <?php echo $item['heading']; ?>
                    </a>
                </span>
                <span class="mypwn_footer_description"><?php echo $item['text']; ?></span>
            </div>
        <?php endforeach; ?>
    </div>
</div>