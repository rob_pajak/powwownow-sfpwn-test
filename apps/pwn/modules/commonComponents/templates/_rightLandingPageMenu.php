<div id="right-sidebar" class="grid_6">
    <div id="right-menu-container">
        <span class="icon-telephone-green png" style="width: 150px; padding-left: 40px;">Dial-in numbers:</span>
        <div class="dial-in-number tap-to-dial"><?php echo $dialInNumber; ?></div>
        <div class="margin-bottom-10">from your UK landline number.<br/>(From 4.3p a min)</div>
        <div class="dial-in-number tap-to-dial"><?php echo $mobileDialInNumber; ?></div>
        <div class="margin-bottom-10">for low cost calls from your UK mobile.<br/>(12.5p a min + VAT)</div>

        <ul class="information-links">
            <li>
                <span class="icon icon-green"><span class="icon-globe-white png"><!--Blank--></span><!--Blank--></span>
                <div><a id="tooltip-international-dial-in-numbers-link" class="grey" href="#" data-tracking="International Dial-in Numbers/onClick">International dial-in numbers</a></div>
                <div id="tooltip-international-dial-in-numbers" class="tooltip">
                    <div class="tooltip-arrow-top png"><!--Blank--></div>
                    <a href="#" class="floatright sprite tooltip-close" id="tooltip-international-dial-in-numbers-close">
                        <span class="sprite-cross-white png"></span>
                    </a>
                    <?php include_component('commonComponents', 'InternationalDialInNumbersSmall', array('fromResponsive' => false)) ?>
                </div>
            </li>
            <li>
                <span class="icon icon-green"><span class="icon-play-pause-white png"><!--Blank--></span><!--Blank--></span>
                <div><a id="tooltip-in-conference-controls-link" class="grey" href="" data-tracking="In-Conference controls/onClick">In-conference controls</a></div>
                <div class="tooltip" id="tooltip-in-conference-controls">
                    <div class="tooltip-arrow-top png"><!--Blank--></div>
                    <a id="tooltip-in-conference-controls-close" class="floatright sprite tooltip-close" href="#">
                        <span class="sprite-cross-white png"><!--Blank--></span>
                    </a>
                    <h4 class="rockwell">In-conference controls</h4>
                    <p style="margin:0; padding:0;">During a conference the following control keys are available:</p>
                    <dl>
                        <dt># = Skip Intro</dt>
                        <dd>Skips name recording and PIN playback when joining the call</dd>
                        <dt>#6 = Mute</dt>
                        <dd>Mutes and unmutes your handset</dd>
                        <dt>#1 = Head Count</dt>
                        <dd>Reviews the number of people on the call</dd>
                        <dt>#2 = Roll Call</dt>
                        <dd>Replays the names recorded when the callers arrived on the conference</dd>
                        <dt>#3 = Lock</dt>
                        <dd>Prevents anyone else joining the call</dd>
                        <dt>#8 = Record</dt>
                        <dd>This allows you to record a conference</dd>
                    </dl>
                    <h4>Download user guide</h4>
                    <ul class="information-links">
                        <li><span class="sprite-new sprite-new-pdf"><!--Blank--></span><a target="_blank" class="dotted grey" href="/sfpdf/en/Powwownow-User-Guide.pdf" onclick="dataLayer.push({'event': 'In-Conference controls - Download Powwownow-User-Guide/onClick'});">Powwownow</a></li>
                        <li><span class="sprite-new sprite-new-pdf"><!--Blank--></span><a target="_blank" class="dotted grey" href="/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" onclick="dataLayer.push({'event': 'In-Conference controls - Download Powwownow-Web-Conferencing-User-Guide/onClick'});">Web Conferencing</a></li>
                    </ul>
                </div>
            </li>
            <li>
                <span class="icon icon-green"><span class="icon-asterisks-white png"><!--Blank--></span><!--Blank--></span>
                <div><a class="grey" href="/Pin-Reminder" data-tracking="PIN Reminder/onClick">PIN reminder</a></div>

            </li>
            <li>
                <span class="icon icon-green"><span class="icon-calender-white png"><!--Blank--></span><!--Blank--></span>
                <?php include_component(
                    'commonComponents','outlookPluginTooltip',
                    array(
                        'id' => 'home-outlook-plugin',
                        'title'=> 'Schedule a call',
                        'classes' => array(
                            'font' => 'grey'
                        ),
                        'css' => array(
                            'left' => '-79px',
                            'top' => '20px'
                        )
                    )
                );?>
            </li>
            <li>
                <span class="icon icon-green"><span class="icon-apple-white png"><!--Blank--></span><!--Blank--></span>
                <div><a id="tooltip-mobile-app-link" class="grey" href="" data-tracking="Mobile App/onClick">Mobile app</a></div>
                <div id="tooltip-mobile-app" class="tooltip">
                    <div class="tooltip-arrow-top png"><!--Blank--></div>
                    <a id="tooltip-mobile-app-close" class="floatright sprite tooltip-close" href="#"><span class="sprite-cross-white png"><!--Blank--></span></a>
                    <h4 class="rockwell">Mobile app</h4>
                    <br/>Schedule conferences, set automatic call alerts plus store your details so joining calls is only ever one touch away.<br/>
                    <ul class="mobileapp-links">
                        <li><a class="dotted grey" href="https://market.android.com/details?id=com.grppl.android.shell.CMBpwn&amp;feature=search_result" target="_blank" onclick="dataLayer.push({'event': 'Mobile App - Download Android/onClick'});">Android App</a></li>
                        <li><a class="dotted grey" href="http://app.powwownow.com" target="_blank" onclick="dataLayer.push({'event': 'Mobile App - Download iPhone/onClick'});">iPhone App</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript">
(function($) {
    'use strict';
    $(function() {
        rightLandingPageMenuInitialise();
    });
})(jQuery);
</script>
