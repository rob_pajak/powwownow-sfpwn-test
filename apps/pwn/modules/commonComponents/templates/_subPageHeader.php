<?php if (false !== $breadcrumbs && false !== $title) : ?>
<div class="grid_12" style="margin-top:0;">
    <h1 class="mypwn_sub_page_heading mypwn_sub_page_heading_<?php echo $headingSize; ?> mypwn_sub_page_heading_<?php echo strtolower($user_type); ?>"><?php echo $title; ?></h1>
</div>
<?php
    if (!isset($disableBreadcrumbs)) $disableBreadcrumbs = false;
    if ($disableBreadcrumbs!=true):
        include_partial('commonComponents/headerBreadcrumbs',array('breadcrumbs' => $breadcrumbs,'title' => $title));
    endif;
endif;
