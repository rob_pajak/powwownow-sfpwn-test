<?php if ($powwownowSupportLevel < 20) : ?>
    <?php if (sfContext::getInstance()->getRequest()->getCookie('non_supported_browser_closed', false) != true): ?>
        <div id="non-supported-browser-container">
            <style scoped>
                .non-supported-browser {
                    padding: 10px;
                    margin: 0;
                    background: #eee;
                    color: #515151;
                    border: 1px solid rgba(0,0,0,0.1);
                    font-family: verdana, sans-serif;
                    font-size: 12px;

                }
                .non-supported-browser .close {
                    border: none;
                    float: right;
                    cursor: pointer;
                    font-size: 18px;
                    color: #000;
                    opacity: 0.2;
                }
                .non-supported-browser p {
                    padding: 0;
                }
                .non-supported-browser-comment a {
                    border-bottom: 1px solid;
                }
            </style>
            <div class="non-supported-browser clearfix">
                <div class="container_24">
                    <div class="grid_22">
                        <p class="non-supported-browser-comment">
                            <?php echo __('Should you be experiencing functionality and rendering issues please ') . '<a title="FAQs" href="' . url_for('@faqs') . '#Browsers">click here</a>' . __(' for more information'); ?>
                        </p>
                    </div>
                    <div class="grid_1">
                        <p><a class="close" title="Close">&times;</a></p>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <?php sfContext::getInstance()->getResponse()->setCookie('non_supported_browser_closed', true, date('Y-m-d h:i:s', strtotime('+1500 days')), '/'); ?>
    <?php endif; ?>
<?php endif; ?>
