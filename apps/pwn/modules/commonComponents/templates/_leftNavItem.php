<?php if ($active === true): ?>
    <li class="active"><a href="#"><?php echo $leftNavItem['description']; ?></a></li>
<?php else: ?>
    <li><a href="<?php echo $leftNavItem['url']; ?>" class="grey"><?php echo $leftNavItem['description']; ?></a></li>
<?php endif; ?>
