<?php if ($service_type == 'powwownow') : ?>
    Your PIN: <span class="pin rightpadding"><?php echo $enhancedPin; ?></span>
<?php elseif ($service_type == 'plus') : ?>
    Your Chairperson PIN: <span class="pin rightpadding"><?php echo $chairmanPin; ?></span>
    Your Participant PIN: <span class="pin rightpadding"><?php echo $pin; ?></span>
    <a href="<?php echo url_for('@pins'); ?>">View All</a>
<?php elseif ($service_type == 'premium') : ?>
    Your Chairperson PIN: <span class="pin rightpadding"><?php echo $chairmanPin; ?></span>
    Your Participant PIN: <span class="pin rightpadding"><?php echo $pin; ?></span>
    <a href="<?php echo url_for('@pins'); ?>">View All</a>
<?php endif; ?>