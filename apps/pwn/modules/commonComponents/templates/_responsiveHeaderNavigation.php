<div class="row">
    <div class="large-12 columns">
        <header>
            <nav id="top-menu-container" class="top-menu-container">
                <ul class="top-menu" id="top-menu1">
                    <li>
                        <a href="<?php echo url_for('@conference_call'); ?>">Conference Call</a>
                        <div class="sub-menu-outer-container">
                            <div class="top-submenu-container">
                                <ul class="top-submenu">
                                    <li><a href="<?php echo url_for('@how_conference_calling_works'); ?>">How conference calling works</a></li>
                                    <li><a href="<?php echo url_for('@costs'); ?>">Cost comparison</a></li>
                                    <li><a href="<?php echo url_for('@international_number_rates'); ?>">International dial-in numbers</a></li>
                                    <li><a href="<?php echo url_for('@conference_call_set_up_a_conference_call'); ?>">Set up a conference call</a></li>
                                    <li><a href="<?php echo url_for('@plus_service'); ?>">Plus</a></li>
                                    <li><a href="<?php echo url_for('@premium_service'); ?>">Premium</a></li>
                                    <li><a href="<?php echo url_for('@event_conference_calls'); ?>">Event calls</a></li>
                                    <li><a href="<?php echo url_for('@call_minute_bundles'); ?>">Call minute bundles</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo url_for('@web_conferencing'); ?>">Web Conference</a>
                        <div class="sub-menu-outer-container">
                            <div class="top-submenu-container">
                                <ul class="top-submenu">
                                    <li><a href="<?php echo url_for('@how_web_conferencing_works'); ?>">How web conferencing works</a></li>
                                    <li><a href="<?php echo url_for('@web_conferencing_get_started'); ?>">Get started</a></li>
                                    <li><a href="http://powwownow.yuuguu.com">Join a session</a></li>
                                    <li><a href="<?php echo url_for('@top_ten_tips_for_web_conferencing'); ?>">Top 10 tips</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <a class="white" href="<?php echo url_for('@video_conferencing'); ?>">Video Conference</a>
                        <div class="sub-menu-outer-container">
                            <div class="top-submenu-container">
                                <ul class="top-submenu">
                                    <li><a href="<?php echo url_for('@engage_service'); ?>">Powwownow Engage</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo url_for('@how_we_are_different'); ?>">Why Powwownow</a>
                        <div class="sub-menu-outer-container">
                            <div class="top-submenu-container">
                                <ul class="top-submenu">
                                    <li><a href="<?php echo url_for('@how_we_are_different'); ?>">How we are different</a></li>
                                    <li><a href="<?php echo url_for('@about_us'); ?>">About us</a></li>
                                    <li><a href="<?php echo url_for('@meeting_cost_calculator'); ?>">Meeting cost calculator</a></li>
                                    <li><a href="<?php echo url_for('@going_green'); ?>">Going green</a></li>
                                    <li><a href="<?php echo url_for('@business_efficiency'); ?>">Business efficiency</a></li>
                                    <li><a href="<?php echo url_for('@faqs'); ?>">FAQs</a></li>
                                    <li><a href="<?php echo url_for('@compare_services'); ?>">Compare services</a></li>
                                    <li><a href="<?php echo url_for('@news'); ?>">News</a></li>
                                    <li><a href="<?php echo url_for('@tell_a_friend'); ?>">Tell a friend</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li><a href="<?php echo url_for('@testimonials'); ?>">Testimonials</a></li>
                    <li><a href="http://www.powwownow.co.uk/blog/">Blog</a></li>
                    <li><a href="<?php echo url_for('@contact_us'); ?>">Contact Us</a></li>

                </ul>

            </nav>
        </header>
    </div>
</div>

<script>

    (function(){

        var DropDownMenu = {
            options: {
                showHideSpeed: null
            },
            initialize: function() {
                var _this = this;
                $('.js.no-touch #top-menu2 > li').mouseenter(function() {
                    $(this).addClass('active');
                    $('div',this).show(_this.options.showHideSpeed);
                });

                $('.js.no-touch #top-menu1 > li').mouseenter(function() {
                    $(this).addClass('active');
                    $('div',this).show(_this.options.showHideSpeed);
                });

                $('.js.no-touch #top-menu2 > li').mouseleave(function() {
                    $(this).removeClass("active");
                    $(this).children("div").hide(_this.options.showHideSpeed);
                });

                $('.js.no-touch #top-menu1 > li').mouseleave(function() {
                    $(this).removeClass("active");
                    $(this).children("div").hide(_this.options.showHideSpeed);
                });

            }
        };

        $('header nav').meanmenu({
            meanScreenWidth: 768,
            meanMenuCloseSize: "12px",
            meanExpandableChildren: true,
            meanExpand: '+',
            meanContract: '-',
            meanRemoveAttrs: true
        });

        DropDownMenu.initialize();

    })();

</script>