<?php $template = isset($template) ? $template : 'default'; ?>

<?php $noJsText = '<img src="/sfimages/warn.png" width="50" height="50" alt="no javascript enabled"
                             style="float: left;"/>

                        <p class="text-left white" style="float: left; ">For full functionality of this site it is
                            necessary to enable JavaScript. Here are the instructions <a
                                href="http://www.enable-javascript.com/" target="_blank" class="white">how to enable
                                JavaScript in your web browser</a>.</p>'; ?>

<?php if ($template === 'default') : ?>
    <noscript>
        <div>
            <style scoped>.nojavascript {padding: 0 15px 10px 10px; background: #a19fa0; text-align:center;};</style>
            <div class="nojavascript clearfix">
                <div class="container_24">
                    <div class="grid_23">
                        <img src="/sfimages/warn.png" width="50" height="50" alt="no javascript enabled"
                             style="float: left;"/>

                        <p class="text-left white" style="float: left; ">For full functionality of this site it is
                            necessary to enable JavaScript. Here are the instructions <a
                                href="http://www.enable-javascript.com/" target="_blank" class="white">how to enable
                                JavaScript in your web browser</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </noscript>
<?php elseif ($template === 'foundation-5-layout') : ?>

    <noscript>
        <section class="no-js-container">
            <div class="row">
                <div class="small-12 medium-12 large-12 columns">
                    <p class="section-text">For full functionality of this site it is necessary to enable JavaScript.
                        Here are the instructions <a href="http://www.enable-javascript.com/" target="_blank">
                            how to enable JavaScript in your web browser</a>.
                    </p>
                </div>
            </div>
        </section>
    </noscript>

<?php endif; ?>
