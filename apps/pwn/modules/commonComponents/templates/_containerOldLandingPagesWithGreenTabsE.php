<?php if(!isset($registrationSource)) { $registrationSource = 'undefined'; } ?>

<?php use_stylesheet('/sfcss/components/commonComponents.containerOldLandingPagesWithGreenTabsE.css'); ?>
<div id="tabs-grid" class="grid_18">
    <h1 class="tab_page_heading"><?php echo $sf_data->getRaw('header'); ?></h1>
    <div class="subheading"><p><?php echo $sf_data->getRaw('subHeader'); ?></p></div>

    <?php include_component('commonComponents', 'hintBox', array('title' => $hintBoxTitle, 'contents' => $hintBoxContent)); ?>

    <div class="landingpage-tab-1 png active" id="tab-1">
        <div class="rockwell font-large white"><?php echo $tabTitle; ?></div>
        <div style="display:none" class="tab-shadow png"><!--Blank--></div>
    </div>

    <div>
        <div id="landingpage-tabs-container">
            <div class="white" id="tab-pwn">
                <div class="grid_sub_11">
                    <form enctype="application/x-www-form-urlencoded" method="post" id="form-generate-pin" action="<?php echo url_for("@free_pin_registration"); ?>">
                        <h3 class="rockwell white">Enter your email to get started:<sup>1</sup>:</h3>
                        <div class="margin-top-20">
                            <span class="mypwn-input-container floatleft" style="margin-top: 7px;">
                                <input type="text" name="email" class="mypwn-input input-medium font-small" id="register_email" maxlength="50" />
                                <input type="hidden" name="registration_source" id="registration_source" value="<?php echo $registrationSource; ?>" />
                                <input type="hidden" id="registration_type" name="registration_type" value="lp" />
                            </span>
                            <button type="submit" class="button-orange" id="btn-generate" onclick="dataLayer.push({'event': 'Generate Pin - <?php echo $registrationSource; ?>/onClick'});">
                                <span>GENERATE PIN</span>
                            </button>
                            <div class="margin-top-5">
                                <input value="agreed" type="checkbox" name="agree_tick" id="agree-tick" style="margin-top: 0px;" checked class="required" />
                                <label for="agree-tick" class="floatleft">I have read and agree to the <a href="<?php echo url_for("@terms_and_conditions"); ?>" target="_blank" class="white">terms &amp; conditions</a></label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="grid_sub_13">
                    <span class="rockwell your-pin-is">***</span>Your PIN is:
                    <?php if (!$registeredPin) : ?><div class="pin-container png">******</div>
                    <?php else : ?><div class="pin-container pin-container-response png"><?php echo $registeredPin; ?></div>
                    <?php endif; ?>
                </div>
                <div class="grid_sub_24">
                    <ul id="features-list">
                        <li class="png">Just pay the cost of your own 0844 call</li>
                        <li class="png">Free <a class="white" title="iphone conferencing app" href="http://app.powwownow.com" target="_blank">iPhone app</a></li>
                        <li class="png">Low-cost <a class="white" title="International Number Rates" href="<?php echo url_for("@international_number_rates"); ?>">international dial-in access</a></li>
                        <li class="png">Free <a class="white" title="Powwownow Web Conferencing" href="<?php echo url_for("@web_conferencing"); ?>">web conferencing</a><br></li>
                        <li class="png">Free call recordings</li>
                        <li class="png">Free <a class="white" title="Scheduler Tool" href="http://myscheduler.powwownow.com/">scheduler tool</a> to invite participants</li>
                        <li class="png">Free handy wallet card</li>
                        <li class="png">A choice of on-hold music</li>
                    </ul>
                </div>
            </div>
            <div class="white" id="tab-pwn-pin-registered"><!-- Content to be replaced --></div>
        </div>
    </div>
</div>
<script type="text/javascript">
(function($) {
    'use strict';
    $(function() {
        registration.basic('form-generate-pin', 'register_email', '<?php echo $registrationSource; ?>', true, false);
    });
})(jQuery);
</script>
