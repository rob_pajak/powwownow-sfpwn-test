<div class="grid_24 dialog-plus-main-content" id="plusEmailForm">
    <div class="grid_sub_13" id="plus-registration">
        <h2>Enter your email address to register</h2>
        <form id="form-plus-switch-step1" method="post" enctype="application/x-www-form-urlencoded" action="/Plus-Signup-New" class="clearfix" style="position: relative;">
            <span class="mypwn-input-container<?php echo (isset($email) && $email != '') ? '-focused' : '';?>">
                <input class="mypwn-input email" id="plus_register_email" name="email" type="text" placeholder="Your email" value="<?php echo (isset($email)) ? $email : ''; ?>"/>
            </span>
            <button class="button-orange floatright" type="submit" id="plus_switch_step1" style="position: relative; z-index: 1500;">
                <span>CONTINUE</span>
            </button>
            <div class="form-field clearfix">
                <input type="checkbox" value="1" id="t_and_c" name="t_and_c" style="vertical-align: middle"/>&nbsp;<span>I have read and agree to the <a class="white" title="Terms and Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>" target="_blank">terms &amp; conditions</a></span>
            </div>
        </form>
        <p class="plus-reg-info">You will receive all the great features of Powwownow for free, plus the option to purchase additional products and features to build a conference package to meet your business requirements.</p>
    </div>
    <div class="grid_sub_9" id="existing-customer">
        <h2>Existing Plus Customer?</h2>
        <form id="form-existing-customer" method="post" enctype="application/x-www-form-urlencoded" action="/Plus-Existing-Customer" class="clearfix">
            <div><label for="form-existing-customer_email">Enter your email address</label></div>
            <span class="mypwn-input-container" id="form-existing-customer-email-span">
                <input class="mypwn-input email" id="form-existing-customer_email" name="email" type="text" placeholder="Enter your email address" value="<?php echo (isset($existing_email)) ? $existing_email : ''; ?>"/>
            </span>
            <div><label for="form-existing-customer_password">Password</label></div>
            <span class="mypwn-input-container<?php echo (isset($existing_email) && $existing_email != '') ? '-focused' : '';?>">
                <input class="mypwn-input" id="form-existing-customer_password" name="password" type="password" placeholder="Enter your password"/>
            </span>
            <div><a class="white" href="<?php echo url_for('@forgotten_password'); ?>" style="vertical-align: middle;">Forgotten your password?</a></div>
            <div style="width:194px;">
                <button class="button-orange float-right" type="submit" id="form-existing-customer_submit">
                    <span>Login</span>
                </button>
            </div>
        </form>
    </div>
    <div class="grid_sub_1 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>