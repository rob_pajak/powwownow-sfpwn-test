<script type="text/javascript">
    $(document).ready(function() {
        promotionalFadeInitialise();
    });
</script>
<div class="grid_12 omega" id="right-content-container" style="float:right">
    <ul id="rotating-promotional-links" class="circles floatleft">
        <li class=""><a onmouseover="promotionalFadeShowSlide(0, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class="filled"><a onmouseover="promotionalFadeShowSlide(1, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(2, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(3, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(4, true);" href="#" onclick="return false;"><!--Blank--></a></li>
        <li class=""><a onmouseover="promotionalFadeShowSlide(5, true);" href="#" onclick="return false;"><!--Blank--></a></li>
    </ul>
    <div id="rotating-promotional-container">
        <ul id="rotating-promotional-list">
            <li title="" style="position: relative; background: url('/sfimages/promo/plus-promo.jpg') no-repeat scroll 0 0 transparent; display: none;">
                <div style="padding-left: 0; padding-top: 0;">
                    <a href="javascript:void(0);" style="border-bottom: none;" class="get-plus" data-tracking-id="Promo-Rotator">
                        <img alt="" src="/sfimages/promo/promo-hover.png" class="png">
                    </a>
                </div>
            </li>
            <li title="" style="position: relative; background: url('/sfimages/promo-blank.png') no-repeat scroll 272px 80px transparent; display: block;">
                <div style="padding-left: 8px; padding-top: 15px;">
                    <div class="rockwell">
                            <span>
                                <span style="font-size: 45px; line-height: 67px">
                                    <span style="color: #495e69;">TECH TRACK</span>
                                    <span style="font-size: 45px; line-height: 67px">
                                        <span style="color: #a6cf5a;">
                                            <span style="color: #6e91a4;"> </span>
                                            <span style="color: #ee9900;">100</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <span style="color: #999999;"><span style="font-size: 22px;"><br/>Powwownow are</span></span>
                    </div>
                    <div class="rockwell"><span style="color: #999999;"><span style="font-size: 22px;">proud to be included</span></span></div>
                    <div class="rockwell"><span style="color: #999999;"><span style="font-size: 22px;">in The Sunday Times</span></span></div>
                    <div class="rockwell"><span style="color: #999999;"><span style="font-size: 22px;">Tech Track 100, 2011<br><br></span></span></div>
                    <button style="margin: 0; position: relative" class="button-orange"><span>read the full story here</span></button>
                </div>
                <div style="position: absolute; top: 0; left: 0">
                    <a style="border-bottom: none;" href="/blog/opinions/powwownow-included-prestigious-tech-track-100/ "><img src="/sfimages/promo/promo-hover.png" class="png" alt=""/></a>
                </div>
            </li>
            <li title="" style="position: relative; background: url('/sfimages/promo/3steps.jpg') no-repeat scroll 0 0 transparent; display: none;">
                <div style="padding-left: 0; padding-top: 0;">  </div>
                <div style="position: absolute; top: 0; left: 0">
                    <a style="border-bottom: none;" href="<?php echo url_for('@how_conference_calling_works'); ?>"><img src="/sfimages/promo/promo-hover.png" class="png" alt=""/></a>
                </div>
            </li>
            <li title="" style="position: relative; background: url('/sfimages/promo/savings.jpg') no-repeat scroll 0 0 transparent; display: none;">
                <div style="padding-left: 170px; padding-top: 15px;">
                    <div class="rockwell"><span style="font-size: 55px; line-height: 67px"><span style="color: #808080;">COST</span></span></div>
                    <div class="rockwell">
                        <span style="font-size: 55px; line-height: 67px"><span style="color: #a6cf5a;">SAVINGS</span></span>
                        <span style="color: #999999;"><br/></span>
                        <div class="rockwell"><div class="hr-spotted-top png" id="content-seperator"><!--Blank--></div></div>
                        <div class="rockwell"><span style="color: #999999;"><span style="font-size: 24px;">How on earth do we manage to save you so much money?</span></span></div>
                        <button style="position:relative" class="button-green "><span>We spell it all out here</span></button>
                    </div>
                </div>
                <div style="position: absolute; top: 0; left: 0"><a style="border-bottom: none;" href="<?php echo url_for('@costs'); ?>"><img src="/sfimages/promo/promo-hover.png" class="png" alt=""></a></div>
            </li>
            <li title="" style="position: relative; background: url('/sfimages/promo/interDial.jpg') no-repeat scroll 0 0 transparent; display: none;">
                <div style="padding-left: 178px; padding-top: 15px;">
                    <div style="text-align: right;" class="rockwell">
                            <span style="font-size: 30px; line-height: 36px;">INTERNATIONAL
                                <br/>
                                <span class="rockwell">DIAL-IN</span>
                                <br/><span class="rockwell">NUMBERS</span>
                            </span>
                    </div>
                    <div style="text-align: right;" class="rockwell">
                            <span style="color: #808080;">
                                <span style="font-size: 22px; line-height:26px">Keeping you
                                    <br/>
                                    <span class="rockwell">better connected</span>
                                    <br/>
                                    <span class="rockwell">for less</span>
                                </span>
                            </span>
                    </div>
                    <div style="text-align: right;" class="rockwell">
                        <br/>
                        <br/>
                        <button style="margin: 0; position: relative" class="button-orange"><span>view numbers and rates</span></button>
                    </div>
                </div>
                <div style="position: absolute; top: 0; left: 0">
                    <a style="border-bottom: none;" href="<?php echo url_for('@international_number_rates'); ?>"><img src="/sfimages/promo/promo-hover.png" class="png" alt=""></a>
                </div>
            </li>
            <li title="" style="position: relative; background: url('/sfimages/promo/mobileApp.jpg') no-repeat scroll 0 0 transparent; display: none;">
                <div style="padding-left: 172px; padding-top: 15px;">
                    <div class="rockwell">
                            <span style="font-size: 22px; line-height: 26px;">
                                <span style="color: #333333;">
                                    FOR CONFERENCE
                                    <br/>
                                    CALLS ON THE MOVE,
                                    <br/>
                                    DOWNLOAD OUR
                                    <br/>
                                    FREE APP
                                </span>
                            </span>
                    </div>
                    <br/>
                    <div class="rockwell">
                            <span style="font-size: 18px; line-height: 24px;">
                                <span style="color: #808080;">
                                    Join conference calls at the
                                    <br/>
                                    touch of a button
                                </span>
                            </span>
                    </div>
                    <br/>
                    <button onClick="window.open('http://app.powwownow.com')" style="position: relative; width: 100px; float: left;" class="button-green pie_first-child">
                        <span>iPhone</span>
                    </button>
                    <button onClick="window.open('https://market.android.com/details?id=com.grppl.android.shell.CMBpwn&amp;feature=search_result')" style="position: relative; width: 100px; float: left;" class="button-green pie_first-child">
                        <span>Android</span>
                    </button>
                </div>
            </li>
        </ul>
    </div>
</div>
