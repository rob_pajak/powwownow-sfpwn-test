<div class="grid_24 dialog-plus-main-content" id="plusNoSwitchPowwownow">
    <div class="grid_sub_21">
        <h2>You are already a Powwownow customer!</h2>
        <p>To purchase a Bundle, please <a href="#" class="first-screen-powwownow">switch to Powwownow Plus</a>.</p>
        <p><input type="hidden" id="email" value=""/>
    </div>
    <div class="grid_sub_2 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>