<?php use_helper('DataUri') ?>
<!-- How It Works Start-->
<div class="row">
    <div class="large-12 columns <?php echo $classes?>">
        <div class="large-12 columns panel border-green rounded-corners">
            <section class="how-it-works large-8 columns">
                <h3 class="sub-header">How It Works</h3>

                <ul class="how-it-works-list list-font-12">
                    <li class="list-item">
                        <span class="sprite first"></span>
                        <span class="description">Enter your email to generate your PIN</span>
                    </li>
                    <li class="list-item">
                        <span class="sprite second"></span>
                        <span class="description">Share your PIN and dial-in number with your call participants</span>
                    </li>
                    <li class="list-item">
                        <span class="sprite third"></span>
                        <span class="description">At the agreed time, all dial in, enter the PIN and start talking</span>
                    </li>
                </ul>
            </section>
            <!-- HomepageG Comp -->
            <section class="large-4 columns hide-for-medium-down modal-threeeasysteps">
                <a href="#" data-reveal-id="modal-threeeasysteps" id="modal-threeeasysteps-btn">
                    <img src="<?php echo getDataURI("/homepageG/img/three-easy-steps-play.png") ?>" alt="3 Easy Steps"/>
                </a>
            </section>
        </div>
    </div>
</div>
<!-- How It Works Start End-->