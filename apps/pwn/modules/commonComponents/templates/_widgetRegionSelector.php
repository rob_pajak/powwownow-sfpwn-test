<?php if (isset($regions)): ?>
    <ul class="widget_region_selector clearfix">
        <li data-state="closed">
            <a>
                <span class="region_selector_title">Change region</span>
                <div class="cx_sprite sprite_GBR sprite_GBR_override"></div>
            </a>
            <ul class="region_selector_regions">
                <?php foreach ($regions as $region):?>
                    <?php if ($region['country_code'] !== 'GBR'): ?>
                        <li class="clearfix">
                            <a href="http://<?php echo $region['domain'];?>" target="_blank"  class="clearfix">
                                <div class="columns">
                                    <div class="column_one cx_sprite sprite_<?php echo $region['country_code']?>"></div>
                                    <div class="column_two"><?php echo $region['country']?></div>
                                </div>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach;?>
            </ul>
        </li>
    </ul>
<?php endif; ?>