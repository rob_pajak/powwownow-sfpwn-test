<div style="display:none;" class="tooltip-header-all-tels tooltip" style="left: 200px;">
    <div class="tooltip-arrow-top png"></div>
    <a href="#" class="tooltip-header-all-tels-close floatright sprite tooltip-close"><span class="sprite-cross-white png"></span></a>

    <table class="mypwn powwownow">
        <thead>
            <tr><th>Country</th><th>Number</th></tr>
        </thead>
        <tbody>
            <?php foreach($dins AS $k => $din) { ?>
                <tr<?php echo $k%2 ? '' : ' class="odd"'; ?>><td><?php echo $din[0]; ?></td><td><?php echo $din[1]; ?></td></tr>            
            <?php } ?>
        </tbody>
    </table>    
    <?php if ($dinsPageUri) { ?>
        <br/>
        <a href="<?php echo $dinsPageUri; ?>">Click here to view your Dial-in Numbers in more detail</a>
    <?php } ?>
</div>