<div class="grid_24 dialog-enhanced-main-content" id="plusFormWithPassword1">
    <div class="grid_sub_22">
        <h2>Please provide us with a few details</h2>
        <form id="form-plus-registration-with-password1" method="post" enctype="application/x-www-form-urlencoded" action="/Enable-Plus-Account-Upgrade-Dashboard" class="clearfix">
            <input type="hidden" name="email" id="plus_signup_email" value="<?php echo $email; ?>"/>
            <input type="hidden" id="plus_signup_fname" value="<?php echo $first_name; ?>"/>
            <input type="hidden" id="plus_signup_lname" value="<?php echo $last_name; ?>"/>
            <div class="form-field">
                <label for="plus_signup_first_name">First Name *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_first_name" name="first_name" type="text" placeholder="Your first name" value="<?php echo $first_name; ?>"/>
                </span>
            </div>
            <div class="form-field">
                <label for="plus_signup_last_name">Last Name *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_last_name" name="last_name" type="text" placeholder="Your last name" value="<?php echo $last_name; ?>"/>
                </span>
            </div>
            <div class="form-field">
                <label for="plus_signup_business_phone">Company *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_company_name" name="company_name" type="text" placeholder="Your company name" value=""/>
                </span>
            </div>
            <div class="form-field">
                <label for="plus_signup_business_phone">Business Phone *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_business_phone" name="business_phone" type="text" placeholder="Business phone number" value=""/>
                </span>
            </div>
            <div class="form-field">
                <label for="plus_signup_confirm_password">Confirm password *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_confirm_password" name="confirm_password" type="password" placeholder="Confirm your password" />
                </span>
            </div>
            <div class="form-field">
                <label>* Required fields</label>
                <button class="button-orange" type="submit" id="plus_signup_submit">
                    <span>Switch to Plus</span>
                </button>
            </div>
            <div>
                <input type="checkbox" value="1" id="plus_signup_t_and_c" name="t_and_c"/>&nbsp;<span>I have read and agree to the <a class="white" title="Terms and Conditions" href="<?php echo url_for('@terms_and_conditions'); ?>#Money-Saving-Minute-Bundles" target="_blank">terms &amp; conditions</a></span>
            </div>
        </form>
        <p class="plus-reg-info">By switching to Plus you will receive all the great features of Powwownow for free, plus a Chairperson PIN for greater security and the option to purchase additional products and features.</p>
    </div>
    <div class="grid_sub_1 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>