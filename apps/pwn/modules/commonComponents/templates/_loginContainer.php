<div id="loginContainer" class="grid_24 clearfix">
    <div class="floatright">
        <?php if ($authenticated) : ?>
            <?php if ($showButton) : ?>
                <div id="mypwnbutton" class="floatright">
                    <button onclick="window.location.href=URL_MYPWN_START_PAGE" class="button-stroked button-orange" type="submit"><span>Enter myPowwownow</span></button>
                </div>
            <?php endif; ?>
            <div id="loggedinBox" class="floatright">
                 <div class="rockwell green font-big loginmyPwn clearfix"><?php echo __("myPowwownow"); ?></div>
                <?php include_partial('commonComponents/headerLoggedIn', array('first_name' => $first_name, 'showButton' => $showButton)); ?>
            </div>
            <div id="loginPadlock" class="icon icon-orange floatright">
                <span class="icon-padlock-white png"><!--Blank--></span>
            </div> 
        <?php else : ?>
            <div id="loginBox">
                <div class="rockwell green font-big notloggedinmyPwn clearfix"><?php echo __("myPowwownow"); ?></div>
                <?php //include_partial('commonComponents/headerNotLoggedIn', array('social_register' => (isset($social_register) && $social_register == true ? true : false))); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
