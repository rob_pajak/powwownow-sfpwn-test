<?php include_partial('commonComponents/headerBreadcrumbs',array('breadcrumbs' => $breadcrumbs,'title' => $title, 'style' => '', 'classes' => $classes)); ?>
<div class="<?php echo $headingClassPrefix ?>sub_page_heading_container">
    <h1 class="<?php echo implode(' ', $headingClasses->getRawValue()) ?>"><?php echo $title; ?></h1>
</div>