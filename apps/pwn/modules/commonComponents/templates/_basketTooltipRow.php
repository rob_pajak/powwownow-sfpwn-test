<tr data-cost="<?php echo isset($product['price']) ? $product['price'] : $product['prorata_charge'] ?>" <?php if (!empty($product['id'])): echo 'id="'.$product['id'].'"'; endif; ?> class="product <?php if (!empty($extraClasses)): echo $extraClasses; endif; ?>">
    <td class="label">
        <?php if (isset($product['price'])): ?>
            <?php echo $product['label'] ?>
        <?php else: ?>
            <?php printf('%s (£%.2f monthly) <em>(current month pro-rata charge)</em>', $product['label'], $product['monthly_charge']) ?>
        <?php endif; ?>
    </td>
    <td class="price">
        <?php if (isset($product['price'])): ?>
            <?php printf('£%.2f', $product['price']) ?>
        <?php else: ?>
            <?php printf('£%.2f', $product['prorata_charge']) ?>
        <?php endif; ?>
    </td>
</tr>