<li class="active"><a href="#">About us</a></li>
<li><a href="<?php echo url_for('@how_we_are_different'); ?>" class="grey">How we are different</a></li>
<li><a href="<?php echo url_for('@going_green'); ?>" class="grey">Going green</a></li>
<li><a href="<?php echo url_for('@business_efficiency'); ?>" class="grey">Business efficiency</a></li>
<li><a href="<?php echo url_for('@news'); ?>" class="grey">Latest news and press</a></li>
<li><a href="/blog" class="grey">Blog</a></li>
<li><a href="<?php echo url_for('@tell_a_friend'); ?>" class="grey">Tell a friend</a></li>
<li><a href="<?php echo url_for('@contact_us'); ?>" class="grey">Contact us</a></li>
