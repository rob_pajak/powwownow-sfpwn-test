<?php
$c[] = ($sf_user->isAuthenticated() === true ? link_to('Home', '/myPwn/') : link_to('Home', '/'));

$breadcrumbs = $sf_data->getRaw('breadcrumbs');

if (is_array($breadcrumbs)) {
    foreach ($breadcrumbs AS $b) {
        $c[] = is_array($b) ? link_to($b[0], $b[1]) : $b;
    }
}

if (substr($c[count($c) - 1], 1, 1) == 'a') {
    $c[] = $title;
}
?>
<div class="pwn-breadcrumbs">You are in: <?php echo implode(' > ', $c); ?></div>
