<?php if ($page == 'plusService') : ?>

    <div class="bubble-gray clearfix" style="position: relative">
        <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-globe.png" alt=""/></div>
        <div class="grid_sub_19 text grey">
            <p class="rockwell font-bigger green-dark">Free, local or global numbers</p>
            <p>Choose from a selection of phone numbers and rates for your conference calls. As well as the regular 0844 shared-cost numbers, you can also choose 0800 Freephone, local or international numbers. That means you can give your attendees <strong>free or low-cost landline and mobile access from over 100 countries around the world</strong>.</p>
        </div>
    </div>
    <div class="bubble-gray clearfix">
        <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-multiuser.png" alt=""/></div>
        <div class="grid_sub_19 text grey">
            <p class="rockwell font-bigger green-dark">Manage multiple users</p>
            <p>Plus gives you the ability to manage multiple users under one account - you can invite and create users in your myPowwownow account area.  Having all of your users under one account gives you added security and the ability to keep track and manage how the account is being used.</p>
        </div>
    </div>
    <div class="bubble-gray clearfix">
        <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-phone.png" alt=""/></div>
        <div class="grid_sub_19 text grey">
            <p class="rockwell font-bigger green-dark">Unlimited conference calls</p>
            <p>With Plus, you can hold unlimited conference calls on demand, and with individual Chairperson and Participant PIN sets you can run as many secure conference calls at the same time as you like.<br/>&nbsp;</p>
        </div>
    </div>
    <div class="bubble-gray clearfix">
        <div class="grid_sub_5 icon"><img src="/sfimages/plus/bookmark-yellow-speaker.png" alt=""/></div>
        <div class="grid_sub_19 text grey">
            <p class="rockwell font-bigger green-dark">Unbeatable quality</p>
            <p>Unlike other conference call services, Powwownow has its own fully-owned infrastructure, so, call quality is as good as your landline. Our conference calls use the exact same fibre optic cabling as your landline, and along with BT, we're the only conference call provider to have this quality.</p>
        </div>
    </div>
<?php elseif ($page == 'callMinuteBundle') : ?>
    <div class="bubble-gray clearfix">
        <div class="grid_sub_3 icon icon-light-grey">
            <img src="/sfimages/plus/powwow_individual_yellow_dark.png" alt="AYCM" id="dialog-229-link" class="bundle-dialog-link"/>
        </div>
        <div class="grid_sub_20 push_1 text grey">
            <p class="rockwell font-bigger green-light">All You Can Meet (AYCM)</p>
            <p class="font-big font-bold">2,500 UK Landline minutes for £35 a month!</p>
            <p class="font-size-12">This is a personal Bundle so can only be assigned to a single chairperson on the account. Ideal for individuals making on average one daily 30 minute conference call, with 4 people using a UK Landline number.</p>
            <p class="font-italic font-size-12">Upgrade to include worldwide numbers with our International Add-On for only £20 a month.</p>
        </div>
    </div>
    <div class="bubble-gray clearfix">
        <div class="grid_sub_3 icon icon-light-grey">
            <img src="/sfimages/plus/powwow_1000_yellow_dark.png"  alt="1000" id="dialog-223-link" class="bundle-dialog-link"/>
        </div>
        <div class="grid_sub_20 push_1 text grey">
            <p class="rockwell font-bigger green-light">Landline 1000</p>
            <p class="font-big font-bold">1,000 UK Landline minutes for £25 a month!</p>
            <p class="font-size-12">This is a team Bundle so can be assigned to as few or as many chairpersons on the account. Ideal for companies making on average one weekly 60 minute conference call, with 4 people using a UK Landline number.</p>
            <p class="font-italic font-size-12">Upgrade to include worldwide numbers with our International Add-On for only £10 a month.</p>
        </div>
    </div>
    <div class="bubble-gray clearfix">
        <div class="grid_sub_3 icon icon-light-grey">
            <img src="/sfimages/plus/powwow_2500_yellow_dark.png" alt="2500" id="dialog-225-link" class="bundle-dialog-link"/>
        </div>
        <div class="grid_sub_20 push_1 text grey">
            <p class="rockwell font-bigger green-light">Landline 2500</p>
            <p class="font-big font-bold">2,500 UK Landline minutes for £55 a month!</p>
            <p class="font-size-12">This is a team Bundle so can be assigned to as few or as many chairpersons on the account. Ideal for companies making on average one daily 30 minute conference call, with 4 people using a UK Landline number.</p>
            <p class="font-italic font-size-12">Upgrade to include worldwide numbers with our International Add-On for only £20 a month.</p>
        </div>
    </div>
    <div class="bubble-gray clearfix">
        <div class="grid_sub_3 icon icon-light-grey">
            <img src="/sfimages/plus/powwow_5000_yellow_dark.png" alt="5000" id="dialog-227-link" class="bundle-dialog-link"/>
        </div>
        <div class="grid_sub_20 push_1 text grey">
            <p class="rockwell font-bigger green-light">Landline 5000</p>
            <p class="font-big font-bold">5,000 UK Landline minutes for £100 a month!</p>
            <p class="font-size-12">This is a team Bundle so can be assigned to as few or as many chairpersons on the account. Ideal for companies making on average one daily 60 minute conference call, with 4 people using a UK Landline number.</p>
            <p class="font-italic font-size-12">Upgrade to include worldwide numbers with our International Add-On for only £40 a month.</p>
        </div>
    </div>
<?php endif; ?>