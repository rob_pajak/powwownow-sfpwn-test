<?php 
if (!isset($form)) {
    echo json_encode($sf_data->getRaw('errors'));
} else {
    $error = NULL;
    if (!$form->isValid()) $error = $form->pwn_getErrors();
    if (isset($postFormValidationError) && $postFormValidationError && is_null($error)) $error = array('error_messages' => $sf_data->getRaw('postFormValidationError'));
    if (!is_null($error)) echo json_encode($error);
}
