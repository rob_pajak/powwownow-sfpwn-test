<ul id="products-nav" class="clearfix" <?php if (!empty($forceStepStyles)) echo 'style="' . $globalStyle . '"'; ?>>
<?php 
$flowRaw   = $sf_data->getRaw('flow'); 
$lastChild = end($flowRaw);
foreach ($flow as $liststep => $listitem) :
    echo '<li';
    $classes = array();
    if ($liststep == $step) :
        $classes[] = 'current';
    elseif ($liststep == $previous) :
        $classes[] = 'previous';
    endif;
    if ($listitem === $lastChild) :
        $classes[] = 'last-child';
    endif;
    echo ' class="' . implode(' ', $classes) . '"';
    echo '>' . __($listitem) . '<span></span></li>';
endforeach; ?>
</ul>