<?php use_stylesheet('/shared/assets/stylesheets/responsiveRightMenu.css'); // SASS file located in/shared/assets/sass/responsiveRightMenu.scss. ?>
<?php use_javascript('/sfjs/pwn.js'); ?>
<?php use_javascript('/shared/assets/js/responsiveRightMenu.js'); ?>
    <div id="right-sidebar" class="large-3 columns hide-for-medium-down right-sidebar">
        <div id="right-menu-container" class="right-menu-container">
            <span class="icon-telephone-green-top">Dial-in numbers:</span>
            <div class="dial-in-number tap-to-dial" style="white-space: nowrap;"><?php echo $dialInNumber; ?></div>
            <div class="margin-bottom-10">from your UK landline number.<br/>(From 4.3p a min)</div>
            <div class="dial-in-number tap-to-dial"><?php echo $mobileDialInNumber; ?></div>
            <div class="margin-bottom-10">for low cost calls from your UK mobile.<br/>(12.5p a min + VAT)</div>
            <ul class="information-links">
                <li>
                    <span class="icon icon-green"><span class="icon-globe-white png"><!--Blank--></span><!--Blank--></span>
                    <div><a id="tooltip-international-dial-in-numbers-link" class="link-title" href="#" data-tracking="International Dial-in Numbers/onClick">International dial-in numbers</a></div>
                    <div id="tooltip-international-dial-in-numbers" class="tooltip tooltip-international-dial-in-numbers">
                        <div class="tooltip-arrow-top"></div>
                        <a href="#" class="right sprite tooltip-close" id="tooltip-international-dial-in-numbers-close">
                            <span class="sprite-cross-white"></span>
                        </a>
                        <?php include_component('commonComponents', 'InternationalDialInNumbersSmall',array('fromResponsive' => true)) ?>
                    </div>
                </li>
                <li>
                    <span class="icon icon-green"><span class="icon-play-pause-white"></span></span>
                    <div><a id="tooltip-in-conference-controls-link" href="#" class="link-title" data-tracking="In-Conference controls/onClick">In-conference controls</a></div>
                    <div class="tooltip tooltip-in-conference-controls" id="tooltip-in-conference-controls">
                        <div class="tooltip-arrow-top"></div>
                        <a id="tooltip-in-conference-controls-close" class="right sprite tooltip-close" href="#">
                            <span class="sprite-cross-white"></span>
                        </a>
                        <h4>In-conference controls</h4>
                        <p>During a conference the following control keys are available:</p>
                        <dl>
                            <dt># = Skip Intro</dt>
                            <dd>Skips name recording and PIN playback when joining the call</dd>
                            <dt>#6 = Mute</dt>
                            <dd>Mutes and unmutes your handset</dd>
                            <dt>#1 = Head Count</dt>
                            <dd>Reviews the number of people on the call</dd>
                            <dt>#2 = Roll Call</dt>
                            <dd>Replays the names recorded when the callers arrived on the conference </dd>
                            <dt>#3 = Lock</dt>
                            <dd>Prevents anyone else joining the call</dd>
                            <dt>#8 = Record</dt>
                            <dd>This allows you to record a conference</dd>
                        </dl>
                        <h4>Download user guide</h4>
                        <ul class="information-links">
                            <li><span class="sprite-new sprite-new-pdf"><!--Blank--></span><a target="_blank" class="dotted grey" href="/sfpdf/en/Powwownow-User-Guide.pdf" onclick="dataLayer.push({'event': 'In-Conference controls - Download Powwownow-User-Guide/onClick'});">Powwownow</a></li>
                            <li><span class="sprite-new sprite-new-pdf"><!--Blank--></span><a target="_blank" class="dotted grey" href="/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" onclick="dataLayer.push({'event': 'In-Conference controls - Download Powwownow-Web-Conferencing-User-Guide/onClick'});">Web Conferencing</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <span class="icon icon-green"><span class="icon-asterisks-white"></span></span>
                    <div><a id="tooltip-pin-reminder-link" class="link-title" href="#" data-tracking="PIN Reminder/onClick">PIN reminder</a></div>
                    <div id="tooltip-pin-reminder" class="tooltip tooltip-pin-reminder">
                        <div class="tooltip-arrow-top"></div>
                        <a id="tooltip-pin-reminder-close" class="right sprite tooltip-close" href="#">
                            <span class="sprite-cross-white"></span>
                        </a>
                        <div id="pin-reminder-form-placeholder">
                            <h4>PIN reminder</h4>
                            <form enctype="application/x-www-form-urlencoded" id="form-pin-reminder" class="clearfix h5form" method="post" action="/Pin-Reminder-Ajax" name="form-pin-reminder" novalidate="novalidate">
                                <div class="form-field">
                                    <h4>Email me my PIN</h4>
                                    <label for="email">Email Address*</label>
                                    <input type="text" name="email" placeholder="Enter email address" id="email">
                                </div>
                                <div class="form-field">
                                    <h4>Text me my PIN too</h4>
                                    <label for="mobile_number">Mobile number*</label>
                                    <input type="text" name="mobile_number" placeholder="Enter mobile number" id="mobile_number">
                                </div>
                                <div class="large-12 clearfix">
                                    <div class="form-action">
                                        <p class="nopadding">* Required fields</p>
                                        <input type="submit" class="yellow small button radius" value="GET MY PIN"/>
                                    </div>
                                </div>
                                <input type="hidden" name="request_source" value="<?php echo $requestSource ?>" />
                            </form>
                        </div>
                        <div style="display:none;" id="pin-reminder-response-placeholder"></div>
                    </div>
                </li>
                <li>
                    <span class="icon icon-green"><span class="icon-calender-white"></span></span>
                    <div><a id="tooltip-schedule-call-link" class="link-title" href="#" data-tracking="Schedule a call/onClick">Schedule a call</a></div>
                    <div id="tooltip-schedule-call" class="tooltip tooltip-schedule-call">
                        <div class="tooltip-arrow-top"></div>
                        <a id="tooltip-schedule-call-close" class="right sprite tooltip-close" href="#">
                            <span class="sprite-cross-white"></span>
                        </a>
                        <h4> Schedule a call</h4>

                        <div class="font-grey">
                           <p> If you use Microsoft<sup>&reg;</sup>Outlook<sup>&reg;</sup>, download our new plugin and log into your myPowwownow account to schedule a conference using your
                            Outlook calendar and contacts (PC only).
                            <br/><br/>
                            Already using our Plugin? Then upgrade now for new and improved features.</p>
                            <?php include_component('commonComponents', 'outlookPlugin'); ?>
                        </div>
                    </div>
                </li>
                <li>
                    <span class="icon icon-green">
                        <span class="icon-apple-white"></span>
                    </span>
                    <div>
                        <a id="tooltip-mobile-app-link" class="link-title" href="#" data-tracking="Mobile App/onClick">Mobile app</a>
                    </div>
                    <div id="tooltip-mobile-app" class="tooltip tooltip-mobile-app">
                        <div class="tooltip-arrow-top"></div>
                        <a id="tooltip-mobile-app-close" class="right sprite tooltip-close" href="#">
                            <span class="sprite-cross-white"></span>
                        </a>
                        <h4> Mobile App</h4>
                        <p>
                        Download the Powwownow app for conference calling on the go! Schedule conferences, set automatic call alerts plus store your details so joining calls is
                        only ever one touch away.
                        </p>
                        <ul class="mobileapp-links">
                            <li><a class="link-dotted" href="https://market.android.com/details?id=com.grppl.android.shell.CMBpwn&amp;feature=search_result" target="_blank" onclick="dataLayer.push({'event': 'Mobile App - Download Android/onClick'});">Android App</a></li>
                            <li><a class="link-dotted" href="http://app.powwownow.com" target="_blank" onclick="dataLayer.push({'event': 'Mobile App - Download iPhone/onClick'});">iPhone App</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>