<div class="grid_24" id="index-choose-plan">
    <h2 id="choose-the-plan" class="rockwell blue">Choose the plan that’s right for you</h2>
    <table class="choice-of-plan">
        <thead>
            <tr>
                <th class="col-1"></th>
                <th class="col-2">
                    <div style="position:relative">Powwownow
                        <small>Free</small>
                    </div>
                </th>
                <th class="col-3"></th>
                <th class="col-4">
                    <div style="position:relative">Plus
                        <small>Monthly or PAYG</small>
                    </div>
                </th>
                <th class="col-5"></th>
                <th class="col-6">
                    <div style="position:relative">Premium
                        <small>Tailor<br>-Made</small>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td class="row-gap" colspan="6"></td>
        </tr>
        <tr>
            <td class="col-1"><span class="rnd-crn">Who is it for?</span></td>
            <td class="col-2">If you need to make free and instant conference calls with no booking, no billing, no
                fuss!
            </td>
            <td class="col-3"></td>
            <td class="col-4">If you need all that we offer for free <a class="white"
                                                                        href="<?php echo url_for("plus_service"); ?>">plus
                    some additional extras</a> like a Chairperson PIN for greater security.
            </td>
            <td class="col-5"></td>
            <td class="col-6">If numerous people in your company make conference calls or if you're going to have
                regular calls and need more numbers and extra features at a negotiated price, plus dedicated customer
                support.
            </td>
        </tr>
        <tr>
            <td class="col-1"></td>
            <td class="col-2">Up to 50 participants per conference call</td>
            <td class="col-3"></td>
            <td class="col-4">Save even more on your Landline calls with our <a class="white"
                                                                                href="<?php echo url_for("@call_minute_bundles"); ?>">Money-Saving
                    Bundles</a>.
            </td>
            <td class="col-5"></td>
            <td class="col-6">Up to 1,000 participants per conference call</td>
        </tr>
        <tr>
            <td class="row-gap" colspan="6"></td>
        </tr>
        <tr>
            <td class="col-1"><span class="rnd-crn">How do I use it?</span></td>
            <td class="col-2">You can sign up and start conference calling instantly. There's no contract, and you can
                have as many or as few conference calls as you like.
            </td>
            <td class="col-3"></td>
            <td class="col-4">You can sign up and start conference calling immediately.<br><br>
                You can buy credit for <a class="white"
                                          href="<?php echo url_for("@product_details_worldwide_landline"); ?>">Worldwide</a>
                and UK Landline and <a class="white"
                                       href="<?php echo url_for("@product_details_worldwide_freephone"); ?>">Freephone</a>
                numbers to use as and when you need them.
                <br>
                <br>
                For a personal touch, you can purchase a <a class="white"
                                                            href="<?php echo url_for("@product_details_bwm"); ?>">Branded
                    Welcome Message</a> to greet participants.
            </td>
            <td class="col-5"></td>
            <td class="col-6">Contact us and we'll arrange a tailor-made solution for your company.</td>
        </tr>
        <tr>
            <td class="row-gap" colspan="6"></td>
        </tr>
        <tr>
            <td class="col-1" style="position: relative">
                <span class="rnd-crn">What does it cost?</span>
            </td>
            <td class="col-2">It’s FREE! You just pay the cost of an 0844 call which is added to your regular phone
                bill.
            </td>
            <td class="col-3"></td>
            <td class="col-4">It’s FREE! Just pay the cost of an 0844 call which is added to your regular phone bill.
                You can then purchase a <a class="white" href="<?php echo url_for("@call_minute_bundles"); ?>">Money-Saving
                    Bundle</a> or PAYG Call Credit to use additional numbers.
            </td>
            <td class="col-5"></td>
            <td class="col-6">Powwownow Premium is completely bespoke, so the price will vary according to your business
                needs.
            </td>
        </tr>
        <tr>
            <td class="row-gap" colspan="6"></td>
        </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="col-1"></td>
                <td class="col-2 rnd-crn"><a class="button-orange goto-registration-free" data-tracking="Generate Pin - Homepage-Middleleft/onClick">GENERATE PIN</a>No commitment and no credit card needed.</td>
                <td class="col-3"></td>
                <td class="col-4 rnd-crn"><a onclick="dataLayer.push({'event': 'Plus - Get Started/onClick'});" class="button-orange get-plus" <?php (isset($plusTrackingName)) ? 'data-tracking-id="'.$plusTrackingName.'"' : ''; ?> data-redirect="<?php echo url_for('@plus_service'); ?>">Find out more</a>Register for FREE and only pay for what you need!</td>
                <td class="col-5"></td>
                <td class="col-6 rnd-crn"><a href="<?php echo url_for("@premium_service"); ?>" class="button-orange">Find out More</a>Or call us on <strong style="display:block;font-size:13px">0800 022 9781</strong></td>
            </tr>
        </tfoot>
    </table>

    <div class="additional-features" style="width:750px">
        <h2 class="rockwell blue">Plus receive these additional features</h2>
        <table>
            <thead></thead>
            <tbody>
                <tr class="row-first">
                    <td class="col-2 tick">Web conferencing</td>
                    <td class="col-3"></td>
                    <td class="col-4 tick">Web conferencing</td>
                    <td class="col-5"></td>
                    <td class="col-6 tick">Premium web + video conferencing</td>
                </tr>
                <tr>
                    <td class="col-2 tick">Low-cost access numbers in 15 countries</td>
                    <td class="col-3"></td>
                    <td class="col-4 tick"><a class="white pwn-modal" href="<?php echo url_for("@product_details_worldwide_landline"); ?>">Low-cost</a> + <a class="white pwn-modal" href="<?php echo url_for("@product_details_worldwide_freephone"); ?>">free</a> access numbers for mobiles and landlines in over 80 countries</td>
                    <td class="col-5"></td>
                    <td class="col-6 tick">Low-cost + free access numbers for mobiles and landlines in over 70 countries</td>
                </tr>
                <tr>
                    <td class="col-2 tick">Call recording</td>
                    <td class="col-3"></td>
                    <td class="col-4 tick">Call recording</td>
                    <td class="col-5"></td>
                    <td class="col-6 tick">Call recording</td>
                </tr>
                <tr>
                    <td class="col-2"></td>
                    <td class="col-3"></td>
                    <td class="col-4 tick">Real-time reporting on all the account’s calls</td>
                    <td class="col-5"></td>
                    <td class="col-6 tick">Real-time reporting on all the account’s calls</td>
                </tr>
                <tr class="row-last">
                    <td class="col-2"></td>
                    <td class="col-3"></td>
                    <td class="col-4"></td>
                    <td class="col-5"></td>
                    <td class="col-6 tick">Branded welcome message</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
