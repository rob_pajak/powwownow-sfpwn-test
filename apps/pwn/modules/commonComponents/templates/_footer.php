<?php use_stylesheet('/sfcss/components/commonComponents.footer.css'); ?>
<div class="grid_24">
    <div id="pwn-footer">
        <div id="footer-bar">
            <p class="footer-links">
                <?php include_partial('commonComponents/footerLinks', array('footerLinks' => $footerLinks));?>
            </p>
            <p class="footer-social-media-links">
                <?php include_partial('commonComponents/socialLinks', array('socialLinks' => $socialLinks));?>
            </p>                            
            <p class="footer-copyright"><?php echo date('Y');?> Powwownow</p>
            <div class="clear"><!--Blank--></div>
        </div>
        <?php if ($showInternational) : ?>
        <p class="footer-international-links">
            <?php include_partial('commonComponents/internationalLinks', array('internationalLinks' => $internationalLinks)); ?>
        </p>
        <?php endif; ?>
    </div>
</div>