<div class="grid_24 dialog-plus-main-content" id="plusNoSwitchPlusUser">
    <div class="grid_sub_21">
        <h2>You are a Powwownow Plus user!</h2>
        <p class="toggle-firstname">Hi </p>
        <p>If you wish to purchase a Bundle for your account, please contact your Account Administrator.</p>
    </div>
    <div class="grid_sub_2 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>