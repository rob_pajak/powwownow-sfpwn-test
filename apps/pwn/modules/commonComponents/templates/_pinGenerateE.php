<a id="register"></a>
<div class="register-box">
    <form id="form-generate-pin-E" method="post" enctype="application/x-www-form-urlencoded" action="<?php echo url_for("@free_pin_registration"); ?>" class="clearfix">
        <h2 class="white"><?php echo __("Our one-step sign-up means you can start conference calling right now"); ?></h2>
        <div class="form-input-container">
            <span class="mypwn-input-container">
               <input type="text" id="register_email-a" name="email" class="mypwn-input input-medium font-small" maxlength="50"/>
               <input type="hidden" id="register-box-agree-tick" name="agree_tick" value="agreed" />
               <input type="hidden" id="register-box-registration_source" name="registration_source" value="<?php echo $registrationSource; ?>" />
               <input type="hidden" id="register-box-registration_type" name="registration_type" value="hp" />
            </span>
        </div>
        <div class="floatright form-button-container">
            <a class="button-orange" id="generate-pin-btn"><?php echo __("GENERATE PIN"); ?></a>
        </div>
        <div class="clear"></div>
        <span class="white floatleft font-small"><?php
            echo __("By clicking Generate PIN, you agree to the %1",
                array('%1' => link_to(
                    __("terms and conditions"),
                    "@terms_and_conditions",
                    array('class' => 'white')
                ))
            );
        ?></span>
    </form>
</div>
<script>
(function($) {
    'use strict';
    $(function() {
        registration.basic('form-generate-pin-E', 'register_email-a', 'HomepageE', true, false);
        $('#generate-pin-btn').click(function(){
            dataLayer.push({'event': 'Generate Pin - Homepage-Top/onClick'});
            $('#form-generate-pin-E').submit();
        });
    });
})(jQuery);
</script>
