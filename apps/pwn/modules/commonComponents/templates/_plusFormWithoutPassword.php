<div class="grid_24 dialog-plus-main-content" id="plusFormWithoutPassword">
    <div class="grid_sub_22">
        <h2>Please provide us with a few details</h2>
        <form id="form-plus-registration-without-password" method="post" enctype="application/x-www-form-urlencoded" action="/Enable-Plus-Account-New" class="clearfix">
            <input type="hidden" name="email" id="plus_register_email" value=""/>
            <input type="hidden" name="password" id="plus_register_password" value=""/>
            <input type="hidden" name="confirm_password" id="plus_register_confirm_password" value=""/>
            <div class="form-field">
                <label for="plus_signup_first_name">First Name *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_first_name" name="first_name" type="text" placeholder="Your first name" value=""/>
                </span>
            </div>
            <div class="form-field">
                <label for="plus_signup_last_name">Last Name *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_last_name" name="last_name" type="text" placeholder="Your last name" value=""/>
                </span>
            </div>
            <div class="form-field">
                <label for="plus_signup_business_phone">Company *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_company_name" name="company_name" type="text" placeholder="Your company name" value=""/>
                </span>
            </div>
            <div class="form-field">
                <label for="plus_signup_business_phone">Business Phone *</label>
                <span class="mypwn-input-container">
                    <input class="mypwn-input" id="plus_signup_business_phone" name="business_phone" type="text" placeholder="Business phone number" value=""/>
                </span>
            </div>
            <div class="floatright">
                <p>* Required fields</p>
                <button class="button-orange" type="submit" id="plus_signup_submit">
                    <span>Switch to Plus</span>
                </button>
            </div>
        </form>
    </div>
    <div class="grid_sub_1 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>