
<ul id="top-menu<?php echo $menuID; ?>" class="clearfix top-menu rockwell">
    <?php foreach ($menu as $linkDetails) : ?>
        <?php
            if (!empty($linkDetails['check_against_users']) && $accountUsers !== $linkDetails['check_against_users']) {
                continue;
            }
        ?>
        <li class="<?php echo (isset($linkDetails['cssClass'])) ? $linkDetails['cssClass'] : ''; ?>">
            <a
                class="white<?php echo (isset($linkDetails['link'])) ? '' : ' noHover'; ?>"
                <?php echo (isset($linkDetails['target'])) ? ' target="' . $linkDetails["target"] . '"' : ''; ?>
                <?php echo (isset($linkDetails['link'])) ? ' href="' : ''; ?>
                <?php if (isset($linkDetails['link'])) {
                    include_partial('commonComponents/headerNavigationLink', array('link' => $linkDetails['link']));
                } ?>
                <?php echo (isset($linkDetails['link'])) ? '"' : ''; ?>
            >
                <?php echo $linkDetails['item']; ?>
            </a>
        <?php if (empty($linkDetails['submenu'])) : ?>
        </li>
        <?php else : ?>
            <div>
                <div class="top-submenu-container">
                    <ul class="top-submenu">
                        <?php foreach ($linkDetails['submenu'] as $submenu) : ?>
                            <?php
                                if (!empty($submenu['check_against_users']) && $accountUsers != $submenu['check_against_users']) {
                                    continue;
                                }
                            ?>
                            <li>
                                <a href="<?php include_partial('commonComponents/headerNavigationLink', array('link' => $submenu['link'])); ?>"
                                   class="white"
                                   <?php echo (isset($submenu['target'])) ? ' target="' . $submenu["target"] . '"' : ''; ?>>
                                   <?php echo html_entity_decode($submenu['item']); ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>
