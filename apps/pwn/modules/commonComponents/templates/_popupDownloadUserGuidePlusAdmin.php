<div class="mypwn_footer_title title-<?php echo $service; ?>">
    <div style="display:none;" class="tooltip-footer-user-guides tooltip">
        <a class="tooltip-footer-user-guides-close floatright sprite tooltip-close" href="#"><span class="sprite-cross-white png"></span></a>
        <p>
            <a title="Powwownow Plus Admin Guide" href="/sfpdf/en/Powwownow-Plus-User-Guide-For-Admins.pdf" target="_blank">Powwownow Plus Admin User Guide</a>
        </p>
        <p>
            <a title="Powwownow Plus User Guide" href="/sfpdf/en/Powwownow-User-Guide.pdf" target="_blank">Powwownow Plus User Guide</a>
        </p>
        <p>
        <a title="Web Conferencing Guide" href="/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf" target="_blank">Web Conferencing Guide</a>
        </p>
        <p>
            <a title="Bundle User Guides" href="/sfpdf/en/Powwownow-Plus-Bundle-User-Guide-For-Admin.pdf" target="_blank">Bundles User Guide</a>
        </p>
        <div class="tooltip-arrow-bottom png"></div>
    </div>
</div>