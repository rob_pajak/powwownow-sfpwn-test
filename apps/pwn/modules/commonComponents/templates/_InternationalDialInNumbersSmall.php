
<dl class="international-dial-in-numbers">
    <?php $odd = 0; ?>
    <?php foreach ($internationalNumbers as $k => $details) : ?>
        <?php if (substr($k, -1) == '2') : // If this is the first number for the country ?>
    <dt class="<?php echo ($odd%2 != 1) ? 'odd' : null; ?>">
            <?php if (isset($fromResponsive) && ($fromResponsive === true)) : ?>
        <span></span><span>Mobile Number</span>
            <?php else:?>
        <span style="width: 16px; height: 11px; margin-right: 5px;"></span><span style="font-size: 10px;">Mobile Number</span>
            <?php endif; ?>
    </dt>
        <?php else : ?>
    <dt class="<?php echo ($odd++%2 == 1) ? 'odd' : null; ?>"><span class="sprite-square-flag sprite-square-flag-<?php echo strtolower($details['country_code']); ?>"></span><?php echo $details['country']; ?></dt>
        <?php endif; ?>
    <dd class="<?php echo ($odd%2!=1) ? 'odd' : null; ?>"><?php echo $details['national_formatted']; ?></dd>
    <?php endforeach; ?>

    <dt class="<?php echo ($odd++%2 == 1) ? 'odd' : null; ?>"><span class="sprite-square-flag sprite-square-flag-worldwide"></span>Worldwide &amp;</dt>
    <dd class="<?php echo ($odd%2!=1) ? 'odd' : NULL; ?>">+44 844 4 73 73 73</dd>

    <dt class="<?php echo ($odd%2 != 1) ? 'odd' : null; ?>"><span class="sprite-square-flag sprite-square-flag-skypeout"></span>SkypeOut</dt>

    <?php if (isset($showSkype) && ($showSkype == false)):?>
        <dd class="<?php echo ($odd%2!=1) ? 'odd' : NULL; ?>">+49 1803 001 178</dd>
    <?php else:?>
        <dd class="<?php echo ($odd%2!=1) ? 'odd' : NULL; ?>">+44 8447 620 398</dd>
    <?php endif; ?>

</dl>
<?php if (isset($fromResponsive) && ($fromResponsive === true)) : ?>
    <p>
        <a href="/sfpdf/en/Powwownow-Dial-in-Numbers.pdf" target="_blank" onclick="dataLayer.push({'event': 'International Dial-in Numbers - Download Powwownow-Dial-in-Numbers/onClick'});">Download</a> or
        <a style="margin: 0;" onclick="dataLayer.push({'event': 'International Dial-in Numbers - Mail/onClick'});" href="mailto:?subject=Powwownow&amp;body=Please%20download%20Powwownow%27s%20international%20dial-in%20numbers%20from%20http://www.powwownow.co.uk/sfpdf/en/Powwownow-Dial-in-Numbers.pdf">email</a>
        our international dial-in number guide
    </p>
<?php else:?>
    <p style="margin: 0">
        <a style="margin: 0;" href="/sfpdf/en/Powwownow-Dial-in-Numbers.pdf" target="_blank" onclick="dataLayer.push({'event': 'International Dial-in Numbers - Download Powwownow-Dial-in-Numbers/onClick'});">Download</a> or
        <a style="margin: 0;" onclick="dataLayer.push({'event': 'International Dial-in Numbers - Mail/onClick'});" href="mailto:?subject=Powwownow&amp;body=Please%20download%20Powwownow%27s%20international%20dial-in%20numbers%20from%20http://www.powwownow.co.uk/sfpdf/en/Powwownow-Dial-in-Numbers.pdf">email</a>
        our international dial-in number guide
    </p>
<?php endif; ?>

