<div class="grid_24 social-container">
    <div class="grid_sub_6">
        <span class="icon icon-blue floatleft"><span class="icon-blog-white png"><!--Blank--></span><!--Blank--></span>
        <h6><a class="grey" target="_blank" href="/blog">Powwownow Blog</a></h6>
        <ul class="list-fading" id="socialMediaFooter_pwn_blog"><li>&nbsp;</li></ul>
    </div>
    <div class="grid_sub_6">
        <span class="icon icon-blue floatleft"><span class="icon-twitter-white png"><!--Blank--></span><!--Blank--></span>
        <h6><a class="grey" target="_blank" href="http://www.twitter.com/powwownow">@powwownow</a></h6>
        <ul class="list-fading" id="socialMediaFooter_twitter"><li>&nbsp;</li></ul>
    </div>
    <div class="grid_sub_6">
        <span class="icon icon-blue floatleft"><span class="icon-facebook-white png"><!--Blank--></span><!--Blank--></span>
        <h6><a class="grey" target="_blank" href="http://www.facebook.com/powwownow">facebook.com/powwownow</a></h6>
        <ul class="list-fading" id="socialMediaFooter_facebook"><li>&nbsp;</li></ul>
    </div>
    <div class="grid_sub_6">
        <span class="icon icon-blue floatleft"><span class="icon-news-and-press-white png"><!--Blank--></span><!--Blank--></span>
        <h6><a class="grey" target="_blank" href="<?php echo url_for('@news'); ?>">Latest News &amp; Press</a></h6>
        <ul class="list-fading" id="socialMediaFooter_pwn_news"><li>&nbsp;</li></ul>
    </div>
</div>