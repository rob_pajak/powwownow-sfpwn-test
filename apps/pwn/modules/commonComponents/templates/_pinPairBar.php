<?php if ($authenticated) : ?>
<div class="mypwn_sub_header <?php echo $service; ?>">
    <div class="grid_15 floatleft">
        <div class="grid_sub_24 clearfix">
            <span class="myPowwownow">myPowwownow</span>
            <?php if ('powwownow' != $service) : ?><span class="service_name"><?php echo strtoupper($service); ?></span><?php endif; ?>
        </div>
        <div class="hello grid_sub_24 clearfix">
            Hello <span class="firstname"><?php echo $firstName; ?></span>, welcome to myPowwownow
        </div>
        <?php if ('powwownow' == $service) : ?><div class="yourPin"><?php else : ?><div class="yourPin grid_sub_24 clearfix"><?php endif; ?>
            <?php include_partial('commonComponents/pinPairBarYourPin', array(
                'service_type'=> $service,
                'chairmanPin' => $chairmanPin,
                'pin'         => $pin,
                'enhancedPin' => $enhancedPin
            )); ?>
        </div>
        <?php include_partial('commonComponents/pinPairBarDialInNumber', array(
            'service_type'  => $service,
            'number'        => $number,
            'dialInNumbers' => $dialInNumbers
        )); ?>
    </div>
    <div class="grid_9 floatright">
        <?php if ('plus' == $service && isset($balance)) : ?>
            <div class="plus-header" data-cost="<?php echo $basketCost; ?>">
                <?php if ($isPlusAdmin) : ?>
                    <div id="bskt-prds">
                        <?php echo link_to(
                        "<span id=\"basket-items\">$numAddedPr</span>"
                            . image_tag("/sfimages/299-brighter-small.png", array('alt' => 'Basket'))
                            ."<span id=\"basket-link\">View Basket</span>",
                        'basket',
                        array(), // because the route is an internal route, the third argument contains query arguments.
                        array('class' => 'basket-link')
                        ); ?>

                        <div class="basket-contents <?php if ($basketProducts->count()): ?>has-products<?php endif; ?>">
                            <h4>Your basket</h4>
                            <table>
                                <thead>
                                    <tr>
                                        <td class="bold" colspan="2">Products</td>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td class="bold">Total</td>
                                        <td class="price">
                                            <?php printf('£%.2f', $basketCost) ?>
                                        </td>
                                    </tr>
                                </tfoot>
                                <tbody class="products">
                                    <?php include_partial('commonComponents/basketTooltipRow', array('product' => array('price' => '', 'label' => ''), 'extraClasses' => 'prototype')); ?>
                                    <?php foreach ($basketProducts as $product): ?>
                                        <?php include_partial('commonComponents/basketTooltipRow', array('product' => $product)); ?>
                                    <?php endforeach; ?>
                                    <tr class="total-divider">
                                        <td colspan="2"><hr /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (!$isPostPayCustomer): ?>
                    <div id="balance">
                        <p>Balance: <span class="balance rightpadding"><?php echo ltrim(format_currency($balance, 'GBP')); ?></span></p>
                        <?php if ($isPlusAdmin) : ?>
                            <?php if (empty($basketInBundle)): ?>
                                <button id="header-purchase-credit" class="button-orange" type="button" onclick="window.location='<?php echo url_for('@products_select'); ?>'">
                                    <span><?php echo __('Purchase Call Credit') ?></span>
                                </button>
                            <?php else: ?>
                                <button id="header-purchase-credit" class="button-disabled" type="button" disabled="disabled">
                                    <span><?php echo __('Purchase Call Credit') ?></span>
                                </button>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php else: ?>
                    <div id="minute-allowance">
                        <p><?php echo __('Remaining minutes') ?>: <span class="minute-allowance rightpadding"><?php echo $remainingMinuteAllowance ?></span></p>
                    </div>
                    <?php if (isset($outstandingBalance) && $outstandingBalance != 0): ?>
                        <div id="outstanding-balance">
                            <p><?php echo __('Outstanding balance') ?>: <span class="outstanding-balance rightpadding"><?php echo '£' . sprintf("%.2f", $outstandingBalance); ?></span></p>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        //myPwnHeader_init();
    });
    </script>
</div>
<div class="grid_24">
    <div class="hr-spotted-top png content-seperator"></div>
</div>
<?php endif; ?>
