<?php echo form_tag(url_for('create_user_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-create-user', 'class' => 'pwnform clearfix')); ?>
    <div class="grid_sub_24 clearfix">
        <?php $count = 1; ?>
        <?php foreach ($fields->getRawValue() as $field) : ?>
            <?php include_partial('mypwn/createUserFormField', array(
                    'field'       => $field,
                    'form'        => $createUserForm,
                    'count'       => $count
                )); ?>
            <?php $count = (2==$count) ? 1 : 2 ?>
        <?php endforeach; ?>
    </div>
    <div class="form-action grid_sub_24 clearfix">
        <p>* Required fields</p>
        <?php if (!empty($addCancel)): ?>
            <button id="form-create-user-cancel" class="button-orange" type="button"><span><?php echo __('Cancel'); ?></span></button>
        <?php endif; ?>
        <button id="form-create-user-submit" class="button-green" type="submit"><span><?php echo __('Create User'); ?></span></button>
    </div>
</form>