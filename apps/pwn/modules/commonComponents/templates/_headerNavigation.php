<div class="grid_24 top-menu-container-grid">
    <div class="clearfix<?php echo (!$service) ? '' : ' ' . $service; ?>" id="top-menu-container">
        <div class="<?php echo ($serviceUser !== 'UNAUTHENTICATED') ? 'grid_sub_10' : 'grid_sub_12'; ?>">
            <?php if (!empty($navigation['leftMenu'])) include_partial('commonComponents/headerNavigationLinks', array('menu' => $navigation['leftMenu'], 'menuID' => 1, 'accountUsers' => $accountUsers)); ?>
        </div>
        <div class="<?php echo ($serviceUser !== 'UNAUTHENTICATED') ? 'grid_sub_14' : 'grid_sub_12'; ?>">
            <?php if (!empty($navigation['rightMenu'])) include_partial('commonComponents/headerNavigationLinks', array('menu' => $navigation['rightMenu'], 'menuID' => 2, 'accountUsers' => $accountUsers)); ?>
        </div>
    </div>
    <?php if ($headerHint) include_partial('commonComponents/headerHint',array('authenticated' => $authenticated)); ?>
</div>