<?php
use_helper('pwnVideo');
$howItWorksVideo = output_video (
    array (
        'config' =>
            array(
                'width' => 460,
                'height' => 260,
                'autoplay' => false,
                'controls' => true
            ),
        'video' => array(
            'image' => '/sfimages/video-stills/3-steps-video.jpg',
            'mp4'   => '/sfvideo/pwn_3steps460x260_LR.mp4',
            'webm'  => '/sfvideo/pwn_3steps460x260_LR.webm',
            'ogg'   => '/sfvideo/pwn_3steps460x260_LR.ogv'
        ),
        'called' => 'script'
    )
);
$howItWorksVideo = str_replace(
    array("'", "\n"),
    array("\'", " "),
    $howItWorksVideo
);
?>
<div class="how-it-works-box clearfix">
    <div class="how-it-works-arrow"></div>
    <h2 class="rockwell blue">How it works</h2>
    <img class="videoThumb" src="/sfimages/video-stills/3_easy_steps_video_still_homepage.jpg" alt="3 easy steps">
    <ul class="how-it-works-list">
        <li class="first">
            Enter your email to <a class="goto-registration-free"
                                   data-tracking="Generate Pin - Homepage-GenerateYourPIN/onClick">generate your PIN</a>
        </li>
        <li class="second">Share your PIN and dial-in number with your call participants</li>
        <li class="third">At the agreed time, all dial in, enter the PIN and start talking!</li>
    </ul>
</div>
<div id="how-It-Works-video" title="3 easy steps"></div>
<script>
    $(function () {
        var howItWorksVideoElement = $('#how-It-Works-video'),
            howItWorksVideoVideoText = '#how-It-Works-video>video';
        // Create Dialog
        howItWorksVideoElement.dialog({
            autoOpen: false,
            width: "auto",
            height: "auto",
            modal: true,
            draggable: false,
            dialogClass: "how-it-works-video",
            close: function (event, ui) {
                $(howItWorksVideoVideoText).trigger("stop");
                howItWorksVideoElement.html('');
            }
        });
        // Open Video
        $(".videoThumb").on("click", function () {
            howItWorksVideoElement.html('<?php echo $howItWorksVideo; ?>');
            howItWorksVideoElement.dialog("open");
            $(howItWorksVideoVideoText).trigger("play");
            $(howItWorksVideoVideoText).on("ended", function () {
                $(howItWorksVideoVideoText)[0].currentTime = 1;
            });
        });
    });
</script>
