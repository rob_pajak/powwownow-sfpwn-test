<?php
// Internet Explorer 8 Check for Product Navigation. IE 8 has funky viewing, so that the Navigation should be an Image.
if (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE 8.0')) { ?>

    <img src='/sfimages/<?php echo $flow.$step;?>.png' alt='Products Navigation' style='margin:10px 0 20px;'/>

<?php } else { ?>

    <ul id="products-nav" class="clearfix">
        <?php foreach ($flows[$flow] as $id => $list) { ?>
            <li<?php echo $id == $step ? ' class="current"' : ''; ?>>
                <?php echo __($list); ?><span></span>
            </li>    
        <?php } ?>
    </ul>

<?php }