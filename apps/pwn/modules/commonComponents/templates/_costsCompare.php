<div class="grid_24 index-costs-compare">
    <h3 id="cost-compare" class="rockwell blue">How our costs compare</h3>
    <p> This is what a chairperson would pay for a 1 hour conference with 4 participants (using the standard 0844 shared cost number):</p>
    <div id="tooltip-comparison" class="tooltip" style="right: 22px;top: 110px;left: auto;">
        <div class="tooltip-arrow-left png" style="top: 50px;"><!--#empty tag saver--></div>
        Unlike other conference call providers, Powwownow doesn't charge the chairperson a "bridging fee" - they just pay the same low-call rate as all participants*.
    </div>
    <img src="/sfimages/hp-compare-graph.gif" alt="Powwownow BT comparison">
</div>
<div class="grid_24">
    <small class="footnote">* Call charges may vary depending on your network provider.</small>
</div>
