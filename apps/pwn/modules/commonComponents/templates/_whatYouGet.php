<section class="container_16 what_you_get clearfix">
    <section class="grid_8 alpha omega left">
        <div>
            <span class="sprite icons_fccs-pound"></span>
            <span class="title font_green">No hidden costs</span>
            <span class="body font_blue_e">Each participant pays 4.3p per minute from a BT
                landline and 12.5p per minute using our dedicated mobile shortcode.
            </span>
        </div>
        <div>
            <span class="sprite icons_fccs-speaker"></span>
            <span class="title font_green">Amazing call quality</span>
            <span class="body font_blue_e">Unlike almost all conference call providers, our conference calls use the
                exact same fibre optic cabling as your landline.
            </span>
        </div>
    </section>
    <section class="grid_8 alpha omega right">
        <div>
            <span class="sprite icons_fccs-laptop"></span>
            <span class="title font_green">Free screen sharing</span>
            <span class="body font_blue_e">Share your screen with your participants using web conferencing without
                them needing to install any extra software.
            </span>
        </div>
        <div>
            <span class="sprite icons_fccs-handset"></span>
            <span class="title font_green">Low-cost international calls</span>
            <span class="body font_blue_e">We have over 14 international numbers so that you or your
                participants can get connected wherever you are.
            </span>
        </div>
    </section>
</section>