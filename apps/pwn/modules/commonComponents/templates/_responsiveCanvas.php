<?php  $layers = (!isset($layers) ? array('It\'s your call.','And you\'re about to make the right one.') : $layers);?>
<div class="row">
    <div class="small-12 columns">
        <!-- Hero Element-->
        <div class="row">
            <div class="small-12 columns">
                <section id="hero" class="hero">
                    <div class="canvas">
                        <div class="layer-one"><?php echo $layers[0];?></div>
                        <div class="layer-two"><?php echo $layers[1];?></div>
                    </div>
                </section>
            </div>
        </div>
        <!-- Hero Element End-->
    </div>
</div>