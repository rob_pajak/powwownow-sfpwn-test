<video
    class="pwn-html5-video"
    width="<?php echo $config['width']; ?>"
    height="<?php echo $config['height']; ?>"
    <?php echo ($video['image'] !== '') ? 'poster="' . $video['image'] . '"' : '' ; ?>
    <?php echo $config['controls']; ?>
    preload="<?php echo $config['preload']; ?>"
    >
    <source type="video/mp4" src="<?php echo $video['mp4']; ?>">
    <source type="video/webm" src="<?php echo $video['webm']; ?>">
    <source type="video/ogg" src="<?php echo $video['ogg']; ?>">
    <div
        id="video-<?php echo $config['playerID']; ?>"
        class="pwn-html5-video"
        style="width:<?php echo $config['width']; ?>px;height:<?php echo $config['height']; ?>px;display:block"
        ></div>
</video>
<script>
(function($) {
    'use strict';
    $(function() {
        setUpVideo(
            false,
            false,
            "<?php echo $video['mp4']; ?>",
            "video-<?php echo $config['playerID']; ?>",
            '<?php echo $errorMessage; ?>'
        );
    });
})(jQuery);
</script>