<div class="grid_24 dialog-plus-main-content" id="plusPasswordForm">
    <div class="grid_sub_22">
        <h2>Enter your myPowwownow password</h2>
        <form id="form-plus-switch-auth" method="post" enctype="application/x-www-form-urlencoded" action="/Plus-Signup-New" class="clearfix">
            <input type="hidden" name="email" id="plus_register_email" value="<?php echo (isset($email)) ? $email : ''; ?>"/>
            <input type="hidden" value="1" id="t_and_c" name="t_and_c"/>
            <span class="mypwn-input-container">
                <input class="mypwn-input" id="plus_auth_password" name="password" type="password" placeholder="Your password"/>
            </span>
            <button class="button-orange floatright" type="submit" id="plus_switch_auth" style="position: relative; z-index: 1500;">
                <span>CONTINUE</span>
            </button><br/>
            <a href="<?php echo url_for('@forgotten_password'); ?>">Forgotten your password?</a>
            <p class="plus-reg-info">All of your existing Powwownow account settings will be transferred on to your Plus account, so there is nothing to worry about!</p>
        </form>
    </div>
    <div class="grid_sub_1 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>