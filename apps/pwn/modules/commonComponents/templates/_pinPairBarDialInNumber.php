<?php use_helper('dialInNumbers') ?>
<?php if ($service_type == 'powwownow') : ?>
    <div class="yourDialinNumber">
        Your dial-in number: <span class="dialin rightpadding"><?php echo $number; ?></span>
        <a href="<?php echo url_for('@pins'); ?>" class="tooltip-header-all-tels-link">View All</a>
        <div style="display:none;" class="tooltip-header-all-tels tooltip">
            <div class="tooltip-arrow-top png"></div>
            <a href="#" class="tooltip-header-all-tels-close floatright sprite tooltip-close"><span class="sprite-cross-white png"></span></a>
            <table class="mypwn powwownow">
                <thead><tr><th>Country</th><th>Number</th></tr></thead>
                <tbody>
                    <?php echo getDialInNumbersHeaderPopup($dialInNumbers); ?>
                </tbody>
            </table>
            <br>
            <a href="<?php echo url_for('@dial_in_numbers'); ?>">Click here to view your Dial-in Numbers in more detail</a>
        </div>
    </div>
<?php endif; ?>
