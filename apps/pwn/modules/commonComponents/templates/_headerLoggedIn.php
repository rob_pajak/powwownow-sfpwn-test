    <?php if ($showButton) : ?>
    <div id="mypwnbutton" class="floatright">
        <button onclick="window.location.href=URL_MYPWN_START_PAGE" class="button-stroked button-orange" type="submit">
            <span>Enter myPowwownow</span>
        </button>
    </div>
    <?php endif; ?>
    <div id="loggedinBox" class="floatright">
        <div class="rockwell green font-big loginmyPwn clearfix"><?php echo __("myPowwownow"); ?></div>
        <div class="logged grey font-small clearfix">
            <div class="<?php echo ($showButton) ? 'show-button-enabled' : 'show-button-disabled'; ?>">
                <?php echo __("Hello") . ' '; ?><span class="orange-dark loggedInFirstname"><?php echo $first_name; ?></span>!&nbsp;<?php echo __("You are logged in"); ?>

            </div>
        </div>
        <div class="smalllogout">
            <a href="#" class="grey font-small" id="logout"><?php echo __("Log out"); ?></a>
        </div>
    </div>
    <div id="loginPadlock" class="icon icon-orange floatright">
        <span class="icon-padlock-white png"><!--Blank--></span>
    </div>
