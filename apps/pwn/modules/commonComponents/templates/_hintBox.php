<div id="hint-box">
    <h3 class="rockwell green"><?php echo $title; ?></h3>
    <?php if (isset($contents) && !is_null($contents)) : echo $contents; else : ?>
        <ol style="padding: 15px 15px 0 20px;">
            <li><p>Enter your email to generate your PIN</p></li>
            <li><p>Contact your participants with the PIN, dial-in number and time of the call</p></li>
            <li><p style="text-align: justify;">At the agreed time, all dial <br/><?php echo $dialInNumbers['local']; ?>, enter the PIN and start talking</p></li>
        </ol>
    <?php endif; ?>
</div>
