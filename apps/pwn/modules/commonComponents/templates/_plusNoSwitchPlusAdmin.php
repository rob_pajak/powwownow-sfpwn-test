<div class="grid_24 dialog-plus-main-content" id="plusNoSwitchPlusAdmin">
    <div class="grid_sub_21">
        <h2>You are already a Powwownow Plus customer!</h2>
        <p class="toggle-firstname">Hi </p>
        <p>Please log in to your Plus account through the <a href="#" class="first-screen-plusadmin">Existing Plus account area</a>.</p>
        <p><input type="hidden" id="email" value=""/></p>
    </div>
    <div class="grid_sub_2 dialog-plus-close">
        <a class="floatright sprite tooltip-close pst-tooltip-close" href="#">
            <span class="sprite-cross-white png"><!--Blank--></span>
        </a>
    </div>
</div>