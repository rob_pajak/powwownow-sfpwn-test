<?php if ($showSkype): ?>
    <div class="container_24 important-messages clearfix">
        <style scoped>
            .warning-message {border-radius: 5px; background-color: #93C8E6;  font-family: Verdana;}
            .warning-message p {line-height: normal; padding: 10px 0;}
            .warning-message .close { position: relative; text-decoration: none; border: none; font-size: 12px; top: 0; right: 0;}
        </style>
        <div class="grid_24 warning-message warning-blue">
            <div class="grid_2 alpha omega">
                <img src="/cx2/img/skypeLogo.png" alt="Skype" />
            </div>
            <div class="grid_22 omega">
                <p>Skype is currently experiencing technical issues connecting to our 0844 number. Please use
                <strong>+44 8447 620 398</strong> until the issue is resolved
                <a class="close" title="Close">&#215;</a>
                </p>
            </div>
        </div>
    </div>
    <script>
        (function($) {
            $('.warning-message').on('click','.close', function(event){
                event.preventDefault();
                var $element = $('.warning-message');
                $element.fadeOut(500, function() {
                    $element.css({'visibility': 'hidden', 'display': 'block'}).slideUp(function() {
                        $element.remove();
                    });
                });
            });
        })(window.jQuery);
    </script>
<?php endif; ?>
