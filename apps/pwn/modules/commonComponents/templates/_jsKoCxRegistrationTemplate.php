<?php
/**
 * @desc
 *  Registration form JS Template
 */
?>

<?php $buttonText = isset($buttonText) ? $buttonText : 'Start Talking'; ?>
<?php $tcText = isset($tcText) ? $tcText : 'In registering for our service you are agreeing to our <a target="_blank" href="'.url_for('@terms_and_conditions').'">terms and conditions.</a>'?>
<?php $buttonCssClass = isset($buttonCssClass) ? $buttonCssClass : (($isLegacyCss === true) ? 'legacy_button button-orange large' : 'cx_button cx_button_orange'); ?>
<?php $formCssClass = ($isLegacyCss === true) ? 'legacy_form' : 'cx_form'; ?>

<?php if (isset($callingActionName)) : ?>
    <?php
        /**
         * The New Music on Hold Page, use Foundation 5 and requires different markup
         * inside the form to allow responsive behaviour on different devices.
         * URI: /d
         */
    ?>
    <?php if ($callingActionName === 'onMusicHoldLandingPage'): ?>
        <!-- START KO Template registration_form -->
        <div id="<?php echo $koTemplateId?>" data-bind="template: {name: '<?php echo $koTemplateDataId?>'}"></div>
            <script type="text/html" id="<?php echo $koTemplateDataId?>">
                <form class="ko_registration_form <?php echo $formCssClass; ?>" data-bind="submit: register" novalidate>
                <div class="row">
                    <div class="large-5 medium-6 medium-offset-1 columns">
                        <input type='email' data-bind='value: registrationModel.email, valueUpdate: "afterkeydown", enable: isEnabled.email, click: clearErrorWindowOnClick' placeholder="Enter your email address" />
                        <img id="loader" src="/cx2/img/throbber.gif" alt="loading..." class="loader" data-bind="visible: isEnabled.loader"/>
                        <div class="error_message" data-bind="fadeVisible: registrationErrors.errors().length > 0">
                            <p class="error" data-bind="html: registrationErrors.errors() "></p>
                        </div>
                    </div>
                    <div class="large-5 medium-4 columns end">
                        <button class="custom-button-orange" type="submit" data-bind="enable: isEnabled.submit">
                            <?php echo $buttonText; ?>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="large-11 medium-11 medium-offset-1 columns">
                        <p class="terms_conditions">
                            <small><?php echo html_entity_decode($tcText); ?></small>
                        </p>
                    </div>
                </div>
                </form>
            </script>
        <!-- END KO Template registration_form -->
    <?php endif; ?>
<?php else : ?>
    <!-- START KO Template registration_form -->
    <div id="<?php echo $koTemplateId?>" data-bind="template: {name: '<?php echo $koTemplateDataId?>'}"></div>
    <script type="text/html" id="<?php echo $koTemplateDataId?>">
        <form class="ko_registration_form <?php echo $formCssClass; ?>" data-bind="submit: register" novalidate>
            <?php if(isset($seoPage) && $seoPage) : ?>
                <p class="registration_form_pre">Our one-step sign-up means you can start conference calling right now</p>
                <input type='text' data-bind='value: registrationModel.email, valueUpdate: "afterkeydown"' placeholder="user@domain.com" />
                <img id="loader" src="/cx2/img/throbber.gif" alt="loading..." class="loader throbber" data-bind="visible: isEnabled.loader" />
                <button class="<?php echo $buttonCssClass; ?>" type="submit" data-bind="enable: isEnabled.submit">
                    <?php echo $buttonText; ?>
                </button>
                <div class="error_message" data-bind="css:{fix_legacy_positioning: registrationErrors.errors().length > 50}, fadeVisible: registrationErrors.errors().length > 0">
                    <p class="error" data-bind="html: registrationErrors.errors() "></p>
                </div>
                <p class="registration_form_post"><?php echo html_entity_decode($tcText); ?></p>
            <?php else : ?>
                <input type='email' data-bind='value: registrationModel.email, valueUpdate: "afterkeydown", enable: isEnabled.email, click: clearErrorWindowOnClick' placeholder="Enter your email address to get started" />
                <img id="loader" src="/cx2/img/throbber.gif" alt="loading..." class="loader throbber" data-bind="visible: isEnabled.loader"/>
                <!-- .fix_legacy_positioning will be removed when we migrate to CX. -->
                <div class="error_message" data-bind="css:{fix_legacy_positioning: registrationErrors.errors().length > 50}, fadeVisible: registrationErrors.errors().length > 0">
                    <p class="error" data-bind="html: registrationErrors.errors() "></p>
                </div>
                <p class="terms_conditions">
                    <small><?php echo html_entity_decode($tcText); ?></small>
                </p>
                <button class="<?php echo $buttonCssClass; ?>" type="submit" data-bind="enable: isEnabled.submit">
                    <?php echo $buttonText; ?>
                </button>
            <?php endif; ?>
        </form>
    </script>
    <!-- END KO Template registration_form -->
<?php endif; ?>