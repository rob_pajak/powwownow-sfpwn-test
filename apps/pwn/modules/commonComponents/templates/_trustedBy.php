<?php if ($pageName == 'callMinuteBundle') : ?>
<div class="grid_24">
    <div class="grey-block clearfix">
        <div class="left">
            <p><strong>Powwownow is trusted by:</strong></p>
            <div class="white-block" style="padding: 10px;">
                <img src="/sfimages/motorla-solutions.jpg" alt="Motorola Solutions" style="height:72px;margin-right:5px;">
                <img src="/sfimages/university-brighton.jpg" alt="University of Brighton" style="height:72px;margin-right:5px;">
                <img src="/sfimages/network-marketing.jpg" alt="networkmarketing" style="height:72px;margin-right:5px;">
                <img src="/sfimages/accenture.jpg" alt="accenture" style="height:72px">
            </div>
            <p>...And over 100,000 people and businesses around the world!</p>
        </div>
        <div class="right">
            <p><strong>Powwownow is so easy to use and costs so little; That's why...</strong></p>
            <img src="/sfimages/customers-recommend-v2.gif" alt="98% of our customers WOULD recommend us">
            <p>Read our customers testimonials <a href="<?php echo url_for('@testimonials'); ?>" class="grey">here</a></p>
        </div>
    </div>
</div>
<?php else : ?>
<div class="grey-block grid_24 clearfix index-costs-compare">
    <div class="right">
        <p><strong>Powwownow is so easy to use and costs so little; That's why...</strong></p>
        <img src="/sfimages/customers-recommend-v2.gif" alt="98% of our customers WOULD recommend us">
        <p>Read our customers testimonials <a href="<?php echo url_for('@testimonials'); ?>" class="grey">here</a></p>
    </div>
    <div class="left">
        <p><strong>Powwownow is trusted by:</strong></p>
        <div class="white-block">
            <img src="/sfimages/motorla-solutions.jpg" alt="Motorola Solutions" style="height:72px;margin-right:5px;">
            <img src="/sfimages/university-brighton.jpg" alt="University of Brighton" style="height:72px;margin-right:5px;">
            <img src="/sfimages/network-marketing.jpg" alt="networkmarketing" style="height:72px;margin-right:5px;">
            <img src="/sfimages/accenture.jpg" alt="accenture" style="height:72px">
        </div>
        <p>...And over 100,000 people and businesses around the world!</p>
    </div>
</div>
<?php endif; ?>