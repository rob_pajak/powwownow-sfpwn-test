<?php $activeItem = isset($activeItem) ? $activeItem : ''; //Fixes PHP Warnings for 404 pages ?>
<ul class="texty-page-menu">
    <?php include_partial('commonComponents/leftNavItems', array('leftNavItems' => $leftNavItems, 'activeItem' => $activeItem));?>
</ul>
