<?php use_stylesheet('/shared/assets/stylesheets/responsiveCallingCard.css'); // SASS file located in/shared/assets/sass/responsiveCallingCard.scss. ?>
<div class="large-12 columns responsive-calling-card">
    <div class="panel green rounded-corners-10 clearfix">
        <h2 class="header-style-1">Start your conference call now... </h2>

        <section class="large-7 push-5 columns">
            <div class="lozenge lozenge-style-1 your-pin-details">
                <p class="paragraph-font-11">Your dial-in number:</p>
                <p class="paragraph-font-12"><strong class="generate-dial-in-number"><?php echo $dialInNumber; ?></strong></p>
                <p class="paragraph-font-11 font-grey">Your UK mobile shortcode is*:</p>
                <p class="paragraph-font-12"><strong class="generated-shortcode"><?php echo $mobileNumber; ?></strong></p>
                <p class="paragraph-font-11">Your PIN is:</p>
                <?php if (isset($pin) && $pin !== false):?>
                    <p class="paragraph-font-12"><strong class="generated-pin"><?php echo $pin?></strong></p>
                <?php endif;?>
            </div>
            <p class="font-white paragraph-font-8 text-center">* Dedicated shortcode that gives you cheaper mobile call rates, charged at 12.5ppm + VAT from any UK mobile provider</p>
        </section>

        <section id="start-conferencing" class="start-conferencing large-5 pull-7 columns">
            <ul class="how-it-works-list list-font-11 font-white">
                <li class="list-item">
                    <span class="sprite first"></span>
                    <span class="description">Share your PIN and dial-in number with your call participants</span>
                </li>
                <li class="list-item">
                    <span class="sprite second"></span>
                    <span class="description">At the agreed time, dial the number and enter your PIN</span>
                </li>
                <li class="list-item">
                    <span class="sprite third"></span>
                    <span class="description">That's it! Start talking!</span>
                </li>
            </ul>
        </section>
        <div class="row">
            <div class="large-12 columns">
                <p class="font-white paragraph-font-18 font-rockwell">Repeat this anytime you need a conference call. This is your PIN for life!</p>
            </div>
        </div>
    </div>
</div>