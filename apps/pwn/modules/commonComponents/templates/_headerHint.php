<div style="position: relative;">
	<div class="png" id="header-hint">
		<div style="padding-bottom: 5px" class="notLogged  <?php if ($authenticated == true) echo "hidden" ?>">
			<span class="rockwell green-dark font-large">Existing users</span>
		</div>
		<div style="padding-bottom: 3px" class="logged <?php if ($authenticated != true) echo "hidden" ?>">
			<span class="rockwell green-dark font-large">Check your account</span>
		</div>
		<div id="header-hint-content">
			<span class="icon icon-white floatleft"><span class="icon-padlock-orange png"></span><!--#empty tag saver--></span>
			<div class="notLogged <?php if ($authenticated == true) echo "hidden" ?>">
				<a href="http://www.powwownow.co.uk/Create-A-Login" class="darkgreen">Create a login</a>&nbsp;to unlock your features and benefits
			</div>
			<div class="logged <?php if ($authenticated != true) echo "hidden" ?>">
				<a class="darkgreen" href="http://www.powwownow.co.uk/mypwn/mypins">Enter</a>&nbsp;myPowwownow to manage call settings, use Scheduler and more
			</div>
		</div>
	</div>
</div>