<?php if ($showBanner) : ?>
<div class="grid_24 clearfix iMeetBanner">
    <!--Right Side Logo-->
    <div class="grid_6 iMeetBannerLeft">
        <img src="<?php echo getDataURI("/sfimages/iMeetLogo.png") ?>" alt="IMeet Logo"/>
    </div>
    <div class="grid_18 iMeetBannerRight">
        <!--Left Side Top Row-->
        <div class="grid_24 noMargin">
            <span class="iMeetBannerFontShare">You're one of the lucky few we've invited to get a sneak peak at our <br/><a href="<?php echo $iMeetRoomLink; ?>" class="iMeetBannerLinkShare" target="_blank">new screen sharing app</a> iMeet.</span><br/>
            <span class="iMeetBannerFontFeedback">We'd love for you to try it out on a call and give us some feedback on how you got on.</span>
        </div>

        <!--Left Side Bottom Row-->
        <div class="grid_19 iMeetBannerSecondParagraph">
            <span class="iMeetBannerFontRoom">Your room: <a href="<?php echo $iMeetRoomLink; ?>" class="iMeetBannerFontRoomName" target="_blank"><?php echo $iMeetRoomLink; ?></a></span><br/>
            <span class="iMeetBannerFontTip">Tip: </span>
            <span class="iMeetBannerFontForgot">If you've forgotten your login details, you can reset them in your room.</span>
        </div>

        <!--Left Side Bottom Row, Links-->
        <div class="grid_5 iMeetBannerLinks">
            <a href="<?php echo $iMeetRoomLink; ?>" class="cx_button cx_button_blue_j">Try iMeet</a><br/><br/>
            <!--<a href="" class="iMeetBannerSurveyFont">Tell us what you think</a>-->
        </div>
    </div>
</div>
<?php endif; ?>
