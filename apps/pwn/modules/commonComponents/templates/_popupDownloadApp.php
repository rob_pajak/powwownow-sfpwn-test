<div class="mypwn_footer_title title-<?php echo $service; ?>">
    <div style="display:none;" class="tooltip-footer-mobile-apps tooltip">
        <a class="tooltip-footer-mobile-apps-close floatright sprite tooltip-close" href="#">
            <span class="sprite-cross-white png"></span>
        </a>
        <h4 class="aligncenter">Mobile app</h4>
        <p style="font-weight:normal;"><br/>Schedule conferences, set automatic call alerts plus store your details so joining calls is only ever one touch away.<br/></p>
        <a title="Android App" href="https://market.android.com/details?id=com.grppl.android.shell.CMBpwn&amp;feature=search_result" target="_blank" style="font-weight:bold;">Android App</a><br/>
        <a title="iPhone App" href="http://app.powwownow.com" target="_blank" style="font-weight:bold;">iPhone App</a><br/>
        <div class="tooltip-arrow-bottom png"></div>
    </div>
</div>