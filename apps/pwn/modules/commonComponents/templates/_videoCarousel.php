<div class="grid_24" id="videoCarouselContainer">
    <h1 class="rockwell green"><?php echo $heading; ?></h1>
    <p class="subheading"><?php echo $subHeading; ?></p>
    <ul id="videos" class="jcarousel-skin-pwn">
        <?php foreach ($videoOrder as $videoId => $videoName) : ?>
        <li>
            <div class="polaroid">
                <h3 class="rockwell grey-dark"><?php echo $video[$videoName]['h3']; ?></h3>
                <img class="prettyPhoto" src="<?php echo $video[$videoName]['image']; ?>" width="279" height="123" alt="" onclick="runPlayer('<?php echo $video[$videoName]['loadVideo']; ?>', '<?php echo $video[$videoName]['tracking']; ?>', '#video<?php echo ($videoId+1); ?>');"/>
                <div id="video<?php echo ($videoId+1); ?>" class="video" title="<?php echo $video[$videoName]['title']; ?>"></div>
                <p class="hidden"><?php echo $video[$videoName]['caption']; ?></p>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<script>
    function runPlayer(divElement, tracking, videoElement) {
        var videoElementSelector = $(videoElement),
            actualVideoSelector = $(videoElement + '>video');

        // Create Dialog
        videoElementSelector.dialog({
            autoOpen: false,
            width: "auto",
            height: "auto",
            modal: true,
            draggable: false,
            dialogClass: "videoCarousel",
            close: function (event, ui) {
                actualVideoSelector.trigger("stop");
                videoElementSelector.remove();
            }
        });

        // Open Video
        videoElementSelector.html(divElement);
        videoElementSelector.dialog("open");
        actualVideoSelector.trigger("play");
        actualVideoSelector.on("ended", function () {
            actualVideoSelector[0].currentTime = 1;
        });

        dataLayer.push({'event': tracking});
    }

    $(document).ready(function () {
        var videosContainer = $('#videos');
        if (videosContainer.is(':visible')) {
            videosContainer.jcarousel({ visible: 3, scroll: 3 });
            videosContainer.find('.polaroid').polaroid({zoom: 1.6});
        }
    });
</script>