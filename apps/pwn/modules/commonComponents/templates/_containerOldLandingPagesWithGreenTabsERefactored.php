<?php use_stylesheet('/sfcss/components/commonComponents.containerOldLandingPagesWithGreenTabsE.css'); ?>

<div id="tabs-grid" class="grid_18">
    <h1 class="tab_page_heading"><?php echo $header; ?></h1>
    <div class="subheading">
        <?php foreach ($sf_data->getRaw('subHeader') as $sh): ?>
            <h3><?php echo $sh; ?></h3>
        <?php endforeach; ?>
    </div>
    <?php include_component('commonComponents', 'hintBox', array('title' => $hintBoxTitle, 'contents' => $hintBoxContent)); ?>

    <div class="landingpage-tab-1 png active" id="tab-1">
        <div class="rockwell font-large white"><?php echo $tabTitle; ?></div>
        <div style="display:none" class="tab-shadow png"><!--Blank--></div>
    </div>

    <div>
        <div id="landingpage-tabs-container">
            <div class="white" id="tab-pwn">
                <div class="grid_sub_24">
                    <section class="register_box tabbed-registration">
                        <h2 class="white">Our one-step sign-up means you can start conference calling right now</h2>
                        <?php include_component(
                            'commonComponents',
                            'jsKoCxRegistrationTemplate',
                            array(
                                'koTemplateId' => 'ko.generic.registration.container',
                                'koTemplateDataId' => 'ko.generic.registration.data',
                                'buttonText' => 'Generate Pin',
                                'isLegacyCss' => true
                            )
                        ); ?>
                    </section>
                </div>

                <div class="grid_sub_24">
                    <ul id="features-list">
                        <li class="png">Just pay the cost of your own 0844 call</li>
                        <li class="png">Free <a class="white" title="iphone conferencing app" href="http://app.powwownow.com" target="_blank">iPhone app</a></li>
                        <li class="png">Low-cost <a class="white" title="International Number Rates" href="<?php echo url_for("@international_number_rates"); ?>">international dial-in access</a></li>
                        <li class="png">Free <a class="white" title="Powwownow Web Conferencing" href="<?php echo url_for("@web_conferencing"); ?>">web conferencing</a><br></li>
                        <li class="png">Free call recordings</li>
                        <li class="png">Free <a class="white" title="Scheduler Tool" href="http://myscheduler.powwownow.com/">scheduler tool</a> to invite participants</li>
                        <li class="png">Free handy wallet card</li>
                        <li class="png">A choice of on-hold music</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>