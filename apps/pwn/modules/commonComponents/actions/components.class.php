<?php

class commonComponentsComponents extends sfComponents
{

    private $defaultFlow = array(
        'Payment',
        'Purchase Confirmation',
    );

    /**
     * Hint Box. Mainly used on the Landing Pages
     * @author Asfer Tamimi
     *
     */
    public function executeHintBox()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));
        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Hint Box Title
        if (!isset($this->title) || is_null($this->title)) {
            $this->title = "It's easy as 1, 2, 3!";
        }

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $this->dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $this->getUser()->getCulture(),
                'service_ref'    => $this->getUser()->getAttribute('service_ref'),
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
    }

    /**
     * This container is used for the old Landing pages on the E Version
     * @author Asfer Tamimi
     * @amended Robert
     *
     */
    public function executeContainerOldLandingPagesWithGreenTabsERefactored()
    {
        // Page Header and Sub Header
        if (!isset($this->header)) {
            $this->header = 'Page Header';
        }
        if (!isset($this->subHeader)) {
            $this->subHeader = 'Page Sub Header';
        }

        // Hint Box Information
        if (!isset($this->hintBoxTitle)) {
            $this->hintBoxTitle = null;
        }
        if (!isset($this->hintBoxContent)) {
            $this->hintBoxContent = null;
        }

        // Tab Title
        if (!isset($this->tabTitle)) {
            $this->tabTitle = 'Get started';
        }

        // Check if PIN has been registered by Checking Session.
        // Original Code used to store the Information in $_COOKIE, but its better if it was in $_SESSION
        $this->registeredPin = $this->getUser()->getAttribute('registered_pin', false);

        // Registration Source for the Unified Ajax
        if (!isset($this->registrationSource)) {
            $this->registrationSource = 'registration_source';
        }
    }


    /**
     * This container is used for the old Landing pages on the E Version
     *
     * @author Asfer Tamimi
     *
     */
    public function executeContainerOldLandingPagesWithGreenTabsE()
    {
        // Page Header and Sub Header
        if (!isset($this->header)) {
            $this->header = 'Page Header';
        }
        if (!isset($this->subHeader)) {
            $this->subHeader = 'Page Sub Header';
        }

        // Hint Box Information
        if (!isset($this->hintBoxTitle)) {
            $this->hintBoxTitle = null;
        }
        if (!isset($this->hintBoxContent)) {
            $this->hintBoxContent = null;
        }

        // Tab Title
        if (!isset($this->tabTitle)) {
            $this->tabTitle = 'Get started';
        }

        // Check if PIN has been registered by Checking Session.
        // Original Code used to store the Information in $_COOKIE, but its better if it was in $_SESSION
        $this->registeredPin = $this->getUser()->getAttribute('registered_pin', false);

        // Registration Source for the Unified Ajax
        if (!isset($this->registrationSource)) {
            $this->registrationSource = 'registration_source';
        }
    }

    /**
     * Footer Component
     *
     * This is the Footer, used on all pages.
     *
     * @author Robert + Asfer Tamimi (Complete Rewrite)
     *
     */
    public function executeFooter()
    {
        $this->footerLinks        = sfConfig::get('app_footer_links');
        $this->socialLinks        = sfConfig::get('app_social_links');
        $this->internationalLinks = sfConfig::get('app_international_links');
        $this->showInternational  = (isset($this->showInternational)) ? $this->showInternational : true;
    }

    /**
     * This is the Left Navigation Column
     *
     * The active link can be set here like so:
     *     $this->activeItem = 'Tell a friend';
     *
     * @todo - Whole Component needs to be done
     *
     * @author Asfer
     *
     */
    public function executeLeftNav()
    {

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@about_us'),
                'description' => 'About us',
            ),
            array(
                'url'         => url_for('@how_we_are_different'),
                'description' => 'How we are different',
            ),
            array(
                'url'         => url_for('@going_green'),
                'description' => 'Going green',
            ),
            array(
                'url'         => url_for('@business_efficiency'),
                'description' => 'Business efficiency',
            ),
            array(
                'url'         => url_for('@news'),
                'description' => 'Latest news and press',
            ),
            array(
                'url'         => '/blog',
                'description' => 'Blog',
            ),
            array(
                'url'         => url_for('@tell_a_friend'),
                'description' => 'Tell a friend',
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the unsubscribe page.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     *
     * @stole executeConferenceCallLeftNav
     * @author Alexander Farrow
     *
     */
    public function executeUnsubscribeLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@about_us'),
                'description' => 'About us',
            ),
            array(
                'url'         => url_for('@privacy'),
                'description' => 'Privacy',
            ),
            array(
                'url'         => url_for('@terms_and_conditions'),
                'description' => 'Terms and conditions'
            ),
            array(
                'url'         => url_for('@unsubscribe'),
                'description' => 'Unsubscribe'
            ),
            array(
                'url'         => url_for('@faqs'),
                'description' => 'FAQs'
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
        );
    }


    /**
     * Displays the left-hand side navigation used for the glossary page.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     *
     * @stole executeConferenceCallLeftNav
     * @author Alexander Farrow
     *
     */
    public function executeGlossaryLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@costs'),
                'description' => 'What it costs',
            ),
            array(
                'url'         => url_for('@how_conference_calling_works'),
                'description' => 'How it works',
            ),
            array(
                'url'         => url_for('@international_number_rates'),
                'description' => 'International dial-in numbers'
            ),
            array(
                'url'         => url_for('@compare_services'),
                'description' => 'Compare services'
            ),
            array(
                'url'         => url_for('@glossary'),
                'description' => 'Glossary'
            ),
            array(
                'url'         => url_for('@faqs'),
                'description' => 'FAQs'
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the sitemap page.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     *
     * @stole executeConferenceCallLeftNav
     * @author Alexander Farrow
     *
     */
    public function executeSitemapLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@about_us'),
                'description' => 'About us',
            ),
            array(
                'url'         => url_for('@privacy'),
                'description' => 'Privacy',
            ),
            array(
                'url'         => url_for('@useful_links'),
                'description' => 'Useful links'
            ),
            array(
                'url'         => '/blog',
                'description' => 'Blog'
            ),
            array(
                'url'         => url_for('@sitemap'),
                'description' => 'Sitemap'
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the conference-call pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     */
    public function executeConferenceCallLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@conference_call_services_reporting'),
                'description' => 'Reporting',
            ),
            array(
                'url'         => url_for('@top_ten_tips_for_web_conferencing'),
                'description' => 'Top 10 tips for web conferencing',
            ),
            array(
                'url'         => url_for('@conference_call_branding'),
                'description' => 'Branding',
            ),
            array(
                'url'         => url_for('@security'),
                'description' => 'Security',
            ),
            array(
                'url'         => url_for('@mobile_phone_conference_call'),
                'description' => 'Mobile',
            ),
            array(
                'url'         => url_for('@conference_call_services_powwownow_and_skype'),
                'description' => 'Skype',
            ),
            array(
                'url'         => url_for('@testimonials'),
                'description' => 'Testimonials',
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the conference-call sub pages (part 2).
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     */
    public function executeConferenceCallSubPages2LeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@conference_call_retail'),
                'description' => 'Retail',
            ),
            array(
                'url'         => url_for('@conference_call_finance'),
                'description' => 'Finance',
            ),
            array(
                'url'         => url_for('@conference_call_manufacturing'),
                'description' => 'Manufacturing',
            ),
            array(
                'url'         => url_for('@conference_call_consultants'),
                'description' => 'Consultants',
            ),
            array(
                'url'         => url_for('@conference_call_it_services'),
                'description' => 'IT services',
            ),
            array(
                'url'         => url_for('@conference_call_public_sector'),
                'description' => 'Public sector',
            ),
            array(
                'url'         => url_for('@conference_call_pharmaceutical'),
                'description' => 'Pharmaceutical',
            ),
            array(
                'url'         => url_for('@conference_call_charities'),
                'description' => 'Charities',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the conference-call solutions pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     */
    public function executeConferenceCallSolutionsLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@conference_call_solutions_meetings'),
                'description' => 'Meetings',
            ),
            array(
                'url'         => url_for('@conference_call_solutions_live_events_and_webinars'),
                'description' => 'Events and webinars',
            ),
            array(
                'url'         => url_for('@conference_call_solutions_training'),
                'description' => 'Training',
            ),
            array(
                'url'         => url_for('@conference_call_solutions_technical_support'),
                'description' => 'Technical support',
            ),
            array(
                'url'         => url_for('@conference_call_solutions_product_and_product_launches'),
                'description' => 'Project and product launches',
            ),
        );
    }


    /**
     * Displays the left-hand side navigation used for the Contact Us pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     *
     * @stole executeConferenceCallLeftNav
     * @author Asfer Tamimi
     *
     */
    public function executeContactUsLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@about_us'),
                'description' => 'About us',
            ),
            array(
                'url'         => url_for('@costs'),
                'description' => 'What it costs',
            ),
            array(
                'url'         => '/How-Conferencing-Calling-Works',
                'description' => 'How conference calling works',
            ),
            array(
                'url'         => url_for('@how_web_conferencing_works'),
                'description' => 'How web conferencing works',
            ),
            array(
                'url'         => url_for('@international_number_rates'),
                'description' => 'International numbers & rates',
            ),
            array(
                'url'         => url_for('@compare_services'),
                'description' => 'Compare services',
            ),
            array(
                'url'         => url_for('@event_conference_calls'),
                'description' => 'Event calls',
            ),
            array(
                'url'         => url_for('@faqs'),
                'description' => 'FAQs',
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
            array(
                'url'         => url_for('@forgotten_password'),
                'description' => 'Forgotten password',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the News pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     *
     * @stole executeConferenceCallLeftNav
     * @author Asfer Tamimi
     *
     */
    public function executeNewsLeftNav()
    {

        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');

        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@about_us'),
                'description' => 'About us',
            ),
            array(
                'url'         => url_for('@how_we_are_different'),
                'description' => 'How we are different',
            ),
            array(
                'url'         => url_for('@going_green'),
                'description' => 'Going green',
            ),
            array(
                'url'         => url_for('@business_efficiency'),
                'description' => 'Business efficiency',
            ),
            array(
                'url'         => url_for('@news'),
                'description' => 'Latest news and press',
            ),
            array(
                'url'         => '/blog',
                'description' => 'Blog',
            ),
            array(
                'url'         => url_for('@tell_a_friend'),
                'description' => 'Tell a friend',
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
            array(
                'url'         => url_for("@meeting_cost_calculator"),
                'description' => 'Meeting Cost Calculator',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the News pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     *
     * @stole executeConferenceCallLeftNav
     * @author Asfer Tamimi
     *
     */
    public function executeCareersLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@about_us'),
                'description' => 'About us',
            ),
            array(
                'url'         => url_for('@careers'),
                'description' => 'Careers',
            ),
            array(
                'url'         => url_for('@useful_links'),
                'description' => 'Useful links',
            ),
            array(
                'url'         => '/blog',
                'description' => 'Blog',
            ),
            array(
                'url'         => '/Sitemap',
                'description' => 'Site map',
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the News pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     *
     * @stole executeConferenceCallLeftNav
     * @author Asfer Tamimi
     *
     */
    public function executeTermsAndConditionsLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@about_us'),
                'description' => 'About us',
            ),
            array(
                'url'         => url_for('@privacy'),
                'description' => 'Privacy',
            ),
            array(
                'url'         => url_for('@terms_and_conditions'),
                'description' => 'Terms and conditions',
            ),
            array(
                'url'         => url_for('@useful_links'),
                'description' => 'Useful links',
            ),
            array(
                'url'         => '/Sitemap',
                'description' => 'Site map',
            ),
            array(
                'url'         => url_for('@contact_us'),
                'description' => 'Contact us',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the conference-call pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     */
    public function executeConferenceCallSubPagesLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@top_ten_tips_for_conference_calling'),
                'description' => 'Top 10 tips for conference calling',
            ),
            array(
                'url'         => url_for('@conference_call_etiquette'),
                'description' => 'Conference call etiquette',
            ),
            array(
                'url'         => url_for('@conference_call_headsets'),
                'description' => 'Conference call headsets',
            ),
            array(
                'url'         => url_for('@conference_call_time_zones'),
                'description' => 'Conference call time zones',
            ),
            array(
                'url'         => url_for('@conference_call_what_is_a_conference_call'),
                'description' => 'What is a conference call',
            ),
            array(
                'url'         => url_for('@conference_call_set_up_a_conference_call'),
                'description' => 'Set up a conference call',
            ),
            array(
                'url'         => url_for('@conference_call_how_to_conference_call'),
                'description' => 'How to conference call',
            ),
            array(
                'url'         => url_for('@conference_call_services_powwownow_and_skype'),
                'description' => 'Skype',
            ),
            array(
                'url'         => url_for('@conference_call_services_reporting'),
                'description' => 'Reporting',
            ),
            array(
                'url'         => url_for('@conference_call_branding'),
                'description' => 'Branding',
            ),
            array(
                'url'         => url_for('@mobile_phone_conference_call'),
                'description' => 'Mobile',
            ),
            array(
                'url'         => url_for('@security'),
                'description' => 'Security',
            ),
        );
    }

    /**
     * Displays the left-hand side navigation used for the conference-call pages.
     *
     * Uses the activeItem property for the active menu item, which is compared against the description of the menu item,
     * which doubles as the label.
     */
    public function executeWebConferencingLeftNav()
    {
        if (!isset($this->activeItem)) {
            $this->activeItem = null;
        }

        $this->leftNavItems = array(
            array(
                'url'         => url_for('@how_web_conferencing_works'),
                'description' => 'How web conferencing works',
            ),
            array(
                'url'         => url_for('@set_up_a_web_conference'),
                'description' => 'Setting up a Web conference',
            ),
            array(
                'url'         => url_for('@how_to_hold_a_web_conference'),
                'description' => 'How to hold a web conference',
            ),
            array(
                'url'         => url_for('@web_conferencing_demo'),
                'description' => 'Watch a demo',
            ),
            array(
                'url'         => url_for('@web_conferencing_get_started'),
                'description' => 'Get started',
            ),
            array(
                'url'         => 'http://powwownow.yuuguu.com/',
                'description' => 'Join a session',
            ),
        );
    }

    /**
     * Show the Header.
     * Based on "CX" design.
     *
     */
    public function executeHeader()
    {
        // Load Helpers
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('hermesCallWithCaching'));

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        /** @var myUser $user */
        $user = $this->getUser();

        // Additional Stylesheets and Javascript Files To be used with the Header
        $response->addStylesheet('/cx2/stylesheets/header.css');
        $response->addJavascript('/sfjs/components/commonComponents.header.js');

        $isAuthenticated = $user->isAuthenticated();

        $isAuthenticatedPage = sfContext::getInstance()
            ->getController()
            ->getActionStack()
            ->getEntry(0)
            ->getActionInstance()
            ->isSecure();

        $isPublicPage = (isset($isPublicPage)) ? $isPublicPage : !$isAuthenticatedPage;
        $logos        = sfConfig::get('app_logos');
        $service      = $user->getAttribute('service', false);
        $accountId    = $user->getAttribute('account_id', false);
        $contactRef   = $user->getAttribute('contact_ref', false);
        $serviceUser  = $user->getAttribute('service_user', 'UNAUTHENTICATED');

        $accountUsers = false;
        // Balance Information
        if ('plus' === $service && $isAuthenticated) {
            $accountInfo  = plusCommon::getAccountBalanceAndUsers($accountId, $contactRef);
            $accountUsers = $accountInfo['accountUsers'];
        } elseif ('plus' === $service) {
            $accountUsers = 'S';
        }

        // Premium Customer Information
        if ($service === 'premium' && $contactRef !== false) {
            try {
                $customerInfo = hermesCallWithCaching(
                    'getPremiumCustomerByContact',
                    array('contact_ref' => $contactRef),
                    'array',
                    3600
                );
            } catch (Exception $e) {
                $this->logMessage(
                    'getPremiumCustomerByContact Failed to Return a Result for Contact_ref :: ' . $contactRef . ',
                     Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode(),
                    'err'
                );
            }
            if (isset($customerInfo) && is_array($customerInfo)) {
                foreach ($customerInfo as $customer) {
                    if ($user->hasCredential('PREMIUM_ADMIN') && $customer['contact_type'] === 'Admin') {
                        $customerCode = isset($customer['customer_code']) ? $customer['customer_code'] : '';
                        break;
                    }
                    if ($user->hasCredential('PREMIUM_USER') && $customer['contact_type'] === 'Chairman') {
                        $customerCode = isset($customer['customer_code']) ? $customer['customer_code'] : '';
                        break;
                    }
                    break;
                }

                // Check if the Image Exists for the Premium Customer
                if (isset($customerCode) && $customerCode !== false) {
                    $imageFile = '/sfimages/branding/' . $customerCode . '.jpg';
                    if (file_exists(getcwd() . $imageFile)) {
                        $customerLogo = '<div class="customer_logo" style="background-image: url(' . $imageFile . ')"></div>' . "\n";
                    }
                }
            }
        }

        $isPremiumUser = ($isAuthenticated && $service == 'premium' && !$isPublicPage);

        // Header Navigation
        $navigation      = new Navigation();
        $navigationLinks = $navigation->getNavigationLinks($user);

        // Header Navigation
        $headerNavigationArgs = array(
            'navigation'    => ($isPublicPage) ? $navigationLinks['UNAUTHENTICATED'] : $navigationLinks[$serviceUser],
            'service'       => ($isPublicPage) ? false : $service,
            'serviceUser'   => ($isPublicPage) ? 'UNAUTHENTICATED' : $serviceUser,
            'accountUsers'  => (!isset($accountUsers)) ? false : $accountUsers,
            'headerHint'    => (!isset($headerHint)) ? false : $headerHint,
            'authenticated' => $isAuthenticated
        );

        // Required View Variables
        $this->setVar('currentCountry', sfConfig::get('app_default_country'));
        $this->setVar('isPublicPage', $isPublicPage);
        $this->setVar('logos', $logos);
        $this->setVar('accountUsers', $accountUsers);
        $this->setVar('navigationLinks', $navigationLinks);
        $this->setVar('isPremiumUser', $isPremiumUser);
        $this->setVar('headerNavigationArgs', $headerNavigationArgs);

        // Optional View Variables
        $this->setVar('customerLogo', isset($customerLogo) ? $customerLogo : null);
    }

    /**
     * Sub Page Header Component
     *
     * This is the Sub Page Header, used on all pages.
     *
     * @author Asfer
     *
     */
    public function executeSubPageHeader()
    {
        if (!isset($this->breadcrumbs)) {
            $this->breadcrumbs = array();
        }
        if (!isset($this->user_type)) {
            $this->user_type = $this->getUser()->getAttribute('service', 'plus');
        }
        if (!isset($this->headingSize)) {
            $this->headingSize = 's';
        }
    }

    /**
     * Generic Heading Component
     *
     * This is the Generic Page Heading
     * The difference between Sub Page Header is that it's not constrained in grid class
     *
     */
    public function executeGenericHeading()
    {
        $this->title       = (isset($this->title)) ? $this->title : '';
        $this->headingSize = (isset($this->headingSize)) ? $this->headingSize : 's';
        $this->user_type   = (isset($this->user_type)) ? $this->user_type : 'powwownow';
    }

    /**
     * Flow Bar - This is the Products Flow Bar
     *
     *
     * @author Asfer
     *
     */
    public function executeFlowBar()
    {

        // Check if the User is Not an Admin. Return Nothing
        if (!$this->getUser()->hasCredential('PLUS_ADMIN')) {
            return sfView::NONE;
        }

        // Obtain the Account Balance and the Account Users
        $accountInfo = plusCommon::getAccountBalanceAndUsers(
            $this->getUser()->getAttribute('account_id'),
            $this->getUser()->getAttribute('contact_ref')
        );

        // Retrieve the minimum cost to determine which parts of the flow will be visible.
        $minimumCredit = (isset($this->minimumCredit)) ? $this->minimumCredit : 5;

        // Allow overriding of the balance value.
        if (isset($this->balance) && is_numeric($this->balance) && $this->balance > 0) {
            $accountInfo['balance'] = $this->balance;
        } else {
            $this->balance = $accountInfo['balance'];
        }

        // Override the Balance. This forces the Flow to be changed.
        if (isset($this->override) && 'balance' === $this->override && $accountInfo['balance'] >= $minimumCredit) {
            $accountInfo['balance'] = 0;
        } elseif (isset($this->override) && 'balance' === $this->override && $accountInfo['balance'] < $minimumCredit) {
            $accountInfo['balance'] = $minimumCredit;
        }

        $this->flow = $this->defaultFlow;

        // Check the Step
        if (!isset($this->step)) {
            $this->step = 0;
        }

        if ($this->step == 'last') {
            $this->step = count($this->flow) - 1;
        }

        if ($this->step < 0) {
            $this->step = count($this->flow) - 1 + $this->step;
        }

        // Set the Previous Element in the Flow - Used to Fix the Internet Explorer 8 Issue on the Flow
        $this->previous = $this->step - 1;

        if (!empty($this->forceStepStyles)) {
            $this->globalStyle = 'max-width: 25em;';
        }
    }

    private function prepareBasketProductsForDialog(array $products, Basket $basket)
    {
        $processed = array();
        foreach ($products as $productId => $product) {
            if (isset($product[1]['BWM_total_cost'])) {
                $processed[] = $this->prepareBWMBasketProductForDialog($productId, $product);
            } elseif (isset($product['type']) && $product['type'] === 'credit') {
                $processed[] = $this->prepareCreditBasketProductForDialog($productId, $product);
            } elseif (isset($product['second_allowance'])) {
                $bundle      = $basket->retrieveBundleObject($productId);
                $processed[] = $this->prepareBundleProductForDialog($bundle);
            }
        }
        return $processed;
    }

    private function prepareCreditBasketProductForDialog($productId, array $product)
    {
        return array(
            'id'    => 'tooltip-product-' . $productId,
            'label' => $product['label'],
            'price' => $product['amount'],
        );
    }

    private function prepareBWMBasketProductForDialog($productId, array $product)
    {
        return array(
            'id'    => 'tooltip-product-' . $productId,
            'label' => $product[0]['bwm_label'],
            'price' => $product[1]['BWM_total_cost']['total'] + $product[1]['BWM_total_cost']['connectionFee'],
        );
    }

    private function prepareBundleProductForDialog(Bundle $bundle)
    {
        return array(
            'id'             => 'tooltip-product-' . $bundle->getProductGroupId(),
            'label'          => $bundle->getGroupName(),
            'prorata_charge' => $bundle->calculateProRataCost(),
            'monthly_charge' => $bundle->getMonthlyPrice(),
        );
    }

    /**
     * PIN Pair Bar. This is used in the Sub Page Header
     *
     * @author Unknown + Maarten + Asfer Tamimi
     *
     */
    public function executePinPairBar()
    {
        // Helpers
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('hermesCallWithCaching', 'dialInNumbers'));

        /** @var myUser $user */
        $user = $this->getUser();

        // Plus Basket
        $basket = new Basket($user);

        $this->basketInBundle = $basket->hasBundleBeenAdded();

        // Session Variables
        $this->authenticated  = $user->isAuthenticated();
        $this->pin            = false;
        $this->chairmanPin    = false;
        $this->enhancedPin    = false;
        $this->firstName      = $user->getAttribute('first_name');
        $this->isPlusAdmin    = $user->hasCredential('PLUS_ADMIN');
        $this->numAddedPr     = ($this->isPlusAdmin) ? count($basket->retrieveAddedProducts()) : null;
        $this->service        = $user->getAttribute('service', false);
        $this->country        = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";
        $this->basketCost     = $basket->calculateTotalCost();
        $this->basketProducts = $this->prepareBasketProductsForDialog(
            array_reverse($basket->retrieveAddedProducts(), true),
            $basket
        );

        // Basket Title
        if (!$this->numAddedPr) {
            $this->basketTitle = "Your basket is empty";
        } else {
            $this->basketTitle = sprintf("Your basket value is £%.2f", $this->basketCost);
        }

        // PIN Information
        $pins = PinHelper::getDefaultPins($user->getAttribute('contact_ref'));
        if ($user->hasCredential('POWWOWNOW')) {
            $this->enhancedPin = $pins['pin'];
        } else {
            $this->chairmanPin = $pins['pin'];
            $this->pin         = $pins['participant_pin'];
        }

        // Dial-in Number Information
        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers       = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $user->getCulture(),
                'service_ref'    => $user->getAttribute('service_ref'),
                'country'        => $this->country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
        $this->dialInNumbers = $dialInNumbers['localList'];
        $this->number        = $dialInNumbers['local'];

        // Plus Account Information
        $this->isPostPayCustomer        = false;
        $this->remainingMinuteAllowance = 0;
        if ('plus' == $this->service) {
            $accountId     = $user->getAttribute('account_id');
            $account       = Hermes_Client_Rest::call(
                'getPlusAccount',
                array(
                    'account_id' => $accountId
                )
            );
            $this->balance = $account['account']['credit_balance_formatted'];

            $this->isPostPayCustomer  = plusCommon::retrievePostPayStatus($accountId);
            $this->outstandingBalance = false;
            if ($this->isPostPayCustomer) {
                $this->remainingMinuteAllowance = plusCommon::getRemainingAllowanceFormatted($accountId);
                $this->outstandingBalance       = $user->getRawBalance(true);
            }
        }
    }

    /**
     * This is the Social Media Footer for the Unauthenticated Pages
     *
     */
    public function executeSocialMediaFooterE()
    {
        $this->getResponse()->addJavascript('/sfjs/utils.js');
        $this->getResponse()->addJavascript('/sfjs/components/commonComponents.socialMediaFooterE.js');
    }

    /**
     * @desc
     *  This is the Right Landing Page Menu, which is used on All Pages, bar the Its-Your-Call related pages
     * @param sfRequest $request
     */
    public function executeRightLandingPageMenu(sfRequest $request)
    {
        $this->getResponse()->addJavascript('/sfjs/mobile-detect.min.js');
        $this->getResponse()->addJavascript('/sfjs/components/commonComponents.rightLandingPageMenu.js');
        $this->getResponse()->addStyleSheet('/sfcss/components/commonComponents.rightLandingPageMenu.css');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));
        $this->setVar('requestSource',  $request->getUri());
        $country             = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $this->getUser()->getCulture(),
                'service_ref'    => $this->getUser()->getAttribute('service_ref'),
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
        $this->setVar('dialInNumber', $dialInNumbers['local']);
        $this->setVar('mobileDialInNumber', $dialInNumbers['mobile']);
    }

    /**
     * This container rotating promotional links and images for E version ( migrated from Web-Conferencing page )
     *
     * @author Michal Macierzynski
     *
     */
    public function executeRotatingPromotialLinksE()
    {
        $this->getResponse()->addJavascript('/sfjs/plus.js');
    }

    /**
     * This container with green tabs for E version ( migrated from Web-Conferening page )
     *
     * @author Michal Macierzynski
     *
     */
    public function executeContainerWithGreenTabsE()
    {
        $this->getResponse()->addJavascript('/shared/flowplayer/flowplayer-3.2.4.min.js');
    }

    /**
     * This is the Authenticated Social Media Footer
     *
     * @todo It needs to be made Refactored.
     * @author Unknown + Asfer Tamimi
     *
     */
    public function executeSocialMediaFooter()
    {
        $this->getResponse()->addJavascript('/sfjs/components/commonComponents.socialMediaFooter.js');

        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
        $this->service      = $this->getUser()->getAttribute('service', false);
        $this->service_user = $this->getUser()->getAttribute('service_user', false);
        $this->user_type    = $this->getUser()->getAttribute('user_type', false);

        // Social Media Footer Information
        $this->items = array(
            'POWWOWNOW'     => array(
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download App',
                    'text'       => 'For conference calling on the move, download our mobile app.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadApp',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-mobile-apps',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download user guide',
                    'text'       => 'For printer-friendly summaries that you can print out and keep to hand.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadUserGuidePowwownow',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-user-guides',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_powwownews',
                    'heading'    => 'News and Press',
                    'text'       => 'Catch up with the latest news from Powwownow.',
                    'link'       => url_for('@news'),
                    'popUp'      => false,
                    'popUpName'  => '',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_tell_a_friend',
                    'heading'    => 'Tell a friend',
                    'text'       => 'Have a friend or colleague who is interested in free and easy conference calls with Powwownow? Spread the word!',
                    'link'       => url_for('@tell_a_friend'),
                    'popUp'      => false,
                    'popUpName'  => '',
                )
            ),
            'PLUS_USER'     => array(
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download App',
                    'text'       => 'For conference calling on the move, download our apps. Select your mobile below:',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadApp',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-mobile-apps',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download user guide',
                    'text'       => 'For printer-friendly summaries that you can print out and keep to hand.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadUserGuidePlusUser',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-user-guides',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_powwownews',
                    'heading'    => 'News and Press',
                    'text'       => 'Catch up with the latest news from Powwownow.',
                    'link'       => url_for('@news'),
                    'popUp'      => false,
                    'popUpName'  => '',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_tell_a_friend',
                    'heading'    => 'Tell a friend',
                    'text'       => 'Have a friend or colleague who is interested in free and easy conference calls with Powwownow? Spread the word!',
                    'link'       => url_for('@tell_a_friend'),
                    'popUp'      => false,
                    'popUpName'  => '',
                )
            ),
            'PLUS_ADMIN'    => array(
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download App',
                    'text'       => 'For conference calling on the move, download our apps. Select your mobile below:',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadApp',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-mobile-apps',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download user guide',
                    'text'       => 'For printer-friendly summaries that you can print out and keep to hand.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadUserGuidePlusAdmin',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-user-guides',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_powwownews',
                    'heading'    => 'News and Press',
                    'text'       => 'Catch up with the latest news from Powwownow.',
                    'link'       => url_for('@news'),
                    'popUp'      => false,
                    'popUpName'  => '',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_tell_a_friend',
                    'heading'    => 'Tell a friend',
                    'text'       => 'Have a friend or colleague who is interested in free and easy conference calls with Powwownow? Spread the word!',
                    'link'       => url_for('@tell_a_friend'),
                    'popUp'      => false,
                    'popUpName'  => '',
                )
            ),
            'PREMIUM_USER'  => array(
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download App',
                    'text'       => 'For conference calling on the move, download our app.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadApp',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-mobile-apps',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download user guide',
                    'text'       => 'For printer-friendly summaries that you can print out and keep to hand.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadUserGuidePremiumUser',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-user-guides',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_powwownews',
                    'heading'    => 'News and Press',
                    'text'       => 'Catch up with the latest news from Powwownow.',
                    'link'       => url_for('@news'),
                    'popUp'      => false,
                    'popUpName'  => '',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_tell_a_friend',
                    'heading'    => 'Tell a friend',
                    'text'       => 'Have a friend or colleague who is interested in free and easy conference calls with Powwownow? Spread the word!',
                    'link'       => url_for('@tell_a_friend'),
                    'popUp'      => false,
                    'popUpName'  => '',
                )
            ),
            'PREMIUM_ADMIN' => array(
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download App',
                    'text'       => 'For conference calling on the move, download our app.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadApp',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-mobile-apps',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_download_app',
                    'heading'    => 'Download user guide',
                    'text'       => 'For printer-friendly summaries that you can print out and keep to hand.',
                    'link'       => '',
                    'popUp'      => get_partial(
                        'commonComponents/popupDownloadUserGuidePremiumAdmin',
                        array('service' => $this->service)
                    ),
                    'popUpName'  => 'tooltip-footer-user-guides',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_powwownews',
                    'heading'    => 'News and Press',
                    'text'       => 'Catch up with the latest news from Powwownow.',
                    'link'       => url_for('@news'),
                    'popUp'      => false,
                    'popUpName'  => '',
                ),
                array(
                    'icon_class' => 'mypwn_footer_icon_tell_a_friend',
                    'heading'    => 'Tell a friend',
                    'text'       => 'Have a friend or colleague who is interested in free and easy conference calls with Powwownow? Spread the word!',
                    'link'       => url_for('@tell_a_friend'),
                    'popUp'      => false,
                    'popUpName'  => '',
                )
            )
        );

        /**
         * To create the pop ups, a JS array MyPWNFooterToolTips needs to get
         * populated with the tooltip names, not too sure why as this isnt the
         * best way of doing it, but never mind.
         */
        $popUpNames = array();

        foreach ($this->items[$this->service_user] as $item) {
            if (!empty($item['popUpName'])) {
                $popUpNames[] = $item['popUpName'];
            }
        }

        $this->popUpNames = json_encode($popUpNames);
    }

    /**
     * This is the Hintbox advising user to log in to myPwn
     *
     * @author Vitaly
     *
     */
    public function executeHeaderHint()
    {
        $user                = sfContext::getInstance()->getUser();
        $this->authenticated = $user->isAuthenticated();
    }

    /**
     * Display a subpage header, but with the breadcrumbs above the header.
     *
     * @author Maarten Jacobs
     */
    public function executeSubPageHeaderStacked()
    {
        if (!isset($this->breadcrumbs)) {
            $this->breadcrumbs = array();
        }
        if (!isset($this->user_type)) {
            $this->user_type = 'plus';
        }
        if (!isset($this->headingSize)) {
            $this->headingSize = 's';
        }
        if (!isset($this->title)) {
            $this->title = '';
        }
        if (!isset($this->classes)) {
            $this->classes = 'breadcrumb-sub-page';
        }
        if (!isset($this->headingClassPrefix)) {
            $this->headingClassPrefix = '';
        }

        // Determine the heading classes.
        $this->headingClasses = array(
            $this->headingClassPrefix . 'sub_page_heading',
            $this->headingClassPrefix . 'sub_page_heading_' . $this->headingSize,
            $this->headingClassPrefix . 'sub_page_heading_' . strtolower($this->user_type)
        );
    }

    /**
     * Gets the content for the smaller international dialin numbers, as used in the tooltip on the homepage
     *
     * @author Wiseman
     */
    public function executeInternationalDialInNumbersSmall()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));
        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $this->getUser()->getCulture(),
                'service_ref'    => $this->getUser()->getAttribute('service_ref'),
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );

        // Stores the compiled list of international number
        $internationalNumbers = array();

        foreach ($dialInNumbers['localList'] as $details) {
            $internationalNumbers[$details['country_en'] . ' - 1'] = $details;
        }
        foreach ($dialInNumbers['mobileList'] as $details) {
            $internationalNumbers[$details['country_en'] . ' - 2'] = $details;
        }
        ksort($internationalNumbers);
        $this->setVar('internationalNumbers', $internationalNumbers);
    }

    /**
     * Gets a link tag with the share my pin mailto link
     *
     * Set linkContent and pin when calling get_component() to set the relevant details
     *
     * @author Wiseman
     */
    public function executeShareMyPinLink(sfWebRequest $request)
    {
        $this->dialInNumber = Common::getDefaultDialInNumber(801, $this->getUser()->getCulture());
    }

    /**
     * Gets the Outlook Plugin For the Homepage
     *
     * @author Dav C
     * @amend Asfer T
     *
     */
    public function executeOutlookPlugin()
    {
        /** @var myUser $user */
        $user = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $isAuthenticated = $user->isAuthenticated();

        if (!$isAuthenticated) {
            $response->addStylesheet('/sfcss/components/commonComponents.outlookPlugin.css');
            $response->addJavascript('/sfjs/pwn.js');
            $response->addJavascript('/sfjs/mypwn.js');
        }

        $this->setVar('formID', isset($this->formID) ? $this->formID : 'frm-outlook-plugin');

        $this->setVar('outlookBtnData', array(
            '2003' => array(
                'text' => 'Download Plugin 2003-2007',
                'href' => '/Outlook_Plugin/current/setup-powwownow-plugin-2003-2007.exe'
            ),
            '2010' => array(
                'text' => 'Download Plugin 2010-2013',
                'href' => '/Outlook_Plugin/current/setup-powwownow-plugin-2010-2013.exe'
            )
        ));

        $this->setVar('formHash', substr(md5($this->getVar('formID')), 0, 5));
    }

    /**
     * Render a responsive Powwownow Logo.
     * @require
     *  Foundation4 CSS framework
     * @require
     *  HTML5-Responsive-layout.php
     * @author Dav C
     */
    public function executeResponsiveLogo()
    {
        // SASS file located in/shared/assets/sass/responsiveLogo.scss.
        $this->getResponse()->addStyleSheet('/shared/assets/stylesheets/responsiveLogo.css');
    }

    /**
     * Render a responsive "Canvas" Banner.
     * @require
     *  Foundation4 CSS framework
     * @require
     *  HTML5-Responsive-layout.php
     * @author Dav C
     */
    public function executeResponsiveCanvas()
    {
        // SASS file located in/shared/assets/sass/responsiveCanvas.scss.
        $this->getResponse()->addStyleSheet('/shared/assets/stylesheets/responsiveCanvas.css');
    }

    /**
     * Render a responsive "How It Works" Section
     * @require
     *  Foundation4 CSS Framework
     * @require
     *  HTML5-Responsive-layout.php
     * @author Dav C
     */
    public function executeResponsiveHowItWorks()
    {
        // SASS file located in/shared/assets/sass/responsiveHowItWork.scss.
        $this->getResponse()->addStyleSheet('/shared/assets/stylesheets/responsiveHowItWorks.css');
    }

    /**
     * Render a responsive Header Navigation Menu
     * @require
     *  Foundation4 CSS Framework
     * @require
     *  HTML5-Responsive-layout.php
     * @author Dav C
     */
    public function  executeResponsiveHeaderNavigation()
    {
        // SASS file located in/shared/assets/sass/responsiveMenu.scss.
        $this->getResponse()->addStyleSheet('/shared/assets/stylesheets/responsiveHeaderNavigation.css');
    }

    /**
     * Render a responsive "Footer" Section
     * @require
     *  Foundation4 CSS Framework
     * @require
     *  HTML5-Responsive-layout.php
     * @author Dav C
     */
    public function executeResponsiveFooter()
    {
        // SASS file located in/shared/assets/sass/responsiveFooter.scss.
        $this->getResponse()->addStyleSheet('/shared/assets/stylesheets/responsiveFooter.css');
    }

    /**
     * Render a responsive right hand menu
     * @require
     *  Foundation4 CSS Framework
     * @require
     *  HTML5-Responsive-layout.php
     * @author Dav C
     */
    public function executeResponsiveRightMenu()
    {
        $this->getResponse()->addJavascript('/sfjs/mobile-detect.min.js');
        $this->getResponse()->addJavascript('/sfjs/components/commonComponents.rightLandingPageMenu.js');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));
        $this->requestSource = $_SERVER['REQUEST_URI'];
        $country             = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $this->getUser()->getCulture(),
                'service_ref'    => $this->getUser()->getAttribute('service_ref'),
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );

        $this->dialInNumber       = $dialInNumbers['local'];
        $this->mobileDialInNumber = $dialInNumbers['mobile'];
    }

    /**
     * Render a responsive Calling Card
     * @require
     *  Foundation4 CSS Framework
     * @require
     *  HTML5-Responsive-layout.php
     * @author Dav C
     */
    public function executeResponsiveCallingCard()
    {
        // Helpers
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('hermesCallWithCaching', 'dialInNumbers'));

        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $this->getUser()->getCulture(),
                'service_ref'    => $this->getUser()->getAttribute('service_ref'),
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );

        $this->dialInNumber = $dialInNumbers['local'];
        $this->mobileNumber = $dialInNumbers['mobile'];

    }

    /**
     * Display the How It Works Code. Currently used in Homepage E
     * @author Asfer Tamimi
     */
    public function executeHowItWorks()
    {
        $this->getResponse()->addStyleSheet('/sfcss/components/commonComponents.howItWorks.css');
        $this->getResponse()->addStyleSheet('/sfcss/vendor/jQuery/jquery-ui-1.10.4.custom.css');
        $this->getResponse()->addJavascript('/shared/jQuery/jquery-ui-1.10.4.custom.min.js');
        $this->getResponse()->addJavascript('/sfjs/goto-registration-free.js');
    }

    /**
     * Display the Pin Generate Box for Homepage E
     * @author Asfer Tamimi
     */
    public function executePinGenerateE()
    {
        $this->getResponse()->addStyleSheet('/sfcss/components/commonComponents.pinGenerateE.css');

        $this->registrationSource = (isset($this->registrationSource)) ? $this->registrationSource : 'undefined';
    }

    /**
     * Display the How Costs Compare. Currently used in Homepage E
     * @author Asfer Tamimi
     */
    public function executeCostsCompare()
    {
        $this->graphs   = (isset($this->graphs)) ? $this->graphs : true;
    }

    /**
     * Display the Trusted By.
     * @author Asfer Tamimi
     */
    public function executeTrustedBy()
    {
        $this->pageName = (isset($this->pageName)) ? $this->pageName : '';
    }

    /**
     * Display the Promotional Rotation for the Homepage E.
     *
     * @author Asfer Tamimi
     *
     */
    public function executeRotatingPromoLinksHomepageE()
    {
        $this->getResponse()->addStyleSheet('/sfcss/components/commonComponents.rotatingPromoLinksHomepageE.css');
        $this->getResponse()->addJavascript('/sfjs/components/commonComponents.rotatingPromoLinksHomepageE.js');
        $this->getResponse()->addJavascript('/sfjs/plus.js');
    }

    /**
     * Displays the create-user form.
     */
    public function executeCreateUserForm()
    {
        $this->createUserForm = new createUserForm();
        $this->fields         = array(
            'email',
            'cost_code',
            'title',
            'first_name',
            'last_name',
            'phone_number',
            'password',
            'confirm_password',
            'notify_user'
        );
    }

    public function executeWhatsNext()
    {
    }

    /**
     * Render "What to do now" content
     * @author Dav C
     */
    public function executeWhatToDoNow()
    {
        $this->getResponse()->addStyleSheet('/sfcss/components/commonComponents.whatToDoNow.css');
        $this->getResponse()->addJavascript('/sfjs/mobile-detect.min.js');
        $this->getResponse()->addJavascript('/sfjs/components/commonComponents.rightLandingPageMenu.js');
    }

    /**
     * Render a outlook plugin tooltip
     * @author Dav C
     * @params OPTIONAL
     *  id => uniquie ID for DIV
     *  title => the title to display
     *  classes => array(
     *        // Font Colour Class
     *  )
     *  css => array(
     *       // basic css properties
     * )
     */
    public function executeOutlookPluginTooltip()
    {
        $this->getResponse()->addStylesheet('/sfcss/components/commonComponents.outlookPluginTooltip.css');
        $this->getResponse()->addJavascript('/sfjs/mobile-detect.min.js');
        $this->getResponse()->addJavascript('/sfjs/components/commonComponents.rightLandingPageMenu.js');
        $this->elementId = isset($this->id) ? $this->id : rand(0, 10);
        $this->hrefTitle = isset($this->title) ? $this->title : 'Schedule through Outlook';
        $this->fontClass = isset($this->classes['font']) ? $this->classes['font'] : 'green';
    }

    /**
     *Plus Features
     */
    public function executePlusFeatures()
    {
        $this->page = (isset($this->page)) ? $this->page : 'plusService';
    }

    /**
     * Why Plus
     */
    public function executeWhyPlus()
    {
        $this->page = (isset($this->page)) ? $this->page : 'plusService';
    }

    /**
     * @desc
     *  Cookie Policy
     * @param sfRequest $request
     */
    public function executeCookiePolicy(sfRequest $request)
    {
        if ($request->getCookie('cookie_policy_accepted', false) != 1) {
            $this->getResponse()->addJavascript('/sfjs/components/commonComponents.cookiePolicy.js');
        }
    }

    /**
     * Display the Choose the Plan. Currently used in Homepage E
     * @author Asfer Tamimi
     */
    public function executeChoosePlan()
    {
        $this->getResponse()->addStyleSheet('/sfcss/vendor/jQuery/jquery-ui-1.10.4.custom.css');
        $this->getResponse()->addJavascript('/shared/jQuery/jquery-ui-1.10.4.custom.min.js');
        $this->getResponse()->addJavascript('/sfjs/plus.js');
        $this->getResponse()->addJavascript('/sfjs/goto-registration-free.js');
    }

    /**
     * Returns a HTML5 Output Video.
     * It is almost Identical to Davinder's Helper, But I think it should be placed in a Component.
     * @return string
     * @author Asfer Tamimi (Stole it off Davinders Helper)
     */
    public function executeHTML5OutputVideo()
    {
        $config = (isset($this->config) && !empty($this->config)) ? $this->config : array();
        $video = (isset($this->video) && !empty($this->video)) ? $this->video : array();
        $called = (isset($this->called)) ? $this->called : 'html';
        $showBlank = (empty($config) || empty($video));
        $this->setVar('showBlank', (empty($config) || empty($video)));

        if (!$showBlank) {

            // Configuration
            $config['width'] = (isset($config['width'])) ? $config['width'] : '460';
            $config['height'] = (isset($config['height'])) ? $config['height'] : '260';
            $config['controls'] = (isset($config['controls']) && (bool)$config['controls'] === true) ? 'controls' : '';
            $config['autoplay'] = (isset($config['autoplay']) && (bool)$config['autoplay'] === true) ? 'autoplay' : '';
            $config['preload'] = (isset($config['preload'])) ? $config['preload'] : '';

            if (!isset($config['preload']) || !in_array($config['preload'], array('auto', 'metadata', 'none'))) {
                $config['preload'] = 'auto';
            }

            // Flash Config Defaults
            $config['allowscriptaccess'] = (isset($options['allowscriptaccess'])) ? $options['allowscriptaccess'] : 'always';
            $config['bgcolor'] = (isset($options['bgcolor'])) ? $options['bgcolor'] : '#fff';
            $config['allowfullscreen'] = (isset($options['allowfullscreen'])) ? $options['allowfullscreen'] : 'true';

            $config['playerID'] = (isset($options['playerID'])) ? $options['playerID'] : "altContent" . rand();

            // Video
            if (!isset($video['mp4']) || empty($video['mp4'])) {
                sfContext::getInstance()->getLogger()->err('Failed Generating HTML5 Video Markup - MP4 Format Required');
                $this->setVar('showBlank', true);
            } elseif (!isset($video['webm']) || empty($video['webm'])) {
                sfContext::getInstance()->getLogger()->err('Failed Generating HTML5 Video Markup - WebM Format Required');
                $this->setVar('showBlank', true);
            } elseif (!isset($video['ogg']) || empty($video['ogg'])) {
                sfContext::getInstance()->getLogger()->err('Failed Generating HTML5 Video Markup - OGG Format Required');
                $this->setVar('showBlank', true);
            }

            // Optional - Still Image
            if (!isset($video['image'])) {
                $video['image'] = '';
            }
        } else {
            sfContext::getInstance()->getLogger()->err('HTML5 Video Configuration is Not Valid');
        }

        $this->config = $config;
        $this->video = $video;
        $this->called = $called;
    }

    /**
     * HTML for new CX banner header.
     */
    public function executeCxHeader()
    {

    }

    /**
     * CX MyPinDetails Section.
     */
    public function executeCxMyPinDetails()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));

        $user = $this->getUser();
        $pins = PinHelper::getDefaultPins($user->getAttribute('contact_ref'));

        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $user->getCulture(),
                'service_ref'    => $user->getAttribute('service_ref'),
                'country'        => 'GBR',
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
        $this->setVar('pin', $pins['pin']);
        $this->setVar('email', $user->getAttribute('email'));
        $this->setVar('dialInNumber', substr($dialInNumbers['local'], 0, 6).' '.str_replace(" ", "", substr($dialInNumbers['local'], -8)));
        $this->setVar('mobileDialInNumber', $dialInNumbers['mobile']);
    }
    
    /**
     * Static TrustPilot Banner
     */
    public function executeTrustPilotBanner()
    {
    }

    /**
     * TrustPilot HTML Feed Code
     */
    public function executeTrustPilotFeed()
    {
        $trustPilotConfig       = sfConfig::get('app_trustpilot');
        $trustPilotTotalReviews = $trustPilotConfig['totalReviewsInYear'];
        $showReviews            = isset($this->showReviews) ? $this->showReviews : false;
        $name                   = isset($this->name) ? $this->name : 'Powwownow';
        $this->setVar('trustPilotFeed', TrustPilot::getFeed(true));
        $this->setVar('showReviews', $showReviews);
        $this->setVar('name', $name);
        $this->setVar('maxReviews', $trustPilotTotalReviews);
    }

    public function executeWhatYouGet() {
        $this->getResponse()->addStylesheet('/cx2/stylesheets/what.you.get.css');
    }

    /**
     * @desc
     *  Non Supported Browser Element
     * @param sfRequest $request
     */
    public function executeNonSupportedBrowser(sfRequest $request)
    {
        if ($request->getCookie('non_supported_browser_closed', false) != true) {
            $this->getResponse()->addJavascript('/sfjs/components/commonComponents.nonSupportedBrowser.js');
            $browserDetection = $this->getUser()->getAttribute(
                'browser_detection',
                array('data' => array('pwn_support_level' => 0))
            );

            $this->setVar('powwownowSupportLevel', $browserDetection['data']['pwn_support_level']);
        }
    }

    /**
     * Shows both the Breadcrumbs and the Heading Title.
     * Basically in combines the Breadcrumbs Component and the Heading Partial together.
     */
    public function executeBreadcrumbsAndHeader()
    {
        $this->getResponse()->addStylesheet('/sfcss/components/commonComponents.breadcrumbsAndHeader.css');
        $this->setVar('breadcrumbs', (isset($this->breadcrumbs)) ? $this->breadcrumbs : array());
        $this->setVar('breadcrumbsTitle', (isset($this->breadcrumbsTitle)) ? $this->breadcrumbsTitle : '');
        $this->setVar('headingTitle', (isset($this->headingTitle)) ? $this->headingTitle : '');
        $this->setVar('headingSize', (isset($this->headingSize)) ? $this->headingSize : 's');
        $this->setVar('headingUserType', (isset($this->headingUserType)) ? $this->headingUserType : 'powwownow');
    }

    /**
     * @desc
     *  JS Knockout Template Form for Registration
     */
    public function executeJsKoCxRegistrationTemplate()
    {
        // Only load the CSS on required actions
        switch($this->getContext()->getActionName()) {
            case 'onMusicHoldLandingPage':
                $this->setVar('callingActionName', 'onMusicHoldLandingPage');
                break;
            default:
                $this->getResponse()->addStylesheet('/cx2/stylesheets/components/registration.core.css');
        }
    }

    public function executeWidgetRegionSelector()
    {
        /**
         * The Javascript for this widget is located in:
         *  sfPwn/web/sfjs/components/commonComponents.header.js
         */
        $this->getResponse()->addStylesheet('/cx2/stylesheets/components/region.widget.css');
        $this->setVar('regions', Common::getRegionalLinks($this->getUser()->getCulture()));
    }

    /**
     * Video Carousel
     */
    public function executeVideoCarousel()
    {
        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->addStylesheet('/sfcss/vendor/jQuery/jquery.jcarousel.css');
        $response->addStylesheet('/sfcss/components/commonComponents.videoCarousel.css');
        $response->addStylesheet('/sfcss/vendor/jQuery/jquery-ui-1.8.7.custom.css');
        $response->addJavascript('/shared/jQuery/jquery.jcarousel.min.js');
        $response->addJavascript('/shared/jQuery/jquery.polaroid.js');
        $response->addJavascript('/sfjs/components/commonComponents.videoCarousel.js');
        $response->addJavascript('/shared/jQuery/jquery-ui-1.8.7.custom.min.js');

        // Get Video Information
        $videoInformation = sfConfig::get('app_video_carousel');

        // Get the HTML5 Videos
        $video3Steps                      = get_component(
            'commonComponents',
            'hTML5OutputVideo',
            array(
                'config' => array(
                    'width'    => 460,
                    'height'   => 260,
                    'autoplay' => false,
                    'controls' => true
                ),
                'video'  => array(
                    'mp4'   => $videoInformation['3Steps']['videos']['mp4'],
                    'webm'  => $videoInformation['3Steps']['videos']['mp4'],
                    'ogg'   => $videoInformation['3Steps']['videos']['mp4'],
                    'image' => $videoInformation['3Steps']['image']
                ),
                'called' => 'script'
            )
        );
        $videoInConferenceControls = get_component(
            'commonComponents',
            'hTML5OutputVideo',
            array(
                'config' => array(
                    'width'    => 460,
                    'height'   => 260,
                    'autoplay' => false,
                    'controls' => true
                ),
                'video'  => array(
                    'mp4'   => $videoInformation['InConferenceControls']['videos']['mp4'],
                    'webm'  => $videoInformation['InConferenceControls']['videos']['mp4'],
                    'ogg'   => $videoInformation['InConferenceControls']['videos']['mp4'],
                    'image' => $videoInformation['InConferenceControls']['image']
                ),
                'called' => 'script'
            )
        );
        $videoMyPowwownow                 = get_component(
            'commonComponents',
            'hTML5OutputVideo',
            array(
                'config' => array(
                    'width'    => 460,
                    'height'   => 260,
                    'autoplay' => false,
                    'controls' => true
                ),
                'video'  => array(
                    'mp4'   => $videoInformation['myPowwownow']['videos']['mp4'],
                    'webm'  => $videoInformation['myPowwownow']['videos']['mp4'],
                    'ogg'   => $videoInformation['myPowwownow']['videos']['mp4'],
                    'image' => $videoInformation['myPowwownow']['image']
                ),
                'called' => 'script'
            )
        );
        $videoWebConferencing             = get_component(
            'commonComponents',
            'hTML5OutputVideo',
            array(
                'config' => array(
                    'width'    => 460,
                    'height'   => 260,
                    'autoplay' => false,
                    'controls' => true
                ),
                'video'  => array(
                    'mp4'   => $videoInformation['WebConferencing']['videos']['mp4'],
                    'webm'  => $videoInformation['WebConferencing']['videos']['mp4'],
                    'ogg'   => $videoInformation['WebConferencing']['videos']['mp4'],
                    'image' => $videoInformation['WebConferencing']['image']
                ),
                'called' => 'script'
            )
        );

        // Strip the Tags
        $video3Steps               = $this->VideoCarouselStripTags($video3Steps);
        $videoInConferenceControls = $this->VideoCarouselStripTags($videoInConferenceControls);
        $videoMyPowwownow          = $this->VideoCarouselStripTags($videoMyPowwownow);
        $videoWebConferencing      = $this->VideoCarouselStripTags($videoWebConferencing);

        // Video Order
        $videoOrder = (isset($this->videoOrder) ? $this->videoOrder : array(
            '3Steps',
            'InConferenceControls',
            'myPowwownow',
            'WebConferencing'
        ));
        foreach ($videoOrder as $videoName) {
            switch ($videoName) {
                case '3Steps':
                    $videoInformation[$videoName]['loadVideo'] = $video3Steps;
                    break;
                case 'InConferenceControls':
                    $videoInformation[$videoName]['loadVideo'] = $videoInConferenceControls;
                    break;
                case 'myPowwownow':
                    $videoInformation[$videoName]['loadVideo'] = $videoMyPowwownow;
                    break;
                case 'WebConferencing':
                    $videoInformation[$videoName]['loadVideo'] = $videoWebConferencing;
                    break;
            }
        }

        // Set the Heading and Sub Heading
        $this->setVar('heading', isset($this->heading) ? $this->heading : 'Conference calling made easy');
        $this->setVar(
            'subHeading',
            isset($this->subHeading) ? $this->subHeading : 'Need help with setting up your Powwownow? Check out these handy tips on how to get the most out of your conference calling.'
        );

        $this->setVar('video', $videoInformation);
        $this->setVar('videoOrder', $videoOrder);
    }

    private function VideoCarouselStripTags($video)
    {
        return str_replace(array("'", "\n"), array("\'", " "), $video);
    }

    /**
     * IMeet Banner
     * @author Asfer Tamimi
     */
    public function executeIMeetBanner()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('DataUri'));

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->addStylesheet('/sfcss/components/commonComponents.iMeetBanner.css');

        $hermesResult = HermesCommon::iMeetGetIMeetUserByContactRef(
            array(
                'contact_ref' => $this->getUser()->getAttribute('contact_ref', false),
                'has_first_email_sent' => true
            )
        );

        if (!empty($hermesResult)) {
            $hermesResult = $hermesResult[0];
            $this->setVar('showBanner', true);
            $this->setVar('iMeetRoomLink', $hermesResult['imeet_url']);
        } else {
            $this->setVar('showBanner', false);
        }
    }

    /**
     * Returns a Cross Desktop, Cross Browser HTML5 Video,
     * with the addition of a Fallback to FlowPlayer, if Flash is enabled,
     * with the addition of a Further Fallback to a Textual Message if Flash is disabled.
     *
     * @return string
     * @author Asfer Tamimi
     * @todo Currently only been tested on the Testimonials Page
     * @todo Get Fallback Text Working
     */
    public function executeOutputCrossBrowserHtml5AndFallbackVideo()
    {
        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->addJavascript('/shared/SWFObject/swfobject.js', '', array('header' => true));
        $response->addJavascript('/shared/flowplayer/flowplayer-3.2.6.min.js');
        $response->addJavascript('/sfjs/components/commonComponents.outputCrossBrowserHtml5AndFallbackVideo.js');

        $config    = (isset($this->config) && !empty($this->config)) ? $this->config : array();
        $video     = (isset($this->video) && !empty($this->video)) ? $this->video : array();
        $showBlank = (empty($config) || empty($video));
        $errorMessage = (isset($this->errorMessage) && !empty($this->errorMessage)) ? $this->errorMessage : false;
        $this->setVar('showBlank', (empty($config) || empty($video)));

        if (!$showBlank) {

            // Configuration
            $config['width']    = (isset($config['width'])) ? $config['width'] : '460';
            $config['height']   = (isset($config['height'])) ? $config['height'] : '260';
            $config['controls'] = (isset($config['controls']) && (bool)$config['controls'] === true) ? 'controls' : '';
            $config['autoplay'] = (isset($config['autoplay']) && (bool)$config['autoplay'] === true) ? 'autoplay' : '';
            $config['preload']  = (isset($config['preload'])) ? $config['preload'] : '';
            $config['playerID'] = (isset($options['playerID'])) ? $options['playerID'] : rand();

            if (!isset($config['preload']) || !in_array($config['preload'], array('auto', 'metadata', 'none'))) {
                $config['preload'] = 'auto';
            }

            $videoFormats = array('mp4', 'webm', 'ogg');
            foreach ($videoFormats as $format) {
                if (!isset($video[$format]) || empty($video[$format])) {
                    sfContext::getInstance()->getLogger()->err(
                        'Failed Generating HTML5 Video Markup - ' . strtoupper($format) . ' Format Required'
                    );
                    $this->setVar('showBlank', true);
                }
            }

            // Optional - Still Image
            if (!isset($video['image'])) {
                $video['image'] = '';
            }

            // Error Message
            if (!$errorMessage) {
                $errorMessage .= '<strong>You do not have Flash installed, or it is older than the required 10.0.0.';
                $errorMessage .= '</strong><br/>';
                $errorMessage .= '<strong>Click below to install the latest version and then try again.</strong><br/>';
                $errorMessage .= '<a target="_blank" href="//www.adobe.com/go/getflashplayer">';
                $errorMessage .= '<img src="//www.adobe.com/misc/images/160x41_get_flashplayer.gif" width="112" ';
                $errorMessage .= 'height="33" alt="Get Adobe Flash player"/>';
                $errorMessage .= '</a>';
            }

        } else {
            sfContext::getInstance()->getLogger()->err('HTML5 Video Configuration is Not Valid');
        }

        $this->setVar('config', $config);
        $this->setVar('video', $video);
        $this->setVar('errorMessage', $errorMessage, true);
    }

    /**
     * Show the Google Content Experiment.
     * This is used for the Google Analytics A/B/n Testing
     */
    public function executeGoogleContentExperiment()
    {
        $this->setVar('experimentId', (isset($this->experimentId)) ? $this->experimentId : null);
    }

    /**
     * What You Get Icons used in the Free-Conference-Call-Service Page
     */
    public function executeWhatYouGetFreeConferenceCallService()
    {
        /** @var sfWebResponse $response */
        $response = $this->getResponse();
        $response->addStylesheet('/cx2/stylesheets/what.you.get.free.service.css');
    }

    /**
     * @desc
     *  This Component was used when we experiencing technical problems
     *  with
     */
    public function executeShowCustomerImportantMessage()
    {
        $showSkype = sfConfig::get('app_messages_skype');
        if ($showSkype == true) {
            $this->setVar('showSkype', $showSkype);
        }
    }
}
