<?php

/**
 * Debug Page aka Asfer Page
 *
 * @package    powwownow
 * @subpackage pages
 * @author     Asfer Tamimi
 *
 */
class debugActions extends sfActions
{

    /**
     * Debug aka Asfer [PAGE]
     *
     * This page contains lots of Debugging Dumps
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Robert Pajak + Asfer Tamimi + Wiseman
     *
     */
    public function executeIndex(sfWebRequest $request)
    {
        // Ensure users IP is in our white list
        $ipValidator = new PWNSafeIp();
        $userIp      = $request->getHttpHeader('addr', 'remote');
        try {
            $ipValidator->clean($userIp);
        } catch (Exception $e) {
            $this->logMessage("IP $userIp tried to check the Debug Page but is not in the 'allowed IP' list", 'err');
            $this->forward404();
        }
    }

    /**
     * Clear Cache [PAGE]
     *
     * Clear the Symfony Cache
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     */
    public function executeClearCache(sfWebRequest $request)
    {
        // Ensure users IP is in our white list
        $ipValidator = new PWNSafeIp();
        $userIp      = $request->getHttpHeader('addr', 'remote');
        try {
            $ipValidator->clean($userIp);
            $this->logMessage('IP : ' . $userIp . ' is accessing the Clear Cache Page', 'info');
            $rootDir = sfConfig::get('sf_root_dir') . '/';
            exec('cd ' . $rootDir . ' && symfony cc');
        } catch (Exception $e) {
            $this->logMessage("IP $userIp is trying to access the Clear Cache Page", 'err');
            $this->forward404();
        }
    }

}
