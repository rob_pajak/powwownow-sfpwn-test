<?php use_helper('DataUri') ?>

<div class="row">
    <div class="large-3 columns">
        <!-- Logo start -->
        <a href="<?php echo url_for('@homepage'); ?>" class="logolink">
            <img title="Powwownow Free Conference Call Provider" src="<?php echo getDataURI('/sfimages/powwownow_logo.png');?>" alt="Powwownow Free Conference Call Provider" class="logo png"/>
        </a>
        <!-- Logo End -->
    </div>
</div>