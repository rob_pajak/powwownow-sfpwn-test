<footer class="row">
    <div class="large-12 columns rounded-corners">
        <ul class="inline-list left hide">
            <li><a href="">Privacy</a></li>
            <li><a href="">T&Cs</a></li>
            <li><a href="">About Us</a></li>
            <li><a href="">Contact Us</a></li>
            <li class="hide-for-small"><a href="">Glossary</a></li>
            <li class="hide-for-small"><a href="">Useful Links</a></li>
            <li class="hide-for-small"><a href="">Site Map</a></li>
        </ul>

        <div class="social-icons right">
            <a href=""><i class="pwn-icons social foundicon-facebook"></i></a>
            <a href=""><i class="pwn-icons social foundicon-twitter"></i></a>
            <a href=""><i class="pwn-icons social foundicon-linkedin"></i></a>
            <a href=""><i class="pwn-icons social foundicon-youtube"></i></a>
        </div>
    </div>
</footer>