<?php if (isset($pinData)) : ?>
    <?php if (!empty($pinData[0]['pin']) && ($pinData[0]['active'] === 'Y' || empty($pinData[0]['active']) )) :?>
        <?php if (!empty($yugguuForm)):?>
            <div data-alert class="alert-box alert margin-top-20px">
                <p><?php echo $yugguuForm['errorMessages']?></p>
            </div>
        <?php endif;?>

        <form id="login_form" action="http://powwownow.yuuguu.com" method="get" class="yugguu-form">
            <input type="hidden" name="callback" value="<?php echo sfContext::getInstance()->getRequest()->getUri();?>"/>
            <input type="hidden" name="pin" value="<?php echo $pinData[0]['pin']?>">

            <div class="row">
                <div class="large-12 columns">
                    <label>Name</label>
                    <input type="text" placeholder="Enter your name" name="nickname" autocomplete="off"/>
                </div>
            </div>

            <div class="row">
                <div class="large-12 columns">
                    <input id="login_button" class="small button success radius right" name="login_button" type="submit" value="CONNECT"/>
                </div>
            </div>
        </form>
    <?php endif;?>
<?php endif;?>
