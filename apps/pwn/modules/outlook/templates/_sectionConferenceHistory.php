<section id="conference-history" class="conference-history">
    <p class="title" data-section-title><a class="font-regular" href="#panel2">Conference History</a></p>
    <div class="content" data-section-content>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
        <ul class="timeline">
            <?php foreach($appointmentData['history'] as $history) :?>
                <li class="list-item">
                    <article>
                        <h5 class="timeline-title"><?php echo $history['updated'] ?></h5>
                        <div class="panel rounded-corners">
                            <ul class="pwn-list">
                                <li>
                                    <i class="pwn-icons general foundicon-clock"></i>
                                    <?php echo $history['start_time']?>
                                    <small>Until</small>
                                    <?php echo $history['end_time']?>
                                </li>

                                <?php if ($history['conference_type'] === 1):?>
                                    <li>
                                        <i class="pwn-icons general foundicon-website"></i>
                                        <span>Web Conference</span>
                                    </li>
                                <?php else: ?>
                                    <li>
                                        <i class="pwn-icons general foundicon-phone"></i>
                                        <span>Voice Conference</span>
                                    </li>
                                <?php endif;?>

                                <li>
                                    <i class="pwn-icons general foundicon-location"></i>
                                    <?php echo $history['location']?>
                                </li>
                            </ul>
                            <p>
                                           <pre class="pre-style font-regular paragraph-font-11">
                                            <?php echo $history['body']?>
                                           </pre>
                            </p>
                        </div>

                    </article>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</section>
