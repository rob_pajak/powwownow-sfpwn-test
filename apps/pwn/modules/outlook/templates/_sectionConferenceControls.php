<section id="in-conference-controls" class="in-conference-controls">
    <p class="title last" data-section-title><a class="font-regular" href="#panel2">In-Conference Controls</a></p>
    <div class="content" data-section-content>
        <div class="row">
            <div class="large-12 columns">
                <p>During a conference call the following control keys are available.</p>
                <ul class="legend">
                    <li class="list-item">
                        <span class="legend-icon"><strong>#</strong></span>
                        <span class="legend-short">Skip Intro</span>
                        <span class="legend-long">Skips name recording and PIN playback when joining the call</span>
                    </li>
                    <li class="list-item">
                        <span class="legend-icon"><strong>#6</strong></span>
                        <span class="legend-short">Mute</span>
                        <span class="legend-long">Mutes and unmutes your handset</span>
                    </li>
                    <li class="list-item">
                        <span class="legend-icon"><strong>#1</strong></span>
                        <span class="legend-short">Head Count</span>
                        <span class="legend-long">Reviews the number of people on the call</span>
                    </li>
                    <li class="list-item">
                        <span class="legend-icon"><strong>#2</strong></span>
                        <span class="legend-short">Roll Call</span>
                        <span class="legend-long">Replays the names recorded when the callers arrived on the conference</span>
                    </li>
                    <li class="list-item">
                        <span class="legend-icon"><strong>#3</strong></span>
                        <span class="legend-short">Lock</span>
                        <span class="legend-long">Prevents anyone else joining the call</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
