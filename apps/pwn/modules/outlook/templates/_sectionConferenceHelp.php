<section>
    <p class="title last" data-section-title><a class="font-regular" href="#panel2">Download Outlook Plugin</a></p>
    <div class="content" data-section-content>
        <p>Want to make scheduling your next call simple and hassle free? Then download the Powwownow Outlook Plugin for free now!</p>

        <a href="/Outlook_Plugin/current/setup-powwownow-plugin-2003-2007.exe" target="_blank" class="small yellow radius button">Outlook 2003-2007</a>
        <a href="/Outlook_Plugin/current/setup-powwownow-plugin-2010-2013.exe" target="_blank" class="small yellow radius button">Outlook 2010-2013</a>

        <br />

        <a href="/sfpdf/en/Powwownow-Plugin-for-Outlook-User-Guide.pdf" target="_blank" class="small green radius button">Download User Guide</a>

        <p>If you have any questions about this or any other Powwownow product or service, then please give our friendly Customer Services team
            a call on 0203 398 0398, or get in touch using our <a class="link font-green chatlink" href="">Live Chat</a></p>

    </div>
</section>
<?php use_javascript('/homepageG/js/components/whosOn.js');?>