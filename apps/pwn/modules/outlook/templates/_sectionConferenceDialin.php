<section id="dial-in-numbers" class="dial-in-number">
    <p class="title" data-section-title><a class="font-regular" href="#panel2">Dial-in Numbers</a></p>
    <div class="content" data-section-content>
        <?php if (isset($dialinNumbersData)) :?>
            <ul class="small-block-grid-1 large-block-grid-3 pwn-list">
                <?php foreach ($dialinNumbersData as $dialinNumbers):?>
                    <li class="list-item">
                        <span class="sprite-flags-alt flag flag-<?php echo strtolower($dialinNumbers['iso2'])?>"></span>
                        <span><?php echo $dialinNumbers['country_name']?></span>
                        <span><?php echo $dialinNumbers['national_formatted']?></span>
                    </li>
                <?php endforeach;?>
            </ul>
        <?php endif;?>
    </div>
</section>