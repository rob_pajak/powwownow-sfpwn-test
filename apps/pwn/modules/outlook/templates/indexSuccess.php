<?php date_default_timezone_set('Europe/London'); ?>

<!-- Header and Nav -->
<?php include_partial('outlook/header') ?>
<!-- End Header and Nav -->

<div id="outlook-index" class="row container outlook-index">
    <div class="large-12 columns">
    <div class="section-container auto" data-section>

<?php if (!isset($appointmentData['error'])):?>
                <!-- -->
                <?php include_partial('outlook/sectionConferenceDetails',
                    array('appointmentData'=> $appointmentData,'pinData' => $pinData,'isRecurring' => $isRecurring)) ?>
                <!-- -->

                <!-- Conference History Start-->
                <?php //include_partial('outlook/sectionConferenceHistory',array('appointmentData'=> $appointmentData))?>
                <!-- Conference History End-->
                <!--Dial in Numbers -->
                <?php
                include_partial('outlook/sectionConferenceDialin', array('dialinNumbersData'=> $dialinNumbersData));
                //In-Conference controls
                include_partial('outlook/sectionConferenceControls', array('appointmentData'=> $appointmentData));
                ?>

<?php else :?>
    <section>
        <p class="title" data-section-title><a class="font-regular" href="#panel1">Conference Details</a></p>

        <div class="content" data-section-content>

            <div class="row">
                <p class="text-center">
                    <i class="pwn-icons general foundicon-error"></i><br />
                    <div data-alert class="margin-left-20">
                        <p>
                            <?php if ($appointmentData['error'] == '001:023:017') :?>
                            <p class="alert-box alert margin-right-20 text-center">URL Not Valid, please contact your Conference organiser.</p>
                            <?php elseif ($appointmentData['error'] == '001:023:002') : ?>
                            <p class="alert-box alert margin-right-20">Powwownow does not have any record of this Conference - This may be due to firewall configuration with the conference organiser.<br />If you are an organiser please check your firewall rules, allowing HTTPS traffic.<br /><br />If you need any further assistance, please contact customer services team at Powwownow on 0203 398 0398</p>
                            <p>Participants join the web conference here<br /><br />
                            <a class="radius success button" href="http://powwownow.yuuguu.com" target="_blank">Join Session</a>
                            <p>If you are the Host/Chairperson please start the web conference using your Powwownow web conferencing app.</p>
                            <p><img class="pwn-logo" src="/sfimages/pwn-icon.png" height="41" width="41" />Powwownow</p>



                            <?php elseif ($appointmentData['appointment']['end_time'] <  time()) :?>
                            This Conference has already taken place!
                            <?php elseif ($appointmentData['appointment']['deleted'] === '1') :?>
                            This Conference has been Cancelled!
                            <?php endif; ?>
                        </p>
                    </div>
                </p>
            </div>
    </section>
<?php endif; ?>
        <?php include_partial('outlook/sectionConferenceHelp')?>
    </div>
    </div>
</div>

<!-- Footer -->
<?php include_component('commonComponents','responsiveFooter');?>


<?php if (!isset($appointmentData['error']) && count($appointmentData)):?>
    <?php if ( strtolower($appointmentData['appointment']['conference_type']) === 'web'): ?>
        <div id="JoinSessionModal" class="join-session-modal reveal-modal small">
            <a class="close-reveal-modal">&#215;</a>
            <?php if (isset($pinData)) : ?>
                <?php if (!empty($pinData[0]['pin']) && ($pinData[0]['active'] === 'Y' || empty($pinData[0]['active']) )) :?>
                    <?php include_component('outlook','getYugguuForm',array('pinData'=>$pinData));?>
                <?php endif;?>
            <?php endif;?>
            <a class="close-reveal-modal">&#215;</a>
        </div>
    <?php endif;?>
<?php endif; ?>

<?php if ( (isset($yugguuForm)) && $yugguuForm['errorMessages']):?>
<script>
    $('#JoinSessionModal').foundation('reveal', 'open');
</script>
<?php endif;?>