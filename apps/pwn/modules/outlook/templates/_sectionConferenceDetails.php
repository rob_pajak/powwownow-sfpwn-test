<?php $displayDetails = true; ?>
<section>
    <p class="title" data-section-title><a class="font-regular" href="#panel1">Conference Details</a></p>

    <div class="content" data-section-content>

        <div class="row">
            <div class="large-12 columns">
                <header>
                    <h1 class="header-style-1"><?php echo $appointmentData['appointment']['subject']?> <small>Organised by <?php echo $appointmentData['appointment']['first_name'] .' ' . $appointmentData['appointment']['last_name'] ?></small></h1>
                    <div class="border"></div>
                </header>
            </div>
        </div>
        <div class="row">
            <div class="large-9 push-3 columns">
                <div class="appointment-details">
                    <?php if (!$isRecurring):?>
                        <ul class="pwn-list-a">
                            <li class="list-item">
                                <label for="timezone-form-select">
                                    <i class="pwn-icons general foundicon-globe"></i>
                                    <span>Timezone</span>
                                </label>
                                <div class="styled-select">
                                    <select id="timezone-form-select">

                                    </select>
                                </div>
                            </li>
                        </ul>
                    <?php endif;?>
                    <ul class="pwn-list">
                        <?php if (!$isRecurring):?>
                            <li class="list-item first">
                                <i class="pwn-icons general foundicon-calendar"></i>
                                <time id="date-time" data-epoch-time="<?php echo $appointmentData['appointment']['start_time'] ?>">
                                    <?php echo date('l, d F Y',$appointmentData['appointment']['start_time']) ?>
                                </time>
                            </li>
                            <li class="list-item">
                                <i class="pwn-icons general foundicon-clock"></i>
                                <time id="start-time" data-epoch-time="<?php echo $appointmentData['appointment']['start_time']?>">
                                    <?php echo date('G:i',$appointmentData['appointment']['start_time']) ?>
                                </time>
                                <span class="small">until</span>
                                <time id="end-time" data-epoch-time="<?php echo $appointmentData['appointment']['end_time']?>">
                                    <?php echo date('G:i',$appointmentData['appointment']['end_time'])?>
                                </time>
                                <span id="timezone-abbreviation" class="small"></span>
                            </li>
                            <?php if ( strtolower($appointmentData['appointment']['conference_type']) === 'web'):?>
                                <li class="list-item">
                                    <i class="pwn-icons general foundicon-website"></i>
                                    <span>Web Conference</span>
                                </li>
                            <?php else: ?>
                                <li class="list-item">
                                    <i class="pwn-icons general foundicon-phone"></i>
                                    <span>Voice Conference</span>
                                </li>
                            <?php endif;?>

                        <?php else:?>
                            <li class="list-item first">
                                <i class="pwn-icons general foundicon-globe"></i>
                                <span>Timezone</span>
                                <span>
                                    <?php if ($appointmentData['appointment']['timezone']): ?>
                                        <?php echo $appointmentData['appointment']['timezone'];?>
                                    <?php endif; ?>
                                </span>
                            </li><br />
                            <li class="list-item first">
                                <i class="pwn-icons general foundicon-calendar"></i>
                                <time id="date-time" data-epoch-time="<?php echo $appointmentData['appointment']['start_time'] ?>">
                                    <?php echo $appointmentData['appointment']['recurrence_pattern'] ?>
                                </time>
                            </li>
                            <br />
                            <?php if ( strtolower($appointmentData['appointment']['conference_type']) === 'web'):?>
                                <li class="list-item first">
                                    <i class="pwn-icons general foundicon-website"></i>
                                    <span>Web Conference</span>
                                </li>
                            <?php else: ?>
                                <li class="list-item first">
                                    <i class="pwn-icons general foundicon-phone"></i>
                                    <span>Voice Conference</span>
                                </li>
                            <?php endif;?>
                        <?php endif;?>
                    </ul>

                    <div class="border"></div>

                    <ul class="pwn-list">
                        <li class="list-item first">
                            <i class="pwn-icons general foundicon-location"></i>
                            <?php echo $appointmentData['appointment']['location']?>
                        </li>
                    </ul>

                    <div class="border"></div>
                </div>

                <div class="panel rounded-corners border-green">
                    <div class="text-center">
                        <p><strong>Dial-in Number</strong></p>

                        <?php $count = count($appointmentData['appointment']['dialin_numbers']);?>

                        <?php if ($count > 5):?>
                            <ul class="large-block-grid-3 small-block-grid-2 list-font-11">
                        <?php else:?>
                            <ul class="large-block-grid-1 list-font-11">
                        <?php endif;?>
                            <?php foreach($appointmentData['appointment']['dialin_numbers'] as $k => $v) : ?>
                                <li><?php echo $v['number'];?> (<?php echo $v['code'];?>)</li>
                            <?php endforeach;?>
                            </ul>
                            <p><strong>PIN</strong><br />
                                <?php if (isset($pinData)) : ?>
                                    <?php if (!empty($pinData[0]['pin']) && ($pinData[0]['active'] === 'Y' || empty($pinData[0]['active']) )) :?>
                                        <?php echo $pinData[0]['pin'] ?>
                                    <?php else: ?>
                                        <strong>PIN Deactivated</strong>
                                    <?php endif;?>
                                <?php endif;?>
                                <br /><br />
                                <?php if ( strtolower($appointmentData['appointment']['conference_type']) === 'web'): ?>

                                    <?php if ($appointmentData['appointment']['end_time'] <  time()) :?>
                                        <a class="radius button disabled"  href="#">Session Ended</a>
                                    <?php else:?>
                                        Participants join the web conference here<br /><br />
                                        <a class="radius success button" data-reveal-id="JoinSessionModal" href="#">Join Session</a>
                                        <p>If you are the Host/Chairperson please start the web conference using your Powwownow web conferencing app.</p>
                                        <p><img class="pwn-logo" src="/sfimages/pwn-icon.png" height="41" width="41" />Powwownow</p>
                                    <?php endif;?>
                            <?php endif;?>
                            </p>
                    </div>
                </div>

                <div class="border"></div>

                <div>
                    <p>
                       <i class="pwn-icons general foundicon-mail"></i>
                       <pre class="font-regular paragraph-font-11">
                        <?php
                            if ( strtolower($appointmentData['appointment']['conference_type']) === 'web'):
                                $bodySplitTail = preg_split('/Enter this PIN: [0-9]{5,}/', $appointmentData['appointment']['body']);
                                $bodySplit = preg_split('/\*If link does not work, copy this URL/', $bodySplitTail[1]);
                                echo $bodySplit[0];
                            else:
                                $bodySplit = preg_split('/including conference controls and international dial-in numbers\./', $appointmentData['appointment']['body']);
                                if (isset($bodySplit[1])):
                                    echo $bodySplit[1];
                                else:
                                    echo $bodySplit[0];
                                endif;
                            endif;
                        ?>
                       </pre>
                    </p>
                </div>

            </div>

            <!-- This is source ordered to be pulled to the left on larger screens -->
            <div class="large-3 pull-9 columns attendee">

                <ul class="side-nav attendee-list">

                    <?php if (isset($appointmentData['appointment']['invitees'])) :?>

                        <li class="list-title"><i class="pwn-icons general foundicon-address-book"></i> <strong>Attending (<?php echo count($appointmentData['appointment']['invitees'])?>)</strong></li>
                        <?php foreach($appointmentData['appointment']['invitees'] as $invitee):?>
                            <li class='list-item'>
                                <strong><?php echo $invitee['name']?></strong>
                                <br />
                                <?php echo $invitee['email']?>
                            </li>
                        <?php endforeach;?>

                    <?php else:?>
                        <li class="list-title"><i class="pwn-icons general foundicon-address-book"></i><strong>Attending</strong></li>
                    <?php endif;?>
                </ul>
                <ul class="side-nav attendee-list">

                    <?php if (isset($appointmentData['appointment']['optional_invitees'])) :?>

                        <li class="list-title"><i class="pwn-icons general foundicon-address-book"></i> <strong>Optional (<?php echo count($appointmentData['appointment']['optional_invitees'])?>)</strong></li>
                        <?php foreach($appointmentData['appointment']['optional_invitees'] as $invitee) :?>
                            <li class='list-item'>
                                <strong><?php echo $invitee['name']?></strong>
                                <br />
                                <?php echo $invitee['email']?>
                            </li>
                        <?php endforeach;?>

                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</section>
