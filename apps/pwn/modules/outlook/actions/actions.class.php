<?php
/**
 * Class outlookActions
 */
class outlookActions extends sfActions
{
    /**
     * The Outlook Plugin Manifest
     *
     * @return string
     */
    public function executeOutlookUpdate()
    {
        $params = sfConfig::get('app_outlook_plugin');
        $json = json_encode($params);
        $output = str_replace(
            array('\/', '{', ',', '}', '":'),
            array('/', "{\n    ", ",\n    ", "\n}", '": '),
            $json
        );
        echo $output;
        return sfView::NONE;
    }

    public function preExecute()
    {
        $request = $this->getRequest();

        // lets check if user tried going to yuggu
        if ($request->getMethod() === 'GET') {
            $errorMessage = $request->getParameter('error_msg',false);
            if ($errorMessage !== false) {
                $this->yugguuForm = array(
                    'errorMessages' => $errorMessage
                );
            }
        }
    }

    /**
     * @param sfRequest $request
     */
    public function executeIndex(sfRequest $request)
    {
        // Validation & Sanitation done within Hermes.
        $params = array(
            'guid'              => $request->getParameter('guid',null),
            'contact_ref'       => $request->getParameter('contact_ref',null),
            'appointment_id'    => $request->getParameter('appointment_id',null),
            'hash'              => $request->getParameter('hash',null)
        );

        $appointmentData = $this->getAppointment($params);

        // check if there is appointment
        if (isset($appointmentData['appointment'])) {
            if (isset($appointmentData['appointment']['participant_pin'])) {
                /**
                 * NOTE
                 * $appointmentData['appointment']['participant_pin'] is ACTUALLY pin_ref
                 */
                $pinData = $this->getPin($appointmentData['appointment']['participant_pin']);
                $dialinNumbersData = $this->getPinDialInNumbers($appointmentData['appointment']['participant_pin']);
            }

            $appointmentData['appointment']['invitees'] = $this->extractNameAndEmail(
                $appointmentData['appointment']['invitees']
            );
            if (isset($appointmentData['appointment']['optional_invitees'])) {
                $appointmentData['appointment']['optional_invitees']
                    = $this->extractNameAndEmail($appointmentData['appointment']['optional_invitees']);
            } else {
                $appointmentData['appointment']['optional_invitees'] = array();
            }

            $this->isRecurring = $this->extractIsRecurring($appointmentData);
            $this->dialinNumbersData = $dialinNumbersData;
            $this->pinData = $pinData;
        }
        $this->timezoneJSON = $this->timezoneJSON();
        $this->appointmentData = $appointmentData;
    }

    private function extractIsRecurring($appointmentData)
    {
        return isset($appointmentData['appointment']['is_recurring']);
    }

    /**
     * @param mixed $invitees
     * @return array
     */
    private function extractNameAndEmail($invitees)
    {
        $contactList = array();
        if (is_array($invitees)) {
            foreach ($invitees as $invitee) {

                /**
                 * We can either use the below regex:
                 *
                 * preg_match( '/\((.*?)\)/', $invitee, $email );
                 *
                 * OR
                 *
                 * use a combination of explode and substr to
                 * extract the name and email.
                 *
                 */
                $inviteeDetails = explode('(', $invitee);
                if (count($inviteeDetails) === 2) {
                    $contactList[] = array(
                        'name' => $inviteeDetails[0],
                        'email' => substr($inviteeDetails[1], 0, -1)
                    );
                } elseif(count($inviteeDetails) === 1) { //sometimes we have only email
                    $contactList[] = array(
                        'name' => '',
                        'email' => $inviteeDetails[0]
                    );
                }
            }
        }

        return $contactList;
    }

    /**
     * @param array $params
     * @return array
     */
    private function getAppointment(array $params)
    {
        try {

            return Hermes_Client_Rest::call(
                'Appointment.retrieveAppointmentData',
                $params
            );

        }

        catch (Hermes_Client_Exception $e) {
            $this->logMessage(
                'An exception occurred calling getAppointmentData: ' . serialize($e),
                'warning'
            );
            return(array("error" => $e->getCode()));
        }

        catch (Exception $e) {
            $this->logMessage(
                'An exception occurred calling getAppointmentData: ' . serialize($e),
                'warning'
            );
        }
    }

    /**
     * @param $pinRef
     * @return array
     */
    private function getPin($pinRef) {

        try {
            return Hermes_Client_Rest::call(
                'getPin',
                array('pin_ref' => $pinRef)
            );

        } catch (Hermes_Client_Exception $e) {
            $this->logMessage(
                'An exception occurred calling outlookActions::getPin: ' . serialize($e),
                'warning'
            );

        } catch (Exception $e) {
            $this->logMessage(
                'An exception occurred calling outlookActions::getPin: ' . serialize($e),
                'warning'
            );
        }
    }

    /**
     * @param $pinRef
     * @return array
     */
    private function getPinDialInNumbers($pinRef) {

        try {

            return Hermes_Client_Rest::call(
                'getPinDialInNumbers',
                array('pin_ref' => $pinRef, 'language' => 'en')
            );

        } catch (Hermes_Client_Exception $e) {
            $this->logMessage(
                'An exception occurred calling outlookActions::getPinDialInNumbers: ' . serialize($e),
                'warning'
            );

        } catch (Exception $e) {
            $this->logMessage(
                'An exception occurred calling outlookActions::getPinDialInNumbers: ' . serialize($e),
                'warning'
            );
        }
    }

    /**
     * @return string|null
     */
    private function timezoneJSON() {

        $pathInfo = $this->getRequest()->getPathInfoArray();
        $file = file_get_contents($pathInfo['DOCUMENT_ROOT'].'/shared/ext/tz/tz.json');
        if ($file) {
            return json_decode($file);
        }
    }
}