<?php

class outlookComponents extends sfComponents {

    /**
     *
     */
    public function executeGetYugguuForm(sfRequest $request) {

        if ($request->getMethod() === 'GET') {
            $errorMessage = $request->getParameter('error_msg',false);
            if ($errorMessage !== false) {
                $this->yugguuForm = array(
                    'errorMessages' => $errorMessage,
                    'params' => array(
                        'pin' => $request->getParameter('pin',false),
                        'nickname' => $request->getParameter('nickname',false)
                    )
                );
            }
        }
    }
}