<?php

/**
 * Facade class for the worldpay/mainCallback to redirect all calls to Hermes.
 */
class WorldPayCallbackHandler
{
    /**
     * TEMPORARY LOGGING OF CALL CREDIT FOR CUSTOMERS WITH NEGATIVE CREDIT
     *
     * @param int $accountId
     * @param float $creditAmount
     * @param int $paymentId
     * @author Maarten Jacobs
     */
    public function logNegativeAccountBalanceReconcilliation($accountId, $creditAmount, $paymentId)
    {
        Hermes_Client_Rest::call(
            'Plus.logCallCreditPurchase',
            array(
                'account_id' => $accountId,
                'credit_amount' => $creditAmount,
                'payment_id' => $paymentId,
                'is_balance_reconcilliation' => true,
            )
        );
    }

    public function retrievePaymentSession($cartID)
    {
        return plusCommon::retrievePaymentSession($cartID);
    }

    public function retrieveAccountAndContactByAccountId($accountId)
    {
        return Hermes_Client_Rest::call('Plus.getAccountAndContactByAccountId', array('account_id' => $accountId));
    }

    public function retrieveBillingAddress($accountId)
    {
        return Hermes_Client_Rest::call('Plus.getPlusBillingAddress', array('account_id' => $accountId));
    }

    public function createInvoiceDB($invoiceArguments)
    {
        return Hermes_Client_Rest::call('Plus.createInvoiceDB', $invoiceArguments);
    }

    public function updatePlusCreditCardAddress($creditCardArguments)
    {
        return Hermes_Client_Rest::call('Plus.updatePlusCreditCardAddress', $creditCardArguments);
    }

    public function deactivateAutoTopupByTopupBundleID($futurePayID, $paymentGatewayID)
    {
        return Hermes_Client_Rest::call(
            'Plus.deactivateAutoTopupByTopupBundleID',
            array(
                'topup_bundle_id'    => $futurePayID,
                'payment_gateway_id' => $paymentGatewayID,
            )
        );
    }

    public function processPaymentGatewayResponse($paymentArguments)
    {
        return Hermes_Client_Rest::call('Plus.doProcessPaymentGatewayResponse', $paymentArguments);
    }

    public function getPlusAccount($accountId)
    {
        return Hermes_Client_Rest::call('getPlusAccount', array('account_id' => $accountId));
    }

    public function sendAccountSummaryEmail($accountId, $totalBasketCost, $topUpCreditValue, $totalTopUpValue)
    {
        return Hermes_Client_Rest::call(
            'BWM.sendAccountSummaryEmail',
            array(
                'account_id'  => $accountId,
                'productCost' => $totalBasketCost,
                'topUpCreditValue' => $topUpCreditValue,
                'totalTopupValue'  => $totalTopUpValue,
            )
        );
    }

    public function sendProductSummaryEmail($accountId, array $products)
    {
        return Hermes_Client_Rest::call(
            'Plus.sendProductSummaryEmail',
            array(
                'account_id' => $accountId,
                'products' => $products,
            )
        );
    }

    public function sendAccountTopupEmail($emailArguments)
    {
        return Hermes_Client_Rest::call('Plus.sendAccountTopupEmail', $emailArguments);
    }

    public function storeProcessedPayment($cartID, $processedPayment)
    {
        return Hermes_Client_Rest::callPOST(
            'Plus.storeProcessedPayment',
            array(
                'cart_id' => $cartID,
                'processed' => $processedPayment,
            )
        );
    }

    public function createTopupBundle($accountId, $futurePayId)
    {
        return Hermes_Client_Rest::call(
            'Plus.createTopupBundle',
            array(
                'account_id' => $accountId,
                'payment_gateway_id' => 1,
                'recurring_payment_id' => $futurePayId,
                'amount' => 0,
            )
        );
    }

    public function notifyPostPayAdminForMissingRecurringPaymentAgreement($accountId)
    {
        return Hermes_Client_Rest::call(
            'Bundle.notifyPostPayAdminForMissingRecurringPaymentAgreement',
            array(
                'account_id' => $accountId,
            )
        );
    }

    /**
     * Calls the global helper function two2three, in lieu of an Object-Oriented country helper.
     *
     * @param string $country
     * @return string
     */
    public function two2three($country)
    {
        return two2three($country);
    }

    /**
     * Triggers the basket processor, assuming that the added-products have been stored on the user.
     *
     * @param myUser $user
     * @param PaymentSession $session
     * @return array
     */
    public function processBasket(myUser $user, PaymentSession $session)
    {
        return BasketProcess::updateAccountDefaultHandler(true, $user, $session);
    }

    /**
     * Logs in the user by using Plus_Authenticate.
     *
     * This is required for running the basket processor; it assumes the user is logged in.
     *
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function loginUser($email, $password)
    {
        return Plus_Authenticate::setCredentials($email, $password);
    }

    /**
     * Retrieves the account balance and manage-users status for an account.
     *
     * @param int $accountId
     * @param int $contactRef
     * @return array
     */
    public function getAccountBalanceContainerAndUsers($accountId, $contactRef)
    {
        return plusCommon::getAccountBalanceContainerAndUsers($accountId, $contactRef);
    }

    /**
     * Sends the a mail listing the errors occurred during the basket process, but only if the environment is not development
     * or SQI.
     *
     * @param array $errorMessages
     * @param string $recipient
     */
    public function sendBasketProcessErrorMail(array $errorMessages, $recipient = 'webteam@powwownow.com')
    {
        if (!isset($_SERVER['APPLICATION_ENV']) || !in_array($_SERVER['APPLICATION_ENV'], array('dev','sqi'))) {
            $errorMessages = array_filter($errorMessages);
            if ($errorMessages) {
                array_unshift($errorMessages, 'The following error messages were thrown by the Basket Processor:', '');
            } else {
                array_unshift($errorMessages, 'No error messages were included from the Basket Processor.');
            }
            array_unshift($errorMessages, $this->createHTTPReferrerMessageForErrorMail());
            array_unshift($errorMessages, $this->createApplicationEnvironmentMessageForErrorMail());

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $message = wordwrap(implode("\r\n", $errorMessages), 70, "\r\n");
            mail($recipient, 'Basket Process has failed', $message, $headers);
        }
    }

    public function handleIncentivesForPlusPurchase($referrer, $accountId, $cartId, $purchases)
    {
        return Hermes_Client_Rest::call(
            'Incentives.determineAndStoreIncentivesForPlusPurchase',
            array(
                'referred_by' => $referrer,
                'account_id' => $accountId,
                'cart_id' => $cartId,
                'purchases' => $purchases,
            )
        );
    }

    /**
     * @return string
     * @author Maarten Jacobs
     */
    private function createApplicationEnvironmentMessageForErrorMail()
    {
        if (!isset($_SERVER['APPLICATION_ENV'])) {
            $environment = 'application environment not specified';
        } else {
            $environment = $_SERVER['APPLICATION_ENV'];
        }
        return sprintf('Environment: %s.', $environment);
    }

    /**
     * @return string
     * @author Maarten Jacobs
     */
    private function createHTTPReferrerMessageForErrorMail()
    {
        if (!isset($_SERVER['HTTP_REFERER'])) {
            $referrer = 'HTTP referer not set';
        } else {
            $referrer = $_SERVER['HTTP_REFERER'];
        }
        return sprintf('HTTP Referrer: %s.', $referrer);
    }

    /**
     * Sends an Email to the WebTeam of Error Messages in Hermes Calls during the Worldpay Transaction.
     *
     * The Message will contain the Hermes Error Message along with a dump of the the POST data, the SERVER data and the SESSION data.
     * In Addition the Error Title will be the Email Title.
     *
     * @param $errorMessage
     * @param $errorTitle
     *
     * @author Asfer Tamimi
     */
    public function sendErrorEmail($errorMessage, $errorTitle)
    {
        $lineSeparator = "\r\n";
        $headers       = 'MIME-Version: 1.0' . $lineSeparator;
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . $lineSeparator;
        $message = array(
            $errorMessage,
            '',
            'POST data:',
            print_r($_POST, true),
            '',
            'SERVER data:',
            print_r($_SERVER, true),
            '',
            'SESSION data:',
            print_r($_SESSION, true),
        );

        $message = wordwrap(implode($lineSeparator, $message), 70, $lineSeparator);
        mail('webteam@powwownow.com', $errorTitle, $message, $headers);
    }

    /**
     * Sends an email to webteam to notify the team that a pending refund request must be created.
     *
     * @param int $accountId
     * @param double $outstandingCredit
     * @author Maarten Jacobs
     */
    public function sendBundleCustomerWithPositiveBalanceEmail($accountId, $outstandingCredit)
    {
        $lineSeparator = "\r\n";
        $headers = 'MIME-Version: 1.0' . $lineSeparator;
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . $lineSeparator;
        $messageLines = array(
            sprintf(
                'A Plus customer with ID %d has purchased a bundle, but has a remaining balance of %.2f.',
                $accountId,
                $outstandingCredit
            ),
            'This customer needs to be refunded for the exact amount.'
        );

        $message = wordwrap(implode($lineSeparator, $messageLines), 70, $lineSeparator);
        mail(
            'webteam@powwownow.com',
            '[' . $_SERVER['SERVER_NAME'] . '] Bundle purchased but outstanding credit remains',
            $message,
            $headers
        );
    }
}
