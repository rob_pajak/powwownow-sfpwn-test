<?php

class WorldPayActionHandler
{
    /**
     * Handles the "main" callback made from WorldPay.
     *
     * This callback handles successful payments, cancelled payments and cancelled Recurring Payment Agreements.
     *
     * @param sfWebRequest $request
     * @param sfFileLogger $logger
     * @param WorldPayCallbackHandler $handler
     * @param array $paymentGatewayDetails
     * @param myUser $user
     * @return string
     *   Either sfView::NONE (meaning nothing needs to be returned to WorldPay) or topupC, or topupS.
     * @author Maarten Jacobs
     */
    public function handleMainCallback(
        sfWebRequest $request,
        sfFileLogger $logger,
        WorldPayCallbackHandler $handler,
        array $paymentGatewayDetails,
        myUser $user
    ) {
        $cartID = $request->getPostParameter('cartId');
        $logger->info("WorldPay/MainCallback - trying to retrieve cart with ID '$cartID'.");
        $session = $handler->retrievePaymentSession($cartID);

        $data = array(
            'gateway_transaction_id'     => $request->getPostParameter('transId'),
            'recurring_payment_id'       => $request->getPostParameter('futurePayId'),
            'gateway_transaction_status' => $request->getPostParameter('transStatus'),
            'gateway_transaction_time'   => $request->getPostParameter('transTime'),
            'payment_gateway_id'         => 1, // 1 = WorldPay
            'payment_method_id'          => null,

            'payment_amount'             => $request->getPostParameter('authAmount'),
            'payment_currency'           => $request->getPostParameter('authCurrency'),

            'gateway_status_message'     => $request->getPostParameter('rawAuthMessage'),
            'gateway_status_code'        => $request->getPostParameter('rawAuthCode'),
            'gateway_action'             => $request->getPostParameter('M_action'),

            'creditcard_address_1'       => $request->getPostParameter('address1'),
            'creditcard_address_2'       => $request->getPostParameter('address2'),
            'creditcard_address_3'       => $request->getPostParameter('address3'),
            'creditcard_town'            => $request->getPostParameter('town'),
            'creditcard_region'          => $request->getPostParameter('region'),
            'creditcard_postcode'        => $request->getPostParameter('postcode'),
            'creditcard_country'         => $handler->two2three($request->getPostParameter('country')),

            'recurring_payment_status'   => $request->getPostParameter('futurePayStatusChange'),
            'installation'               => $request->getPostParameter('installation'),
            'description'                => $request->getPostParameter('desc'),
        );

        // Posted RAW Worldpay Data
        $logger->info('Posted RAW Worldpay Request: ' . print_r($request->getPostParameters(), true));

        // Posted Worldpay Data
        $logger->info('Posted Worldpay Request: ' . print_r($data, true));

        // Check if the CartId or $data['gateway_action'] is NULL, this means the transaction was unsuccessful (ie cancelled or someone tried to hack)
        if (is_null($cartID) || !$session || is_null($data['gateway_action'])) {
            $this->handleWorldPayErrornousAndTopUpBundleRelatedCall(
                $handler,
                1,
                $data,
                $paymentGatewayDetails['installationID'],
                $logger
            );
            return sfView::NONE;
        }

        // Store Worldpay Transaction Information
        $accountId     = $session->getAccountId();
        $autotopup     = $session->getAutotopup();
        $paymentId     = $session->getPaymentId();
        $amountPreVAT  = $session->getAmountPreVAT();
        $vatPercentage = $session->getVATPercentage();
        $addedProducts = $session->getAddedProducts();
        $userData      = $handler->retrieveAccountAndContactByAccountId($accountId);
        $contactRef    = $userData['contact']['contact_ref'];
        $culture       = $userData['account']['language_code'];
        $email         = $userData['contact']['email'];

        $accountInfo = $handler->getPlusAccount($accountId);

        // Retrieve added products from WorldPay.
        $addedProducts = unserialize(base64_decode($addedProducts));

        if ($addedProducts) {
            $basket = new Basket($user);
            $basket->storeAddedProducts($addedProducts);

            $user = sfContext::getInstance()->getUser();
            $user->setAttribute('account_id', $accountId);
            $user->setAttribute('contact_ref', $contactRef);
            $user->setCulture($culture);
        }

        // Create description for ledger and payment record
        if (isset($basket) && $basket->hasBWMBeenAdded() && $basket->retrieveNonExistingProducts(true)) {
            $itemMsg = 'BWM_AND_CREDIT';
        } elseif (isset($basket) && $basket->hasBundleBeenAdded() && $basket->hasBWMBeenAdded()) {
            $itemMsg = 'BWM_AND_BUNDLE_PURCHASE';
        } elseif (isset($basket) && $basket->hasBundleBeenAdded()) {
            $bundle  = $basket->retrieveAddedBundle();
            $itemMsg = $bundle['group_name'] . ' bundle.';
        } elseif (isset($basket) && $basket->hasBWMBeenAdded()) {
            $itemMsg = 'BWM_PURCHASE';
        } else {
            $itemMsg = 'MAN_CREDIT';
        }

        // The timestamp returned from world pay is Java formatted, this converts it so PHP understands it
        if (!is_null($data['gateway_transaction_time'])) {
            $data['gateway_transaction_time'] = date("Y-m-d H:i:s", ($data['gateway_transaction_time'] / 1000));
        }

        // Check the Gateway Action, then Check the Transaction Status
        switch ($data['gateway_action']) {
            case 'checkout':
            case 'topup':
                if ($data['gateway_transaction_status'] == 'C') {
                    $data['transaction_status'] = 'Transaction Cancelled';
                } elseif ($data['gateway_transaction_status'] == 'Y') {
                    $data['transaction_status'] = 'Transaction Successful';
                } else {
                    $data['transaction_status'] = 'Transaction Cancelled';
                    $logger->err('Gateway Transaction Status was Invalid: ' . $data['gateway_transaction_status']);
                    return "topupC";
                }
                break;

            default:
                $data['transaction_status'] = 'Transaction Cancelled';
                $logger->err('Gateway Action Was Not Topup, so was Invalid: ' . $data['gateway_action']);
                return "topupC";
        }

        // For a successful Transaction a PDF Invoice Needs to be created.
        if ('Transaction Successful' == $data['transaction_status'] && in_array(
                $data['gateway_action'],
                array('topup', 'checkout')
            )
        ) {

            // Obtain the Billing Address for this Account
            try {
                $errorMessage   = "";
                $billingAddress = $handler->retrieveBillingAddress($accountId);
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $errorMessage = 'Plus.getPlusBillingAddress Hermes call has timed out. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            } catch (Hermes_Client_Exception $e) {
                $errorMessage = 'Plus.getPlusBillingAddress Hermes call is faulty. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            } catch (Exception $e) {
                $errorMessage = 'Plus.getPlusBillingAddress Failed. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            }
            $this->sendErrorEmail($errorMessage, 'WorldpayFailure :: Billing Address Error', $handler);

            $items = $basket->getInvoiceItems($data['payment_currency']);
            if ($accountInfo['account']['credit_balance'] < 0) {
                if ($basket->hasBundleBeenAdded()) {
                    $items[] = array(
                        'order_no'   => 0,
                        'item'       => 'Outstanding Credit',
                        'amount_net' => $accountInfo['account']['credit_balance'] * (-1),
                        'currency'   => $data['payment_currency'],
                    );
                } elseif (!$basket->hasBundleBeenAdded()) {
                    $items[] = array(
                        'order_no'   => 0,
                        'item'       => 'Outstanding Balance',
                        'amount_net' => $accountInfo['account']['credit_balance'] * (-1),
                        'currency'   => $data['payment_currency'],
                    );
                }

                // TEMPORARY CALL CREDIT LOGGING
                // This will be removed in the future when we address negative balance correctly.
                try {
                    $handler->logNegativeAccountBalanceReconcilliation($accountId, abs($accountInfo['account']['credit_balance']), $paymentId);
                } catch (Exception $e) {
                    $errorMessage = 'Plus.logCallCreditPurchase Hermes call has failed. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode();
                    $logger->err($errorMessage);
                }
            }

            // Build the PDF Invoice, since the Payment is Successful
            try {
                $errorMessage = "";
                $invoiceInfo  = $handler->createInvoiceDB(
                    array(
                        'locale'          => $culture,
                        'invoice_date'    => date("d/m/Y"),
                        'billing_period'  => date("M Y"),
                        'invoice_no'      => "PP " . date("my"),
                        'account_id'      => $accountId,
                        'billing_address' => array(
                            'organisation' => (!empty($billingAddress['organisation'])) ? $billingAddress['organisation'] : '',
                            'building'     => (!empty($billingAddress['building'])) ? $billingAddress['building'] : '',
                            'street'       => (!empty($billingAddress['street'])) ? $billingAddress['street'] : '',
                            'town'         => (!empty($billingAddress['town'])) ? $billingAddress['town'] : '',
                            'county'       => (!empty($billingAddress['county'])) ? $billingAddress['county'] : '',
                            'post_code'    => (!empty($billingAddress['postal_code'])) ? $billingAddress['postal_code'] : '',
                            'country'      => (!empty($billingAddress['country'])) ? $billingAddress['country'] : '',
                        ),
                        'items'           => $items,
                        'invoice_summary' => array(
                            'subtotal' => array(
                                'amount'   => $amountPreVAT,
                                'currency' => $data['payment_currency'],
                            ),
                            'vat'      => array(
                                'rate'     => number_Format($vatPercentage * 100, 0),
                                'amount'   => $vatPercentage,
                                'currency' => $data['payment_currency'],
                            ),
                            'total'    => array(
                                'amount'   => $data['payment_amount'],
                                'currency' => $data['payment_currency'],
                            )
                        )
                    )
                );
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $errorMessage = 'Plus.createInvoiceDB Hermes call has timed out. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            } catch (Hermes_Client_Exception $e) {
                $errorMessage = 'Plus.createInvoiceDB Hermes call is faulty. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            } catch (Exception $e) {
                $errorMessage = 'Plus.createInvoiceDB Failed. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            }
            $this->sendErrorEmail($errorMessage, 'WorldpayFailure :: Create Invoice Error', $handler);

            // Update Customer Card Address Details
            try {
                $errorMessage = "";
                $handler->updatePlusCreditCardAddress(
                    array(
                        'account_id'   => $accountId,
                        'organisation' => $data['creditcard_address_1'],
                        'building'     => $data['creditcard_address_2'],
                        'street'       => $data['creditcard_address_3'],
                        'town'         => $data['creditcard_town'],
                        'county'       => $data['creditcard_region'],
                        'postal_code'  => $data['creditcard_postcode'],
                        'country'      => $data['creditcard_country']
                    )
                );
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $errorMessage = 'Plus.updatePlusCreditCardAddress Hermes call has timed out. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            } catch (Hermes_Client_Exception $e) {
                $errorMessage = 'Plus.updatePlusCreditCardAddress Hermes call is faulty. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            } catch (Exception $e) {
                $errorMessage = 'Plus.updatePlusCreditCardAddress Failed. Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode();
                $logger->err($errorMessage);
            }
            $this->sendErrorEmail($errorMessage, 'WorldpayFailure :: Update Plus Credit Card Address', $handler);
        }

        $paymentArgs = array(
            'payment_id'             => $paymentId,
            'payment_gateway_id'     => $data['payment_gateway_id'],
            'payment_method_id'      => $data['payment_method_id'],
            'account_id'             => $accountId,
            'session_id'             => $cartID,
            'payment_amount'         => $amountPreVAT,

            'product_group_id'       => '', // Will be used on a much later date.
            'invoice_id'             => isset($invoiceInfo['invoice_id']) ? $invoiceInfo['invoice_id'] : '',
            'payment_time'           => $data['gateway_transaction_time'],

            'gateway_status_code'    => $data['gateway_status_code'],
            'gateway_status_message' => $data['gateway_status_message'],
            'gateway_transaction_id' => $data['gateway_transaction_id'],
            'transaction_status'     => $data['transaction_status'],

            'operation_type'         => 1,
            'description'            => $itemMsg, // Hermes CommonText item
        );

        // Check the Auto Topup.
        // The Else part needs to possibly be removed due to the Bundles, BWM subscriptions which also need access to
        // the recurring payment id.
        if ($autotopup) {
            $paymentArgs['recurring_payment_id'] = $data['recurring_payment_id'];
            $paymentArgs['autotopup_amount']     = $amountPreVAT;
            if (!empty($basket)) {
                $callCredits = $basket->retrieveCallCreditProducts();
                if ($callCredits) {
                    $callCredit = array_shift($callCredits);
                    // If call-credit with auto-topup has been purchased, set the amount to this value.
                    $paymentArgs['autotopup_amount'] = $callCredit['amount'];
                }
            }
        }

        // Main Hermes Call, Process the Payment from Worldpay
        try {
            $errorMessage = "";
            $handler->processPaymentGatewayResponse($paymentArgs);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $errorMessage = 'Plus.doProcessPaymentGatewayResponse Hermes call has timed out. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        } catch (Hermes_Client_Exception $e) {
            $errorMessage = 'Plus.doProcessPaymentGatewayResponse Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        } catch (Exception $e) {
            $errorMessage = 'Plus.doProcessPaymentGatewayResponse Failed. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        }
        $this->sendErrorEmail($errorMessage, 'WorldpayFailure :: Process Payment', $handler);

        // With all transactions, Email needs to be sent out. This needs to be last, to get the updated Account Balance
        try {
            $errorMessage = "";
            $accountInfo  = $handler->getPlusAccount($accountId);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $errorMessage = 'getPlusAccount Hermes call has timed out. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        } catch (Hermes_Client_Exception $e) {
            $errorMessage = 'getPlusAccount Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        } catch (Exception $e) {
            $errorMessage = 'getPlusAccount Failed. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        }
        $this->sendErrorEmail($errorMessage, 'WorldpayFailure :: Get Plus Account', $handler);

        // If the invoice was successful, process the basket.
        if (!empty($invoiceInfo['code']) && (int)$invoiceInfo['code'] === 204 && !empty($basket)) {
            $this->processBasketWithErrorHandling(
                $logger,
                $session,
                $handler,
                $user,
                $basket,
                $email,
                $accountInfo,
                $data,
                $addedProducts
            );
        }

        // Check the Gateway Action, then Check the Transaction Status
        if ($data['gateway_transaction_status'] == 'Y') {
            $logger->info('Payment was a Success. Showing Callback.');
            return "topupS";
        } else {
            $logger->info('Transaction was Unsuccessful. Showing Callback.');
            return "topupC";
        }
    }

    /**
     * Handles the case where a request is made with reference to a non-existing payment session, or empty action.
     * Also handles the case of cancelling a Top-Up bundle.
     *
     * Moved from the above function.
     *
     * @param WorldPayCallbackHandler $handler
     * @param int $paymentGatewayID
     * @param array $requestData
     * @param string $expectedInstallationId
     * @param sfFileLogger $worldpayLogger
     */
    private function handleWorldPayErrornousAndTopUpBundleRelatedCall(
        WorldPayCallbackHandler $handler,
        $paymentGatewayID,
        array $requestData,
        $expectedInstallationId,
        sfFileLogger $worldpayLogger
    ) {
        $worldpayLogger->info(
            'WorldPay Request is not linked to a Payment Session or the gateway action is not set: possibly futurepay cancellation or faulty call.'
        );

        // Check if the Payment was Cancelled - No Direct Way, so check if the fields have been returned
        // Array
        // (
        //     [installation] => 245372
        //     [msgType] => authResult
        //     [_SP_charEnc] => ISO-8859-1
        //     [futurePayStatusChange] => Customer Cancelled
        //     [charenc] => ISO-8859-1
        //     [instId] => 245372
        //     [futurePayId] => 6127138
        // )
        $recurringPaymentAgreementId = $requestData['recurring_payment_id'];
        $data                        = array(
            'status'          => $requestData['recurring_payment_status'],
            'topup_bundle_id' => $recurringPaymentAgreementId,
            'installation'    => $requestData['installation'],
        );

        // Customer Cancelled Check
        if ($this->isRecurringPaymentAgreementCancellation($data, $expectedInstallationId)) {
            $worldpayLogger->info('FuturePay cancellation for FuturePay ID: ' . $recurringPaymentAgreementId);

            // The Future Pay ID is the recurring_payment_id in the Topup_bundle table.
            // In Addition the payment_gateway_id will need to be passed,
            // since their could be multiple topup_bundle_ids which are the same but for different payment gateways
            $this->cancelRecurringPaymentAgreement(
                $handler,
                $recurringPaymentAgreementId,
                $paymentGatewayID,
                $worldpayLogger
            );
        } elseif ($this->isInformationalSuccessfulTopUpCall($requestData)) {
            $worldpayLogger->info(
                "A Recurring Payment was completed through Hermes. The FuturePay ID is {$requestData['recurring_payment_id']}."
            );
        } else {
            $errorMessage = 'Session ID / CartId Not Set. Payment Unknown / Cancelled. Request Args: ' . serialize($_REQUEST);
            $worldpayLogger->err($errorMessage);
            $this->sendErrorEmail($errorMessage, 'WorldpayFailure :: Recurring Payment Failure?', $handler);
        }
    }

    /**
     * Checks if the request made is only informational, namely for successful top-ups made.
     *
     * When the CDR calls Hermes to top-up, Hermes calls WorldPay to charge the customer. On success, WorldPay sends out
     * an HTTP request to the mainCallback. As it's already handled, all we need to do, is log the occurrence and move on.
     *
     * @param array $requestData
     * @return int
     * @author Maarten Jacobs
     */
    private function isInformationalSuccessfulTopUpCall(array $requestData)
    {
        $safeFuturePaymentId = preg_quote($requestData['recurring_payment_id']);
        $reqDescFormat       = "/^Payment \d+ of FuturePay agreement ID $safeFuturePaymentId$/";
        return preg_match($reqDescFormat, $requestData['description']);
    }

    /**
     * Tries to cancel a Recurring Payment Agreement.
     *
     * @param WorldPayCallbackHandler $handler
     * @param string $recurringPaymentAgreementId
     * @param int $paymentGatewayID
     * @param sfFileLogger $worldpayLogger
     * @author Maarten Jacobs
     */
    private function cancelRecurringPaymentAgreement(
        WorldPayCallbackHandler $handler,
        $recurringPaymentAgreementId,
        $paymentGatewayID,
        sfFileLogger $worldpayLogger
    ) {
        try {
            $response = $handler->deactivateAutoTopupByTopupBundleID($recurringPaymentAgreementId, $paymentGatewayID);
            $handler->notifyPostPayAdminForMissingRecurringPaymentAgreement($response['account_id']);
            $worldpayLogger->info(
                'Plus.deactivateAutoTopupByTopupBundleID was Called Successful. Request: ' . serialize(
                    $_REQUEST
                ) . ', futurePayId: ' . $recurringPaymentAgreementId . ' , Response: ' . serialize($response)
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $worldpayLogger->err('Plus.deactivateAutoTopupByTopupBundleID Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            $worldpayLogger->err(
                'Plus.deactivateAutoTopupByTopupBundleID Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            $worldpayLogger->err(
                'Plus.deactivateAutoTopupByTopupBundleID Failed. Args: ' . serialize(
                    array('topup_bundle_id' => $recurringPaymentAgreementId, 'payment_gateway_id' => 1)
                ) . ', Error Message: ' . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode()
            );
        }
    }

    /**
     * Asserts that, based on the request data, the request is made for cancelling a Recurring Payment Agreement.
     *
     * @param array $data
     * @param string $expectedInstallationId
     * @return bool
     * @author Maarten Jacobs
     */
    private function isRecurringPaymentAgreementCancellation(array $data, $expectedInstallationId)
    {
        return !is_null($data['status']) && !is_null($data['topup_bundle_id']) && !is_null(
            $data['installation']
        ) && $data['installation'] == $expectedInstallationId;
    }

    private function sendProductPurchaseEmail(
        WorldPayCallbackHandler $handler,
        Basket $basket,
        sfFileLogger $logger,
        $accountId,
        array $emailData
    ) {
        try {
            if ($basket->hasCallCreditBeenAdded() && count($basket->retrieveAddedProducts()) === 1) {
                $handler->sendAccountTopupEmail(
                    array(
                        'email'           => $emailData['email'],
                        'accountId'       => $accountId,
                        'productName'     => 'Manual Top-up of £' . $emailData['amountPreVAT'],
                        'balance'         => '£' . $emailData['credit_balance_formatted'],
                        'transactionDate' => $emailData['gateway_transaction_time'],
                        'orderNo'         => $emailData['paymentId'],
                        'orderStatus'     => $emailData['transaction_status'],
                        'status'          => $emailData['transaction_status'] == 'Transaction Successful' ? 'success' : 'failure',
                    )
                );
            } else {
                $basketProductTypes = $this->prepareProductPricesForEmail(
                    $this->collectProductTypesFromBasketProducts(
                        $basket->retrieveAddedProducts()
                    )
                );
                $handler->sendProductSummaryEmail($accountId, $basketProductTypes);
            }
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $logger->err(
                'Plus.sendAccountTopupEmail or Plus.sendProductSummaryEmail Hermes call has timed out. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Hermes_Client_Exception $e) {
            $logger->err(
                'Plus.sendAccountTopupEmail or Plus.sendProductSummaryEmail Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            $logger->err(
                'Plus.sendAccountTopupEmail or Plus.sendProductSummaryEmail Failed. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
    }

    /**
     * Prepares the product prices used in the product-summary email.
     *
     * The required pattern for any cost specifies exactly 2 digits of precision. This method enforces that pattern.
     *
     * @param array $productTypes
     *   A map containing product types (from the basket most likely) linked to their cost and their product name.
     * @return array
     *   The map as it was given, but with the product cost in the correct format.
     * @author Maarten Jacobs
     */
    private function prepareProductPricesForEmail(array $productTypes)
    {
        foreach ($productTypes as $type => $product) {
            $productTypes[$type]['cost'] = sprintf("%.2f", $product['cost']);
        }
        return $productTypes;
    }

    /**
     * Creates a map of product types to their cost and product name.
     *
     * This map is used in the product-summary email.
     *
     * @param array $addedProducts
     * @return array
     * @author Maarten Jacobs
     */
    private function collectProductTypesFromBasketProducts(array $addedProducts)
    {
        $types = array();
        foreach ($addedProducts as $product) {
            if ($product instanceof Bundle || isset($product['second_allowance'])) {
                if (is_array($product) && isset($product['second_allowance'])) {
                    $product = new Bundle($product);
                }
                $types['bundle'] = array(
                    'cost'         => $product->calculateMasterProRataCost(),
                    'product_name' => $product->getGroupName(),
                );
                if ($product->hasAddon()) {
                    $types['bundle_addon'] = array(
                        'cost'         => $product->getAddon()->calculateMasterProRataCost() - $types['bundle']['cost'],
                        'product_name' => $product->getAddon()->getGroupName(),
                    );
                }
            } elseif (isset($product[1]['BWM_total_cost'])) {
                $type = 'bwm';
                if (!isset($types[$type])) {
                    $types[$type] = array(
                        'cost'         => 0,
                        'product_name' => array(),
                    );
                }
                $types[$type]['cost'] += (float)$product[1]['BWM_total_cost']['total'] + (float)$product[1]['BWM_total_cost']['connectionFee'];
                $types[$type]['product_name'][] = $product[0]['bwm_label'];
            } elseif (isset($product['type']) && $product['type'] === 'credit') {
                $types['call_credit'] = array(
                    'cost'         => $product['amount'],
                    'product_name' => $product['label'],
                );
            }
        }
        return $types;
    }

    /**
     * Handles the setup for a new Recurring Payment Agreement, and, if any, purchasing the requested products.
     *
     * @param string $cartID
     * @param string $futurePayId
     * @param WorldPayCallbackHandler $handler
     * @param sfFileLogger $logger
     * @param myUser $user
     * @return string
     * @author Maarten Jacobs
     */
    public function handleRecurringPaymentAgreementCallback(
        $cartID,
        $futurePayId,
        WorldPayCallbackHandler $handler,
        sfFileLogger $logger,
        myUser $user
    ) {
        // Retrieve the payment session, but stop if no session is found.
        $session = $this->attemptToRetrievePaymentSession($cartID, $handler);
        if (!$session) {
            $logger->err(
                'Session ID / CartId Not Set. Payment Unknown / Cancelled. Request Args: ' . serialize($_REQUEST)
            );
            return sfView::NONE;
        }

        // Handle session without auto top-up set, or empty futurePayId.
        if (!$session->getAutotopup() || !$futurePayId) {
            $logger->err(
                'Payment session was not marked as auto top-up and/or empty futurePayId. Request Args: ' . serialize(
                    $_REQUEST
                )
            );
            return sfView::NONE;
        }

        // Setup top-up bundle.
        $accountId = $session->getAccountId();
        $result    = $this->setupNewRecurringPaymentAgreement($accountId, $futurePayId, $handler);
        if (!$result) {
            $logger->err(
                "Unable to create new Top-Up bundle for customer with account id $accountId. Given FuturePayId is $futurePayId"
            );
            return sfView::NONE;
        }

        // Trigger the basket process, if necessary.
        $this->processBasket($session, $handler, $logger, $user);

        return sfView::NONE;
    }

    /**
     * Triggers the BasketProcess to purchase the products of the Basket.
     *
     * @param PaymentSession $session
     * @param WorldPayCallbackHandler $handler
     * @param sfFileLogger $logger
     * @param myUser $user
     * @return bool
     * @author Maarten Jacobs
     */
    private function processBasket(
        PaymentSession $session,
        WorldPayCallbackHandler $handler,
        sfFileLogger $logger,
        myUser $user
    ) {
        // First check: is there even anything in the basket we need to process?
        if (!$session->hasAddedProducts()) {
            return false;
        }

        // Retrieve all variables for the second check.
        $accountId = $session->getAccountId();
        $userData  = $handler->retrieveAccountAndContactByAccountId($accountId);
        $email     = $userData['contact']['email'];
        $handler->loginUser($email, $userData['contact']);
        $user->setAttribute('account_id', $accountId);
        $basket = new Basket($user);

        $addedProducts = unserialize(base64_decode($session->getAddedProducts()));
        $basket->storeAddedProducts($addedProducts);

        $accountInfo = $handler->getAccountBalanceContainerAndUsers($accountId, $user->getContactRef());
        /** @var Balance $balance */
        $balance = $accountInfo['balance'];
        if ((float)$balance->raw() < $basket->calculateTotalCost()) {
            return false;
        }

        // Checks passed: trigger BasketProcess.
        $this->processBasketWithErrorHandling(
            $logger,
            $session,
            $handler,
            $user,
            $basket,
            $email,
            $accountInfo,
            array(),
            $addedProducts
        );
        return true;
    }

    /**
     * Sets up a new Recurring Payment Agreement for the account.
     *
     * This is stored as a "top-up bundle" in the database.
     *
     * @param int $accountId
     * @param string $futurePayId
     * @param WorldPayCallbackHandler $handler
     * @return bool
     * @author Maarten Jacobs
     */
    private function setupNewRecurringPaymentAgreement($accountId, $futurePayId, WorldPayCallbackHandler $handler)
    {
        try {
            $handler->createTopupBundle($accountId, $futurePayId);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Tries to retrieve the payment session (i.e. Cart) of the WorldPay request.
     *
     * @param string $cartID
     * @param WorldPayCallbackHandler $handler
     * @return bool|PaymentSession
     * @author Maarten Jacobs
     */
    private function attemptToRetrievePaymentSession($cartID, WorldPayCallbackHandler $handler)
    {
        if ($cartID) {
            return $handler->retrievePaymentSession($cartID);
        }
        return false;
    }

    /**
     * Stores the result from BasketProcess, linking to the Cart in the process.
     *
     * This allows us to display the result of the purchase on a later time, instead of storing it in a PHP session.
     *
     * @param string $cartID
     * @param array $processedPayment
     * @param WorldPayCallbackHandler $handler
     * @return array
     * @author Maarten Jacobs
     */
    private function storeProcessedPayment($cartID, array $processedPayment, WorldPayCallbackHandler $handler)
    {
        return $handler->storeProcessedPayment($cartID, base64_encode(serialize($processedPayment)));
    }

    /**
     * Processes the basket and sends a product summary email on success.
     *
     * The processing is wrapped in exception handlers, and the error messages of the basket process are collected and
     * then sent to the dev team.
     *
     * @param sfFileLogger $logger
     * @param PaymentSession $session
     * @param WorldPayCallbackHandler $handler
     * @param myUser $user
     * @param Basket $basket
     * @param string $email
     * @param array $accountInfo
     * @param array $data
     * @param array $addedProducts
     *
     * @author Maarten Jacobs
     */
    private function processBasketWithErrorHandling(
        sfFileLogger $logger,
        PaymentSession $session,
        WorldPayCallbackHandler $handler,
        myUser $user,
        Basket $basket,
        $email,
        $accountInfo,
        $data,
        array $addedProducts
    ) {
        // The basket processor either returns false or the summary details array.
        // However, it only returns false if the basket is empty.
        // So as long as the basket is created only if we have products to process, then the basket processor will
        // not return false.
        $basketProcessErrors = array();
        try {
            $summaryDetails = $handler->processBasket($user, $session);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $message = sprintf(
                'Basket Processor call has timed out. Error message: "%s", Error Code: %s',
                $e->getMessage(),
                $e->getCode()
            );
            $logger->err($message);
            $basketProcessErrors[] = $message;
        } catch (Hermes_Client_Exception $e) {
            $message = sprintf(
                'Basket Processor Hermes call is faulty. Error message: "%s", Error Code: %s',
                $e->getMessage(),
                $e->getCode()
            );
            $logger->err($message);
            $basketProcessErrors[] = $message;
        } catch (Exception $e) {
            $message = sprintf(
                'Basket Processor Failed. Error message: "%s", Error Code: %s',
                $e->getMessage(),
                $e->getCode()
            );
            $logger->err($message);
            $basketProcessErrors[] = $message;
        }

        // Regardless of errors during basket processing (except exceptions), we need to store the basket process result.
        if (!empty($summaryDetails)) {
            $this->storeProcessedPayment($session->getCartID(), $summaryDetails, $handler);
        }

        if (empty($summaryDetails) || $summaryDetails['error']) {
            if (!empty($summaryDetails['error_messages'])) {
                $basketProcessErrors = array_merge($summaryDetails['error_messages'], $basketProcessErrors);
            }
            $handler->sendBasketProcessErrorMail($basketProcessErrors);
        } else {
            $basket->storeAddedProducts($addedProducts);
            $basketContainsBundle = $basket->hasBundleBeenAdded();
            $this->sendProductPurchaseEmail(
                $handler,
                $basket,
                $logger,
                $session->getAccountId(),
                array(
                    'email'                    => $email,
                    'amountPreVAT'             => $session->getAmountPreVAT(),
                    'credit_balance_formatted' => $accountInfo['account']['credit_balance_formatted'],
                    'gateway_transaction_time' => $data['gateway_transaction_time'],
                    'paymentId'                => $session->getPaymentId(),
                    'transaction_status'       => $data['transaction_status'],
                )
            );
            $this->handleIncentivesForPurchase(
                $session->getAffiliateCode(),
                $session->getAccountId(),
                $session->getCartID(),
                $basket,
                $logger,
                $handler
            );
            $basket->clearBasket();

            // Get the updated account balance.
            $logger->info('Checking if it is a bundle customer and has positive balance.');
            $accountInfo = $this->getPlusAccountWithErrorHandling($logger, $handler, $accountInfo['account']['id']);
            $updatedCreditBalance = (float) $accountInfo['account']['credit_balance'];
            // Check if we need to refund the user; if so, notify webteam.
            if ($basketContainsBundle && $updatedCreditBalance >= 0.01) {
                $logger->info('Sending email to webteam for customer ' . $accountInfo['account']['id'] . ' with positive balance after bundle purchase.');
                $handler->sendBundleCustomerWithPositiveBalanceEmail($accountInfo['account']['id'], $updatedCreditBalance);
            }
        }
    }

    /**
     * Determines which purchases will be turned into incentives, and passes it to the Incentives handler in Hermes.
     *
     * @param string $referrer
     * @param int $accountId
     * @param string $cartId
     * @param Basket $basket
     * @param sfFileLogger $logger
     * @param WorldPayCallbackHandler $handler
     * @return bool
     * @author Maarten Jacobs
     */
    private function handleIncentivesForPurchase(
        $referrer,
        $accountId,
        $cartId,
        Basket $basket,
        sfFileLogger $logger,
        WorldPayCallbackHandler $handler
    ) {
        // For the moment, we ignore purchases made by the customer without c/s intervention.
        if (!$referrer) {
            $referrer = 'customer';
        }

        $addedBWMs = $basket->retrieveNonExistingProducts();
        /** @var Bundle $addedBundle */
        $addedBundle      = $basket->retrieveAddedBundleObject();
        $addedCallCredits = $basket->retrieveCallCreditProducts();

        $purchases = array();
        if ($addedBWMs) {
            $purchases['bwms'] = array();
            foreach ($addedBWMs as $bwm) {
                $bwmName             = $bwm[0]['bwm_label'];
                $purchases['bwms'][] = array(
                    'bwm_name'                => $bwmName,
                    'dedicated_numbers_count' => $bwm[1]['BWM_total_cost']['qty'],
                    'setup_cost'              => $bwm[1]['BWM_total_cost']['connectionFee'],
                    'dnis_rate'               => $bwm[1]['BWM_total_cost']['dnisPerRate'],
                );
            }
        }
        if ($addedBundle) {
            $purchases['bundles']   = array();
            $purchases['bundles'][] = array(
                'bundle_name' => $addedBundle->getGroupName(),
                'total_cost'  => $addedBundle->getMasterMonthlyPrice(),
            );
            if ($addedBundle->hasAddon()) {
                $addonBundle            = $addedBundle->getAddon();
                $purchases['bundles'][] = array(
                    'bundle_name' => $addonBundle->getGroupName(),
                    'total_cost'  => $addonBundle->getMasterMonthlyPrice() - $addedBundle->getMasterMonthlyPrice(),
                );
            }
        }
        if ($addedCallCredits) {
            $callCredit               = reset($addedCallCredits);
            $purchases['call_credit'] = array(
                'credit_amount' => $callCredit['amount'],
            );
        }

        if ($purchases) {
            try {
                $handler->handleIncentivesForPlusPurchase($referrer, $accountId, $cartId, $purchases);
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $logger->err(
                    'Incentives.determineAndStoreIncentivesForPlusPurchase Hermes call has timed out. Error message: ' .
                    $e->getMessage() . ', Error Code: ' . $e->getCode()
                );
            } catch (Hermes_Client_Exception $e) {
                $logger->err(
                    'Incentives.determineAndStoreIncentivesForPlusPurchase Hermes call is faulty. Error message: ' .
                    $e->getMessage() . ', Error Code: ' . $e->getCode()
                );
            } catch (Exception $e) {
                $logger->err(
                    'Incentives.determineAndStoreIncentivesForPlusPurchase Failed. Error message: ' . $e->getMessage() .
                    ', Error Code: ' . $e->getCode()
                );
            }
        }

        return true;
    }

    /**
     * Sends an Email to the WebTeam of Error Messages in Hermes Calls during the Worldpay Transaction.
     *
     * The Message will contain the Hermes Error Message along with a dump of the the POST data, the SERVER data and the SESSION data.
     * In Addition the Error Title will be the Email Title.
     *
     * @param $errorMessage
     * @param $errorTitle
     * @param $handler
     *
     * @author Asfer Tamimi
     */
    private function sendErrorEmail($errorMessage, $errorTitle, WorldPayCallbackHandler $handler)
    {
        if ("" !== $errorMessage) {
            $handler->sendErrorEmail($errorMessage, $errorTitle);
        }
    }

    private function getPlusAccountWithErrorHandling(sfFileLogger $logger, WorldPayCallbackHandler $handler, $accountId)
    {
        try {
            $errorMessage = "";
            $accountInfo = $handler->getPlusAccount($accountId);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $errorMessage = 'getPlusAccount Hermes call has timed out. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        } catch (Hermes_Client_Exception $e) {
            $errorMessage = 'getPlusAccount Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        } catch (Exception $e) {
            $errorMessage = 'getPlusAccount Failed. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode();
            $logger->err($errorMessage);
        }
        $this->sendErrorEmail($errorMessage, 'WorldpayFailure :: Get Plus Account', $handler);

        if (isset($accountInfo)) {
            return $accountInfo;
        } else {
            return null;
        }
    }
}
