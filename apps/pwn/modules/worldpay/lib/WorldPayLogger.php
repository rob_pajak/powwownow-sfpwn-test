<?php

class WorldPayLogger extends sfFileLogger
{
    public function __construct()
    {
        $logPath = sfConfig::get('sf_log_dir').'/worldpay.log';
        parent::__construct(new sfEventDispatcher(), array('file' => $logPath));
        $this->setLogLevel(7);
    }
}
