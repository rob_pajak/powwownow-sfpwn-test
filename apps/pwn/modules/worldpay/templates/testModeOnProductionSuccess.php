<div class="container_24 clearfix">
    <div class="grid_24">
        <img width="183" height="86" alt="Powwownow" src="/i/245372/logo.png" id="powwownow-logo"/>
        <img id="worldpay-logo" alt="Powered by WorldPay" src="https://secure.worldpay.com/jsp/shopper/pictures/poweredByWorldPay.gif"/>
    </div>

    <div class="grid_24">
        <?php echo $sf_data->getRaw('flow'); ?>

        <h1 class="mypwn_sub_page_heading mypwn_sub_page_heading_l mypwn_sub_page_heading_plus">
            TestMode is not valid for this application.
        </h1>
    </div>
    <div class="grid_12">
        <p><WPDISPLAY ITEM=banner></p>
    </div>
</div>