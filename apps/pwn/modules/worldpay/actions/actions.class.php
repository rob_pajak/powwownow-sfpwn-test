<?php

/**
 * Worldpay Actions: Initially Callbacks with the WorldPay API
 *
 * @package    powwownow
 * @subpackage worldpay
 * @author     Asfer Tamimi
 * @version    SVN: $Id: actions.class.php 23810 2013-04-03 17:45:00Z Asfer Tamimi$
 */
class worldpayActions extends sfActions
{
    /**
     * Asserts the IP address from the request is within our list of valid IP addresses.
     *
     * The list of valid IP addresses are the addresses of WorldPay, and of our office.
     * This ensures no one tops up his own account.
     *
     * @param string $ipAddress
     * @param array $whitelistedIPs
     * @return bool
     * @author Maarten Jacobs
     */
    private function isValidIPAddress($ipAddress, array $whitelistedIPs)
    {
        foreach ($whitelistedIPs as $addressPattern) {
            switch ($addressPattern['type']) {
                case 'pattern':
                    if (preg_match("/^{$addressPattern['ip']}$/", $ipAddress)) {
                        return true;
                    }
                    break;
                case 'literal':
                    if ($ipAddress === $addressPattern['ip']) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    /**
     * This is the Main Callback Method in which Worldpay calls to display the Initial WorldPay Callback Page.
     * The Callback page can be different depending on the User Action.
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeMainCallback(sfWebRequest $request) {

        // Worldpay Logging
        $logPath = sfConfig::get('sf_log_dir').'/worldpay.log';
        $worldpayLogger = new sfFileLogger(new sfEventDispatcher(), array('file' => $logPath));
        $worldpayLogger->setLogLevel(7); // Set to Lowest (Debug). It will show All Log Entries.


        $ipRetriever        = new IpRetriever($request);
        $ipAddress          = $ipRetriever->retrieveIPAddress($request);
        $allowedIpAddresses = sfConfig::get('app_worldpay_ips');
        if (!$this->isValidIPAddress($ipAddress, $allowedIpAddresses)) {
            $worldpayLogger->err('WorldPay/mainCallback was called from an invalid IP address: ' . $ipAddress);
            $this->getResponse()->setStatusCode(400);
            return sfView::NONE;
        }
        $worldpayLogger->info("WorldPay/Callback was called from one of the valid IP address: $ipAddress.");

        $this->handleInvalidTestModeCalls($request->getParameter('testMode'), $ipAddress, $worldpayLogger);

        // Check the request. Only Allow Post Requests to Continue. Log the NON Post Attempt
        if (!$request->isMethod('post')) {
            $worldpayLogger->err('This is Not a Post Request.');
            $this->getResponse()->setStatusCode(400);
            return sfView::NONE;
        }

        // APP Config Settings for WorldPay
        $paymentGateway        = sfConfig::get('app_payment_default','worldpay');
        $paymentGatewayDetails = sfConfig::get('app_'.$paymentGateway);

        sfContext::getInstance()->getConfiguration()->loadHelpers('countryCodes');

        $actionHandler = new WorldPayActionHandler();
        $worldPayAction = $actionHandler->handleMainCallback($request, $worldpayLogger, new WorldPayCallbackHandler(), $paymentGatewayDetails, $this->getUser());
        if ($worldPayAction === 'topupC' || $worldPayAction === 'topupS') {
            // Set the Navigation Flow for the Callback Page
            $this->flow = $request->getPostParameter('MC_navList_CB','<WPDISPLAY ITEM=MC_navList_CB>');

            $this->forward("worldpay", $worldPayAction);
        }
        return sfView::NONE;
    }

    /**
     * This is the Method for the Successful Callback page
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeTopupS(sfWebRequest $request) {
        if (!isset($this->flow)) {
            $this->flow = $request->getParameter('MC_navList_CB','<WPDISPLAY ITEM=MC_navList_CB>');
        }
    }

    /**
     * This is the Method for the Unsuccessful Callback page
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeTopupC(sfWebRequest $request){
        if (!isset($this->flow)) {
            $this->flow = $request->getParameter('MC_navList_CB','<WPDISPLAY ITEM=MC_navList_CB>');
        }
    }

    /**
     * Handles the request from WorldPay to setup a Recurring Payment Agreement.
     *
     * Additionally, the request might be linked to a purchase, but no charges were made through WorldPay.
     * So, if there is a basket, the cost of the basket will be withdrawn from the existing balance.
     *
     * @param sfWebRequest $request
     * @return string
     * @author Maarten Jacobs
     */
    public function executeRecurringPaymentAgreementSetup(sfWebRequest $request)
    {
        $this->forward404Unless($request->getMethod() === 'POST');

        $logger = new WorldPayLogger();
        $logger->info('Received a callback to create a new Recurring Payment Agreement.');

        $ipRetriever        = new IpRetriever($request);
        $ipAddress          = $ipRetriever->retrieveIPAddress($request);
        $allowedIpAddresses = sfConfig::get('app_worldpay_ips');
        if (!$this->isValidIPAddress($ipAddress, $allowedIpAddresses)) {
            $logger->err('WorldPay/mainCallback was called from an invalid IP address: ' . $ipAddress);
            $this->getResponse()->setStatusCode(400);
            return sfView::NONE;
        }
        $logger->info("WorldPay/Callback was called from one of the valid IP address: $ipAddress.");

        $this->handleInvalidTestModeCalls($request->getParameter('testMode'), $ipAddress, $logger);

        $cartID = $request->getPostParameter('cartId');
        $futurePayId = $request->getPostParameter('futurePayId');
        $handler = new WorldPayCallbackHandler();
        $actionHandler = new WorldPayActionHandler();
        return $actionHandler->handleRecurringPaymentAgreementCallback($cartID, $futurePayId, $handler, $logger, $this->getUser());
    }

    /**
     * Checks if the APPLICATION_ENV server variable is set to production.
     *
     * @return bool
     */
    private function onProduction()
    {
        return isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] === 'production';
    }

    /**
     * Handles requests to the Production WorldPay actions which are marked as test.
     *
     * The full criteria is:
     *  - On Production,
     *  - The request is for testMode,
     *  - The request is made through one of our proxies.
     *
     * @param string $testMode
     * @param string $ipAddress
     * @param sfFileLogger $logger
     */
    private function handleInvalidTestModeCalls($testMode, $ipAddress, sfFileLogger $logger)
    {
        $testMode = (int) $testMode;
        $ipRetriever = new IpRetriever();
        $isProxyIp   = $ipRetriever->isProxyIp($ipAddress);
        if ($this->onProduction() && $testMode !== 0 && $isProxyIp) {
            $logger->err('TestMode request was made on production. The relevant data is: ');
            $logger->err('- the data sent via POST (serialised) was "' . serialize($_POST) . '".');
            $logger->err('- the server data (serialised) was "' . serialize($_SERVER) . '".');
            $logger->err('- the session data (serialised) was "' . serialize($_SESSION) . '".');

            $this->forward('worldpay', 'testModeOnProduction');

            $this->notifyWebTeam('WorldPay TestMode request was made on production!');
        }
    }

    /**
     * Notifies the webteam of a possible breach of security.
     *
     * The message of the mail contains the subject (meant as title), the POST data, the SERVER data and the SESSION data.
     *
     * @param string $subject
     *
     * @author Maarten Jacobs
     */
    private function notifyWebTeam($subject)
    {
        $lineSeparator = "\r\n";
        $message = array(
            $subject,
            '',
            'POST data:',
            print_r($_POST, true),
            '',
            'SERVER data:',
            print_r($_SERVER, true),
            '',
            'SESSION data:',
            print_r($_SESSION, true),
        );
        $message = wordwrap(implode($lineSeparator, $message), 70, $lineSeparator);
        mail('webteam@powwownow.com', $subject, $message);
    }

    /**
     * Displays an error message page for users making a test-mode request on production.
     *
     * @param sfWebRequest $request
     */
    public function executeTestModeOnProduction(sfWebRequest $request)
    {
        $this->flow = $request->getParameter('MC_navList_CB','<WPDISPLAY ITEM=MC_navList_CB>');
    }
}
