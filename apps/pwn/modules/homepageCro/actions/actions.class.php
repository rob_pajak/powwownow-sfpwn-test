<?php

/**
 * homepageCro actions.
 *
 * @package    powwownow
 * @subpackage homepageCro
 * @author     Vitaly
 */
class homepageCroActions extends sfActions
{
    /**
     * New Homepage has now been moved to the New Pages Module, so that the GTM Filter can be applied
     * @param sfWebRequest $request
     * @author Asfer
     */
    public function executeIndexE(sfWebRequest $request) {
        $this->forward('pages','indexE');
    }

    /**
     * New Homepage - Wow!
     * @author Dav
     * @param sfWebRequest $request
     */
    public function executeIndexG(sfWebRequest $request)
    {
        $this->setLayout('html5-homepageG-layout');
    }

    /**
     * @param sfWebRequest $request
     */
    public function executeIndexGRedirect(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@homepage'), 301);
    }

    /**
     * @param sfWebRequest $request
     * @return string
     */
    public function executeWelcomePackAjax(sfWebRequest $request){

        $this->setLayout('nolayout');

        if (!$request->isXmlHttpRequest()) {
            //$this->logMessage($this->getRequest()->getUri() . ' was not requested not as an xmlHttpRequest', 'warning');
            //$this->forward404();
        }

        $postData = $request->getParameterHolder()->getAll();

        // server side validation
        $errorMessages = array();

        $company = isset($postData['company']) ? $postData['company'] : '';
        $building = isset($postData['building']) ? $postData['building'] : '';
        $street = isset($postData['street']) ? $postData['street'] : '';
        $town = isset($postData['town']) ? $postData['town'] : '';
        $postalCode = isset($postData['postal_code']) ? $postData['postal_code'] : '';
        $countryCode = isset($postData['country_code']) ? $postData['country_code'] : '';


        if (empty($building)) {
            $errorMessages[] = array('message' => 'FORM_VALIDATION_NO_ADDRESS', 'field_name' => 'building');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 412 ));
            $this->getResponse()->setHttpHeader('Content-type','application/json');

            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        if (empty($town)) {
            $errorMessages[] = array('message' => 'FORM_VALIDATION_NO_TOWN', 'field_name' => 'town');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 412 ));
            $this->getResponse()->setHttpHeader('Content-type','application/json');

            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        if (empty($postalCode)) {
            $errorMessages[] = array('message' => 'FORM_VALIDATION_INVALID_POSTCODE', 'field_name' => 'postal_code');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 412 ));
            $this->getResponse()->setHttpHeader('Content-type','application/json');

            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        //@todo: check postcode validity on server side as well

        $locale = (isset($_SERVER['LOCALE'])) ? $_SERVER['LOCALE'] : 'en_GB';

        
        // Request wallet card request to HERMES
        $args = array('pin_ref' => $_SESSION['reg_pin_ref'], 'contact_ref' => $this->getUser()->getAttribute('contact_ref'));


        try {
            // @todo: why is $walletcardDetails used? It is undefined before usage!
            $apiResponse = Hermes_Client_Rest::callJson('getWalletCardAddress', $walletcardDetails);
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getWalletCardAddress method failed during the registration process ' . serialize($e)
            );

            $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
            $this->getResponse()->setHttpHeader('Content-type','application/json');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR1', 'field_name' => 'alert');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        if (count($apiResponse) == 0 ) {

            $args = array(
                'pin_ref' => $_SESSION['reg_pin_ref'],
                'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                'organisation' => $company,
                'building' => $building,
                'street' => $street,
                'town' => $town,
                'county' => '',
                'post_code' => $postalCode,
                'country_code' => $countryCode
            );


            try {
                Hermes_Client_Rest::callJson('createWalletCardRequest', $args);
            } catch (Hermes_Client_Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'createWalletCardRequest method failed during the registration process ' . serialize($e)
                );

                $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
                $this->getResponse()->setHttpHeader('Content-type','application/json');
                $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR2', 'field_name' => 'alert');
                echo json_encode(array('error_messages' => $errorMessages));
                return sfView::NONE;
            }            
             
        } else {
            $this->getResponse()->setStatusCode(500);
            $this->getResponse()->setHttpHeader('Content-type','application/json');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR3', 'field_name' => 'alert');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;             
        }

        // fetch the template for the next step
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
        $html = $this->getPartial('whatsNext');

        $this->getResponse()->setHttpHeader('Content-type','application/json');
        echo json_encode(array('html' => $html));

        return sfView::NONE;
    }

    /**
     * @param sfWebRequest $request
     * @return string
     */
    public function executePinReminderAjax(sfWebRequest $request){

        $this->setLayout('nolayout');

        if (!$request->isXmlHttpRequest()) {
            //$this->logMessage($this->getRequest()->getUri() . ' was not requested not as an xmlHttpRequest', 'warning');
            //$this->forward404();
        }

        $postData = $request->getParameterHolder()->getAll();

        // server side validation
        $errorMessages = array();

        $email = isset($postData['email']) ? $postData['email'] : '';
        $mobileNumber = isset($postData['mobileNumber']) ? $postData['mobileNumber'] : '';
        
        if (empty($email)) {
            $errorMessages[] = array('message' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'field_name' => 'email');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setHttpHeader('Content-type','application/json');

            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Check if email is valid
        $v = new sfValidatorEmail();

        try {
            $email = $v->clean($email);
        } catch (sfValidatorError $e) {
            $errorMessages[] = array('message' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'field_name' => 'email');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setHttpHeader('Content-type','application/json');

            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }


        // PIN Reminder request

        $locale = isset($_SERVER["locale"]) ? $_SERVER["locale"] : 'en_GB';

        preg_match('/(http|https):\/\/.+?(\/.*)/', $request->getReferer(), $refRequestMatch);
        $refRequest = isset($refRequestMatch[2]) ? $refRequestMatch[2] : '';

        $args = array('email'  => $email,
                      'locale' => $locale,
                      'request_source' => $refRequest);

        if (!empty($mobileNumber)) {
            $args['mobile_number'] = $mobileNumber;
        }

        try {
            Hermes_Client_Rest::callJson('sendPinReminderEmail', $args);
        } catch (Hermes_Client_Exception $e) {
            // Filter out the error message we want to send to the user, and pass the JS constant name of the relevant error message
            switch ($e->getCode()) {
                case '001:000:000':
                    $errorMessages[] = array('message' => 'API_RESPONSE_NO_ACCOUNT_FOR_EMAIL', 'field_name' => 'email');
                    $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
                    $this->getResponse()->setHttpHeader('Content-type','application/json');

                    echo json_encode(array('error_messages' => $errorMessages, 'email' => $email, 'code' => $e->getCode()));
                    return sfView::NONE;

                default:
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    $this->getResponse()->setStatusCode($e->getHTTPStatusCode());
                    $this->getResponse()->setHttpHeader('Content-type','application/json');

                    echo json_encode(array('error_messages' => $errorMessages));
                    return sfView::NONE;
            }
        }


        //  if (isset($pin)) {$html = str_replace('%%pin%%', $pin, $html); }
        //  if (isset($defaultDialInNumber)) { $html = str_replace('%%defaultDialInNumber%%', $defaultDialInNumber, $html); }

        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
        $html = $this->getPartial('pinReminder');
            
        $this->getResponse()->setHttpHeader('Content-type','application/json');
        echo json_encode(array('html' => $html));

        return sfView::NONE;
        
    }

    /**
     * Controller for basic registration which also generates a password and logs user in
     *
     * Possible responses are:
     *   {"status":"success"}
     *   {"error_messages":[{"message":"FORM_RESPONSE_LOG_IN_UNABLE","field_name":"email"}]} When a pin was created but
     * an error occurred logging the user in
     *
     * @see $this->performRegistration() for other responses
     *
     * @author Wiseman
     */
    public function executeBasicRegistrationGAjax(sfWebRequest $request) {

        // If not an ajax request, forward to 404
        if (!$request->isXmlHttpRequest()) {
            $this->logMessage($this->getRequest()->getUri() . ' was not requested not as an xmlHttpRequest', 'warning');
            $this->forward404();
        }

        $this->getResponse()->setHttpHeader('Content-type','application/json');
        $this->setLayout(false);

        // Perform registration
        if (sfView::NONE == $registrationDetails = Common::performRegistration($request->getParameter('email'), 'generatePassword')) {
            $this->getResponse()->setStatusCode(400);
            return sfView::NONE;
        }

        try {
            if (Plus_Authenticate::logIn(
                $request->getParameter('email'), $registrationDetails['password'])) {
                echo json_encode(array('status' => 'success'));
                return sfView::NONE;
            }
        } catch (Exception $e) {
            $this->logMessage('Exception occurred when authenticating contact: ' . serialize($e), 'err');
        }

        $this->logMessage(
            'A newly created contact could not be authenticated: Email: ' . $request->getParameter('email') . ' password: ' .
             $registrationDetails['password'], 'err'
        );
        $this->getResponse()->setStatusCode(400);
        echo json_encode(
            array('error_messages' =>
                array(
                    array('message' => 'FORM_RESPONSE_LOG_IN_UNABLE', 'field_name' => 'email')
                )
            )
        );

        return sfView::NONE;
    }

    /**
     * @param sfWebRequest $request
     * @return string
     */
    public function executeTest(sfWebRequest $request) {
        var_dump($this->getUser()->getAttribute('browser_detection', array()));
        return sfView::NONE;
    }

    /**
     * @param sfWebRequest $request
     * @return string
     */
    public function executeTrackSocialMedia(sfWebRequest $request) {
        $trackParameters = array(
            'twitter' => $request->getPostParameter('twitter', null),
            'facebook' => $request->getPostParameter('facebook', null),
            'google' => $request->getPostParameter('google', null),
            'google_plus' => $request->getPostParameter('google_plus', null),
        );
        $emptyTracking = count(array_filter($trackParameters, 'is_null')) === count($trackParameters);

        if (!$request->isXmlHttpRequest() || !$request->isMethod('post') || $emptyTracking) {
            $this->forward404();
        }

        Hermes_Client_Rest::callJson('trackSocialMedia', $trackParameters);
        return sfView::NONE;
    }

    /**
     * Schedule A Call Outlook Plugin Download for Un-authenticated Users
     * @param sfWebRequest $request
     * @return string
     */
    public function executeOutlookPlugin(sfWebRequest $request)
    {

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->setContentType('application/json');
        $response->setStatusCode(500);

        $emailAddress = $request->getPostParameter('email_address', null);

        // Email is the Only Required Field - Do Validation
        if (!isset($emailAddress)) {
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => 'FORM_VALIDATION_NO_EMAIL',
                        'field_name' => 'email_address'
                    )
                )
            );
            return sfView::NONE;
        } elseif (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS',
                        'field_name' => 'email_address'
                    )
                )
            );
            return sfView::NONE;
        } elseif (strlen($emailAddress) < 6) {
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => 'FORM_VALIDATION_NO_EMAIL',
                        'field_name' => 'email_address'
                    )
                )
            );
            return sfView::NONE;
        }

        // Check if the Contact Exists
        $result = Common::getContact($emailAddress);

        // Check if there was an Error
        if (isset($result['error'])) {
            echo json_encode(
                array('error_messages' => array('message' => $result['error'], 'field_name' => 'email_address'))
            );
            return sfView::NONE;
        }

        // Check if there was no Contact with that Email Address. If there is none, create Full Enhanced Contact.
        if (empty($result)) {
            if (sfView::NONE == $registrationDetails = Common::performRegistration($emailAddress, 'generatePassword')) {
                return sfView::NONE;
            }
        }

        // Either the Contact Already Exists, OR a Contact has been Created Above. Update Contact with New Information
        $result1 = Common::updateContact(
            array(
                'contact_ref'    => (isset($result['contact_ref'])) ? $result['contact_ref'] : $registrationDetails['contact_ref'],
                'organisation'   => (!empty($result['organisation'])) ? $request->getPostParameter(
                        'company',
                        $result['organisation']
                    ) : $request->getPostParameter('company', ''),
                'job_title'      => (!empty($result['job_title'])) ? $request->getPostParameter(
                        'job_title',
                        $result['job_title']
                    ) : $request->getPostParameter('job_title', ''),
                'outlook_plugin' => true
            )
        );

        if (isset($result1['statusCode']) && $result1['statusCode'] != 202) {
            $this->logMessage('Contact Record Was Not Updated. Contact Ref: ' . $result['contact_ref'], 'err');
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_UPDATE_CONTACT_ERROR', 'field_name' => 'alert'))
            );
        } elseif (isset($result1['statusCode']) && $result1['statusCode'] == 202) {
            $response->setStatusCode(200);
            echo json_encode(
                array('success' => true, 'pin' => (isset($registrationDetails)) ? $registrationDetails['pin'] : 0)
            );
        } else {
            $this->logMessage(
                'Unknown Error Occurred in Updating Contact. Contact Ref: ' . $result['contact_ref'],
                'err'
            );
            echo json_encode(
                array('error_messages' => array('message' => 'REGISTRATION_ERROR', 'field_name' => 'alert'))
            );
        }

        return sfView::NONE;
    }
}
