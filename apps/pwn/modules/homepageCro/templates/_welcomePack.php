<h2 class="rockwell white">Order your free wallet card</h2>
<div class="grid_sub_12" style="z-index: 1000; position: relative">
    <form id="mypins-walletcard-form" enctype="application/x-www-form-urlencoded" method="post" action="/s/homepageCro/WelcomePackAjax" name="mypins-walletcard-form" class="label-top white margin-top-10">
        <div class="form-field">
            <label for="company">Company</label>
            <span class="mypwn-input-container">
                <input type="text" value="" id="company" name="company" class="mypwn-input input-large font-small" />
            </span>
        </div>

        <div class="form-field">
            <label for="building">Address</label>
            <span class="mypwn-input-container">
                <input type="text" value="" id="building" name="building" class="mypwn-input input-large font-small required" />
            </span>
        </div>

        <div class="form-field">
            <span class="mypwn-input-container">
                <input type="text" value="" id="street" name="street" class="mypwn-input input-large font-small" />
            </span>
        </div>

        <div class="form-field">
            <label for="town">Town</label>
            <span class="mypwn-input-container">
                <input type="text" value="" id="town" name="town" class="mypwn-input input-large font-small required" />
            </span>
        </div>

        <div class="form-field">
            <label for="postal_code">Postcode</label>
            <span class="mypwn-input-container">
                <input type="text" value="" id="postal_code" name="postal_code" pattern="^([A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKS-UWa-hjks-uw])\ {0,1}[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|([Gg][Ii][Rr]\ 0[Aa][Aa])|([Ss][Aa][Nn]\ {0,1}[Tt][Aa]1)|([Bb][Ff][Pp][Oo]\ {0,1}([Cc]\/[Oo]\ )?[0-9]{1,4})|(([Aa][Ss][Cc][Nn]|[Bb][Bb][Nn][Dd]|[BFSbfs][Ii][Qq][Qq]|[Pp][Cc][Rr][Nn]|[Ss][Tt][Hh][Ll]|[Tt][Dd][Cc][Uu]|[Tt][Kk][Cc][Aa])\ {0,1}1[Zz][Zz]))$" class="mypwn-input input-large font-small required" />
            </span>
        </div>

        <div class="form-field">
            <label for="country">Country</label>
            <span class="mypwn-input-container">
                <input type="text" value="United Kingdom" id="country" name="country" readonly="readonly" class="mypwn-input input-large font-small" />
            </span>
        </div>

        <input type="hidden" value="GBR" id="country_code" name="country_code" />

        <div class="button-left margin-top-20">
            <button class="button-orange" type="submit" id="mypwn-mypins-walletcard-send">
                <span>Send Welcome Pack</span>
            </button>
        </div>
    </form>
</div>
<div class="grid_sub_12" style="z-index: 900; position: relative">
    <div id="welcome_pack_features">
        <div class="aligncenter">
            <img src="/sfimages/walletCard/wallet-card-option.png" width="260" alt="wallet card" />
        </div>

        <div style="margin-top: 10px; margin-right:20px;">
            <i>"Your wallet was very innovative and made me smile"</i><br />

            <div style="text-align: right; font-weight: bold">
                Yvonne, BMW
            </div>
        </div>
        <br />
        <br />

        <ul class="chevron">
            <li><strong>Wallet Card</strong><br />
                <span style="color: #515151">With your personal dial-in numbers and PIN</span>
            </li>
            <li><strong>Stickers</strong><br />
                <span style="color: #515151">For your computer and phone</span>
            </li>
            <li><strong>Free delivery</strong><br />
                <span style="color: #515151">We'll post your card and stickers within 3 working days</span>
            </li>
        
        </ul>

    </div>

</div>