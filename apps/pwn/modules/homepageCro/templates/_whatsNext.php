<div class="what-to-do-now">
	<h2 class="rockwell white">What do you want to do now?</h2>
	<ul id="do-now-list" class="clearfix" style="height: auto">
        <li class="png">
			<span class="icon icon-grey no-shadow png">
				<span class="icon-outlook-white"></span>
			</span>
            <a href="/sfdownload/PowwownowSetup.exe" class="white" target="_blank">
                Schedule through Outlook
            </a>
        </li>
		<li class="png">
			<span class="icon icon-grey no-shadow png">
				<span class="icon-phone-small-white"></span>
			</span>
			<a href="/myPwn/Call-Settings/" class="white">
				Manage my call settings
			</a>
		</li>

		<li class="png">
			<span class="icon icon-grey no-shadow png">
				<span class="icon-play-pause-white"></span>
			</span>
			<a href="/myPwn/In-Conference-Controls/" class="white">
				View in-conference controls 
			</a>
		</li>

        <li class="png">
			<span class="icon icon-grey no-shadow png">
				<span class="icon-calender-white"></span>
			</span>
            <a href="/myPwn/Schedule-A-Call/" class="white">
                Scheduler tool
            </a>
        </li>

		<li class="png">
			<span class="icon icon-grey no-shadow png">
				<span class="icon-arrow-white"></span>
			</span>
			<a href="/myPwn/Web-Conferencing/" class="white">
				Hold a web conference
			</a>
		</li>

		<li class="png">
			<span class="icon icon-grey no-shadow png">
				<span class="icon-globe-white"></span>
			</span>
			<a href="/myPwn/Dial-In-Numbers/" class="white">
				Discover international access
			</a>
		</li>
	</ul>
</div>