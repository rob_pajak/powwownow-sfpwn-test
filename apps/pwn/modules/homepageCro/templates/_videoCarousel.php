<h1 class="rockwell green">Conference calling made easy</h1>
<p class="subheading">Need help with setting up your Powwownow? Check out these handy tips on how to get the most out of your conference calling.</p>
<ul id="videos" class="jcarousel-list jcarousel-list-horizontal jcarousel-skin-pwn">
    <li>
        <div class="polaroid">
            <h3 class="rockwell grey-dark">3 Easy Steps</h3>
            <a href="/shared/flowplayer/flowplayer-3.2.5.swf?height=260&amp;width=460&amp;flashvars=config=%7B%22clip%22:%22/sfvideo/pwn_3steps460x260_LR.flv%22%7D" class="prettyPhoto" title="How to get started with conference calling">
                <img src="/sfimages/video-stills/three-easy-steps.png" width="279" height="123" alt="" onclick="dataLayer.push({'event': 'Video - 3 Easy Steps/onClick'});"/>
            </a>
            <p class="hidden">How to get started with conference calling</p>
        </div>
    </li>

    <li>
        <div class="polaroid">
            <h3 class="rockwell grey-dark">In-conference Controls</h3>
            <a href="/shared/flowplayer/flowplayer-3.2.5.swf?height=260&amp;width=460&amp;flashvars=config=%7B%22clip%22:%22/sfvideo/pwn_inConfCont460x260_LR.flv%22%7D" class="prettyPhoto" title="Available controls you have during your conference call">
                <img src="/sfimages/video-stills/in-conference-controls.png" width="279" height="123" alt="" onclick="dataLayer.push({'event': 'Video - In-Conference Controls/onClick'});"/>
            </a>
            <p class="hidden">Available controls you have during your conference call</p>
        </div>
    </li>

    <li>
        <div class="polaroid">
            <h3 class="rockwell grey-dark">myPowwownow</h3>
            <a href="/shared/flowplayer/flowplayer-3.2.5.swf?height=260&amp;width=460&amp;flashvars=config=%7B%22clip%22:%22/sfvideo/pwn_myPWN460x260_LR.flv%22%7D" class="prettyPhoto" title="Everything about myPowwownow, your personal account area">
                <img src="/sfimages/video-stills/my-powwownow.png" width="279" height="123" alt="" onclick="dataLayer.push({'event': 'Video - My Powwownow/onClick'});"/>
            </a>
            <p class="hidden">Everything about myPowwownow, your personal account area</p>
        </div>
    </li>

    <li>
        <div class="polaroid">
            <h3 class="rockwell grey-dark">Web Conferencing</h3>
            <a href="/shared/flowplayer/flowplayer-3.2.5.swf?height=540&amp;width=960&amp;flashvars=config=%7B%22clip%22:%22/sfvideo/PWN-web-conferencing-music-960x540.flv%22%7D" class="prettyPhoto" title="How to use Powwownow's Web Conferencing service">
                <img src="/sfimages/video-stills/PWN-web-conferencing-music-960x540.png" width="252" height="124" alt="" onclick="dataLayer.push({'event': 'Video - Web Conferencing/onClick'});"/>
            </a>
            <p class="hidden">How to use Powwownow's Web Conferencing service</p>
        </div>
    </li>
</ul>
