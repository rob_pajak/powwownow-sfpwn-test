<h3 class="rockwell">Thanks for confirming those details</h3>
<p>If you go to your inbox now you will find an email from us with your PIN enclosed.</p>
<p>If you entered your mobile number you will also receive a text message from us shortly.</p>
<p>If you need any further assistance, please call us on 0203 398 0398, or complete the <a title="Contact Us" href="<?php echo url_for('@contact_us'); ?>/Contact-Us">contact form</a>.</p>
