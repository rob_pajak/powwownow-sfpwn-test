<?php include_partial('homepageG/topPagePartial');?>


<!-- Start of content -->
<div class="container">
    <div id="feature-panel" class="row">
        <div class="nine columns">
            <div class="feature clearfix">
                <h1>Conference Calling<br />The Easy Way</h1>
                <p class="separator-one feature-blurb">Get up and running in moments with our simple sign-up...</p>
                
                <p class="step-one"><span class="bold big">Step 1.</span> Enter your email address to create your personal PIN</p>
                
                <div id="get-pin-pwn" class="row get-pin-pwn">
                    <div class="eight columns">
                        <?php
                            /**
                             * Include Get My Pin Partial
                             * 
                             */
                            include_component('homepageG','getMyPin', 
                                array('formId'=>'get-my-pin-feature-top',
                                      'inputFieldClasses' => 'large height',
                                      'buttonFieldClasses' => 'right',
                                      'position' => 'top')
                            ); 
                        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="three columns">
            <div class="side">
                    <div class="sprite aside-one">
                        <div class="inner-content">
                            <p>Get talking in seconds</p>
                        </div>
                    </div>
                    <div class="sprite aside-two">
                        <div class="inner-content">
                            <p>Enter your email address to create your PIN</p>
                        </div>
                    </div>
                    <div class="sprite aside-three">
                        <div class="inner-content">
                            <p>Share your PIN and dial in number with your participants</p>
                        </div>
                    </div>
                    <div class="sprite aside-four">
                        <div class="inner-content">
                            <p>Dial <span class="font-enlarge">0844 4 73 73 73</span> enter the PIN and start talking!</p>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<?php include_component('homepageG', 'whyPwn') ?>

<div id="trusted-by" class="container trusted-by">
    <div class="row">
        <div class="twelve columns">
            <h2 class="text-center section-title">Trusted by</h2>
                <div class="inner-content clearfix">
                    <div class="sprite company-bentley"></div>
                    <div class="sprite company-environ"></div>
                    <div class="sprite company-flint"></div>
                    <div class="sprite company-lockup"></div>
                    <div class="sprite company-maytec"></div>
                    <div class="sprite company-threeMC"></div>
                </div>
        </div>
    </div>
</div>

<div id="carousel" class="container">
    <div class="row">
        <div class="twelve columns">
            <?php include_component('homepageG', 'carousel') ?>
        </div>
    </div>
</div>


<div id="account-types" class="container">
    <div class="row">
        <div class="six columns">
            <div id="plus" class="sprite plus">
                <h3 class="title">Powwownow Plus</h3>
                <div class="left">
                    <div class="inner-content">
                        <ul>
                            <li>Option to purchase pay as you go credit to use Worldwide, Freephone and Landline numbers</li>
                            <li>Greater security with unlimited chairperson PINs</li>
                            <li>Ability to add and manage multiple users under one account</li>
                            <li>Personalise your calls with a branded welcome message</li>
                        </ul>
                    </div>
                </div>
                
                <div class="left">
                    <div class="inner-content">
                        <p class="plus-switch headers-bg-style-2 text-center">Register or switch for free</p>
                    </div>
                </div>
                <div class="buttons-wrap">
                    <a class="small yellow button radius right" onclick=" _gaq.push(['_trackEvent', 'link', 'plus', 'G']);" href="<?php echo url_for('plus_service')?>">Powwownow PLUS >></a>
                </div>
                
            </div>
        </div>
        
        
        <div class="six columns">
            <div id="premium" class="sprite premium">
                <div class="left width-overide">
                    <h3 class="title">Powwownow Premium</h3>
                </div>
                <div class="left">
                    <div class="inner-content">
                        <ul>
                            <li>Personalise your calls with a branded welcome message</li>
                            <li>Premium web conferencing</li>
                            <li>Bespoke solutions priced according to your business needs</li>
                            <li>Low-cost and free access numbers for mobiles and landlines, available in more than 70 countries</li>
                        </ul>
                    </div>
                </div>
                
                <div class="left">
                    <div class="inner-content">
                        <ul>
                            <li>Dedicated customer support</li>
                            <li>Call recording</li>
                            <li>Real-time reporting on all the accounts calls</li>
                            <li>Up to 1,000 participants per conference call</li>
                        </ul>
                        <h3 class="small">Call us on 0800 022 9781</h3>
                    </div>
                </div>
                <div class="buttons-wrap">
                    <a class="small yellow button radius right" href="<?php echo url_for('@premium_service'); ?>">Powwownow PREMIUM >></a>
                </div>
            </div>     
        </div>
    </div>
</div>

<div id="get-more-pwn" class="get-more-pwn container">
    <div class="row">
        <h2 class="text-center section-title">Get more from your Powwownow</h2>
        <div class="six columns">
            <div class="inner-content clearfix">
                <div class="left sprite video"></div>
                <div class="left img-content">
                    <h4>Powwownow Engage</h4>
                    <p>Powwownow Engage is a smarter way for people to communicate and collaborate, combining the 
                       highest quality HD video, conferencing, phone and VoIP calls, presence and instant messaging 
                       through one intuitive interface.
                    </p>
                    <a class="more" href="<?php echo url_for('@engage_service'); ?>">Read more >></a>
                </div>
            </div>
        </div>
        <div class="six columns">
            <div class="inner-content clearfix">
                <div class="left sprite web"></div>
                <div class="left img-content">
                    <h4>Web Conferencing</h4>
                    <p>With Powwownow Web you get a web conferencing application that allows you to chat, 
                       present documents and share your desktop with your fellow conference callers, all for free.
                    </p>
                    <a class="more" href="<?php echo url_for('@web_conferencing'); ?>">Read more >></a>
                </div>
            </div> 
        </div>
    </div>
</div>
<!-- End of content -->
<?php include_partial('homepageG/footerPartial');?>
<?php //include_partial('homepageG/FooterPartial',array('sf_data'=>$sf_data));?>



