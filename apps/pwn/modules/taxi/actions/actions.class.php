<?php
/**
 * Taxi Registration Landing Page and Ajax
 *
 * @package    powwownow
 * @subpackage taxi
 * @author     Davinder C
 * @author     Asfer Tamimi
 *
 */
class taxiActions extends sfActions {

    /**
     * Taxi Registration [PAGE]
     * @param sfWebRequest $request
     * @return sfView::None
     * @author Davinder C
     */
    public function executeIndex(sfWebRequest $request) {

    }

    /**
     * Taxi Registration [AJAX]
     * @param sfWebRequest $request
     * @return sfView::None
     * @author Asfer Tamimi
     */
    public function executeTaxiAjax(sfWebRequest $request) {
        $this->getResponse()->setHttpHeader('Content-type','application/json');
        $this->setLayout(false);

        // Perform Registration and Return Error or Success
        if (sfView::NONE == $registrationDetails = Common::performRegistration($request->getPostParameter('email'))) {
            $this->getResponse()->setStatusCode(400);
            return sfView::NONE;
        }

        // Set PIN in Session, since that will be shown on the next Taxi Page
        $this->getUser()->setAttribute('pin',$registrationDetails['pin']);

        echo json_encode(array('status' => 'success'));
        return sfView::NONE;
    }

    /**
     * Taxi Page Second Page
     * @param sfWebRequest $request
     * @return sfView::None
     * @author Davinder C
     * @author Asfer Tamimi
     */
    public function executeYourPin(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));
        $this->pin = $this->getUser()->getAttribute('pin');
        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $this->getUser()->getCulture(),
                'service_ref'    => $this->getUser()->getAttribute('service_ref'),
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
        $this->number = $dialInNumbers['local'];
        $this->mobile = $dialInNumbers['mobile'];
    }

}