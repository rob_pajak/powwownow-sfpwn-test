<?php

class taxiComponents extends sfComponents {

    public function executeHowItWorks(sfWebRequest $request){}

    public function executeGetMyPin(sfWebRequest $request){
        $this->getResponse()->addJavascript('/homepageG/js/BaseView.js');
        $this->getResponse()->addJavascript('/taxi/js/getMyPin.js');
    }

    public function executeModalEmailTaken(sfWebRequest $request) {}

}