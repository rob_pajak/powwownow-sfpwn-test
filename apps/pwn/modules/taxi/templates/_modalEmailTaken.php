<div id="modal-email-taken" class="medium reveal-modal">
    <h3 class="sub-header font-blue">Hi</h3>
    <p class="color grey">It seems you already have an account with us.
        If you have forgotten your PIN please use the <a href="<?php echo url_for('homepage_g_pin_reminder')?>" class="more">PIN reminder form</a>.
    </p>
    <a class="close-reveal-modal">&#215;</a>
</div>

