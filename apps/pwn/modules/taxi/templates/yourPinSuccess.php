<?php include_partial('taxi/topPartial'); ?>
<!-- Content -->
<div class="row">
    <div class="small-12 columns">

        <!-- Hero Element-->
        <div class="row">
            <div class="small-12 columns">
                <section id="hero" class="hero">
                    <div class="canvas">
                        <div class="layer-one">It's your call.</div>
                        <div class="layer-two">And you're about to make the right one.</div>
                    </div>
                </section>
            </div>
        </div>
        <!-- Hero Element End-->

        <!-- Your Pin -->
        <div class="row">
            <div class="small-12 columns">
                <section class="your-pin panel green rounded-corners font-white">
                   <p class="text-center">
                    Your dial-in number:<br />
                   <strong><?php echo $mobile; ?></strong>*<br/>
                   <strong><?php echo $number; ?></strong><br/>
                    Your PIN is:<br />
                    <strong><?php echo $pin; ?></strong></p>
                   <p class="small-text text-left">* Dedicated shortcode that gives you cheaper mobile call rates, charged at 12.5ppm + VAT from any UK mobile provider</p>
                </section>
            </div>
        </div>
        <!-- Your Pin End -->

        <!-- Whats Next -->
        <div class="row">
            <div class="small-12 columns">
                <section class="panel border-green rounded-corners">
                    <ul class="whats-next list-items">
                        <li class="list-item">You can now start talking, your PIN is ready to use.</li>
                        <li class="list-item">Don’t forget you can access your account anytime; we have emailed your password to you.</li>
                        <li class="list-item">Visit our full site to see all the great features and benefits you can get with Powwownow conferencing.</li>
                    </ul>
                </section>
            </div>
        </div>
        <!-- Whats Next End-->

        <?php include_partial('taxi/footerPartial'); ?>
    </div>
</div>
<!-- Content End -->