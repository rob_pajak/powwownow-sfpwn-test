<?php include_partial('taxi/topPartial'); ?>
<!-- Content -->
<div class="row">
    <div class="small-12 columns">
        <!-- Hero Element-->
        <div class="row">
            <div class="small-12 columns">
                <section id="hero" class="hero">
                    <div class="canvas">
                        <div class="layer-one">It's your call.</div>
                        <div class="layer-two">And you're about to make the right one.</div>
                    </div>
                </section>
            </div>
        </div>
        <!-- Hero Element End-->

        <!-- Get My Pin Form-->
        <div class="row">
            <div class="small-12 columns">
                <?php include_component('taxi','getMyPin', array('formId'=> 'get-my-pin-mobile'));?>
            </div>
        </div>
        <!-- Get My Pin Form End-->

        <!-- How It Works-->
        <div class="row">
            <div class="small-12 columns">
                <?php include_component('taxi','howItWorks');?>
            </div>
        </div>
        <!-- How It Works End -->

        <!-- What It Costs-->
        <div class="row">
            <div class="small-12 columns">
                <section id="what-it-costs" class="what-it-costs panel border-green rounded-corners">
                    <h3 class="sub-header font-blue">What It Costs</h3>
                    <ul class="what-it-costs list-items">
                        <li class="list-item">You just pay for the cost of a local 0844 call. That's it - there are no other fees.</li>
                        <li class="list-item">There's no contract or minimum usage, you can start and stop as you like.</li>
                        <li class="list-item">There's no monthly billing. The local charges show up on your phone bill.</li>
                    </ul>
                </section>
            </div>
        </div>
        <!-- What It Costs End-->

        <?php include_partial('taxi/footerPartial'); ?>
    </div>
</div>
<?php include_component('taxi','modalEmailTaken');?>
<!-- Content End -->