<?php use_helper('DataUri') ?>
<!-- Logo start -->
<div class="row">
    <div class="small-12 columns">
        <a href="<?php echo url_for('@homepage'); ?>" class="logolink">
            <img title="Powwownow Free Conference Call Provider" src="<?php echo getDataURI('/sfimages/powwownow_logo.png');?>" alt="Powwownow Free Conference Call Provider" class="logo png"/>
        </a>
    </div>
</div>
<!-- Logo End -->