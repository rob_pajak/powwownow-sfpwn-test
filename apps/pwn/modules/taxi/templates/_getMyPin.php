<?php
    // Lets set some defaults to stop any warning errors.
    $formId = isset($formId) ? $formId : rand();
?>
<section  class="get-my-pin panel green rounded-corners">
    <div id="<?php echo $formId; ?>-container" class="<?php echo $formId; ?>-container clearfix">
        <script type="text/html" id="<?php echo $formId; ?>-template">
            <?php echo form_tag(url_for('taxi_ajax'), array('id' => $formId, 'class' => $formId)); ?>

            <div class="form-field fixed-height-65">
                <input id="<?php echo $formId ?>-email" class="email-address radius input-text" type="text" name="email" placeholder="Enter email address" autocomplete="off" data-trigger="change" data-type="email" data-required="true" data-error-message="Please enter a valid email address." />
            </div>

            <div class="form-action">
                <input id="<?php echo $formId ?>-button" type="submit" class="yellow button radius get-my-pin right" value="Get My Pin" />
            </div>

            <div class="form-field">
                <div class="throbber loading green hide"></div>
            </div>
            </form>
            <p class="font-white terms-conditions"> By clicking Get My PIN, you agree to our <a href="<?php echo url_for('@terms_and_conditions'); ?>" class="link">Terms &amp; Conditions</a></p>
        </script>
    </div>
</section>