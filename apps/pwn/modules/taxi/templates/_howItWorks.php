<section id="how-it-works" class="how-it-works panel border-green rounded-corners">
    <h3 class="sub-header">How It Works</h3>
    <ul class="how-it-works-list">
        <li class="first list-item">Enter your email to generate your PIN</li>
        <li class="second list-item">Share your PIN and dial-in number with your call participants</li>
        <li class="third list-item">At the agreed time, all dial in, enter the PIN and start talking!</li>
    </ul>
</section>