<!-- Footer -->
<div class="row">
    <div class="small-12 columns">
        <footer>
            <div class="panel blue rounded-corners">
                <ul class="inline-list footer-items">
                    <li class="footer-item"><a href="<?php echo url_for('@privacy'); ?>" class="link font-white">Privacy</a></li>
                    <li class="footer-item"><a href="<?php echo url_for('@terms_and_conditions'); ?>" class="link font-white">T&amp;Cs</a></li>
                    <li class="footer-item"><a href="<?php echo url_for('@about_us'); ?>" class="link font-white">About Us</a></li>
                    <li class="footer-item"><a href="<?php echo url_for('@contact_us'); ?>" class="link font-white">Contact Us</a></li>
                </ul>
            </div>
            <p class="text-center link"><a href="/">Go To Full Site</a></p>
        </footer>
    </div>
</div>
<!-- Footer End-->