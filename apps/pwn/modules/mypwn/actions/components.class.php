<?php
/* mypwn Related Components
 * 
 * @author Asfer Tamimi
 *
 */
class mypwnComponents extends sfComponents {

    /**
     * This Component is for creating a Data Table.
     *
     * @param  array  $dataTable        - This will contain the columns and their mapping
     * @param  array  $dataTableOptions - This will contain the data, and any config options
     * @return string 
     * @author Asfer Tamimi
     *
     */
    public function executeCreateDataTable() {
        // Check dataTable Options, and Set Defaults if required
        $this->formID        = (isset($this->dataTableOptions['formID']))        ? $this->dataTableOptions['formID'] : 'form';
        $this->tableID       = (isset($this->dataTableOptions['tableID']))       ? $this->dataTableOptions['tableID'] : 'tbl';
        $this->sortCol       = (isset($this->dataTableOptions['sortCol']))       ? $this->dataTableOptions['sortCol'] : 0;
        $this->sortOrder     = (isset($this->dataTableOptions['sortOrder']))     ? $this->dataTableOptions['sortOrder'] : 'asc';
        $this->onHover       = (isset($this->dataTableOptions['onHover']))       ? $this->dataTableOptions['onHover'] : true;
        $this->floatLeft     = (isset($this->dataTableOptions['floatLeft']))     ? $this->dataTableOptions['floatLeft'] : true;
        $this->bottomPadding = (isset($this->dataTableOptions['bottomPadding'])) ? $this->dataTableOptions['bottomPadding'] : true;
        $this->autoWidth     = (isset($this->dataTableOptions['autoWidth']))     ? $this->dataTableOptions['autoWidth'] : true;
        $this->tableEmpty    = (isset($this->dataTableOptions['tableEmpty']))    ? $this->dataTableOptions['tableEmpty'] : 'No Records were found';
        $this->dataTable     = (count($this->dataTable) == 0)                    ? $this->getDefaultdataTable() : $this->dataTable;
        $this->footer        = (isset($this->dataTableOptions['footer']))        ? $this->dataTableOptions['footer'] : array();
        $this->csvMode       = (isset($this->dataTableOptions['csv-mode']))      ? $this->dataTableOptions['csv-mode'] : false;
        $this->displayLength = (isset($this->dataTableOptions['displayLength'])) ? $this->dataTableOptions['displayLength'] : 25;

        // Set Main Output
        $this->data = array();
        $this->columns = array();

        // Columns / Data / Count Error
        if (!isset($this->dataTable) || !is_array($this->dataTable) || empty($this->dataTable) || empty($this->dataTableOptions['data'])) {
            $this->logMessage('There was no Data in the Data Table. Empty Message will be shown','info');
        }

        // Additional Includes
        if (!isset($this->dataTableOptions['includes'])) {
            $this->includes = array(
                '/shared/jQuery/jquery.dataTables.min.js',
                '/sfjs/language/en.js',
                '/sfcss/vendor/jQuery/datatable.css',
                '/shared/jQuery/jquery.dataTables.fnLengthChange.js'
            );
        } else {
            $this->includes = $this->dataTableOptions['includes'];
        }

        // Go through dataTable Array to Set the Columns
        foreach ($this->dataTable as $fieldArr) {
            $tmpCol = array('sTitle' => $fieldArr['title']);
            if (isset($fieldArr['class'])) $tmpCol['sClass'] = $fieldArr['class'];
            if (isset($fieldArr['width'])) $tmpCol['sWidth'] = $fieldArr['width'];
            $this->columns[] = $tmpCol;
        }

        // Go through the Source Data to set the DataTable data
        foreach ($this->dataTableOptions['data'] as $sourceArr) {
            $tmpData = array();
            foreach ($this->dataTable as $fieldArr) {
                $tmpData[] = (isset($sourceArr[$fieldArr['map']])) ? $sourceArr[$fieldArr['map']] : '';
            }
            $this->data[] = $tmpData;
        }

        // Check if the Data Footer Exists
        if (is_array($this->footer) && !empty($this->footer) && !empty($this->data)) {
            $this->data[] = $this->footer;
        }

        // CSV Mode
        if (false !== $this->csvMode) {
            $this->csv = $this->getDataTableCSV($this->columns,$this->data);
        }

        // Table Colour
        $this->tblColour = (isset($this->dataTableOptions['tblColour'])) ? $this->dataTableOptions['tblColour'] : $this->getUser()->getAttribute('service','');

    }

    /**
     * This Method returns the Default Datatable Array
     *
     * @return array $dataTable
     * @author Asfer Tamimi
     *
     */
    protected function getDefaultdataTable() {
        if ($this->getUser()->hasCredential('admin')) {
            $dataTable = array(
                array('title' => '', 'map' => 'checkboxcol', 'class' => 'checkboxcol'),
                array('title' => 'Name', 'map' => 'chair_full_name'),
                array('title' => 'Email Address', 'map' => 'chair_email', 'class' => 'longcol'),
                array('title' => 'Chairperson PIN', 'map' => 'chair_pin'),
                array('title' => 'Participant PIN', 'map' => 'participant_pin'),
            );
        } else {
            $dataTable = array(
                array('title' => '', 'map' => 'checkboxcol', 'class' => 'checkboxcol'),
                array('title' => 'Chair PIN', 'map' => 'chair_pin'),
                array('title' => 'Participant PIN', 'map' => 'participant_pin'),
            );
        }

        return $dataTable;
    }

    /**
     * This Method returns a CSV Output for the Data Table
     *
     * @param  array  $titles
     * @param  array  $data
     * @return string $output;
     * @author Asfer Tamimi
     */
    protected function getDataTableCSV($titles,$data) {
        $columns    = array();
        $output     = '';
        $outputData = '';
        $delimiter  = ',';

        // Titles
        foreach ($titles as $k => $v) {
            $output.= '"' . $v['sTitle'] . '"' . $delimiter;
        }
        $output = rtrim($output,',') . "\n";

        // Data
        foreach ($data as $k => $v) {
            $outputData = '"' . implode('"' . $delimiter . '"',$v) . '"';
            $output.= $outputData . "\n";
        }
        $output = rtrim($output,',');

        return $output;
    }

    /**
     * This Component is for the myPwn Dashboard, currently used on the mypwn/index.
     *
     * @param  array $grid - The Grid Information.
     * @return string
     * @author Asfer Tamimi
     * @amend Dav C
     */
    public function executeCreateGrid() {
        $user = $this->getUser();

        // Load the URL Helper. Will be used when the other sub pages are done.
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url','Partial'));

        // PIN Information Relating to current user
        $pins = PinHelper::getContactPinPairs($this->getUser()->getAttribute('contact_ref'),'USER');

        // Scheduler Link (Not used on Plus Admin Dashboard Only!)
        if (!$this->getUser()->hasCredential('PLUS_ADMIN')) {

            sfContext::getInstance()->getConfiguration()->loadHelpers( array('getSchedulerAutoLoginLink') );

            $schedulerLink = getSchedulerAutoLoginLink(
                $this->getUser()->getAttribute('contact_ref'),
                $this->getUser()->getAttribute('email')
            );
        }

        // Share Details (Currently Only used for Plus User)
        if ($this->getUser()->hasCredential('PLUS_USER')) {
            foreach ($pins as $id => $pin) {
                $this->pin = $pin['participant_pin'];
                break;
            }
        }

        // Service Changes
        $this->service = $this->getUser()->getAttribute('service');

        // Get Grid Information
        $this->gridInformation = array();
        if ($this->getUser()->hasCredential('POWWOWNOW')) {
            $this->gridInformation = array(
                array(
                    'href'          => url_for('@web_conferencing_auth'),
                    'image'         => 'powwownow_share_my_desktop',
                    'text'          => 'Free Web Conferencing',
                    'hover_text'    => 'Download our FREE screen sharing tool and bring your conference calls to life.',
                    'overlay_text'  => 'FREE',
                    'overlay_class' => 'free_blue'
                ),
                array(
                    'href'  => url_for('@request_welcome_pack'),
                    'image' => 'powwownow_request_wallet_card',
                    'text'  => 'Request a Welcome Pack'
                ),
                array(
                    'href'  => url_for('@dial_in_numbers'),
                    'image' => 'powwownow_dial_in_numbers',
                    'text'  => 'Dial-in Numbers and Rates'
                ),
                array(
                    'href'  => url_for('@schedule_a_call'),
                    'image' => 'powwownow_letter_clock',
                    'text'  => 'Download Plugin for Outlook'
                ),
                array(
                    'href'  => url_for('@call_settings'),
                    'image' => 'powwownow_manage_call_settings',
                    'text'  => 'Manage Call Settings'
                ),
                array(
                    'href'  => url_for('@in_conference_controls'),
                    'image' => 'powwownow_in_conference_controls',
                    'text'  => 'In-Conference Controls'
                ),
                array(
                    'href'        => 'http://scheduler.powwownow.com' . $schedulerLink,
                    'href_target' => '_blank',
                    'image'       => 'powwownow_scheduler',
                    'text'        => 'Use Scheduler Tool'
                ),
                array(
                    'href'  => url_for('@schedule_a_call'),
                    'image' => 'powwownow_envelope',
                    'text'  => 'Share PIN by Email'
                ),
                array(
                    'href'        => url_for('@how_conference_calling_works'),
                    'href_target' => '_blank',
                    'image'       => 'powwownow_how_it_works',
                    'text'        => 'How it Works'
                )
            );
        } elseif ($this->getUser()->hasCredential('PLUS_USER')) {
            $this->gridInformation = array(
                array(
                    'href'        => get_component('mypwn','shareDetailsEmail',array(
                        'subject' => 'Powwownow conference call details',
                        'pin'     => $this->pin
                    )),
                    'image'       => 'plus_envelope',
                    'text'        => 'Share PIN by Email',
                    'title'       => 'Instantly share your PIN and dial-in number with your call Participants.',
                    'hover_title' => 'Instantly share your PIN and dial-in number with your call Participants.',
                    'hover_text'  => 'Instantly share your PIN and dial-in number with your call Participants.',
                    'hover_href'  => get_component('mypwn','shareDetailsEmail',array(
                        'subject' => 'Powwownow conference call details',
                        'pin'     => $this->pin
                    )),
                ),
                array(
                    'href'       => url_for('@web_conferencing_auth'),
                    'image'      => 'plus_share_my_desktop',
                    'text'       => '<span>FREE</span> Web Conferencing',
                    'hover_text' => 'Bring your conference calls to life by sharing your screen with your call participants.',
                    'hover_href'    => url_for('@web_conferencing_auth'),
                    'hover_text'    => 'Download our FREE screen sharing tool and bring your conference calls to life.',
                    'overlay_class' => 'plus_web_conferencing'
                ),
                array(
                    'href'       => url_for('@dial_in_numbers'),
                    'image'      => 'plus_dial_in_numbers',
                    'text'       => 'Dial-in Numbers and Rates',
                    'hover_text' => 'View the dial-in numbers and rates that are available to you.',
                ),
                array(
                    'href'        => url_for('@schedule_a_call'),
                    'image'       => 'plus_letter_clock',
                    'text'        => 'Download Plugin for Outlook',
                    'hover_href'  => url_for('@schedule_a_call'),
                    'hover_text'  => 'Use the Plugin for Outlook to schedule your conference calls using your Outlook calendar and contacts.'
                ),
                array(
                    'href'       => url_for('@call_settings'),
                    'image'      => 'plus_manage_call_settings',
                    'text'       => 'Manage Call Settings',
                    'hover_text' => 'Change your call settings, such as your on-hold music.'
                ),
                array(
                    'href'       => url_for('@in_conference_controls'),
                    'image'      => 'plus_in_conference_controls',
                    'text'       => 'In-Conference Controls',
                    'hover_text' => 'View the conference controls that are available during a conference.'
                ),
                array(
                    'href'        => 'http://scheduler.powwownow.com' . $schedulerLink,
                    'href_target' => '_blank',
                    'image'       => 'plus_scheduler',
                    'text'        => 'Use Scheduler Tool',
                    'hover_href'  => 'http://scheduler.powwownow.com' . $schedulerLink,
                    'hover_text'  => 'Don\'t use Outlook? Try our scheduler tool to organize your conference calls and invite participants.'
                ),
                array(
                    'href'       => url_for('@request_welcome_pack'),
                    'image'      => 'plus_request_wallet_card',
                    'text'       => 'Request a Welcome Pack',
                    'hover_text' => 'Request a Welcome Pack containing a handy wallet card and stickers with your dial-in numbers and PINs.',
                ),
                array(
                    'href'        => url_for('@how_conference_calling_works'),
                    'href_target' => '_blank',
                    'image'       => 'plus_how_it_works',
                    'text'        => 'How it Works',
                    'hover_text'  => 'Here you can find out further information on how conference calling works.'
                )
            );
        } elseif ($this->getUser()->hasCredential('PLUS_ADMIN')) {

            $gridItemBundles = array(
                'href'       => url_for('@products_select'),
                'image'      => 'plus_minute_bundles_new',
                'text'       => 'Minutes Bundles',
                'hover_text' => 'View and purchase Money Saving Minute Bundles. <br><br> Talk More Pay Less!',
                'overlay_text' => 'NEW',
                'overlay_class' => 'new'
            );

            $gridItemWelcomePack= array(
                'href'       => url_for('@request_welcome_pack'),
                'image'      => 'plus_request_wallet_card',
                'text'       => 'Request a Welcome Pack',
                'hover_text' => 'Request a Welcome Pack containing a handy wallet card and stickers with your dial-in numbers and PINs.'
            );

            $gridItemDialinNumber = array(
                'href'       => url_for('@dial_in_numbers'),
                'image'      => 'plus_dial_in_numbers',
                'text'       => 'Dial-in Numbers and Rates',
                'hover_text' => 'View Freephone, Landline and Shared cost numbers and rates. Remember to activate these for your users and purchase credit before using them.',
            );

            $gridItemBWM =  array(
                'href'       => url_for('@products_select'),
                'image'      => 'plus_in_branded_welcome_message',
                'text'       => 'Branded Welcome Messages',
                'hover_text' => 'Help your company stand out from the crowd by purchasing a Branded Welcome Message for any dedicated number.',
                'overlay_class' => 'new'
            );

            $gridItemSelectPrds =  array(
                'href'       => url_for('@products_select'),
                'image'      => 'plus_purchase_products',
                'text'       => 'Select Products',
                'hover_text' => 'Purchase call credit for Freephone and Landline numbers; or buy branded welcome messages with dedicated dial-in numbers to customise your conference calls.'
            );

            $gridItemMyPrds = array(
                'href'       => url_for('@my_products'),
                'image'      => 'plus_my_products',
                'text'       => 'My Products',
                'hover_text' => 'View the features you have assigned to your account and the products you have purchased.',
                'hover_href' => url_for('@my_products'),
            );

            $gridItemWebConference =   array(
                'href'          => url_for('@web_conferencing_auth'),
                'image'         => 'plus_share_my_desktop',
                'text'          => '<span>FREE</span> Web Conferencing',
                'hover_href'    => url_for('@web_conferencing_auth'),
                'hover_text'    => 'Download our FREE screen sharing tool and bring your conference calls to life.',
                'overlay_class' => 'plus_web_conferencing'
            );
            $gridItemScheduleCall =   array(
                'href'       => url_for('@schedule_a_call'),
                'image'      => 'plus_scheduler',
                'text'       => 'Schedule a Call',
                'hover_href' => url_for('@schedule_a_call'),
                'hover_text' => 'You can either share your details by email, download the Plugin for Outlook or use our scheduler tool.'
            );

            $gridItemBundlesAllowance = array(
                'href'       => '', // $this->generateUrl('bundle_remaining_allowance'),
                'image'      => 'plus_minute_bundles', // to be changed when an icon has been given.
                'text'       => 'Bundle Allowance',
                'hover_text' => 'View your remaining Bundle Allowance.'
            );

            $gridItemPurchaseCC =  array(
                'href'       => url_for('@products_select'),
                'image'      => 'plus_pound',
                'text'       => 'Purchase Call Credit',
                'hover_text' => 'Purchase credit or top-up your account to use your Pay As You Go numbers.'
            );

            $gridItemCreateUser = array(
                'href'       => url_for('@mypwn_users'),
                'image'      => 'plus_create_users',
                'text'       => 'Create Users',
                'hover_text' => 'Create Users'
            );

            $gridItemManageCallSetting = array(
                'href'       => url_for('@call_settings'),
                'image'      => 'plus_manage_call_settings',
                'text'       => 'Manage Call Settings',
                'hover_text' => 'Manage Call Settings'
            );

            // default
            $this->gridInformation = array(
                $gridItemBundles,
                $gridItemWebConference,
                $gridItemWelcomePack,
                $gridItemBWM,
                $gridItemSelectPrds,
                $gridItemMyPrds,
                $gridItemPurchaseCC,
                $gridItemDialinNumber,
                $gridItemScheduleCall
            );

            if ($user->isPostPayCustomer()) {
                $this->gridInformation = array(
                    $gridItemBWM,
                    $gridItemWebConference,
                    $gridItemWelcomePack,
                    $gridItemCreateUser,
                    $gridItemSelectPrds,
                    $gridItemMyPrds,
                    $gridItemManageCallSetting,
                    $gridItemDialinNumber,
                    $gridItemScheduleCall
                );
            }

        } elseif ($this->getUser()->hasCredential('PREMIUM_USER')) {
            $this->gridInformation = array(
                array(
                    'href'  => url_for('@schedule_a_call'),
                    'image' => 'premium_envelope',
                    'text'  => 'Share PIN by Email',
                ),
                array(
                    'href'  => url_for('@request_welcome_pack'),
                    'image' => 'premium_request_wallet_card',
                    'text'  => 'Request a Welcome Pack',
                ),
                array(
                    'href'  => url_for('@dial_in_numbers'),
                    'image' => 'premium_dial_in_numbers',
                    'text'  => 'Dial-in Numbers and Rates',
                ),
                array(
                    'href'  => url_for('@schedule_a_call'),
                    'image' => 'premium_letter_clock',
                    'text'  => 'Download Plugin for Outlook',
                ),
                array(
                    'href'  => url_for('@call_settings'),
                    'image' => 'premium_manage_call_settings',
                    'text'  => 'Manage Call Settings',
                ),
                array(
                    'href'  => url_for('@in_conference_controls'),
                    'image' => 'premium_in_conference_controls',
                    'text'  => 'In-Conference Controls',
                ),
                array(
                    'href'        => 'http://scheduler.powwownow.com' . $schedulerLink,
                    'href_target' => '_blank',
                    'image'       => 'premium_scheduler',
                    'text'        => 'Use Scheduler Tool',
                ),
                array(
                    'href'       => url_for('@web_conferencing_auth'),
                    'image'      => 'premium_share_my_desktop',
                    'text'       => 'Free Web Conferencing',
                    'hover_text' => 'Bring your conference calls to life by sharing your screen with your call participants.'
                )
            );
    
            if ($this->getUser()->hasAttribute('time_limited_pins') && (true === $this->getUser()->getAttribute('time_limited_pins') || 'YES' == $this->getUser()->getAttribute('time_limited_pins'))) {
                $this->gridInformation[] = array(
                    'href'        => '/mypwn/mytimelimitedpins/',
                    'href_target' => '_blank',
                    'image'       => 'premium_time_limited_pins',
                    'text'        => 'Time-Limited PINs',
                );
            } else {
                $this->gridInformation[] = array(
                    'href'        => url_for('@how_conference_calling_works'),
                    'href_target' => '_blank',
                    'image'       => 'premium_how_it_works',
                    'text'        => 'How it Works',
                );
            }
        } elseif ($this->getUser()->hasCredential('PREMIUM_ADMIN')) {
            $this->gridInformation = array(
                array(
                    'href'  => url_for('@pins'),
                    'image' => 'premium_manage_sets_of_pins',
                    'text'  => 'Manage PINs',
                ),
                array(
                    'href'  => url_for('@dial_in_numbers'),
                    'image' => 'premium_dial_in_numbers',
                    'text'  => 'Dial-in Numbers and Rates',
                ),
                array(
                    'href'  => url_for('@in_conference_controls'),
                    'image' => 'premium_in_conference_controls',
                    'text'  => 'In-Conference Controls',
                ),
                array(
                    'href'  => url_for('@schedule_a_call'),
                    'image' => 'premium_letter_clock',
                    'text'  => 'Download Plugin for Outlook',
                ),
                array(
                    'href'  => url_for('@recordings'),
                    'image' => 'premium_access_recordings',
                    'text'  => 'Access Recordings',
                ),
                array(
                    'href'  => url_for('@call_history'),
                    'image' => 'premium_call_history',
                    'text'  => 'Call History',
                ),
                array(
                    'href'        => 'http://scheduler.powwownow.com' . $schedulerLink,
                    'href_target' => '_blank',
                    'image'       => 'premium_scheduler',
                    'text'        => 'Use Scheduler Tool',
                ),
                array(
                    'href'       => url_for('@web_conferencing_auth'),
                    'image'      => 'premium_share_my_desktop',
                    'text'       => 'Free Web Conferencing',
                    'hover_text' => 'Bring your conference calls to life by sharing your screen with your call participants.'
                ),
            );

            if ($this->getUser()->hasAttribute('time_limited_pins') && (true === $this->getUser()->getAttribute('time_limited_pins') || 'YES' == $this->getUser()->getAttribute('time_limited_pins'))) {
                $this->gridInformation[] = array(
                    'href'        => '/mypwn/mytimelimitedpins/',
                    'href_target' => '_blank',
                    'image'       => 'premium_time_limited_pins',
                    'text'        => 'Time-Limited PINs',
                );
            } else {
                $this->gridInformation[] = array(
                    'href'        => url_for('@how_conference_calling_works'),
                    'href_target' => '_blank',
                    'image'       => 'premium_how_it_works',
                    'text'        => 'How it Works',
                );
            }
        }
    }

    /**
     * This Component is for creating the contents for Share Details Email
     *
     * @param  string  $subject        - Email Subject (Required)
     * @param  bool    $newUser        - Is the Current User New
     * @param  integer $pin            - PIN
     * @param  integer $part_pin       - Participant PIN
     * @param  integer $chair_pin      - Chairperson PIN
     * @param  string  $dial_in_number - Dial In Number
     * @return string
     * @author Asfer Tamimi
     *
     */
    public function executeShareDetailsEmail() {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url','Partial'));

        $this->subject        = (isset($this->subject))        ? $this->subject : '';
        $this->newUser        = (isset($this->newUser))        ? $this->newUser : false;
        $this->pin            = (isset($this->pin))            ? $this->pin : '';
        $this->part_pin       = (isset($this->part_pin))       ? $this->part_pin : '';
        $this->chair_pin      = (isset($this->chair_pin))      ? $this->chair_pin : '';
        $this->dial_in_number = (isset($this->dial_in_number)) ? $this->dial_in_number : '';

        // Set the Email Body Template
        switch ($this->getUser()->getAttribute('service_user')) {
            case 'POWWOWNOW':
                $this->bodyTemplate = 'shareDetailsPowwownow';
                break;
            case 'PLUS_USER':
                if ($this->newUser) {
                    $this->bodyTemplate = 'shareDetailsPlusUserNew';
                } else {
                    $this->bodyTemplate = 'shareDetailsPlusUser';
                }
                break;
            case 'PLUS_ADMIN':
                //$this->bodyTemplate = '_shareDetailsPlusAdmin';
                $this->bodyTemplate = 'shareDetailsPlusUser';
                break;
            case 'PREMIUM_USER':
                $this->bodyTemplate = 'shareDetailsPremiumUser';
                break;
            case 'PREMIUM_ADMIN':
                $this->bodyTemplate = 'shareDetailsPremiumUser';
                // $this->bodyTemplate = '_shareDetailsPremiumAdmin';
                break;
            default:
                $this->bodyTemplate = null;
                break;
        }

        // Obtain the Body
        if (!is_null($this->bodyTemplate)) {
            $this->body = get_partial('mypwn/' . $this->bodyTemplate, array(
                'pin'            => $this->pin,
                'part_pin'       => $this->part_pin,
                'chair_pin'      => $this->chair_pin,
                'dial_in_number' => $this->dial_in_number
            ));
        } else {
            $this->body = '';
        }

        // Filter the Email Subject and Body

        // Decode encoded HTML to make it easier to replace and remove it
        $this->subject = html_entity_decode($this->subject);
        $this->body    = html_entity_decode($this->body);

        // Replace HTML spaces and line breaks with plain text, and then remove any other
        // HTML tags; as we are converting to plain text they are no use to us.
        $this->subject = strip_tags(str_ireplace('<br />' , PHP_EOL, $this->subject));
        $this->subject = strip_tags(str_ireplace('&nbsp;' , ' ', $this->subject));

        $this->body    = strip_tags(str_ireplace('<br />' , PHP_EOL, $this->body));
        $this->body    = strip_tags(str_ireplace('&nbsp;' , ' ', $this->body));

        // // Trim white space and then convert to url encoding
        $this->subject = rawurlencode(trim($this->subject));
        $this->body    = rawurlencode(trim($this->body));
    }

    /**
     * Added for typehinting: we are using a custom class (as generated by symfony 1.4).
     *
     * @return myUser
     */
    public function getUser()
    {
        return parent::getUser();
    }
}
