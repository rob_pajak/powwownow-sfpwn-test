<?php

require_once dirname(__FILE__) . '/../../login/lib/AJAXResponse.php';

/**
 * MyPowwownow
 *
 * @package    powwownow
 * @subpackage pages
 * @author     Asfer Tamimi + Robert Pajak + Maartin Jacobs
 *
 */
class mypwnActions extends sfActions {

    /**
     * Web-Conferencing [PAGE]
     *
     * @author Asfer Tamimi
     *
     */
    public function executeWebConferencing()
    {
        /** @var myUser $user */
        $user = $this->getUser();
        $first_name = $user->getAttribute('first_name', null);
        $last_name = $user->getAttribute('last_name', null);
        $this->setVar('form', new webConferencingForm());
        if (empty($first_name) || empty($last_name)) {
            $this->setVar('fields', array('first_name', 'last_name', 'password'));
        } else {
            $this->setVar('fields', array('password'));
        }
        $this->setVar('first_name', $first_name);
        $this->setVar('last_name', $last_name);
        $this->setVar('service', $user->getAttribute('service'));
    }

    /**
     * Web-Conferencing [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeWebConferencingAjax(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new webConferencingForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments = Common::getArguments(
            $request->getParameter($form->getName()),
            array(
                'contact_ref' => $user->getAttribute('contact_ref'),
                'service_ref' => $user->getAttribute('service_ref'),
                'email'       => $user->getAttribute('email'),
            )
        );
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $response->setStatusCode(500);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        $result = YuuguuHelper::registerYuuguu(
            $arguments['contact_ref'],
            (isset($arguments['first_name'])) ? $arguments['first_name'] : $user->getAttribute('first_name'),
            (isset($arguments['last_name'])) ? $arguments['last_name'] : $user->getAttribute('last_name'),
            $arguments['email'],
            $arguments['service_ref'],
            $arguments['password']
        );

        if ($result['error']) {
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $result['error']));
        } else {
            // Check the Filetype, and display the appropriate Success Message
            $result['result']['message'] = $this->getPartial(
                'mypwn/webConferencing' . $arguments['filetype'] . 'Success',
                array()
            );
            echo json_encode(array('success_message' => $result['result']));
        }
        return sfView::NONE;
    }

    /**
     * Call-History [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeCallHistory()
    {
        /** @var myUser $user */
        $user = $this->getUser();

        // Check if the Current User has any Recordings, by Checking their Conference IDs
        $conferenceIDCheck = Common::getConferenceIds($user->getAttribute('contact_ref'));
        if (count($conferenceIDCheck) == 0) {
            return sfView::ERROR;
        }

        // Form Details
        $this->setVar('callHistoryForm', new callHistoryForm());
        $fields = array(
            'service',
            'called_number',
            'country',
            'pin_type',
            'from_date',
            'to_date',
            'callers_number',
            'report',
            'include_unsuccessful_calls',
            'show_all_account'
        );

        if ($user->hasCredential('PLUS_ADMIN')) {
            $fields[] = 'cost_covered';
        }
        $this->setVar('fields', $fields, true);

        // Initial Run of the Call History Data Table
        $result = $this->executeCallHistoryDataTableSelectorAjax(
            array(
                'callhistory_from_date'          => date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 7, date("Y"))),
                'callhistory_to_date'            => date("Y-m-d"),
                'callhistory_service'            => $user->getAttribute('service_ref'),
                'callhistory_called_number'      => '',
                'callhistory_country'            => '',
                'callhistory_pin_type'           => '',
                'callhistory_cli'                => '',
                'callhistory_unsuccessful_calls' => false,
                'callhistory_report_level'       => 'detail',
                'callhistory_show_all_account'   => false,
            ),
            $user
        );

        if (count($result['error']) > 0) {
            $this->logMessage(
                'Error Occurred Trying to obtain Call History at Start, Error:' . print_r($result['error'], true),
                'err'
            );
            return sfView::ERROR;
        }

        $this->setVar('dataTable', $result['dataTable']);
        $this->setVar('dataTableOptions', $result['dataTableOptions']);

        return sfView::SUCCESS;
    }

    /**
     * Call-History [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeCallHistoryAjax(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new callHistoryForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments               = Common::getArguments(
            $request->getParameter($form->getName()),
            array('contact_ref' => $user->getAttribute('contact_ref'))
        );
        $csvMode                 = $request->getParameter('frm-callhistory-csv', false);
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $response->setStatusCode(500);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        $result = $this->executeCallHistoryDataTableSelectorAjax(
            array(
                'callhistory_from_date'          => $arguments['from_date'],
                'callhistory_to_date'            => $arguments['to_date'],
                'callhistory_service'            => $arguments['service'],
                'callhistory_called_number'      => $arguments['called_number'],
                'callhistory_country'            => $arguments['country'],
                'callhistory_pin_type'           => $arguments['pin_type'],
                'callhistory_cli'                => $arguments['callers_number'],
                'callhistory_unsuccessful_calls' => (isset($arguments['include_unsuccessful_calls'])) ? $arguments['include_unsuccessful_calls'] : false,
                'callhistory_report_level'       => $arguments['report'],
                'callhistory_show_all_account'   => (isset($arguments['show_all_account'])) ? $arguments['show_all_account'] : false,
                'csv-mode'                       => (false !== $csvMode) ? true : false,
                'callhistory_cost_covered'       => isset($arguments['cost_covered']) ? $arguments['cost_covered'] : null,
            ),
            $user
        );

        if (count($result['error']) > 0) {
            $this->logMessage(
                'Error Occurred Trying to obtain Call History, Args: ' . print_r($arguments, true)
                . ', Error:' . print_r($result['error'], true),
                'err'
            );
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $result['error']));
            return sfView::NONE;
        }

        // Get Component and Store for Output
        $dataTableRendered = $this->getComponent(
            'mypwn',
            'createDataTable',
            array('dataTable' => $result['dataTable'], 'dataTableOptions' => $result['dataTableOptions'])
        );

        // Response
        if (!$csvMode) {
            $response->setContentType('application/json');
            echo json_encode(array('success' => $dataTableRendered));
        } else {
            $response->clearHttpHeaders();
            $response->setContentType('application/csv');
            $response->setHttpHeader('Content-Description', 'File Transfer');
            $response->setHttpHeader('Content-Disposition', "attachment; filename=call_history_report.csv");
            $response->setHttpHeader('Pragma', 'no-cache');
            $response->setHttpHeader('Cache-Control', '');
            $response->setHttpHeader('Expires', 0);
            $response->sendHttpHeaders();
            echo $dataTableRendered;
        }
        return sfView::NONE;
    }

    /**
     * Call-History Data Table Selector
     *
     * @param array $args - Pre Selected Call History Arguments
     * @param myUser $user
     * @return array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions, 'error' => $error);
     * @author Asfer Tamimi
     *
     */
    protected function executeCallHistoryDataTableSelectorAjax($args, $user)
    {
        // Show All Users Check
        if (isset($args['show_all_account']) && !$user->hasCredential('admin')) {
            $args['show_all_account'] = false;
        }

        $getCallHistoryArgs = array(
            'contact_ref'                => $user->getAttribute('contact_ref'),
            'from_date'                  => $args['callhistory_from_date'],
            'to_date'                    => $args['callhistory_to_date'],
            'service_ref'                => $args['callhistory_service'],
            'called_number'              => $args['callhistory_called_number'],
            'country_code'               => $args['callhistory_country'],
            'pin'                        => $args['callhistory_pin_type'],
            'calling_number'             => $args['callhistory_cli'],
            'include_unsuccessful_calls' => (false !== $args['callhistory_unsuccessful_calls']) ? true : false,
            'report_level'               => $args['callhistory_report_level'],
            'show_all_account'           => (false !== $args['callhistory_show_all_account']) ? true : false,
            'cost_covered'               => isset($arguments['cost_covered']) ? $arguments['cost_covered'] : null,
        );

        // Call History Call
        $callHistoryData = Common::getCallHistory($getCallHistoryArgs);

        // Check for Errors
        if (count($callHistoryData['error']) > 0) {
            return array('error' => $callHistoryData['error']);
        }

        // Data Table Settings
        $dataTableOptions = array(
            'data'       => Common::getFilteredData($callHistoryData['result']['data'], 'Call-History'),
            'tableID'    => 'tblCallHistory',
            'sortOrder'  => 'false',
            'autoWidth'  => false,
            'tableEmpty' => 'You currently do not have any call history.',
            'csv-mode'   => (isset($args['csv-mode'])) ? $args['csv-mode'] : false,
        );

        // Data Table Columns and Mapping Selection
        if (isset($args['dataTable'])) {
            $dataTable = $args['dataTable'];
        } else {
            $report = (isset($args['callhistory_report_level'])) ? $args['callhistory_report_level'] : 'detail';
            switch ($report) {
                case 'summary_by_service':
                    $dataTable                  = array(
                        array('title' => 'Date', 'map' => 'end_date_time'),
                        array('title' => 'Service', 'map' => 'service_name'),
                        array('title' => 'Total number of calls', 'map' => 'num_calls'),
                        array('title' => 'Total Duration', 'map' => 'minutes'),
                    );
                    $dataTableOptions['footer'] = array(
                        '',
                        'Report Total:',
                        $callHistoryData['result']['totals']['num_calls'],
                        $callHistoryData['result']['totals']['minutes'],
                    );
                    break;
                case 'summary_by_dialin':
                    $dataTable                  = array(
                        array('title' => 'Date', 'map' => 'end_date_time'),
                        array('title' => 'Called Number', 'map' => 'called_number'),
                        array('title' => 'Number Type', 'map' => 'dnis_type'),
                        array('title' => 'Service', 'map' => 'service_name'),
                        array('title' => 'Country', 'map' => 'country_code'),
                        array('title' => 'Total number of calls', 'map' => 'num_calls'),
                        array('title' => 'Total Duration', 'map' => 'minutes'),
                    );
                    $dataTableOptions['footer'] = array(
                        '',
                        'Report Total:',
                        '',
                        '',
                        '',
                        $callHistoryData['result']['totals']['num_calls'],
                        $callHistoryData['result']['totals']['minutes'],
                    );
                    break;
                case 'detail':
                default:
                    $dataTable = array(
                        array('title' => 'Date', 'map' => 'end_date_time'),
                        array('title' => 'PINs', 'map' => 'pin_pair'),
                        array('title' => 'PIN Desc / Cost Code', 'map' => 'username', 'class' => 'desc'),
                        array('title' => 'Caller\'s Number', 'map' => 'calling_number'),
                        array('title' => 'Called Number', 'map' => 'called_number'),
                        array('title' => 'Number Type', 'map' => 'dnis_type'),
                        array('title' => 'Country', 'map' => 'country_code'),
                        array('title' => 'Duration', 'map' => 'duration'),
                    );
                    break;
            }
        }

        return array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions, 'error' => array());
    }

    /**
     * Schedule-A-Call [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeScheduleACall()
    {
        /** @var myUser $user */
        $user = $this->getUser();

        // PIN Information Relating to current user
        $pins = PinHelper::getDefaultPins($user->getAttribute('contact_ref'));
        $this->setVar('pin', ($user->hasCredential('POWWOWNOW')) ? $pins['pin'] : $pins['participant_pin']);

        // Get the Default Dial In Number for the User
        $this->setVar('dialInNumber', Common::getDefaultDialInNumber(
            $user->getAttribute('service_ref'),
            $user->getCulture()
        ));

        $this->setVar('service', $user->getAttribute('service'));
        $this->setVar('contactRef', $user->getContactRef());
        $this->setVar('email', $user->getAttribute('email'));
    }

    /**
     * Schedule-A-Call-Ajax [AJAX]
     * This page is only called if a post request is done, and the User Downloads the Outlook Plugin
     *
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeScheduleACallAjax()
    {
        /** @var myUser $user */
        $user = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->setContentType('application/json');

        $result = Common::updateContact(array(
            'contact_ref'    => $this->getUser()->getAttribute('contact_ref'),
            'outlook_plugin' => true
        ));

        if (isset($result['statusCode']) && $result['statusCode'] != 202) {
            $this->logMessage(
                'Contact Record Was Not Updated. Contact Ref: ' . $user->getAttribute('contact_ref'),
                'err'
            );
            $response->setStatusCode(500);
            echo json_encode(array('updated' => 'false'));
        } elseif (isset($result['statusCode']) && $result['statusCode'] == 202) {
            echo json_encode(array('updated' => 'true'));
        } else {
            $this->logMessage(
                'Unknown Error Occurred in Updating Contact. Contact Ref: ' . $user->getAttribute('contact_ref'),
                'err'
            );
            $response->setStatusCode(500);
            echo json_encode(array('updated' => 'false'));
        }

        return sfView::NONE;
    }

    /**
     * Update Recordings [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeUpdateRecordingsAjax(sfWebRequest $request) {
        $this->getResponse()->setContentType('application/json');

        // Form Parameters
        $publish_set    = $request->getParameter('publish_set',false);
        $publish_expire = $request->getParameter('publish_expire',null);
        $password_set   = $request->getParameter('password_set',false);
        $password       = $request->getParameter('rec_password',null);
        $recording_ref  = $request->getParameter('recording_ref',null);
        $description    = $request->getParameter('rec_desc','');
        $fieldId        = $request->getParameter('hidden_field_id',0);
        $password_set   = (!$publish_set) ? false : $password_set;

        // Publish Expire is not Set
        if (is_null($publish_expire) && $publish_set) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(array('error_messages' => array('message' => 'FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_DATE', 'field_name' => 'publish_expire')));
            return sfView::NONE;
        }

        if ($publish_set) {
            $publish_expire_expanded = explode('/',$publish_expire);
            
            // Check Individual Publish Expire Date Components
            if ($publish_expire_expanded[0] == '00' || $publish_expire_expanded[0] > 31) {
                $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_DAY', 'field_name' => 'publish_expire');
            } elseif ($publish_expire_expanded[1] == '00' || $publish_expire_expanded[1] > 12) {
                $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_MONTH', 'field_name' => 'publish_expire');
            } elseif ($publish_expire_expanded[2] < date('Y')) {
                $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_YEAR', 'field_name' => 'publish_expire');
            } else {
                // Check Combined Publish Expire Date
                if (!is_numeric($publish_expire_expanded[0]) || !is_numeric($publish_expire_expanded[1]) || !is_numeric($publish_expire_expanded[2])) {
                    $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_DATE', 'field_name' => 'publish_expire');
                } elseif (!checkdate($publish_expire_expanded[1], $publish_expire_expanded[0], $publish_expire_expanded[2])) {
                    $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_DATE', 'field_name' => 'publish_expire');
                } elseif (strtotime(str_replace('/','-',$publish_expire)) <= strtotime(date('Y-m-d',mktime(0,0,0,date('m'),date('d')+1,date('Y'))))) {
                    $errorMessages = array('message' => 'FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_OLD', 'field_name' => 'publish_expire');
                }
            }
            
            if (isset($errorMessages)) {
                $this->getResponse()->setStatusCode(500);
                echo json_encode(array('error_messages' => $errorMessages));
                return sfView::NONE;
            }
        }

        if ($password_set) {
            $passwordValidation = Common::checkifInvalidPassword($password);
            if (false!== $passwordValidation) {
                $this->getResponse()->setStatusCode(500);
                $errorMessages = array('message' => $passwordValidation, 'field_name' => 'rec_password');
                echo json_encode(array('error_messages' => $errorMessages));
                return sfView::NONE;
            }
        }

        // Check if the Current User has access to the Recording
        $recordingTmp = Common::getRecording(array(
            'contact_ref'   => $this->getUser()->getAttribute('contact_ref'),
            'recording_ref' => $recording_ref
        ));
        $recording = $recordingTmp['result'];
        $error     = $recordingTmp['error'];

        // Check if No Recordings were Found
        if (empty($recording['recording']) || count($error)>0) {
            $this->logMessage('Recording was not Returned from Database. Contact Ref: ' . $this->getUser()->getAttribute('contact_ref') . ', Recording Ref: ' . $recording_ref,'err');
            // Exit? Check Action Type
        }

        // Update Recording
        $result = Common::updateRecording(array(
            'recording_ref'  => $recording_ref,
            'publish_set'    => $publish_set,
            'publish_expire' => $publish_expire,
            'password_set'   => $password_set,
            'password'       => $password,
            'description'    => $description,
            'contact_ref'    => $this->getUser()->getAttribute('contact_ref')
        ));

        if (!empty($resultTmp['error'])) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(array('error_messages' => array('message' => $resultTmp['error'], 'field_name' => 'rec_password')));
            return sfView::NONE;
        }

        // Set Responses
        echo json_encode(array(
            'publish_set'     => (!$publish_set)  ? 0 : 1,
            'password_set'    => (!$password_set) ? 0 : 1,
            'description'     => $description,
            'recording_ref'   => $recording_ref,
            'publish_expire'  => $publish_expire,
            'hidden_field_id' => $fieldId
        ));

        return sfView::NONE;
    }

    /**
     * @desc
     *  Download Recordings [AJAX]
     *  Do NOT use sendContent or setContent as the output for this method.
     * @author
     *  Asfer Tamimi, Dav C
     * @amend
     *  31/07/2014 - Removed ALOT of commented out code. Removed unused vars, Added 404 redirects where sfView::None is returned.
     * @param sfWebRequest $request
     * @return string
     * @throws Exception
     */
    public function executeDownloadRecording(sfWebRequest $request) {
        // Timer
        $timer = sfTimerManager::getTimer('myTimer');

        // Recordings Logging
        $logPath = sfConfig::get('sf_log_dir').'/recordings.log';
        $recordingsLogger = new sfFileLogger(new sfEventDispatcher(), array('file' => $logPath));
        $recordingsLogger->setLogLevel(7); // Set to Lowest (Debug). It will show All Log Entries.

        $recording_ref = $request->getParameter('recording_ref',null);

        if (is_null($recording_ref)) {
            $this->logMessage('Recording Ref Does not Exist for Contact_ref: ' . $this->getUser()->getAttribute('contact_ref'),'err');
            $this->forward404();
            return sfView::NONE;
        }

        $recording = Common::getRecording(array(
            'contact_ref'   => $this->getUser()->getAttribute('contact_ref'),
            'recording_ref' => $recording_ref
        ));

        // Check if No Recordings were Found
        if (empty($recording)) {
            $elapsedTime = $timer->getElapsedTime();
            $recordingsLogger->err(
                'Recording was not Returned from Database. Contact Ref: ' .
                $this->getUser()->getAttribute('contact_ref') . ', Recording Ref: ' . $recording_ref .
                ', Time Taken: ' . $elapsedTime
            );
            $this->forward404();
            return sfView::NONE;
        } else {
            $mm_type="audio/mpeg";

            header('Cache-Control: private, must-revalidate, pre-check=0, post-check=0');
            header('Pragma: private');
            header("Content-Type: " . $mm_type);
            header("Content-Length: " .(string)(filesize($recording['recording'])) );
            header('Content-Disposition: attachment; filename="'.$recording_ref.'.mp3"');
            header("Content-Transfer-Encoding: binary");
            // Show the File
            // We had issues using setContent since it was showing extra bytes in the footer.
            // We also had issues using sendContent since it was showing extra bytes x2 in the footer.
            // Both times the extra bytes were the file size of the source file, which was getting appended..
            //readfile($recording['recording']);
            $handle = fopen($recording['recording'], "rb");
            if (!$handle) {
                throw new Exception('Can not open recording: ' . $recording['recording'] . __CLASS__ . __FILE__);
            }
            while (!feof($handle)) {
                echo fread($handle, 4096);
                ob_flush();
                flush();
            }
            fclose($handle);

            $elapsedTime = $timer->getElapsedTime();
            $recordingsLogger->info(
                'Download Recording. Contact Ref: '. $this->getUser()->getAttribute('contact_ref') .
                ', Recording Ref: ' . $recording_ref . ', FileSize: ' . filesize($recording['recording']) .
                ', Time Taken: ' . $elapsedTime
            );
            die;
        }
    }

    /**
     * Recordings [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeRecordings()
    {
        /** @var myUser $user */
        $user = $this->getUser();
        $minimumDate = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y") - 1));
        $maximumDate = date("Y-m-d");
        $this->setVar('isAdmin', $user->hasCredential('admin'));

        // Form Settings
        if ($user->hasCredential('admin')) {
            $recordingsForm = new recordingsForm();
            $this->setVar('fields', array('from_date', 'to_date', 'description', 'pin', 'show_all_users'), true);
            $recordingsForm->setDefault('from_date', $minimumDate);
            $recordingsForm->setDefault('to_date', $maximumDate);
            $this->setVar('recordingsForm', $recordingsForm);
        }

        // Recordings Data
        $recordings = Common::getRecordingsByContactRef(
            array(
                'contact_ref'    => $user->getAttribute('contact_ref'),
                'show_all_users' => false
            )
        );

        $tableData = Common::getFilteredData(
            $recordings,
            'Recordings',
            array(
                'from_date' => $minimumDate,
                'to_date'   => $maximumDate,
            )
        );

        $this->setVar('showInactivePinMessage', !empty($tableData['contains_inactive_pins']));
        unset($tableData['contains_inactive_pins']);

        // Data Table Settings
        $this->setVar('dataTableOptions', array(
            'data'          => $tableData,
            'tableID'       => 'tblRecordings',
            'sortOrder'     => 'false',
            'autoWidth'     => false,
            'tableEmpty'    => 'You currently do not have any recordings.',
            'displayLength' => 10
        ));

        $this->setVar('dataTable', array(
            array('title' => 'PIN(s)', 'map' => 'pin_pair'),
            array('title' => 'Start Time', 'map' => 'start_date'),
            array('title' => 'Description', 'map' => 'description', 'class' => 'desc'),
            array('title' => 'Conference Duration', 'map' => 'duration_formatted'),
            array('title' => 'Play Recording', 'map' => 'play_recording'),
            array('title' => 'Download / Share Recording', 'map' => 'download_share_recording'),
        ));
    }

    /**
     * Recordings [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeRecordingsAjax(sfWebRequest $request) {
        $this->getResponse()->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new recordingsForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments = Common::getArguments($request->getParameter($form->getName()),array('contact_ref' => $this->getUser()->getAttribute('contact_ref')));
        $arguments['show_all_users'] = (!$this->getUser()->hasCredential('admin')) ? false : $arguments['show_all_users'];
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(500);
            return $this->renderPartial('commonComponents/formErrors', array('form' => $form, 'postFormValidationError' => $postFormValidationError));
        }

        // Recordings Data
        $recordings = Common::getRecordingsByContactRef(array(
            'contact_ref'    => $arguments['contact_ref'], 
            'show_all_users' => $arguments['show_all_users']
        ));

        // Rerun the Component again ???
        // Resend the DataTable data again and refresh the table.
        $dataTmp = Common::getFilteredData($recordings,'Recordings',array(
            'from_date'      => $arguments['from_date'],
            'to_date'        => $arguments['to_date'],
            'description'    => $arguments['description'],
            'pin'            => $arguments['pin'],
            'show_all_users' => $arguments['show_all_users'],
        ));

        // Redo the Data
        $data = array();
        foreach ($dataTmp as $row) {
            $data[] = array(
                $row['pin_pair'],
                $row['start_date'],
                $row['description'],
                $row['duration_formatted'],
                $row['play_recording'],
                $row['download_share_recording']
            );
        }

        echo json_encode(array('data' => $data));
        return sfView::NONE;
    }

    /**
     * Validates public recording passwords
     *
     * @param sfWebRequest $request
     * @return string
     *
     * @author Albert Kozlowski
     */
    public function executeRecordingPasswordValidateAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        $recordingRef = $request->getParameter('recording_ref');
        $password   = $request->getParameter('password');

        $recording = Common::getPublishedRecording(array('recording_ref' => $recordingRef));
        if (!empty($recording['data']['password']) && md5('powwownow'. $password) == $recording['data']['password']) {
            $this->getUser()->setAttribute('rec_' . $recordingRef, true);
            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array('status' => false));
        }
        return sfView::NONE;
    }

    /**
     * Published recording page
     * public page - do not require login
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Albert Kozlowski
     */
    public function executePublicRecording(sfWebRequest $request)
    {
        $recordingRef = $request->getParameter('id');
        $password   = $request->getParameter('password');

        $this->recording = null;
        $this->error = false;
        $this->passwordProtected = false;
        $this->recordingRef = $recordingRef;

        // Recordings status data
        $recordingStatus = Common::getPublishedRecordingStatus(array('recording_ref' => $recordingRef));

        if (empty($recordingStatus)) {
            $this->logMessage('Recording not found ref: ' . $recordingRef,'err');
            $this->error = true;
            $this->errorCode = 0; //not found
            return sfView::SUCCESS;
        }

        $recordingStatus = $recordingStatus['recording_status'];

        if ($recordingStatus['expired']) {
            $this->error = true;
            $this->errorCode = 1; //expired
            return sfView::SUCCESS;
        }

        $this->passwordProtected = $recordingStatus['password_protected'];

        //Recording data
        $recording = Common::getPublishedRecording(array('recording_ref' => $recordingRef));

        if (!isset($recording['data'])) {
            $this->error = true;
            $this->errorCode = 0; //not found
            return sfView::SUCCESS;
        }

        if (!$this->passwordProtected ||
                $this->getUser()->getAttribute('rec_' . $recordingRef) === true) {
            $this->recording = $recording['data'];
            $datePublish = date_create($recording['data']['publish_expire']);
            $this->datePublish = date_format($datePublish,'d/m/Y');
        }
        return sfView::SUCCESS;
    }

    /**
     * Download public recording action, if recording is protected by password
     * we set session vars in publicRecording action
     *
     * @param sfWebRequest $request
     *
     * @throws Exception
     * @return sfView:NONE
     * @author Albert Kozlowski
     */
    public function executePublicRecordingDownload(sfWebRequest $request)
    {
        $recordingRef = $request->getParameter('recording_ref',null);

        // Recordings status data
        $recordingStatus = Common::getPublishedRecordingStatus(array('recording_ref' => $recordingRef));
        if (empty($recordingStatus)) {
            $this->getResponse()->setStatusCode(400);
            $this->logMessage('Recording not found ref: ' . $recordingRef,'err');
            return sfView::NONE;
        }
        $recordingStatus = $recordingStatus['recording_status'];
        if ($recordingStatus['expired']) {
            $this->getResponse()->setStatusCode(400);
            $this->logMessage('Recording expired r ef: ' . $recordingRef,'err');
            return sfView::NONE;
        }
        if ($recordingStatus['password_protected']) {
            if (!$this->getUser()->getAttribute('rec_' . $recordingRef)) {
                $this->getResponse()->setStatusCode(400);
                $this->logMessage('User do not have access to public recording: ' . $recordingRef,'err');
                return sfView::NONE;
            }
        }
        $recording = Common::getPublishedRecording(array('recording_ref' => $recordingRef));
        $this->getResponse()->setStatusCode(206);
        header('Cache-Control: private, must-revalidate, pre-check=0, post-check=0');
        header("Pragma: private");
        header('Accept-Ranges: bytes');
        header("Content-Type: audio/mpeg");
        header("Content-Length: " .(string)(filesize($recording['filePath'])) );
        header('Content-Disposition: attachment; filename="'.$recordingRef.'.mp3"');
        header("Content-Transfer-Encoding: binary");

        $handle = fopen($recording['filePath'], "rb");

        if (!$handle) {
            $this->getResponse()->setStatusCode(400);
            $this->logMessage('Can not open recording: ' . $recording['filePath'] . ' ( ' . __METHOD__ . ' ' . __FILE__ . ')');
            return sfView::NONE;
        }

        while (!feof($handle)) {
            echo fread($handle, 4096);
            ob_flush();
            flush();
        }
        fclose($handle);
        
        return sfView::NONE;
    }

    /**
     * New Registration Overlay
     * @param sfWebRequest $request
     * @todo: Figure out a better way to override a GET variable in a request,
     * (without changing the executeIndex action).
     */
    public function executeNewRegistrationOverlay(sfWebRequest $request)
    {
        $_GET['regNew'] = '';
        $request->initialize($this->dispatcher);
        $this->forward('mypwn', 'index');
    }

    /**
     * Existing Registration Overlay
     * @param sfWebRequest $request
     * @todo: Figure out a better way to override a GET variable in a request,
     * (without changing the executeIndex action).
     */
    public function executeExistingRegistrationOverlay(sfWebRequest $request)
    {
        $_GET['regEx'] = '';
        $request->initialize($this->dispatcher);
        $this->forward('mypwn', 'index');
    }

    /**
     * Index - Homepage Dashboard [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeIndex(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();
        $this->setVar('socialConnect', false);
        $indexParams = $request->getGetParameters();

        // Display information about successful connection of social media
        if (in_array($request->getParameter('social_connect'), array('success', 'failAlreadyConnected'))
            && in_array($request->getParameter('media'), PwnSocialUser::$allowedSocialMedia)
        ) {
            $this->setVar('relatedMedia', $request->getParameter('media'));
            $this->setVar('socialConnect', $request->getParameter('social_connect'));
        }

        $this->setVar('countSocialMediaAccounts', count(
            Hermes_Client_Rest::call(
                'Social.getOAuthUsersByContactRef',
                array('contact_ref' => $this->getUser()->getAttribute('contact_ref'))
            )
        ));

        // Initial Check for Overlay Selection
        $overlayArray  = array('regNew', 'regEx', 'invNew', 'invEx', 'regPromo');
        $this->setVar('overlay', false);

        $initialPopupViewed = $user->getAttribute('initial_popup_viewed');
        $previousLoginTime  = $user->getAttribute('previous_login_time');

        foreach ($overlayArray as $overlay) {
            if (isset($indexParams[$overlay]) ||
                (empty($previousLoginTime) && $overlay == 'invNew'
                    && $user->hasCredential('PLUS_USER')
                    && !empty($initialPopupViewed))
            ) {
                $this->setVar('overlay', $overlay);

                /**
                 * This is set so that people wont be able to see the overlay,
                 * after refresh Assuming they don't go direct to the url
                 */
                $user->setAttribute('initial_popup_viewed', true);
                break;
            }
        }

        // Further Overlay Options
        if (false !== $this->getVar('overlay')) {

            // PIN Information Relating to current user
            $pins = PinHelper::getDefaultPins($user->getAttribute('contact_ref'));

            // Get the Default Dial In Number (Plus User)
            $dialInNumber = Common::getDefaultDialInNumber(850, $user->getCulture());

            $overlayDetails = array(
                'dialInNumber'         => $dialInNumber,
                'pins_chairperson_pin' => $pins['pin'],
                'pins_participant_pin' => $pins['participant_pin'],
                'modal_width'          => 840,
                'modal_height'         => 500,
                'intro_text'           => '',
            );

            if (in_array($this->getVar('overlay'), array('regNew', 'regEx'))) {
                $overlayDetails['modal_height'] = 540;
            }

            $this->setVar('overlayDetails', $overlayDetails, true);
        }

        $bundleDialogs                = sfConfig::get('app_bundle_dialog_default');
        $this->setVar('individualBundleDialog', $bundleDialogs['individual_bundle']);

        // Product Selector Tool
        $result               = Common::getBundlesForProductSelector();
        $this->setVar('bundles', array_merge($result, array('showBuyButtons' => true)));
        $this->setVar('isPlusAdmin', $user->hasCredential('PLUS_ADMIN'));
        $this->setVar('isEnhancedUser', $user->hasCredential('POWWOWNOW'));

        if ($this->getVar('isEnhancedUser')) {
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Partial'));
            $this->setVar('plusTemplates', array(
                'plusFormWithPassword1'   => get_partial(
                    'commonComponents/plusFormWithPassword1',
                    array(
                        'email'      => $user->getAttribute('email'),
                        'first_name' => $user->getAttribute('first_name'),
                        'last_name'  => $user->getAttribute('last_name')
                    )
                ),
                'plusRedirectionExisting' => get_partial('commonComponents/plusRedirectionExisting', array()),
                'plusNoSwitch1'           => get_partial('commonComponents/plusNoSwitch1', array()),
            ), true);
        }
    }

    /**
     * Users [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeUsers(sfWebRequest $request) {
        $this->contactsAll = PinHelper::getContactPinPairs($this->getUser()->getAttribute('contact_ref'),'ADMIN',true);

        // Data Table
        $this->dataTableOptions = array(
            'data'      => Common::getFilteredData($this->contactsAll,'USERS'),
            'tableID'   => 'tblUsers',
            'sortOrder' => 'false',
            'autoWidth' => false
        );

        $this->dataTable = array(
            array('title' => '<input type="checkbox" onchange="myPwnApp.users.toggleCheckbox(this.checked);">', 'map' => 'checkboxcol', 'class' => 'checkboxcol'),
            array('title' => 'Name', 'map' => 'chair_full_name'),
            array('title' => 'Email Address', 'map' => 'chair_email', 'class' => 'longcol'),
            array('title' => 'Chair PIN', 'map' => 'chair_pin'),
            array('title' => 'Participant PIN', 'map' => 'participant_pin'),
            array('title' => 'Cost Code', 'map' => 'chair_username'),
        );
    }

    /**
     * PINs [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executePins()
    {
        /** @var myUser $user */
        $user         = $this->getUser();
        $userType     = ($user->hasCredential('PREMIUM_ADMIN')) ? 'ADMIN' : 'USER';
        $contactsPins = PinHelper::getContactPinPairs($user->getAttribute('contact_ref'), $userType);

        // Get the Default Dial In Number (Powwownow User) - May Need to be changed later on
        $this->setVar('dialInNumber', Common::getDefaultDialInNumber(801, $user->getCulture()));

        // Powwownow PIN / Plus User PIN Information
        foreach ($contactsPins as $contact) {
            if ($user->hasCredential('POWWOWNOW')) {
                $this->setVar('pin', $contact['chair_pin']);
            } elseif ($user->hasCredential('PLUS_USER')) {
                $this->setVar('part_pin', $contact['participant_pin']);
                $this->setVar('chair_pin', $contact['chair_pin']);
                $this->setVar('chair_pin_ref', $contact['chair_pin_ref']);
            }
        }

        // Data Table
        $this->setVar('dataTableOptions', array(
            'data'      => Common::getFilteredData($contactsPins, 'PINS'),
            'tableID'   => 'tblPINs',
            'sortOrder' => 'false',
            'autoWidth' => false
        ));

        if ($user->hasCredential('PLUS_ADMIN')) {
            $this->setVar('dataTable', array(
                array(
                    'title' => '<input type="checkbox" onchange="pinsToggleCheckbox(this.checked);">',
                    'map'   => 'checkboxcol',
                    'class' => 'checkboxcol'
                ),
                array('title' => 'Name', 'map' => 'chair_full_name'),
                array('title' => 'Email Address', 'map' => 'chair_email', 'class' => 'longcol'),
                array('title' => 'Chair PIN', 'map' => 'chair_pin'),
                array('title' => 'Participant PIN', 'map' => 'participant_pin'),
                array('title' => 'Cost Code', 'map' => 'chair_username'),
            ));
        } elseif ($user->hasCredential('PREMIUM_USER')) {
            $this->setVar('dataTable', array(
                array(
                    'title' => '<input type="checkbox" onchange="pinsToggleCheckbox(this.checked);">',
                    'map'   => 'checkboxcol',
                    'class' => 'checkboxcol'
                ),
                array('title' => 'Chair PIN', 'map' => 'chair_pin'),
                array('title' => 'Participant PIN', 'map' => 'participant_pin'),
                array('title' => 'Geographic No.', 'map' => 'chair_enable_geographic'),
                array('title' => 'Freephone No.', 'map' => 'chair_enable_freefone'),
                array('title' => 'Cost Code', 'map' => 'chair_username'),
            ));
        } elseif ($user->hasCredential('PREMIUM_ADMIN')) {
            $this->setVar('dataTable', array(
                array(
                    'title' => '<input type="checkbox" onchange="pinsToggleCheckbox(this.checked);">',
                    'map'   => 'checkboxcol',
                    'class' => 'checkboxcol'
                ),
                array('title' => 'Name', 'map' => 'chair_full_name'),
                array('title' => 'Email Address', 'map' => 'chair_email', 'class' => 'longcol'),
                array('title' => 'Chair PIN', 'map' => 'chair_pin'),
                array('title' => 'Participant PIN', 'map' => 'participant_pin'),
                array('title' => 'Geographic No.', 'map' => 'chair_enable_geographic'),
                array('title' => 'Freephone No.', 'map' => 'chair_enable_freefone'),
                array('title' => 'Cost Code', 'map' => 'chair_username'),
            ));
        }
    }

    /**
     * PINs / Users [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executePinsAjax(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $function      = $request->getPostParameter('function', null);
        $pins          = $request->getPostParameter('r', null);
        $errorMessages = array();
        $successMessage = array();
        $result = array();

        // Check for Initial Errors
        if (!in_array($function, array('delete_pins', 'add_pin', 'delete_users'))) {
            // Invalid Function
            // There is no Error Message
            $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
        } elseif ('delete_pins' == $function && !is_array($pins)) {
            $errorMessages = array('message' => 'FORM_VALIDATION_NO_PINS_SELECTED', 'field_name' => 'alert');
        }

        if (in_array($function, array('delete_pins', 'delete_users')) && empty($errorMessages)) {
            // Setup Lists to check against
            $userPinRefs  = PinHelper::getContactPinPairsRefs(
                PinHelper::getContactPinPairs($user->getAttribute('contact_ref'), 'USER')
            );
            $adminPinRefs = PinHelper::getContactPinPairsRefs(
                PinHelper::getContactPinPairs($user->getAttribute('contact_ref'), 'ADMIN')
            );

            // Check if the User Posted a PIN Ref Which is not in their Account.
            // Also Check if the Admin was trying to Delete ALL his PIN Refs. Count Should NOT be 0.
            if (count(array_diff($pins, $adminPinRefs)) != 0) {
                $this->logMessage(
                    'Contact Ref: ' . $user->getAttribute(
                        'contact_ref'
                    ) . ' tried to delete a PIN Ref not belonging to their Account',
                    'err'
                );
                $errorMessages = array(
                    'message'    => 'FORM_RESPONSE_PINS_DELETED_FAILURE_ADMIN_PIN',
                    'field_name' => 'alert'
                );
            } elseif (count(array_diff($userPinRefs, $pins)) == 0) {
                $this->logMessage(
                    'Contact Ref: ' . $user->getAttribute(
                        'contact_ref'
                    ) . ' tried to delete a ALL his Admin PINs',
                    'err'
                );
                $errorMessages = array(
                    'message'    => 'FORM_RESPONSE_PINS_DELETED_FAILURE_ADMIN_PIN',
                    'field_name' => 'alert'
                );
            }

            if (count($errorMessages) == 0) {
                $result = PinHelper::deactivatePINPairs($user->getAttribute('contact_ref'), $pins);

                if (count($result['failure']) > 0) {
                    $errorMessages = array('message' => $result['message'], 'field_name' => 'alert');
                } else {
                    $successMessage = array('message' => $result['message'], 'field_name' => 'alert');
                }
            }
        } elseif ($function == 'add_pin' && count($errorMessages) == 0) {
            if ($user->hasCredential('PLUS_ADMIN')) {
                $resultTmp     = PinHelper::doPlusAddPins(
                    array(
                        'service_ref' => $user->getAttribute('service_ref'),
                        'contact_ref' => $user->getAttribute('contact_ref'),
                        'account_id'  => $user->getAttribute('account_id'),
                    )
                );
                $errorMessages = $resultTmp['error'];
                $result        = $resultTmp['result'];

                if (isset($result['pins'], $result['pins']['chairman'], $result['pins']['participant'])) {
                    $pins                = array(
                        $result['pins']['chairman']['pin_ref'],
                        $result['pins']['participant']['pin_ref'],
                    );
                    $featureAssignResult = ProductHelper::assignPinsToFeatures($pins);
                    if (!$featureAssignResult) {
                        $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                    }
                    $accountId                    = $user->getAttribute('account_id');
                    $adminProductAssignmentResult = ProductHelper::assignAdminPinsToNonAYCMFeatureProducts(
                        $accountId,
                        $pins
                    );
                    if (!$adminProductAssignmentResult) {
                        $errorMessages = array(
                            'message'    => 'Unable to assign your new PIN pair to your enabled products.',
                            'field_name' => 'alert'
                        );
                    }
                } elseif (!$errorMessages) {
                    $this->logMessage(
                        'Could not assign the new pins to all feature products, because no created pins were found, and no errors were found either.',
                        'err'
                    );
                    $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                }
            } else {
                $this->logMessage(
                    'A Non Plus Admin, with the Contact Ref: ' . $user->getAttribute(
                        'contact_ref'
                    ) . ' tried to Add a New PIN.',
                    'err'
                );
                $errorMessages = array('message' => 'FORM_RESPONSE_PINS_DELETED_FAILURE', 'field_name' => 'alert');
            }

            if (empty($errorMessages)) {
                $successMessage = array(
                    'message'    => 'Your PIN pair has been created. Chairperson PIN: ' .
                        $result['pins']['chairman']['pin'] . ', Participant PIN: ' .
                        $result['pins']['participant']['pin'],
                    'field_name' => 'alert'
                );
            }
        }

        $response->setContentType('application/json');
        if (count($errorMessages) > 0) {
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $errorMessages));
        } else {
            echo json_encode(array('success_message' => $successMessage));
        }

        return sfView::NONE;
    }

    /**
     * Create-User [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     * @author Maarten Jacobs
     */
    public function executeCreateUser()
    {
    }

    /**
     * Create-User [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     * @author Maarten Jacobs
     * @amend Dav
     *  Changed response output - put some good old HTML in so that response looks pretty
     *  since it is rendered straight of,
     *
     */
    public function executeCreateUserAjax(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user       = $this->getUser();
        $contactRef = $user->getContactRef();
        $accountId  = $user->getAccountId();
        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new createUserForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments               = Common::getArguments(
            $request->getParameter($form->getName()),
            array(
                'contact_ref' => $contactRef,
                'account_id'  => $accountId,
            )
        );
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $response->setStatusCode(500);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        // Add Contact and PINs to Plus Account
        $result = PinHelper::doPlusAddContactAndPins(
            array(
                'title'       => $arguments['title'],
                'first_name'  => $arguments['first_name'],
                'last_name'   => $arguments['last_name'],
                'cost_code'    => $arguments['cost_code'],
                'email'       => $arguments['email'],
                'phone'       => $arguments['phone_number'],
                'password'    => $arguments['password'],
                'locale'      => $user->getCulture(),
                'source'      => 'Admin via myPwn',
                'service_ref' => 850,
                'account_id'  => $arguments['account_id'],
            ),
            (isset($arguments['notify_user'])) ? $arguments['notify_user'] : false,
            $arguments['contact_ref']
        );

        // Assign all assignable products to the pins
        $newUserPinAssignmentErrors = false;
        if (!empty($result['result']['pins']['chairman']['pin_ref']) &&
            !empty($result['result']['pins']['participant']['pin_ref'])) {
            $newUserPinAssignmentErrors = $this->assignNonAYCMProductsToPins(
                $accountId,
                array(
                    $result['result']['pins']['chairman']['pin_ref'],
                    $result['result']['pins']['participant']['pin_ref']
                )
            );
        }

        if (empty($result['result'])) {
            $result['error']['field_name'] = str_replace('email', 'createUser[email]', $result['error']['field_name']);
            $response->setStatusCode(500);
            echo json_encode(array('error_messages' => $result['error']));
        } elseif ($newUserPinAssignmentErrors) {
            $response->setStatusCode(500);
            $error = array(
                'message'    => 'We were unable to assign all of your products to the PINs of the user.',
                'field_name' => 'alert',
            );
            echo json_encode(array('error_messages' => array($error)));
        } else {
            $notifyMessage = '';
            if (isset($arguments['notify_user']) && $arguments['notify_user'] === 'on') {
                $notifyMessage = '<div class="notify-user">These details will be emailed to the user shortly.</div>';
            }

            $message = '<div class="create-user-container"><div class="title">Your PIN pair has been created.</div>' .
                '<div class="chairperson"> Chairperson PIN: ' . $result['result']['pins']['chairman']['pin'] .
                '</div><div class="participant-pin"> Participant PIN: ' .
                $result['result']['pins']['participant']['pin'] . '</div>' . $notifyMessage . '</div>';

            echo json_encode(
                array(
                    'success_message' =>
                        array('message' => $message, 'field_name' => 'alert')
                )
            );
        }
        return sfView::NONE;
    }

    /**
     * Assigns all account products, except for the AYCM Bundle (if any) to the given contact.
     *
     * @param int $accountId
     * @param array $pinRefs
     * @return bool
     *   Checks if an error has occurred whilst trying to update the assigned products.
     * @author Maarten Jacobs
     */
    private function assignNonAYCMProductsToPins($accountId, array $pinRefs)
    {
        // Get all products, only their product group ids, assigned to the account,
        // Excluding the AYCM bundle product group id.
        $accountProductIds = $this->retrieveAllNonAYCMAccountProductGroupIds($accountId);

        // In case of an error occurring in retrieval: push it up the flow.
        if ($accountProductIds === false) {
            return true;
        }

        // Assign each PIN to the product.
        $errorOccurred = false;
        foreach ($pinRefs as $pinRef) {
            try {
                Hermes_Client_Rest::call(
                    'Default.updatePlusPinProductGroups', array(
                        'pin_ref' => $pinRef,
                        'assign_product_group_ids' => $accountProductIds,
                        'use_transactions' => false,
                    )
                );
            } catch (Exception $e) {
                $errorOccurred = true;

                $errorMessage = sprintf(
                    "An exception was thrown when trying to assign one of the new user's pins to the account products. " .
                    'The exception message is "%s". And the exception code is "%s".',
                    $e->getMessage(),
                    $e->getCode()
                );
                $this->logMessage($errorMessage, 'error');
            }
        }

        return $errorOccurred;
    }

    /**
     * Retrieves all ids of the products assigned to the account, excluding the AYCM bundle.
     *
     * @param int $accountId
     * @return array|bool
     *   If an Exception occurs (Hermes times out or similar), returns false.
     *   Else, returns a list of product group ids.
     * @author Maarten Jacobs
     */
    private function retrieveAllNonAYCMAccountProductGroupIds($accountId)
    {
        // A bit of a hack: passing an account_id means we'll have the result from Default.getPlusAccountProductGroups
        // which isn't filtered by the bundle parameter.
        // The bundles are then returned in the product_list key, and the assigned products in the assigned_products key.
        try {
            $accountProducts = Hermes_Client_Rest::call('getAllPlusProductGroups', array('account_id' => $accountId, 'bundle' => true));
        } catch (Exception $e) {
            $errorMessage = sprintf(
                'An exception was thrown when trying to retrieve the account products. ' .
                'The exception message is "%s". And the exception code is "%s".',
                $e->getMessage(),
                $e->getCode()
            );
            $this->logMessage($errorMessage, 'error');

            // Push the error up the flow.
            return false;
        }

        // Go through all Bundle products to retrieve all AYCM product group ids, so we can filter them out at the end.
        $aycmProductIds = array();
        foreach ($accountProducts['product_list'] as $bundleProduct) {
            if (!empty($bundleProduct['is_individual_bundle'])) {
                $aycmProductIds[] = $bundleProduct['product_group_id'];
            }
        }

        // Go through all account products to retrieve all account product group ids.
        $accountProductIds = array();
        foreach ($accountProducts['assigned_products'] as $accountProduct) {
            $accountProductIds[] = $accountProduct['product_group_id'];
        }

        return array_diff($accountProductIds, $aycmProductIds);
    }

    /**
     * Create-Pin [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeCreatePin()
    {
        $this->setVar('form', new createPinForm());
        $this->setVar(
            'fields',
            array(
                'email',
                'cost_code',
                'title',
                'first_name',
                'middle_initials',
                'last_name',
                'phone_number',
                'mobile_phone_number',
                'password',
                'confirm_password'
            )
        );
    }

    /**
     * Create-Pin [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeCreatePinAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new createPinForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments = Common::getArguments(
            $request->getParameter($form->getName()),
            array('contact_ref' => $this->getUser()->getAttribute('contact_ref'))
        );
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        // Get Current Admin Details
        $current_details = Common::getContactByRef(
            array('contact_ref' => $arguments['current_contact_ref'], 'master_service_ref' => 900)
        );
        if (empty($current_details) || isset($current_details['error'])) {
            $this->getResponse()->setStatusCode(400);
            $this->logMessage('Failed to Return User Contact Details','err');
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        } else {
            // Different Actions for New User / Existing User
            if (empty($arguments['contact_ref'])) {
                if (empty($arguments['cost_code'])) {
                    $arguments['cost_code'] = $arguments['first_name'] . ' ' . $arguments['last_name'];
                }
                $result = PinHelper::doPremiumAddContactAndPins(array(
                    'title'            => $arguments['title'],
                    'first_name'       => $arguments['first_name'],
                    'middle_initials'  => $arguments['middle_initials'],
                    'last_name'        => $arguments['last_name'],
                    'email'            => $arguments['email'],
                    'mobile_phone'     => $arguments['mobile_phone_number'],
                    'phone'            => $arguments['phone_number'],
                    'password'         => $arguments['password'],
                    'locale'           => $this->getUser()->getCulture(),
                    'source'           => 'Admin via myPwn',
                    'service_ref'      => $current_details['service_ref'],
                    'customer_ref'     => $current_details['customer_ref'],
                    'username'         => $arguments['cost_code'],
                ));
            } else {

                $new_user = Common::getContactByRef(
                    array('contact_ref' => $arguments['contact_ref'], 'master_service_ref' => 900)
                );

                if (!empty($arguments['cost_code'])) {
                    $cost_code = $new_user['first_name'] . ' ' . $new_user['last_name'];
                }
                $result = PinHelper::doPremiumAddPins(array(
                    'contact_ref'  => $arguments['contact_ref'],
                    'service_ref'  => $current_details['service_ref'],
                    'customer_ref' => $current_details['customer_ref'],
                    'username'     => $cost_code
                ));
            }
        }

        if (!empty($result['error'])) {
            $result['error']['field_name'] = str_replace('email','createPin[email]', $result['error']['field_name']);
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error_messages' => $result['error']));
        } else {
            echo json_encode(
                array(
                    'success_message' => array(
                        'message' => 'Your PIN pair has been created. Chairperson PIN: '
                            . $result['result']['pins']['chairman']['pin']
                            . ', Participant PIN: '
                            . $result['result']['pins']['participant']['pin'], 'field_name' => 'alert'
                    )
                )
            );
        }
        return sfView::NONE;
    }

    /**
     * In-Conference Controls [PAGE]
     *
     * @return string
     * @author Robert Pajak
     *
     */
    public function executeInConferenceControls()
    {
        $this->setVar('service', $this->getUser()->getAttribute('service'));
    }

    /**
     * Dial-In-Numbers [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeDialInNumbers()
    {
        $user = $this->getUser();
        $this->setVar('service', $this->getUser()->getAttribute('service'));

        $dialInNumbers = Common::getContactDialInNumbers(
            $user->getAttribute('contact_ref'),
            (isset($_SERVER['language'])) ? $_SERVER['language'] : "en"
        );
        
        // Data Table Settings
        $this->setVar('dataTableOptions', array(
            'data'       => Common::getFilteredData($dialInNumbers,'Dial-In-Numbers'),
            'tableID'    => 'tblcontactdialinnumbers',
            'sortOrder'  => 'false',
            'autoWidth'  => false,
            'tableEmpty' => 'You currently do not have any dial in numbers.'
        ));

        $this->setVar('dataTable', array(
            array('title' => 'Country (Language)',   'map' => 'country_name_language'),
            array('title' => 'In-Country Number',    'map' => 'national_formatted'),
            array('title' => 'International Number', 'map' => 'international_formatted'),
            array('title' => 'Cost per min',         'map' => 'cpm_check'),
            array('title' => 'Type',                 'map' => 'dnis_type'),
        ));

        $this->setVar('isPostPayCustomer', false);
        $this->setVar('hasInternationalAddOn', false);
        if ($user->hasCredential('PLUS_ADMIN') || $user->hasCredential('PLUS_USER')) {
            $this->setVar('isPostPayCustomer', $user->isPostPayCustomer());
            if ($this->getVar('isPostPayCustomer')) {
                $accountBundle = $user->getCurrentAccountBundle();
                $this->setVar('hasInternationalAddOn', (bool) $accountBundle['int_geo']);
            }
        }
    }

    /**
     * Request-Welcome-Pack [PAGE]
     *
     * @param sfWebRequest $request
     * @throws Exception
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeRequestWelcomePack(sfWebRequest $request)
    {
        $user       = $this->getUser();
        $contactRef = $user->getAttribute('contact_ref');
        $this->getContext()->getConfiguration()->loadHelpers('countryCodes');
        $this->setVar('isAdmin', $user->hasCredential('admin'));
        $this->setVar('powwownowUser', $user->hasCredential('POWWOWNOW'));
        $this->setVar('plusUser', $user->hasCredential('PLUS_USER'));
        $this->setVar('premiumUser', $user->hasCredential('PREMIUM_USER'));
        $this->setVar('service', $user->getAttribute('service'));

        // Check if there are any PINs in the URL (Arr)
        $this->setVar('preSelectedPINs', $request->getGetParameter('r', array()));

        // Add the Additional Wallet Card Information to the PIN Pairs Array
        if ($this->getVar('isAdmin') || $this->getVar('premiumUser')) {
            $userType = 'ADMIN';
        } else {
            $userType = 'USER';
        }
        $pinPairsArr = Common::updatePinPairsToWalletCardInfo(
            PinHelper::getContactPinPairs($contactRef, $userType),
            $contactRef
        );

        // Check if there were any Errors found.
        if ($pinPairsArr['errorsFound']) {
            $this->logMessage("An error occurred loading your welcome pack information.", 'err');
            throw new Exception("An error occurred loading your welcome pack information.");
        }

        $this->setVar('pinPairs', $pinPairsArr['pinPairs']);

        // Check if a USER has previously ordered a Wallet Card
        $this->setVar('previouslyOrderedAWalletCard', false);
        foreach ($this->getVar('pinPairs') as $pin) {
            if ($pin['remaining_wallet_card_requests'] < 3) {
                $this->setVar('previouslyOrderedAWalletCard', true);
                break;
            }
        }

        // Does Address and Preview Calculations
        $results = $this->requestWalletCardPreview($this->getVar('preSelectedPINs'), $this->getVar('pinPairs'));

        // Additional Form Values
        $this->setVar('walletCardCanBeOrdered', false);
        foreach ($this->getVar('pinPairs') as $pinPair) {
            if ($this->getVar('isAdmin')
                && !empty($pinPair['chair_first_name'])
                && !empty($pinPair['chair_last_name'])
            ) {
                $this->setVar('walletCardCanBeOrdered', true);
            }

            if (count($this->getVar('pinPairs')) == 1
                && ($this->getVar('powwownowUser') || $this->getVar('plusUser'))
            ) {
                $this->setVar('pin', $pinPair['chair_pin_ref']);
            }
        }

        $formDefaults = array(
            'first_name' => $user->getAttribute('first_name'),
            'last_name'  => $user->getAttribute('last_name'),
            'company'    => $results['address']['organisation'],
            'address'    => $results['address']['streetAddress'],
            'town'       => $results['address']['town'],
            'county'     => $results['address']['county'],
            'postcode'   => $results['address']['post_code'],
            'country'    => $results['address']['country'],
        );

        // Get Countries List
        $countriesList = Common::getCountries($user->getCulture());
        $default             = array(
            'text'       => 'Select Country',
            'country_id' => '',
        );
        $countriesList = array_merge(array($default), $countriesList);
        $this->setVar('countriesList', getCountriesList($countriesList, $formDefaults['country']), true);

        // Request Welcome Pack Form
        $this->setVar('requestWelcomePackForm', new requestWelcomePackForm($formDefaults));
        if (!$this->getVar('isAdmin')) {
            $this->setVar(
                'formFields',
                array('first_name', 'last_name', 'company', 'address', 'town', 'county', 'postcode')
            );
        } else {
            $this->setVar('formFields', array('company', 'address', 'town', 'county', 'postcode'));
        }

        // Data Table
        $this->setVar('dataTableOptions', array(
            'data'      => Common::getFilteredData(
                    $this->getVar('pinPairs'),
                    'Request-Welcome-Pack',
                    $this->getVar('preSelectedPINs')
                ),
            'tableID'   => 'tblRequestWelcomePack',
            'sortOrder' => 'false',
        ));
        $this->setVar('dataTable', array());
        $this->setVar('formDefaults', $formDefaults);
        $this->setVar('results', $results);
    }

    /**
     * Request-Welcome-Pack [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeRequestWelcomePackAjax(sfWebRequest $request) {
        $errorMessages   = array();
        $errorsFound     = false;
        $successPINPairs = array();
        $failurePINPairs = array();
        $result          = array();

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new requestWelcomePackForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments = Common::getArguments($request->getParameter($form->getName()),array('contact_ref' => $this->getUser()->getAttribute('contact_ref')));
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(500);
            return $this->renderPartial('commonComponents/formErrors', array('form' => $form, 'postFormValidationError' => $postFormValidationError));
        }

        // If the Current User is a USER (Powwownow User, Plus User or Premium User) you need to update their Contact Record
        // First and Last name are stored in Contact Table, and also are Required Fields in this Form
        if ($this->getUser()->hasCredential('user')) {
            $contactUpdate = Common::updateContact(array(
                'contact_ref' => $arguments['contact_ref'],
                'first_name'  => $arguments['first_name'],
                'last_name'   => $arguments['last_name']
            ));
            if ($contactUpdate['statusCode'] != 202) {
                $this->logMessage('Contact Record was not Updated. Contact Ref: ' . $arguments['contact_ref'], 'err');
            } else {
                // Update Session Cookies Here with the New First Name and Last Name
                if ($this->getUser()->hasAttribute('first_name')) $this->getUser()->setAttribute('first_name',$arguments['first_name']);
                if ($this->getUser()->hasAttribute('last_name')) $this->getUser()->setAttribute('last_name',$arguments['last_name']);
            }

            // Clear Cache
            try {
                $cacheCleared = PWN_Cache_Clearer::modifiedContact($arguments['contact_ref']);
                $this->logMessage('PWN_Cache_Clearer::modifiedContact cleared for Contact Ref: ' . $arguments['contact_ref'] . ', Cache: ' . serialize($cacheCleared), 'err');
            } catch (Exception $e) {
                $this->logMessage('An exception occurred calling PWN_Cache_Clearer::modifiedContact for Contact Ref: ' . $arguments['contact_ref'] . ', Error: ' . serialize($e), 'err');
            }
        }

        // Add the Additional Wallet Card Information to the PIN Pairs Array
        $userType = ($this->getUser()->hasCredential('admin') || $this->getUser()->hasCredential('PREMIUM_USER')) ? 'ADMIN' : 'USER';
        $pinPairsArr = Common::updatePinPairsToWalletCardInfo(PinHelper::getContactPinPairs($this->getUser()->getAttribute('contact_ref'),$userType), $this->getUser()->getAttribute('contact_ref'));
        $pinPairs = $pinPairsArr['pinPairs'];
        $validPins = array();
        foreach ($pinPairs as $pin) {
            $validPins[] = $pin['chair_pin_ref'];
        }


        // Go Through Each PIN Ref
        foreach ($arguments['pin_ref'] as $id => $pin_ref) {
            if ($this->getUser()->hasCredential('admin') || $this->getUser()->hasCredential('PREMIUM_USER')) {
                $pinsRefArr = explode(',',$pin_ref);
                $pinRef   = $pinsRefArr[0];
                $chairPin = $pinsRefArr[1];
                $partPin  = $pinsRefArr[2];
            } else {
                $pinRef = $pin_ref;
            }

            if (!in_array($pinRef, $validPins)) {
                $failurePINPairs[$result[$id]['error'][0]['message']][] = 'Wrong data';
                $errorsFound = true;
                continue;
            }

            $result[$id] = Common::createWalletCardRequest(array(
                // Wallet Card Arguments
                'contact_ref' => $arguments['contact_ref'],
                'pin_ref'     => $pinRef,
                'company'     => $arguments['company'],
                'street'      => $arguments['address'],
                'town'        => $arguments['town'],
                'county'      => $arguments['county'],
                'post_code'   => $arguments['postcode'],
                'country'     => $arguments['country'],

                // Additional Arguments
                'Chair_PIN'   => (isset($chairPin)) ? $chairPin : '',
                'Part_PIN'    => (isset($partPin)) ? $partPin : '',
                'isAdmin'     => ($this->getUser()->hasCredential('admin')) ? true : false,
            ));

            // Store PIN Pairs - Both Successful and Failure PIN Pairs
            if (count($result[$id]['error']) > 0 && $this->getUser()->hasCredential('admin')) {
                $failurePINPairs[$result[$id]['error']['message']][] = array('ChairPIN' => $chairPin, 'PartPIN' => $partPin);
                $errorsFound = true;
            } elseif (count($result[$id]['error']) == 0 && $this->getUser()->hasCredential('admin')) {
                $successPINPairs[] = array('ChairPIN' => $chairPin, 'PartPIN' => $partPin);
            } elseif (count($result[$id]['error']) > 0) {
                $failurePINPairs = $result[$id]['error']['message'];
                $errorsFound = true;
            }
        }

        // Errors Found for All Users - Set Output Response
        $this->getResponse()->setContentType('application/json');
        if ($errorsFound) {
            $this->getResponse()->setStatusCode(500);
        }

        // Optimise the Successful PIN Pair Arrays
        if (count($successPINPairs) > 0) {
            $successPINPairs = $this->requestWalletCardPinPairArrays($successPINPairs,'success');
        }

        // Optimise the Failure PIN Pair Arrays
        if (count($failurePINPairs) > 0 && $this->getUser()->hasCredential('admin')) {
            $failurePINPairs = $this->requestWalletCardPinPairArrays($failurePINPairs,'failure');
        }

        // Powwownow User / Plus User / Premium User
        if ($this->getUser()->hasCredential('POWWOWNOW') || $this->getUser()->hasCredential('PLUS_USER') || $this->getUser()->hasCredential('PREMIUM_USER')) {
            if ($errorsFound) {
                echo json_encode(array('error_messages'  => array('message' => Common::getRequestWelcomePackMessage($failurePINPairs,'',false), 'field_name' => 'alert')));
            } else {
                echo json_encode(array('success_message' => array('message' => Common::getRequestWelcomePackMessage('WALLET_CARD_REQUEST_SUCCESS','',false), 'field_name' => 'alert')));
            }
        }

        // Plus Admin / Premium Admin
        if ($this->getUser()->hasCredential('admin')) {
            if ($errorsFound) {
                $message = '';

                if (count($successPINPairs) != 0) {
                    $message = Common::getRequestWelcomePackMessage('WALLET_CARD_REQUEST_SUCCESS_SINGLE',$successPINPairs,true);
                }

                if (count($failurePINPairs) != 0) {
                    foreach ($failurePINPairs as $errorType => $pinPairs) {
                        $message.= Common::getRequestWelcomePackMessage($errorType,$pinPairs,true);
                    }
                }

                echo json_encode(array('error_messages'  => array('message' => rtrim($message), 'field_name' => 'alert')));
            } else {
                echo json_encode(array('success_message' => array('message' => Common::getRequestWelcomePackMessage('WALLET_CARD_REQUEST_SUCCESS_SINGLE',$successPINPairs,true), 'field_name' => 'alert')));
            }
        }

        return sfView::NONE;
    }

    /**
     * Optimise the Request Wallet Card PIN Pair Arrays
     *
     * @param array  $inputPinPair - PIN Pairs
     * @param string $arrType      - Success | Failure
     * @return array $result       - Updated PIN Pairs Array
     * @author Asfer Tamimi
     *
     */
    protected function requestWalletCardPinPairArrays(array $inputPinPair, $arrType) {
        $result = array();
        if ('success' == $arrType) {
            $tmpPINs = '';
            foreach ($inputPinPair as $pin_pair) {
                $tmpPINs.= $pin_pair['ChairPIN'] . '/' . $pin_pair['PartPIN'] . ' , ';
            }
            $result = rtrim($tmpPINs,' , ');
        } elseif ('failure' == $arrType) {
            $tmpPINsArr = array();
            $tmpPINsStr = '';
            foreach ($inputPinPair as $errorType => $pin_pairs) {
                foreach ($pin_pairs as $pin_pair) {
                    $tmpPINs[$errorType][] = $pin_pair['ChairPIN'] . '/' . $pin_pair['PartPIN'];
                }
                $tmpPINsStr[$errorType] = rtrim(implode(' , ', $tmpPINs[$errorType]),' , ');
            }
            $result = $tmpPINsStr;
        }
        return $result;
    }

    /**
     * Obtain the Request Wallet Card Information for the Preview
     *
     * @param  array $preSelectedPINs - User Defined PIN Pair Refs
     * @param  array $pinPairs        - Account PIN Pairs
     * @return array('preview' = array(),'address' = array())
     * @author Asfer Tamimi
     *
     */
    protected function requestWalletCardPreview($preSelectedPINs,$pinPairs) {
        // Obtain the Wallet Card Address for the Current User
        $address = Common::getWalletCardAddress(array(
            'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
            'pin_ref'     => false
        ));

        // Street Address
        $streetAddress = '';
        if (!empty($address)) {
            if (!empty($address['building']) && !empty($address['street'])) {
                $streetAddress = $address['building'] . ', ' . $address['street'];
            } else {
                $streetAddress = $address['building'] . $address['street'];
            }
        }

        $organisation = (!empty($address['organisation'])) ? $address['organisation'] : '';
        $town         = (!empty($address['town']))         ? $address['town']         : '';
        $county       = (!empty($address['county']))       ? $address['county']       : '';
        $post_code    = (!empty($address['post_code']))    ? $address['post_code']    : '';
        $country_code = (!empty($address['country_code'])) ? $address['country_code'] : '';

        // Preview Information
        $previewDialInNumber = Common::getDefaultDialInNumber(801,$this->getUser()->getCulture());

        // Check if Any PINs were sent to the Page
        if (!empty($preSelectedPINs)) {
            foreach ($pinPairs as $pinPair) {
                if (in_array($pinPair['chair_pin_ref'], $preSelectedPINs)) {
                    $previewChairPin  = $pinPair['chair_pin'];
                    $previewPartPin   = $pinPair['participant_pin'];
                    $previewFirstName = $pinPair['chair_first_name'];
                    $previewLastName  = $pinPair['chair_last_name'];
                    break;
                }
            }
        }

        // Check if Preview Chair PIN is not Selected.
        // Use the Initial First PIN Pair Information instead
        if (!isset($previewChairPin)) {
            reset($pinPairs);
            $pinPair = current($pinPairs);
            $previewChairPin  = $pinPair['chair_pin'];
            $previewPartPin   = isset($pinPair['participant_pin']) ? $pinPair['participant_pin'] : '';
            $previewFirstName = isset($pinPair['chair_first_name']) ? $pinPair['chair_first_name'] : '';
            $previewLastName  = isset($pinPair['chair_last_name']) ? $pinPair['chair_last_name'] : '';
        }

        // Check if the First Name and Last Name are NOT set; Otherwise use the Current Users First and Last Names.
        if (!$previewFirstName && !$previewLastName) {
            $previewFirstName = $this->getUser()->getAttribute('first_name');
            $previewLastName  = $this->getUser()->getAttribute('last_name');
        }

        return array(
            'preview' => array(
                'previewDialInNumber' => $previewDialInNumber,
                'previewChairPin'     => $previewChairPin,
                'previewPartPin'      => $previewPartPin,
                'previewFirstName'    => $previewFirstName,
                'previewLastName'     => $previewLastName,
            ),
            'address' => array(
                'address'             => $address,
                'streetAddress'       => $streetAddress,
                'organisation'        => $organisation,
                'town'                => $town,
                'county'              => $county,
                'post_code'           => $post_code,
                'country'             => $country_code
            )
        );
    }

    /**
     * Displays the Account-Details page, and the forms that allow changing of user details,
     * password and organisation details..
     *
     */
    public function executeAccountDetails()
    {
        $user       = $this->getUser();
        $contactRef = (int)$user->getAttribute('contact_ref');
        $accountId  = $user->getAttribute('account_id');
        $contact    = Common::getContactByContactRef($contactRef);

        $this->setVar('isPlusUser', $user->hasCredential('PLUS_USER'));
        $this->setVar('isPremium', $user->hasCredential('premium'));
        $this->setVar('serviceType', $user->getAttribute('service'));
        $this->setVar('titleClass', $this->getVar('serviceType'));

        // Add the contact details form, to configure the contact.
        $defaults = $this->prepareContactFormDetails($contact);
        $this->setVar('contactForm', new ContactDetailsForm($defaults, array('user' => $user)));
        $this->getVar('contactForm')->defaultEmail = $contact['email'];
        $this->setVar(
            'hasEditableEmail',
            Common::hasEditableEmail(
                $user->getAttribute('service_user'),
                $contactRef
            )
        );

        // Add the password form, to change the password.
        $this->setVar('passwordForm', new PasswordForm());
        $this->setVar('hasYuuguu', !empty($contact['hasYuuguu']) && $contact['hasYuuguu'] === 'Y');

        // Add the organisation (Plus account manipulation) form for Plus admins.
        if ($user->hasCredential('PLUS_ADMIN')) {
            $plusAccount = plusCommon::getPlusAccount($user->getAttribute('account_id'));

            // Add the organisation form, to change the organisation details.
            // Note that I'm passing the entire plus account. If the column name of the plus account does not match
            // the form element, it will not show up as a default.
            $this->setVar('organisationForm', new OrganisationDetailsForm($plusAccount));
        } else {
            if ($this->getVar('isPlusUser')) {
                // Show the account information for the Plus users.
                $this->setVar('account', plusCommon::getPlusAccount($accountId));
                $this->setVar('accountAdmin', plusCommon::getPlusAccountAdmin($accountId));
            } else if ($this->getVar('isPremium')) {
                // Show the premium details for premium users.
                $premiumContact = Common::getPremiumAccountDetailsByContactRef($contactRef);

                // Generate the premium account address and billing address.
                $premiumCompanyAddress = array();
                $premiumBillingAddress = array();

                if ($premiumContact) {
                    $companyAddressColumns = array(
                        'building',
                        'street',
                        'town',
                        'postal_code',
                        'major_town',
                        'county',
                        'country_code'
                    );
                    $billingAddressColumns = array(
                        'building',
                        'street',
                        'town',
                        'postal_code',
                        'major_town',
                        'county',
                        'country'
                    );
                    $premiumCompanyAddress = array_filter(
                        Common::retrieveArrayColumns(
                            $premiumContact['company_address'],
                            $companyAddressColumns
                        )
                    );
                    $premiumBillingAddress = array_filter(
                        Common::retrieveArrayColumns(
                            $premiumContact['billing_address'],
                            $billingAddressColumns
                        )
                    );
                }

                $this->setVar('premiumContact', $premiumContact);
                $this->setVar('premiumCompanyAddress', $premiumCompanyAddress, true);
                $this->setVar('premiumBillingAddress', $premiumBillingAddress, true);
            }
        }

        $socialMediaAccountsResponse = Hermes_Client_Rest::call(
            'Social.getOAuthUsersByContactRef',
            array('contact_ref' => $contactRef)
        );
        $socialMediaAccounts = array();
        if (count($socialMediaAccountsResponse) > 0) {
            foreach ($socialMediaAccountsResponse as $media) {
                $socialMediaAccounts[$media['server']] = $media;
            }
        }
        $this->setVar('socialMediaAccounts', $socialMediaAccounts);
    }

    /**
     * Prepares a contact for use as defaults in the contact details form.
     *
     * @param array $contact
     * @return array
     */
    protected function prepareContactFormDetails(array $contact)
    {
        $defaults            = $contact;
        $defaults['company'] = $defaults['organisation'];
        $defaults['country'] = $defaults['country_code'];
        return $defaults;
    }

    /**
     * Handles form submission of the Contact Details form on the Account-Details page.
     * @param sfWebRequest $request
     * @return string
     */
    public function executeSubmitContactDetails(sfWebRequest $request) {
        // Retrieve the form and the POST values to it.
        $form = new ContactDetailsForm();
        $contact = $request->getPostParameter('contact');
        $form->bind($contact);
        $user = $this->getUser();
        $contactRef = $user->getAttribute('contact_ref');
        $hasEditableEmail = Common::hasEditableEmail($user->getAttribute('service_user'), $contactRef);

        // Check if the form is valid.
        if ($form->isValid()) {
            // If the form is valid, update the contact.
            $currentContact = Common::getContactByContactRef($contactRef);
            $contactArguments = $this->retrieveContactArguments($contact, $currentContact, $hasEditableEmail);
            try {
                Hermes_Client_Rest::call('updateContact', $contactArguments);
            } catch (Exception $e) {
                $this->logMessage('Failed to update the contact via the Account Details page.', 'error');
                // @todo: inform the user that we failed.
            }
        } else {
            $this->getResponse()->setStatusCode(500);
            echo json_encode($form->pwn_getErrors());
        }

        return sfView::NONE;
    }

    /**
     * Retrieves the contact arguments from the POST values and the current contact.
     * @param array $postContact
     * @param array $currentContact
     * @param bool $hasEditableEmail
     * @return array
     */
    protected function retrieveContactArguments(array $postContact, array $currentContact, $hasEditableEmail) {
        $contact = array(
            'contact_ref'     => trim($currentContact['contact_ref']),
            'title'           => trim($postContact['title']),
            'first_name'      => trim($postContact['first_name']),
            'middle_initials' => trim($postContact['middle_initials']),
            'last_name'       => trim($postContact['last_name']),
            'business_phone'  => trim($postContact['business_phone']),
            'mobile_phone'    => trim($postContact['mobile_phone']),
            'country_code'    => trim($postContact['country']),
            'organisation'    => trim($postContact['company']),
            'email'           => trim($currentContact['email']),
        );

        if ($hasEditableEmail) {
            $contact['email'] = $postContact['email'];
        }

        return $contact;
    }

    /**
     * Handles execution of the password configuration form.
     * @param sfWebRequest $request
     * @return string
     */
    public function executeSubmitPassword(sfWebRequest $request) {
        $passwordForm = new PasswordForm();
        $passwordForm->bind($request->getPostParameter('authentication'));
        $errors = $passwordForm->pwn_getErrors();

        if (empty($errors['error_messages'])) {
            // If the form has no errors, update the account password.
            $updateArguments = array(
                'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                'password'    => $passwordForm['password']->getValue(),
            );
            try {
                Hermes_Client_Rest::call('updateContact', $updateArguments);
            } catch (Exception $e) {
                $this->logMessage('Failed to update the contact password via the Account Details page.', 'error');
                // @todo: inform the user that we failed.
            }
        } else {
            // Edge-case: pwn_getErrors uses getMessageFormat (why?) for confirm_password, which returns no message.
            $invalidError = array('message' => '', 'field_name' => 'authentication[confirm_password]');
            $invalidErrorKey = array_search($invalidError, $errors['error_messages']);
            if ($invalidErrorKey !== false) {
                $errors['error_messages'][$invalidErrorKey]['message'] = 'FORM_VALIDATION_NO_PASSWORD';
            }
            $this->getResponse()->setStatusCode(500);
            echo json_encode($errors);
        }

        return sfView::NONE;
    }

    /**
     * Handles execution of the organisation details form.
     * @param sfWebRequest $request
     * @return string
     */
    public function executeSubmitOrganisationDetails(sfWebRequest $request) {
        $user = $this->getUser();
        $accountId = $user->getAttribute('account_id');
        $plusAccount = plusCommon::getPlusAccount($accountId);
        $organisationForm = new OrganisationDetailsForm($plusAccount);
        $organisationForm->bind($request->getPostParameter('organisation'));
        $errors = $organisationForm->pwn_getErrors();

        if ($organisationForm->isValid() && empty($errors['error_messages'])) {
            // If the form is valid, update the plus account.
            $accountArguments = $organisationForm->getValues();
            $accountArguments['account_id'] = $accountId;

            try {
                Hermes_Client_Rest::call('updatePlusAccount', $accountArguments);
            } catch (Exception $e) {
                $this->logMessage('Failed to update the plus account via the Account Details page.', 'error');
                // @todo: inform the user that we failed.
            }
        } else {
            $this->getResponse()->setStatusCode(500);
            echo json_encode($errors);
        }

        return sfView::NONE;
    }

    /**
     * Call-Settings [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @throws Exception
     * @author Asfer Tamimi
     *
     */
    public function executeCallSettings(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user            = $this->getUser();
        $userType        = strtoupper($user->getAttribute('user_type'));
        $contactRef      = $user->getAttribute('contact_ref');
        $selectedPinRefs = $request->getGetParameter('r', array());

        $pinInformation = PinHelper::getPinInformation($userType, $contactRef, $selectedPinRefs);
        $pinPairs       = PinHelper::getContactPinPairs($contactRef, $userType);

        $powwownowCredential = $user->hasCredential('POWWOWNOW');

        // Check if there are any Editable PIN Refs
        // Highly unlikely this can happen, since you need pins to be able to login
        if (empty($pinInformation['editingRefs'])) {
            $errorMessage = 'Contact Ref: ' . $contactRef . 'tried to edit pins, but no pins were found to edit.';
            $this->logMessage($errorMessage, 'err');
            throw new Exception($errorMessage);
        }

        // Check where PIN is 1 and User Type is either Powwownow or Plus User
        if (
            count($pinInformation['usersPins']) === 1 &&
            ($powwownowCredential || $user->hasCredential('PLUS_USER'))
        ) {
            foreach ($pinInformation['usersPins'] as $pinPair) {
                $this->setVar('pin', $pinPair['chair_pin_ref']);
            }
        }

        // Form
        $form = new callSettingsForm();
        $this->setVar('fields', $form->getFormFields());
        $this->setVar('helpers', array());

        if (!$powwownowCredential) {
            $this->setVar(
                'helpers',
                array(
                    'chairman_present'         => array(
                        'title' => 'chairman_present-info',
                        'class' => 'chairman_present_info_response'
                    ),
                    'chairperson_only'         => array(
                        'title' => 'chairman_only-info',
                        'class' => 'chairman_only_response'
                    ),
                    'entry_exit_announcements' => array(
                        'title' => 'entry_exit_announcement-info',
                        'class' => 'entry_exit_announcement_response'
                    ),
                    'announcement_type'        => array(
                        'title' => 'entry_exit_announcement_type-info',
                        'class' => 'entry_exit_announcement_type_response'
                    ),
                )
            );
        }

        // Data Table
        $this->setVar(
            'dataTableOptions',
            array(
                'data'    => Common::getFilteredData($pinPairs, 'Call-Settings', $pinInformation['editingRefs']),
                'tableID' => 'tblCallSettings',
            )
        );
        $this->setVar('dataTable', array());
        $this->setVar('form', $form);

        $this->setVar('showMusicOnHold', $request->getGetParameter('music', 0));
        $this->setVar('showMusicOnHoldUserType', $user->getAttribute('service'));
        $this->setVar('powwownowCredential', $powwownowCredential);
    }

    /**
     * @desc
     *  Special action to display Error 500 page
     * @author
     *  Robert Pajak
     * @param sfWebRequest $request
     * @throws Exception
     */
    public function executeError(sfWebRequest $request) {
        throw new Exception("Expected exception from Error action was thrown");
    }

    /**
     * Sends an email to all given contacts with newly assigned products.
     *
     * @param sfWebRequest $request
     * @return string
     * @author Maarten Jacobs
     */
    public function executeNotifyUsersOfProductAssignment(sfWebRequest $request)
    {
        if (!$request->isMethod('post') || !$request->isXmlHttpRequest()) {
            $this->redirect404();
        }
        /** @var myUser $user */
        $user = $this->getUser();
        $response = new AJAXResponse($this->getResponse());
        $notifyMap = $request->getPostParameter('notify');
        $accountPins = $this->retrieveAccountPins($user->getContactRef());
        $assignedProducts = plusCommon::retrieveAssignedProducts($user->getAccountId(), $user->getCulture());
        $logFormat = 'Attempt at notifying users with newly assigned products. Error: "%s".';

        // If the notify map is not an array, then this is not one of our requests.
        if (!is_array($notifyMap)) {
            $this->logMessage(sprintf($logFormat, 'Invalid notify map given.'), 'error');
            $response->addContent('invalid_request', true)
                ->setToGenericErrorCode();
            return $response->outputResponse();
        } else {
            $notifyMap = array_filter($notifyMap);
        }

        // If no account pins were found, then Hermes is failing. So propogate the failure.
        if (!$accountPins) {
            $this->logMessage(sprintf($logFormat, 'No account pins found.'), 'err');
            $response->addContent('error_occurred', true)
                ->setToGenericErrorCode();
            return $response->outputResponse();
        }

        // Verify that all the contacts are users under the current admin.
        if (!$this->verifyContactsBelongToAccount(array_keys($notifyMap), $accountPins)) {
            $this->logMessage(sprintf($logFormat, 'The contacts do not all belong to the account.'), 'err');
            $response->addContent('invalid_request', true)
                ->setToGenericErrorCode();
            return $response->outputResponse();
        }

        // Verify that all products in the map are enabled for the current account.
        if (!$this->verifyAssignedProductsLinkedToAccount($notifyMap, $assignedProducts)) {
            $this->logMessage(sprintf($logFormat, 'The products do not all belong to the account.'), 'err');
            $response->addContent('invalid_request', true)
                ->setToGenericErrorCode();
            return $response->outputResponse();
        }

        // We;re clear: notify the users!
        $this->notifyUsersOfProductAssignment($notifyMap, $assignedProducts, $response);

        return $response->outputResponse();
    }

    /**
     * Notifies the users for whom products have been enabled.
     *
     * @param array $notifyMap
     * @param array $assignedProducts
     * @param AJAXResponse $response
     * @author Maarten Jacobs
     */
    private function notifyUsersOfProductAssignment(array $notifyMap, array $assignedProducts, AJAXResponse $response)
    {
        $failedSends = array();
        foreach ($notifyMap as $contactRef => $products) {
            $enabledProductNames = array();
            foreach ($products as $productGroupId) {
                $enabledProductNames[] = $assignedProducts[$productGroupId]['group_name'];
            }
            $contact = Common::getContactByRef(array('contact_ref' => $contactRef, 'master_service_ref' => null));
            $emailResult = $this->sendProductAssignmentEmail($contact['email'], $enabledProductNames);
            if (!$emailResult) {
                $failedSends[] = "{$contact['first_name']} {$contact['last_name']} - {$contact['email']}";
                $this->logMessage("Unable to send product assignment email to {$contact['email']}.", 'err');
                $response->setToGenericErrorCode();
            }
        }
        $response->addContent('failed_sends', $failedSends);
    }

    /**
     * Sends the product assignment email.
     *
     * @param string $email
     * @param array $enabledProductNames
     * @return bool
     * @author Maarten Jacobs
     */
    private function sendProductAssignmentEmail($email, array $enabledProductNames)
    {
        $emailTemplate = $this->getPartial('productsAssignedToUser', array('enabledProducts' => $enabledProductNames));
        $headers = implode(
            "\r\n",
            array(
                // Default from-address. Also used for DotMailer accounts.
                'From: Powwownow <info@powwownews.co.uk>',
                // Setting the content type (requires mime-type) to HTML.
                'MIME-Version: 1.0',
                'Content-type: text/html; charset=utf-8',
            )
        );
        return mail($email, 'Newly enabled products', $emailTemplate, $headers);
    }

    /**
     * Verifies that every product referenced in the notify-map is linked to the account.
     *
     * @param array $notifyMap
     * @param array $assignedProducts
     * @return bool
     * @author Maarten Jacobs
     */
    private function verifyAssignedProductsLinkedToAccount(array $notifyMap, array $assignedProducts)
    {
        foreach ($notifyMap as $productGroupIds) {
            foreach ($productGroupIds as $productGroupId) {
                if (!isset($assignedProducts[$productGroupId])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Verifies that every contact ref of the notify-map is a user of the current admin, by checking the contact refs
     * from the account pins.
     *
     * @param array $contactRefs
     * @param array $accountPins
     * @return bool
     * @author Maarten Jacobs
     */
    private function verifyContactsBelongToAccount(array $contactRefs, array $accountPins)
    {
        // Collect the account contact refs from the pins.
        $accountContactRefs = array();
        foreach ($accountPins as $pin) {
            $accountContactRefs[$pin['contact_ref']] = true;
        }

        // For every contact ref, check if the contact ref is a key in the account contact ref map.
        // If it isn't, then the test has failed.
        foreach ($contactRefs as $contactRef) {
            if (!isset($accountContactRefs[$contactRef])) {
                return false;
            }
        }

        // No incorrect contact refs -> pass.
        return true;
    }

    /**
     * Retrieves all pins linked to the account, via the contact ref.
     *
     * @param int $contactRef
     * @return array
     * @author Maarten Jacobs
     */
    private function retrieveAccountPins($contactRef)
    {
        try {
            $contactPins = Hermes_Client_Rest::call(
                'getAccountPinsByContactRef',
                array(
                    'contact_ref' => $contactRef,
                )
            );
        } catch (Exception $e) {
            $this->logMessage("Error whilst calling Default.getAccountPinsByContactRef: '" . $e->getMessage() . "'", 'err');
            return array();
        }
        return $contactPins;
    }

    /**
     * User-Details [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeUserDetails(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        // Check if the Contact is Editable
        $this->setVar('permissionDenied', $this->getIsContactEditable($user, $request));

        if (!$this->getVar('permissionDenied')) {

            // Obtain the Contact Ref from the URL. If it does not exist, get the Contact Ref from the Session.
            $this->setVar('contactRef', (int)$request->getParameter('contact_ref', $user->getAttribute('contact_ref')));
            $contact = Common::getContactByContactRef($this->getVar('contactRef'));

            // User Details Form
            $defaults                        = $this->prepareContactFormDetails($contact);
            $contactForm               = new ContactDetailsForm($defaults, array('user' => $user));
            $contactForm->defaultEmail = $contact['email'];
            $this->setVar('contactForm', $contactForm);

            $this->setVar('hasEditableEmail', Common::hasEditableEmail(
                $user->getAttribute('service_user'),
                $this->getVar('contactRef')
            ));

            // Password Form
            $this->setVar('passwordForm', new PasswordForm());

            // Yuuguu Check
            $this->setVar('hasYuuguu', !empty($contact['hasYuuguu']) && $contact['hasYuuguu'] === 'Y');

            $this->setVar('service', $user->getAttribute('service'));
        }
    }

    /**
     * User-Details Contact Form [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeUserDetailsContactAjax(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        /** @var myUser $user */
        $user = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        // Check if the Contact is Editable
        $permissionDenied = $this->getIsContactEditable($user, $request);

        if (!$permissionDenied) {

            // Contact Form
            $form    = new ContactDetailsForm();
            $contact = $request->getPostParameter('contact');
            $form->bind($contact);

            // Obtain the Contact Ref from the URL. If it does not exist, get the Contact Ref from the Session.
            $contactRef       = (int)$request->getPostParameter('contact_ref', $user->getAttribute('contact_ref'));
            $hasEditableEmail = Common::hasEditableEmail($user->getAttribute('service_user'), $contactRef);

            // Check if the form is valid.
            if ($form->isValid()) {
                $currentContact   = Common::getContactByContactRef($contactRef);
                $contactArguments = $this->retrieveContactArguments($contact, $currentContact, $hasEditableEmail);
                $result           = Common::updateContact($contactArguments);
                if (empty($result)) {
                    $response->setStatusCode(500);
                    echo json_encode(
                        array(
                            'error_message' => 'FORM_UPDATE_CONTACT_ERROR',
                            'field_name'    => 'alert'
                        )
                    );
                } else {
                    echo json_encode(
                        array(
                            'message'    => 'FORM_VALIDATION_USER_DETAILS_CONTACT_SUCCESS',
                            'field_name' => 'alert'
                        )
                    );
                }
            } else {
                $response->setStatusCode(500);
                echo json_encode($form->pwn_getErrors());
            }
        } else {
            $response->setStatusCode(500);
            echo json_encode(
                array(
                    'error_message' => 'FORM_UPDATE_CONTACT_ERROR',
                    'field_name'    => 'alert'
                )
            );
        }
        return sfView::NONE;
    }

    /**
     * User-Details Password Form [AJAX]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeUserDetailsPasswordAjax(sfWebRequest $request)
    {
        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        /** @var myUser $user */
        $user = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        // Check if the Contact is Editable
        $permissionDenied = $this->getIsContactEditable($user, $request);

        if (!$permissionDenied) {

            // Password Form
            $passwordForm   = new PasswordForm();
            $authentication = $request->getPostParameter('authentication');
            $passwordForm->bind($authentication);

            $errors = $passwordForm->pwn_getErrors();

            // Check for Errors
            if (empty($errors['error_messages'])) {
                $result = Common::updateContact(
                    array(
                        'contact_ref' => $request->getPostParameter('contact_ref', $user->getAttribute('contact_ref')),
                        'password'    => $passwordForm['password']->getValue(),
                    )
                );
                if (empty($result)) {
                    $response->setStatusCode(500);
                    echo json_encode(
                        array(
                            'error_message' => 'PASSWORD_NOT_CHANGED',
                            'field_name'    => 'alert'
                        )
                    );
                } else {
                    echo json_encode(
                        array(
                            'message'    => 'FORM_VALIDATION_USER_DETAILS_PASSWORD_SUCCESS',
                            'field_name' => 'alert'
                        )
                    );
                }
            } else {

                // Edge-case: pwn_getErrors uses getMessageFormat (why?) for confirm_password, which returns no message.
                $invalidError    = array('message' => '', 'field_name' => 'authentication[confirm_password]');
                $invalidErrorKey = array_search($invalidError, $errors['error_messages']);
                if ($invalidErrorKey !== false) {
                    $errors['error_messages'][$invalidErrorKey]['message'] = 'FORM_VALIDATION_NO_PASSWORD';
                }
                $response->setStatusCode(500);
                echo json_encode($errors);
            }
        } else {
            $response->setStatusCode(500);
            echo json_encode(
                array(
                    'error_message' => 'PASSWORD_NOT_CHANGED',
                    'field_name'    => 'alert'
                )
            );
        }
        return sfView::NONE;
    }

    /**
     * Check if the Contact is Editable and Belongs to the Admin Account
     * @param myUser $user
     * @param sfWebRequest $request
     * @return bool
     * @author Asfer Tamimi
     */
    private function getIsContactEditable(myUser $user, sfWebRequest $request)
    {
        $contactRef       = (int)$request->getParameter('contact_ref', $user->getAttribute('contact_ref'));
        $userContactRef   = (int)$user->getAttribute('contact_ref');
        $permissionDenied = true;

        if ($contactRef !== $userContactRef) {
            // Find if the Contact Ref is within the current Account
            $result = PinHelper::getContactPinPairs($userContactRef, 'ADMIN', true);
            foreach ($result as $pinInfo) {
                if ($pinInfo['chair_contact_ref'] == $contactRef) {
                    $permissionDenied = false;
                    break;
                }
            }
        } else {
            $permissionDenied = false;
        }

        return $permissionDenied;
    }

    public function executeCx2H1Page()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));

        $user = $this->getUser();
        $pins = PinHelper::getDefaultPins($user->getAttribute('contact_ref'));

        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $user->getCulture(),
                'service_ref'    => $user->getAttribute('service_ref'),
                'country'        => 'GBR',
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );

        $this->setVar('pin', $pins['pin']);
        $this->setVar('email', $user->getAttribute('email'));
        $this->setVar('dialInNumber', $dialInNumbers['local']);
        $this->setVar('mobileDialInNumber', $dialInNumbers['mobile']);
    }

    /**
     * @desc
     *  This Action is a directy copy paste (include setting in view.yml) of executeIndex in the
     *  controller. The only change here is the template.
     * @param sfWebRequest $request
     */
    public function executeCx2H2Page(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));

        $user = $this->getUser();
        $pins = PinHelper::getDefaultPins($user->getAttribute('contact_ref'));

        $this->pin = $pins['pin'];
        $this->email = $user->getAttribute('email');
        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $user->getCulture(),
                'service_ref'    => $user->getAttribute('service_ref'),
                'country'        => 'GBR',
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );

        $bundleDialogs = sfConfig::get('app_bundle_dialog_default');
        $this->individualBundleDialog = $bundleDialogs['individual_bundle'];

        // Product Selector Tool
        $result = Common::getBundlesForProductSelector();
        $this->bundles = array_merge($result,array('showBuyButtons' => true));
        $this->isPlusAdmin = $user->hasCredential('PLUS_ADMIN');
        $this->isEnhancedUser = $user->hasCredential('POWWOWNOW');

        if ($this->isEnhancedUser) {
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Partial'));
            $this->plusTemplates = array(
                'plusFormWithPassword1'   => get_partial('commonComponents/plusFormWithPassword1',array(
                    'email'      => $user->getAttribute('email'),
                    'first_name' => $user->getAttribute('first_name'),
                    'last_name'  => $user->getAttribute('last_name')
                )),
                'plusRedirectionExisting' => get_partial('commonComponents/plusRedirectionExisting',array()),
                'plusNoSwitch1'           => get_partial('commonComponents/plusNoSwitch1',array()),
            );
        }
    }
}
