<main class="container_16 clearfix">
    <section class="grid_8 left generic_section">
        <div class="recording_blue_container">
            <p class="title">We're sorry...</p>
            <p class="sub_title">This recording has expired.</p>
            <p class="body">
                <strong>Quick Fix:</strong> Try contacting your conference organiser for a copy of the call.
                    We can only store calls for up to 6 months.
            </p>
            <div class="find_out_more">
                <p class="body">
                    Now that you've got a moment, why not learn more about what we do?
                </p>
                <a href="<?php echo url_for('@landing_page_free_conference_call_1');?>" class="blue_big_button">Find out more</a>
                <br class="clear" />
            </div>
        </div>
    </section>
    <section class="grid_8 right generic_section">
        <img src="/cx2/img/recording/woman.png" />
    </section>
</main>