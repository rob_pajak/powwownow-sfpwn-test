<h6># = Skip Intro</h6>
<p>During the welcome message, pressing # will skip the PIN playback. Pressing # again will skip the name recording and place you on the call. Please note that if you skip name recording, when a roll call is played, the name will be played as "Participant X" X being your Participant number, e.g. "Participant 5".</p>

<h6>#1 = Head Count</h6>
<p>This will allow you to review the number of people on the conference call.</p>

<h6>## = Mute All</h6>
<p>Enables the Chairperson to mute/un-mute all their Participants.</p>

<h6>#2 = Roll Call</h6><p>This is a replay of all names recorded when people arrived on the conference call. All Participants will hear the number of people and the roll call if chosen so in PIN settings.</p>

<h6>#6 = Mute</h6>
<p>By pressing #6, an individual can mute and un-mute his/her handset. This is very useful if you are in a noisy location. Muting means that you can hear the rest of the conference but the other Participants cannot hear anything from your handset.</p>

<h6>#3 = Lock</h6>
<p>This allows you to lock and unlock a conference call. Locking a conference call stops anyone else from joining it, giving Participants peace of mind if sensitive information is being discussed and preventing unnecessary interruptions.</p>

<h6>#7 = Private Roll Call</h6>
<p>Allows the Chairperson to hear who is on the call without Participants hearing.</p>

<h6>#9 = Private Head Count</h6><p>Allows the Chairperson to hear how many people are on the call without the Participants hearing.</p>

<h6>#8 = Record</h6>
<p>This allows you to record a conference. To start the recording, press #8. (You will be asked to confirm this by pressing 1). To stop and save the recording, press #8 again and confirm or just hang up the phone.</p><p>To retrieve your saved recordings go to &lsquo;Recordings&rsquo; under &lsquo;My Conference Tools&rsquo; tab. Your saved recordings will appear a few minutes after your call ends, easily identified by the conference date.<br /><br />There your recordings can be played, published or shared with whoever you wish. We&rsquo;ll hold recordings for 60 days, or up to 6 months if published.</p><br/><br/>

<button class="button-green-schedule" onclick="window.open('/sfpdf/en/Powwownow-User-Guide.pdf');">
    DOWNLOAD PDF
</button>

