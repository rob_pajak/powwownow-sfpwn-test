<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Call History'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)); ?>
    <div class="grid_24 clearfix">
        <?php echo form_tag(url_for('@call_history_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-callhistory', 'class' => 'pwnform clearfix')); ?>
            <div class="grid_sub_24 clearfix">
                <?php $count = 1; ?>
                <?php foreach ($fields as $field) : ?>
                    <?php include_partial('callHistoryFormField', array(
                            'field'       => $field, 
                            'form'        => $callHistoryForm, 
                            'count'       => $count
                        )); ?>
                    <?php $count = (2==$count) ? 1 : 2 ?>
                <?php endforeach; ?>
            </div>
            <div class="form-action grid_sub_24 clearfix">
                <button id="frm-callhistory-submit" name="frm-callhistory-submit" class="button-green" type="submit" value="Main-Report">
                    <?php echo __('Run Report'); ?>
                </button>
            </div>
        </form>

        <div class="grid_sub_24 clearfix div-callhistory">
            <?php include_component('mypwn', 'createDataTable', array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)); ?>
        </div>

        <div class="form-action grid_sub_24 clearfix">
            <button id="frm-callhistory-csv" name="frm-callhistory-csv" class="button-green downloadcsv floatright" type="submit" value="Download-Report">
                <?php echo __('Download Report'); ?>
            </button>
        </div>

        <script>
            $(document).ready(function () {
                callHistoryFormInitialise('#frm-callhistory');
                callHistoryDateInitialise('#frm-callhistory');
            });
        </script>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
