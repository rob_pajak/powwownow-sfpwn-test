<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => 'Create PIN', 'headingSize' => 'm', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24 clearfix">
        <?php echo form_tag(
            url_for('create_pin_ajax'),
            array(
                'enctype' => 'application/x-www-form-urlencoded',
                'id'      => 'form-create-pin',
                'class'   => 'pwnform clearfix'
            )
        ); ?>
            <div class="grid_sub_24 clearfix" id="existing_user">
                <?php echo $form['contact_ref']->renderLabel(); ?>
                <span class="mypwn-input-container">
                        <?php echo $form['contact_ref']->render(
                            array(
                                'class' => 'input-large font-small mypwn-input',
                                'onchange' => 'createPinFormToggle(this);'
                            )
                        ); ?>
                    </span>
            </div>
            <div class="grid_sub_24 clearfix" id="new_user_options">
                <?php $count = 1; ?>
                <?php foreach ($fields as $field) : ?>
                    <?php include_partial(
                        'createPinFormField',
                        array(
                            'field' => $field,
                            'form'  => $form,
                            'count' => $count
                        )
                    ); ?>
                    <?php $count = (2 == $count) ? 1 : 2 ?>
                <?php endforeach; ?>
            </div>
            <div class="form-action grid_sub_24 clearfix">
                <p>* Required fields</p>
                <button id="form-create-pin-submit" class="button-green" type="submit">
                    <?php echo 'Create PIN'; ?>
                </button>
            </div>
        </form>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>

<style>

    .error_message {
        position: absolute;
        background: #ffffff;
        border: 2px solid #ff0000;
        right: -225px;
        width: 210px;
        top: -2px;
        padding: 3px;
        border-radius: 5px;
        display: inline-block;
    }

    .error_message:after, .error_message:before {
        right: 100%;
        top: 50%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
    }

    .error_message:after {
        border-color: rgba(255, 255, 255, 0);
        border-right-color: #ffffff;
        border-width: 5px;
        margin-top: -5px;
    }

    .error_message:before {
        border-color: rgba(255, 0, 0, 0);
        border-right-color: #ff0000;
        border-width: 8px;
        margin-top: -8px;
    }

    .error_message .close {
        top: -7px;
        right: -6px;
        border-radius: 20px;
        background: #7f7f7f;
        width: 15px;
        height: 15px;
        line-height: 15px;
        color: #FFFFFF;
        font-size: 11px;
        text-align: center;
    }

    .success_message {
        border: 1px solid #97c000;
        background-color: #FFFFFF;
        display: block;
        position: relative;
        margin: 0 auto;
        width: 80%;
        padding: 5px;
        border-radius: 5px;
        text-align: center;
    }
</style>