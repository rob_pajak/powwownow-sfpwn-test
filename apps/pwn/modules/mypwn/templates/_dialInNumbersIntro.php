<?php if (sfContext::getInstance()->getUser()->hasCredential('POWWOWNOW')) : ?>
    <p><?php echo __('Now even more international callers can dial into your conference calls at much lower rates. Simply locate your caller&rsquo;s country below and send them the corresponding number for your next international conference call.'); ?></p>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_USER')) : ?>
    <p><?php echo __('Simply locate your caller’s country below and send them the corresponding number for your next international conference call.');?></p>
    <?php if (!$isPostPayCustomer): ?>
        <p><?php echo __('Low-call rate <strong>Shared Cost</strong> numbers are automatically enabled on your account; Pay As You Go Freephone and Landline numbers are managed by your Administrator.') ?></p>
        <p><?php echo __('Please note that there needs to be enough available credit on your account for you and your Participants to join a conference call using a Freephone or Landline number. The balance of your account is displayed on the top right-hand corner of this page. Contact your Administrator to purchase PAYG Call Credit.') ?></p>
    <?php elseif (!$hasInternationalAddOn): ?>
        <p><?php echo __("Don't forget that you have a UK Landline Bundle enabled on your account!"); ?></p>
        <p><?php echo __("Calls made using Freephone and Shared Cost numbers will not be included in this Bundle allowance and will be charged separately at the start of the following month. Visit the %my_products_link% page to see full details of your purchased Bundle.", array('%my_products_link%' => link_to('My Products', '@my_products'))); ?></p>
        <p><?php echo __('Worried about how many minutes you have left? Easily keep track with the usage clock on the top right-hand corner of this page.') ?></p>
    <?php else: ?>
        <p><?php echo __("Don't forget that you have a UK Landline Bundle with an International Landline Add-on enabled on your account!"); ?></p>
        <p><?php echo __("Calls made using Freephone and Shared Cost numbers will not be included in this allowance and will be charged separately at the end of the month. Visit the %my_products_link% page to see full details of your purchased Bundle.", array('%my_products_link%' => link_to('My Products', '@my_products'))); ?></p>
        <p><?php echo __('Worried about how many minutes you have left? Easily keep track with the usage clock on the top right-hand corner of this page.') ?></p>
    <?php endif; ?>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_ADMIN')) : ?>
    <p><?php echo __('Simply locate your caller’s country below and send them the corresponding number for your next international conference call.'); ?></p>
    <?php if (!$isPostPayCustomer): ?>
        <p><?php echo __('Low-call rate <strong>Shared Cost</strong> numbers are automatically enable on your account. Pay As You Go <strong>Freephone</strong> and <strong>Landline</strong> numbers are also automatically enabled, these numbers are automatically enabled for account users, these can be disabled for any users via the %products_assign_link% page.', array('%products_assign_link%' => link_to('Assign Products', '@products_assign'))) ?></p>
        <p><?php echo __('Please note that when using a Freephone or Landline number, there needs to be enough available PAYG Call Credit on your account for you and your Participants to join a conference call. The balance of your account is displayed on the top right-hand corner of this page. Need to Top-up? %products_select_link%.', array('%products_select_link%' => link_to('Purchase credit here', '@products_select'))); ?></p>
    <?php elseif (!$hasInternationalAddOn): ?>
        <p><?php echo __("Don't forget that you have a UK Landline Bundle enabled on your account!"); ?></p>
        <p><?php echo __("Calls made using Freephone and Shared Cost numbers will not be included in this Bundle allowance and will be charged separately at the start of the following month. Visit the %my_products_link% page to see full details of your purchased Bundle.", array('%my_products_link%' => link_to('My Products', '@my_products'))); ?></p>
        <p><?php echo __('Worried about how many minutes you have left? Easily keep track with the usage clock on the top right-hand corner of this page.') ?></p>
    <?php else: ?>
        <p><?php echo __("Don't forget that you have a UK Landline Bundle with an International Landline Add-on enabled on your account!"); ?></p>
        <p><?php echo __("Calls made using Freephone and Shared Cost numbers will not be included in this allowance and will be charged separately at the end of the month. Visit the %my_products_link% page to see full details of your purchased Bundle.", array('%my_products_link%' => link_to('My Products', '@my_products'))); ?></p>
        <p><?php echo __('Worried about how many minutes you have left? Easily keep track with the usage clock on the top right-hand corner of this page.') ?></p>
    <?php endif; ?>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_USER')) : ?>
    <p><?php echo __('Now even more international callers can dial into your conference calls at much lower rates. Simply locate your caller&rsquo;s country below and send them the corresponding number for your next international conference call.'); ?></p>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_ADMIN')) : ?>
    <p><?php echo __('Now even more international callers can dial into your conference calls at much lower rates. Simply locate your caller&rsquo;s country below and send them the corresponding number for your next international conference call.'); ?></p>
<?php endif; ?>