Please join my conference call.

Your dial-in number is: <?php echo $dial_in_number; ?> (International dial-in numbers: http://www.powwownow.co.uk/sfpdf/en/Powwownow-Dial-in-Numbers.pdf)
When dialling-in to conference calls using a mobile phone, use our mobile shortcode to help save you money: Dial 87373 and enter your conference PIN as usual to join your conference.
Your PIN is: <?php echo $pin; ?>

Time of call: [TYPE DAY AND TIME OF CALL]

Instructions:
1. Dial the conference number at the time indicated above
2. Enter the PIN, when prompted
3. Speak your full name when prompted and then you will join the conference. If you are the first person to arrive on the conference call, you will be placed on hold until the next person joins. 

Find out how much easier your life could be with Powwownow's free conference calling service. Powwownow - get together whenever!
http://www.powwownow.co.uk