<p><?php 
switch ($service) {
    case 'powwownow':
        if ($walletCardOrdered) {
            echo __('If you have not received your welcome pack or have lost your welcome pack and would like to have a replacement sent, please confirm your details below and resubmit. Please note: We can only issue a maximum of three welcome packs per person.');
        } else {
            echo __('<strong>As a registered user, you\'re entitled to a free welcome pack that contains a personalised wallet card and stickers for your computer and phone, printed with your personal dial-in numbers and PIN. Delivery is free, and we\'ll post your pack within 2 working days!</strong>');
        }
        break;
    case 'plus':
        if ($walletCardOrdered) {
            echo __('If you have not received your welcome pack or have lost your welcome pack and would like to have a replacement sent, please confirm your details below and resubmit. Please note: We can only issue a maximum of three welcome packs per person.');
        } else {
            echo __('As a registered user, you\'re entitled to a free welcome pack that contains a personalised wallet card and stickers for your computer and phone with your personal dial-in numbers and PIN. Delivery is free and we\'ll post your pack within 2 working days!');
        }
        break;
    case 'premium':
        if ($walletCardOrdered) {
            echo __('If you have not received your welcome pack or have lost your welcome pack and would like to have a replacement sent, please confirm your details below and resubmit. Please note: We can only issue a maximum of three welcome packs per person.');
        } else {
            echo __('<strong>As a registered user, you\'re entitled to a free welcome pack that contains a personalised wallet card and stickers for your computer and phone, printed with your personal dial-in numbers and PIN. Delivery is free, and we\'ll post your pack within 2 working days!</strong>');
        }
        break;
}
?></p>
