<?php if (sfContext::getInstance()->getUser()->hasCredential('POWWOWNOW')) : ?>
    <div class="grid_sub_24 clearfix" style="padding-bottom:40px;">
        <p><?php echo __('Your PIN is:'); ?></p>
        <p class="single-pin" style="font-size:50px; padding:0; margin:0; color:#F38100;"><?php echo $pin; ?></p>
        <a onclick="parent.location='<?php include_component('mypwn','shareDetailsEmail',array(
            'subject'        => 'Powwownow conference call details',
            'pin'            => $pin,
            'dial_in_number' => $dialInNumber
            )); ?>'; return false;">
            <button class="button-orange" id="share-details"><?php echo __('SHARE DETAILS'); ?></button>
        </a>        
    </div>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_USER')) : ?>
    <div class="grid_sub_24 clearfix" style="padding-bottom:20px;">
        <p><?php echo __('Your Chairperson PIN is:'); ?></p>
        <p class="single-pin" style="font-size:50px; padding:0; margin:0; color:#F38100;"><?php echo $chair_pin; ?></p>
        <p><?php echo __('Your Participant PIN is:'); ?></p>
        <p class="single-pin" style="font-size:50px; padding:0; margin:0; color:#F38100;"><?php echo $part_pin; ?></p>
        <a onclick="parent.location='<?php include_component('mypwn','shareDetailsEmail',array(
            'subject' => 'Powwownow conference call details',
            'pin'     => $part_pin,
            )); ?>';">
            <button class="button-orange" id="share-details"><?php echo __('SHARE DETAILS'); ?></button>
        </a>
    </div>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_ADMIN')) : ?>
    <div class="grid_sub_24 clearfix">
        <?php include_component('mypwn', 'createDataTable', array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)); ?>
    </div>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_USER')) : ?>
    <div class="grid_sub_24 clearfix">
        <?php include_component('mypwn', 'createDataTable', array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)); ?>
    </div>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_ADMIN')) : ?>
    <div class="grid_sub_24 clearfix">
        <?php include_component('mypwn', 'createDataTable', array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)); ?>
    </div>
<?php endif; ?>