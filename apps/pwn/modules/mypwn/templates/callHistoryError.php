<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Call History'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)); ?>
    <div class="grid_24 clearfix">
        <p><?php echo __('You have no Call History Records.'); ?></p>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
