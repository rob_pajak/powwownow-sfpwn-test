<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('Recordings'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24 clearfix">
        <p><?php echo __('You can start recording a conference call at any time. All you need to do is press #8 on your telephone keypad (you will be asked to confirm this by pressing 1). To stop and save the recording just press #8 again or hang up the phone.'); ?></p>
        <p><?php echo __('Your saved recordings will appear below a few minutes after your call ends, identified by the conference date and time. Here you can play the recordings or publish and share them with whoever you wish. We\'ll hold recordings for 60 days, or up to 6 months if published.'); ?></p>
        <?php if ($isAdmin) : ?>
            <?php echo form_tag(
                url_for('recordings_ajax'),
                array(
                    'enctype' => 'application/x-www-form-urlencoded',
                    'id'      => 'frm-recordings',
                    'class'   => 'pwnform clearfix'
                )
            ); ?>
                <div class="grid_sub_24 clearfix">
                    <?php $count = 1; ?>
                    <?php foreach ($fields as $field) : ?>
                        <?php include_partial(
                            'recordingsFormField',
                            array(
                                'field' => $field,
                                'form'  => $recordingsForm,
                                'count' => $count
                            )
                        ); ?>
                        <?php $count = (2 == $count) ? 1 : 2 ?>
                    <?php endforeach; ?>
                </div>
                <div class="form-action grid_sub_24 clearfix">
                    <p>* Required fields</p>
                    <button id="frm-recordings-submit" class="button-green" type="submit">
                        <span><?php echo __('Run Report'); ?></span>
                    </button>
                </div>
            </form>
            <script>
                $(document).ready(function () {
                    recordingsFormInitialise('#frm-recordings');
                });
            </script>
        <?php endif; ?>
        <script>
            $(document).ready(function () {
                recordingsDateInitialise('#frm-recordings');
                $(document).bind('draw.datatables', function () {
                    $('audio').mediaelementplayer();
                });
            });
        </script>
        <div class="grid_sub_24 clearfix divrecordings margin-top-10">
            <?php include_component(
                'mypwn',
                'createDataTable',
                array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)
            ); ?>
            <?php if ($showInactivePinMessage): ?>
                <p><sup class="deactivated-pin" title="Deactivated pin">1</sup> indicates deactivated pins.</p>
            <?php endif; ?>
        </div>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
