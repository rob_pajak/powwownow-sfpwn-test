<div class="form-field grid_sub_12 <?php echo (1==$count) ? 'alpha' : 'omega'; ?>">
    <?php echo $form[$field]->renderLabel(); ?>
    <span class="mypwn-input-container">
        <?php if ('show_all_users' == $field) : ?>
        	<?php echo $form[$field]->render(array('class' => 'font-small mypwn-input')); ?>
        <?php else: ?>
        	<?php echo $form[$field]->render(array('class' => 'input-large font-small mypwn-input')); ?>
        <?php endif; ?>
    </span>
</div>