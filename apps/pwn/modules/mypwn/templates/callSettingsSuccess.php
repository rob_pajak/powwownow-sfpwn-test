<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('Call Settings'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)
    ); ?>

    <div class="grid_24 clearfix">
        <?php echo form_tag(
            url_for('call_settings_ajax'),
            array(
                'enctype' => 'application/x-www-form-urlencoded',
                'id' => 'frm-call-settings',
                'class' => 'pwnform clearfix'
            )
        ); ?>
            <div class="grid_sub_24 clearfix">
                <?php if (!$powwownowCredential) : ?>
                    <script>
                        (function($) {
                            'use strict';
                            $(function() {
                                callSettingsFormInit('#frm-call-settings', true);
                                callSettingsSetupMusicOnHold($('select#callSettings_on_hold_music').val(), false);
                            });
                        })(jQuery);
                    </script>
                    <?php include_component(
                        'mypwn',
                        'createDataTable',
                        array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)
                    ); ?>
                <?php else : ?>
                    <script>
                        (function($) {
                            'use strict';
                            $(function() {
                                callSettingsFormInit('#frm-call-settings', false);
                                callSettingsSetupMusicOnHold($('select#callSettings_on_hold_music').val(), false);
                            });
                        })(jQuery);
                    </script>
                    <input type="hidden" id="callSettings_pin_ref" name="callSettings[pin_ref][]" value="<?php echo $pin; ?>"/>
                <?php endif; ?>
            </div>
            <div class="grid_sub_24 clearfix">
                <?php $count = 1; ?>
                <?php foreach ($fields as $field) : ?>
                    <?php if ($field === 'pin_ref') continue; ?>
                    <?php include_partial('callSettingsFormField', array(
                        'field'       => $field,
                        'form'        => $form,
                        'helperTitle' => (isset($helpers[$field])) ? $helpers[$field]['title'] : false,
                        'clear'       => (1===$count) ? false : true,
                        'hasPowwownowCredential' => $powwownowCredential,
                        'showMusicOnHold' => $showMusicOnHold,
                        'showMusicOnHoldUserType' => $showMusicOnHoldUserType,
                    )); ?>
                    <?php $count = (2==$count) ? 1 : 2 ?>
                <?php endforeach; ?>
                <div class="grid_24 clearfix">
                    <div class="form-action">
                        <p>* Required fields</p>
                        <button class="button-green" type="submit">UPDATE CALL SETTINGS</button>
                    </div>
                </div>
                <?php foreach ($helpers as $id => $field) : ?>
                    <?php include_partial('callSettingsInformationHelper', array(
                        'text'  => get_partial('mypwn/callSettingsTitleHelper',array('field' => $id)),
                        'class' => $field['class']
                    )); ?>
                <?php endforeach; ?>
            </div>
        </form>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
