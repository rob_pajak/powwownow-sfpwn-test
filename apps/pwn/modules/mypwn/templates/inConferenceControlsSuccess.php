<?php include_component('commonComponents', 'header', array('showButton' => false)); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('In-Conference Controls'), 'headingSize' => 'l', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24 clearfix">
        <div class="grid_sub_24 clearfix margin-bottom-10">
            <p><strong><?php echo __('During a conference the following control keys are available:'); ?></strong></p>
        </div>
        <div class="grid_sub_24 clearfix margin-bottom-10">
            <div class="inconferencecontrols">
                <?php switch ($service):
                    case 'powwownow':
                        include_partial('inConferenceControlsFreePartial');
                        break;
                    case 'plus':
                        include_partial('inConferenceControlsPlusPartial');
                        break;
                    case 'premium':
                        include_partial('inConferenceControlsPremiumPartial');
                        break;
                endswitch; ?>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
