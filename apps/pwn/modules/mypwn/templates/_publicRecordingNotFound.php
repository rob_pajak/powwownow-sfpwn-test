<main class="container_16 clearfix">
    <section class="grid_8 left generic_section">
        <div class="recording_blue_container">
            <p class="title">Oops...</p>
            <p class="sub_title">This recording isn't available online</p>
            <p class="body">
                <strong>Quick Fix:</strong> Get in touch with the conference organiser to make sure you don't miss out
            </p>
            <div class="find_out_more">
                <p class="body">
                    Now that you've got a moment, why not learn more about what we do?
                </p>
                <a href="<?php echo url_for('@landing_page_free_conference_call_1');?>" class="blue_big_button">Find out more</a>
                <br class="clear" />
            </div>
        </div>
    </section>
    <section class="grid_8 right generic_section">
        <img src="/cx2/img/recording/woman.png" />
    </section>
</main>