<div class="wallet_card_preview wallet_card_preview_<?php echo $service; ?>">
    <div id="walletCardPreview_line_name">
        <?php if ($service == 'powwownow') : ?>Name:&nbsp;<span><?php endif; ?>
        <?php echo htmlentities($previewFirstName) . (!empty($previewFirstName) && !empty($previewLastName) ? ' ' : '') . htmlentities($previewLastName); ?>
        <?php if ($service == 'powwownow') : ?></span><?php endif; ?>
    </div>
    <?php if ($plus || $premium) : ?>
        <?php if ($previewChairPin) : ?>
            <div id="walletCardPreview_line_chairPin">Chairperson PIN:<span id="walletCardPreview_chairPin"><?php echo $previewChairPin; ?></span></div>
        <?php endif; ?>
        <?php if ($previewParticipantPin) : ?>
            <div id="walletCardPreview_line_participantPin">Participant PIN:<span id="walletCardPreview_participantPin"><?php echo $previewParticipantPin; ?></span></div>
        <?php endif; ?>
        <div id="walletCardPreview_line_number">UK Landline use:<span id="walletCardPreview_number"><?php echo $previewDefaultDialInNumber; ?></span></div>
        <?php if ($premium) : ?>
            <div id="walletCardPreview_line_url">myPowwownow:<span id="walletCardPreview_url">www.powwownow.co.uk/login</span></div>
        <?php endif; ?>
    <?php else : ?>
        <div id="walletCardPreview_line_number">Dial-in Number:<span id="walletCardPreview_number"><?php echo $previewDefaultDialInNumber; ?></span></div>
        <div id="walletCardPreview_line_chairPin">PIN:<span id="walletCardPreview_chairPin"><?php echo $previewChairPin; ?></span></div>
    <?php endif; ?>
</div>
<div class="wallet_card_back_preview wallet_card_back_preview_<?php echo $service; ?>"></div>
