<main class="container_16 clearfix">
    <section>
        <div class="recording-header">
            <p class="title_line">Here's your</p>
            <p class="title_line sketch">FREE</p>
            <p class="title_line">recording</p>
        </div>
    </section>
    <section class="container_16 clearfix">
        <section class="grid_7 left generic_section">
            <div class="recording_blue_trapezoid recording_box">
                <p class="title">Recording</p>
                <?php if (!empty($recording['description'])):?>
                    <p class="description"><?php echo $recording['description']?></p>
                <?php else: ?>
                    <p class="id">#<?php echo $recording['recording_ref']?></p>
                <?php endif;?>
                <p class="body">
                    Listen to your recording below or save a copy for later if you prefer.
                </p>
                <p class="body-small">
                    Make sure to download or listen before it
                    <strong>expires on <?php echo $datePublish;?></strong>
                </p>
                <div class="sm2-bar-ui compact full-width">
                    <div class="bd sm2-main-controls">
                        <div class="sm2-inline-texture"></div>
                        <div class="sm2-inline-gradient"></div>
                        <div class="sm2-inline-element sm2-button-element">
                            <div class="sm2-button-bd">
                                <a href="#play" class="sm2-inline-button play-pause">Play / pause</a>
                            </div>
                        </div>
                        <div class="sm2-inline-element sm2-inline-status">
                            <div class="sm2-playlist">
                                <div class="sm2-playlist-target">
                                    <noscript><p>JavaScript is required.</p></noscript>
                                </div>
                            </div>
                            <div class="sm2-progress">
                                <div class="sm2-row">
                                    <div class="sm2-inline-time">0:00</div>
                                    <div class="sm2-progress-bd">
                                        <div class="sm2-progress-track">
                                            <div class="sm2-progress-bar"></div>
                                            <div class="sm2-progress-ball"><div class="icon-overlay"></div></div>
                                        </div>
                                    </div>
                                    <div class="sm2-inline-duration">0:00</div>
                                </div>
                            </div>
                        </div>
                        <div class="sm2-inline-element sm2-button-element sm2-volume">
                            <div class="sm2-button-bd">
                                <span class="sm2-inline-button sm2-volume-control volume-shade"></span>
                                <a href="#volume" class="sm2-inline-button sm2-volume-control">volume</a>
                            </div>
                        </div>
                    </div>
                    <div class="bd sm2-playlist-drawer sm2-element">
                        <div class="sm2-inline-texture">
                            <div class="sm2-box-shadow"></div>
                        </div>
                        <!-- playlist content is mirrored here -->
                        <div class="sm2-playlist-wrapper">
                            <ul class="sm2-playlist-bd">
                                <li><a href="/Recordings/recording-download?recording_ref=<?php echo $recording['recording_ref']?>&action=listen&autostart=yes"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="audio-error">
                    Looks like a network error has occurred, you can try, reloading the page OR use the link below and download your conference call.
                </div>
                <div class="download_container">
                    <a href="/Recordings/recording-download?recording_ref=<?php echo $recording['recording_ref']?>" class="blue_big_button">Download</a>
                    <br class="clear" />
                </div>
            </div>
        </section>
        <section class="grid_9 right generic_section">
            <div class="free_recording_text">
                <h3>Maybe it's time to start your own conference calls</h3>
                <p>
                    Someday, somewhere, you're going to need to make a conference call. We want to be there for you when you do.
                </p>
                <p>
                    Whether you need to finalise a project, discuss your company's latest analytics or train your staff,
                    our FREE audio and web conferencing service allows you to work remotely and get things done.
                </p>
                <p>
                    <strong>FREE recordings come standard with any free account.</strong>
                </p>
            </div>
        </section>
    </section>
    <section class="container_16 registration_container clearfix">
        <section class="registration_form">
            <!-- START KO Template registration_form -->
            <div data-bind="template: {name: 'registration_form'}"></div>
            <script type="text/html" id="registration_form">
                <form class="ko_registration_form" data-bind="submit: register">
                    <h2>Register with your email address in just one click</h2>
                    <input type='text' data-bind='value: RegistrationModel.email, valueUpdate: "afterkeydown"' placeholder="One click sign up with your email address" />
                    <input class="ko_button" type="submit" data-bind="enable: RegistrationModel.email().length > 0" value="Get Started" />
                    <img src="/cx2/img/throbber.gif" alt="loading..." class="throbber hide" />
                    <div class="error_message" data-bind="fadeVisible: RegistrationErrors.errors().length > 0">
                        <p class="error" data-bind="html: RegistrationErrors.errors"></p>
                    </div>
                    <br class="clear" />
                    <!--<button class="ko_button" type="submit" data-bind="enable: RegistrationModel.email().length > 0">Get Started</button>-->
                    <div>
                        <p class="terms">
                            In registering for our service you are agreeing to our <a href="<?php echo url_for('@terms_and_conditions');?>">terms and conditions.</a>
                        </p>
                    </div>
                </form>
            </script>
            <!-- END KO Template registration_form -->
        </section>
    </section>
        <?php include_component('commonComponents', 'whatYouGet'); ?>
    <section class="container_16 customer_service_info">
        <p>Our friendly customer service are waiting to help on <a class="chatlink">Live Chat</a></p>
        <p>otherwise you can call <strong class="light_blue">0203 398 0398</strong> to speak on the phone.</p>
    </section>
</main>