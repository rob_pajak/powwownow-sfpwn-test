Please join my conference call.

Your dial-in number is: [TYPE THE NUMBER YOU WANT YOUR PARTICIPANTS TO DIAL IN WITH]
Your PIN is: <?php echo $pin; ?>

Time of call: [TYPE DAY AND TIME OF CALL]

Instructions:
1. Dial the conference number at the time indicated above
2. Enter the PIN, when prompted
3. Speak your full name when prompted and then you will join the conference. If you are the first person to arrive on the conference call, you will be placed on hold until the next person joins. 

Find out how much easier your life could be with Powwownow's Premium conference calling service. 
http://www.powwownow.co.uk/Premium