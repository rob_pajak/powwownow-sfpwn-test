<?php if (!isset($csv)) : ?>
<table id="<?php echo $tableID;?>" class="mypwn<?php echo ' ' . $tblColour; ?>"></table>
<script type="text/javascript">
(function($) {
    'use strict';
    $(function() {
        pwnApp.common.dataTable(
            '#<?php echo $tableID; ?>',
            <?php echo json_encode($sf_data->getRaw('data')); ?>,
            <?php echo json_encode($sf_data->getRaw('columns')); ?>,
            <?php echo json_encode($sf_data->getRaw('includes')); ?>,
            <?php echo $sortCol; ?>,
            '<?php echo $sortOrder; ?>',
            <?php echo (false===$onHover) ? 0 : $onHover; ?>,
            <?php echo (false===$floatLeft) ? 0 : $floatLeft; ?>,
            <?php echo (false===$bottomPadding) ? 0 : $bottomPadding; ?>,
            <?php echo (false===$autoWidth) ? 0 : $autoWidth; ?>,
            '<?php echo $tableEmpty; ?>',
            <?php echo $displayLength; ?>
        );
    });
})(jQuery);
</script>
<?php else : ?>
<?php echo $sf_data->getRaw('csv'); ?>
<?php endif; ?>