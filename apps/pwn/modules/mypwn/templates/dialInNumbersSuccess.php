<?php use_helper('dialInNumbers') ?>
<?php include_component('commonComponents', 'header', array('showButton' => false)); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array(
            'title' => __('Dial-in Numbers and Rates'),
            'disableBreadcrumbs' => true,
            'headingSize' => 'l',
            'user_type' => $service
        )
    ); ?>
    <div class="grid_24 clearfix">
        <div class="grid_sub_24 clearfix">
            <?php include_partial(
                'dialInNumbersIntro',
                array('isPostPayCustomer' => $isPostPayCustomer, 'hasInternationalAddOn' => $hasInternationalAddOn)
            ); ?>
        </div>
        <div class="grid_sub_24 clearfix">
            <?php include_component(
                'mypwn',
                'createDataTable',
                array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)
            ); ?>
        </div>
        <div class="grid_sub_24 clearfix">
            <?php if (sfContext::getInstance()->getUser()->hasCredential('POWWOWNOW')) : ?>
                <button class="button-green floatright margin-bottom-10" type="button" onclick="window.open('/sfpdf/en/Powwownow-Dial-in-Numbers.pdf');">
                    <?php echo __('DOWNLOAD PDF'); ?>
                </button>
            <?php else : ?>
                <!--<button class="button-green print-full-data-table floatright margin-bottom-10" type="button">--><?php //echo __('PRINT FULL LIST');?><!--</button>-->
            <?php endif; ?>
        </div>
        <div class="grid_sub_24 clearfix" id="postcomment">
            <div class="dial_in_numbers_footer">
                <?php include_partial('dialInNumbersFooter', array()); ?>
            </div>
        </div>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
