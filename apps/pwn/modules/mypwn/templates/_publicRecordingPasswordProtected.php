<main class="container_16 clearfix">
    <section class="grid_8 left generic_section">
        <div class="recording_blue_container recording_box">
            <p class="title">Almost there</p>
            <p class="sub_title">Type in the password for your recording to start listening</p>
            <p class="body">
                If you don't know just ask the conference organiser for help.
            </p>
            <form method="POST" action="/" id="public-recording-password" data-recording_ref="<?php echo $recordingRef?>">
                <div class="password_form">
                    <input type="text" name="default-password" class="password-field-fake" value="Type your password" />
                    <input type="password" name="password" class="password-field" value="" />
                    <div class="form-error">Uh oh! That's not the right password.</div>
                    <a href="#" id="recording-password-submit" class="blue_big_button">Listen to Call</a>
                    <br class="clear" />
                </div>
            </form>

        </div>
    </section>
    <section class="grid_8 right generic_section">
        <img src="/cx2/img/recording/woman.png" />
    </section>
</main>