<?php include_component('commonComponents', 'header', array('showButton' => false)); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Web Conferencing'), 'headingSize' => 'l', 'disableBreadcrumbs' => true)); ?>
    <div class="grid_24 clearfix webconferencing">
        <h3 class="<?php echo $service; ?> clearfix"><?php echo __('Screen Sharing'); ?></h3>
        <div class="grid_sub_24 clearfix margin-bottom-10"><p>You know how useful conference calls can be. Now how about if you could do it with pictures? With Powwownow's web conferencing service, that's exactly what you get. A web conference that allows you to chat, present documents and share your desktop with your fellow conference callers, all for free.</p></div>

        <h3 class="<?php echo $service; ?> clearfix getstarted"><?php echo __('Get Started'); ?></h3>
        <div class="grid_sub_24 clearfix margin-bottom-10 getstarted"><p>To download and install the web conferencing application please choose your platform:</p></div>

        <div class="grid_sub_24 clearfix margin-bottom-10 downloadicons">
            <div class="grid_5" style="text-align:center;">
                <img src="/sfimages/download-windows.gif" alt="Download for PC"/><br/>
                <button class="button-green loadWebConferencingForm" id="downloadpc" value="PC">Windows PC</button>
            </div>
            <div class="grid_5" style="text-align:center;">
                <img src="/sfimages/download-apple.gif" alt="Download for MAC"/><br/>
                <button class="button-green loadWebConferencingForm" id="downloadmac" value="MAC">Apple Macintosh</button>
            </div>
        </div>

        <div class="grid_sub_24 clearfix margin-bottom-10 webConferencingForm">
            <?php echo form_tag(url_for('@web_conferencing_auth_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-webconferencing', 'class' => 'pwnform clearfix')); ?>
                <?php echo $form['filetype']->render(); ?>
                <?php foreach ($fields as $field) : ?>
                    <?php include_partial('webConferenceFormField', array('field' => $field, 'form' => $form)); ?>
                <?php endforeach; ?>
                <div class="form-action grid_24 clearfix">
                    <p>* Required fields</p>
                    <button class="button-green floatleft margin-top-10" type="submit" id="confirm-btn"><?php echo __('Confirm'); ?></button>
                </div>
            </form>
        </div>

        <div class="grid_sub_24 clearfix formresults"></div>

        <h3 class="<?php echo $service; ?> clearfix margin-top-10"><?php echo __('Using Web Conferencing'); ?></h3>
        <div class="grid_sub_24 clearfix margin-bottom-10">
            <p>Powwownow's web conferencing service offers you far greater flexibility and screen sharing capability. With a little help from this guide, you'll be sharing your desktop to present documents, presentations, movies and more with colleagues in no time.</p>
        </div>

        <div class="grid_sub_24 clearfix margin-bottom-10" style="text-align:right;" id="webConferencingButtons">
            <button class="button-green" type="button" id="see-demo"><?php echo __('SEE DEMO'); ?></button>
            <button class="button-green" type="button" id="user-guide"><?php echo __('DOWNLOAD USER GUIDE');?></button>
        </div>

        <script>
            $(document).ready(function () {
                webConferencingFormInitialise('#frm-webconferencing');
                utils.buttonInitialise('#webConferencingButtons');
            });
        </script>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
