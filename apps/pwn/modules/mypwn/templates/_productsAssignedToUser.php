<html>
<body>
<!-- ved document -->
<table style="table-layout: auto;" width="100%">
    <tbody>
    <tr>
        <td align="center">
            <table style="table-layout: auto;" cellpadding="0" cellspacing="0" width="600">
                <tbody>
                <tr>
                    <td style="padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px; background-color: #ffffff;"
                        class="ee_dropzone" align="left" width="600">
                        <table class="ee_columns ee_element"
                               style="color: rgb(0, 0, 0); width: 600px; position: relative; table-layout: auto;"
                               border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td class="ee_column ved-scaled-cols" style="padding: 6px 0px 6px 6px; width: 232px;"
                                    valign="top" width="40%">
                                    <img src="https://i.emlfiles.com/cmpimg/2011/35064/2468464_smalllogo.png"
                                         class="ee_editable ee_pnggif_image vedpw232"
                                         style="display: block; width: 200px; height: 95px;" border="0" width="200"
                                         height="95">
                                </td>
                                <td style="padding: 0px; width: 7px;" class="eegap" width="6">
                                    <img src="https://i.emlfiles.com/cmpimg/t/s.gif" alt="" height="6" width="6">
                                </td>
                                <td class="ee_column ved-scaled-cols" style="padding: 6px 6px 6px 0px; width: 349px;"
                                    valign="top" width="60%">
                                    <div class="ee_editable"><br></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 600px; table-layout: auto;" class="ee_element" cellpadding="0"
                               cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding: 6px; width: 588px;" valign="top">
                                    <div style="position: static;" class="ee_editable"><br>
                                        <b>
                                            <font style="font-family: verdana,geneva,sans-serif; font-size: 16px; color: rgb(110, 145, 164);"><br><br>
                                                Your Powwownow Plus account has been updated
                                            </font>
                                        </b>

                                        <div style="text-align: justify; ">
                                            <p>
                                                <font style="font-family: verdana,geneva,sans-serif; font-size: 13px; color: rgb(60, 78, 89);">
                                                    Hi
                                                    <br>
                                                    <br>
                                                    Your Account Administrator has enabled the following products on your account:
                                                </font>
                                            </p>
                                            <ul>
                                                <?php foreach ($enabledProducts as $productName): ?>
                                                    <li>
                                                        <font style="font-family: verdana,geneva,sans-serif; font-size: 13px; color: rgb(60, 78, 89);">
                                                            <?php echo $productName ?>
                                                        </font>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <p>
                                                <font style="font-family: verdana,geneva,sans-serif; font-size: 13px; color: rgb(60, 78, 89);">
                                                    <strong>Got a question?</strong>
                                                    <br>
                                                    You can <a href="<?php echo url_for('@contact_us'); ?>" style="color: #96C84B;">contact us</a> via our website,
                                                    tweet us <a href="http://www.twitter.com/Powwownow_Help" style="color: #96C84B;">@Powwownow_Help</a>
                                                    or speak to one of our team on <a href="<?php echo url_for('@contact_us'); ?>" style="color: #96C84B;">live chat</a>.
                                                </font>
                                            </p>
                                            <p>
                                                <font style="font-family: verdana,geneva,sans-serif; font-size: 13px; color: rgb(60, 78, 89);">
                                                    Speak soon!
                                                </font>
                                            </p>
                                        </div>
                                        <font style="font-family: verdana, geneva, sans-serif; font-size: 12px;">
                                            <font class="" style="color: rgb(81, 81, 81);">
                                                The Powwownow Team
                                            </font>
                                            <br>
                                        </font>
                                        <font style="font-family: verdana,geneva,sans-serif; font-size: 12px; color: rgb(51, 51, 51);">
                                            <font class="" style="color: rgb(81, 81, 81);">0203 398 0398</font>
                                            <br>
                                            <a style="color: #6e91a4; " href="http://www.powwownow.co.uk/"></a>
                                            <a style="color: #96c84b; text-decoration: underline;" href="http://www.powwownow.co.uk">www.powwownow.co.uk</a> </font><br>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 600px; table-layout: auto;" class="ee_element" cellpadding="0"
                               cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding: 0px; width: 600px;">
                                    <table style="table-layout: auto;" border="0" cellpadding="0" cellspacing="0"
                                           width="600">
                                        <tbody>
                                        <tr>
                                            <td width="346">
                                                <img style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                    src="http://images.powwownow.com/2011/emails/newsletter-img1.jpg">
                                            </td>
                                            <td width="31">
                                                <a href="http://www.powwownow.co.uk/blog/" target="_blank">
                                                    <img style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                        alt="Powwownow Blog"
                                                        src="http://images.powwownow.com/2011/emails/newsletter-img2.jpg">
                                                </a>
                                            </td>
                                            <td style="" width="22"><img
                                                    style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                    alt=""
                                                    src="http://images.powwownow.com/2011/emails/newsletter-img3.jpg">
                                            </td>
                                            <td style="" width="25"><a href="http://twitter.com/powwownow"
                                                                       target="_blank"><img
                                                        style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                        alt="Follow us on Twitter"
                                                        src="http://images.powwownow.com/2011/emails/newsletter-img4.jpg"></a>
                                            </td>
                                            <td style="" width="20"><img
                                                    style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                    alt=""
                                                    src="http://images.powwownow.com/2011/emails/newsletter-img5.jpg">
                                            </td>
                                            <td style="" width="11"><a href="http://www.facebook.com/powwownow"
                                                                       target="_blank"><img
                                                        style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                        alt="Like us on Facebook"
                                                        src="http://images.powwownow.com/2011/emails/newsletter-img6.jpg"></a>
                                            </td>
                                            <td style="" width="22"><img
                                                    style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                    alt=""
                                                    src="http://images.powwownow.com/2011/emails/newsletter-img7.jpg">
                                            </td>
                                            <td style="" width="47"><a href="http://www.youtube.com/user/MyPowwownow"
                                                                       target="_blank"><img
                                                        style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                        alt="Watch Powwownow video clips"
                                                        src="http://images.powwownow.com/2011/emails/newsletter-img8.jpg"></a>
                                            </td>
                                            <td style="" width="22"><img
                                                    style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                    alt=""
                                                    src="http://images.powwownow.com/2011/emails/newsletter-img9.jpg">
                                            </td>
                                            <td style="" width="24"><a href="http://www.linkedin.com/company/powwownow"
                                                                       target="_blank"><img
                                                        style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                        alt="Connect with Powwownow"
                                                        src="http://images.powwownow.com/2011/emails/newsletter-img10.jpg"></a>
                                            </td>
                                            <td style="" width="30"><img
                                                    style="border-top: 0px none;border-right: 0px none;border-bottom: 0px none;border-left: 0px none;"
                                                    alt=""
                                                    src="http://images.powwownow.com/2011/emails/newsletter-img11.jpg">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr></tr>
                            </tbody>
                        </table>
                        <table class="ee_element ee_borders eeb_width" style="width: 600px; table-layout: auto;"
                               cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td style="padding-top: 6px;padding-right: 6px;padding-bottom: 6px;padding-left: 6px; width: auto;"
                                    class="ee_pad" valign="top">
                                    <div class="ee_editable eev_element" style="width: 588px;"><font
                                            style="font-family: verdana,geneva,sans-serif; color: #333333; font-size: 12px;"><font
                                                class="" style="color: #3a4b56; font-size: 10px;">Powwownow is the
                                                trading name of Via-Vox Limited, a UK incorporated company registered at
                                                Companies House (Company No. 04646978). Postal address: Via-Vox Ltd, 1st
                                                Floor, Vectra House, 36 Paradise Rd, Richmond, Surrey TW9 1SE, UK</font>
                                        </font></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body></html>