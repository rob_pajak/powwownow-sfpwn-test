<div class="form-field grid_24">
    <?php echo $form[$field]->renderLabel(); ?>
    <span class="mypwn-input-container">
        <?php echo $form[$field]->render(array('class' => 'input-large font-small mypwn-input')); ?>
    </span>
</div>