<?php include_component('commonComponents', 'header', array('showPinBar' => false)); ?>

<!-- CX Top header Banner -->
<?php include_component('commonComponents', 'cxHeader');?>

<main class="container_16 clearfix">
    <section class="grid_12">
        <?php include_component('commonComponents', 'cxMyPinDetails');?>
    </section>

    <aside class="grid_4 sidebars">
        <div class="pwn_sidebar_unit background background_grey">
            <h2 class="font_green">You play DJ...</h2>
            <p class="font_blue">Disco, Funk? Spanish Salsa? Classical? Select from 10 different tracks for your on-hold music.</p>
            <button data-destination="<?php echo url_for('@call_settings'); ?>?music=1" class="cx_button cx_button_orange cx_button_multi_lines">Set my tune NOW</button>
        </div>
    </aside>
</main>

<div class="container_24 clearfix">

    <div class="grid_24 clearfix">
        <?php include_component('mypwn', 'createGrid', array()); ?>
        <?php if ($isPlusAdmin) : ?>
            <?php include_partial('productSelectorTool/sliders', array('pageName' => 'dashboard', 'auth' => true)); ?>
            <?php include_partial('productSelectorTool/result', array('bundles' => $bundles, 'pageName' => 'dashboard')); ?>
        <?php elseif ($isEnhancedUser) : ?>
            <?php include_partial('productSelectorTool/sliders', array('pageName' => 'dashboard_enhanced', 'auth' => true)); ?>
            <?php include_partial('productSelectorTool/result', array('bundles' => $bundles, 'pageName' => 'dashboard_enhanced')); ?>
        <?php else : ?>
            <?php include_partial('indexPromotions', array('individualBundleDialog' => $individualBundleDialog)); ?>
        <?php endif; ?>
    </div>

    <script type="text/javascript">
        $(window).bind("load", function() {
            myPwnApp.index.init();
        });
    </script>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
