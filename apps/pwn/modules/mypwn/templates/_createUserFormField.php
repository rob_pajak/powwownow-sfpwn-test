<div class="form-field grid_sub_12 <?php echo (1==$count) ? 'alpha' : 'omega'; ?>" style="z-index: <?php echo (1==$count) ? 350 : 300; ?>">
    <?php echo $form[$field]->renderLabel(); ?>
    <span class="mypwn-input-container">
        <?php if ('notify_user' == $field) : ?>
        	<?php echo $form[$field]->render(array('class' => 'font-small mypwn-input')); ?>
        <?php else: ?>
        	<?php echo $form[$field]->render(array('class' => 'input-large font-small mypwn-input')); ?>
        <?php endif; ?>
    </span>
</div>