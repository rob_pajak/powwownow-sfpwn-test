<?php if (sfContext::getInstance()->getUser()->hasCredential('POWWOWNOW')) : ?>
<div class="mypwn_promotional" style="background: url('/sfimages/promo/plus-banner-for-mypwn.jpg') no-repeat 40px 0">
    <div style="position:relative;">
        <div style="position:absolute; top:0; left:0;">
            <a style="border-bottom: none;" href="<?php echo url_for('@plus_service'); ?>">
                <img class="png" style="width: 300px; height: 400px;" src="/sfimages/promo/promo-hover.png" alt="Powwownow Plus"/>
            </a>
        </div>
    </div>
</div>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_USER')) : ?>

<div class="stand-out-banner">
    <h3 class="header aligncenter">Even MORE savings on your conference calls</h3>
    <h4 class="sub-header aligncenter rockwell">Talk more pay less!</h4>
    <div class="button-wrapper aligncenter">
        <div class="button button-orange expand">
            <?php echo link_to('Find out more', '@products_select', array('class' => 'bundle-modal')) ?>
        </div>
    </div>

    <div class="bundle-product-dialog">
        <?php include_partial('products/bundleProductDialog', array('dialog' => $individualBundleDialog, 'productName' => 'All You Can Meet')) ?>
    </div>
</div>

<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_ADMIN')) : ?>

 <!--
    <div class="plus-announcement">
        <h3 class="plus-header aligncenter"><strong>Welcome to your updated Plus account!</strong></h3>
        <div class="plus-text">
            <p class="aligncenter"> To make things easier, we’ve changed a few things round.
            <br /><br />
            Keep track of your account features and purchased products with your new <a href="/myPwn/My-Products">My Products page</a>.
            <br /><br />
            Landline and Freephone numbers are now automatically enabled on your account.
               Simply <a href="/myPwn/Products">purchase call credit</a> and enable for other users to start using them today!
                <br />
            </p>
        </div>
        <div class="aligncenter">
            <button onclick="window.location='<?php echo url_for('@contact_us'); ?>'" type="button" class="button-orange">
                <span>Got a question? Contact us</span>
            </button>
        </div>
    </div>
-->
<!--
<div class="stand-out-banner">
    <h3 class="header aligncenter">Stand out from the crowd...</h3>
    <h4 class="sub-header aligncenter rockwell">Personalise your conference calls!</h4>
    <div class="button-wrapper aligncenter">
        <div class="button button-orange expand">
            <a class="pwn-modal" href="/s/Branded-Welcome-Message">Find out more</a>
        </div>
    </div>
</div>
-->

<div class="stand-out-banner">
    <h3 class="header aligncenter">Even MORE savings on your conference calls</h3>
    <h4 class="sub-header aligncenter rockwell">Talk more pay less!</h4>
    <div class="button-wrapper aligncenter">
        <div class="button button-orange expand">
            <?php echo link_to('Find out more', '@products_select', array('class' => 'bundle-modal')) ?>
        </div>
    </div>

    <div class="bundle-product-dialog">
        <?php include_partial('products/bundleProductDialog', array('dialog' => $individualBundleDialog, 'productName' => 'All You Can Meet')) ?>
    </div>
</div>


<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_USER')) : ?>
<div class="mypwn_promotional" style="background: url('/sfimages/promo/engage-premium.jpg') no-repeat 15px 13px">
    <div style="position:relative;">
        <div style="position:absolute; top:0; left:0;">
            <a style="border-bottom:none;" href="/s/Powwownow-Engage-Service" target="_blank">
                <img class="png" style="width: 300px; height: 400px;" src="/sfimages/promo/promo-hover.png" alt="Powwownow Engage"/>
            </a>
        </div>
    </div>
</div>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_ADMIN')) : ?>
<div class="mypwn_promotional" style="background: url('/sfimages/promo/engage-premium.jpg') no-repeat 15px 13px">
    <div style="position:relative;">
        <div style="position:absolute; top:0; left:0;">
            <a style="border-bottom:none;" href="/s/Powwownow-Engage-Service" target="_blank">
                <img class="png" style="width: 300px; height: 400px;" src="/sfimages/promo/promo-hover.png" alt="Powwownow Engage"/>
            </a>
        </div>
    </div>
</div>
<?php endif; ?>
