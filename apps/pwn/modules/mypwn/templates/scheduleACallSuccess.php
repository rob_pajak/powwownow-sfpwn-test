<?php use_helper('getSchedulerAutoLoginLink') ?>
<?php include_component('commonComponents', 'header', array('showButton' => false)); ?>
<div class="container_24 clearfix page-schedule-a-call-success">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Schedule A Call'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)); ?>
    <div class="grid_24 clearfix schedule-a-call">
        <h3 id="button-email-details" class="<?php echo $service; ?>"><?php echo __('Email details'); ?></h3>
        <p><?php echo __('In a hurry? Click on \'email details\' and an email will appear prepopulated with the details you need to share with your Participants - all you need to do is fill in their email addresses and the time of the call!'); ?></p>
        <p>
            <button onclick="window.open('<?php include_component('mypwn','shareDetailsEmail', array(
                    'subject'        => 'Powwownow conference call details',
                    'pin'            => $pin,
                    'dial_in_number' => $dialInNumber
                )); ?>');" class="button-green" style="margin: 15px 0 30px 0;">
                <?php echo __('EMAIL DETAILS'); ?>
            </button>
        </p>
        <h3 id="button-scheduler-tool" class="<?php echo $service; ?>"><?php echo __('Scheduler Tool'); ?></h3>
        <p>
            <?php echo __('Powwownow is all about making things easy for you. So we though we would invent an easy way to invite and remind people to join your conference calls. All you have to do is enter the details of your Participants and the scheduler will do the rest, automatically e-mailing them all the necessary information.'); ?></p>
        <p>
            <br />
            <button class="button-green" onclick="window.open('http://scheduler.powwownow.com<?php echo getSchedulerAutoLoginLink($contactRef, $email); ?>', '_blank');" style="margin: 5px 0 30px 0;">
                <?php echo __('GO TO SCHEDULER'); ?>
            </button>
        </p>
        <h3 id="button-plugin-for-outlook" class="<?php echo $service; ?> clearfix">
            <?php echo __('Plugin for Outlook'); ?>
        </h3>
        <p><?php echo __('The Powwownow Plugin for Outlook makes it quick and easy to schedule a conference using you Outlook calendar and contacts (only PC). No more looking up conference phone numbers, PINs, passwords or URLs and no need for manual reminders.'); ?></p>
        <p style="margin-top: 15px;">Select which version of the Outlook plugin to download<small>*</small>:</p>

        <button id="download-plugin-2003-2007" onclick="window.open('/Outlook_Plugin/current/setup-powwownow-plugin-2003-2007.exe');" class="button-green">
            <?php echo __('DOWNLOAD PLUGIN 2003-2007'); ?>
        </button>

        <button id="download-plugin-2010-2013" onclick="window.open('/Outlook_Plugin/current/setup-powwownow-plugin-2010-2013.exe');" class="button-green" style="margin: 15px 0 20px 0;">
            <?php echo __('DOWNLOAD PLUGIN 2010-2013'); ?>
        </button>

        <p>*Need help finding out which version of Outlook you’re using? Go into Outlook and select File, Help.</p>
    </div>
    <script>
        (function ($) {
            'use strict';
            $(function () {
                $('#download-plugin-2003-2007').click(function () {
                    $.ajax({
                        url: '<?php echo url_for('schedule_a_call_ajax'); ?>'
                    });
                });
                $('#download-plugin-2010-2013').click(function () {
                    $.ajax({
                        url: '<?php echo url_for('schedule_a_call_ajax'); ?>'
                    });
                });
            });
        })(jQuery);
    </script>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
