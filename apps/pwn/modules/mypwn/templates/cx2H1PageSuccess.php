<?php include_component('commonComponents', 'header', array('showPinBar' => false)); ?>
<!-- CX Top header Banner -->
<?php include_component('commonComponents', 'cxHeader');?>

<main class="container_16 clearfix">
    <section class="grid_12">
        <?php include_component('commonComponents', 'cxMyPinDetails');?>

        <section class="get_started clearfix">
            <h2 class="font_green">Get started instantly...</h2>
            <h3 class="font_grey">We've created some simple video guides to get you up and running.</h3>

            <ul class="pwn_list_basic video_player_popout">
                <li><span class="sprite icons-camera"></span><span class="font_blue sprite_text">Using in-conference tools</span></li>
                <li class="body">
                    <img alt="In Conference Controls" data-video-id="#in_conference_control" src="/cx2/img/In-Conference-Macbook.png" width="191" height="116"/>
                </li>
            </ul>

            <div id="in_conference_control" class="pop_out_modal_video_player">
                <div>
                    <a href="#close" title="Close" class="close">X</a>
                    <?php include_component(
                        'commonComponents',
                        'hTML5OutputVideo',
                        array(
                            'config' => array(
                                'width'    => 420,
                                'height'   => 255,
                                'autoplay' => false,
                                'controls' => true
                            ),
                            'video'  => array(
                                'image' => '/sfimages/video-stills/In_Conf_Controls_Video_Still.jpg',
                                'mp4'   => '/sfvideo/pwn_inConfCont460x260_LR.mp4',
                                'webm'  => '/sfvideo/pwn_inConfCont460x260_LR.webm',
                                'ogg'   => '/sfvideo/pwn_inConfCont460x260_LR.ogv'
                            ),
                            'called' => 'html'
                        )
                    ); ?>
                </div>
            </div>

            <ul class="pwn_list_basic video_player_popout">
                <li><span class="sprite icons-camera"></span><span class="font_blue sprite_text">Using your account area</span></li>
                <li class="body">
                    <img alt="MyPowwownow" data-video-id="#my_pwn_video" src="/cx2/img/MyPWN-Macbook.png" width="191" height="116"/>
                </li>
            </ul>

            <div id="my_pwn_video" class="pop_out_modal_video_player">
                <div>
                    <a href="#close" title="Close" class="close">X</a>
                    <?php include_component(
                        'commonComponents',
                        'hTML5OutputVideo',
                        array(
                            'config' => array(
                                'width'    => 420,
                                'height'   => 255,
                                'autoplay' => false,
                                'controls' => true
                            ),
                            'video'  => array(
                                'image' => '/sfimages/video-stills/myPowwownow_Video_Still.jpg',
                                'mp4'   => '/sfvideo/pwn_myPWN460x260_LR.mp4',
                                'webm'  => '/sfvideo/pwn_myPWN460x260_LR.webm',
                                'ogg'   => '/sfvideo/pwn_myPWN460x260_LR.ogv'
                            ),
                            'called' => 'html'
                        )
                    ); ?>
                </div>
            </div>

            <ul class="pwn_list_basic">
                <li><span class="sprite icons-arrow"></span><span class="font_blue sprite_text">Download user guides</span></li>
                <li class="body">
                    <ul>
                        <li><a target="_blank" href="/sfpdf/en/Powwownow-User-Guide.pdf">Conference calling user guides</a></li>
                        <li><a target="_blank" href="/sfpdf/en/Powwownow-Dial-in-Numbers.pdf">International dial-in numbers</a></li>
                    </ul>
                </li>
            </ul>
        </section>
    </section>

    <aside class="grid_4 sidebars">
        <div class="pwn_sidebar_unit background background_grey">
            <h2 class="font_green">You play DJ...</h2>
            <p class="font_blue">Disco, Funk? Spanish Salsa? Classical? Select from 10 different tracks for your on-hold music.</p>
            <button data-destination="<?php echo url_for('@call_settings'); ?>?music=1" class="cx_button cx_button_orange cx_button_multi_lines margin-top-1em cx_button_small_font">Set my tune NOW</button>
        </div>
        <div class="pwn_sidebar_unit background background_grey">
            <h2 class="font_green">Get your <strong>FREE</strong> Welcome Pack</h2>
            <ul class="font_blue pwn_list_simple">
                <li>Wallet card with your unique PIN and dial-in numbers.</li>
                <li>Printed stickers</li>
            </ul>
            <button data-destination="<?php echo url_for('@request_welcome_pack')?>" class="cx_button cx_button_blue_e cx_button_multi_lines margin-top-bottom-1em cx_button_small_font">Order yours NOW</button>
        </div>
    </aside>
</main>

<section class="container_16 clearfix">
    <section class="additional_information background background_green clearfix">
        <div class="grid_4">
            <h3 class="font_blue">That was easy!</h3>
            <p class="font_white">Our contract-free conference calls are just the start of what you can do now you have a Powwownow account.</p>
        </div>

        <div class="grid_6 push_1">
            <h3 class="font_blue">It's better when you share...</h3>
            <p class="font_white">Chat, present document and <br />share your desktop with others <br />on the call.</p>
            <a class="alt_hover" target="_blank" href="<?php echo url_for('@web_conferencing_auth')?>">Download for FREE now</a>
        </div>

        <div class="grid_4 push_1">
            <p class="font_white">Discover what else you can do with your account...</p>
            <br />
            <button data-destination="<?php echo url_for('@mypwn_index')?>" class="cx_button cx_button_blue_e_white_hover">Go to MyPowwownow</button>
        </div>
    </section>
</section>

<footer class="container_24 clearfix">
    <?php include_component('commonComponents', 'footer'); ?>
</footer>