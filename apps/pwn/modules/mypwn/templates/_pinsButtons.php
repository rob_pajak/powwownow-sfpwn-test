<?php if (sfContext::getInstance()->getUser()->hasCredential('POWWOWNOW')) : ?>
    <button class="button-green" type="button" id="call-settings-single"><?php echo __('EDIT CALL SETTINGS'); ?></button>
    <button class="button-green" type="button" id="request-welcome-pack"><?php echo __('REQUEST WELCOME PACK');?></button>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_USER')) : ?>
    <button class="button-green" type="button" id="call-settings-single"><?php echo __('EDIT CALL SETTINGS'); ?></button>
    <button class="button-green" type="button" id="request-welcome-pack"><?php echo __('REQUEST WELCOME PACK');?></button>
    <button class="button-green" type="button" id="call-history"><?php echo __('VIEW USAGE');?></button>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PLUS_ADMIN')) : ?>
    <button class="button-green" type="button" id="call-settings"><?php echo __('EDIT CALL SETTINGS'); ?></button>
    <button class="button-green" type="button" id="request-welcome-pack"><?php echo __('REQUEST WELCOME PACK');?></button>
    <button class="button-green" type="button" id="assign-products"><?php echo __('ASSIGN PRODUCTS');?></button>
    <button class="button-green" type="button" id="call-history"><?php echo __('VIEW USAGE');?></button>
    <button class="button-orange floatright" type="submit" id="delete-pins" value="delete_pins" name="function"><?php echo __('DELETE PINs');?></button>
    <button class="button-orange floatright pwn-tooltip" type="submit" id="create-pin" title="By Creating a new PIN pair for yourself, these PINs will automatically be linked to your email address. If you wish to add a new User to your account, go to Create User page." value="add_pin" name="function"><?php echo __('CREATE PINs'); ?></button>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_USER')) : ?>
    <button class="button-green" type="button" id="call-settings"><?php echo __('EDIT CALL SETTINGS'); ?></button>
    <button class="button-green" type="button" id="request-welcome-pack-single"><?php echo __('REQUEST WELCOME PACK');?></button>
    <button class="button-green" type="button" id="call-history"><?php echo __('VIEW USAGE');?></button>
<?php elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_ADMIN')) : ?>
    <button class="button-green" type="button" id="call-settings"><?php echo __('EDIT CALL SETTINGS'); ?></button>
    <button class="button-green" type="button" id="request-welcome-pack"><?php echo __('REQUEST WELCOME PACK');?></button>
    <button class="button-green" type="button" id="call-history"><?php echo __('VIEW USAGE');?></button>
    <button class="button-orange floatright" type="submit" id="add-pin"><?php echo __('ADD NEW PIN');?></button>
    <button class="button-orange floatright" type="submit" id="delete-pins" value="delete_pins" name="function"><?php echo __('DELETE PINs');?></button>
<?php endif; ?>