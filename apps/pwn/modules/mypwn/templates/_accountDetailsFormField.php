<div class="grid_sub_12 form-field">
    <?php echo $form[$field]->renderLabel() ?>
    <span class="mypwn-input-container">
        <?php echo $form[$field]->render(array('class' => 'input-large font-small mypwn-input')) ?>
    </span>
</div>