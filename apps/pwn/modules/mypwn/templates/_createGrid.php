<div class="home-grid">
    <?php foreach ($gridInformation as $grid) : ?>
        <div class="grid-element <?php if (isset($grid['overlay_class'])) echo $grid['overlay_class']; ?>">
            <a class="<?php echo $service; ?>" href="<?php echo $grid['href']; ?>"<?php echo (isset($grid['href_target'])) ? ' target="' . $grid['href_target'] . '"' : ''; ?>>
                <div class="<?php echo $service; ?> image <?php echo $grid['image']; ?>">&nbsp;</div>
                <div class="grid-element-description"><?php echo $grid->getRaw('text'); ?></div>
                <?php if (!empty($grid['overlay_text'])): ?>
                    <div class="ribbon-wrapper"><div class="ribbon"><?php echo $grid['overlay_text'] ?></div></div>
                <?php endif; ?>
            </a>
            <?php if (isset($grid['hover_text']) && false!==$grid['hover_text']) : ?>
                <a class="grid_hover"<?php echo (isset($grid['hover_href'])) ? ' href="' . $grid['hover_href'] . '"' : ''; ?><?php echo (isset($grid['href_title'])) ? ' title="' . $grid['href_title'] . '"' : ''; ?>>
                    <span><?php echo $grid->getRaw('hover_text'); ?></span>
                </a>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>