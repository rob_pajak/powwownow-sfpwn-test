<?php 
    if (!sfContext::getInstance()->getUser()->hasCredential('POWWOWNOW') && in_array($field,array('chairman_present','chairperson_only','entry_exit_announcements','announcement_type'))) {
        switch ($field) {
            case 'chairman_present':?>
<p style="text-align:left;">Choose if you need to be present to hold conferences with this PIN set:</p>
<ul style="text-align:left;">
    <li>Optional: The conference can start without a Chairperson and can continue after the Chairperson hangs up.</li>
    <li>Throughout: The conference will not start until the Chairperson joins and will be terminated when the Chairperson hangs up.</li>
    <li>Start: The conference will not start until the Chairperson joins.</li>
</ul>
<p style="text-align:left;">If a Participant arrives on the conference before the Chairperson and 'Throughout' or 'Start' has been selected, they will be put on hold and then introduced to the conference one at a time as soon as the Chairperson has joined.</p>
<?php           break;
            case 'chairperson_only':
                echo "By selecting “YES”, only the Chairperson has access to the in-conference controls (excluding 'Private Mute' which is available to all Participants).";
                break;
            case 'entry_exit_announcements':
                echo "Select if you want announcements to occur on entry or exit to the call - or both.";
                break;
            case 'announcement_type':
                echo "Do you want to hear the person’s name or just a beep?";
                break;
            default:
                break;
        }
    }
