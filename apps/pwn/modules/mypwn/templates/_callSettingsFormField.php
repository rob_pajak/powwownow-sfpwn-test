<div class="<?php echo ($showMusicOnHold && $field === 'on_hold_music') ? 'grid_10' : 'grid_12'; ?>">
    <div
        class="form-field<?php echo ($field === 'chairperson_only') ? ' radio_fields' : ''; ?>
        <?php echo ($showMusicOnHold && $field === 'on_hold_music') ? ' showMusic showMusic_' . $showMusicOnHoldUserType : ''; ?>"
        <?php echo ($field === 'chairperson_only') ? ' id="radio_fields' : ''; ?>
    >
        <?php echo ($showMusicOnHold && $field === 'on_hold_music') ? '<label for="callSettings_on_hold_music" class="rockwell">Change your<br/>on-hold music</label>' : $form[$field]->renderLabel(); ?>
        <span class="mypwn-input-container"><?php echo $form[$field]->render(); ?></span>
        <?php if ($helperTitle) : ?><span title="Click for more Information" class="field-help-info" id="<?php echo $helperTitle; ?>"></span><?php endif; ?>
        <?php if ($showMusicOnHold && $field === 'on_hold_music') : ?><span class="subLabel">Select a track to listen to it</span><?php endif; ?>
        <?php if ($field === 'on_hold_music') : ?><div id="player_span"></div><?php endif; ?>
    </div>
</div>
<?php if (isset($clear) && false!==$clear) : ?><div class="clear"></div><?php endif; ?>