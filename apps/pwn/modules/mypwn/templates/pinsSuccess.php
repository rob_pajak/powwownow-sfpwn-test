<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('My PINs'), 'headingSize' => 's', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24 clearfix">
        <?php echo form_tag(
            url_for('pins_ajax'),
            array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-myPINs', 'class' => 'pwnform clearfix')
        ); ?>
            <?php include_partial(
                'pinsTable',
                array(
                    'pin'              => (isset($pin)) ? $pin : '',
                    'chair_pin'        => (isset($chair_pin)) ? $chair_pin : '',
                    'part_pin'         => (isset($part_pin)) ? $part_pin : '',
                    'dialInNumber'     => (isset($dialInNumber)) ? $dialInNumber : '',
                    'dataTable'        => (isset($dataTable)) ? $dataTable : '',
                    'dataTableOptions' => (isset($dataTableOptions)) ? $dataTableOptions : ''
                )
            ); ?>
            <div class="grid_sub_24 clearfix">
                <?php include_partial('pinsButtons', array()); ?>
            </div>
        </form>
    </div>
    <div id="pins-success"></div>
    <script>
        $(document).ready(function () {
            pinsFormInitialise('#frm-myPINs');
            utils.buttonInitialise('#frm-myPINs');
        });
    </script>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
