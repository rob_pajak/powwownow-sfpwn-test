<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('Request Welcome Pack'), 'headingSize' => 'l', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24 clearfix">
        <?php include_partial(
            'requestWelcomePackIntro',
            array(
                'walletCardOrdered' => $previouslyOrderedAWalletCard,
                'service'           => $service
            )
        ); ?>
        <?php echo form_tag(
            url_for('request_welcome_pack_ajax'),
            array(
                'enctype' => 'application/x-www-form-urlencoded',
                'id'      => 'frm-request-welcome-pack',
                'class'   => 'pwnform clearfix'
            )
        ); ?>
            <div class="grid_sub_24 clearfix">
                <?php if (count($pinPairs) == 1 && ($powwownowUser || $plusUser)) : ?>
                    <input type="hidden" id="requestWelcomePack_pin_ref" name="requestWelcomePack[pin_ref][]" value="<?php echo $pin; ?>"/>
                <?php else : ?>
                    <?php include_component(
                        'mypwn',
                        'createDataTable',
                        array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)
                    ); ?>
                <?php endif; ?>
            </div>
            <div class="grid_sub_24 clearfix">
                <?php if ($isAdmin && !$walletCardCanBeOrdered) : ?>
                    <p><strong><?php echo __('Before a wallet card can be ordered, please ensure users have both a first and last name. This can be done by visiting the My PIN\'s page and clicking Edit User'); ?></strong></p>
                <?php else: ?>
                    <div class="grid_12">
                        <?php foreach ($formFields as $field) : ?>
                            <?php include_partial(
                                'requestWelcomePackFormField',
                                array('field' => $field, 'form' => $requestWelcomePackForm)
                            ); ?>
                        <?php endforeach; ?>
                        <div class="form-field grid_24">
                            <?php if (in_array($service, array('powwownow', 'plus'))) : ?>
                                <label>Country *</label>
                                <input value="GBR" id="requestWelcomePack_country" name="requestWelcomePack[country]" type="hidden"/>
                                <span class="mypwn-input-container">
                                    <input required="required" readonly="readonly" value="United Kingdom" class="input-large font-small mypwn-input" id="country_name" name="country_name" type="text"/>
                                </span>
                            <?php else : ?>
                                <?php echo $requestWelcomePackForm['country']->renderLabel(); ?>
                                <span class="mypwn-input-container">
                                    <select required="required" id="requestWelcomePack_country" name="requestWelcomePack[country]" class="input-large font-small mypwn-input">
                                        <?php echo $countriesList; ?>
                                    </select>
                                </span>
                            <?php endif; ?>
                        </div>
                        <div class="form-action grid_24">
                            <p>* Required fields</p>
                            <button class="button-green walletcard-sendbtn floatleft margin-top-10" type="submit">
                                <?php echo __('REQUEST WELCOME PACK'); ?>
                            </button>
                        </div>
                    </div>
                    <div class="grid_12">
                        <?php include_partial(
                            'requestWelcomePackPreview',
                            array(
                                'previewDefaultDialInNumber' => $results['preview']['previewDialInNumber'],
                                'previewChairPin'            => $results['preview']['previewChairPin'],
                                'previewParticipantPin'      => $results['preview']['previewPartPin'],
                                'previewFirstName'           => $results['preview']['previewFirstName'],
                                'previewLastName'            => $results['preview']['previewLastName'],
                                'service'                    => $service,
                                'plus'                       => sfContext::getInstance()->getUser()->hasCredential('plus'),
                                'premium'                    => sfContext::getInstance()->getUser()->hasCredential('premium')
                            )
                        ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </form>
        <script>
            $(document).ready(function () {
                requestWelcomePackFormInitialise('#frm-request-welcome-pack');
            });
        </script>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
