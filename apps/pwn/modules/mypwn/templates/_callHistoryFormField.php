<div class="form-field grid_sub_12 <?php echo (1==$count) ? 'alpha' : 'omega'; ?>" style="float: none;">
    <?php echo $form[$field]->renderLabel(); ?>
    <span class="mypwn-input-container" style="<?php echo (1==$count) ? 'z-index:699;' : 'z-index:698'; ?>">
        <?php if (in_array($field,array('show_all_account','include_unsuccessful_calls'))) : ?>
        	<?php echo $form[$field]->render(array('class' => 'font-small mypwn-input')); ?>
        <?php else: ?>
        	<?php echo $form[$field]->render(array('class' => 'input-large font-small mypwn-input')); ?>
        <?php endif; ?>
    </span>
</div>