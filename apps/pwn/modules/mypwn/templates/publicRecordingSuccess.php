<?php include_component('commonComponents', 'header', array('showPinBar' => false)); ?>

<?php if ($error):?>
    <?php if($errorCode === 1):?>
        <?php include_partial('publicRecordingExpired'); ?>
    <?php else: ?>
        <?php include_partial('publicRecordingNotFound'); ?>
    <?php endif;?>
<?php elseif($passwordProtected && !$recording):?>
    <?php include_partial('publicRecordingPasswordProtected',array('recordingRef' => $recordingRef)); ?>
<?php else:?>
    <?php if ($recording['is_free_pin'] == true):?>
        <?php include_partial('publicRecordingFree', array('recording' => $recording,'datePublish' => $datePublish)); ?>
    <?php else: ?>
        <?php include_partial('publicRecordingPremiumPlus', array('recording' => $recording,'datePublish' => $datePublish)); ?>
    <?php endif;?>
<?php endif;?>

<footer class="container_24 clearfix">
    <?php include_component('commonComponents', 'footer'); ?>
</footer>
