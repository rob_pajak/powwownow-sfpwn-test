<?php include_component('commonComponents', 'header', array('showButton' => false)); ?>
<div class="container_24 clearfix account-details">
    <div>
        <?php include_component(
            'commonComponents',
            'subPageHeaderStacked',
            array(
                'title'              => __('Account Details'),
                'headingSize'        => 'm',
                'headingClassPrefix' => 'mypwn_',
                'user_type'          => $serviceType,
            )
        ); ?>
    </div>
    <!-- The Contact Details form. -->
    <?php echo form_tag(
        url_for('account_details_contact_submit'),
        array(
            'enctype' => 'application/x-www-form-urlencoded',
            'id'      => 'frm_account_details_my_details',
            'class'   => 'clearfix fx-frm'
        )
    ); ?>
        <h3 class="<?php echo $titleClass; ?>">My Details</h3>

        <p>Your registered details are listed below. If you would like to change anything, this is the place to do it.</p>

        <?php $fields = array('title', 'first_name', 'middle_initials', 'last_name', 'business_phone', 'mobile_phone'); ?>
        <?php foreach ($fields as $field): ?>
            <?php include_partial('accountDetailsFormField', array('field' => $field, 'form' => $contactForm)); ?>
        <?php endforeach; ?>

        <div class="grid_sub_12 form-field">
            <?php if ($hasEditableEmail): ?>
                <?php echo $contactForm['email']->renderLabel() ?>
                <span class="mypwn-input-container">
                    <?php echo $contactForm['email']->render(array('class' => 'input-large font-small mypwn-input')) ?>
                </span>
            <?php else: ?>
                <label>Email*</label>
                <span><?php echo $contactForm->defaultEmail; ?></span>
                <span id="email-change-info"></span>
            <?php endif; ?>
        </div>

        <?php include_partial('accountDetailsFormField', array('field' => 'country', 'form' => $contactForm)); ?>
        <?php include_partial('accountDetailsFormField', array('field' => 'company', 'form' => $contactForm)); ?>

        <div class="grid_sub_24 clearfix form-action">
            <p>*Required fields</p>
            <button type="submit" value="run report" class="button-green">SAVE</button>
        </div>

        <?php if (!$hasEditableEmail): ?>
            <div class="frmaccountdetails-results-placeholder1 grid_sub_24 clearfix" style="text-align:center"></div>
        <?php endif; ?>
    </form>

    <!-- Password form. -->
    <?php echo form_tag(
        url_for('account_details_password_submit'),
        array(
            'enctype' => 'application/x-www-form-urlencoded',
            'id'      => 'frm_account_details_my_password',
            'class'   => 'clearfix fx-frm'
        )
    ); ?>
        <h3 class="<?php echo $titleClass ?>">My Password</h3>
        <?php if ($hasYuuguu): ?>
            <p><span class="yuuguu-password-message">You can reset your password here.</span></p>
            <p><span class="yuuguu-remember-message">Next time you log in to your myPowwownow account and web conferencing application remember to use your new password.</span></p>
        <?php else: ?>
            <p>You can reset your password here.</p>
        <?php endif; ?>

        <?php include_partial('accountDetailsFormField', array('field' => 'password', 'form' => $passwordForm)); ?>
        <?php include_partial('accountDetailsFormField', array('field' => 'confirm_password', 'form' => $passwordForm)); ?>

        <div class="grid_sub_24 clearfix form-action">
            <p>*Required fields</p>
            <button type="submit" value="run report" class="button-green">SAVE</button>
        </div>
    </form>

    <!-- Organisation Details form. -->
    <?php if (isset($organisationForm)): ?>
        <?php echo form_tag(
            url_for('account_details_organisation_submit'),
            array(
                'enctype' => 'application/x-www-form-urlencoded',
                'id'      => 'form_plus_account_details',
                'class'   => 'clearfix fx-frm'
            )
        ); ?>
            <h3 class="blue">Organisation Details</h3>

            <?php $fields = array('account_name', 'organisation', 'vat_number'); ?>
            <?php foreach ($fields as $field): ?>
                <?php include_partial('accountDetailsFormField', array('field' => $field, 'form' => $organisationForm)); ?>
            <?php endforeach; ?>

            <div class="grid_sub_24 clearfix form-action">
                <p>*Required fields</p>
                <button type="submit" value="run report" class="button-green">SAVE</button>
            </div>
        </form>
    <?php elseif (!empty($isPlusUser)): ?>
        <!-- For plus uses, show a summary of the plus account. -->
        <div class="grid_20 clear-both plus-details">
            <h3 class="blue">Organisation Details</h3>
            <?php include_partial(
                'accountListItem',
                array(
                    'value' => $accountAdmin['first_name'] . ' ' . $accountAdmin['last_name'],
                    'label' => 'Admin Name'
                )
            ) ?>
            <?php include_partial(
                'accountListItem',
                array('value' => $accountAdmin['business_phone'], 'label' => 'Business Phone')
            ) ?>
            <?php include_partial('accountListItem', array('value' => $accountAdmin['email'], 'label' => 'Email')) ?>
            <?php include_partial(
                'accountListItem',
                array('value' => $account['account_name'], 'label' => 'Account Name')
            ) ?>
            <?php include_partial(
                'accountListItem',
                array('value' => $account['organisation'], 'label' => 'Organisation')
            ) ?>
            <?php include_partial(
                'accountListItem',
                array('value' => $account['vat_number'], 'label' => 'VAT Number')
            ) ?>
        </div>
    <?php elseif (!empty($isPremium)): ?>
        <div class="grid_20 clear-both premium-details">
            <h3>My Company</h3>
            <?php if (!$premiumContact->count()): ?>
                <div class="grid_sub_24 clearfix">An error occurred loading your account details.</div>
            <?php else: ?>
                <div class="grid_20 clear-both">
                    <div class="grid_20 clear-both">
                        If any of these details need updating, please use the <a title="Contact Us" href="<?php echo url_for('@contact_us'); ?>" target="_self">Contact Us</a> page to let us know.
                    </div>

                    <?php include_partial(
                        'accountListItem',
                        array('value' => $premiumContact['customer']['customer_name'], 'label' => 'Title')
                    ) ?>
                    <?php include_partial(
                        'accountListItem',
                        array('value' => $premiumContact['customer']['customer_mnemonic'], 'label' => 'Mnemonic')
                    ) ?>
                    <?php include_partial(
                        'accountListItem',
                        array('value' => $premiumContact['customer']['customer_code'], 'label' => 'Code')
                    ) ?>
                    <?php include_partial(
                        'accountListItem',
                        array('value' => $premiumContact['billing_address']['customer_type'], 'label' => 'Type')
                    ) ?>
                    <?php include_partial(
                        'accountListItem',
                        array('value' => $premiumContact['customer']['country'], 'label' => 'Country')
                    ) ?>
                    <?php include_partial(
                        'accountListItem',
                        array('value' => $premiumContact['billing_address']['status'], 'label' => 'Status')
                    ) ?>
                    <?php include_partial(
                        'accountListItem',
                        array('value' => $premiumContact['account_manager'], 'label' => 'Account Manager')
                    ) ?>
                </div>

                <div class="grid_20 clear-both list-contact">
                    <div class="grid_6 bottompadding clear-both"><h4>Admin Contact</h4></div>
                    <div class="grid_6 bottompadding"><h4>Billing Contact</h4></div>
                    <div class="grid_6 bottompadding"><h4>Company Address</h4></div>
                    <div class="grid_6 bottompadding"><h4>Billing Address</h4></div>

                    <div class="grid_6 bottompadding wordwrap clear-both">
                        <span><?php echo $premiumContact['admin_contact']['first_name'], $premiumContact['admin_contact']['last_name'] ?></span>
                        <br/>
                        <span><?php echo $premiumContact['admin_contact']['email']; ?></span>
                    </div>
                    <div class="grid_6 bottompadding wordwrap">
                        <span><?php echo $premiumContact['billing_contact']['first_name'], $premiumContact['billing_contact']['last_name'] ?></span>
                        <br/>
                        <span><?php echo $premiumContact['billing_contact']['email'] ?></span>
                    </div>
                    <div class="grid_6 bottompadding wordwrap"><?php echo implode('<br/>', $premiumCompanyAddress); ?></div>
                    <div class="grid_6 bottompadding wordwrap"><?php echo implode('<br/>', $premiumBillingAddress); ?></div>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <!-- Social Media Accounts -->
    <div class="grid_24 clear-both premium-details" id="social-media-accounts">
        <h3 class="blue">My Linked Accounts</h3>
        <br/>
        <div>
            <?php foreach (array('LinkedIn', 'Google', 'Facebook') as $_media): ?>
                <?php $_linkedAccount = isset($socialMediaAccounts[$_media]) ? $socialMediaAccounts[$_media] : array('id' => 0); ?>
                <div style="clear: both; padding: 10px 0;">
                    <div style="float: left; width: 120px;">
                        <img src="/sfimages/social/<?php echo strtolower($_media); ?>-small.png"
                             alt="<?php echo strtolower($_media); ?>"/>
                    </div>
                    <div style="margin-left: 130px;">
                        <?php if ($_linkedAccount['id'] > 0): ?>
                            <?php echo $_linkedAccount['email']; ?><br/>
                            <a href="#"
                               onclick="dataLayer.push({'event': 'OAuth - Unlink Account - .<?php echo $_media; ?>/onClick'}); setTimeout(function(){window.location.href = '<?php echo url_for(
                                   '@social_disconnect?oauth_user_id=' . $_linkedAccount['id']
                               ); ?>';}, 1000);">
                                click to unlink with <?php echo $_media; ?>
                            </a>
                        <?php else: ?>
                            No account associated.<br/>
                            <a href="#"
                               onclick="dataLayer.push({'event': 'OAuth - Link Account - <?php echo $_media; ?>/onClick'}); setTimeout(function(){window.location.href = '<?php echo url_for(
                                   '@social_connect?media=' . $_media
                               ); ?>';}, 1000);">
                                Click to link with another <?php echo $_media; ?> account
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
