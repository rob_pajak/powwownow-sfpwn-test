<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('Create User'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24 clearfix">
        <?php include_component('commonComponents', 'createUserForm') ?>
    </div>
    <div id="createUser-success" title="User successfully created"></div>
    <script>
        $(document).ready(function () {
            createUserFormInitialise('#form-create-user');
        });
    </script>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
