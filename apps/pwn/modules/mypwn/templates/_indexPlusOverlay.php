<?php $details = $sf_data->getRaw('details'); ?>
<div id="overlay">
    <a class="floatright sprite tooltip-close" href="#" onclick="$('#overlay').dialog('close');">
        <span class="sprite-cross-white png"></span>
    </a>
    
    <div class="container_24">
        <h1 class="mypwn_sub_page_heading mypwn_sub_page_heading_xl mypwn_sub_page_heading_plus">
            <?php echo __('Welcome to Powwownow Plus'); ?>
        </h1>
        <div class="grid_16 overlay-description clearfix"><?php echo $details['intro_text']; ?></div>
        
        <div class="grid_9 yourDialinNumber">
            <strong><?php echo __('Your dial-in number is:'); ?></strong>
            <div class="dialin"><?php echo $details['dialInNumber']; ?></div>
        </div>

        <div class="grid_9 yourPin">
            <strong><?php echo __('Your PINs are:'); ?></strong>
            <div class="pins clearfix"><?php echo __('Chairperson PIN: '); ?><span class="pin"><?php echo $details['pins_chairperson_pin']; ?></span></div>
            <div class="pins clearfix"><?php echo __('Participant PIN: '); ?><span class="pin"><?php echo $details['pins_participant_pin']; ?></span></div>
        </div>

        <div class="grid_6 clearfix">
            <div class="tooltip">
                <div class="tooltip-box"></div>
                <p class="tooltip-p1"><?php echo __('In a hurry? You can start your first conference call right now.');?></p>
                <p class="tooltip-p2"><?php echo __('Just share the dial-in number and Participant PIN with the other callers and you\'re off!'); ?></p>
            </div>
            <?php if ($overlay == 'regNew') : ?>
                <button id="btn-share-details" class="button-orange" type="button" onclick="location.href='<?php include_component('mypwn','shareDetailsEmail',array(
                    'subject'        => 'Powwownow conference call details',
                    'pin'            => $details['pins_participant_pin'],
                    'newUser'        => true
                    )); ?>'; dataLayer.push({'event': 'Plus After Registration - Share Details/onClick'});"><span><?php echo __('SHARE DETAILS'); ?></span></button>
            <?php else : ?>
                <button id="btn-share-details" class="button-orange" type="button" onclick="location.href='<?php include_component('mypwn','shareDetailsEmail',array(
                    'subject'        => 'Powwownow conference call details',
                    'pin'            => $details['pins_participant_pin'],
                    'newUser'        => true
                )); ?>';"><span><?php echo __('SHARE DETAILS'); ?></span></button>
            <?php endif; ?>
        </div>

        <div class="grid_24 pin-usage-description clearfix">
            <div class="chairpin">
                <strong><?php echo __('Your Chairperson PIN');?></strong> <?php echo __('has been generated to offer you additional security and complete control over your conference calls.');?>
            </div>
            <div class="partpin">
                <strong><?php echo __('Your Participant PIN');?></strong> <?php echo __('is your exisitng PIN which you can continue to share with your call Participants.'); ?>
            </div>
            <p class="overlay-item payg">

                <strong><?php echo __('PAYG Worldwide and UK Feephone 0800) and Landline (0203) dial-in numbers') ?></strong>
                <?php echo __('are ready for you to use. As the administrator these have already been enabled on your account; simply purchase PAYG Call Credit to start using them immediately!')?>
            </p>
        </div>

        <div class="grid_24 clearfix"><h2 class="rockwell plus-blue"><?php echo __('So, what\'s next?'); ?></h2></div>

        <div class="grid_24 footer-text clearfix">
            <?php if (in_array($overlay,array('regNew','regEx'))) : ?>
                <p>
                    <strong> -Bundles: </strong>make substantial savings on your conference call charges with one of our Money-Saving Minute Bundles - Free to sign up and no long contracts! Try our <a href="/myPwn#shake" title="Bundle Selector Tool">Bundle Selector Tool</a> to calculate exactly how much you could save!
                </p>

                <p><strong> -<?php echo __('Branded Welcome Messages');?>: </strong><?php echo __('stand out from the crowd! Personalise your conference calls with company branding added to a dedicated Freephone or Landline number.');?></p>

                <p><strong> -<?php echo __('Your free Welcome Pack');?>: </strong><?php echo __('contains a personalised wallet card and stickers for your computer and phone, so you can always keep your dial-in number and PINs handy!');?></p>


            <?php elseif (in_array($overlay,array('invNew','invEx'))) : ?>
                <p>Your free Welcome Pack contains a personalised wallet card and stickers for your computer and phone, so you can keep your dial-in number and PINs handy.</p>
            <?php elseif ($overlay == 'regPromo') : ?>
                <p>You've now redeemed your promo code and your Plus account has been activated with all products enabled. You now have access to Landline and Freephone numbers for over 70 countries.</p>
                <p>Next time you conference call don’t forget to dial-in using your Chairperson PIN and share your participant PIN with your callers.</p>
            <?php endif; ?>

            <?php if (in_array($overlay,array('regNew'))) : ?>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="dataLayer.push({'event': 'Plus After Registration - Request Welcome Pack/onClick'}); window.location='/myPwn/Request-Welcome-Pack';"><span>REQUEST WELCOME PACK</span></button></div>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="dataLayer.push({'event': 'Plus After Registration - Purchase Bundles/onClick'}); window.location='<?php echo url_for('@products_select') ?>';"><span>PURCHASE BUNDLES</span></button></div>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="dataLayer.push({'event': 'Plus After Registration - Welcome Message/onClick'}); window.location='<?php echo url_for('@products_select') ?>';"><span>BRANDED WELCOME MESSAGES</span></button></div>
            <?php elseif (in_array($overlay,array('regEx'))) : ?>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="window.location='/myPwn/Request-Welcome-Pack';"><span>REQUEST WELCOME PACK</span></button></div>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="window.location='<?php echo url_for('@products_select') ?>';"><span>PURCHASE BUNDLES</span></button></div>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="window.location='<?php echo url_for('@products_select') ?>';"><span>BRANDED WELCOME MESSAGES</span></button></div>
            <?php elseif (in_array($overlay,array('invNew','invEx'))) : ?>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="window.location='/myPwn/Request-Welcome-Pack';"><span>REQUEST WELCOME PACK</span></button></div>
            <?php elseif ($overlay == 'regPromo') : ?>
                <div class="grid_6 do-now-buttons"><button type="button" class="button-green" target="_self" onclick="window.location='/s/Account-Update?promo';"><span>CONTINUE</span></button></div>
            <?php endif; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#overlay').dialog({modal: true, width: <?php echo $details['modal_width']; ?>, height: <?php echo $details['modal_height']; ?>, resizable: false});
    $('#overlay').parent('div').css('border','2px solid #7EA2B4');
});
</script>
