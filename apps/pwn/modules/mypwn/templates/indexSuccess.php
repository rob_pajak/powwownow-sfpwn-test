<?php include_component('commonComponents', 'header'); ?>

<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => false, 'breadcrumbs' => false)); ?>
    <?php include_component('commonComponents', 'iMeetBanner', array()); ?>
    <div class="grid_24 clearfix">
        <?php if ($socialConnect !== false): ?>
            <?php if ($socialConnect == 'success'): ?>
                <div class="success message" style="margin: 10px 35px 10px 10px;">
                    Your <?php echo $connectedMedia; ?> account has now been linked with your myPowwownow account.
                </div>
            <?php elseif ($socialConnect == 'failAlreadyConnected'): ?>
                <div class="error message" style="margin: 10px 35px 10px 10px;">
                    This <?php echo $relatedMedia; ?> account is linked with different myPowwownow account<br/>
                    Unlink first link with this myPowwownow account.
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?php include_component('mypwn', 'createGrid', array()); ?>
        <?php if ($isPlusAdmin) {
            include_partial('productSelectorTool/sliders', array('pageName' => 'dashboard', 'auth' => true));
            include_partial('productSelectorTool/result', array('bundles' => $bundles, 'pageName' => 'dashboard'));
        } elseif ($isEnhancedUser) {
            include_partial('mypwn/indexQuizBanner', array());
        } else {
            include_partial('indexPromotions', array('individualBundleDialog' => $individualBundleDialog));
        } ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            dashboardInitialise();
            <?php if ($countSocialMediaAccounts == 0) : ?>
            tryToShowSocialDialog();
            <?php endif; ?>
        });
    </script>
    <?php if (false !== $overlay) : ?>
        <?php include_partial(
            'indexPlusOverlay',
            array('overlay' => $overlay, 'details' => $overlayDetails)
        ); ?>
    <?php endif; ?>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>

    <?php if ($isEnhancedUser) : ?>
        <!-- Start Modal Windows -->
        <div style="display: none;" id="dialog-plus-link"></div>
        <div id="dialog-plus">
            <div class="clearfix container_24" id="dialog-plus-main">
                <!-- Start Plus Templates -->
                <?php foreach ($plusTemplates as $formName => $form) : ?>
                    <?php echo $form; ?>
                <?php endforeach; ?>
                <!-- End Plus Templates -->
            </div>
        </div>
        <!-- End Modal Windows -->
        <?php use_javascript('/sfjs/enhancedPst.js'); ?>
        <?php use_stylesheet('/sfcss/pages/product-selector-tool/enhancedPst.css'); ?>
        <?php use_javascript('/shared/jQuery/dialogs.js'); ?>
    <?php endif; ?>
    <!-- Tool Tips and Dialog Windows -->
    <script type="text/javascript">
        $(document).ready(function () {
            var plusWidth = 460;

            if ($.browser.msie && $.browser.version <= 8.0) {
                plusWidth = 500;
            }

            dialogs.initDialog({
                'NewPlusPopup': {
                    'id': 'dialog-plus',
                    'modal': true,
                    'closeOnEscape': true,
                    'width': plusWidth,
                    'minHeight': 200
                }
            });
        });
    </script>
</div>
<div id="social-dialog-message" title="Download complete" style="display: none;">
    <div id="social-dialog-header">
        <div class="floatright sprite plus-close" style="border-bottom: none; padding-top: 0; width: 22px; height: 14px;"
             onclick="$(this).parent().parent().dialog('close');">
            <span class="sprite-cross-white png" style="margin: 2px;"></span>
        </div>
        Would you like to connect your account with:
    </div>
    <div style="text-align: center;" id="social-dialog-content">
        <p>
            <a href="#" onclick="dataLayer.push({'event': 'OAuth - Header - LinkedIn/onClick'});
                   setTimeout(function(){window.location.href = '<?php echo url_for('@social_connect?media=LinkedIn'); ?>';}, 1000);">
                <img src="/sfimages/social/linkedin-big.png" alt="LinkedIn"/>
            </a>
        </p>
        <p>
            <a href="#" onclick="dataLayer.push({'event': 'OAuth - Header - Google/onClick'});
                   setTimeout(function(){window.location.href = '<?php echo url_for('@social_connect?media=Google'); ?>';}, 1000);">
                <img src="/sfimages/social/google-big.png" alt="Google"/>
            </a>
        </p>
        <p>
            <a href="#" onclick="dataLayer.push({'event': 'OAuth - Header - Facebook/onClick'});
                setTimeout(function(){window.location.href = '<?php echo url_for('@social_connect?media=Facebook'); ?>';}, 1000);">
                <img src="/sfimages/social/facebook-big.png" alt="Facebook"/>
            </a>
        </p>
        <p style="text-align: right; padding-right: 20px;">
            <a href="javascript: void(0);" onclick="hideSocialDialog();" style="text-decoration: underline; color: #7f7f7f;">
                Remind me later/Don't show this message again
            </a>
        </p>
    </div>
</div>
