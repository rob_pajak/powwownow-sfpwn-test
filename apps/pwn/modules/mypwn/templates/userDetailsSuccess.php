<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix user-details">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('User Details'), 'headingSize' => 'm', 'disableBreadcrumbs' => false)); ?>
    <?php if ($permissionDenied) : ?>
    <div class="grid_24 clearfix">You do not have permission to edit that user.</div>
    <?php else: ?>
    <div class="grid_24 clearfix">
        <!-- The Contact Details form. -->
        <?php echo form_tag(url_for('user_details_contact_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm_user_details_user_details', 'class' => 'clearfix')); ?>
            <h3 class="<?php echo $titleClass ?>">User Details</h3>
            <p>Your registered details are listed below. If you would like to change anything, this is the place to do it.</p>
            <?php $fields = array('title', 'first_name', 'middle_initials', 'last_name', 'business_phone', 'mobile_phone'); ?>
            <?php foreach ($fields as $field): ?>
                <?php include_partial('userDetailsFormField', array('field' => $field, 'form' => $contactForm)); ?>
            <?php endforeach; ?>
            <div class="grid_sub_12 form-field">
            <?php if ($hasEditableEmail): ?>
                <?php echo $contactForm['email']->renderLabel(); ?>
                <span class="mypwn-input-container">
                    <?php echo $contactForm['email']->render(array('class' => 'input-large font-small mypwn-input')); ?>
                </span>
            <?php else: ?>
                <label>Email*</label>
                <span><?php echo $contactForm->defaultEmail; ?></span>
                <span id="email-change-info"></span>
            <?php endif; ?>
            </div>

            <?php include_partial('userDetailsFormField', array('field' => 'country', 'form' => $contactForm)); ?>
            <?php include_partial('userDetailsFormField', array('field' => 'company', 'form' => $contactForm)); ?>

            <div class="grid_sub_24 clearfix form-action">
                <p>*Required fields</p>
                <input type="hidden" name="contact_ref" value="<?php echo $contactRef; ?>" />
                <button type="submit" value="Save" class="button-green">SAVE</button>
            </div>

            <?php if (!$hasEditableEmail): ?>
            <div class="frm_user_details_user_details_results_placeholder1 grid_sub_24 clearfix"></div>
            <?php endif; ?>
        </form>
    </div>
    <div class="grid_24 clearfix">
        <!-- Password form. -->
        <?php echo form_tag(url_for('user_details_password_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm_user_details_user_password', 'class' => 'clearfix')); ?>
            <h3 class="<?php echo $titleClass ?>">My Password</h3>
            <?php if ($hasYuuguu): ?>
                <p><span class="yuuguu-password-message">You can reset your password here.</span></p>
                <p><span class="yuuguu-remember-message">Next time you log in to your myPowwownow account and web conferencing application remember to use your new password.</span></p>
            <?php else: ?>
                <p>You can reset your password here.</p>
            <?php endif; ?>

            <?php include_partial('userDetailsFormField', array('field' => 'password', 'form' => $passwordForm, 'left' => true)); ?>
            <?php include_partial('userDetailsFormField', array('field' => 'confirm_password', 'form' => $passwordForm)); ?>

            <div class="grid_sub_24 clearfix form-action">
                <p>*Required fields</p>
                <input type="hidden" name="contact_ref" value="<?php echo $contactRef; ?>" />
                <button type="submit" value="Save" class="button-green">SAVE</button>
            </div>
        </form>
    </div>
    <script>
        (function ($) {
            'use strict';
            $(function () {
                userDetailsFormInitialise('#frm_user_details_user_details', '#frm_user_details_user_password');
            });
        })(jQuery);
    </script>
    <?php endif; ?>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
