<?php $sf_response->setTitle(__('My Users')); ?>
<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('My Users'), 'headingSize' => 's', 'disableBreadcrumbs' => true)); ?>

    <div class="grid_24 clearfix">
        <?php echo form_tag(url_for('users_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-users', 'class' => 'pwnform clearfix')); ?>
            <div class="grid_sub_24 clearfix">
                <?php include_component('mypwn', 'createDataTable', array('dataTable' => $dataTable, 'dataTableOptions' => $dataTableOptions)); ?>
            </div>
            <div class="grid_sub_24 clearfix">
                <button class="button-green" type="button" id="call-settings"><?php echo __('EDIT CALL SETTINGS'); ?></button>
                <button class="button-green" type="button" id="request-welcome-pack"><?php echo __('REQUEST WELCOME PACK');?></button>
                <button class="button-green" type="button" id="assign-products"><?php echo __('ASSIGN PRODUCTS');?></button>
                <button class="button-green" type="button" id="call-history"><?php echo __('VIEW USAGE');?></button>
                <button class="button-orange floatright" type="button" id="add-user"><?php echo __('ADD NEW USER');?></button>
                <button class="button-orange floatright" type="submit" id="delete-users" value="delete_users" name="function"><?php echo __('DELETE USERS');?></button>
            </div>
        </form>
    </div>
     <script type="text/javascript">
    $(document).ready(function() {
        myPwnApp.users.init('#frm-users');
        utils.buttonInitialise('#frm-users');
    });
    </script>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
