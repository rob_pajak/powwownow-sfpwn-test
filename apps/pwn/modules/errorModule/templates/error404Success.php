<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array(),
                'breadcrumbsTitle' => __('404'),
                'headingTitle' => __('Page not found'),
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <p><?php echo __('Sorry, the page you requested cannot be found.'); ?></p>
        <p><?php echo __('Try one of the following options:'); ?></p>
        <ul class="chevron-green">
            <li class="png">
                <?php echo __('Find what your looking for on our <a href="' . url_for('@sitemap') . '" title="Sitemap">sitemap</a>, return to the <a title="Powwownow homepage" href="' . url_for('@homepage') . '">homepage</a>'); ?>
            </li>
            <li class="png">
                <?php echo __('Or <a href="' . url_for('@contact_us') . '" title="Contact Us">contact us</a> to report a broken link'); ?>
            </li>
        </ul>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
