<?php $sf_response->setTitle(__('No Access')); ?>
<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('No Access'), 'user_type' => sfContext::getInstance()->getUser()->getAttribute('service','powwownow'))); ?>

    <div class="grid_24 clearfix">
        <h2 class="<?php echo sfContext::getInstance()->getUser()->getAttribute('service','powwownow'); ?> clearfix"><?php echo __('You do not have permission to access this page!'); ?></h2>
    </div>
    <div class="grid_24 clearfix">
        <?php if (sfContext::getInstance()->getUser()->hasCredential('plus') || sfContext::getInstance()->getUser()->hasCredential('premium')) : ?>
            <p><?php echo __("If this is incorrect, please contact your account administrator. Click either of the links below to continue:"); ?></p>
        <?php endif; ?>
        <ul>
            <li><a href="javascript:history.go(-1)"><?php echo __("Go back to previous page"); ?></a></li>
            <li><a href="/"><?php echo __("Go to Homepage"); ?></a></li>
        </ul>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
