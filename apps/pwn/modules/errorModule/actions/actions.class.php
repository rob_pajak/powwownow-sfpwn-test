<?php

/**
 * errorModule actions.
 *
 * @package    powwownow
 * @subpackage errorModule
 *
 */
class errorModuleActions extends sfActions
{

    /**
     * Tries to match a redirect, otherwise executes 404 Error action
     * @param sfWebRequest $request
     * @return bool|sfView::SUCCESS
     */
    public function executeError404(sfWebRequest $request)
    {
        // Gets url path from request without trailing slash
        $requestPath  = parse_url($request->getUri(), PHP_URL_PATH);
        $requestPath1 = rtrim($requestPath, '/');
        $requestPath2 = $requestPath . '?' . parse_url($request->getUri(), PHP_URL_QUERY);

        // Tries to perform redirect or rewrite
        try {
            if ($this->performRedirect($requestPath)) {
                return true;
            } elseif ($this->performRedirect($requestPath1)) {
                return true;
            } elseif ($this->performRedirect($requestPath2)) {
                return true;
            } else {
                $this->logMessage(
                    'Showing 404 Page. No Redirects Found for: '
                    . $requestPath
                    . ' or '
                    . $requestPath1
                    . ' or '
                    . $requestPath2
                    ,
                    'err'
                );
            }
        } catch (sfStopException $e) {
            $this->logMessage('Redirect Successful for ' . $requestPath, 'info');
        } catch (Exception $e) {
            $this->logMessage('Failed Redirects Caused An Exception for: ' . $requestPath, 'err');
        }

        // Redirect was Unsuccessful, therefore Default to showing 404 Page
        $this->setVar('user_type', strtolower($this->getUser()->getAttribute('service')));
        $this->getResponse()->setStatusCode(404);
        return sfView::SUCCESS;
    }

    /**
     * @return string
     */
    public function executeUnAuthenticated()
    {
        return sfView::SUCCESS;
    }

    /**
     * @return string
     */
    public function executeUnAuthenticatedPlus()
    {
        $this->setVar('isPlusUser', $this->getUser()->hasCredential('PLUS_USER'));
        $this->setVar('isFreeUser', $this->getUser()->hasCredential('POWWOWNOW'));
        return sfView::SUCCESS;
    }

    /**
     * Performs redirects for $uri if found in rewrite rules
     * @param $uri string
     * @return false|string -  if redirect rule not found
     *
     */
    private function performRedirect($uri)
    {
        // Get Redirect
        $redirects = $this->getRewriteRules();
        $redirect  = $redirects[$uri];

        // Check if a Redirect has been found
        if (!$redirect) {
            return false;
        }

        // Update Key since there is a Bug in the Code when extracting the Information from the YML File.
        // The YML File adds an extra ',' in the KEY, therefore existing code was throwing an exception and also redirecting old pages.
        $key      = str_replace(',', '', key($redirect));
        $redirect = array($key => $redirect[key($redirect)]);

        // Decide what to do with the Redirect Code
        switch ($key) {
            case 301:
            case 302:
                $this->redirect($redirect[$key], $key);
                break;
            default:
                return false;
        }

        return false;
    }

    /**
     * Returns content of $file - Uses output buffering as file_get_contents does not parse php
     *
     * @param $file
     * @return string file contents
     */
    private function getFileContents($file)
    {
        ob_start();
        include($file);
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    /**
     * Gets array of redirects for TS and SF pages, or rewrite rules for TS pages
     * @return mixed
     */
    private function getRewriteRules()
    {
        return sfYaml::load(sfConfig::get('sf_root_dir') . '/apps/pwn/config/redirects.yml');
    }
}
