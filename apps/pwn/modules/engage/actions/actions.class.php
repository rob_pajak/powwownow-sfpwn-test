<?php

/**
 * Engage
 *
 * @package    powwownow
 * @subpackage engage
 * @author     Asfer Tamimi
 *
 */
class engageActions extends sfActions
{

    /**
     * Download Engage [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeIndex()
    {
        $this->setVar('operatingSystem', $this->getOperatingSystem(getenv("HTTP_USER_AGENT")));
    }

    /**
     * How It Works [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeHowItWorks()
    {
    }

    /**
     * Get the Operating System from the User Agent
     *
     * @param string $userAgent
     * @return string
     * @author Asfer Tamimi
     *
     */
    private function getOperatingSystem($userAgent)
    {
        if (strpos($userAgent, "Win") !== false) {
            return "Windows";
        } elseif ((strpos($userAgent, "Mac") !== false) || (strpos($userAgent, "PPC") !== false)) {
            return "Mac";
        } else {
            return 'Other';
        }
    }

}
