<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix margin-top-10">
    <div class="grid_24 clearfix">
        <div class="margin-top-10">
            <?php include_partial(
                'commonComponents/genericBreadcrumbs',
                array('breadcrumbs' => array(), 'title' => __('How Powwownow Engage Works'))
            ) ?>
        </div>
        <div class="margin-top-20">
            <?php include_component(
                'commonComponents',
                'genericHeading',
                array('title' => __('How Powwownow Engage Works'), 'headingSize' => 'xl', 'user_type' => 'powwownow')
            ); ?>
        </div>
        <p class="margin-top-20-only">Powwownow Engage allows you to video call, VoIP, conference call, instant message and screen share all through a single intuitive interface. There’s no simpler way for people to share their ideas, all at once.</p>

        <div>
            <?php include_component(
                'commonComponents',
                'hTML5OutputVideo',
                array(
                    'config' => array(
                        'width'    => 460,
                        'height'   => 303,
                        'autoplay' => false,
                        'controls' => true
                    ),
                    'video' => array(
                        'mp4'   => '/sfvideo/pwn_engage.mp4',
                        'webm'  => '/sfvideo/pwn_engage.webm',
                        'ogg'   => '/sfvideo/pwn_engage.ogv',
                        'image' => '/sfimages/visualise/howEngageWorks.jpg'
                    ),
                    'called' => 'html'
                )
            ); ?>
        </div>

        <div class="hr-spotted-top png"></div>
        
        <div class="grid_sub_14">       
            <h3 class="green rockwell">The Powwownow Engage Interface</h3>
            <p>Once you have logged in you will be taken to a user friendly interface.</p>
            <p>From here you can select one or more of the following features to communicate with your contacts:</p>
            <ul class="chevron-green">
                <li>Presence – to check the availability of your contacts</li>
                <li>Instant Messaging – to chat with your contacts</li>
                <li>Video Call – to run an HD video conference</li>
                <li>Call – to run VoIP or conference call</li>
                <li>Screen Share – to instantly share your screen with your colleagues</li>
            </ul>
            <p>Alternatively, you can click the ‘Start’ arrow and choose to instantly start a web conference.</p>
        </div>

        <div class="grid_sub_10">
            <p class="text-align-right">
                <img src="/sfimages/visualise/engage_1.jpg" alt="" width="230" class="engageImageBorder"/>
            </p>
        </div>
        <div class="clearfix"></div>
        <div class="hr-spotted-top png"></div>

        <div class="grid_sub_14">
            <h3 class="green rockwell">Presence</h3>
            <p>A list of your contacts and their availability will appear on the left hand side of your interface. Presence notification will identify which of your contacts is available and the most appropriate way to contact them.</p>
            <ul class="chevron-green">
                <li>To set your Presence to show as ‘Available’ or ‘Do Not Disturb’ click on the ‘Available’ arrow at the top of your contacts list.</li>
                <li>Add contacts to your favourites by clicking their status – this will automatically move them to the top of your contact list, providing faster access to your most frequent contacts.</li>
            </ul>
        </div>
        <div class="grid_sub_10 clearfix">
            <div class="tooltip-grey-left margin-top-30">
                <div class="triangle-left-grey png top-20-pct"></div>
                <h4 class="green-dark">HANDY SHORTCUTS</h4>
                For handy shortcuts to change your status, right click the application in your task bar.
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="hr-spotted-top png"></div>

        <div class="grid_sub_14">
            <h3 class="green rockwell">Instant Messaging</h3>
            <p>Chat with your Participants one-to-one or in a group - simply type in the message box at the bottom of your  chat screen and hit enter.</p>
        </div>
        <div class="grid_sub_10">
            <div class="tooltip-grey-left">
                <div class="triangle-left-grey png top-20-pct"></div>
                <h4 class="green-dark">NOTIFICATIONS</h4>
                You will receive alerts that notify you when a message is received.
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="hr-spotted-top png"></div>

        <h3 class="green rockwell">Holding a video call</h3>
        <div class="grid_sub_14">
            <ol class="engageList">
                <li><span>Click the ‘Video Call’ button in the Powwownow Engage interface.</span></li>
                <li><span>The Powwownow Video desktop launches.</span></li>
                <li><span>Select the webcam, microphone and speakers you want to use.</span></li>
                <li><span>Click ‘Apply’ followed by ‘Cancel’ to proceed to the video desktop.</span></li>
                <li><span>When your Participants join the video you will see them appear in your video screen.</span></li>
            </ol>
            <h3 class="green rockwell">Sharing documents whilst video calling</h3>
            <p>To share documents click on the ‘share’ button in the video tool bar and select from the drop down, the document you would like to share.</p>
            <ul class="chevron-green">
                <li>The document you are sharing will appear within your shared screen.</li>
                <li>You can undock this document by double-clicking the application window. This is especially useful for Participants with 2 monitor screens.</li>
            </ul>
        </div>
        <div class="grid_sub_10">
            <div class="tooltip-grey-left">
                <div class="triangle-left-grey png top-20-pct"></div>
                <h4 class="green-dark">ECHO CANCELLATION</h4>
                It is only necessary to use echo cancellation if the device you are using does not have it built in, most modern headsets and speakers do.
            </div>
            <p class="text-align-right margin-bottom-10">
                <img src="/sfimages/visualise/engage_2.jpg" alt="" width="260" class="engageImageBorder"/>
            </p>
        </div>
        <div class="clearfix"></div>
        <div class="hr-spotted-top png"></div>

        <h3 class="green rockwell">Call</h3>
        <p>Click the ‘Call’ button and you will be presented with the following options:</p>
        <ul class="chevron-green">
            <li>Call using VoIP from your PC via the internet free of charge. Ensure you and your colleagues have a headset plugged in.</li>
            <li>Call using a phone you will be provided with a dial-in number and PIN for you and your Participants to dial-in with.</li>
        </ul>

        <div class="hr-spotted-top png"></div>
        <div class="grid_sub_14">
            <h3 class="green rockwell">Screen Sharing</h3>
            <p>To start sharing your screen with contacts click the ‘Screen Share’ button.</p>
            <ul class="chevron-green">
                <li>Chat and share your screen individually with each of your contacts by clicking their name.</li>
                <li>Add other users to your conversation by clicking the 'Add Contacts' button on your chat window.</li>
                <li>To show your screen to various contacts at the same time, click on the 'Start' arrow, 'Start sharing your screen' and choose the contacts you want to show your screen to. Your participants will be notified when you have started screen-sharing.</li>
            </ul>
            <p><strong>Ask to see</strong></p>
            <p>Using this feature will allow you to see your Participants’ screen.  You can swap presenters during a screen-sharing meeting with one or more contacts.</p>
            <ul class="chevron-green">
                <li>Select the contact you want to share a screen with.</li>
                <li>Select ‘Ask to see’ in the ‘Screen Share’ drop down.</li>
                <li>The Participants will get a pop-up asking him/her to show you his/her screen.</li>
            </ul>
            <p><strong>Choose what to show</strong></p>
            <p>You can select this option from the ‘Screen Share’ drop down menu. This allows you to select the application you wish to share with your Participants.</p>
            <h3 class="green rockwell">Screen sharing controls</h3>
            <p></p>
            <ul class="chevron-green">
                <li><strong>Pause:</strong> Allows you to pause your screen share so that your participants will see the last image frozen. Click '<strong>Restart</strong>' when you're ready to continue sharing your screen.</li>
                <li><strong>Change Quality:</strong> Allows you to select from 'Fastest/Low colour', 'Normal speed/Good colour' or 'Slowest/Full colour' depending on your preferences.</li>
                <li><strong>Remote Control:</strong> Allows another user to control your keyboard and mouse.</li>
            </ul>
            <p>
                1) Your participant needs to click the '<strong>Request Control</strong>' button in their screen sharing window.<br/>
                2) You need to accept the request by clicking '<strong>Share</strong>' in the pop-up (permission is always required).<br/>
                3) The '<strong>Release Control</strong>' button releases their control over your computer.<br/>
                4) The '<strong>Re-take Control</strong>' button can be clicked at any time by you in order to regain control.
            </p>
        </div>
        <div class="grid_sub_10">
            <p class="text-align-right">
                <img src="/sfimages/visualise/engage_3.jpg" alt="" width="230" class="engageImageBorder"/>
            </p>
            <div class="tooltip-grey-left margin-top-230">
                <div class="triangle-left-grey png"></div>
                <h4 class="green-dark">PAUSE</h4>
                This can be useful when you want to open up a file which you do not want your participants to see.
            </div>
            <div class="tooltip-grey-left">
                <div class="triangle-left-grey png"></div>
                <h4 class="green-dark">CHANGE QUALITY</h4>
                ‘Fastest’ is the standard setting for screen sharing – use this when you require speed over quality.  Use ‘Full colour’ to replicate your screen in the exact same quality as you see it.
            </div>
        </div>
        <div class="clear"></div>
        <div class="hr-spotted-top png"></div>

        <div class="grid_sub_14">
            <h3 class="green rockwell">Hold a web conference</h3>
            <div>
                <ol class="engageList">
                    <li><span>Start your web conference by clicking on the ‘Start’ arrow, ‘Start Web Conference’.</span></li>
                    <li><span>Copy the website link and your unique PIN and send this to your Participants via email.</span></li>
                    <li><span>Click on the '<strong>Screen Share</strong>' button to start showing your screen.</span></li>
                    <li><span>Participants join using a standard web browser and by visiting <a href="http://powwownow.yuuguu.com">http://powwownow.yuuguu.com</a> where they enter your unique PIN and their name.</span></li>
                    <li><span>When participants join, you will see them appearing on your Powwownow Web chat window. Participants are now viewing your screen.</span></li>
                </ol>
            </div>
        </div>
        <div class="grid_sub_10">
            <p class="text-align-right margin-bottom-10">
                <img src="/sfimages/visualise/engage_4.jpg" alt="" width="230" class="engageImageBorder"/>
            </p>
        </div>
        <div class="clear"></div>
        <div class="hr-spotted-top png"></div>
        <div class="grid_sub_14">
            <h3 class="green rockwell">User guide</h3>
            <p>For a detailed step-by-step user guide, simply click on the link below, and keep a printer-friendly summary to hand.</p>
            <p><a href="/sfpdf/en/Powwownow-Engage-User-Guide.pdf" target="_blank">Powwownow Engage user guide</a></p>
        </div>
        <div class="clear"></div>
        <div class="hr-spotted-top png"></div>
        <div class="grid_sub_14">
            <h3 class="green rockwell">Tips</h3>
            <p>Here are some helpful tips to enhance your Powwownow Engage experience.</p>
            <p><a href="/sfpdf/en/Powwownow-Engage-Top-Tips.pdf" target="_blank">Powwownow Engage top tips</a></p>
        </div>
    </div>
</div>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'footer'); ?>
</div>
