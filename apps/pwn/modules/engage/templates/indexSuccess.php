<div class="container_24 clearfix">
	<div class="grid_5">
		<a href="/" id="logolink">
			<img src="/sfimages/powwownow_logo.png" alt="Powwownow Free Conference Call Provider" id="logo" class="png"/>
		</a>
	</div>

	<div class="grid_24">
		<h1>Download Powwownow Engage</h1>
		<h3 class="subheading" style="padding-top: 20px; color: #7B9EAE">All together, now…</h3>
		<p>Download and install the Powwownow Engage application by simply clicking the download button below. Further instructions on how to install this product are demonstrated below.</p>
		<p>Once the application is installed, the Powwownow Engage start-up window will appear. Here you will need to log in with your email address and password.</p>
	</div>

	<div class="grid_24">
        <?php if ($operatingSystem === 'Windows' || $operatingSystem === 'Other') : ?>
            <div class="instructions" id="WindowsIns">
                <div class="grid_sub_8 aligncenter">
                    <img src="/sfimages/visualise/download_install_pc.jpg" alt="Install"/><br/><br/>
                    <div class="instruction">1. Click Run to install the application</div>
                </div>
                <div class="grid_sub_8 aligncenter">
                    <img src="/sfimages/visualise/install_visualise_pc.png" alt="Start"/><br/><br/>
                    <div class="instruction">2. Click Finish to start Powwownow Engage</div>
                </div>
                <div class="grid_sub_8 aligncenter">
                    <img src="/sfimages/visualise/login_visualise_pc.jpg" alt="Log In"/><br/><br/>
                    <div class="instruction">3. Log in with your email address and password</div>
                </div>
            </div>
        <?php elseif ($operatingSystem === 'Mac') : ?>
            <div class="instructions" id="MacIns">
                <div class="grid_sub_8 aligncenter">
                    <img src="/sfimages/visualise/mac1.png" alt="Install"/><br/><br/>
                    <div class="instruction">1. Download the application</div>
                </div>
                <div class="grid_sub_8 aligncenter">
                    <img src="/sfimages/visualise/mac2.png" alt="Start"/><br/><br/>
                    <div class="instruction">2. Click Open to start Powwownow Engage</div>
                </div>
                <div class="grid_sub_8 aligncenter">
                    <img src="/sfimages/visualise/mac3.png" alt="Log In"/><br/><br/>
                    <div class="instruction">3. Log in with your email address and password</div>
                </div>
            </div>
        <?php endif ?>
	</div>
	<div class="grid_24" id="download-container">
		<?php if ($operatingSystem === 'Windows' || $operatingSystem === 'Other') : ?>
			<a href="/sfdownload/powwownow-installer.exe" class="button-orange" id="download-btn">Download for Windows</a>
		<?php elseif ($operatingSystem === 'Mac') : ?>
			<a href="/sfdownload/Powwownow.app.zip" class="button-orange" id="download-btn">Download for Mac</a>
		<?php endif ?>	
	</div>

	<div class="grid_24">
		<?php include_component('commonComponents', 'footer'); ?>
	</div>
</div>
