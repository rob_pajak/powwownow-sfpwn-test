<div class="hr-spotted-top png content-seperator"><!--blank--></div>
<!--Left Column-->
<div class="grid_10 clearfix">
    <!--Main Text-->
    <h3>Choose your Numbers</h3>
    <p>Now you have created your welcome message you need to select the numbers you require.</p>
    <p>Selecting your numbers is very simple, click the required number type in the table on the right and you will see it appear in the table below. You can add as many numbers as you like.</p>
    <p>All calls made to these numbers will be answered with the branded welcome message you have just created. If you require a different branded welcome message you will need to create a new product.</p>

    <!--White Box including Top Arrow-->
    <div class="bwm-step2-container">
<!--        <div class="bwm-arrow-up"></div>-->
        <h3>Your Dedicated Numbers</h3>
        <p>Listed below are the numbers you have selected:</p>
        <div class="rnd_tbl_wrapper">
        <table cellpadding="0" cellspacing="0" border="0" class="mypwn plus" id="selectedRates">
            <thead>
                <tr class="">
                    <th>Country</th>
                    <th>City</th>
                    <th>Type</th>
                    <th>Rate per min</th>
                </tr>
            </thead>
            <tbody>
                <tr class="bwm-no-number">
                    <td colspan="4" class="aligncenter"><em>No number selected</em></td>
                </tr>
            </tbody>
        </table>
        </div>
        <div id="bwmCharges">
            <table cellpadding="0" cellspacing="0" border="0" >
                <tr class="connection-fee">
                    <td><?php echo __('Branded Welcome Message') ?></td>
                    <td class="bwm-one-off-price">£50</td>
                </tr>
                <tr>
                    <td class="alignright"><?php echo __('Total:') ?></td>
                    <td class="bwm-total-price">£74</td>
                </tr>
            </table>
        </div>
        <div class="form-action margin-top-20" style="text-align: left">
            <button type="button" class="button-orange bwm-cancel plus-button-rel">
                <span>Cancel</span>
            </button>
            <button type="button" class="button-green plus-button-rel" id="addNumbers">
                <span>Continue</span>
            </button>
        </div>
    </div>
</div>

<!--Right Column-->
<div class="grid_13 push_1 clearfix">
    <!--Simple Number Details-->
    <div style="padding: 15px">
        <strong>UK Freephone Numbers</strong>
        <p>Do you want your UK call participants to dial-in for free? With our UK Freephone number your call participants can dial-in from a landline without paying a penny!</p>

        <strong>UK Landline Numbers</strong>
        <p>Local rate numbers that are generally included in most mobile contracts and landline call packages, allowing you to call for free or for a very low cost.</p>
    </div>

    <!--Branded Welcome Message Rates Table-->
    <table cellpadding="0" cellspacing="0" border="0" class="mypwn plus" id="bwmRates" title="Click to select">
    </table>

</div>