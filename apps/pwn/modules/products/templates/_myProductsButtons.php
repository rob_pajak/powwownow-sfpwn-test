<button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('products_select');?>'">
    <span>purchase products</span>
</button>

<?php if(!$adminHasUsers && $balance == 0): ?>

    <?php if(!$isPostPay): ?>
    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('products_select');?>'">
        <span>purchase call credit</span>
    </button>
    <?php endif; ?>

    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('create_user');?>'">
        <span>invite users</span>
    </button>

<?php elseif (!$adminHasUsers && $hasPurchasedAProduct && $balance >= 5): ?>

    <?php if(!$isPostPay): ?>
    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('create_user');?>'">
        <span>purchase call credit</span>
    </button>
    <?php endif; ?>

<?php elseif (!$adminHasUsers && $balance >= 5): ?>

    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('create_user');?>'">
        <span>create users</span>
    </button>

<?php elseif (!$atLeastOneUserHasNoProducts && $balance < 5 && !$autoTopupEnabled): ?>

    <?php if(!$isPostPay): ?>
    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('products_select');?>'">
        <span>purchase call credit</span>
    </button>
    <?php endif; ?>

<?php elseif ($adminHasUsers && !$atLeastOneUserHasNoProducts && $balance >= 5): ?>

    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('products_assign');?>'">
        <span>assign products</span>
    </button>

<?php elseif ($atLeastOneUserHasNoProducts && $balance < 5): ?>
    <?php if(!$isPostPay): ?>
    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('products_select');?>'">
        <span>purchase call credit</span>
    </button>
    <?php endif; ?>

    <button class="button-orange floatright" type="button" onclick="window.location='<?= url_for('products_assign');?>'">
        <span>assign products</span>
    </button>

<?php endif; ?>
