<div class="tooltip product<?php echo $productGroup['product_group_id']; ?>" style="display:none;">

    <div class="tooltip-arrow-top png"></div>

    <a class="floatright sprite tooltip-close" href="#"><span class="sprite-cross-white png"></span></a>

    <div class="product-accordion">

        <h4><?php echo __('Product Description'); ?>:</h4>

        <div>
            <?php
            $group = $sf_data->getRaw('productGroup');
            echo $group['group_description'];
            ?>
        </div>

        <?php if ($isBwm && $isPlusAdmin): ?>

            <h4><?php echo __('Product Price:'); ?></h4>

            <div>
                <table class="product-pricing">
                    <tbody>

                        <?php $i = 0; ?>

                        <tr class="<?php echo $i++ % 2 ? 'odd' : ''; ?>">
                            <td><?php echo __('Recording of personalised branded welcome message:'); ?></td>
                            <td><?php echo format_currency($bwmAccountConnectionFee, $currencySymbol); ?></td>
                        </tr>

                        <tr class="<?php echo $i++ % 2 ? 'odd' : ''; ?>">
                            <td>
                                <?php echo __('Annual charge for dedicated number(s): '); ?>
                                (<?php echo $dnisCount; ?> X <?php echo format_currency($ratePerBwmDnis, $currencySymbol); ?>)
                            </td>
                            <td><?php echo format_currency($totalDnisCost, $currencySymbol); ?></td>
                        </tr>

                        <tr class="<?php echo $i++ % 2 ? 'odd' : ''; ?>">
                            <td style="text-align:right;"><b><?php echo __('Total: '); ?></b></td>
                            <td><b><?php echo format_currency($totalProductCost, $currencySymbol); ?></b></td>
                        </tr>

                    </tbody>
                </table>

                <?php if (isset($nextChargeDate) && $nextChargeDate) : ?>
                    <p><?php echo __('The next charge date for your numbers will be %chargeDate%.', array('%chargeDate%' => $nextChargeDate)); ?></p>
                <?php else: ?>
                    <p><?php echo __('The annual renewal date for your dedicated dial-in numbers will start from the date your branded welcome message is uploaded.'); ?></p>
                <?php endif; ?>

            </div>

        <?php endif; ?>

        <?php if (!$isBundle && $hasBeenPurchased  && !empty($productRates[$productGroup['product_group_id']])) : ?>

            <h4><?php echo __('Dial-in Numbers and Rates'); ?></h4>
            <div>
                <span><?php echo $productGroup['dnis_summary']; ?></span>

                <table class="product-numbers plus mypwn">
                    <thead>
                        <tr>
                            <?php if ($isBwm && $hasBeenPurchased) : ?>
                                <th><?php echo __('Number');?></th>
                            <?php endif; ?>
                            <th><?php echo __('Country'); ?></th>
                            <th><?php echo __('Type'); ?></th>
                            <th><?php echo __('Rate per min'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;

                        foreach ($productRates[$productGroup['product_group_id']] as $dnis): ?>

                            <tr class="<?php echo $i++%2 ? 'odd' : ''; ?>">

                                <?php
                                $country = (empty($dnis['country'])) ? $dnis['country_en'] : $dnis['country'];

                                // Decide which dialin number is best to display
                                if ($isBwm && $hasBeenPurchased) {

                                    if (!empty($dnis['international_formatted'])) {
                                        $dnisNumber = $dnis['international_formatted'];
                                    } elseif (!empty($dnis['national_formatted'])) {
                                        $dnisNumber = $dnis['national_formatted'];
                                    } else {
                                        $dnisNumber = $dnis['in_country_number'];
                                    }

                                    echo '<td>' . $dnisNumber . '</td>';
                                }
                                ?>

                                <td><?php echo $country; ?></td>
                                <td><?php echo ($dnis['dnis_type'] == 'Geographic' ? 'Landline' : $dnis['dnis_type']); ?></td>
                                <td><?php echo __('%cost% pence', array('%cost%' => ($dnis['rate'] * 100))); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>

    </div>
</div>
