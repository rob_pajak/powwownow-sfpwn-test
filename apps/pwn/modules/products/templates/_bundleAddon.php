<tr class="addon addon-<?php echo $productId ?>">
    <td class="product-name">
        <span class="addon-indicator">+</span>
        <div class="product-name product<?php echo $productId ?>">
            <?php echo __('International Numbers Add On (monthly contract)') ?>
        </div>
    </td>
    <td class="one-off charge"></td>
    <td class="monthly charge"><?php if (isset($addon['monthly_charge'])) echo $addon['monthly_charge'] ?></td>
    <td class="yearly charge"><?php if (isset($addon['yearly_charge'])) echo $addon['yearly_charge'] ?></td>
    <td class="total charge"><?php echo $addon['total'] ?> <span>(pro-rata charge)</span></td>
    <?php if (!$disableProductActions): ?>
        <td></td>
    <?php endif ?>
</tr>