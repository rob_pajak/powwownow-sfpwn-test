<?php $classes = 'product-basket-removal-action'; ?>
<?php if (!$markedAsAdded): ?>
    <?php $classes .= ' unadded-product' ?>
<?php endif; ?>
<?php if (!empty($extraClasses)): ?>
    <?php $classes .= ' ' . $extraClasses; ?>
<?php endif; ?>

<?php echo form_tag(url_for('remove_from_basket'), array('enctype' => 'application/x-www-form-urlencoded', 'class' => $classes)); ?>
    <input type="hidden" name="remove-product" value="<?php echo $productGroupId ?>">
    <button title="Remove" type="submit" value="Remove from basket"
            class="<?php echo !$markedAsAdded ? "button-disabled" : "";?>" style="position:relative; left:0px;top:0px;"
        <?php if (!$markedAsAdded) { echo 'disabled="disabled"'; } ?>>
        <span>Remove from basket</span>
    </button>
</form>