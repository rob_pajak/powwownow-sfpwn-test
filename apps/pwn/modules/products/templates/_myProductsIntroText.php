<?php if(!$adminHasUsers && $balance == 0): ?>
    <?php if(!$isPostPay): ?>
        <p><?php echo __('To use Worldwide and UK Freephone and Landline numbers <a href="'. url_for('products_select') .'">add call credit</a> to your account.');?></p>
    <?php endif; ?>
    <p><?php echo __('Don\'t forget you can <a href="'. url_for('create_user') .'">add and invite users</a> to your account.');?></p>
<?php elseif (!$adminHasUsers && $hasPurchasedAProduct && $balance >= 5): ?>
    <p>
        <?php echo __(
            'Don\'t forget you can <a href="%1%">manage multiple users</a> under your account.</p>',
            array('%1%' => array('%1%' => url_for('create_user')))
        );
        ?>
    </p>

<?php elseif (!$adminHasUsers && $balance >= 5): ?>

    <p>
        <?php echo __(
            'Don\'t forget you can <a href="%1%">manage multiple users</a> under your account.</p>',
            array('%1%' => array('%1%' => url_for('create_user')))
        );
        ?>
    </p>

<?php elseif (!$atLeastOneUserHasNoProducts && $balance < 5 && !$autoTopupEnabled): ?>

    <?php if(!$isPostPay): ?>
    <p>
        <?php echo __(
            'Your call credit is below £5, to make your next conference call you need to <a href="%1%">top up your call credit</a>.',
            array('%1%' => url_for('products_select')));
        ?>
    </p>
    <?php endif; ?>

<?php elseif ($adminHasUsers && !$atLeastOneUserHasNoProducts && $balance >= 5): ?>

    <p>
        <?php echo __(
            'There are users on your account that do not have products assigned to their PINs. Manage your users products %assign_products_link%.',
            array('%assign_products_link%' => link_to('here', '@products_assign')));
        ?>
    </p>

<?php elseif ($atLeastOneUserHasNoProducts && $balance < 5): ?>

    <?php if(!$isPostPay): ?>
    <p>
        <?php echo __(
            'Your call credit is below £5, to make your next conference call you need to <a href="%1%">top up your call credit</a>.',
            array('%1%' => url_for('products_select')));
        ?>
    </p>
    <?php endif; ?>

    <p>
        <?php echo __(
            'There are users on your account that do not have products assigned to their PINs. Manage your users products %assign_products_link%.',
            array('%assign_products_link%' => link_to('here', '@products_assign')));
        ?>
    </p>

<?php else: ?>

    <p><?php echo __('Here you can view the features and products you have assigned to your account.');?></p>
    <p><?php echo __('Download our <a href="' . url_for("@web_conferencing_auth") . '">FREE screen sharing</a> tool for your next conference call and show people what you’re talking about.');?></p>

<?php endif; ?>

<?php if (!$hasBWM && $isPostPay): ?>
    <p><?php echo __('Purchase a Branded Welcome Message and dedicated UK Landline or Freephone number and help your company stand out from the crowd.') ?></p>
<?php endif; ?>
