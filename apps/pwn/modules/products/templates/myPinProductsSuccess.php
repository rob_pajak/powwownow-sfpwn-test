<?php $odd = 1; ?>
<?php include_component('commonComponents', 'header'); ?>

<!-- Dialogs - Start -->
<div id="dialog-modal-confirm-update" title="Products updated" class="not-show">
    <p><?php echo __('Your users have been updated.'); ?></p>
    <?php if (empty($isPostPayCustomer)): ?>
    <p id="dialog-modal-topup" class="not-show"><?php echo __('You have less than £5 call credit on your account'); ?>, <a href="<?php echo url_for('@products_select') ?>"><?php echo __('purchase call credit'); ?></a> <?php echo __('to use these numbers'); ?>.</p>
    <?php endif; ?>

    <div id="submodal-notify-users" class="not-show">
        <p><strong><?php echo __('Send an account update email notification to your users by simply checking the box next to the relevant user:') ?></strong></p>
        <form id="notify-users-of-product-assignment" action="<?php echo url_for('notify_users_of_product_assignment') ?>" method="post">
            <ul id="notifiable-contacts"></ul>
        </form>
    </div>
</div>
<div id="dialog-modal-notify-users-success" class="not-show">
    <p><?php echo __('Email notification successful.') ?></p>
</div>
<div id="dialog-modal-notify-users-failure" class="not-show">
    <p><?php echo __('Your email notification to the following users was unsuccessful:') ?></p>
    <ul id="failed-notifications"></ul>
</div>
<div id="dialog-modal-notify-users-backend-error" class="not-show">
    <p><?php echo __('An error occurred whilst trying to notify your users. Please try again later.') ?></p>
</div>
<!-- Dialogs - End -->

<!-- Main content -->
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array(
            'title'              => __('Assign Products'),
            'headingSize'        => 'm',
            'breadcrumbs'        => array(),
            'disableBreadcrumbs' => true
        )
    ); ?>
    <div class="grid_24">
        <?php if (count($assignedProducts)): ?>
            <p><?php echo __('Here you can manage the products you wish to make available for the users on your account depending on their individual conference calling requirements.'); ?></p>
            <p><?php echo __('Remember, all purchased products, including Bundles, are automatically assigned to all users. This ensures that everyone is getting the most out of the Powwownow account and is able to make the cheapest calls available to them.') ?></p>
            <p><?php echo __('Simply check the product box next to the appropriate user in the table below and click Update.') ?></p>
            <?php echo form_tag(url_for('products/myPinProductsAjax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-update-my-pin-products', 'class' => 'pwnform clearfix fx-frm')); ?>
                <div id="form-response-placeholder"></div>
                <div id="pinproductscontainer">
                    <table class="mypwn mypwn-fixed plus" id="pinproducts">
                        <thead>
                            <tr>
                                <th class="pinpair fixed-column">&nbsp;&nbsp;<?php echo __('Name / Email'); ?></th>
                                <?php foreach ($assignedProducts as $id => $groups) : ?>
                                    <?php if (isset($tableCellAlign) && 'center' == $tableCellAlign) : ?>
                                    <th class="pinproduct normal-column" style="text-align:center; width: 220px;">
                                <?php else : ?>
                                    <th class="pinproduct normal-column" style="width: 220px;">
                                <?php endif; ?>
                                        <input title="<?php echo __('Apply to all'); ?>" type="checkbox" value="p<?php echo $groups['product_group_id']; ?>" class="checkall va-middle" <?php if ($groups['is_individual_bundle']) echo 'disabled="disabled"' ?>/>
                                        <?php if (empty($groups['group_name_short'])) : ?>
                                        <span title="<?php echo $groups['group_name_en'] . ' | PIN Connection Cost: ' . format_currency($groups['pin_connection_fee'], $groups['pin_connection_currency']); ?>" class="group-short productheader" data-product="<?php echo $groups['product_group_id']; ?>">
                                            <?php echo $groups['group_name_en']; ?>
                                        </span>
                                        <?php else : ?>
                                        <span title="<?php echo $groups['group_name'] . ' | PIN Connection Cost: ' . format_currency($groups['pin_connection_fee'], $groups['pin_connection_currency']); ?>" class="group-short productheader" data-product="<?php echo $groups['product_group_id']; ?>" <?php if (isset($featureProducts[$groups['product_group_id']])) echo 'style="border-bottom: 1px dashed white; cursor: help;"' ?>>
                                            <?php echo $groups['group_name_en']; ?>
                                        </span>
                                        <?php endif; ?>
                                    </th>
                                <?php endforeach; ?>
                                <th class="autosize normal-column" style="width: 10px; padding: 0;"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($usersArr as $i => $userInfo) : ?>
                            <?php echo ($odd % 2) ? '<tr class="odd">' : '<tr>'; ?>
                                <td style="height:30px" class="pinpair"><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name'] . '<br/>' . $userInfo['email']; ?></td>
                                <?php foreach ($assignedProducts as $j => $groups) : ?>
                                    <?php $adminFeature = isset($userInfo['featureInfo'][$groups['product_group_id']]) && $userInfo['featureInfo'][$groups['product_group_id']] == true ? true : false; ?>
                                    <?php if (isset($userInfo['pinInfo'][$groups['product_group_id']])) : ?>
                                        <?php if ($groups['is_individual_bundle'] && !in_array($groups['product_group_id'], $assignedIndividualBundleIds)) : ?>
                                            <td class="pinproduct normal-column pinproduct-<?php echo $groups['product_group_id']; ?>">
                                                <input type="radio" name="p<?php echo $groups['product_group_id']; ?>[]" id="p<?php echo $groups['product_group_id']; ?>[]" value="<?php echo $userInfo['contact_ref']; ?>"/>
                                            </td>
                                        <?php else: ?>
                                            <?php $checkMe = (true === ($userInfo['pinInfo'][$groups['product_group_id']])); ?>
                                            <td class="pinproduct <?php echo ($checkMe) ? 'normal-column' : ''; ?> pinproduct-<?php echo $groups['product_group_id']; ?> <?php if (!empty($groups['is_bundle'])) echo 'bundle-row' ?> <?php if (!empty($releventBundleLandLineFeatures[$groups['product_group_id']])) echo 'relevant-bundle-feature' ?>">
                                                <?php if ($adminFeature || $groups['is_individual_bundle']): ?>
                                                <input disabled type="checkbox" value="<?php echo $userInfo['contact_ref']; ?>" <?php echo ($checkMe) ? 'checked="checked"' : ''; ?>/>
                                                <input type="hidden" name="p<?php echo $groups['product_group_id']; ?>[]" value="<?php echo $userInfo['contact_ref']; ?>"/>
                                                <?php else: ?>
                                                <input type="checkbox" name="p<?php echo $groups['product_group_id']; ?>[]" value="<?php echo $userInfo['contact_ref']; ?>" <?php echo ($checkMe) ? 'checked="checked"' : ''; ?>/>
                                                <?php endif; ?>
                                            </td>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td class="autosize normal-column" style="width: 10px; padding: 0;"></td>
                            </tr>
                            <?php $odd++; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div>
                    <button id="adminupdate" type="submit" class="button-orange floatright pinproductssubmit">
                        <span><?php echo __('Update'); ?></span>
                    </button>
                </div>
            </form>
            <div class="tooltip-container">
                <?php foreach ($featureProducts as $id => $groups) : ?>
                    <?php include_component(
                        'products',
                        'productTooltip',
                        array('productGroup' => $groups, 'productRates' => $listUsageRates)
                    ) ?>
                <?php endforeach; ?>
            </div>
        <?php else : ?>
            <div class="error message">
                <?php echo __('You currently do not have any products assigned to your account. To view available products, <a href="' . url_for('products/index') . '">click here</a>.'); ?>
            </div>
        <?php endif; ?>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>

<?php if (!empty($releventBundleLandLineFeatures)): ?>
    <div id="relevant-bundle-feature-disabled-dialog">
        <p><?php echo __('You have removed a bundle from a user/s that has UK Landline or WW Landline numbers enabled, if you wish to disable these numbers you can do so here, otherwise any usage on these numbers by the user will be charged at outside bundle standard rate.'); ?></p>
    </div>
<?php endif; ?>
