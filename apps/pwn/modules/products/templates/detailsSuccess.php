<div class="clearfix container_24" style="width: 98%; margin: 0 1%;">
    <div class="grid_24">
        <div class="grid_sub_15">
            <div class="clearfix">
                <img class="floatleft" width="48" height="48" src="<?php echo $productGroup['group_thumbnail'];?>" alt="<?php echo $productGroup['group_name'];?>"/>
                <h2 class="rockwell blue productdetailshead"><?php echo $productGroup['group_name'];?></h2>
            </div>
            <div class="clearfix">
                <?php echo $productGroup['group_full_description']; ?>
            </div>
        </div>
        <div class="grid_sub_8 push_1 margin-top-20 ">
            <div class="whatNextContainer" style="display: none">
                <h2 class="margin-bottom-20 rockwell blue"><?php echo __("What's next?"); ?></h2>
                <?php if ($userType === 'ANON' || $userType === 'POWWOWNOW') : ?>
                    <div class="clearfix" style="height: 50px;">
                        <label class="floatleft margin-top-5">
                            <?php echo __('To start using ' . $productGroup['group_name'] . ', register now for Powwownow Plus for FREE.'); ?>
                        </label>
                        <button data-tracking-id="Product-Details" type="submit" class="button-orange floatright get-plus">
                            <span><?php echo __('Register'); ?> </span>
                        </button>
                    </div>
                <?php elseif ($userType == 'PLUS_ADMIN' && $productGroupEnabled) : ?>
                    <div>
                        <p><?php echo $productGroup['group_name'] . __(' is already enabled on your account.'); ?></p>
                    </div>
                    <div>
                        <button type="submit" class="button-orange" onclick="window.location='<?php echo url_for("products/index");?>'">
                            <span><?php echo __('Select Products');?></span>
                        </button>
                        <button type="submit" class="button-orange" onclick="window.location='<?php echo url_for("products/myPinProducts");?>'">
                            <span><?php echo __('Assign Products');?></span>
                        </button>
                    </div>
                <?php elseif ($userType == 'PLUS_ADMIN' && !$productGroupEnabled) : ?>
                    <div class="clearfix">
                        <label class="floatleft margin-top-5" >
                            <?php echo __('To start using ' . $productGroup['group_name'] . ', enable the product on your PLUS account.'); ?>
                        </label>
                        <?php echo form_tag(url_for('products_select'), array('enctype' => 'application/x-www-form-urlencoded', 'class' => 'form-enable-product pwnform clearfix')); ?>
                        <input type="hidden" name="productGroupID" value="<?php echo $productGroup['product_group_id']; ?>"/>
                        <button type="submit" class="button-orange floatright">
                            <span><?php echo __('Enable'); ?></span>
                        </button>
                        </form>
                    </div>
                <?php elseif ($userType == 'PLUS_USER' && $productGroupEnabled) : ?>
                    <div>
                        <p><?php echo $productGroup['group_name'] . __('is already enabled on your account.'); ?></p>
                    </div>
                <?php elseif ($userType == 'PLUS_USER' && !$productGroupEnabled) : ?>
                    <div>
                        <label class="floatleft margin-top-5" style="width: auto;">
                            <?php echo __('To start using ' . $productGroup['group_name'] . ', please contact your PLUS Account Administrator.'); ?>
                        </label>
                    </div>
                <?php endif; ?>
                <?php if (!$productGroupEnabled) : ?>
                    <div>
                        <p>
                            <?php echo __('Then, simply select the product you wish to assign to your account (can be either UK or Worldwide), purchase credit and you’ll be ready to start conference calling - it really is that simple!'); ?>
                        </p>
                    </div>
                <?php endif; ?>
            </div>

            <table border="0" class="mypwn plus floatleft" id="usageRates<?php echo $productID; ?>" style="padding: 0; border-spacing: 0">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>City</th>
                        <th>Type</th>
                        <th>Rate per min</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $odd = 0; ?>
                    <?php foreach ($usageRates as $i => $details) : ?>
                    <tr class="<?php echo ($odd++%2 == 0) ? 'odd' : 'even'; ?>">
                        <td><?php echo $details['country']; ?></td>
                        <td><?php echo $details['city']; ?></td>
                        <td><?php echo $details['type']; ?></td>
                        <td><?php echo $details['rate']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <?php if (($productGroup['group_name'] == 'Worldwide Freephone Numbers') || ($productGroup['group_name'] == 'UK Freephone Number')) : ?>
                <p>Calls are charged at a rate per minute, so for example:</p>
                <p>A 30 minute conference call, with 4 callers plus the Chairperson, all using a UK Freephone number (7p/min) would cost you £10.50:</p>
                <p><strong>7p x 30 mins x 5 participants = £10.50</strong></p>
            <?php endif; ?>

            <?php if (($productGroup['group_name'] == 'Worldwide Landline Numbers') || ($productGroup['group_name'] == 'UK Landline Number')) : ?>
                <p>Calls are charged a rate per minute, so for example:</p>
                <p>A 30 minute conference call, with 4 callers plus the Chairperson, all using a UK Landline number (4.3p/min) would cost the Chairperson £6.45:</p>
                <p><strong>4.3p x 30 mins x 5 participants = £6.45</strong></p>
            <?php endif; ?>

            <?php if ($userType == 'ANON') : ?>
                <p>If you wish to find out more about Plus, or any of our services, please <a href="<?php echo url_for('@contact_us'); ?>">contact us</a>.</p>
            <?php endif; ?>
        </div>
    </div>
    <script>
        $(function() {
            var productTableID = '#usageRates<?php echo $productID; ?>';

            window.oTableScreen<?php echo $productID; ?> = $(productTableID).dataTable({
                "oLanguage" : {
                    "sProcessing": DATA_TABLES_sProcessing,
                    "sLengthMenu": DATA_TABLES_sLengthMenu,
                    "sZeroRecords": DATA_TABLES_sZeroRecords,
                    "sEmptyTable": DATA_TABLES_sEmptyTable,
                    "sLoadingRecords": DATA_TABLES_sLoadingRecords,
                    "sInfo": DATA_TABLES_sInfo,
                    "sInfoEmpty": DATA_TABLES_sInfoEmpty,
                    "sInfoFiltered": DATA_TABLES_sInfoFiltered,
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sSearch": DATA_TABLES_sSearch,
                    "sUrl": "",
                    "fnInfoCallback": null,
                    "oPaginate": {
                        "sFirst":    '&lt;&lt;',
                        "sPrevious": '&lt;',
                        "sNext":     '&gt;',
                        "sLast":     '&gt;&gt;'
                    }
                },
                "sPaginationType": "full_numbers",
                "bFilter": true,
                "bInfo": false,
                "bLengthChange": false,
                "iDisplayLength": 8,
                "fnDrawCallback":function(){
                    //init_table();
                }
            });

            if (window.oTableScreen<?php echo $productID; ?>.fnGetData().length <= 10) {
                $(productTableID + '_paginate').hide();
            }
            window.oTableScreen<?php echo $productID; ?>.fnSort([[0,'asc']]);

            // Styling Changes
            $(productTableID + 'th')
                .append('<div></div>')
                .css('text-decoration', 'underline')
                .css('cursor', 'pointer')
            ;
            $(productTableID + '_filter input[type=text]')
                .attr('placeholder', 'Enter country name')
                .css('width', '120px')
            ;

            $('button.upgrade_button').click(function(){
                location.replace("<?php echo url_for('/Login');?>");
            });

            $('button.register_button').click(function(){
                location.replace("/Create-A-Login");
            });

            $('button.login_button').click(function(){
                location.replace("<?php echo url_for('/Login');?>");
            });

            $('button.products_button').click(function(){
                location.replace("<?php echo url_for('products/index');?>");
                return false;
            });
        });
    </script>
</div>
