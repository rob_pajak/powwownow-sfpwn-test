<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix my-products">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('My Products'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24">
        <?php if ($isUser) : ?>
            <p>Here is all the Information about the features and products that you have enabled on your PINs.</p>
            <p>If you do not have any features enabled and you wish to conference call instantly - you can use the shared cost numbers displayed in the <a href="<?php echo url_for('dial_in_numbers'); ?>">Dial-in Numbers</a> list. Alternatively, contact your account administrator to enable features for you to use.</p>
        <?php elseif ($isAdmin): ?>
            <?php include_component(
                'products',
                'myProductsIntroText',
                array(
                    'adminHasUsers'               => $adminHasUsers,
                    'balance'                     => $balance,
                    'autoTopupEnabled'            => $autoTopUpEnabled,
                    'atLeastOneUserHasNoProducts' => $atLeastOneUserHasNoProducts,
                    'hasPurchasedAProduct'        => $hasPurchasedAProduct,
                    'isPostPay'                   => $isPostPay,
                    'hasBWM'                      => $hasBWM,
                )
            ); ?>
        <?php endif; ?>

        <?php include_component('products', 'productTable'); ?>
        <?php include_component('products', 'featureTable'); ?>

        <?php if ($isAdmin) : ?>
            <?php include_component(
                'products',
                'myProductsButtons',
                array(
                    'adminHasUsers'               => $adminHasUsers,
                    'balance'                     => $balance,
                    'autoTopupEnabled'            => $autoTopUpEnabled,
                    'atLeastOneUserHasNoProducts' => $atLeastOneUserHasNoProducts,
                    'hasPurchasedAProduct'        => $hasPurchasedAProduct,
                    'isPostPay'                   => $isPostPay
                )
            ); ?>
        <?php endif; ?>
    </div>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
