<ul>
    <?php foreach ($links as $link): ?>
    <li class="bundle-description">
        <span class="bundle-description-line"><?php echo __($link['text']) ?></span>
        <div class="tooltip product-bundle">
            <div class="tooltip-arrow-top png"></div>
            <a class="floatright sprite tooltip-close" href="#">
                <span class="sprite-cross-white png"></span>
            </a>
            <div class="product-accordion">
                <h4><?php echo __($bundleName . ':'); ?></h4>
                <div>
                    <?php foreach ($link['hover_lines'] as $line): ?>
                        <p><?php echo __($line); ?></p>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </li>
    <?php endforeach; ?>
</ul>