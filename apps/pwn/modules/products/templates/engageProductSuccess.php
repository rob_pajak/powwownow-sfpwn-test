<style>

	#engage-popup h2, h3 {color: #96C84B; text-shadow: none;}
	#engage-features {
		list-style: none;
		margin: 10px 0;
		padding: 0;
	}

	#engage-features li {
		padding: 0;
		margin: 5px 0;
		padding: 10px 0 10px 40px;
	}


	#engage-features li.presence-icon {

	}
	#engage-features li.messaging-icon {
		background: url('/sfimages/engage/im.png') left center no-repeat;
	}
	#engage-features li.video-icon {
		background: url('/sfimages/engage/hd.png') left center no-repeat;	
	}
	#engage-features li.voip-icon {
		background: url('/sfimages/engage/voip.png') left center no-repeat;
	}
	#engage-features li.screenshare-icon {
		background: url('/sfimages/engage/webconf.png') left center no-repeat;
	}

</style>

<div class="clearfix container_24" style="width: 98%; margin: 0 1%;" id="engage-popup">
	<div class="grid_24">
		<h2>Why Powwownow Engage?</h2>
		<strong>It’s a real-time meeting experience...</strong>

		<p>Powwownow Engage a faster way to communicate and collaborate - designed for savvy users who want an easy and smart alternative to standard conference calls.  
		Our one-click collaboration tool combines the highest quality HD video calling, screen sharing, phone and VoIP calls, presence and instant messaging – all through one single intuitive interface.</p>

	</div>
	<div class="grid_15">
		<ul id="engage-features">
			<li class="presence-icon"><strong>Presence</strong> – to check the availability of your contacts</li>
			<li class="messaging-icon"><strong>Instant Messaging</strong> – to chat with your contacts</li>
			<li class="video-icon"><strong>Video Calling</strong> – to run an HD video conference with multiple contacts</li>
			<li class="voip-icon"><strong>Calling</strong> – to run VoIP or Conference calls</li>
			<li class="screenshare-icon"><strong>Screen Sharing</strong> – to instantly share your screen with your colleagues. You can also share control of the keyboard and mouse, enabling your colleagues to edit documents on your screen with you.</li>
		</ul>
	</div>

	<div class="grid_8 prefix_1">
		<img src="/sfimages/engage_screenshot.jpg" alt="Powwownow Engage" style="width: 100%; margin-bottom: 20px;" />
	</div>
	<div class="grid_15">
	    <h3 class="rockwell">So, what are the business benefits?</h3>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png"></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Improving productivity and cost savings</span><br/>Real-time communication means an increase in productivity whilst minimising company overheads. All you pay is a monthly subscription fee, there’s no up-front investment or upgrade costs.</p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png"></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Bringing people together</span><br/>Instantly connects teams across multiple locations whether that’s communicating face-to-face via HD video, document sharing and editing or talking via VoIP or conference call. </p>
            </div>
        </div>
        <div class="clearfix">
            <div class="grid_sub_3"><img src="/sfimages/list-yellow-arrow.png"></div>
            <div class="grid_sub_21">
                <p><span class="rockwell font-bigger">Faster decision making</span><br/>Speeds up decision making, our in-built presence technology helps you identify the quickest way to communicate with your colleagues, so you can get instant answers to quick questions by using Instant Messenger an helps eliminates the wait time of emails.</p>
            </div>
        </div>
        
	</div>
	<div class="grid_8 prefix_1">
		 <h3 class="rockwell">So, what’s next?</h3>
		<p>If you’re looking for a simple, reliable real-time communication tool – contact one of our Powwownow Engage specialists today.</p>
		<p>Call us on 0800 022 9900 or +44 (0) 20 3398 9900 to arrange a product demo with one of our specialists or a free one-month trial.</p>
		<p><strong>Click <a href="<?php echo url_for('@engage_service'); ?>">here</a> to find out more about how Engage can benefit your business.</strong></p>
	</div>
</div>