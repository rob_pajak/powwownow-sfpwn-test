<tr class="bundle <?php echo strtolower($product['group_name_short']) ?>"
    id="bundle-row-<?php echo strtolower($product['group_name_short']) ?>">
    <td style="width:100px;">
        <img src="<?php echo $product['icon'] ?>" alt="<?php echo __($product['group_name']) ?>" class="product-image"/>
    </td>
    <td style="width:200px;">
        <div class="product-name">
            <span class="bundle-name"><?php echo __($product['group_name']) ?></span>
        </div>
        <div class="tooltip product-bundle">
            <div class="tooltip-arrow-top png"></div>
            <a class="floatright sprite tooltip-close" href="#">
                <span class="sprite-cross-white png"></span>
            </a>
            <div class="product-accordion">
                <h4><?php echo __('Product Description:'); ?></h4>
                <div><p><?php echo __($product['group_description']); ?></p></div>
            </div>
        </div>
        <br/>
        <span class="bundle-price">
            <?php echo __(
                "£%price% per month",
                array('%price%' => sprintf('%.2f', $product['price_per_month']))
            ); ?>
        </span>
    </td>
    <td>
        <div class="product-desc bundle-desc">
            <?php if (!empty($product['links'])): ?>
                <?php include_partial(
                    'bundleLinks',
                    array('links' => $product['links'], 'bundleName' => $product['group_name'])
                ); ?>
            <?php endif; ?>
            <p>
                <span class="product-bundle-dialog-link"><?php echo __('Full details'); ?></span>
                <a href="/sfpdf/en/Powwownow-Plus-Bundle-FAQs.pdf" target="_blank" title="FAQs">FAQs</a>
            </p>

            <div class="bundle-product-dialog" id="bundle-product-dialog-<?php echo $product['product_group_id'] ?>">
                <?php include_partial(
                    'bundleProductDialog',
                    array(
                        'dialog' => isset($product['product_dialog']) ? $product['product_dialog'] : '',
                        'productName' => $product['group_name']
                    )
                ) ?>
            </div>
        </div>
    </td>

    <td class="product-action">
        <button type="button" class="button-green bundle-configure <?php echo ($product['show_button'] !== 'select-button') ? 'not-show' : ''; ?>">
            <span><?php echo addBWMButtonLabel() ?></span>
        </button>
        <button type="button" class="button-disabled bundle-configure-disabled <?php echo ($product['show_button'] !== 'disabled-select-button') ? 'not-show' : ''; ?>" disabled="disabled">
            <span><?php echo addBWMButtonLabel() ?></span>
        </button>
        <button disabled="disabled" class="button-disabled bundle-configured <?php echo ($product['show_button'] !== 'basket-select-button') ? 'not-show' : ''; ?>">
            <span><?php echo __('Added to basket'); ?></span>
        </button>
    </td>
</tr>
<tr id="bundle-form-row-<?php echo strtolower($product['group_name_short']) ?>"
    class="bundle <?php echo strtolower($product['group_name_short']) ?> form not-show">
    <td colspan="4">
        <?php echo form_tag(url_for('add_bundle_to_basket_ajax')) ?>
            <div class="addon-bundle-form-row addon-bundle-field">
                <div class="addon-bundle-icon"></div>
                <div class="addon-bundle-description">
                    <h4><?php echo __('International Add-On') ?></h4>
                    <?php $checkboxId = 'upgrade-' . strtolower($product['group_name_short']); ?>
                    <label for="<?php echo $checkboxId ?>">
                        <span><?php echo __('Add International Landline numbers to your Bundle.') ?></span>
                        <input type="checkbox" id="<?php echo $checkboxId ?>" name="upgrade-bundle" value="<?php echo $product['upgrade_bundle']['product_group_id'] ?>"/>
                    </label>
                </div>
                <div class="addon-bundle-approval">
                    <?php if (!empty($product['upgrade_bundle']) && !empty($product['upgrade_bundle']['product_group_id'])): ?>
                        <p><?php echo __($product['upgrade_bundle']['label']) ?></p>
                    <?php endif; ?>
                </div>
                <div class="addon-bundle-denial">
                    <p><span class="addon-bundle-denial-label"><?php echo __('No thanks X') ?></span></p>
                </div>
            </div>

            <?php if (!empty($product['is_individual_bundle'])): ?>
                <div class="addon-bundle-form-row pin-pair-selection">
                    <div class="pin-pair-selection-fields">
                        <?php include_partial(
                            'individualBundleUserSelection',
                            array('individualBundleForm' => $individualBundleForm)
                        ) ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="buttons">
                <button id="add-<?php echo strtolower($product['group_name_short']) ?>" name="add-bundle" class="button-green" value="<?php echo $product['product_group_id'] ?>" type="submit">
                    <span><?php echo __('Add to basket') ?></span>
                </button>
                <button type="button" class="button-orange cancel">
                    <span><?php echo __('Cancel') ?></span>
                </button>
                <label class="bundle-tc" for="bundle-tc-<?php echo $product['product_group_id'] ?>">
                    <span><?php $rawProduct = $product->getRawValue(); echo __($rawProduct['compliance_label']) ?></span>
                    <input type="checkbox" id="bundle-tc-<?php echo $product['product_group_id'] ?>" name="bundle-tc" value="1"/>
                </label>
            </div>
        </form>
    </td>
</tr>
