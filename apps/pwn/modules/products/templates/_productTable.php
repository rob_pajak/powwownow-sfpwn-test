<table id="products" class="mypwn plus">
    <thead>
        <?php if (count($products) == 0) : ?>
        <tr>
            <th class="first-child"><?php echo __('Purchased Products'); ?></th>
            <th class="description last-child"><?php echo __('Description'); ?></th>
        </tr>
        <?php else : ?>
        <tr>
            <th colspan="2" class="first-child"><?php echo __('Purchased Products'); ?></th>
            <th class="description last-child"><?php echo __('Description'); ?></th>
        </tr>
        <?php endif; ?>
    </thead>

    <tbody>
    <?php if (count($products) == 0): ?>
        <tr class="last-row">
            <td colspan="2" style="text-align: center;" class="last-row">
                <?php echo __('You do not have any products enabled on your account.'); ?>
            </td>
        </tr>
    <?php else : ?>
        <tr class="last-row">
            <td colspan="3" style="text-align: center;" class="last-row">
                <?php echo __('You do not have any products enabled on your account.'); ?>
            </td>
        </tr>
    <?php endif; ?>

    <?php $odd = 0; ?>

    <?php foreach ($products as $product) : ?>
        <?php $odd++; ?>
        <tr class="<?php echo ($odd%2) ? 'odd ' : ''; ?>product-enabled">
            <td class="width-100">
                <img src="<?php echo $product['group_thumbnail'] ?>" alt="<?php echo $product['group_name'] ?>" class="product-image width-100"/>
            </td>
            <td class="width-200">
                <?php if ($product["is_BWM"]): ?>
                    <div class="bwm-prod-wrapper">
                        <div class="product-name bwm-name product<?php echo $product['product_group_id']; ?>"><?php echo $product['group_name'] ?></div>
                        <?php echo form_tag(
                            url_for('bwm_change_name_ajax'),
                            array(
                                'enctype' => 'application/x-www-form-urlencoded',
                                'class'   => 'bwm-name-form',
                                'id'      => 'product' . $product['product_group_id'] . '-form'
                            )
                        ); ?>
                            <span class="mypwn-input-container">
                                <input class="bwm-name-input font-medium mypwn-input" type="text" value="<?php echo $product['group_name'] ?>" maxlength="25" name="bwm_name"/>
                            </span>
                            <input type="hidden" value="<?php echo $product['product_group_id']; ?>" name="product_id">
                            <button class="tick-button green-tick" value="Confirm" type="submit" title="Confirm">
                                <span><?php echo __('Confirm product\'s name change'); ?></span>
                            </button>
                            <button class="tick-button red-cross" value="Cancel" type="button" title="Cancel">
                                <span><?php echo __('Cancel'); ?></span>
                            </button>
                        </form>
                        <?php include_component(
                            'products',
                            'productTooltip',
                            array('productGroup' => $product, 'productRates' => $productRates)
                        ) ?>
                    </div>

                    <?php if ($isAdmin): ?>
                        <img class="bwm-pencil" src="/sfimages/139.png" title="Edit"/>
                    <?php endif; ?>

                <?php else: ?>
                    <div class="product-name product<?php echo $product['product_group_id']; ?>">
                        <?php echo $product['group_name'] ?>
                    </div>
                    <?php include_component(
                        'products',
                        'productTooltip',
                        array('productGroup' => $product, 'productRates' => $productRates)
                    ) ?>
                <?php endif; ?>
            </td>
            <td<?php echo ($odd === count($products)) ? ' class="last-row"' : ''; ?>>
                <div class="product-desc">
                    <?php if ($product['is_bundle']): ?>
                        <p><?php echo __('Activation date') ?>: <?php echo $product['activation_date'] ?><br />
                            <?php echo __('Renewal date') ?>: <?php echo $product['renewal_date'] ?>
                            <?php if (!empty($product['is_individual_bundle']) && !empty($product['pin_pair'])): ?>
                                <br />
                                <?php echo __('Assigned PIN pair') ?>: <?php echo $product['pin_pair'] ?>
                            <?php endif; ?>
                        </p>
                        <a class="bundle-information"><?php echo __('Find out more about your Bundle.') ?></a>
                        <div class="bundle-product-dialog" id="bundle-product-dialog-<?php echo $product['product_group_id'] ?>">
                            <?php include_partial('bundleProductDialog', array('dialog' => isset($product['product_dialog']) ? $product['product_dialog'] : '', 'productName' => $product['group_name'])) ?>
                        </div>
                        <?php if ($isAdmin): ?>
                            <p><?php echo __('To cancel or change your bundle please contact Customer Services on 020 3398 0398.') ?></p>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php echo $product['group_summary']; ?>
                    <?php endif; ?>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
