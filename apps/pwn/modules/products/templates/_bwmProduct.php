<?php use_helper('BWM', 'Basket'); ?>
<?php $productRates = $sf_data->getRaw('productRates'); ?>
<?php $product = $sf_data->getRaw('product'); ?>

<tr class="product-<?php echo ($enabled == true) ? 'enabled' : 'disabled' ?>" <?php if (isset($product['cost'])): ?>data-cost="<?php echo $product['cost'] ?>"<?php endif; ?>>
    <td style="width:100px;">
        <img src="/sfimages/products/branded-welcome-message-logo-grey.png" alt="<?php echo $product['group_name'] ?>" class="product-image" width="100"/>
    </td>
    <td style="width:200px;" id="bwm-prod<?php echo $product['product_group_id']; ?>-td">
        <div class="bwm-prod-wrapper">
             <div class="product-name bwm-name product<?php echo $product['product_group_id']; ?>"><?php echo $product['group_name'] ?></div>
                <?php echo form_tag(url_for('bwm_change_name_ajax'), array('enctype' => 'application/x-www-form-urlencoded', 'class' => 'bwm-name-form', 'id' => 'product'.$product['product_group_id'].'-form')); ?>
                    <span class="mypwn-input-container"><input class="bwm-name-input font-medium mypwn-input" type="text" value="<?php echo $product['group_name'] ?>" maxlength="25" name="bwm_name"/></span>
                    <input type="hidden" value="<?php echo $product['product_group_id']; ?>" name="session_id">
                    <button class="tick-button green-tick" style="position:relative; left:0; top:0;" value="Confirm" type="submit" title="Confirm">
                        <span>Confirm product's name change</span>
                    </button>
                    <button class="tick-button red-cross" style="position:relative; left:0; top:0;" value="Cancel" type="button" title="Cancel">
                        <span>Cancel</span>
                    </button>
                </form>
            <?php include_component('products', 'productTooltip', array('productGroup' => $product, 'productRates' =>  $productRates)) ?>
         </div>
        <img class="bwm-pencil" src="/sfimages/139-bright1.png" title="Edit"/>
    </td>
    <td><div class="product-desc"><?php echo formBWMDescription($product); ?></div></td>
    <td class="basket-actions product-action" style="position:relative;">
        <?php if (!$enabled): ?>
            <?php
            include_partial('removeFromBasket', array(
                'productGroupId' => $product['product_group_id'],
                'markedAsAdded' => hasProductBeenMarkedAsAdded($product['product_group_id']),
            ))
            ?>
            <?php
            include_partial('addProductForm', array(
                'productGroupId' => $product['product_group_id'],
                'addLabel' => determineAddLabel($product['product_group_id']),
                'markedAsAdded' => hasProductBeenMarkedAsAdded($product['product_group_id']),
            ))
            ?>
        <?php endif; ?>
    </td>
</tr>