<table id="products-feature" class="mypwn plus features <?php if ($isUser) echo "user-features"; ?>">
    <tr>
        <th colspan="2" class="first-child"><?php echo __('Features'); ?></th>
        <?php if ($isUser): ?>
            <th class="description"><?php echo __('Description'); ?></th>
            <th class="last-child"><?php echo __('Status'); ?></th>
        <?php else : ?>
            <th class="description last-child"><?php echo __('Description'); ?></th>
        <?php endif; ?>
    </tr>

    <?php if (empty($products) && $isUser): ?>
        <tr class="last-row">
            <td colspan="4" style="text-align: center;">
                <?php echo __('You do not have any features enabled on your account. Please contact your account admin if you wish to have these enabled.'); ?>
            </td>
        </tr>
    <?php endif; ?>

    <?php $odd = 0; ?>

    <?php foreach ($products as $product) : ?>
        <?php $odd++; ?>
        <tr class="<?php echo ($odd%2) ? 'odd ' : ''; ?>product-enabled<?php echo ($odd === count($products)) ? ' last-row' : ''; ?>">
            <td class="width-100<?php echo ($odd === count($products)) ? ' first-child-last-row' : ''; ?>">
                <img src="<?php echo $product['group_thumbnail'] ?>" alt="<?php echo $product['group_name'] ?>" class="product-image" width="100"/>
            </td>
            <td class="width-200">
                <div class="product-name product<?php echo $product['product_group_id']; ?>">
                    <?php echo $product['group_name'] ?>
                </div>
                <?php include_component('products', 'productTooltip', array('productGroup' => $product, 'productRates' =>  $productRates)) ?>
            </td>
            <td<?php echo (!$isUser && $odd === count($products)) ? ' class="last-child-last-row"' : ''; ?>>
                <div class="product-desc"><?php echo $product['group_summary']; ?></div>
            </td>
            <?php if ($isUser): ?>
                <td<?php echo ($odd === count($products)) ? ' class="last-child-last-row"' : ''; ?>>
                    <?php if (in_array($product['product_group_id'],  $activeProducts)) : ?>
                        <?php echo __('Enabled'); ?>
                    <?php else : ?>
                        <?php echo __('Disabled'); ?>
                    <?php endif; ?>
                </td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>

    <?php foreach ($automaticProducts as $product): ?>
        <?php $odd++; ?>
        <tr class="<?php echo ($odd%2) ? 'odd ' : ''; ?>product-enabled">
            <td class="width-100"></td>
            <td class="width-200">
                <div class="no-tooltip product-name">
                    <?php echo $product['group_name'] ?>
                </div>
            </td>
            <td>
                <div class="product-desc"><?php echo $product['group_summary']; ?></div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
