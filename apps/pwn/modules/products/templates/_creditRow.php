<tr class="credit-row">
    <td style="width:100px;">
        <img src="/sfimages/products/purchase-credit.png" alt="<?php echo __('Purchase Credit') ?>" class="product-image"  />
    </td>
    <td style="width:200px;">
        <div class="product-name credit"><?php echo __('PAYG Call Credit') ?></div>
        <div class="tooltip product-credit">
            <div class="tooltip-arrow-top png"></div>
            <a class="floatright sprite tooltip-close" href="#">
                <span class="sprite-cross-white png"></span>
            </a>
            <div class="product-accordion">
                <h4><?php echo __('Product Description:'); ?></h4>
                <div><p><?php echo __('Purchase PAYG Call Credit and you will only ever pay for what you use on Landline and Freephone numbers.'); ?></p></div>
            </div>
        </div>
    <td>
        <div class="product-desc credit">
            <?php echo __('Purchasing PAYG Call Credit for your account will allow you and users to use Worldwide and UK Freephone and Landline numbers. Select the amount of call credit you wish to purchase and you can start talking!'); ?>
        </div>
    </td>

    <td class="product-action">
        <button id="credit-select-button" type="button" class="button-green credit-configure<?php echo ($creditProduct !== false || $hasBundleBeenAddedToBasket ? ' not-show' : null); ?>">
            <span><?php echo addBWMButtonLabel() ?></span>
        </button>
        <button id="credit-added-button" disabled="disabled" style="position: relative; left: 0px;top: 0px;" class="button-disabled<?php echo ($creditProduct===false || $hasBundleBeenAddedToBasket?' not-show':null); ?>" value="Added to Basket" type="submit">
            <span><?php echo __('Added to Basket'); ?></span>
        </button>
        <button id="credit-disabled-button" disabled="disabled" class="button-disabled credit-configure-disabled <?php if (!$hasBundleBeenAddedToBasket) echo 'not-show'; ?>">
            <span><?php echo __('Disabled'); ?></span>
        </button>
    </td>
</tr>
<tr class="credit-row not-show" id="credit-purchase-row">
    <td colspan="4">
        <div>
<!--            <form id="credit-form"> -->
            <?php echo form_tag('@add_credit_to_basket_ajax',array('id' => 'credit-form')); ?>
            <div class="form-field" style="width: 400px; float: left; margin: 10px 0px;">
                <ul class="radio_list">
                    <li>
                        <input name="topup[topup-amount]" type="radio" value="20" id="topup_topup-amount_20" <?php echo ($minimumCreditRequired <= 20) ? 'checked="checked"' : NULL; ?>>&nbsp;
                        <label for="topup_topup-amount_20">£20.00 +20% VAT (£24.00)</label>
                    </li>
                    <li>
                        <input name="topup[topup-amount]" type="radio" value="50" id="topup_topup-amount_50">&nbsp;
                        <label for="topup_topup-amount_50">£50.00 +20% VAT (£60.00)</label>
                    </li>
                    <li>
                        <input name="topup[topup-amount]" type="radio" value="100" id="topup_topup-amount_100">&nbsp;
                        <label for="topup_topup-amount_100">£100.00 +20% VAT (£120.00)</label>
                    </li>
                    <li>
                        <input name="topup[topup-amount]" type="radio" value="input" id="topup_topup-amount_xxx" <?php echo ($minimumCreditRequired > 20) ? 'checked="checked"' : NULL; ?>>&nbsp;
                        <label for="topup_topup-amount_input">
                            <?php echo __('Enter amount £'); ?>
                            <?php if ($minimumCreditRequired > 20) : ?>
                                <input name="topup[topup-amount-input]" type="text" value="<?php echo number_format($minimumCreditRequired,2);?>" id="topup_topup-amount_input" min="20" maxlength="6">
                                <span class="topup-amount-fill">+20% VAT (£<?php echo number_format($minimumCreditRequired*1.2,2);?>)</span>
                            <?php else : ?>
                                <input name="topup[topup-amount-input]" type="text" value="20.00" id="topup_topup-amount_input" maxlength="6">
                                <span class="topup-amount-fill">+20% VAT (£24.00)</span>
                            <?php endif; ?>
                        </label>
                    </li>
                </ul>
            </div>
            <div class="auto-recharge-box" id="purchse-credit-auto-recharge-box">
                <?php $autoTopupEnabled = false; ?>
                <?php if($autoTopupEnabled): ?>
                    <strong><?php echo __('You currently have Auto Top-up enabled'); ?></strong>
                    <p><?php echo __('Auto Top-up works by topping up your account automatically when your call credit balance falls below £5, with the same amount and payment method as your most recent transaction. You can easily cancel your auto top-up settings at any time. falls below &pound;5.00. If you do not wish to have Auto Top-up enabled on your account, please untick the box below.'); ?></p>
                <?php else: ?>
                    <strong><?php echo __('Save time with Auto Top-up'); ?></strong>
                <?php endif; ?>
                <p><?php echo __('Your balance will be automatically topped up when your call credit balance falls below &pound;5, with the same amount and payment method as your most recent transaction. You can cancel your auto top-up settings at any time.'); ?></p>
                <p style="color: #515151;">
                    <input id="topup_auto-recharge" type="checkbox" checked="checked" name="topup[auto-recharge]">
                    <?php echo __('Top-up my account automatically.'); ?>
                </p>
            </div>
            <br style="clear: both;"/>
            <div>
                <button class="button-orange" value="Cancel" type="button" id="purchse-credit-cancel-button">
                    <span>Cancel</span>
                </button>
                <button class="button-green" value="Add" type="submit">
                    <span>Add</span>
                </button>
            </div>
            </form>
        </div>
    </td>
</tr>
