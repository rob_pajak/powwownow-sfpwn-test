<!--Left Column-->
<div class="grid_10 clearfix" style="position: relative;width:380px;">
    <h3>Create your Branded Welcome Message</h3>
    <p>On completing the 3 simple requirements below your branded welcome message script will be completed.</p>
    <!--<div class="bwm-arrow-up"></div>-->
    <form id="configure_bwm_script" class="clearfix" action="/s/products/BwmAddNumbersAjax" enctype="application/x-www-form-urlencoded" method="post" name="configure_bwm_script">
        <p>Enter your Company Name, followed by the Contact Name and Contact Number of the person who will be responsible for the allocation of your Company's PINs.</p>
        <div class="form-field">
            <label for="company"><?php echo __('Company Name'); ?><sup class="asterisk">*</sup></label>
            <span class="mypwn-input-container">
                <input type="text" name="company" id="company" class="input-large font-small mypwn-input required" maxlength="80" placeholder="<?php echo __('Your company name') ?>" value="<?php echo $organisation ?>" />
            </span>
        </div>
        <div class="form-field">
            <label for="contact_name"><?php echo __('Contact Name'); ?><sup class="asterisk">*</sup></label>
            <span class="mypwn-input-container">
                <input type="text" name="contact_name" id="contact_name" class="input-large font-small mypwn-input required" maxlength="80" placeholder="<?php echo __('Contact name') ?>" value="<?php echo $firstName ?> <?php echo $lastName ?>" />
            </span>
        </div>
        <div class="form-field">
            <label for="contact_number"><?php echo __('Contact Number'); ?><sup class="asterisk">*</sup></label>
            <span class="mypwn-input-container">
                <input type="text" name="contact_number" id="contact_number" class="input-large font-small mypwn-input required" maxlength="80" placeholder="<?php echo __('Contact number') ?>" value="" />
            </span>
        </div>

        <div class="form-field">
            <input type="checkbox" value="1" id="script_accept" name="script_accept" class="required" style="vertical-align: middle" /> I confirm that script on the right is correct.
        </div>
        <div class="form-field">
            Once you are happy with the script, please continue to the next step where you can choose the dedicated numbers you require.
        </div>

        <div class="form-action margin-top-20" style="text-align: left">
            <button type="button" class="button-orange bwm-cancel" value="Cancel">
                <span>Cancel</span>
            </button>
            <button type="submit" class="button-green" value="Continue">
                <span>Continue</span>
            </button>
        </div>
    </form>

</div>
<div class="grid_13 push_1">
    <p style="margin-top:5px;">Your conference calls will be answered with the following Branded Welcome Message:</p>
    <h3>Welcome Message</h3>
    <div class="bwm-script" id="bwm-script1">
        <blockquote>
            <?php include_partial('bwmScriptWelcomeMessage', array('company' => '{company}', 'contact_name' => '{contact_name}', 'contact_number' => '<span class="var">{contact_number}</span>')) ?>
        </blockquote>
    </div>
    <h3>Instructions Message</h3>
    <div class="bwm-script" id="bwm-script2">
        <blockquote>
            <?php include_partial('bwmScriptInstructionsMessage', array('company' => '{company}', 'contact_name' => '{contact_name}', 'contact_number' => '<span class="var">{contact_number}</span>')) ?>
        </blockquote>
    </div>
    <div class="margin-top-10">
        Your recording will take up to 10 working days, we will notify you as soon as it is available.<br/><br/>
        Once you have listened to the recording of your welcome message, please make us aware of any amendments you wish to make to it within 48h. Any changes that are requested after 48h will be subject to a £50 re-branding fee.
    </div>
</div>
