<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <form action="<?php echo url_for('add_bwm') ?>" method="POST">
        <p>The setup cost of this product is <strong>£<?php echo $setupCost ?></strong> plus the cost of every selected dial-in number.</p>
        <table>
            <?php echo $form ?>
            <tr>
                <td colspan="2">
                    <input type="submit" name="create" value="Create new Branded Welcome Message" />
                </td>
            </tr>
        </table>
    </form>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>