<tr class="bwm-row">
    <td class="width-100">
        <img src="/sfimages/products/cowgirl-bwm.png" alt="<?php echo __('Branded Welcome Message') ?>" class="product-image" />
    </td>
    <td class="product-name-cell width-200">
        <div class="product-name bwm"><?php echo __('Branded Welcome Message') ?></div>
        <div class="tooltip product-bwm">
            <div class="tooltip-arrow-top png"></div>
            <a class="floatright sprite tooltip-close" href="#">
                <span class="sprite-cross-white png"></span>
            </a>
            <div class="product-accordion">
                <h4>Product Description:</h4>
                <div><p>Customise your conference calls by adding your own Branded Welcome Messages to the dedicated numbers of your choice.</p></div>
                <h4>Product Price:</h4>
                <div>
                    <table id="configured-bwm-prices">
                        <tr>
                            <td>Branded Welcome Message: one-off charge</td>
                            <td>&pound;<?= $bwmAccountConnectionCost; ?></td>
                        </tr>
                        <tr class="default-dnis-price">
                            <td>Annual charge per dedicated number</td>
                            <td>&pound;<?= $bwmProductRatePerDnis; ?></td>
                        </tr>
                    </table>
                </div>
                <h4>Dial-in Numbers and Rates:</h4>
                <div>
                    <table class="product-numbers">
                        <tbody>
                            <tr>
                                <th>Country</th>
                                <th>City</th>
                                <th>Type</th>
                                <th>Rate per min</th>
                            </tr>
                            <?php $x = 1;?>
                            <?php foreach($bwmProductRates['products'] as $prd):?>
                                <tr class="<?php echo ($x%2 == 0 ? 'even' : 'odd')?>">
                                    <td><?php echo __($prd['country']);?></td>
                                    <td><?php echo __($prd['city']); ?></td>
                                    <td><?php echo __($prd['dnis_type'] === 'Geographic') ? 'Landline' : $prd['dnis_type']; ?></td>
                                    <td><?php echo __($prd['rate'] * 100); ?> pence</td>
                                </tr>
                                <?php $x++;?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="product-desc bwm">
            Customise your conference calls by creating your own Branded Welcome Messages - this will allow you to introduce participants to your brand as soon as they dial your new dedicated number. Find out more <a href="<?php echo url_for('@product_details_bwm'); ?>" class="pwn-modal">here</a>.
        </div>
    </td>
    <td class="product-action">
        <button type="button" class="button-green bwm-configure">
            <span><?php echo addBWMButtonLabel(); ?></span>
        </button>
    </td>
</tr>

<script>
    $(document).ready(function() {
        myPwnApp.bwm.init();
    });
</script>
