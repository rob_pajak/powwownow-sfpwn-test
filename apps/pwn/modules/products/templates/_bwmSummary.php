<div class="hr-spotted-top png content-seperator"><!--blank--></div>
<div class="grid_10 clearfix bwm-summary">
    <?php //echo "<pre>"; print_r($BWM_summary); echo "<pre>";?>
    <div class="bwm-step2-container">
        <div class="bwm-arrow-up"></div>

        <h3>Product Confirmation</h3>
        <p>You have created the following product:</p>

        <form id="bwm_summary_form" class="clearfix" action="/s/products/BwmSummaryAjax" enctype="application/x-www-form-urlencoded" method="post" name="bwm_summary_form">

            <div class="form-field">
                <label for="bwm_label"><?php echo __('Product Name'); ?><sup class="asterisk">*</sup></label>
                <span class="mypwn-input-container">
                    <input type="text" name="bwm_label" id="bwm_label" required="required" class="input-large font-small mypwn-input" required="required" maxlength="25" placeholder="<?php echo __('Your custom product label') ?>" value="<?php echo __('Branded Welcome Message') ?>" />
                </span>
            </div>

            <div class="form-field rnd_tbl_wrapper">
                <table cellpadding="0" cellspacing="0" border="0" class="mypwn plus" id="summaryRates">
                    <thead>
                        <tr class="">
                            <th>Country</th>
                            <th>City</th>
                            <th>Type</th>
                            <th>Rate per min</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="bwm-no-number">
                            <?php if (!count($BWM_summary['BWM_attrs_dialin_details'])):?>
                                <td colspan="4" class="aligncenter"><em>No number selected</em></td>
                            <?php else: ?>
                                <?php foreach ($BWM_summary['BWM_attrs_dialin_details'] as $value) :?>
                                    <tr>
                                        <td><?php echo $value['country']?></td>
                                        <td><?php echo $value['city']?></td>
                                        <td><?php echo $value['type']?></td>
                                        <td><?php echo $value['rate']?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif;?>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="bwmCharges">
                <table cellpadding="0" cellspacing="0" border="0" >
                    <tr>
                        <td><?php echo __('Branded Welcome Message') ?></td>
                        <td class="bwm-one-off-price">&pound;<?php echo $BWM_summary['BWM_attrs_dialin_details']['BWM_total_cost']['connectionFee']?></td>
                    </tr>
                    <?php foreach ($groupedDnis as $dnisType => $numbers): ?>
                        <tr>
                            <td>
                                <?php echo $dnisType . ' &times; ' . count($numbers)  ?>
                            </td>
                            <td class="bwm-numbers-price">
                                <?php echo '&pound;' . (count($numbers) * $BWM_summary['BWM_attrs_dialin_details']['BWM_total_cost']['dnisPerRate']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td class="alignright"><?php echo __('Total:') ?></td>
                        <td class="bwm-total-price">&pound;<?php echo $BWM_summary['BWM_attrs_dialin_details']['BWM_total_cost']['total'] + $BWM_summary['BWM_attrs_dialin_details']['BWM_total_cost']['connectionFee']; ?></td>
                    </tr>
                </table>
            </div>
            <p>Your dedicated dial-in numbers will be available to use immediately and your message will take up to 10 working days, we will notify you as soon as it’s available.</p>
            <div class="form-field">
                <label class="script-accept-label" for="script_accept"><input type="checkbox" value="1" id="script_accept" name="script_accept" required="required" style="vertical-align: middle" /> I have read and agree to the <a href="<?php echo url_for('@terms_and_conditions'); ?>" target="_blank">terms &amp; conditions</a></label>
            </div>
            <div class="form-action margin-top-20" style="text-align: left">
                <button type="button" class="button-orange bwm-cancel">
                    <span>Cancel</span>
                </button>
                <button type="submit" class="button-green" id="completeBwm">
                    <span>Finish</span>
                </button>
            </div>

        </form>



    </div>

</div>

<div class="grid_13 push_1">
    <div class="selected-bwm-script">
        <h3>Your Branded Welcome Message</h3>

        <p>Welcome Message</p>
        <div class="bwm-script">
            <blockquote>
                <?php echo $welcomeMessage ?>
            </blockquote>
        </div>

        <p>Instructions Message</p>
        <div class="bwm-script">
            <blockquote>
                <?php echo $instructionsMessage ?>
            </blockquote>
        </div>
    </div>
</div>
