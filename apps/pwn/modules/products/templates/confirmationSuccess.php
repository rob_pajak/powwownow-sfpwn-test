<?php include_component('commonComponents', 'header'); ?>
<?php $assignedProducts = $sf_data->getRaw('assignedProducts'); ?>
<?php $PINProducts = $sf_data->getRaw('PINProducts'); ?>
<?php $sf_response->setTitle(__('Enabled Products Confirmation')); ?>
<?php $odd = 1; ?>
<div class="container_24 clearfix">
   <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Enabled Products Confirmation'), 'headingSize' => 'xl','breadcrumbs' => $breadcrumbs, 'disableBreadcrumbs' => true)); ?>
   <div class="grid_24">
      <?php //include_component('commonComponents', 'productsFlowBar',array('step' => 5)); ?>
      <?php //include_component('commonComponents', 'productsFlowBar',array('step' => (is_null($newUser) ? 0 : 2), 'flow' => (is_null($newUser) ? 2 : 0))); ?>
      <?php //include_component('commonComponents', 'productsFlowBar',array('step' => (is_null($newUser) ? 2 : 4), 'flow' => (is_null($newUser) ? 2 : 0))); ?>
      
      <?php if (count($assignedProducts)>0) : ?>
         <p class="grid_sub_24 margin-bottom-10"><?php echo __('These are the products you have enabled on your account:'); ?></p>
      <?php else : ?>
         <p class="grid_sub_24 margin-bottom-10"><?php echo __('You have no Products Enabled.'); ?></p>
      <?php endif; ?>

      <?php foreach ($assignedProducts as $i => $productInfo) : ?>
         <h2 class="grid_sub_24">
            <img src="<?php echo $productInfo['group_thumbnail']; ?>" alt="<?php echo $productInfo['group_name'] ?>" class="product-image" style="margin-right:0;" width="100"/>
            <?php echo $productInfo['group_name']; ?>
         </h2>
         
         <?php if (isset($productInfo['hasUKNumbers']) && true === $productInfo['hasUKNumbers']) : ?>
            <p class="grid_sub_24 margin-bottom-10">
               Your <?php echo $productInfo['group_name']; ?> is: <?php echo $productInfo['UKCallingNumber']; ?> - this number is displayed in your <a href="/myPwn/Dial-In-Numbers">dial-in numbers list*</a>.
               <?php if ($hasUsers && count($PINProducts > 2)) : ?>
                  <img src="/sfimages/zoom_plus.png" class="toggleme" id="pins_<?php echo $i; ?>" width="25px" height="25px" alt="Click to Toggle users who have this Product Enabled on their PINs" title="Click to Toggle users who have this Product Enabled on their PINs"/>
               <?php endif; ?>
            </p>
         <?php else : ?>
            <p class="grid_sub_24 margin-bottom-10">
               Your <?php echo $productInfo['group_name']; ?> are displayed in your <a href="/myPwn/Dial-In-Numbers">dial-in numbers list*</a>.
               <?php if ($hasUsers && count($PINProducts > 2)) : ?>
                  <img src="/sfimages/zoom_plus.png" class="toggleme" id="pins_<?php echo $i; ?>" width="25px" height="25px" alt="Click to Toggle users who have this Product Enabled on their PINs" title="Click to Toggle users who have this Product Enabled on their PINs"/>
               <?php endif; ?>               
            </p>
         <?php endif; ?>

         <?php if ($hasUsers && count($PINProducts > 2)) : ?>
            <table class="pins_<?php echo $i; ?> mypwn plus floatleft margin-bottom-10" style="display:none">
               <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>PINs</th>
               </tr>
               <?php foreach ($PINProducts[$productInfo['product_group_id']] as $j => $pinProductInfo) : ?>
                  <tr class="<?php echo ($odd%2) ? 'odd ' : ''; ?>">
                     <td><?php echo $pinProductInfo['first_name'] . ' ' . $pinProductInfo['last_name'];?></td>
                     <td><?php echo $pinProductInfo['email'];?></td>
                     <td><?php echo $pinProductInfo['master_pin'] . ' / ' . $pinProductInfo['pin'];?></td>
                  </tr>
                  <?php //echo "<pre>"; print_r($pinProductInfo); echo "</pre>"; ?>
                  <?php $odd++; ?>
               <?php endforeach; ?>
            </table>
         <?php endif; ?>
      <?php endforeach; ?>

      <?php if (count($assignedProducts)>0) : ?>
         <p class="grid_sub_24 margin-top-20"><?php echo __('* Each user might have a different dial-in numbers list depending on the enabled products set to their PIN'); ?></p>
      <?php endif; ?>

      <button type="submit" class="button-orange floatright" id="continue">
         <span><?php echo __('Finish'); ?></span>
      </button>
   </div>
   <script type="text/javascript">
   $(document).ready(function(){
      $('#continue').click(function(){
         window.location = "/myPwn";
      });
      $('.toggleme').click(function(){
         $('.'+$(this).attr('id')).toggle();
      });
   });
   </script>
   <?php include_component('commonComponents', 'socialMediaFooter'); ?>
   <?php include_component('commonComponents', 'footer'); ?>
</div>
