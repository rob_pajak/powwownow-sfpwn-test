<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <h2>Error</h2>
    <p>We were unable to find any available dial-in numbers to link to a new Branded Welcome Message.</p>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>