<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix products">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array(
            'title'              => __('Products'),
            'headingSize'        => 's',
            'breadcrumbs'        => array(),
            'disableBreadcrumbs' => true
        )
    ); ?>
    <div class="grid_24">
        <?php if($isAdmin): ?>
            <p><?php echo __('You\'re already using conference calls. So why not add a little extra by purchasing some account enhancing products below or download our <a href="' . url_for("@web_conferencing_auth") . '">FREE screen sharing</a> tool and show people what you’re talking about.'); ?></p>
            <p>Unsure which Bundle is best for the call time you make? Try our simple <a href="/myPwn#shake" title="Bundle Selector Tool">Bundle Selector Tool</a> to calculate which fits your requirements and to see how much you could save!</p>
        <?php else: ?>
            <p><?php echo __('Here you can view the products you currently have enabled on your Users and information on each product can be found in the descriptions below.'); ?></p>
            <p><?php echo __('If there is a product you would like to have enabled, you need to contact your Account Administrator to arrange this.'); ?></p>
            <p><?php echo __('If you do not have any products enabled and you wish to conference call instantly - you can use the shared cost numbers displayed in the <a href="' . url_for("@dial_in_numbers") . '">Dial-in Numbers list</a>.'); ?></p>
        <?php endif; ?>

        <table id="products" class="mypwn plus">
            <tr>
                <th colspan="2" class="first-child"><?php echo __('Products'); ?></th>
                <th><?php echo __('Description'); ?></th>
                <th style="width: 80px;" class="basket-actions last-child"></th>
            </tr>

            <!-- Bundles Start -->
            <?php foreach ($bundles as $bundle): ?>
                <?php include_partial(
                    'bundleRow',
                    array(
                        'product'                    => $bundle,
                        'hasBundleBeenAddedToBasket' => $hasBundleBeenAddedToBasket,
                        'hasBundleBeenActivated'     => $hasBundleBeenActivated,
                        'individualBundleForm'       => $individualBundleForm,
                        'selectedBundleId'           => $selectedBundleId,
                    )
                ); ?>
            <?php endforeach; ?>
            <!-- Bundles End -->

            <!-- BWM Start -->
            <?php include_component(
                'products',
                'bwmRow',
                array(
                    'bwmProductRates'          => $bwmProductRates,
                    'bwmAccountConnectionCost' => $bwmAccountConnectionCost,
                    'bwmProductRatePerDnis'    => $bwmProductRatePerDnis
                )
            ); ?>
            <!-- BWM End -->

            <!-- Credit Start -->
            <?php if (!$hasBundleBeenActivated): ?>
                <?php include_partial(
                    'creditRow',
                    array(
                        'minimumCreditRequired'      => 20,
                        'creditProduct'              => $creditProduct,
                        'hasBundleBeenAddedToBasket' => $hasBundleBeenAddedToBasket,
                    )
                ); ?>
            <?php endif; ?>
            <!-- Credit End -->
        </table>

        <?php echo form_tag('select_products_submit') ?>
            <button class="button-orange floatright" type="submit" id="updateAccount" <?php if (!showContinueButtonInitially(true)) echo 'style="display: none;"' ?>>
                <span>Continue</span>
            </button>
        </form>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
