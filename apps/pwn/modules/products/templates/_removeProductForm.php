<?php echo form_tag(url_for('confirm_product_removal'), array('enctype' => 'application/x-www-form-urlencoded', 'class' => 'product-removal-action')); ?>
    <input type="hidden" name="remove-product" value="<?php echo $product['product_group_id']; ?>" />
    <button type="submit" value="Remove from account" class="button-green" style="position:relative; left:0px;top:0px;">
        <span>Remove from account</span>
    </button>
</form>