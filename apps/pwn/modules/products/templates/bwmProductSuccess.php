<?php use_stylesheet('/sfcss/vendor/jQuery/datatable.css'); ?>
<?php use_javascript('/shared/jQuery/jquery.dataTables.min.js'); ?>
<div class="clearfix container_24 branded-welcome-messages-container" style="width: 98%; margin: 0 1%;">
  <div class="grid_24">
    <div class="grid_sub_15">
      <div class="clearfix">
        <img class="floatleft" width="48" height="48" alt="UK Landline Number" src="/sfimages/products/branded-welcome-message-logo.png">
        <h2 class="rockwell blue productdetailshead">
          Branded Welcome Messages
        </h2>
      </div>
      <div class="clearfix">
        <p>
           Now you can customise your conference calls with your company branding and add it to dedicated dial-in numbers of your choice.
        </p>
        <p>
          In today's competitive marketplace it’s more important than ever to stand out from the crowd and with Powwownow it's simple to add that personal touch to your conference call.
        </p>
        <div class="hr-spotted-top png content-seperator">
        </div>
        <h3 class="rockwell blue">
          What are the benefits?
        </h3>
        <ul class="chevron-green">
            <li class="png">
                  Allows you to introduce participants to your brand, your new product or even simply the subject of your teleconference as soon as they dial the relevant number.
            </li>
            <li class="png">
                  Having a dedicated number gives you extra security knowing that nobody else can use your number.
            </li>
            <li class="png">
                  Choose from both Freephone and Landline numbers to make sure you have the right number for your conference call. Whether you're dialling in from a mobile using a Landline number or you want your participants to dial in for free using a Freephone number.
            </li>
        </ul>
        <br/>
        <div class="hr-spotted-top png content-seperator"></div>
        <h3 class="rockwell blue">What’s the cost?</h3>
        <p>
            All you pay for is your branded message, along with an annual fee per dedicated dial-in number.
        </p>
        <p>
        <strong>
          Easy to set up
        </strong>
        <br/>
        Just create the branded message and select the dial-in numbers you require and pay using our simple and secure credit card payment system.
      </p>
      <p>
        You'll need call credit on your account to conference call using your new dedicated numbers, if you don't have any credit when purchasing this product, &pound;5 will be added to your purchase so you can make your first conference call instantly.
      </p>


      </div>

    </div>

    <div class="grid_sub_8 push_1 margin-top-20">
        <table cellpadding="0" cellspacing="0" border="0" class="mypwn plus floatleft" id="usageRates<?php echo $productID; ?>"></table>
        <p>Calls are charged at the rate per minute shown above.</p>
        <p>Branding is charged at a one-off fee of &pound;50 per recording.  All dedicated dial-in numbers are charged annually at &pound;60 per number.</p>

        <?php if (sfContext::getInstance()->getUser()->hasCredential('PLUS_USER')) : ?>
            <br/>
            <div class="hr-spotted-top png content-seperator"></div>
            <h3 class="rockwell blue">Like the sound of this?</h3>
            <p>Contact your account administrator to purchase and enable this product on your PINs.</p>
        <?php endif;?>

    </div>
  </div>
</div>
<script type="text/javascript">
    $(function() {
        // Set the Languages
        var oLanguage = {
                "sProcessing": DATA_TABLES_sProcessing,
                "sLengthMenu": DATA_TABLES_sLengthMenu,
                "sZeroRecords": DATA_TABLES_sZeroRecords,
                "sEmptyTable": DATA_TABLES_sEmptyTable,
                "sLoadingRecords": DATA_TABLES_sLoadingRecords,
                "sInfo": DATA_TABLES_sInfo,
                "sInfoEmpty": DATA_TABLES_sInfoEmpty,
                "sInfoFiltered": DATA_TABLES_sInfoFiltered,
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sSearch": DATA_TABLES_sSearch,
                "sUrl": "",
                "fnInfoCallback": null,
                "oPaginate": {
                    "sFirst":    '&lt;&lt;',
                    "sPrevious": '&lt;',
                    "sNext":     '&gt;',
                    "sLast":     '&gt;&gt;'
                }
            };

        // Set the Column Headings
        var aoColumns = [
            { "sTitle": "<?php echo __('Country');?>" },
            { "sTitle": "<?php echo __('City');?>" },
            { "sTitle": "<?php echo __('Type');?>" },
            { "sTitle": "<?php echo __('Rate per min');?>" }
        ];

        // Load dataTable if needed
        window.oTableScreen<?php echo $productID; ?> = $('#usageRates<?php echo $productID; ?>').dataTable({
            "aaData": <?php echo json_encode($sf_data->getRaw('products')); ?>,
            "aoColumns": aoColumns,
            "oLanguage" : oLanguage,
            "sPaginationType": "full_numbers",
            "bFilter": true,
            "bInfo": false,
            "bLengthChange": false,
            "iDisplayLength": 8,
            "fnDrawCallback":function(){
                //init_table();
            }
        });

        if (window.oTableScreen<?php echo $productID; ?>.fnGetData().length <= 10) {
            $('#usageRates<?php echo $productID; ?>_paginate').hide();
        }
        window.oTableScreen<?php echo $productID; ?>.fnSort([[0,'asc']]);

        // Styling Changes
        $('#usageRates<?php echo $productID; ?> th')
            .append('<div></div>')
            .css('text-decoration', 'underline')
            .css('cursor', 'pointer')
        ;
        $('#usageRates<?php echo $productID; ?>_filter input[type=text]')
            .attr('placeholder', 'Enter country name')
            .css('width', '120px')
        ;

        $('button.upgrade_button').click(function(){
            location.replace("<?php echo url_for('/Login');?>");
        });

        $('button.register_button').click(function(){
            location.replace("/Create-A-Login");
        });

        $('button.login_button').click(function(){
            location.replace("<?php echo url_for('/Login');?>");
        });

        $('button.products_button').click(function(){
            location.replace("<?php echo url_for('products/index');?>");
            return false;
        });
    });
    </script>
