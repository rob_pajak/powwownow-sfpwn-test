<?php $sf_response->setTitle(__('Basket')) ?>

<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix basket-page"  <?php if (isset($bundleNotification)) echo 'data-bundle-notification="' . $bundleNotification . '"' ?>>
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Your Basket'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)); ?>
    <div class="clearfix"></div>

    <?php include_partial('emptyBasket') ?>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>

<!-- Modal Windows -->
<?php include_partial('productSelectorTool/accountHasBundle'); ?>