<?php if ($addedProducts): ?>
    <?php $disableProductActions = !empty($disableProductActions) ?>
    <div class="added-products">
        <table class="mypwn plus" id="basket-products">
            <thead>
            <tr>
                <th class="product-name"><?php echo __('Products') ?></th>
                <th class="charge"><?php echo __('One-off Charge') ?></th>
                <th class="charge"><?php echo __('Monthly Charge') ?></th>
                <th class="charge"><?php echo __('Yearly Charge') ?></th>
                <th class="<?php if ($disableProductActions) echo 'last-charge-column' ?>"><?php echo __('Total') ?></th>
                <?php if (!$disableProductActions): ?>
                    <th class="product-action"></th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($addedProducts as $product): ?>
                <tr data-cost="<?php echo isset($product['extra_data']['override_cost']) ? $product['extra_data']['override_cost'] : $product['total']; ?>"
                    class="<?php if (!empty($product['extra_data']['has_addon'])) echo 'has-addon' ?>">
                    <td class="product-name">
                        <div class="product-name product<?php echo $product['product_id']; ?>">
                            <?php echo $product['label'] ?>
                        </div>
                    </td>
                    <td class="one-off charge"><?php if (isset($product['one_off_charge'])) echo $product['one_off_charge'] ?></td>
                    <td class="monthly charge"><?php if (isset($product['monthly_charge'])) echo $product['monthly_charge'] ?></td>
                    <td class="yearly charge"><?php if (isset($product['yearly_charge'])) echo $product['yearly_charge'] ?></td>
                    <td class="total charge">
                        <?php echo $product['total'] ?>
                        <?php if ($product['type'] === 'bundle') echo " <span>(pro-rata charge)</span>" ?>
                    </td>
                    <?php if (!$disableProductActions): ?>
                        <td class="added-product-action">
                            <?php
                            if ($product['removable']) {
                                include_partial('removeFromBasket', array(
                                    'productGroupId' => $product['product_id'],
                                    'markedAsAdded' => true,
                                    'extraClasses' => 'basket-product'
                                ));
                            }
                            ?>
                        </td>
                    <?php endif; ?>
                </tr>

                <?php if (!empty($product['editable_amount']) || !empty($product['editable_recharge'])): ?>
                    <tr class="editable-credit-form-row">
                        <td colspan="6">
                            <?php echo form_tag('update_call_credit_ajax', array('id' => 'editable-credit-form')) ?>
                            <input type="hidden" value="<?php echo $product['product_id'] ?>" name="product_id" />

                            <?php if (!empty($product['editable_amount'])): ?>
                                <div class="form-field" style="width: 400px; float: left; margin: 10px 0px;">
                                    <ul class="radio_list">
                                        <li>
                                            <input name="topup[topup-amount]" type="radio" value="20" id="topup_topup-amount_20" <?php echo ($product['one_off_charge'] == 20.0) ? 'checked="checked"' : NULL; ?>>&nbsp;
                                            <label for="topup_topup-amount_20">£20.00 +20% VAT (£24.00)</label>
                                        </li>
                                        <li>
                                            <input name="topup[topup-amount]" type="radio" value="50" id="topup_topup-amount_50"  <?php echo ($product['one_off_charge'] == 50.0) ? 'checked="checked"' : NULL; ?>>&nbsp;
                                            <label for="topup_topup-amount_50">£50.00 +20% VAT (£60.00)</label>
                                        </li>
                                        <li>
                                            <input name="topup[topup-amount]" type="radio" value="100" id="topup_topup-amount_100"  <?php echo ($product['one_off_charge'] == 100.0) ? 'checked="checked"' : NULL; ?>>&nbsp;
                                            <label for="topup_topup-amount_100">£100.00 +20% VAT (£120.00)</label>
                                        </li>
                                        <li>
                                            <input name="topup[topup-amount]" type="radio" value="input" id="topup_topup-amount_xxx" <?php echo (!in_array($product['one_off_charge'], array(20, 50, 100))) ? 'checked="checked"' : NULL; ?>>&nbsp;
                                            <label for="topup_topup-amount_input">
                                                Enter amount £
                                                <input name="topup[topup-amount-input]" type="text" value="<?php echo number_format($product['editable_charge'], 2); ?>" id="topup_topup-amount_input" min="20" maxlength="6">
                                                <span class="topup-amount-fill">+20% VAT (£<span class="topup-amount-with-vat"><?php echo number_format($product['editable_charge'] * 1.2, 2); ?></span>)</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($product['editable_recharge'])): ?>
                                <div class="auto-recharge-box" style="width: 440px; float: right; margin-right: 20px;">
                                    <?php $autoTopupEnabled = false; ?>
                                    <?php if($autoTopupEnabled): ?>
                                        <strong><?php echo __('You currently have Auto Top-up enabled'); ?></strong>
                                        <p><?php echo __('Auto Top-up works by topping up your account automatically when your account balance falls below &pound;5.00. If you do not wish to have Auto Top-up enabled on your account, please untick the box below.'); ?></p>
                                    <?php else: ?>
                                        <strong><?php echo __('Save time with Auto Top-up'); ?></strong>
                                    <?php endif; ?>
                                    <p><?php echo __('Your balance will be automatically topped up with the same amount and payment method as your most recent transaction. You can easily cancel your Auto Top-up settings at any time.'); ?></p>
                                    <p style="color: #515151;">
                                        <input id="topup_auto-recharge" type="checkbox" <?php if ($product['auto-recharge']) echo 'checked="checked"'; ?> name="topup[auto-recharge]">
                                        <?php echo __('Top-up my account automatically.'); ?>
                                    </p>
                                </div>
                            <?php endif; ?>

                            <div class="update-call-credit-submit">
                                <button class="button-orange floatright" type="submit" id="updateCallCredit">
                                    <span>Update Call Credit</span>
                                </button>
                            </div>
                            </form>
                        </td>
                    </tr>
                <?php elseif (!empty($product['extra_data']['has_addon'])): ?>
                    <?php include_partial('products/bundleAddon', array('addon' => $product['extra_data']['addon'], 'productId' => $product['product_id'], 'disableProductActions' => $disableProductActions)); ?>
                <?php endif; ?>

                <?php if (!empty($product['extra_data']['show_bundle_actions']) || (isset($showTAndCReload) && $showTAndCReload && !(isset($termsTicked['product'], $termsTicked['product'][$product['product_id']]) || (isset($termsTicked['bundle']) && $product['product_id'] == $termsTicked['bundle'])))): ?>
                    <tr class="bundle-actions-row">
                        <td colspan="6">
                            <div class="bundle-actions">
                                <?php if (!empty($product['extra_data']['show_addon_form'])): ?>
                                    <?php echo form_tag(url_for('upgrade_bundle'), array('id' => 'upgrade-bundle')) ?>
                                        <div class="addon-bundle-form-row addon-bundle-field">
                                            <div class="addon-bundle-icon"></div>
                                            <div class="addon-bundle-description">
                                                <strong><?php echo __('International Add-On') ?></strong>
                                            </div>
                                            <div class="addon-bundle-approval">
                                                <p>
                                                    <strong>Need international numbers?</strong><br />
                                                    <?php echo __('For a minimal charge of £%addon_price% per month International Landline numbers will be included in your Bundle minute allowance.', array('%addon_price%' => $product['extra_data']['addon_price'])) ?>
                                                </p>
                                            </div>
                                            <div class="buttons">
                                                <button type="submit" class="button-green float-right">
                                                    <span>Add To Bundle</span>
                                                </button>
                                            </div>
                                            <div class="addon-bundle-denial">
                                                <p>
                                                    <span class="addon-bundle-denial-label"><?php echo __('No thanks X') ?></span>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                <?php endif; ?>

                                <?php if (!empty($product['extra_data']['show_pin_pair_form'])): ?>
                                    <div class="addon-bundle-form-row pin-pair-selection">
                                        <div class="pin-pair-selection-fields" data-url="<?php echo url_for('select_aycm_pin_pair') ?>">
                                            <?php include_partial('individualBundleUserSelection', array('individualBundleForm' => $product['extra_data']['pin_pair_form'])) ?>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($product['extra_data']['show_t_and_c_form'])): ?>
                                    <div class="bundle-tc-action">
                                        <label class="bundle-tc" for="bundle-tc">
                                            I have read and agreed to the <a href="<?php echo url_for('@terms_and_conditions'); ?>">terms and conditions</a> for this product.
                                            <input type="checkbox" id="bundle-tc" name="bundle-tc" value="1" data-url="<?php echo url_for('agree_to_bundle_t_and_c') ?>" />
                                        </label>
                                    </div>
                                    <?php if (isset($hideTAndC) && $hideTAndC) : ?>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                var $agreeToTAndC = $('#bundle-tc');
                                                $.post($agreeToTAndC.data('url'))
                                                    .done(function(responseArr) {
                                                        if ("basket_page" in responseArr) {
                                                            dataLayer.push({'event': 'ProductSelectorTool/Basket'});
                                                            $('.bundle-tc-action').hide();
                                                        }
                                                    });
                                            });
                                        </script>
                                    <?php endif; ?>
                                <?php elseif (isset($showTAndCReload) && $showTAndCReload && !(isset($termsTicked['product'], $termsTicked['product'][$product['product_id']]) || (isset($termsTicked['bundle']) && $product['product_id'] == $termsTicked['bundle']))) : ?>
                                    <div class="bundle-tc-action">
                                        <label class="bundle-tc" for="bundle-tc">
                                            I have read and agreed to the <a href="/Terms-And-Conditions">terms and conditions</a> for this product.
                                            <input type="checkbox" id="bundle-tc" name="bundle-tc" value="1" data-url="<?php echo url_for('agree_to_bundle_t_and_c') ?>" />
                                        </label>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>