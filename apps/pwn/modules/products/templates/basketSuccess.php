<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('Your Basket'), 'headingSize' => 'm', 'disableBreadcrumbs' => true)
    ); ?>
    <div class="clearfix"></div>

    <div class="basket-content">
        <?php include_partial(
            'products/basketProductTable',
            array(
                'addedProducts'   => $addedProducts,
                'hideTAndC'       => isset($hideTAndC) ? $hideTAndC : false,
                'showTAndCReload' => isset($showTAndCReload) ? $showTAndCReload : false,
                'termsTicked'     => isset($termsTicked) ? $termsTicked : false
            )
        ); ?>

        <p class="total-cost">The total cost of your basket is <strong>£<span class="total-cost-value" data-basket-total="<?php echo totalBasketCost() ?>"><?php echo number_format(totalBasketCost(),2,'.',','); ?></span></strong>.</p>

        <?php echo form_tag('basket_submit', array('id' => 'basket-checkout')) ?>
            <div id="affiliate-code-field">
                <div>
                    <label for="affiliate-code"><?php echo __('Had help getting here? Enter team member name:') ?></label>
                    <input type="text" value="" name="affiliate-code" id="affiliate-code" placeholder="Optional" />
                </div>
            </div>
            <button class="button-orange float-right" type="submit" value="Update account">
                <span>Checkout</span>
            </button>
        </form>
        <?php echo form_tag('products_select') ?>
            <button class="button-green float-right" type="submit" value="Update account">
                <span>Continue Shopping</span>
            </button>
        </form>
    </div>

    <div class="empty-basket-message">
        <?php include_partial('emptyBasket') ?>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".product-accordion").accordion({
                autoHeight: false
            });
        });
    </script>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>