<?php echo form_tag(url_for('add_to_basket'), array('enctype' => 'application/x-www-form-urlencoded', 'class' => 'product-addition-action')); ?>
    <input type="hidden" name="add-product" value="<?php echo $productGroupId ?>" >
    <button type="submit" value="<?php echo $addLabel ?>"
            class="<?php echo ($markedAsAdded) ? "button-disabled" : "button-green";?>" style="position:relative; left:0px;top:0px;"
            <?php if ($markedAsAdded) { echo 'disabled="disabled"'; } ?>>
        <span><?php echo $addLabel ?></span>
    </button>
</form>