<?php $productForRemoval = $productForRemoval->getRawValue(); ?>
<?php $sf_response->setTitle(__('Confirm removing of product')) ?>

<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php echo form_tag('confirmed_product_removal') ?>
        <p>Are you sure you want to remove the product "<?php echo $productForRemoval['group_name'] ?>"?</p>
        <?php echo link_to('Cancel', 'products_select') ?>
        <button type="submit" class="button-orange">
            <span>Remove product</span>
        </button>
    </form>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>