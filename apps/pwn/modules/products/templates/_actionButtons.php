<div class="clearfix">

<?php switch($userType){ 

    case "PLUS_ADMIN":?>

    
    <?php if($productGroupEnabled):?>
    
        <p>This product is already enabled on your account</p>

	<?php else:?>
	
		<?php echo form_tag(url_for('products'), array('enctype' => 'application/x-www-form-urlencoded', 'class' => 'form-enable-product pwnform clearfix')); ?>

		<input type="hidden"
			name="productGroupID"
			value="<?php echo $productGroup['product_group_id']; ?>" /> 

		<button type="submit" class="button-orange floatright">
			<span><?php echo __('Assign Product'); ?> </span>
		</button>

	    		    		
	<?php endif;?>


<?php break;
    case "POWWOWNOW":?>

	<button type="submit"
		class="button-orange floatright upgrade_button">
		<span><?php echo __('Upgrade To Plus'); ?> </span>
	</button>


<?php break;
case "ANON":?>

	<button type="submit"
		class="button-orange floatright register_button">
		<span><?php echo __('Register'); ?> </span>
	</button>


	<button  type="submit"
		class="button-orange floatright upgrade_button">
		<span><?php echo __(' Upgrade'); ?> </span>
	</button>

	<button type="submit"
		class="button-orange floatright login_button">
		<span><?php echo __('Login'); ?> </span>
	</button>


<?php break;
case "PLUS_USER":?>
    
    <?php if($productGroupEnabled):?>
    
        <p>This product is already enabled on your account</p>
    
    <?php else:?>
    
        <p>This product is not yet enabled on your account</p>
    
    <?php endif;?>
        
<?php break;
 default:?>

<?php break;}?>

	<button type="submit"
		class="button-orange floatright products_button">
		<span><?php echo __('Products'); ?> </span>
	</button>
<?php if($userType=='PLUS_ADMIN' && $productGroupEnabled){?> </form><?php }?>	
	
</div>
