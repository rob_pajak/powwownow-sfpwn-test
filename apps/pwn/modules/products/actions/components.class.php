<?php

class ProductsComponents extends sfComponents
{
    /**
     * Gets products assigned to the account for admins, or assigned to the pin for a user
     * @param null $features - Features to be returned
     * @return array
     */
    private function getActiveProducts($features = null)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        // If an admin, then get the features assigned to the account
        if ($user->hasCredential('admin')) {
            $groupsResponse = Hermes_Client_Rest::call('getAllPlusProductGroups', array(
                'locale'     => $user->getCulture(),
                'account_id' => $user->getAttribute('account_id'),
                'feature'    => $features,
            ));

            return $groupsResponse['assigned_products'];
        }

        $contactPinInfo = Hermes_Client_Rest::call('getContactPins', array(
            'contact_ref' => $user->getAttribute('contact_ref'),
            'active_only' => 'Y',
        ));

        return Hermes_Client_Rest::call(
            'getPlusPinProductGroups',
            array('pin_ref' => $contactPinInfo[0]['pin_ref'], 'feature' => $features)
        );
    }

    /**
     * Gets all features (both assigned and unassigned)
     * @return array
     */
    private function getFeatures()
    {
        $user = $this->getUser();

        // Fetch the products assigned to the account
        $groupsResponse = Hermes_Client_Rest::call('getAllPlusProductGroups', array(
            'locale'     => $user->getCulture(),
            'account_id' => $user->getAttribute('account_id'),
            'feature'    => true,
        ));

        return $groupsResponse['assigned_products'];
    }

    /**
     * Gets rates for the specified $products
     *
     * @param array $products
     * @return array
     */
    private function getRatesForProducts($products)
    {
        $rates = array();

        foreach ($products as $group) {
            $rates[$group['product_group_id']] = Hermes_Client_Rest::call(
                'getPlusUsageRatesPerProductGroup',
                array(
                    'group_id'               => $group['product_group_id'],
                    'aggregate_numbers'      => 1,
                    'dnis_type_translations' => array(
                        array('find' => 'Freefone', 'replace' => 'Freephone'),
                        array('find' => 'Geographic', 'replace' => 'Landline'),
                    )
                )
            );
        }

        return $rates;
    }

    /**
     * Shows the features enabled on an account for admins
     * Shows the all features, and a disabled / enabled text for a user
     */
    public function executeFeatureTable()
    {
        /** @var myUser $user */
        $user = $this->getUser();
        $this->setVar('isAdmin', $user->hasCredential('admin'));
        $this->setVar('isUser', $user->hasCredential('user'));

        if ($this->getVar('isAdmin')) {
            $products = $this->getActiveProducts(true);
            $productRates = $this->getRatesForProducts($products);
        } else {
            $activeProductsSource = $this->getActiveProducts(true);
            $activeProducts = array();
            foreach($activeProductsSource as $product) {
                $activeProducts[] = $product['id'];
            }
            $products = $this->getFeatures();
            $productRates = $this->getRatesForProducts($products);
            $this->setVar('activeProducts', $activeProducts);
        }

        $this->setVar('products', $products, true);
        $this->setVar('productRates', $productRates, true);

        /**
         * Disabling Automatic Products for Now.
         * Pick up in Iteration 13
         * Says: Maarten
         */
//        $automaticProducts = array(
//            array(
//                'group_name' => 'Schedule a conference',
//                'group_summary' => '',
//            ),
//            array(
//                'group_name' => 'Plugin for Outlook',
//                'group_summary' => '',
//            ),
//            array(
//                'group_name' => 'Manage my call settings',
//                'group_summary' => '',
//            ),
//            array(
//                'group_name' => 'Hold a web conference',
//                'group_summary' => '',
//            ),
//            array(
//                'group_name' => 'View in-conference controls',
//                'group_summary' => '',
//            ),
//            array(
//                'group_name' => 'Discover international numbers',
//                'group_summary' => '',
//            ),
//        );
        $this->setVar('automaticProducts', array());
    }

    /**
     * Component for the intro text for the 'My Products' page
     */
    public function executeMyProductsIntroText()
    {

    }

    /**
     * Component for the buttons on the 'My Products' page
     */
    public function executeMyProductsButtons()
    {

    }

    /**
     * Gets the products (not including features) enabled on an account
     */
    public function executeProductTable()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('BWM');

        /** @var myUser $user */
        $user = $this->getUser();

        $products = $this->getActiveProducts(false);
        $productRates = $this->getRatesForProducts($products);

        // Add additional metadata to any BWMs for the view
        $products = plusCommon::processPotentialBWMs($products, $productRates);
        $bundleDisplay = new BundleProductDisplay();
        $this->setVar('products', $bundleDisplay->prepareAssignedProducts($products, $user->getContactRef()), true);
        $this->setVar('productRates', $productRates);

        $bwmGetRatePerDnis = Hermes_Client_Rest::call('BWM.getRatePerDnis', array());
        $this->setVar('bwmProductRatePerDnis', $bwmGetRatePerDnis['rate_per_dnis']);

        $bwmGetAccountConnectionFee = Hermes_Client_Rest::call('BWM.getAccountConnectionFee', array());
        $this->setVar('bwmAccountConnectionCost', $bwmGetAccountConnectionFee['connection_fee']);

        $this->setVar('isAdmin', $user->hasCredential('admin'));
    }

    /**
     * Renders products in the tooltip before purchase, after purchase and in the basket
     *
     * @author Vitaly Kondratiev
     * @author Asfer
     * @author Maarten
     *
     * @return string tooltip html
     */
    public function executeProductTooltip()
    {
        $this->currencySymbol = '£';

        if ($this->productGroup['product_group_id'] < 0) {
            $this->hasBeenPurchased = false;
        } else {
            $this->hasBeenPurchased = true;
        }

        $this->isBwm    = !$this->hasBeenPurchased || (isset($this->productGroup['is_BWM']) && $this->productGroup['is_BWM']);
        $this->isBundle = !empty($this->productGroup['is_bundle']);

        $this->isPlusAdmin = $this->getUser()->hasCredential('PLUS_ADMIN');
        if ($this->isBwm && $this->isPlusAdmin) {

            $ratePerBwmDnisResponse = Hermes_Client_Rest::call('BWM.getRatePerDnis', array());
            $this->ratePerBwmDnis   = $ratePerBwmDnisResponse['rate_per_dnis'];

            if (!$this->hasBeenPurchased) {

                $bwmGetAccountConnectionFeeResponse = Hermes_Client_Rest::call('BWM.getAccountConnectionFee', array());
                $this->bwmAccountConnectionFee      = $bwmGetAccountConnectionFeeResponse['connection_fee'];

                // Get the number of dnises assigned to the product from the basket
                $this->basket  = new Basket($this->getUser());
                $basketProduct = $this->basket->retrieveAddedProductById($this->productGroup['product_group_id']);

                $this->dnisCount = 0;

                // @kludge Currently the dnises are sitting in an array with other items, they should be in their own array!
                foreach ($basketProduct[1] as $v) {
                    if (!empty($v['id']) || !empty($v['country_code'])) {
                        $this->dnisCount++;
                    }
                }

            } else {

                $this->bwmAccountConnectionFee = $this->productGroup['account_connection_fee'];

                $this->dnisCount = count($this->productGroup['dnises']);

                $chargeDate           = new DateTime($this->productGroup['next_cyclic_charge_date']);
                $this->nextChargeDate = $chargeDate->format('d/m/Y');

            }

            $this->totalDnisCost    = ($this->dnisCount * $this->ratePerBwmDnis);
            $this->totalProductCost = $this->totalDnisCost + $this->bwmAccountConnectionFee;
        }
    }

    /**
     * Branded Welcome Message Row
     */
    public function executeBwmRow()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Basket');

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $response->addStylesheet('/sfcss/components/products.bwmRow.css');

        $this->setVar('bwmProductRates', (isset($this->bwmProductRates) ? $this->bwmProductRates : array()));
        $this->setVar(
            'bwmAccountConnectionCost',
            (isset($this->bwmAccountConnectionCost) ? $this->bwmAccountConnectionCost : array())
        );
        $this->setVar(
            'bwmProductRatePerDnis',
            (isset($this->bwmProductRatePerDnis) ? $this->bwmProductRatePerDnis : array())
        );
    }
}
