<?php

require_once dirname(__FILE__) . '/../../login/lib/AJAXResponse.php';

/**
 * Products Actions.
 *
 * @package    powwownow
 * @subpackage products
 */
class productsActions extends sfActions
{
    /**
     *
     */
    public function executeRedirectToIndex()
    {
        $this->redirect($this->generateUrl('products_select'));
    }

    /**
     * My Products page - Gives the customer visibility of their products and features available on their account.
     *
     * @author Mark Wiseman
     * @author Asfer Tamimi
     */
    public function executeMyProducts()
    {
        /** @var myUser $user */
        $user            = $this->getUser();
        $accountId       = $user->getAccountId();

        $this->setVar('isAdmin', $user->hasCredential('admin'));
        $this->setVar('isUser', $user->hasCredential('user'));
        $this->setVar('isPostPay', $user->isPostPayCustomer());

        if ($this->getVar('isAdmin')) {
            $response = plusCommon::getAccountBalanceAndUsers($accountId, $user->getAttribute('contact_ref'));
            $this->setVar('balance', $response['balance']);
            $this->setVar('adminHasUsers', $response['accountUsers'] == 'M');

            $response = Hermes_Client_Rest::call('Plus.getAutoTopupSettings', array('account_id' => $accountId));
            $this->setVar('autoTopUpEnabled', isset($response['id']));

            // See if at least one user has no products assigned
            $response = Hermes_Client_Rest::call(
                'ProductGroup.getProductGroupsAssignedToUsersCount',
                array('account_id' => $accountId)
            );

            $atLeastOneUserHasNoProducts = false;
            foreach ($response as $user) {
                if (!$user['is_admin'] && $user['product_count'] == 0) {
                    $atLeastOneUserHasNoProducts = true;
                }
            }
            $this->setVar('atLeastOneUserHasNoProducts', $atLeastOneUserHasNoProducts);

            $accountProducts = $this->getProductGroups();
            $this->setVar('hasPurchasedAProduct', (count($accountProducts['product_list']) > 0));
            $this->setVar('hasBWM', $this->hasPurchasedBWM($accountProducts['assigned_products']));

        }
    }

    private function hasPurchasedBWM(array $purchasedProducts)
    {
        foreach ($purchasedProducts as $product) {
            if (!empty($product['allocated']) && !empty($product['dedicated']) && !empty($product['active'])) {
                return true;

            }
        }
        return false;
    }

    /**
     * Products Selection Page [PAGE]
     *
     * This is the Start of the Main Products Workflow.
     *
     * @author Vialy
     * @author Asfer
     * @author Maarten
     * @author Michael
     *
     */
    public function executeIndex()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Basket');

        /** @var myUser $user */
        $user = $this->getUser();

        if (!$user->hasCredential('admin')) {
            $this->redirect($this->generateUrl('my_products'));
        }

        $basket = new Basket($user);

        // Account Information. Used in the User Flow
        $this->setVar('accountInfo', plusCommon::getAccountBalanceAndUsers(
            $user->getAttribute('account_id'),
            $user->getAttribute('contact_ref')
        ));

        $nonBwmProducts = $basket->retrieveNonExistingProducts(true);
        $creditProduct  = false;
        if (count($nonBwmProducts) > 0) {
            foreach ($nonBwmProducts as $productId => $product) {
                if ($product['type'] == 'credit') {
                    $creditProduct = $productId;
                }
            }
        }
        $this->setVar('creditProduct', $creditProduct);

        // Variables required to display the BWM row.
        $this->setVar('bwmProductRates', Hermes_Client_Rest::call(
            'BWM.getProductDialInNumbersWithRates',
            array(
                'dedicated'    => 1,
                'allocated'    => 0,
                'service_ref'  => 850,
                'product_type' => 'VOICE',
                'grouped'      => 1
            )
        ));
        $bwmGetAccountConnectionFee = Hermes_Client_Rest::call('BWM.getAccountConnectionFee', array());
        $this->setVar('bwmAccountConnectionCost', $bwmGetAccountConnectionFee['connection_fee']);

        $bwmGetRatePerDnis = Hermes_Client_Rest::call('BWM.getRatePerDnis', array());
        $this->setVar('bwmProductRatePerDnis', $bwmGetRatePerDnis['rate_per_dnis']);

        $hasBundleBeenAddedToBasket = $basket->hasBundleBeenAdded();
        $bundleIds                  = $basket->retrieveBundleProductIds();
        $selectedBundleId           = array_shift($bundleIds);
        $hasBundleBeenActivated     = plusCommon::retrievePostPayStatus($user->getAttribute('account_id'));
        $bundleDisplay              = new BundleProductPageDisplay();
        $bundleProducts             = plusCommon::retrieveBundleProducts($user->getCulture());
        $this->setVar('bundles', $bundleDisplay->prepareBundlesForDisplay(
            $bundleProducts,
            $hasBundleBeenActivated,
            $hasBundleBeenAddedToBasket,
            $selectedBundleId
        ));

        $this->setVar('individualBundleForm', new IndividualUserBundleAssignForm());
        $this->setVar('hasBundleBeenActivated', $hasBundleBeenActivated);
        $this->setVar('hasBundleBeenAddedToBasket', $hasBundleBeenAddedToBasket);
        $this->setVar('selectedBundleId', $selectedBundleId);
        $this->setVar('isAdmin', $user->hasCredential('admin'));
    }

    private function translateBundleShortNameToBundleId(array $bundles, $bundleName)
    {
        foreach ($bundles as $bundle) {
            if (strcasecmp($bundle['group_name_short'], $bundleName) === 0) {
                return (int) $bundle['product_group_id'];
            }
        }
        return false;
    }

    /**
     * Products Selection Page [AJAX]
     *
     * This is the Start of the Main Products Workflow.
     *
     * @author Vitaly
     * @amend Asfer
     *
     */
    public function executeUpdateAccountProducts(sfWebRequest $request) {
        // Check if the User Posted the Form Correctly
        if (!($request->isXmlHttpRequest() && $request->isMethod('POST'))) {
            $this->logMessage('Method was accessed without being Posted and being an Ajax Call', 'err');
            $this->forward404();
        }

        // Populate the Assigned Before Array
        $assignedBefore = array();
        $listProductGroups = $this->getProductGroups();
        foreach ($listProductGroups['assigned_products'] as $p) {
            $assignedBefore[] = $p['product_group_id'];
        }

        // Populate the Assigned After Array
        $assignedAfter  = array();
        foreach ($request->getParameterHolder()->getAll() as $key => $value) {
            if (substr($key,0,5) =='prod-') {
                $assignedAfter[$key] = $value;
            }
        }

        // Use Array Diff to find the Assigned and Unassigned Products
        $assignProducts   = array_diff($assignedAfter, $assignedBefore);
        $unassignProducts = array_diff($assignedBefore, $assignedAfter);

        // Store the Above into Session
        Plus_Authenticate::startSession();
        $_SESSION['assigned_products']   = $assignProducts;
        $_SESSION['unassigned_products'] = $unassignProducts;
        $_SESSION['credit_amount']       = null;

        // Update the Account Product Groups
        try {
            $response = Hermes_Client_Rest::call('doUpdatePlusAccountProductGroups', array(
                'account_id'                 => $this->getUser()->getAttribute('account_id'),
                'assign_product_group_ids'   => $assignProducts,
                'unassign_product_group_ids' => $unassignProducts
            ));

            /**
             * Clear Teamsite cache
             */
            try {
                PWN_Cache_Clearer::modifiedPin($this->getUser()->getAttribute('contact_ref'));
                PWN_Cache_Clearer::modifiedContact($this->getUser()->getAttribute('contact_ref'));
                $this->logMessage('Cleared Cache for contact_ref: ' . $this->getUser()->getAttribute('contact_ref') . ' while switching to Plus', 'info');
            } catch (Exception $e) {
                $this->logMessage('Unable to clear cache after doUpdatePlusAccountProductGroups' .  serialize($e), 'err');
            }

        } catch(Exception $e) {

            $this->logMessage('Exception for update Account Products: ' . $e->getMessage(), 'err');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR');

            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setHttpHeader('Content-type','text/plain');

            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;

        }

        // Obtain All PINs
        try {
            $pins = Hermes_Client_Rest::call('getAccountPinsByContactRef', array(
                'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                'active' => 'Y'
            ));

            $userArgs = array();
            foreach ($pins as $id => $pin) {
                $userArgs[$pin['pin_ref']] = array('pin_ref' => $pin['pin_ref']);
            }

        }catch(Exception $e) {

            $this->logMessage('Exception for Get Contact PINS Triggered: ' . serialize($e->responseBody), 'err');
            $errorMessages[] = array('message' => 'API_RESPONSE_SYSTEM_ERROR_PIN_ACCOUNT_PRODUCTS');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setHttpHeader('Content-type: text/plain');
            echo json_encode(array('error_messages' => $errorMessages));
            exit;

        }

        foreach ($pins as $pin) {
            $updateArgs[$pin['pin_ref']] = array(
                'pin_ref'                    => $pin['pin_ref'],
                'assign_product_group_ids'   => $assignProducts,
                'unassign_product_group_ids' => $unassignProducts
            );
        }

        // If this is a Single User Update His PINs aswell
        if (count($pins)<3) {
            $this->_updatePins($updateArgs,$pins);
        }

        echo json_encode(array('success_message' => 'FORM_RESPONSE_PINS_PRODUCTS_UPDATE_SUCCESS'));
        return sfView::NONE;

    }

    private function retrieveIndividualBundleIdsFromPinProducts(array $pinProducts)
    {
        $individualUserBundleIds = array();
        foreach ($pinProducts as $product) {
            if (!empty($product['is_individual_bundle'])) {
                $individualUserBundleIds[] = $product['product_group_id'];
            }
        }
        return $individualUserBundleIds;
    }

    private function hasIndividualBundleBeenAssigned(array $userMap, array $individualBundleIds)
    {
        $assignedBundleIds = array();
        foreach ($userMap as $user) {
            foreach ($user['pinInfo'] as $productGroupId => $set) {
                if (in_array($productGroupId, $individualBundleIds) && $set) {
                    $assignedBundleIds[] = $productGroupId;
                }
            }
        }
        return $assignedBundleIds;
    }

    /**
     * My PIN Products Page [PAGE]
     *
     * This page will show the Products for each PIN Ref. The User will be able to Enable / Disable A Product for each PIN Ref
     *
     * @author Asfer
     * @return sfView::NONE | sfView::SUCCESS | string | redirect
     */
    public function executeMyPinProducts()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Text', 'Number'));

        /** @var myUser $user */
        $user       = $this->getUser();
        $accountId  = $user->getAccountId();
        $contactRef = $user->getContactRef();

        $accountInfo = plusCommon::getAccountBalanceAndUsers($accountId, $contactRef);

        $this->setVar('isPostPayCustomer', $user->isPostPayCustomer());

        // Checks for balance to decide if show notification
        $this->setVar('noCreditNotification', false);
        if ($accountInfo['balance'] == 0) {
            $this->setVar('noCreditNotification', true);
        }

        // Check to See if the Account User is Single. Redirect them to the Select-Products Page
        if ($accountInfo['accountUsers'] == 'S') {
            $this->logMessage(
                'Single User tried to access Assign-Products Page. Account_ID: ' . $accountId . ', Contact Ref: ' . $contactRef,
                'err'
            );
            $this->redirect('products/index');
            return sfView::NONE;
        }

        $this->setVar('accountInfo', $accountInfo);

        // Obtain All the Account Product Groups
        $allProducts      = $this->getProductGroups();
        $assignedProducts = $allProducts['assigned_products'];
        $bundleDisplay          = new BundleProductDisplay();
        $assignedProducts = $bundleDisplay->prepareAssignedProducts($assignedProducts, $contactRef);
        $individualBundleIds    = $this->retrieveIndividualBundleIdsFromPinProducts($assignedProducts);

        // Have landline bundles been added?
        $this->setVar('releventBundleLandLineFeatures', $this->getRelevantLandLineFeatures($assignedProducts));

        // Collect features for dialogs.
        $featureProducts = array();
        foreach ($assignedProducts as $product) {
            if (!empty($product['feature'])) {
                $featureProducts[$product['product_group_id']] = $product;
            }
        }
        $this->setVar('featureProducts', $featureProducts);

        $this->setVar('successTransaction', $this->_hasSuccessfulTransactions());
        $this->setVar('pinsSet', false);

        // Obtain All PINs for the User, based on the user Type
        try {
            $contactPins = Hermes_Client_Rest::call(
                'getAccountPinsByContactRef',
                array(
                    'contact_ref' => $contactRef,
                    'active'      => 'Y'
                )
            );
        } catch (Exception $e) {
            $this->logMessage('Exception for Get Contact PINS Triggered: ' . print_r($e->getMessage()), 'err');
            $this->redirect('myPwn/Pins');
            return sfView::NONE;
        }

        // Create Users Array and Product Status for the User
        $usersArr  = array();
        $adminContact    = Hermes_Client_Rest::call('getPlusAccountAdmin', array('account_id' => $accountId));
        $adminContactRef = $adminContact['contact_ref'];
        foreach ($contactPins as $contactPin) {
            $contactRef = $contactPin['contact_ref'];

            if (!isset($usersArr[$contactRef]) && $contactPin['master_pin'] !== 0) {
                $usersArr[$contactRef] = array(
                    'contact_ref' => $contactRef,
                    'first_name'  => $contactPin['first_name'],
                    'last_name'   => $contactPin['last_name'],
                    'email'       => $contactPin['email'],
                );
            }

            if ($contactPin['master_pin'] !== 0) {
                $assignedPinProducts = Hermes_Client_Rest::call(
                    'getPlusPinProductGroups',
                    array(
                        'pin_ref' => $contactPin['pin_ref']
                    )
                );

                foreach ($assignedProducts as $productInfo) {
                    $assignedProductGroupId = $productInfo['product_group_id'];
                    $adminFeature           = $productInfo['feature'] == 1 && $adminContactRef == $contactRef;

                    // If the product is enabled for one of the PINs of the contact, this value will already be set to true.
                    // In that case, we skip checking if it's enabled (because it already is).
                    if (empty($usersArr[$contactRef]['pinInfo'][$assignedProductGroupId])) {
                        if (count($assignedPinProducts)) {
                            $status = null;
                            foreach ($assignedPinProducts as $product) {
                                if ($product['product_group_id'] == $assignedProductGroupId) {
                                    $usersArr[$contactRef]['pinInfo'][$assignedProductGroupId] = true;
                                    $status                                                          = true;
                                    $this->setVar('pinsSet', true);
                                    if ($adminFeature) {
                                        $usersArr[$contactRef]['featureInfo'][$assignedProductGroupId] = true;
                                    }
                                    break;
                                }
                            }
                            if (is_null($status)) {
                                $usersArr[$contactRef]['pinInfo'][$assignedProductGroupId] = false;
                            }
                        } else {
                            $usersArr[$contactRef]['pinInfo'][$assignedProductGroupId] = false;
                        }
                    }
                }
            }
        }

        if ($individualBundleIds) {
            $this->setVar('assignedIndividualBundleIds', $this->hasIndividualBundleBeenAssigned(
                $usersArr,
                $individualBundleIds
            ), true);
        } else {
            $this->setVar('assignedIndividualBundleIds', array(), true);
        }

        // Obtain the Dial In Number Rates
        $listUsageRates = array();
        foreach ($allProducts['product_list'] as $productGroupID) {
            try {
                $listUsageRates[$productGroupID['product_group_id']] = Hermes_Client_Rest::call(
                    'getPlusUsageRatesPerProductGroup',
                    array(
                        'group_id'               => $productGroupID['product_group_id'],
                        'aggregate_numbers'      => 1,
                        'dnis_type_translations' => array(
                            array('find' => 'Freefone', 'replace' => 'Freephone'),
                            array('find' => 'Geographic', 'replace' => 'Landline'),
                        )
                    )
                );
            } catch (Exception $e) {
                $this->logMessage(
                    'Exception Occurred for api_getPlusUsageRatesPerProductGroup: ' . print_r($e->getMessage()),
                    'err'
                );
                $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            }
        }

        $this->setVar('allProducts', $allProducts);
        $this->setVar('assignedProducts', $assignedProducts, true);
        $this->setVar('usersArr', $usersArr, true);
        $this->setVar('listUsageRates', $listUsageRates, true);

        return sfView::SUCCESS;
    }

    /**
     * My PIN Products Page [AJAX]
     *
     * This page will show the Products for each PIN Ref.
     * The User will be able to Enable / Disable A Product for each PIN Ref
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer
     */
    public function executeMyPinProductsAjax(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user            = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $adminContactRef = $user->getAttribute('contact_ref');
        $adminAccountId  = $user->getAttribute('account_id');

        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->logMessage('Method was accessed without a valid Ajax Post Request', 'err');
            $this->forward404();
        }

        // Obtain the Form POST Parameters
        $requestParams = $request->getParameterHolder()->getAll();
        unset($requestParams['module']);
        unset($requestParams['action']);

        // Obtain All the Account Product Groups
        $allProducts      = $this->getProductGroups();
        $bundleCollection = new BundleCollection(plusCommon::retrieveBundleProducts($user->getCulture()));
        $this->setVar('assignedProducts', $allProducts['assigned_products']);
        $this->setVar('allProducts', $allProducts);

        // Filter the individual user bundle from the request parameters.
        $individualBundleIds = $bundleCollection->getIndividualBundleProductGroupIds();
        $pinRefs             = array_keys(plusCommon::retrievePinsFromContact($user->getContactRef()));
        $requestParams       = $this->filterOutIndividualBundle($requestParams, $individualBundleIds, $pinRefs);

        // User Arguments
        $userArgs = array();

        // Current Pre Update Database Values
        $currentArgs = array();

        try {
            // Obtain the Plus Account Product Groups
            $accountProductGroups = Hermes_Client_Rest::call(
                'getPlusAccountProductGroups',
                array(
                    'account_id' => $adminAccountId,
                    'locale'     => $user->getCulture()
                )
            );

            // Obtain All PINs relating to the Account Contact PINs
            $pins = Hermes_Client_Rest::call(
                'getAccountPinsByContactRef',
                array(
                    'contact_ref' => $adminContactRef,
                    'active'      => 'Y'
                )
            );

            foreach ($pins as $pin) {
                $userArgs[$pin['pin_ref']] = array('pin_ref' => $pin['pin_ref']);
            }

            // Obtain the PIN Product Groups
            foreach ($userArgs as $pin) {
                $pinProductGroups = Hermes_Client_Rest::call(
                    'getPlusPinProductGroups',
                    array(
                        'pin_ref' => $pin['pin_ref'],
                        'locale'  => $user->getCulture()
                    )
                );

                $productGroupID = array();
                foreach ($pinProductGroups as $productGroup) {
                    $productGroupID[] = $productGroup['product_group_id'];
                }
                $currentArgs[$pin['pin_ref']] = array(
                    'pin_ref'                  => $pin['pin_ref'],
                    'assign_product_group_ids' => $productGroupID
                );
            }
        } catch (Exception $e) {
            $this->logMessage('Exception for Get Account Product Groups: ' . print_r($e->getMessage()), 'err');
            $errorMessages[] = array('message' => 'API_RESPONSE_SYSTEM_ERROR_PIN_ACCOUNT_PRODUCTS');
            $response->setStatusCode(500);
            $response->setHttpHeader('Content-type', 'text/plain');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        // Convert Between Contact Ref and PIN Ref in the Request Params
        $newRequestParams = array();

        // Keep a list of pin_ref to contact_ref for the notify response.
        $masterPinsToContact = array();

        foreach ($accountProductGroups as $product) {
            $productId       = $product['product_group_id'];
            $prefixProductId = "p$productId";
            if (isset($requestParams[$prefixProductId]) && is_array($requestParams[$prefixProductId])) {
                foreach ($requestParams[$prefixProductId] as $contactRef) {
                    $contactPins = Hermes_Client_Rest::call('getContactPins', array('contact_ref' => $contactRef));
                    foreach ($contactPins as $contactInfo) {
                        if ($contactInfo['master_pin'] != 0) {
                            $newRequestParams[$prefixProductId][] = $contactInfo['pin_ref'];

                            // Collect the master_pin => contact_ref, if the contact_ref is not of admin.
                            // This is because we don't need to notify the admin (she made the changes).
                            if ($contactRef != $adminContactRef) {
                                $masterPinsToContact[$contactInfo['pin_ref']] = $contactRef;
                            }
                        }
                    }
                }
            }
        }
        $requestParams = $newRequestParams;

        // Set the Update Arguments
        foreach ($userArgs as $pin_ref => $pinArgs) {
            foreach ($accountProductGroups as $product) {
                $productId       = $product['product_group_id'];
                $prefixProductId = "p$productId";
                if (isset($requestParams[$prefixProductId]) && is_array($requestParams[$prefixProductId])) {
                    foreach ($requestParams[$prefixProductId] as $user_pin_ref) {
                        if ($user_pin_ref == $pin_ref) {
                            $userArgs[$pin_ref]['assign_product_group_ids'][] = $product['product_group_id'];
                            $userArgs[$this->getPinPair(
                                $pins,
                                $pin_ref
                            )]['assign_product_group_ids'][]                  = $product['product_group_id'];
                        }
                    }
                }
            }

            if (!isset($userArgs[$pin_ref]['assign_product_group_ids'])) {
                $userArgs[$pin_ref]['assign_product_group_ids']                           = array();
                $userArgs[$this->getPinPair($pins, $pin_ref)]['assign_product_group_ids'] = array();
            }
        }
        unset($userArgs[0]);

        // Compare the Two Arrays Together, to decide the Final Argument List
        $updateArgs = array();
        foreach ($currentArgs as $currentPin) {
            foreach ($userArgs as $newPin) {
                if ($currentPin['pin_ref'] == $newPin['pin_ref']) {
                    $updateArgs[$newPin['pin_ref']] = array(
                        'pin_ref'                    => $newPin['pin_ref'],
                        'assign_product_group_ids'   => array_diff(
                            $newPin['assign_product_group_ids'],
                            $currentPin['assign_product_group_ids']
                        ),
                        'unassign_product_group_ids' => array_diff(
                            $currentPin['assign_product_group_ids'],
                            $newPin['assign_product_group_ids']
                        ),
                    );
                    if ($individualBundleIds) {
                        $updateArgs[$newPin['pin_ref']]['unassign_product_group_ids'] = array_diff(
                            $updateArgs[$newPin['pin_ref']]['unassign_product_group_ids'],
                            $individualBundleIds
                        );
                    }
                }
            }
        }

        $this->_updatePins($updateArgs, $pins);

        // If the balance of the user is below the minimum, they should be aware that the products might not be usable
        // (i.e. the products won't work with no credit).
        $accountInfo        = plusCommon::getAccountBalanceAndUsers($adminAccountId, $adminContactRef);
        $insufficientCredit = $accountInfo['balance'] < plusCommon::calculateMinimumCreditRequired();

        echo json_encode(
            array(
                'success_message'     => 'FORM_RESPONSE_PINS_PRODUCTS_UPDATE_SUCCESS',
                'insufficient_credit' => $insufficientCredit,
                'notifiable_contacts' => $this->prepareNotifyProductAssignment($updateArgs, $masterPinsToContact),
            )
        );
        return sfView::NONE;
    }

    /**
     * Builds a map of contact reference to assigned products and contact name.
     *
     * This map is used to allow the admin to choose which user, if any, should be notified of the newly assigned products.
     *
     * @param array $updateArgs
     * @param array $masterPinsToContact
     * @return array
     * @author Maarten Jacobs
     */
    private function prepareNotifyProductAssignment($updateArgs, array $masterPinsToContact)
    {
        if (!is_array($updateArgs) || empty($updateArgs)) {
            return array();
        }

        // Create a map of contact_ref to product ids.
        $contactRefToAssignedProduct = array();
        foreach ($updateArgs as $masterPinRef => $update) {
            if (isset($masterPinsToContact[$masterPinRef]) && !empty($update['assign_product_group_ids'])) {
                $contactRef = $masterPinsToContact[$masterPinRef];
                if (!isset($contactRefToAssignedProduct[$contactRef])) {
                    $contactRefToAssignedProduct[$contactRef] = array(
                        'products' => array()
                    );
                }
                $productGroupIds = $update['assign_product_group_ids'];
                $contactRefToAssignedProduct[$contactRef]['products'] = array_merge(
                    $contactRefToAssignedProduct[$contactRef]['products'],
                    array_combine($productGroupIds, $productGroupIds)
                );
            }
        }

        // Add the first and last name of every contact in the response to the response.
        foreach ($contactRefToAssignedProduct as $contactRef => $contactData) {
            $contact = Common::getContactByRef(array('contact_ref' => $contactRef, 'master_service_ref' => null));
            if (isset($contact['first_name'], $contact['last_name'])) {
                $contactRefToAssignedProduct[$contactRef]['contact_name'] = $contact['first_name'] . ' ' . $contact['last_name'];
            } else {
                $this->logMessage(
                    "No full contact data found for a user which now has products assigned. Received contact data: " . serialize($contactData),
                    'error'
                );
            }
        }

        return $contactRefToAssignedProduct;
    }

    /**
     * Show product details
     *
     * Shows the full description of the product referenced in the last segment of the url
     *
     * Page is available to all users. However, a different of action buttons are available to users depending on their credentials
     *
     * @author Bob
     * @amend Asfer
     * @param sfWebRequest $request
     */
    public function executeDetails(sfWebRequest $request)
    {
        $user = $this->getUser();

        $usageRates        = array();
        $productGroup      = array();
        $currentProductIds = array();
        $products          = array(
            'worldwidefreephone' => 3,
            'worldwidelandline'  => 4,
            'ukfreephone'        => 5,
            'uklandline'         => 6
        );

        $this->setVar('productGroupEnabled', false);
        $locale = $user->getCulture();

        // Obtain Product Type
        $product = $request->getParameter('product');

        // Check to see if the Product Type is Valid, Otherwise Log Error and Show the 404 Page
        if (!in_array($product, array_keys($products))) {
            $this->logMessage('Method was accessed without a valid Product', 'err');
            $this->forward404();
        }

        // Check to see if the User is Authenticated
        if ($user->isAuthenticated()) {
            $this->setVar('successTransaction', $this->_hasSuccessfulTransactions());

            if ($user->hasCredential('PLUS_ADMIN')) {
                $userType          = 'PLUS_ADMIN';
                $currentProductIds = Hermes_Client_Rest::call(
                    'getPlusAccountProductIds',
                    array(
                        'account_id' => $user->getAttribute('account_id')
                    )
                );
            } elseif ($user->hasCredential('PLUS_USER')) {
                $userType = 'PLUS_USER';
            } else {
                $userType = 'POWWOWNOW';
            }
        } else {
            $userType = 'ANON';
            $this->setVar('successTransaction', false);
        }

        try {
            $productGroup = Hermes_Client_Rest::call(
                'Plus.getPlusProductFullDetails',
                array(
                    'locale'           => $locale,
                    'product_group_id' => $products[$product]
                )
            );
            $this->setVar('productGroup', $productGroup, true);
        } catch (Exception $e) {
            $this->logMessage('HERMES API call getPlusProductFullDetails failed on line ', 'err');
        }

        if (empty($productGroup)) {
            $this->logMessage('The Product: ' . $product . ' was not found', 'err');
        }

        $usageRatesResult = Hermes_Client_Rest::call(
            'getPlusUsageRatesPerProductGroup',
            array(
                'group_id'               => $products[$product],
                'aggregate_numbers'      => 1,
                'dnis_type_translations' => array(
                    array('find' => 'Freefone', 'replace' => 'Freephone'),
                    array('find' => 'Geographic', 'replace' => 'Landline'),
                )
            )
        );

        if (empty($usageRatesResult)) {
            $this->logMessage('Usage Rates Not Found for the Product: ' . $product, 'err');
        } else {
            foreach ($usageRatesResult as $rate) {
                $dnis       = ($rate['dnis_type'] != 'Geographic') ? $rate['dnis_type'] : 'Landline';
                $usageRates[] = array(
                    'country' => $rate['country'],
                    'city'    => $rate['city'],
                    'type'    => $dnis,
                    'rate'    => $rate['rate'] * 100 . ' pence'
                );
            }
        }

        if ($userType == 'PLUS_ADMIN' && in_array($productGroup['product_group_id'], $currentProductIds)) {
            $this->setVar('productGroupEnabled', true);
        }

        $this->setVar('usageRates', $usageRates, true);
        $this->setVar('userType', $userType);
        $this->setVar('productGroups', $this->getProductGroups());
        $this->setVar('productID', $products[$product]);
    }

    /**
     * Show product details (Ajax)
     *
     * Page is supposed to be shown in a modal window above the page content
     *
     * @author Vitaly
     *
     */
    public function executeDetailsAjax() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->forward404();
        }else{
            $this->getResponse()->setContent('<p align="center">NOTE ADDED</p>');
        }
        return sfView::NONE;
    }

    /**
     * update pins (private method)
     *
     * refactored from executeMyPinProductsAjax so that this code can be called in multiple places.
     *
     * method updates the pins activatong the products groups and emailing the users
     *
     * @param array $updateArgs
     * @param array $pins
     *
     * @author Bob
     *
     */
    private function _updatePins($updateArgs, $pins) {
        // Call the Hermes API with the Update Arguments
        try {

            // Build an array of pins refs being updated, this is used to see which users to send an email to
            $updatedPinRefs = array();
            foreach($updateArgs as $pin_ref => $pinArgs) {
                if (0 == count($pinArgs['assign_product_group_ids']) && 0 == count($pinArgs['unassign_product_group_ids'])) {
                    continue;
                }

                $updateArgs[$pin_ref]['response'] = Hermes_Client_Rest::call('updatePlusPinProductGroups',$pinArgs);
                $updatedPinRefs[] = $pin_ref;
            }

            /**
             * Clear Teamsite cache
             */
            try {
                PWN_Cache_Clearer::modifiedPin($this->getUser()->getAttribute('contact_ref'));
                PWN_Cache_Clearer::modifiedContact($this->getUser()->getAttribute('contact_ref'));
                $this->logMessage('Cleared Cache for contact_ref: ' . $this->getUser()->getAttribute('contact_ref') . ' while switching to Plus', 'info');
            } catch (Exception $e) {
                $this->logMessage('Unable to clear cache after doUpdatePlusAccountProductGroups' .  serialize($e), 'err');
            }

        }catch(Exception $e){

            $this->logMessage('Exception for Get Contact PINS Triggered: ' . serialize($e->responseBody), 'err');
            $errorMessages[] = array('message' => 'API_RESPONSE_SYSTEM_ERROR_PIN_ACCOUNT_PRODUCTS_UPDATE');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setHttpHeader('Content-type: text/plain');
            echo json_encode(array('error_messages' => $errorMessages));
            exit;

        }

        //  * Email the users of the pins which are having their products updated
        //  *
        //  * Email account admin letting them know pins associated with their account
        //  * have been update
        //  *
        //  * Due to dotmailer variable limits, we are unable to put the actual product
        //  * changes into the email, therefore we have considered these emails a bit
        //  * useless, and removed until we have products which charge customers upon
        //  * assigninment. Wiseman 2012-07-10
        //  *

        // try {
        //     // A user could have more than one pin being updated at the same time,
        //     // so we need to extract the pin refs for unique email addresses
        //     $pinRefsForUnqiueEmailAddresses = array();
        //     foreach ($pins as $pin) {
        //         if (in_array($pin['pin_ref'], $updatedPinRefs)) {
        //             $pinRefsForUnqiueEmailAddresses[$pin['email']] = $pin['pin_ref'];
        //         }
        //     }

        //     // If the admin is in the list remove it; he gets a seperate email regardless if
        //     // his pin was being updated
        //     unset($pinRefsForUnqiueEmailAddresses[$this->getUser()->getAttribute('email')]);

        //     // Send the email to each user
        //     foreach ($pinRefsForUnqiueEmailAddresses as $uniquePinRef) {
        //         Hermes_Client_Rest::call('sendPlusPinProductGroupsUpdateEmailDotmailer', array(
        //                 'locale'  => $this->getUser()->getCulture(),
        //                 'pin_ref' => $uniquePinRef)
        //         );
        //     }

        //     // Send email to admin
        //     Hermes_Client_Rest::call('sendPlusPinProductGroupsUpdateAdminEmailDotmailer', array(
        //             'locale' => $this->getUser()->getCulture(),
        //             'account_id' => $this->getUser()->getAttribute('account_id')
        //     ));

        // } catch(Exception $e) {

        //     // Sending email here is not that important, so only log an error
        //     $this->logMessage('An exception occured sending an email regarding update products assigned to pins: ' . serialize($e), 'err');

        // }

        // Obtain the Admin PINs, Check the chairman_present Field, Update the chairman_present Field to 'Start'
        $pins = Hermes_Client_Rest::call('getContactPins', array(
            'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
            'active_only' => 'Y'
        ));

        foreach($pins as $id => $pin) {
            if ($pin['chairman_present'] == '') {
                $updatePin = Hermes_Client_Rest::call('updatePin', array(
                    'pin_ref'          => $pin['pin_ref'],
                    'chairman_present' => 'Start'
                ));
            }
        }

        /**
        * Clear Teamsite cache
        */
        try {
            PWN_Cache_Clearer::modifiedPin($this->getUser()->getAttribute('contact_ref'));
            PWN_Cache_Clearer::modifiedContact($this->getUser()->getAttribute('contact_ref'));
            $this->logMessage('Cleared Cache for contact_ref: ' . $this->getUser()->getAttribute('contact_ref') . ' while switching to Plus', 'info');
        } catch (Exception $e) {
            $this->logMessage('Unable to clear cache after doUpdatePlusAccountProductGroups' .  serialize($e), 'err');
        }

    }

    /**
     * Get PIN Pair
     * @param $pins
     * @param $pin_ref
     * @return int
     */
    private function getPinPair($pins, $pin_ref) {

      foreach($pins as $id => $pin){
         if ($pin['pin_ref'] == $pin_ref) {
            $master_pin = $pin['master_pin'];
            break;
         }
      }

      if (isset($master_pin)) {
         foreach($pins as $id => $pin) {
            if ($pin['pin'] == $master_pin) {
               return $pin['pin_ref'];
            }
         }
      }

      return 0;

   }

    /**
     * Has Successful Transactions
     * @return bool
     */
    private function _hasSuccessfulTransactions() {
        // Check if the User has done a Manual Topup @Jim Says to check If the User has done a Payment
        $topupHistory = Hermes_Client_Rest::call('Plus.getTopupHistoryForAccount', array(
            'account_id'       => $this->getUser()->getAttribute('account_id'),
            'transaction_type' => 'manual',
            'date_from'        => date("Y-m-d", strtotime("-6 months")),
            'date_to'          => date('Y-m-d'),
        ));

        $successTransaction = false;
        foreach($topupHistory as $id => $topup) {
            if ($topup['transaction_status'] == 'Transaction Successful' || $topup['transaction_status'] == 'Transaction successful') {
                $successTransaction = true;
                break;
            }
        }
        return $successTransaction;
    }

    /**
     * Engage Product
     * @param sfWebRequest $request
     */
    public function executeEngageProduct(sfWebRequest $request) {
        // Blank
    }

    /**
     * Send html form to configure the script for BWM
     *
     * @author Vitaly
     *
     */
    public function executeBwmScriptConfigAjax(sfWebRequest $request) {

        // Check if the Ajax Call was Requested
        $this->forward404Unless($request->isXmlHttpRequest());

        // Use Symfony wrapper for session:
        $user = $this->getUser();
        $accountId = $user->getAttribute('account_id');

        $response = Hermes_Client_Rest::call('getPlusAccount', array(
            'account_id' => $accountId,
        ));

        $companyName = $response['account']['organisation'];
        if ($companyName == 'Unknown Organisation') {
            $companyName = '';
        }

        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        $html = $this->getPartial('configureBwmScript', array(
            'firstName' => $_SESSION['first_name'],
            'lastName' => $_SESSION['last_name'],
            'organisation' => $companyName
        ));

        $this->getResponse()->setHttpHeader('Content-type','application/json');
        echo json_encode(array('html' => $html));

        return sfView::NONE;

    }

    /**
     * Add dedicated numbers for BWM
     *
     * @author Vitaly
     *
     */
    public function executeBwmAddNumbersAjax(sfWebRequest $request) {

        // check if ajax
        $this->forward404Unless($request->isXmlHttpRequest());
        $errorMessages = array();

        if ($request->getParameter('script_accept') !== '1') {
            $errorMessages[] = array(
                'message' => 'FORM_VALIDATION_T_AND_C_EMPTY',
                'field_name'  => 'script_accept'
            );
        } else if(trim($request->getParameter('company')) === '') {
            $errorMessages[] = array(
                'message' => 'FORM_VALIDATION_NO_COMPANY_NAME',
                'field_name'  => 'company'
            );
        } elseif (trim($request->getParameter('contact_name')) === '') {
            $errorMessages[] = array(
                'message' => 'FORM_VALIDATION_NO_NAME',
                'field_name'  => 'contact_name'
            );
        } elseif (trim($request->getParameter('contact_number')) === '' || !is_numeric($request->getParameter('contact_number'))) {
            $errorMessages[] = array(
                'message' => 'FORM_VALIDATION_INVALID_PHONE_NUMBER',
                'field_name'  => 'contact_number'
            );
        }
        if (!$errorMessages) {
            $user = $this->getUser();

            $requestParams = $request->getParameterHolder()->getAll();
            $requestParams['welcome_message'] = $this->getPartial('bwmScriptWelcomeMessage', array(
                'company' => trim($request->getParameter('company')),
                'contact_name' => trim($request->getParameter('contact_name')),
                'contact_number' => trim($request->getParameter('contact_number')),
            ));
            $requestParams['instructions_message'] = $this->getPartial('bwmScriptInstructionsMessage', array(
                'company' => trim($request->getParameter('company')),
                'contact_name' => trim($request->getParameter('contact_name')),
                'contact_number' => trim($request->getParameter('contact_number')),
            ));
            $requestParams['transcript'] = $requestParams['welcome_message'] . ' ' . $requestParams['instructions_message'];
            $user->setAttribute('BWM_attrs', $requestParams);
            $user->setAttribute('BWM_status', false);
            $bwmProductRates = Hermes_Client_Rest::call('BWM.getProductDialInNumbersWithRates', array(
                'dedicated' => 1,
                'allocated' => 0,
                'service_ref' => 850,
                'product_type' => 'VOICE',
                'grouped' => 1
            ));
            // for proper JSON for dataTable
            $bwmNumbersJson = array();
            foreach ($bwmProductRates['products'] as $prd) {
                $bwmNumbersJson[] = array(
                    $prd['geo_city_id'],
                    $prd['country'],
                    $prd['city'],
                    $this->switchDnisTypesForDisplay($prd['dnis_type']),
                    ($prd['rate'] * 100) . ' pence',
                    $prd['country_code'],
                );
            }
            $connectionFee = Hermes_Client_Rest::call('BWM.getAccountConnectionFee',array());
            $dnisPerRate = Hermes_Client_Rest::call('BWM.getRatePerDnis',array());

            $bwmCharges = array('connectionFee' => $connectionFee['connection_fee'],'dnisPerRate' => $dnisPerRate['rate_per_dnis']);
            $html = $this->getPartial('bwmAddNumbers');
            $this->getResponse()->setHttpHeader('Content-type','application/json');
            echo json_encode(array('html' => $html, 'bwmNumbersJson' => $bwmNumbersJson, 'bwmCharges' => $bwmCharges ));

        } else {
            $this->getResponse()->setStatusCode(500);
            $this->getResponse()->setHttpHeader('Content-type','application/json');
            echo json_encode(array('error_messages' => $errorMessages));
        }

        return sfView::NONE;
    }

    /**
     * 2nd Part of customisation of BWM message ajax request. This method
     * simply stores the numbers users have selected into session.
     *
     * @param sfWebRequest $request
     * @return string|null If validation fails json encoded string or success null
     * @author Dav
     */
    public function executeBwmSelectedNumbersAjax(sfWebRequest $request) {

        // check if ajax
        $this->forward404Unless($request->isXmlHttpRequest());

        $errorMessages = array();
        $requestParam = $request->getParameter('json');
        $json = json_decode($requestParam);

        // Prepare the JSON response.
        $jsonResponse = '';

        if (is_array($json) && count($json)) {
            //success
            $dialingNo = array();
            $groupedDialingNo = array();
            foreach($json as $dNo) {
                $dnisType = $dNo[3];
                $dnisData = array(
                    'id' => $dNo[0],
                    'country' => $dNo[1],
                    'city' => $dNo[2],
                    'type' => $dnisType,
                    'rate' => $this->revertRateToNumeric($dNo[4]),
                    'country_code' => $dNo[5],
                );

                if (!isset($groupedDialingNo[$dnisType])) {
                    $groupedDialingNo[$dnisType] = array();
                }
                $groupedDialingNo[$dnisType][] = $dnisData;
                $dialingNo[] = $dnisData;
            }
            $connectionFee = Hermes_Client_Rest::call('BWM.getAccountConnectionFee',array());
            $dnisPerRate = Hermes_Client_Rest::call('BWM.getRatePerDnis',array());
            $qty = count($dialingNo);
            $total = $qty * $dnisPerRate['rate_per_dnis'];

            $dialingNo['BWM_total_cost'] = array(
                'total' => $total,
                'qty' => $qty,
                'connectionFee' => $connectionFee['connection_fee'],
                'dnisPerRate' => $dnisPerRate['rate_per_dnis']
            );
            $user = $this->getUser();
            $user->setAttribute('BWM_attrs_dialin_no', $dialingNo);
            // Pass all data related to BWM to partial for summmary page.
            $summary = array();
            $summary['BWM_attrs'] = $user->getAttribute('BWM_attrs');
            $summary['BWM_attrs_dialin_details'] = $dialingNo;
            $summary['BWM_product_info'] = $this->generateProductDesc($dialingNo);
            $html = $this->getPartial('bwmSummary', array(
                'BWM_summary' => $summary,
                'groupedDnis' => $groupedDialingNo,
                'welcomeMessage' => $summary['BWM_attrs']['welcome_message'],
                'instructionsMessage' => $summary['BWM_attrs']['instructions_message'],
            ));
            $jsonResponse = array('html' => $html);
        } else {
            $errorMessages[] = array(
                'message' => 'FORM_VALIDATION_ERRORS_FOUND',
                'field_name'  => 'alert'
            );
        }
        // Check for errors
        if (count($errorMessages) > 0) {
            // Change header response for errors
            $this->getResponse()->setStatusCode(500);
            $jsonResponse = array('error_messages' => $errorMessages);
        }
        $this->getResponse()->setHttpHeader('Content-type','application/json');
        echo json_encode($jsonResponse);
        return sfView::NONE;
    }

    /**
     * BwmSummaryAjax
     * @param sfWebRequest $request
     * @return string
     */
    public function executeBwmSummaryAjax(sfWebRequest $request)
    {
        // check if ajax
        $this->forward404Unless($request->isXmlHttpRequest());
        // lets do some validation
        $errorMessages = array();

        if ($request->getParameter('script_accept') !== '1') {

            $errorMessages[] = array(
                'message' => 'FORM_VALIDATION_T_AND_C_EMPTY',
                'field_name'  => 'script_accept'
            );

        } elseif (trim($request->getParameter('bwm_label')) === '') {

           $errorMessages[] = array(
               'message' => 'FORM_VALIDATION_FIELD_EMPTY',
               'field_name'  => 'bwm_label'
           );

        }

        // Check if there are any DNIS numbers selected.
        $user = $this->getUser();
        $dnises = $user->getAttribute('BWM_attrs_dialin_no');
        if (!count($dnises)) {
            // Assign the error to test
            $errorMessages[] = array(
                'message' => 'No dialing numbers were selected.',
            );
        }

        if ($request->getParameter('script_accept') === '1' && count($errorMessages) === (int)0) {
            $dnises = $user->getAttribute('BWM_attrs_dialin_no');
            $switchedDnises = $this->switchDNISTypesToHermesDNISType($dnises);
            // Re-add the total cost
            $switchedDnises['BWM_total_cost'] = $dnises['BWM_total_cost'];

            $bwmAttrs = $user->getAttribute('BWM_attrs');
            // Add our label and desc
            $bwmAttrs['bwm_label'] = $request->getParameter('bwm_label');
            // Push back to session
            $user->setAttribute('BWM_attrs', $bwmAttrs);
            // Create our product array
            $productAttr = array();
            $productAttr[] = $user->getAttribute('BWM_attrs');
            $productAttr[] = $switchedDnises;
            // user has completed the BWM process successfully
            $user->setAttribute('BWM_status', true);
            // init our basket
            $basket = new Basket($user);
            // We dont have a product group ID at this
            // insert -1 to indicate this.
            $productId = $basket->addNonExistingProduct($productAttr);


            $accountInfo = plusCommon::getAccountBalanceAndUsers(
                $user->getAttribute('account_id'),
                $user->getAttribute('contact_ref')
            );

            if (!$user->isPostPayCustomer() && $accountInfo['balance'] < 5 && !$basket->hasBundleBeenAdded()) {
                $nonBwmProducts = $basket->retrieveNonExistingProducts(true);
                // checks if credit already in basket
                $creditAlreadyInBasket = false;
                if (count($nonBwmProducts) > 0) {
                    foreach ($nonBwmProducts as $creditId => $product) {
                        if (isset($product['type']) && $product['type'] == 'credit') {
                            $creditAlreadyInBasket = $creditId;
                        }
                    }
                }
                // checks if it is at least 5 pounds, if not removes it
                if ($creditAlreadyInBasket !== false) {
                    if ($nonBwmProducts[$creditId]['total'] < 5) {
                        $basket->removeAddedProduct($creditId);
                        $creditAlreadyInBasket = false;
                    }
                }
                // adds 5 pounds to basket
                if ($creditAlreadyInBasket === false) {
                    $creditAttr = array();
                    $creditAttr['amount'] = 5;
                    $creditAttr['auto-recharge'] =  false;
                    $creditAttr['type'] = 'credit';
                    $creditAttr['qty'] = 1;
                    $creditAttr['total'] = 5;
                    $creditAttr['label'] = 'Purchase PAYG Credit';
                    $creditAttr['editable_amount'] = true;
                    $creditAttr['editable_recharge'] = true;
                    $creditAttr['removable'] = true;

                    $creditId = $basket->addNonExistingProduct($creditAttr);
                    $creditAttr['id'] = $creditId;
                }
            }
            $result = array(
                'product_id' => $productId,
            );

            if (count($creditAttr) > 0) {
                $result['credit'] = $creditAttr;
            }

            // Return the new ID.
            echo json_encode( $result );
        }
        // return any errors
        if (count($errorMessages)) {
            $this->getResponse()->setStatusCode(500);
            $jsonResponse = array('error_messages' => $errorMessages);
            $this->getResponse()->setHttpHeader('Content-type','application/json');
            echo json_encode($jsonResponse);
        }

        $ticks = $user->getAttribute('terms_and_conditions_ticked');
        if (!isset($ticks['product'])) {
            $ticks['product'] = array();
        }
        $ticks['product'][$productId] = $productId;
        $user->setAttribute('terms_and_conditions_ticked', $ticks);
        return sfView::NONE;
    }

    /**
     * Branded welcome message product description popup
     *
     * @author Vitaly
     *
     */
    public function executeBwmProduct(sfWebRequest $request) {

        $hermes_data = Hermes_Client_Rest::call('BWM.getProductDialInNumbersWithRates', array(
             'dedicated' => 1,
             'allocated' => 0,
             'service_ref' => 850,
             'product_type' => 'VOICE',
             'grouped' => 1
        ));

        $bwmNumbers = array();

        foreach($hermes_data['products'] as $product) {

            $bwmNumbers[] = array(
                $product['country'],
                $product['city'],
                ($product['dnis_type'] === 'Geographic') ? 'Landline' : $product['dnis_type'],
                $product['rate']*100 .' pence'
            );
        }

        $this->products = $bwmNumbers;

        $this->productID = 'BWM';
    }

    public function executeIndexSubmit() {
        $this->redirect('basket');
    }

    /**
     * Handles the form submission for the select products form.
     *
     * Assert that the form contains changes or new products.
     * If there are changes and the admin has users below them, redirect to the assign-users page.
     * If there are changes and the admin has no users below them, redirect to the payment page.
     * If there are no changes, redirects to the select-products page and shows an error message saying there are no
     * changes.
     *
     * @param sfWebRequest $request
     */
    public function executeBasketSubmit(sfWebRequest $request) {
        $user = $this->getUser();

        // Assert there are added products.
        // If not, redirect to the select products page.
        $basket = new Basket($user);
        if ($basket->isEmpty()) {
            $this->redirect('products_select');
        }

        // Check if the Basket has Invalid Bundles
        if ($basket->hasInvalidBundles()) {
            $this->redirect('basket');
        }

        // Remove any Basket Page Session Variables
        $user->getAttributeHolder()->remove('basket_page');

        // Generate the url for redirection, so as to pass the affiliate code.
        $routing = $this->getContext()->getRouting();
        $routeParameters = array(
            'affiliate-code' => $request->getParameter('affiliate-code'),
        );

        $this->redirect($routing->generate('payment_method', $routeParameters));
    }

    /**
     * Handles adding products to the basket via AJAX.
     * Redirects to the basket page on success.
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeAddCreditToBasketAjax(sfWebRequest $request) {
        // Assert that the request contains the add-product parameter.
        // Assert that the added product exists in the product list.

        $topup = $request->getParameter('topup', array());
        $response = array();
        $errorMessages = array();
        if (isset($topup['topup-amount'],$topup['topup-amount-input'])) {
            $user = $this->getUser();
            $basket = new Basket($user);

            if ($basket->hasBundleBeenAdded()) {
                $response['incompatible'] = 'Bundles are not compatible with this product.';
            } else {
                $nonBwmProducts = $basket->retrieveNonExistingProducts(true);
                $bwmProducts = $basket->retrieveNonExistingProducts(false);
                $creditAlreadyInBasket = false;
                if (count($nonBwmProducts) > 0) {
                    foreach ($nonBwmProducts as $product) {
                        if (isset($product['type']) && $product['type'] == 'credit') $creditAlreadyInBasket = true;
                    }
                }
                if ($creditAlreadyInBasket == false) {
                    $productAttr = array();
                    if ($topup['topup-amount']=='input') {
                        $productAttr['amount'] = $topup['topup-amount-input'];
                    } else {
                        $productAttr['amount'] = $topup['topup-amount'];
                    }

                    if (count($bwmProducts) > 0) {
                        $validCreditAmount = $basket->validateBWMCreditAmount($productAttr['amount']) && $this->decimalNumbers($productAttr['amount']) <= 2;
                    } else {
                        $validCreditAmount = $basket->validateCreditAmount($productAttr['amount']) && $this->decimalNumbers($productAttr['amount']) <= 2;
                    }

                    if ($validCreditAmount) {
                        $productAttr['auto-recharge'] = $topup['auto-recharge'] == 'on' ? true : false;
                        $productAttr['type'] = 'credit';
                        $productAttr['qty'] = 1;
                        $productAttr['total'] = $productAttr['amount'];
                        $productAttr['label'] = 'Purchase PAYG Credit';
                        $productAttr['editable_amount'] = false;
                        $productAttr['editable_recharge'] = false;
                        $productAttr['removable'] = true;

                        $productId = $basket->addNonExistingProduct($productAttr);
                        $productAttr['id'] = $productId;
                        $response = array('data' => $productAttr);
                    } else {
                        if (count($bwmProducts) > 0) {
                            $errorMessages[] = array(
                                'message' => 'The minimum amount of call credit that can be purchased is &pound;5',
                                'field_name'  => 'topup[topup-amount-input]',
                            );
                        } else {
                            $errorMessages[] = array(
                                'message' => 'The minimum amount of call credit that can be purchased is &pound;20',
                                'field_name'  => 'topup[topup-amount-input]',
                            );
                        }
                        $this->getResponse()->setStatusCode(500);
                    }
                } else {
                    $this->getResponse()->setStatusCode(500);
                    $errorMessages[] = array(
                        'message' => 'Credit already in basket.',
                        'field_name'  => 'alert',
                    );
                }
            }
        } else {
            $this->getResponse()->setStatusCode(500);
            $errorMessages[] = array(
                'message' => 'Data invalid.',
                'field_name'  => 'alert',
            );
        }

        $this->getResponse()->setHttpHeader('Content-type','application/json');
        if (count($errorMessages) > 0) {
            $response['error_messages'] = $errorMessages;
        }
        echo json_encode($response);

        return sfView::NONE;
    }

    /**
     * Handles adding products to the basket via AJAX.
     * Redirects to the basket page on success.
     * Redirects to 404 if the product id does not exist.
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeAddToBasketAJAX(sfWebRequest $request) {
        // Assert that the request contains the add-product parameter.
        // Assert that the added product exists in the product list.
        $productId = $request->getParameter('add-product');
        $invalidProductId = $productId === null || !$this->canProductBeAssigned($productId);
        if ($invalidProductId) {
            $this->getResponse()->setStatusCode(400);
        }
        else {
            // Add the product id to the basket.
            // If the product has already been added, the basket will simply override the added key; thus keeping a list
            // of unique products to add.
            $basket = new Basket($this->getUser());
            $basket->addProduct($productId);
        }

        return sfView::NONE;
    }

    /**
     * Handles adding products to the basket.
     * Redirects to the basket page on success.
     * Redirects to 404 if the product id does not exist.
     *
     * @param sfWebRequest $request
     */
    public function executeAddToBasket(sfWebRequest $request) {
        // Assert that the request contains the add-product parameter.
        // Assert that the added product exists in the product list.
        $productId = $request->getParameter('add-product');
        $invalidProductId = $productId === null || !$this->canProductBeAssigned($productId);
        $this->forward404If($invalidProductId, 'The product to add to the basket does not exist.');

        // Add the product id to the basket.
        // If the product has already been added, the basket will simply override the added key; thus keeping a list
        // of unique products to add.
        $basket = new Basket($this->getUser());
        $basket->addProduct($productId);

        // Redirect the user to the basket page.
        $this->redirect('basket');
    }

    /**
     * Displays the basket.
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeBasket(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        // Check if you are not a Plus Admin
        if (!$user->hasCredential('PLUS_ADMIN')) {
            $this->forward('errorModule', 'unAuthenticatedPlus');
        }

        $this->getContext()->getConfiguration()->loadHelpers(array('Basket', 'BWM'));

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        // Set the Expires to don't clear
        $response->setHttpHeader('Expires', -1);

        $basket = new Basket($user);

        // Product Selector Tool Check - @todo Needs Refactoring
        // Check if the Request is a POST
        if ($request->isMethod('post')) {
            // Check if the Request was started from an AJAX POST
            if ($request->isXmlHttpRequest()) {

                // Check if the clearBasket Parameter was Posted
                if ($request->getPostParameter('clearbasket') == 'yes') {
                    $basket->clearBasket();
                    $basket = new Basket($user);
                }

                // Check if the notAndC Parameter was Posted
                if ($request->getPostParameter('notandc') == 'yes') {
                    $user->setAttribute('hideTAndC', 'yes');
                }

                // Check if the User is a POST Pay Customer
                if ($user->isPostPayCustomer()) {
                    $response->setStatusCode(400);
                    if ($request->getPostParameter('fromLogin')) {
                        $user->setAttribute(
                            'bundle_notification',
                            'You have already purchased a Bundle, if you wish to change your Bundle please contact Customer Services on 0203 398 0398.'
                        );
                    } else {
                        echo json_encode(array('modal' => 'accountHasBundle'));
                    }
                } elseif ($basket->hasBundleBeenAdded()) {
                    echo json_encode(array('modal' => 'bundleInTheBasket'));
                    $response->setStatusCode(400);
                } else {
                    $this->addBundleSkeletonFromSession($user, $basket);
                    $response->setStatusCode(200);
                }
                return sfView::NONE;
            } else if (!$user->isPostPayCustomer() && !$basket->hasBundleBeenAdded()) {
                $this->addBundleSkeletonFromSession($user, $basket);
            }
        } elseif ($user->getAttribute('bundle_notification')) {
            $this->setVar('bundleNotification', $user->getAttribute('bundle_notification'));
            $user->setAttribute('bundle_notification', false);
        }

        // Check if the Basket is Empty
        if ($basket->isEmpty()) {
            $response->addJavascript('/sfjs/basket/empty-basket', 'last');
            $this->setTemplate('emptyBasket');
            return sfView::SUCCESS;
        }

        // Pass the added products to the view. @todo: refactor to pass an array of product IDs.
        $basketDisplay = new BasketDisplay();
        $this->setVar(
            'addedProducts',
            $basketDisplay->prepareProductsForBasketDisplay(array_reverse($basket->retrieveAddedProducts(), true)),
            true
        );

        // Check if the notAndC Parameter was Saved in Session
        if ($user->getAttribute('hideTAndC') === 'yes') {
            $user->getAttributeHolder()->remove('hideTAndC');
            $this->setVar('hideTAndC', true);
            $ticks = $user->getAttribute('terms_and_conditions_ticked');
            $ticks['bundle'] = $user->getAttribute('ProductSelector_bundle_id');
            $user->setAttribute('terms_and_conditions_ticked', $ticks);
        }

        // Check if the Page is being Reloaded from a Add-on Purchase
        if (true == $user->getAttribute('basket_page')) {
            $this->setVar('showTAndCReload', true);
        }

        // Obtain all Products / Bundles, which have already been Ticked
        $this->setVar('termsTicked', $user->getAttribute('terms_and_conditions_ticked'));

        return sfView::SUCCESS;
    }

    /**
     * Retrieves the selected bundle id from the session, and adds the related bundle skeleton from it.
     *
     * @param myUser $user
     * @param Basket $basket
     * @author Maarten Jacobs
     */
    private function addBundleSkeletonFromSession(myUser $user, Basket $basket)
    {
        // Add Bundle Skeleton
        $bundleId = $_SESSION['ProductSelector_bundle_id'];

        $bundleProducts = plusCommon::retrieveBundleProducts($user->getCulture());
        if ($bundleId && isset($bundleProducts[$bundleId])) {
            $this->addBundleSkeletonToBasket($bundleId, $basket, $user, $bundleProducts);
        }
    }

    /**
     * Generates a Bundle skeleton for the requested Bundle, and adds it to the Basket.
     *
     * @param int $bundleId
     * @param Basket $basket
     * @param myUser $user
     * @param array $bundleProducts
     */
    private function addBundleSkeletonToBasket($bundleId, Basket $basket, myUser $user, array $bundleProducts)
    {
        $bundleProduct = $bundleProducts[$bundleId];
        $bundle = new Bundle($bundleProduct);
        if ($bundle->isIndividualBundle()) {
            $pinMap = plusCommon::retrieveContactPinMap($user->getContactRef());
            $bundleProduct['pins'] = reset($pinMap);
        }
        unset($bundleProduct['addons']);
        $basket->addBundleSkeleton($bundleId, $bundleProduct);
    }

    /**
     * Clears the basket if any.
     *
     * Redirects back to the Select-Products page after clearing the basket.
     *
     * @param sfWebRequest $request
     */
    public function executeClearBasket(sfWebRequest $request) {
        $basket = new Basket($this->getUser());
        $basket->clearBasket();
        $this->redirect('products_select');
    }

    /**
     * Outputs the markup for the add-to-basket form.
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeRetrieveAddProductToBasketForm(sfWebRequest $request) {
        $productId = (int) $request->getParameter('productId');
        $canBeAssigned = $productId > 0 && $this->canProductBeAssigned($productId);

        // Assert that this is an AJAX request, and we have all required parameters.
        // If it isn't, do nothing.
        if ($request->isXmlHttpRequest() && $canBeAssigned) {
            $this->getContext()->getConfiguration()->loadHelpers('Basket');
            $partialVariables = array(
                'productGroupId' => $productId,
                'addLabel' => determineAddLabel($productId),
                'markedAsAdded' => hasProductBeenMarkedAsAdded($productId),
            );
            $this->renderPartial('addProductForm', $partialVariables);
        }

        return sfView::NONE;
    }

    /**
     * Handles the AJAX action to remove a product from the basket.
     *
     * Expects the POST value 'remove-product' with an associated integer (product id).
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeRemoveFromBasket(sfWebRequest $request) {
        $this->getResponse()->setHttpHeader('Content-type','application/json');

        // Check if valid Product id.
        $basket = new Basket($this->getUser());
        $productId = (int) $request->getParameter('remove-product');
        $validProductId = $basket->hasProductBeenMarkedForAddition($productId);

        // Stop this request from continuing in case the product id is invalid, or this is not an AJAX request.
        $this->forward404Unless($request->isXmlHttpRequest() && $validProductId);

        // Remove the product from the basket.
        if (!$basket->removeAddedProduct($productId)) {
            return sfView::ERROR;
        }

        // Check if there is a Call Credit product with less than 20 pounds of worth, AND no BWM left.
        if (!$basket->hasBWMBeenAdded() && $basket->hasCallCreditOfLessThanMinimumAmountBeenAdded()) {
            $removedIds = $basket->removeCallCreditsWorthLessThanMinimumAmount();
            echo json_encode(
                array(
                    'removed_ids' => $removedIds,
                    'message' => 'Your Call Credit has been removed, if you wish to purchase Call Credit visit the products page.',
                )
            );
        }

        $ticks = $this->getUser()->getAttribute('terms_and_conditions_ticked');
        if (isset($ticks['bundle']) && $productId == $ticks['bundle']) {
            $ticks['bundle'] = '';
        } elseif (isset($ticks['product']) && $productId == $ticks['product'][$productId]) {
            $ticks['product'][$productId] = '';
        }

        $this->getUser()->setAttribute('terms_and_conditions_ticked', $ticks);

        return sfView::NONE;
    }

    /**
     * Function executeChangeBWMNameAjax
     * @author Delio D'Anna<delio.danna@powwownow.com>
     *
     * Handles the AJAX actions to update product group's name.
     *It requires a group_id and a new name in order to work, these
     * will be passed in the $request parameter as fields:
     * either group_id or session_id
     * bwm_name
     *
     * bwm_name is the actual name to update to
     * group_id is the id of the product group as stored in the database
     * session_id is the index in the basket session list od added products
     *
     * If the product is a new one the function will update the label in
     * the basket's session using the session_id as reference,
     * if the product has already been stored into the database and assigned
     * to the user the function will use its group_id to  modify the
     * database entry
     *
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeChangeBWMNameAjax(sfWebRequest $request) {
        // check if ajax
        $this->forward404Unless($request->isXmlHttpRequest());
        $errorMessages = array();
        $name = trim($request->getParameter('bwm_name', ''));
        $grId = $request->getParameter('product_id',0);
        $sessId = $request->getParameter('session_id',0);
        if ($grId && $name) {
            try {
                $result = Hermes_Client_Rest::call('ProductGroup.updateGroupName', array(
                    'group_id' => $grId,
                    'group_name' => $name,
                ));
                $response = json_encode(array('gr_id' => $result['group_id'], 'name' => $result['group_name'] ));
            } catch (Exception $e) {
                $errorMessages[] = array(
                    'message' => 'An error occurred while saving the name, please try again',
                    'field_name'  => 'bwm_name'
                );
            }
        }
        elseif ($sessId < 0) {
            $basket = new Basket($this->getUser());
            $products = $basket->retrieveAddedProducts();
            if (isset($products[$sessId][0]['bwm_label'])) {
                $products[$sessId][0]['bwm_label'] = $name;
                $basket->storeAddedProducts($products);
                $response = json_encode(array('gr_id' => $sessId, 'name' => $name ));
            } else {
                $errorMessages[] = array(
                    'message' => 'Unexisting product',
                    'field_name'  => 'bwm_name'
                );
            }
        }
        elseif (!$name) {
            $errorMessages[] = array(
                'message' => 'The product name cannot be empty.',
                'field_name'  => 'bwm_name'
            );
        }
        else {
            $errorMessages[] = array(
                'message' => 'Unexisting product',
                'field_name'  => 'bwm_name'
            );
        }
        if (empty($errorMessages)) {
            echo $response;
        }
        else {
            $this->getResponse()->setStatusCode(500);
            $this->getResponse()->setHttpHeader('Content-type','application/json');
            echo json_encode(array('error_messages' => $errorMessages));
        }

        return sfView::NONE;
    }

    private function decimalNumbers($num)
    {
        return strlen(substr(strrchr($num, "."), 1));
    }

    public function executeUpdateCallCreditAjax(sfWebRequest $request)
    {
        $this->forward404Unless($request->isXmlHttpRequest());
        $this->getResponse()->setHttpHeader('Content-type','application/json');

        $basket = new Basket($this->getUser());
        $errorMessages = array();

        // Validate that the product id exists in the basket.
        $productId = $request->getPostParameter('product_id');
        $creditProduct = $basket->retrieveAddedProductById($productId);
        if ($creditProduct === null) {
            $errorMessages[] = array('message' => 'Product was not found.');
        } else {
            // Validate that the top-up amount is either not given, or given and a valid float.
            $topUpSelected = $request->getPostParameter('topup[topup-amount]', null);
            $topUpInput = $request->getPostParameter('topup[topup-amount-input]', null);
            if ($topUpSelected === null || $topUpInput === null) {
                $errorMessages[] = array(
                    'message' => 'Data invalid.',
                    'field_name'  => 'alert',
                );
            } elseif ($creditProduct['editable_amount'] && $topUpSelected !== null) {
                // Retrieve top-up amount.
                $topUpField = 'topup[topup-amount-input]';
                if ($topUpSelected !== 'input') {
                    $amount = $topUpSelected;
                } else {
                    $amount = $topUpInput;
                }

                // Validate top-up amount.
                $convertedAmount = (float) $amount;
                if (!is_numeric($amount) || $this->decimalNumbers($amount) > 2) {
                    $errorMessages[] = array(
                        'message' => 'Invalid Top-Up amount.',
                        'field_name'  => $topUpField,
                    );
                } elseif (!$basket->validateCreditAmount($convertedAmount, true)) {
                    $minimumAmount = ProductHelper::retrieveMinimumCallCreditAmount($basket);
                    $errorMessages[] = array(
                        'message' => "The minimum value for PAYG call credit when purchasing a Branded Welcome Message is £$minimumAmount.",
                        'field_name'  => $topUpField,
                    );
                } else {
                    $creditProduct['amount'] = (float) $convertedAmount;
                }
            }

            // Validate the the recharge property is given
            if ($creditProduct['editable_recharge']) {
                $recharge = $request->getPostParameter('topup[auto-recharge]');
                $creditProduct['auto-recharge'] = !empty($recharge);
            }
        }

        if (empty($errorMessages)) {
            $basket->addProduct($productId, $creditProduct);
            echo json_encode(array());
        } else {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(array('error_messages' => $errorMessages));
        }
        return sfView::NONE;
    }

    /**
     * Cached result from getProductGroups.
     *
     * @var array
     */
    protected $productGroups;

    /**
     * Retrieves the usage rates related to the requested product group ids.
     *
     * @param array $productIds
     * @return array
     */
    protected function retrieveUsageRatesForProducts(array $productIds) {
        $productRates = array();
        foreach ($productIds as $productGroupID) {
            try {
                $productRates[$productGroupID] = Hermes_Client_Rest::call('getPlusUsageRatesPerProductGroup', array(
                    'group_id'               => $productGroupID,
                    'aggregate_numbers'      => 1,
                    'dnis_type_translations' => array(
                        array(
                            'find'=>'Freefone',
                            'replace'=>'Freephone'
                        ),
                        array(
                            'find'=>'Geographic',
                            'replace'=>'Landline'
                        ),
                    )
                ));
            } catch (Exception $e) {
                $this->logMessage('Exception Occurred for api_getPlusUsageRatesPerProductGroup: ' . serialize($e),'err');
            }
        }
        return $productRates;
    }

    /**
     * Retrieves the DNISes of the Basket BWM.
     *
     * @param array $productData
     * @return array
     */
    protected function retrieveBasketBWMDnises(array $productData) {
        $dnisKeys = array_filter(array_keys($productData), 'is_int');
        return array_intersect_key($productData, array_flip($dnisKeys));
    }

    /**
     * Prepares a single Basket BWM for display.
     *
     * @param $productId
     * @param array $product
     * @return array
     */
    protected function prepareBasketBWMForDisplay($productId, array $product) {
        $product['product_group_id'] = $productId;
        $product['group_name'] = $product[0]['bwm_label'];
        $product['allocated'] = 1;
        $product['dedicated'] = 1;
        $product['service_ref'] = 850;
        $product['cost'] = $product[1]['BWM_total_cost']['total'] + $product[1]['BWM_total_cost']['connectionFee'];

        $dnises = $this->retrieveBasketBWMDnises($product[1]);
        foreach ($dnises as $dnisKey => $dnis) {
            $dnis['dnis_type'] = $dnis['type'];
            $dnises[$dnisKey] = $dnis;
        }
        $product['dnises'] = $dnises;

        return $product;
    }

    /**
     * Prepares basket BWMs for display.
     *
     * The returned BWMs must be run through processPotentialBWMs() to be used in the index template.
     *
     * @param array $products
     *   The newly added BWMs.
     * @return array
     *   The prepared BWMs.
     */
    protected function prepareBasketBWMsForDisplay(array $products) {
        foreach ($products as $productId => $product) {
            $products[$productId] = $this->prepareBasketBWMForDisplay($productId, $product);
        }
        return $products;
    }

    /**
     * Prepares the usage rates for the display of Basket BWMs.
     *
     * @param array $products
     *   The newly added BWMs.
     * @param array $usageRates
     * @return array
     *   The updated usage rates.
     */
    protected function prepareUsageRatesForBasketBWMs(array $products, array $usageRates) {
        foreach (array_keys($products) as $productId) {
            $usageRates[$productId] = array();
        }
        return $usageRates;
    }

    /**
     * Prepares configured BWMs in the basket for display on the select products page.
     *
     * @param array $bwms
     * @return array
     */
    protected function prepareBasketBWMs(array $bwms) {
        foreach ($bwms as $productKey => $product) {
            $bwms[$productKey] = $this->prepareBasketBWM($product);
        }
        return $bwms;
    }

    /**
     * prepareBasketBWM
     * @param array $bwm
     * @return array
     */
    protected function prepareBasketBWM(array $bwm) {
        $bwm = $this->fillBWMForDisplay($bwm['product_group_id'], $bwm[0], $bwm[1]);
        $bwm['account_connection_currency'] = '';
        $bwm['account_cyclic_payment_currency'] = '';
        $bwm['pin_cyclic_payment_currency'] = '';
        return $bwm;
    }

    /**
     * Builds the users to assigned products map for myPinProducts and similar pages.
     *
     * @todo refactor code.
     *
     * @param array $contactPinInfo
     *   Result from Default.getAccountPinsByContactRef
     * @param array $assignedProducts
     * @return array
     */
    protected function buildAssignedProductsList($contactPinInfo, $assignedProducts) {
        // Create Users Array and Product Status for the User
        $users = array();
        foreach ($contactPinInfo as $contactInfo) {
            $contactRef = $contactInfo['contact_ref'];

            if (!isset($users[$contactRef]) && 0 != $contactInfo['master_pin']) {
                $users[$contactRef] = array(
                    'contact_ref' => $contactRef,
                    'first_name'  => $contactInfo['first_name'],
                    'last_name'   => $contactInfo['last_name'],
                    'email'       => $contactInfo['email'],
                );
            }

//            if (0 != $contactInfo['master_pin']) {
//                $productGroupInfo = Hermes_Client_Rest::call('getPlusPinProductGroups', array(
//                    'pin_ref' => $contactInfo['pin_ref']
//                ));
//
//                foreach ($assignedProducts as $productInfo) {
//                    if (count($productGroupInfo)) {
//                        $status = NULL;
//                        foreach ($productGroupInfo as $productGroupInfo) {
//                            if ($productGroupInfo['product_group_id'] == $productInfo['product_group_id']) {
//                                $users[$contactRef]['pinInfo'][$productInfo['product_group_id']] = true;
//                                $status = true;
//                                // @todo: remove setting state
//                                $this->pinsSet = true;
//                                break;
//                            }
//                        }
//                        if (NULL===$status) {
//                            $users[$contactRef]['pinInfo'][$productInfo['product_group_id']] = false;
//                        }
//                    } else {
//                        $users[$contactRef]['pinInfo'][$productInfo['product_group_id']] = false;
//                    }
//                }
//            }
        }
        return $users;
    }

    /**
     * Retrieves the usage rates for a product group. On error, returns false.
     *
     * @param int $productGroupId
     * @return array|bool
     */
    protected function retrieveUsageRatesForProductGroup($productGroupId) {
        $result = false;
        try {
            $result = Hermes_Client_Rest::call('getPlusUsageRatesPerProductGroup', array(
                'group_id'=> $productGroupId,
                'aggregate_numbers' => 1,
                'dnis_type_translations' => array(
                    array(
                        'find' => 'Freefone',
                        'replace'=>'Freephone'
                    ),
                    array(
                        'find'=>'Geographic',
                        'replace'=>'Landline'
                    ),
                )
            ));
        } catch (Exception $e) {
            $this->logMessage('Exception Occured for api_getPlusUsageRatesPerProductGroup: ' . serialize($e),'err');
        }
        return $result;
    }

    /**
     * Create a displayable structure from a configured BWM structure.
     *
     * @param int $bwmId
     * @param array $bwm
     * @param array $dnises
     * @param bool $purchased
     * @return array
     */
    protected function fillBWMForDisplay($bwmId, array $bwm, array $dnises,$purchased = true) {
        // Use the BWM helper to generate the new description.
        $this->getContext()->getConfiguration()->loadHelpers('BWM');

        return array(
            'product_group_id' => $bwmId,
            'group_name' => $bwm['bwm_label'],
            'group_name_en' => $bwm['bwm_label'],
            'group_name_short' => $bwm['bwm_label'],
            'pin_connection_fee' => 0.0,
            'pin_connection_currency' => 0.0,
            'group_thumbnail' => '/sfimages/products/branded-welcome-message-logo'.((!$purchased) ? '-grey' : '').'.png',
            'group_summary' => formNewBWMDescription($dnises),
            'group_description' => 'Customise your conference calls by adding your own Branded Welcome Messages to the dedicated numbers of your choice.',
            'cost' => $dnises['BWM_total_cost']['total'] + $dnises['BWM_total_cost']['connectionFee'],
        );
    }

    /**
     * Builds the currently assigned products to pins array.
     *
     * @param $accountId
     * @param $contactRef
     * @return array
     *   Map of product id to pins.
     */
    protected function buildCurrentProductToPinsMap($accountId, $contactRef) {
        // Retrieve all pins related to the account
        $accountPins = Hermes_Client_Rest::call('getAccountPinsByContactRef', array(
            'contact_ref' => $contactRef,
            'active' => 'Y'
        ));

        // For every pin, retrieve the products assigned to it.
        $productToPins = array();
        foreach ($accountPins as $pin) {
            $pinRef = (int) $pin['pin_ref'];
            $assignedProducts = Hermes_Client_Rest::call('getPlusPinProductGroups',array(
                'pin_ref' => $pinRef,
                'locale'  => $this->getUser()->getCulture()
            ));

            foreach ($assignedProducts as $product) {
                $productId = (int) $product['product_group_id'];
            }
        }

        return $productToPins;
    }

    /**
     * Returns a Generic Description
     * @return string
     */
    protected function generateProductDesc() {
        return "THIS IS SOME GENERIC DESCRIPTION";
    }

    /**
     * Retrieves the cache of product groups.
     * Includes assigned and unassigned products for the account.
     *
     * @param bool $reset
     * @return array
     */
    protected function retrieveProductGroups($reset = false) {
        if (!is_array($this->productGroups) || $reset) {
            $this->productGroups = $this->getProductGroups();
        }
        return $this->productGroups;
    }

    /**
     * Assert that the product is available for the current user.
     *
     * @param int $checkProductId
     *   ID of the product group.
     * @return bool
     */
    protected function doesProductExistForUser($checkProductId) {
        $accountProducts = $this->retrieveProductGroups();
        $checkProductId = (int) $checkProductId;
        foreach ($accountProducts['product_list'] as $product) {
            $productGroupId = (int) $product['product_group_id'];
            if ($productGroupId === $checkProductId) {
                return true;
            }
        }
        return false;
    }

    /**
     * Assert that the product id exists and is unassigned to the account.
     *
     * @param int $checkProductId
     * @return bool
     */
    protected function canProductBeAssigned($checkProductId) {
        $checkProductId = (int) $checkProductId;

        // Assert that the product exists.
        if (!$this->doesProductExistForUser($checkProductId)) {
            return false;
        }

        // Assert that the product does not exist in the assigned products list.
        $accountProducts = $this->retrieveProductGroups();
        foreach ($accountProducts['assigned_products'] as $product) {
            $productGroupId = (int) $product['product_group_id'];
            if ($productGroupId === $checkProductId) {
                return false;
            }
        }
        return true;
    }

    /**
     * Assert that the product id exists and is assigned to the account.
     *
     * @param int $checkProductId
     * @return bool
     */
    protected function canProductBeUnassigned($checkProductId) {
        $checkProductId = (int) $checkProductId;

        // Assert that the product exists.
        if (!$this->doesProductExistForUser($checkProductId)) {
            return false;
        }

        // Assert that the product does not exist in the assigned products list.
        $accountProducts = $this->retrieveProductGroups();
        foreach ($accountProducts['assigned_products'] as $product) {
            $productGroupId = (int) $product['product_group_id'];
            if ($productGroupId === $checkProductId) {
                // BWM products, for the time being, cannot be unassigned from an account.
                if (plusCommon::isBWMProduct($product)) {
                    return false;
                }

                // Non-BWM products can be unassigned.
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves the product group data related to the product group id.
     *
     * @param int $productGroupId
     * @return array|bool
     *   Returns false if the product is not found.
     *   Returns the product data if found.
     */
    protected function retrieveProductData($productGroupId) {
        $productGroupId = (int) $productGroupId;
        $accountProducts = $this->retrieveProductGroups();
        foreach ($accountProducts['product_list'] as $product) {
            $productId = (int) $product['product_group_id'];
            if ($productId === $productGroupId) {
                return $product;
            }
        }
        return false;
    }

    /**
     * Returns a map of product group ids to product data.
     *
     * @param array $productGroupIds
     * @return array
     */
    protected function retrieveProductsData(array $productGroupIds) {
        $map = array_fill_keys($productGroupIds, 0);
        $mapCount = count($map);
        $accountProducts = $this->retrieveProductGroups();
        foreach ($accountProducts['product_list'] as $product) {
            $productId = (int) $product['product_group_id'];
            if (isset($map[$productId]) && !is_array($map[$productId])) {
                $mapCount--;
                $map[$productId] = $product;

                if (!$mapCount) {
                    break;
                }
            }
        }
        return $map;
    }

    /**
     * Retrieves the selected products from the request parameters.
     *
     * @param array $requestParameters
     * @return array
     */
    protected function retrieveSelectedProducts(array $requestParameters) {
        $assignedAfter = array();
        foreach ($requestParameters as $key => $value) {
            if (substr($key, 0, 5) =='prod-') {
                $assignedAfter[$value] = $value;
            }
        }
        return $assignedAfter;
    }

    /**
     * retrieveAssignedProducts
     * @param $accountId
     * @return array
     */
    protected function retrieveAssignedProducts($accountId) {
        $assignedArguments = array(
            'account_id' => $accountId,
        );
        $assignedResponse = Hermes_Client_Rest::call('getPlusAccountProductGroups', $assignedArguments);

        // Give each row the product group id key, for easy comparison.
        $assignedProducts = array();
        foreach ($assignedResponse as $assignedProduct) {
            $assignedProducts[$assignedProduct['product_group_id']] = $assignedProduct;
        }

        return $assignedProducts;
    }

    /**
     * Assert that the product (group) id is valid.
     *
     * A valid product (group) id is either numeric (existing in the database), or is in the basket.
     *
     * @param string $key
     * @param array $basket
     * @return bool
     */
    protected function isValidProductKey($key, array $basket) {
        $valid = true;
        if (strpos($key, 'new-bwm-') === 0 && empty($basket[$key])) {
            $valid = false;
        }
        else if (strpos($key, 'new-bwm-') === false && !is_numeric($key)) {
            $valid = false;
        }
        return $valid;
    }

    /**
     * Redirects the user to the Select-Products page with a flash message indicating that the basket has no changes.
     */
    protected function redirectForNoChangesToBasket() {
        $this->getUser()->setFlash('empty-basket', 'No products were marked for activation or deactivation.');
        $this->redirect('products_select');
    }

    /**
     * Redirects the user to the Select-Products page with a flash message indicating that the basket contains invalid
     * product group ids.
     */
    protected function redirectForInvalidProductGroupKey() {
        $this->getUser()->setFlash('invalid-product-key', 'Invalid products were selected.');
        $this->redirect('products_select');
    }

    /**
     * Adds the contact_refs of a single admin to every added product of the basket,
     * and stores the added products in the basket again.
     *
     * @param int $contactRef
     * @param Basket $basket
     * @return bool
     */
    protected function fillAddedProductsForSingleAdmin($contactRef, Basket $basket) {
        // Retrieve the contact refs.
        try {
            $contactPinInfo = Hermes_Client_Rest::call('getAccountPinsByContactRef', array(
                'contact_ref' => $contactRef,
                'active'      => 'Y'
            ));
            $contactRefs = array();
            foreach ($contactPinInfo as $contactInfo) {
                $masterPin = (int) $contactInfo['master_pin'];
                if ($masterPin > 0) {
                    $contactRefs[] = (int) $contactInfo['contact_ref'];
                }
            }
        }
        catch(Exception $e) {
            $this->logMessage('Exception for Get Contact PINS Triggered: ' . serialize($e),'err');
            return false;
        }

        // Assign all contact refs to the all of the added products.
        $addedProducts = $basket->retrieveAddedProducts();
        foreach ($addedProducts as &$product) {
            $product['pins'] = $contactRefs;
        }
        unset($product);

        // Store the products in the basket once again.
        $basket->storeAddedProducts($addedProducts);

        return true;
    }

    /**
     * Switches a displayable DNIS type to a Hermes understandable DNIS type.
     * If the type is unknown, returns false.
     *
     * @param string $dnisType
     * @return bool|string
     */
    protected function switchDNISTypeToHermesDNISType($dnisType) {
        if ($dnisType === 'Landline') {
            return 'Geographic';
        } else if ($dnisType === 'Freephone') {
            return 'Freefone';
        } else if ($dnisType === 'Shared Cost') {
            return $dnisType;
        }
        return false;
    }

    /**
     * Iterates through selected DNISes and switches displayable types to Hermes understandable DNIS type.
     *
     * Warning: if no DNIS type is found, the dnis is not passed to the result array.
     *
     * @param array $dnises
     * @return array
     */
    protected function switchDNISTypesToHermesDNISType(array $dnises) {
        $result = array();

        foreach ($dnises as $dnis) {
            if (!empty($dnis['type'])) {
                $dnisType = $this->switchDNISTypeToHermesDNISType($dnis['type']);
                if ($dnisType) {
                    $dnis['type'] = $dnisType;
                    $result[] = $dnis;
                }
            }
        }

        return $result;
    }

    /**
     * Reverts the rate used in the second step of the BWM flow to a numeric rate.
     * i.e. '88 pence' to 0.88
     *
     * @param string $rate
     * @return float
     */
    protected function revertRateToNumeric($rate) {
        if (!is_string($rate) || strlen($rate) < 7) {
            return 0.0;
        }

        // Remove the ' pence' suffix, and convert to float.
        $rate = (float) substr($rate, 0, strlen($rate) - 6);
        $rate /= 100.0;

        return $rate;
    }

    /**
     * Changes the DNIS types to a displayable string.
     * i.e. 'Freefone' => 'Freephone'
     *
     * @param string $dnisType
     * @return string
     */
    protected function switchDnisTypesForDisplay($dnisType) {
        if ($dnisType === 'Geographic') {
            $dnisType = 'Landline';
        } else if ($dnisType === 'Freefone') {
            $dnisType = 'Freephone';
        }
        return $dnisType;
    }

    /**
     * Get Product Groups
     * @return array
     */
    protected function getProductGroups()
    {
        // Get All Product Groups
        try {
            $listProductGroups = Hermes_Client_Rest::call(
                'getAllPlusProductGroups',
                array(
                    'locale'     => $this->getUser()->getCulture(),
                    'account_id' => $this->getUser()->getAttribute('account_id')
                )
            );
        } catch (Exception $e) {
            $this->logMessage('Exception Occurred for api_getPlusAccountProductGroups: ' . print_r($e->getMessage()), 'err');
            $errorMessages[]   = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $listProductGroups = array();
        }
        return $listProductGroups;

    }

    /**
     * Get Product Groups
     * @return array
     */
    protected function getProductGroupsDedicatedNumbers(){
        // Get All Product Groups
        try {
            $listProductGroupsDedicatedNumbers = Hermes_Client_Rest::call('BWM.getBwmDedicatedNumbersForAccount', array(
                'account_id' => $this->getUser()->getAttribute('account_id'),
                'show_test_accounts' => 1
            ));
        } catch (Exception $e) {
            $this->logMessage('Exception Occured for BWM.getAllPlusProductGroups: ' . serialize($e),'err');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $listProductGroupsDedicatedNumbers = array();

        }

        $data = array();

        foreach ($listProductGroupsDedicatedNumbers as $numbers) {

            if (!isset($data[$numbers['product_group_id']])) {
                $data[$numbers['product_group_id']] = array(
                  'Freefone' => array(),
                  'Geographic' => array()
                );
            }
            $data[$numbers['product_group_id']][$numbers['dnis_type']][] = str_replace(' ','&nbsp;',htmlentities($numbers['national_formatted']));
        }

        foreach ($data as &$number) {
            $number['Freefone'] = implode(', ',$number['Freefone']);
            $number['Geographic'] = implode(', ',$number['Geographic']);
        }

        return $data;
    }

    /**
     * AJAX action to add a bundle to the basket.
     *
     * @param sfWebRequest $request
     * @return string
     *
     * @author Maarten Jacobs
     */
    public function executeAddBundleToBasketAjax(sfWebRequest $request)
    {
        $this->forward404Unless($request->isXmlHttpRequest());

        $user = $this->getUser();
        $response = new AJAXResponse($this->getResponse());
        $bundleId = $request->getPostParameter('add-bundle');
        $addOnId = $request->getPostParameter('upgrade-bundle');
        $selectedChairmanPin = $request->getPostParameter('user');
        $bundles = plusCommon::retrieveBundleProducts($user->getCulture());
        $basket = new Basket($user);

        if ($bundleId === null || !isset($bundles[$bundleId])) {
            $response->addAlertErrorMessage('Selected bundle was not found.');
        } elseif ($basket->hasBundleBeenAdded()) {
            $response->addAlertErrorMessage('A bundle has already been added to the basket.');
        } elseif ($request->getPostParameter('bundle-tc') !== '1') {
            $response->addErrorMessage(array('field_name' => 'bundle-tc', 'message' => 'Please mark the checkbox if you agree.'));
        } elseif ($addOnId !== null && !$this->isValidUpgradeBundleId($addOnId, $bundles)) {
            $response->addAlertErrorMessage('Selected bundle add-on is not available.');
        } else {
            $bundle = $bundles[$bundleId];
            if ($bundle['is_individual_bundle']) {
                $contactPinMap = plusCommon::retrieveContactPinMap($user->getContactRef());
                if ($selectedChairmanPin === null) {
                    $response->addAlertErrorMessage('Please select a PIN pair.')
                        ->close();
                } elseif (!isset($contactPinMap[$selectedChairmanPin])) {
                    $response->addAlertErrorMessage('Invalid PIN pair selected.')
                        ->addContent('reload_form', true)
                        ->close();
                } else {
                    $bundle['pins'] = $contactPinMap[$selectedChairmanPin];
                }
            }
            if (!$response->isClosed()) {
                if ($addOnId === null) {
                    $bundle['addons'] = false;
                }
                $bundleObject = new Bundle($bundle);
                $basket->addBundle($bundleId, $bundle);
                $response->addContent('success', true)
                    ->addContent('id', $bundleId)
                    ->addContent('cost', $bundleObject->calculateProRataCost())
                    ->addContent('monthly_cost', $bundleObject->getMonthlyPrice())
                    ->addContent('label', $bundleObject->getGroupName());
                if ($basket->hasCallCreditBeenAdded()) {
                    $response->addContent('removed_products', $basket->removeCallCreditProducts());
                }

                $ticks = $user->getAttribute('terms_and_conditions_ticked');
                $ticks['bundle'] = $bundleId;
                $user->setAttribute('terms_and_conditions_ticked', $ticks);
            }
        }

        return $response->outputResponse();
    }

    private function isValidUpgradeBundleId($upgradeBundleId, array $bundles)
    {
        $upgradeBundleId = (int) $upgradeBundleId;
        foreach ($bundles as $bundle) {
            if (!empty($bundle['addons'])) {
                foreach ($bundle['addons'] as $addon) {
                    if ((int) $addon['product_group_id'] === $upgradeBundleId) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private function groupBundlesByBasicWithUpgrade($bundles)
    {
        foreach ($bundles as &$bundle) {
            if (!empty($bundle['addons'])) {
                $bundle['upgrade_bundle'] = array_shift($bundle['addons']);
            }
        }
        return $bundles;
    }

    /**
     * Forward function to force typehinting.
     *
     * @return myUser
     */
    public function getUser()
    {
        return parent::getUser();
    }

    /**
     * Filters out the individual bundle from the Assign-Products page form.
     *
     * @param array $requestParams
     * @param array $individualBundleIds
     * @param array $accountPinRefs
     * @return array
     */
    private function filterOutIndividualBundle(array $requestParams, array $individualBundleIds, array $accountPinRefs)
    {
        $bundleIdCounts = array_fill_keys(array_keys($individualBundleIds), 0);
        foreach ($requestParams as $productKey => $product) {
            $key = substr($productKey, 1);
            if (isset($bundleIdCounts[$key])) {
                $bundleIdCounts[$key] = count($product);
            }
        }
        $bundleIdCounts = array_filter($bundleIdCounts);
        foreach ($bundleIdCounts as $bundleId => $count) {
            if ($count > 1 || plusCommon::isProductAssignedToAnyPin($accountPinRefs, $bundleId)) {
                unset($requestParams['p' . $bundleId]);
            }
        }
        return $requestParams;
    }

    /**
     * This is a dummy Terms-And-Conditions page, to be filled in for a later ticket.
     *
     * @author Maarten Jacobs
     */
    public function executeTermsAndConditions()
    {
        $this->getResponse()->setTitle('Terms and Conditions');
    }

    /**
     * Handles the AJAX POST request to upgrade the bundle in the basket.
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeUpgradeBundle(sfWebRequest $request)
    {
        $this->forward404Unless($request->isXmlHttpRequest());

        $response = new AJAXResponse($this->getResponse());
        $user = $this->getUser();
        $basket = new Basket($user);
        $bundleProducts = plusCommon::retrieveBundleProducts($user->getCulture());

        if (!$basket->hasBundleBeenAdded()) {
            $response->addErrorMessage('The basket contains no bundle.');
        } else {
            $addedBundle = $basket->retrieveAddedBundleObject();
            $bundleUpgrade = $this->retrievePossibleBundleUpgrade($bundleProducts, $addedBundle->getProductGroupId());
            if (!$bundleUpgrade) {
                $response->addErrorMessage('The bundle in the basket is not liable to an upgrade.');
            } else {
                $basket->upgradeBundle($addedBundle->getProductGroupId(), $bundleUpgrade);
                $response->addContent('success', true);

                $ticks = $user->getAttribute('terms_and_conditions_ticked');
                $ticks['bundle'] = '';
                $user->setAttribute('terms_and_conditions_ticked', $ticks);
            }
        }

        return $response->outputResponse();
    }

    /**
     * Handles the AJAX POST request to agree to the Terms and Conditions for the bundle in the basket.
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeAgreeToBundleTermsAndConditions(sfWebRequest $request)
    {
        $this->forward404Unless($request->isXmlHttpRequest());

        $response = new AJAXResponse($this->getResponse());
        $user = $this->getUser();
        $basket = new Basket($user);

        if (!$basket->hasBundleBeenAdded()) {
            $response->addErrorMessage('The basket contains no bundle.');
        } else {
            $addedBundle = $basket->retrieveAddedBundleObject();
            if ($addedBundle->hasAgreedToTAndC()) {
                $response->addContent('Already agreed to terms and conditions.', true);
            } else {
                $basket->agreeToBundleTAndC($addedBundle->getProductGroupId());
                $response->addContent('success', true);

                // Update the Product Selector Tracking Table
                $productSelectorId = $this->retrieveProductSelectorToolId();
                if ($productSelectorId) {
                    Common::updateProductSelector(array(
                        'id' => $productSelectorId,
                        'basket_page' => true,
                        'account_id'  => $this->getUser()->getAttribute('account_id'),
                        'contact_ref' => $this->getUser()->getAttribute('contact_ref')
                    ));

                    // Only Show the Basket Tracking, the first time you come here.
                    // On the Enhanced Dashboard Tracking, in theory the Basket Terms and Conditions Checkbox can be
                    // Clicked Twice. Once Automatically, Second after the Bundle Add-on has been added.
                    if (!$user->hasAttribute('basket_page')) {
                        $user->setAttribute('basket_page',true);
                        $response->addContent('basket_page',true);
                    }
                }
            }
            $ticks = $user->getAttribute('terms_and_conditions_ticked');
            $ticks['bundle'] = $addedBundle->getProductGroupId();
            $user->setAttribute('terms_and_conditions_ticked', $ticks);
        }

        return $response->outputResponse();
    }

    /**
     * Handles the AJAX POST request to select a PIN pair for the AYCM bundle.
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeSelectAYCMPinPair(sfWebRequest $request)
    {
        $this->forward404Unless($request->isXmlHttpRequest());

        $response = new AJAXResponse($this->getResponse());
        $user = $this->getUser();
        $basket = new Basket($user);
        $chairmanPin = $request->getPostParameter('user');

        if (!$basket->hasBundleBeenAdded()) {
            $response->addErrorMessage('The basket contains no bundle.');
        } elseif (!$chairmanPin) {
            $response->addErrorMessage('No Chairman pin was given.');
        } else {
            $addedBundle = $basket->retrieveAddedBundleObject();
            if (!$addedBundle->isIndividualBundle()) {
                $response->addErrorMessage('The bundle in the basket is not an AYCM bundle.');
            } else {
                $contactPinMap = plusCommon::retrieveContactPinMap($user->getContactRef());
                if (!isset($contactPinMap[$chairmanPin])) {
                    $response->addErrorMessage('The selected Chairman pin does not exist.');
                } else {
                    $basket->selectPinPairForAYCMBundle($addedBundle->getProductGroupId(), $contactPinMap[$chairmanPin]);
                    $response->addContent('success', true);
                }
            }
        }

        return $response->outputResponse();
    }

    /**
     * Retrieves the bundle upgrade for the given bundle.
     *
     * @param array $bundleProducts
     * @param int $bundleId
     * @return bool
     */
    private function retrievePossibleBundleUpgrade(array $bundleProducts, $bundleId)
    {
        if (!isset($bundleProducts[$bundleId]['addons'])) {
            return false;
        }
        return $bundleProducts[$bundleId]['addons'][0];
    }

    /**
     * Stores the relevant LandLine feature ids for the myPinProducts view.
     *
     * @param array $assignedProducts
     * @return array $releventBundleLandLineFeatures
     * @author Maarten Jacobs
     * @author Asfer Tamimi
     */
    private function getRelevantLandLineFeatures($assignedProducts)
    {
        $releventBundleLandLineFeatures = array();
        foreach ($assignedProducts as $product) {
            if (!empty($product['is_bundle']) && !$product['is_individual_bundle']) {
                $releventBundleLandLineFeatures[sfConfig::get('app_landline_feature_products_uk')] = true;
                if ($product['is_addon']) {
                    $releventBundleLandLineFeatures[sfConfig::get('app_landline_feature_products_worldwide')] = true;
                }
                break;
            }
        }
        return $releventBundleLandLineFeatures;
    }

    private function retrieveProductSelectorToolId()
    {
        return isset($_SESSION['ProductSelector_id']) ? $_SESSION['ProductSelector_id'] : null;
    }
}
