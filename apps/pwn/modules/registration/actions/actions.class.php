<?php

/**
 * registration actions.
 *
 * @package    powwownow
 * @subpackage registration
 * @author     Albert Kozlowski
 */
class registrationActions extends sfActions
{
    public function preExecute()
    {
        $logPath = sfConfig::get('sf_log_dir').'/registrations.log';
        $this->registrationLogger = new sfFileLogger(new sfEventDispatcher(), array('file' => $logPath));
        $this->registrationLogger->setLogLevel(7); // (Debug).
    }

    /**
     * Free pin registration
     *
     * @param sfWebRequest $request A request object
     * @return string
     */
    public function executeFreePinRegistration(sfWebRequest $request)
    {
        $this->log(" ");
        $this->log("Registration Attempt: ");

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            $this->log("not a Ajax request or not Posted - aborting");
            $this->forward404();
        }

        $this->setLayout(false);
        $this->getResponse()->setContentType('application/json');

        $this->log("POSTed vars: ". serialize($_POST));

        $registrationSource = $request->getPostParameter('registration_source', 'undefined');
        $registrationManager = new RegistrationManager($registrationSource);
        $registrationResult = $registrationManager->doRegistration($request->getPostParameters());

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        if (!isset($registrationResult['error_messages'])) {
            $this->log("Attempt seems to be successful!!");

            $return = array();
            $return['redirect_url'] = url_for($registrationResult['return_url'],true);
            $return['pin'] = $registrationResult['response']['pin']['pin'];
            $return['pinRefHash'] = $registrationResult['shomei_response']['pin_ref_hash'];
        } else {
            $this->log("Attempt seems to be not processed!!");
            $this->getResponse()->setStatusCode(400);

            $return = array();
            $return['error_messages'] = $registrationResult['error_messages'];
        }

        echo json_encode($return);
        return sfView::NONE;
    }

    /**
     * Log message
     *
     * @param string $msg
     */
    private function log($msg)
    {
        $this->registrationLogger->err($msg);
    }
}