<?php

/**
* test actions.
*
* @package    powwownow
* @subpackage test
* @author     Unknown
*/
class testActions extends sfActions {

    /**
     *
     */
    public function preExecute() {
        if (isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] == 'prod') {
            $this->forward404();
        }
    }

    /**
     * This Action is just auto login
     * @param sfWebRequest $request
     * @return string
     * @throws Exception
     * @author Asfer
     *
     */
    public function executeLogin(sfWebRequest $request)
    {
        if ($request->isMethod('post') && isset($_POST['user_email'])) {

            $user_email    = $request->getParameter('user_email', false);
            $user_password = $request->getParameter('user_password', false);

            if (!$user_password) {
                throw new Exception('no password in ' . __method__);
            }

            if (!$user_email) {
                throw new Exception('no email in ' . __method__);
            }

            try {
                if (Plus_Authenticate::logIn($user_email, $user_password) === true) {
                    $this->getResponse()->setStatusCode(200);
                } else {
                    $this->getResponse()->setStatusCode(500);
                }
            } catch (Exception $e) {
                $this->getResponse()->setStatusCode(500);
            }
        }
        return sfView::NONE;
    }

    /**
     * @param sfWebRequest $request
     * @return string
     */
    public function executeSetup(sfWebRequest $request) {
        $this->_login( $this->_register() );
        return sfView::NONE;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function _register(){
        $ts = time();
        $this->user_email .= $ts. $this->user_email_domain;
        $args=array(
            'email'         => $this->user_email ,
            'title'         => 'Mr',
            'first_name'    => 'functional',
            'last_name'     => 'tester',
            'phone'         => '0000000000',
            'mobile_phone'  => '0700000000',
            'organisation'  => 'testing inc.',
            'location'      => 'UK',
            'password'      => $this->password,
            'service_ref'    => '850',
            'source'        =>'functional Test',
            'locale'        =>'en_GB'
        );
        try{
            Hermes_Client_Rest::call( 'doPlusRegistrationRegister' , $args );
        }catch(Exception $e){
            throw $e;
        }
    
        echo 'created user '. $this->user_email . "\r\n";
        return array('email'=>$this->user_email, 'password' =>$this->password);
    }
}
