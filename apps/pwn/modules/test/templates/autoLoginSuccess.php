<?php include_component('commonComponents', 'header'); ?>
<div class="container_24">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => 'Auto Login', 'headingSize' => 'm')); ?>
    <div class="grid_12">
        <form method="post" id="frm" class="clearfix" action="<?php echo url_for('test/autoLogin');?>">
            <label for="user_type">User Type:</label>
            <select id="user_type" name="user_type">
                <option value="logout">Logout</option>
                <option value="POWWOWNOW" <?php echo (isset($_SESSION['service_user']) && $_SESSION['service_user']=='POWWOWNOW') ? 'selected="selected"' : ''; ?>>Powwownow</option>
                <option value="PLUS_USER" <?php echo (isset($_SESSION['service_user']) && $_SESSION['service_user']=='PLUS_USER') ? 'selected="selected"' : ''; ?>>Plus User</option>
                <option value="PLUS_USER2" <?php echo (isset($_SESSION['service_user']) && $_SESSION['service_user']=='PLUS_USER') ? 'selected="selected"' : ''; ?>>Plus User(2)</option>
                <option value="PLUS_ADMIN" <?php echo (isset($_SESSION['service_user']) && $_SESSION['service_user']=='PLUS_ADMIN' && $_SESSION['email'] == 'mark.wiseman+plus@powwownow.com') ? 'selected="selected"' : ''; ?>>Plus Admin</option>
                <option value="PLUS_ADMIN2" <?php echo (isset($_SESSION['service_user']) && $_SESSION['service_user']=='PLUS_ADMIN' && $_SESSION['email'] == 'asfer.tamimi+plusadmin@powwownow.com') ? 'selected="selected"' : ''; ?>>Plus Admin (2)</option>
                <option value="PREMIUM_USER" <?php echo (isset($_SESSION['service_user']) && $_SESSION['service_user']=='PREMIUM_USER') ? 'selected="selected"' : ''; ?>>Premium User</option>
                <option value="PREMIUM_ADMIN" <?php echo (isset($_SESSION['service_user']) && $_SESSION['service_user']=='PREMIUM_ADMIN') ? 'selected="selected"' : ''; ?>>Premium Admin</option>            
            </select>
            <input type="submit" value="Auto Login"/><br/><br/>
            <strong><?php echo "$loginStatus\n";?></strong><br/><br/>
            <a target="_blank" href="<?php echo url_for('login/login');?>">Go to Enhanced to Plus Upgrade page</a><br/><br/>
            
            <a target="_blank" href="<?php echo url_for('products/index');?>">Go to Products page</a><br/>
            <a target="_blank" href="<?php echo url_for('products/myPinProducts');?>">Go to My PIN Products page</a><br/><br/>

            <a target="_blank" href="<?php echo url_for('account/address');?>">Go to Account Address page</a><br/>
            <a target="_blank" href="<?php echo url_for('account/purchaseCredit');?>">Go to Purchase Credit page</a><br/>
            <a target="_blank" href="<?php echo url_for('account/autoTopUp');?>">Go to Auto TopUp page</a><br/><br/>
            
            <a target="_blank" href="<?php echo url_for('worldpay/testCallback');?>">Go to Test Worldpay Callback page</a><br/>
            <a target="_blank" href="<?php echo url_for('account/topupDetails') . '/transaction/3';?>">Go to Topup Details page (Post Worldpay)</a><br/>
            <a target="_blank" href="<?php echo url_for('account/topupDetails') . '/transaction/3/new';?>">Go to Topup Details page (Post Worldpay New User)</a><br/>
            <a target="_blank" href="<?php echo url_for('account/topupHistory');?>">Go to Topup History page</a><br/><br/>

            <a target="_blank" href="<?php echo url_for('account/accountStatement');?>">Go to Account Statement page</a><br/><br/>

            <a target="_blank" href="<?php echo url_for('test/showWorldpayVideo');?>">Go to Worldpay Video</a><br/>
        </form>
    </div>
    <div class="grid_12">
        <?php echo "<pre>"; echo "Session:\n"; var_dump($_SESSION); echo "</pre>"; ?>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
