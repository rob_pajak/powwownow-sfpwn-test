<?php sfContext::getInstance()->getLogger()->info('LayoutCheck::Init'); ?>
<?php include_component('commonComponents', 'header'); ?>
<?php sfContext::getInstance()->getLogger()->info('LayoutCheck::Header Loaded'); ?>
<div class="container_24">
    <div class="grid_24 alpha omega">
        <?php include_component('commonComponents', 'subPageHeader', array('title' => 'Default Layout')); ?>
        <?php sfContext::getInstance()->getLogger()->info('LayoutCheck::Sub Header Loaded'); ?>
    </div>
    <div class="grid_24 alpha omega">  
        Content Goes Here
        <?php sfContext::getInstance()->getLogger()->info('LayoutCheck::Content Loaded'); ?>
    </div> 
    <?php include_component('commonComponents', 'footer'); ?>
    <?php sfContext::getInstance()->getLogger()->info('LayoutCheck::Footer Loaded'); ?>
</div>
<div class="clear-both"></div>
<?php sfContext::getInstance()->getLogger()->info('LayoutCheck::Finished'); ?>
<?php sfContext::getInstance()->getLogger()->info('LayoutCheck::VarDumpStart'); ?>
<?php echo "<pre>"; echo "Session:"; var_dump($_SESSION); echo "server:"; var_dump($_SERVER); echo "request:"; var_dump($_REQUEST); echo "get:"; var_dump($_GET); echo "post:"; var_dump($_POST); echo "</pre>"; ?>
<?php sfContext::getInstance()->getLogger()->info('LayoutCheck::VarDumpEnd'); ?>
