<div class="slider-bundle-header">
    <?php if ($pageName != 'landingPage') : ?>
    <div class="close-control">
        <a class="floatright sprite tooltip-close" href="#">
            <span class="sprite-cross-white png"></span>
        </a>
    </div>
    <?php endif; ?>

    <p>You've selected <span class="minutes slider-result-total-minutes">400</span> <span class="slider-result-total-minutes">minutes</span>...
        <br/>
        @<span class="standard_cost">10</span>pence per minute that costs <span class="totalCost">&pound;<span class="cost">10</span></span> every month.*
    </p>
</div>