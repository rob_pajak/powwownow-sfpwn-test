<div id="assumptions-modal" title="Assumptions and Terms &amp; Conditions" class="bundles-modal">
    <h2>Assumptions and Terms &amp; Conditions</h2>
    <p>Calculations are based on all participants dialling a UK Landline (e.g 03…) number, so in this incidence call costs are covered by the chairperson.</p>
    <ul style="margin: 10px 0;">
        <li class="AYCM">
            <p>
                <span class="title">All You Can Meet (AYCM)<em>&#42;</em></span>
                - includes 2,500 minutes, charged at &pound;35 per month. Minutes are based on all participants time within the conference call where a UK Landline number has been dialled by the participant.
            </p>
        </li>
        <li class="1000">
            <p>
                <span class="title">Landline 1000<em>&#42;</em></span>
                -  includes 1,000 minutes, charged at &pound;25 per month. Minutes are based on all participants time within the conference call where a UK Landline number has been dialled by the participant.
            </p>
        </li>
        <li class="2500">
            <p>
                <span class="title">Landline 2500<em>&#42;</em></span>
                - includes 2,500 minutes, charged at &pound;55 per month. Minutes are based on all participants time within the conference call where a UK Landline number has been dialled by the participant.
            </p>
        </li>
        <li class="5000">
            <p>
                <span class="title">Landline 5000</span>
                - includes 5,000 minutes, charged at &pound;100 per month. Minutes are based on all participants time on the conference call where a UK Landline number has been dialled. Any UK Landline minutes used outside of the Bundle allowance will be charged at the discounted rate of 2pence per minute.
            </p>
        </li>
    </ul>
    <p>&#42;Any minutes used outside of the Bundle allowance will be charged at our standard rate.</p>
    <div style="margin-top: 10px;">
        <div class="assumptions-modal-btn"><button class="assumptions-close-btn button-stroked button-orange" type="submit"><span>CLOSE</span></button></div>
        <div class="assumptions-modal-link"><a onclick="window.location='<?php echo url_for('@terms_and_conditions'); ?>'" href="<?php echo url_for('@terms_and_conditions'); ?>">Powwownow Terms &amp; Conditions</a></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        pst.initModalWindow('assumptions','assumptionsOpener');
        pst.initDynamicTAC();
    });
</script>