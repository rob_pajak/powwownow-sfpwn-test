<div id="slider-results" class="grid_sub_17">
    <!-- Initial Page Content -->
    <div id="slider-introduction">
        <?php include_partial('productSelectorTool/introduction') ?>
        <?php include_partial('productSelectorTool/bundleFooter'); ?>
    </div>

    <!-- No Bundle Content -->
    <div id="slider-no_bundle">
        <?php include_partial('productSelectorTool/bundleHeader',array('pageName' => $pageName)); ?>
        <?php include_partial('productSelectorTool/noBundle',array('pageName' => $pageName)); ?>
        <?php include_partial('productSelectorTool/bundleFooter'); ?>
    </div>

    <!-- Bundle Content -->
    <div id="slider-result">
        <?php include_partial('productSelectorTool/bundleHeader',array('pageName' => $pageName)); ?>
        <h3>
            <span id="multiple-bundle-results-header">You can save a bundle with either...</span>
            <span id="single-bundle-result-header">You can save a bundle with...</span>
        </h3>
        <div id="bundle-suggestions">
            <?php
            foreach($bundles['bundles'] as $name => $data) {
                include_partial('productSelectorTool/bundle', array(
                    'name' => $name,
                    'data' => $data,
                    'showBuyButtons' => $bundles['showBuyButtons']
                ));
            }
            ?>
        </div>
        <?php include_partial('productSelectorTool/bundleFooter'); ?>
    </div>
    <script type="text/javascript">$(document).ready(function () {pst.setStandardCost('<?php echo ($bundles['standard_cost'] * 100); ?>');});</script>
</div>

<!-- Modal Windows -->
<?php include_partial('productSelectorTool/singleMultiple'); ?>
<?php include_partial('productSelectorTool/assumptions'); ?>
<?php include_partial('productSelectorTool/rightMoney'); ?>
<?php include_partial('productSelectorTool/bundleInTheBasket'); ?>
<?php include_partial('productSelectorTool/accountHasBundle'); ?>