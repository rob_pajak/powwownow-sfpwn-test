<div id="rightMoney-modal" title="All Call Bundles..." class="bundles-modal">
    <h2>All Call Bundles...</h2>
    <ul>
        <li>
            <p>
                <span class="title">All You Can Meet (AYCM)</span>
                - This personal Bundle is charged at the staggeringly low price of &pound;35 a month, inclusive of 2,500 minutes - enough talk time for individuals making on average one daily 30 minute conference call, with 4 people using a UK Landline number.
                <a href="#" class="btn-buy_now" data-bundle="AYCM" data-id="229">BUY this Bundle</a>.
            </p>
        </li>
        <li>
            <p>
                <span class="title">Landline 1000</span>
                - This team Bundle is charged at £25 a month, inclusive of 1,000 minutes - ideal for companies making on average one weekly 60 minute conference call, with 4 people using a UK Landline number.
                <a href="#" class="btn-buy_now" data-bundle="1000" data-id="223">BUY this Bundle</a>.
            </p>
        </li>
        <li>
            <p>
                <span class="title">Landline 2500</span>
                - This team Bundle is charged at &pound;55 a month, inclusive of 2,500 minutes - ideal for companies making on average one daily 30 minute conference call, with 4 people using a UK Landline number.
                <a href="#" class="btn-buy_now" data-bundle="2500" data-id="225">BUY this Bundle</a>.
            </p>
        </li>
        <li>
            <p>
                <span class="title">Landline 5000</span>
                - This team Bundle is charged at &pound;100 a month, inclusive of 5,000 minutes - ideal for companies making on average one daily 60 minute conference call, with 4 people using a UK Landline number.
                <a href="#" class="btn-buy_now" data-bundle="5000" data-id="227">BUY this Bundle</a>.
            </p>
        </li>
    </ul>
    <button class="rightMoney-close-btn button-stroked button-orange" type="submit"><span>CLOSE</span></button>
</div>
<script type="text/javascript">$(document).ready(function() {pst.initModalWindow('rightMoney','rightMoneyOpener');});</script>