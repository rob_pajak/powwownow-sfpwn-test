<div id="sliders" class="grid_sub_6">

    <h3>Save a bundle with our call Bundles...</h3>
    <p>How many conference calls do you make a month?</p>
    <div class="pst-slider">
        <div id="conferenceSlider"></div>
    </div>

    <p>How many people are on the calls?</p>
    <div class="pst-slider">
        <div id="peopleSlider"></div>
    </div>

    <p>How many minutes do they last?</p>
    <div class="pst-slider">
        <div id="minuteSlider"></div>
    </div>
    <div class="aligncenter">
        <button id="btn-calculate" class="button-stroked button-orange" type="submit"><span>CALCULATE</span></button>
    </div>
    <script type="text/javascript">$(document).ready(function () {pst.initSlider('<?php echo $pageName; ?>',<?php echo $auth; ?>);});</script>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        pst.initShakeSliders();
    });
</script>

