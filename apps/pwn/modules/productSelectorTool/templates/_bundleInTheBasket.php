<div id="bundleInTheBasket-modal" title="Bundle in the basket" class="bundles-modal">
    <p>
        You already have a Bundle in your basket!
    </p>
    <p>
        You can only purchase one Bundle for your account.
    </p>
    <div class="ui-dialog-buttonset" style="border-top: 1px; text-align: right; position: absolute; bottom: 0px; right: 0px; border-top: 1px solid #7EA2B4; padding: 10px 15px; width: 100%; ">
        <button id="bundleInTheBasket-new" type="button">
            PURCHASE NEW BUNDLE
        </button>
        &nbsp;
        <button id="bundleInTheBasket-basket" type="button">
            GO TO BASKET
        </button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#bundleInTheBasket-modal').dialog({
            autoOpen: false,
            height: 123,
            width: 400,
            modal: true,
            resizable: false
        });

        $('#bundleInTheBasket-modal button#bundleInTheBasket-new').click( function() {

            $.post('/s/Basket', { clearbasket: 'yes' }, 'json')
                .done(function() {
                  $(location).attr('href', '/s/Basket?' + Math.random());
                });
        });

        $('#bundleInTheBasket-modal button#bundleInTheBasket-basket').click( function() {
            $(location).attr('href', '/s/Basket');
        });

    });
</script>