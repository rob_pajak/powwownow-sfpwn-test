<div id="<?php echo $name; ?>" data-cost="<?php echo $data['cost']; ?>" data-minutes="<?php echo $data['minutes']; ?>" data-rate="<?php echo $data['rate']; ?>" data-type="<?php echo $data['type']; ?>"  class="bundle_teaser">
    <p class="bundle-teaser-type">
        <?php if ($data['type'] == 'personal') : ?>
            Personal Bundle (<a href="#" class="singleMultipleOpener">single chairperson</a>)
        <?php else : ?>
            Team Bundle (<a href="#" class="singleMultipleOpener">multiple chairpersons</a>)
        <?php endif; ?>
    </p>
    <h2 class="bundle-teaser-title">
        <?php echo $data['title']; ?>* <span class="bundle-teaser-subtitle">(Inclusive of <?php echo $data['minutes']; ?> minutes)</span>
    </h2>
    <p class="bundle-teaser-saving">
        You'll save <span class="bundle-teaser-saving-money">&pound;<span class="money_savings">6.66</span></span> per month.
        <span class="more_minutes">
            <br/>You'll get <span class="bundle-teaser-saving-time"><span class="time_gained">6</span> hours more</span> call time per month.
        </span>
    </p>
    <?php if ($showBuyButtons) : ?>
    <div class="bundle-teaser-buy-button">
        <button class="button-orange btn-buy_now" data-bundle="<?php echo $name; ?>" data-id="<?php echo $data['product_group_id']; ?>">
            <span class="buy-now">Buy now</span>
            <span class="price">£<?php echo (int) $data['cost'] ?> per month</span>
        </button>
    </div>
    <?php endif; ?>
</div>