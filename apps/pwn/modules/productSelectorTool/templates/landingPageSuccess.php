<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_18" style="margin-bottom: 20px;">
            <h1 class="tab-h1" style="line-height: 100%; margin-bottom: 0; margin-left: 0; margin-top: 20px; font-size: 30px;">
                Get more chat for your cash with one of our
            </h1>
            <h1 class="tab-h1" style="line-height: 100%; margin-left: 0; margin-top: 0; font-size: 30px;">
                Money-Saving Bundles...
            </h1>
        </div>
        <?php include_partial('sliders', array('pageName' => 'landingPage', 'auth' => $authenticated)) ?>
        <?php include_partial('result', array('bundles' => $bundles, 'pageName' => 'landingPage')) ?>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>