<div id="accountHasBundle-modal" title="Account has bundle" class="bundles-modal">
    <p>You have already have a Bundle on your account!</p>
    <p>You can only purchase one Bundle for your account, if you wish to change it contact Customer Services on 0203 398 0398.</p>
    <div class="ui-dialog-buttonset" style="border-top: 1px; text-align: right; position: absolute; bottom: 0; right: 0; border-top: 1px solid #7EA2B4; padding: 10px 15px; width: 100%; ">
        <button type="button">OK</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#accountHasBundle-modal').dialog({
            autoOpen: false,
            minHeight: 130,
            width: 700,
            modal: true,
            resizable: false
        });

        $('#accountHasBundle-modal button').click(function() {
            $('#accountHasBundle-modal').dialog("close");
        });
    });
</script>