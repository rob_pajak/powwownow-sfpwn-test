<div class="slider-introduction-content">
    <p>
        Try our Bundle Selector Tool… use the sliders to answer the 3 simple questions, click calculate and see exactly how much you can start saving!
    </p>
    <p>Your Bundle minutes will be available to use instantly.</p>

    <p class="slider-introduction-content-image"><img src="/sfimages/plus/bundle-product-banner-transparent1.png" alt=""/>
    <p class="slider-introduction-content-view" style="margin-top: 100px;">
        Not sure which Bundle is right for you?
        <br/><a href="#" class="rightMoneyOpener">View all our Money-Saving Bundles</a>
    </p>
</div>