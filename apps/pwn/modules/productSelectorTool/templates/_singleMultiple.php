<div id="singleMultiple-modal" title="Personal &amp; Team Bundles for single or multiple users" class="bundles-modal">
    <h2>Personal &amp; Team Bundles</h2>
    <p><span class="title">Single Chairperson</span> - A Personal Bundle can only be used by one chairperson. Therefore, all the minutes in the Bundle are used by a single chairperson and the participants on their conference calls.</p>
    <p><span class="title">Multiple Chairpersons</span> - A Team Bundle can be used by as few or as many  chairpersons on the account. Therefore, all the minutes in the Bundle are used by multiple chairpersons and the participants on their conference calls.</p>
    <p>Don't forget....you don't have to share the Team Bundle; you can save all the minutes for yourself and your participants.</p>
    <p class="title">If you need help, one of our friendly Customer Services team will be happy to assist you, just contact us <a href="#" class="chatlink">on chat</a> or call us on 0203 398 0398.</p>
    <button class="singleMultiple-close-btn button-stroked button-orange" type="submit"><span>CLOSE</span></button>
</div>
<script type="text/javascript">$(document).ready(function() {pst.initModalWindow('singleMultiple','singleMultipleOpener');});</script>
