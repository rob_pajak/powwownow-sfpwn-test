<div class="slider-no-bundle-content">
<?php if ($pageName == 'landingPage') : ?>
    <p>The good news is that you are already enjoying our lowest call rates possible.</p>
    <p>However if you’re thinking of increasing your usage we have some great Money-Saving Bundles that you might be interested in...</p>
    <p><a href="#" class="rightMoneyOpener">View all our Money-Saving Bundles</a></p>
<?php elseif ($pageName == 'dashboard' || $pageName == 'dashboard_enhanced') : ?>
    <p>The good news is that you are already enjoying our lowest call rates possible.</p>
    <p>However if you’re thinking of increasing your usage we have some great Money-Saving Bundles that you might be interested in...</p>
    <p><a href="#" class="rightMoneyOpener">View all our Money-Saving Bundles</a></p>
<?php elseif ($pageName == 'callMinuteBundles') : ?>
    <p>The good news is that you are already enjoying our lowest call rates possible.</p>
    <p>However if you’re thinking of increasing your usage we have some great Money-Saving Bundles that you might be interested in...</p>
    <p class="slider-no-bundle-content-image"><img src="/sfimages/plus/bundle-product-banner-transparent2.png" alt=""/>
    <p class="slider-no-bundle-content-view"><a href="#" class="rightMoneyOpener">View all our Money-Saving Bundles</a></p>
<?php endif; ?>
</div>