<div class="bundle-product-dialog" id="dialog-<?php echo $dialogId; ?>">
    <div class="close-control">
        <a class="floatright sprite tooltip-close" href="#" id="dialog-<?php echo $dialogId; ?>-close">
            <span class="sprite-cross-white png"></span>
        </a>
    </div>

    <div class="clearfix container_24 bundle-messages-container" style="width: 98%; margin: 0 1%;">
        <div class="grid_24">
            <div class="margin-top-20">
                <div class="clearfix">
                    <img class="floatleft" alt="Bundle icon" src="<?php echo $dialog['icon'] ?>">
                    <h2 class="rockwell blue productdetailshead"><?php echo $productName ?></h2>
                </div>
                <div class="clearfix">
                    <?php if (isset($dialog['product_introduction'])): ?>
                        <?php foreach ($dialog['product_introduction'] as $line): ?>
                            <p><?php echo __($line) ?></p>
                        <?php endforeach ?>
                    <?php endif; ?>

                    <div class="hr-spotted-top png content-seperator"></div>
                    <h3 class="rockwell blue">What are the benefits?</h3>
                    <?php if (isset($dialog['benefits'])): ?>
                        <ul class="chevron-green">
                            <?php foreach ($dialog['benefits'] as $line): ?>
                                <li class="png"><?php echo __($line) ?></li>
                            <?php endforeach ?>
                        </ul>
                    <?php endif; ?>


                    <div class="hr-spotted-top png content-seperator"></div>
                    <h3 class="rockwell blue">Is this the right Bundle for me?</h3>
                    <?php if (isset($dialog['decision'])): ?>
                        <?php foreach ($dialog['decision'] as $line): ?>
                            <p><?php echo __($line) ?></p>
                        <?php endforeach ?>
                    <?php endif; ?>

                    <div class="hr-spotted-top png content-seperator"></div>
                    <h3 class="rockwell blue">What’s the cost?</h3>
                    <?php if (isset($dialog['cost'])): ?>
                        <?php foreach ($dialog['cost'] as $line): ?>
                            <p><?php echo __($line) ?></p>
                        <?php endforeach ?>
                    <?php endif; ?>

                    <div class="hr-spotted-top png content-seperator"></div>
                    <h3 class="rockwell blue">Need to call abroad? </h3>
                    <?php if (isset($dialog['call_abroad'])): ?>
                        <?php foreach ($dialog['call_abroad'] as $line): ?>
                            <p><?php echo __($line) ?></p>
                        <?php endforeach ?>
                    <?php endif; ?>

                    <?php if (isset($dialog['call_abroad_more'])): ?>
                        <div class="hr-spotted-top png content-seperator"></div>
                        <h3 class="rockwell blue">Calling abroad? </h3>
                        <?php foreach ($dialog['call_abroad_more'] as $line): ?>
                            <p><?php echo __($line) ?></p>
                        <?php endforeach ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>