<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="grid_sub_18" style="margin-bottom: 10px;">
            <h1 class="tab-h1" style="line-height: 100%; margin-bottom: 0; margin-left: 0; margin-top: 20px; font-size: 30px;">
                Get more chat for your cash with one of our
            </h1>
            <h1 class="tab-h1" style="line-height: 100%; margin-left: 0; margin-top: 0; font-size: 30px;">
                Money-Saving Bundles...
            </h1>

        </div>

        <div class="grid_sub_24">
            <div>
                <p style="font-size: 12px;">
                    As you know we’re all about making life simple and saving you money on your conference calls. Perfect for individuals and businesses, we’ve a range of Landline Bundles on offer designed to meet a variety of conference calling requirements.
                </p>
            </div>
        </div>

        <?php include_partial('sliders', array('pageName' => 'callMinuteBundles', 'auth' => $authenticated)) ?>
        <?php include_partial('result', array('bundles' => $bundles, 'pageName' => 'callMinuteBundles')) ?>
    </div>

    <div class="grid_24 clearfix margin-top-10">

        <div id="tooltip-callminutebundle" class="tooltip" style="display: none; left:50px;">
            <div class="tooltip-arrow-top png"></div>
            <p>You, the chairperson of the call dials the Landline (0203...) number and shares the Shared Cost (0844...) number with your participants, so they pay for their own call.</p>
        </div>
    </div>

    <div class="grid_24 margin-top-10">
        <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
    </div>

    <?php include_component('commonComponents', 'plusFeatures', array('page' => 'callMinuteBundle')); ?>

    <div class="grid_24 margin-top-10">
        <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
    </div>

    <div class="grid_24 clearfix margin-top-10">
        <h4 class="font-bigger">What's on offer...</h4>
        <?php include_component('commonComponents', 'whyPlus', array('page' => 'callMinuteBundle')); ?>
    </div>

    <div class="grid_24">
        <div class="blue-bubble-contactus font-bigger">
            To find out more about Bundles, call us on 0203 398 0398 or get a instant response using our <a href="javascript:void(0);" class="chatlink">Live Chat</a>.
        </div>
    </div>

    <div class="grid_24">
        <div class="hr-spotted-top png content-seperator"><!--Blank--></div>
    </div>

    <?php include_component('commonComponents', 'socialMediaFooterE'); ?>
    <?php include_component('commonComponents', 'footer'); ?>

    <!-- Start Modal Windows -->
    <?php foreach ($bundleProducts as $product) : ?>
        <?php include_partial('productSelectorTool/bundleDialog', array(
            'dialog'   => isset($product['product_dialog']) ? $product['product_dialog'] : '', 'productName' => $product['group_name'],
            'dialogId' => $product['product_group_id']
        ));
        ?>
    <?php endforeach; ?>
    <div style="display: none;" id="dialog-plus-link"></div>
    <div id="dialog-plus">
        <div class="clearfix container_24" id="dialog-plus-main">
            <!-- Start Plus Templates -->
            <?php foreach ($sf_data->getRaw('plusTemplates') as $formName => $form) : ?>
                <?php echo $form; ?>
            <?php endforeach; ?>
            <!-- End Plus Templates -->
        </div>
    </div>
    <!-- End Modal Windows -->

    <!-- Tool Tips and Dialog Windows -->
    <script type="text/javascript">
        $(document).ready(function() {
            tooltips.initTooltip({
                'main' : {
                    'id' : 'tooltip-callminutebundle',
                    'onClick' : false,
                    'onHover' : true
                }
            });


            var plusWidth = 770;

            if ($.browser.msie && $.browser.version <= 8.0) {
                plusWidth = 830;
            }

            dialogs.initDialog({
                'AYCM' : {
                    'id' : 'dialog-229',
                    'minWidth' : 750,
                    'modal' : true,
                    'closeOnEscape' : true,
                    'width' : 940,
                    'minHeight' : 630
                },
                '1000' : {
                    'id' : 'dialog-223',
                    'minWidth' : 750,
                    'modal' : true,
                    'closeOnEscape' : true,
                    'width' : 940,
                    'minHeight' : 630
                },
                '2500' : {
                    'id' : 'dialog-225',
                    'minWidth' : 750,
                    'modal' : true,
                    'closeOnEscape' : true,
                    'width' : 940,
                    'minHeight' : 630
                },
                '5000' : {
                    'id' : 'dialog-227',
                    'minWidth' : 750,
                    'modal' : true,
                    'closeOnEscape' : true,
                    'width' : 940,
                    'minHeight' : 630
                },
                'NewPlusPopup' : {
                    'id' : 'dialog-plus',
                    'modal': true,
                    'closeOnEscape': true,
                    'width': plusWidth,
                    'minHeight': 200
                }
            });
        });
    </script>
</div>