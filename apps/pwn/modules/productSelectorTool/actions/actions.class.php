<?php
/**
 * Product Selector Tool.
 *
 * @package    powwownow
 * @subpackage productSelectorTool
 * @author     Asfer Tamimi
 *
 */
class productSelectorToolActions extends sfActions {

    /**
     * Product Selector Landing Page [PAGE]
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeLandingPage(sfWebRequest $request) {
        $result = Common::getBundlesForProductSelector();
        $this->bundles = array_merge($result,array('showBuyButtons' => true));
        $this->authenticated = $this->getUser()->getAttribute('authenticated',0);
    }

    /**
     * Store the Initial Bundles Selection
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeStoreCalculationData(sfWebRequest $request) {
        $result = Common::storeProductSelector(array(
            'referer'     => $request->getReferer(),
            'session'     => session_id(),
            'people'      => $request->getParameter('people'),
            'conferences' => $request->getParameter('conferences'),
            'minutes'     => $request->getParameter('minutes'),
            'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
            'account_id'  => $this->getUser()->getAttribute('account_id'),
        ));

        if (!$result) {
            $this->logMessage('storeProductSelector Returned False','err');
        } else {
            echo json_encode(array('id' => $result['id']));
        }

        return sfView::NONE;
    }

    /**
     * Update The Bundles Selection, when the User clicks the Buy Now Button
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeUpdateCalculationData(sfWebRequest $request) {
        $result = Common::updateProductSelector(array(
            'id'                 => $request->getPostParameter('id'),
            'bundle_id_selected' => $request->getPostParameter('bundle_id'),
            'bundle_selected'    => true
        ));

        if (!$result) {
            $this->logMessage('updateProductSelector Returned False','err');
        } else {
            // Create a Session Namespace to for Product Selector and Store the ID
            $_SESSION['ProductSelector_id'] = $request->getPostParameter('id');
            $_SESSION['ProductSelector_bundle_id'] = $request->getPostParameter('bundle_id');
        }

        return sfView::NONE;
    }

    /**
     * Call Minute Bundles Landing Page [PAGE]
     * @param sfWebRequest $request
     * @return sfView::None
     */
    public function executeCallMinuteBundles(sfWebRequest $request) {
        // Load Helpers
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Partial'));

        // Set Globals
        $auth = $this->getUser()->isAuthenticated();

        // Product Selector Tool
        $result = Common::getBundlesForProductSelector();
        $this->bundles = array_merge($result,array('showBuyButtons' => true));
        $this->authenticated = ($auth) ? $auth : 0;

        // Bundle Popups
        $bundleDisplay = new BundleProductPageDisplay();
        $bundleProducts = plusCommon::retrieveBundleProducts($this->getUser()->getCulture());
        $this->bundleProducts = $bundleDisplay->prepareBundlesForDisplay(
            $bundleProducts,
            false,
            false,
            null
        );

        // Plus Templates
        $this->plusTemplates = array(
            'plusEmailForm'            => get_partial('commonComponents/plusEmailForm',array()),
            'plusPasswordForm'         => get_partial('commonComponents/plusPasswordForm',array()),
            'plusFormWithPassword'     => get_partial('commonComponents/plusFormWithPassword',array()),
            'plusFormWithoutPassword'  => get_partial('commonComponents/plusFormWithoutPassword',array()),
            'plusNoSwitchPowwownow'    => get_partial('commonComponents/plusNoSwitchPowwownow',array()),
            'plusNoSwitchPlusUser'     => get_partial('commonComponents/plusNoSwitchPlusUser',array()),
            'plusNoSwitchPlusAdmin'    => get_partial('commonComponents/plusNoSwitchPlusAdmin',array()),
            'plusNoSwitchPremiumUser'  => get_partial('commonComponents/plusNoSwitchPremiumUser',array()),
            'plusNoSwitchPremiumAdmin' => get_partial('commonComponents/plusNoSwitchPremiumAdmin',array()),

            'plusRedirectionNew'       => get_partial('commonComponents/plusRedirectionNew',array()),
            'plusRedirectionExisting'  => get_partial('commonComponents/plusRedirectionExisting',array()),
            'plusNoSwitch1'            => get_partial('commonComponents/plusNoSwitch1',array()),

            'plusExistingCustomerPowwownow'    => get_partial('login/plusExistingCustomerPowwownow',array()),
            'plusExistingCustomerPlusUser'     => get_partial('login/plusExistingCustomerPlusUser',array()),
            'plusExistingCustomerPlusAdmin'    => get_partial('login/plusExistingCustomerPlusAdmin',array()),
            'plusExistingCustomerPremiumUser'  => get_partial('login/plusExistingCustomerPremiumUser',array()),
            'plusExistingCustomerPremiumAdmin' => get_partial('login/plusExistingCustomerPremiumAdmin',array()),

            'plusExistingCustomerPlusAdminAuth'    => get_partial('commonComponents/plusRedirectionAuth',array()),
            'plusExistingCustomerPremiumUserAuth'  => get_partial('login/plusExistingCustomerPremiumUserAuth',array()),
            'plusExistingCustomerPremiumAdminAuth' => get_partial('login/plusExistingCustomerPremiumAdminAuth',array()),
        );
    }

    public function executeCallMinuteBundlesInitialForm(sfWebRequest $request) {
        // Set Globals
        $response = $this->getResponse();
        $user     = $this->getUser();
        $auth     = $user->isAuthenticated();

        // Set Output
        $response->setContentType('application/json');

        // Set Output Variables
        $template   = '';
        $email      = '';
        $first_name = '';

        if ($auth) {
            if ($user->hasCredential('POWWOWNOW')) {
                $template = 'plusExistingCustomerPowwownow';
            } elseif($user->hasCredential('PLUS_USER')) {
                $template = 'plusExistingCustomerPlusUser';
            } elseif($user->hasCredential('PLUS_ADMIN')) {
                $template = 'plusRedirectionAuth';
            } elseif($user->hasCredential('PREMIUM_USER')) {
                $template = 'plusExistingCustomerPremiumUserAuth';
            } elseif($user->hasCredential('PREMIUM_ADMIN')) {
                $template = 'plusExistingCustomerPremiumAdminAuth';
            }

            $email      = $user->getAttribute('email');
            $first_name = $user->getAttribute('first_name');
        } else {
            // Old Functionality
            $template = 'plusEmailForm';
        }


        echo json_encode(array(
            'template'   => $template,
            'email'      => $email,
            'first_name' => $first_name
        ));
        return sfView::NONE;
    }

}