<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <div class="header_breadcrumbs">
            <?php include_partial(
                'commonComponents/genericBreadcrumbs',
                array('breadcrumbs' => array(array('News', '@news'), $result['title']), 'title' => __($result['title']))
            ); ?>
        </div>
        <h2><?php echo $result['title']; ?></h2>
        <h4><?php echo date("d F Y", strtotime($result['first_published'])); ?>,
            <a target="_blank" href="<?php echo $citationInfo['CitationLink']; ?>">
                <?php echo $citationInfo['CitationText']; ?>
            </a>
        </h4>

        <a href="#" class="animate-image">
            <img src="<?php echo $imageInfo['ArticleImage']; ?>"
                 alt="<?php echo $imageInfo['ArticleImageAlt']; ?>"
                 class="news-article-image"
                 id="news-image"
                 style="height: 170px; width: 130px;"
            />
        </a>

        <?php echo $content; ?>

        <script type="text/javascript">
            (function ($) {
                'use strict';
                $(function () {
                    $('a.animate-image').click(function(){
                        var img = $('#news-image'),
                            sWidth = 130,
                            sHeight = 170,
                            lWidth = 600,
                            lHeight = 843
                        ;

                        if (img.width() === sWidth) {
                            img.animate({"width": lWidth, "height": lHeight}, "slow");
                        } else {
                            img.animate({"width": sWidth, "height": sHeight}, "slow");
                        }
                    });
                });
            })(jQuery);
        </script>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
