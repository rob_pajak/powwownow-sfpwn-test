<div class="items-per-page">
    Display
    <select class="displayList" onchange="newsPagination_LoadPaginatedPage(0, $(this).val()); return false;" name="items_per_page">
        <option value="5">5</option>
        <option value="10">10</option>
        <option value="30">30</option>
        <option value="100">100</option>
        <option value="-1">View all</option>
    </select> per page
</div>