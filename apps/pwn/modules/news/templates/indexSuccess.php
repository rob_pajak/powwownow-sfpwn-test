<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'breadcrumbsAndHeader',
            array(
                'breadcrumbs' => array('About Us', 'News'),
                'breadcrumbsTitle' => __('News'),
                'headingTitle' => 'News &amp; Press',
                'headingSize' => 'm',
                'headingUserType' => 'powwownow'
            )
        ); ?>
        <div class="clearfix" style="margin-top: 50px;">
            <table id="tableNewsArticles" class="floatleft"></table>
        </div>
        <script type="text/javascript">
            (function ($) {
                'use strict';
                $(function () {
                    window.dataTableNews = $('#tableNewsArticles').dataTable({
                        "aaData": <?php echo json_encode($sf_data->getRaw('initNews')); ?>,
                        "aoColumns": [
                            { "sTitle": 'News' }
                        ],
                        "oLanguage": {
                            "sProcessing": DATA_TABLES_sProcessing,
                            "sLengthMenu": "Display _MENU_ per page",
                            "sZeroRecords": DATA_TABLES_sZeroRecords,
                            "sEmptyTable": DATA_TABLES_sEmptyTable,
                            "sLoadingRecords": DATA_TABLES_sLoadingRecords,
                            "sInfo": DATA_TABLES_sInfo,
                            "sInfoEmpty": DATA_TABLES_sInfoEmpty,
                            "sInfoFiltered": DATA_TABLES_sInfoFiltered,
                            "sInfoPostFix": "",
                            "sInfoThousands": ",",
                            "sSearch": DATA_TABLES_sSearch,
                            "sUrl": "",
                            "fnInfoCallback": null,
                            "oPaginate": {
                                "sFirst": DATA_TABLES_sFirst,
                                "sPrevious": DATA_TABLES_sPrevious,
                                "sNext": DATA_TABLES_sNext,
                                "sLast": DATA_TABLES_sLast
                            }
                        },
                        "sPaginationType": "full_numbers",
                        "aLengthMenu": [
                            [5, 10, 30, 100, -1],
                            [5, 10, 30, 100, "All"]
                        ],
                        "iDisplayLength": 5,
                        "aaSorting": [],
                        "fnDrawCallback": function () {
                            //init_table();
                        }
                    });

                    // Obtain Paginate and Copy to After the tableNewsArticles_length
                    $('#tableNewsArticles_length').after(
                        $('#tableNewsArticles_info').next()
                    );
                    $('.dataTables_info').hide();
                });
            })(jQuery);
        </script>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
