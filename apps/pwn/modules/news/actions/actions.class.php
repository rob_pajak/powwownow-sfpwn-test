<?php

/**
 * News Actions
 *
 * @package    powwownow
 * @subpackage news
 * @author     Asfer Tamimi
 *
 */
class newsActions extends sfActions
{
    /**
     * Main News Page [PAGE]
     *
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     */
    public function executeIndex()
    {
        $news = new News_New($this->getUser()->getCulture());

        // Get the News Articles from the DB / Cache
        $news->getNewsArticles(array('status' => 'Published'));

        // Build the Resources List
        $news->sortResourceListFromArticles();

        // Build a DataTable Array for the News List
        $dataTableArr = $news->getNewsListForDataTable();

        $this->setVar('initNews', $dataTableArr);
    }

    /**
     * News Articles [PAGE]
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeShowNewsArticle(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        // Given URL
        $url = $request->getParameter('article_route');
        $url = htmlspecialchars($url);

        $news = new News_New($this->getUser()->getCulture());

        // Get the News Articles from the DB / Cache
        $result = $news->getNewsArticles(array('url' => $url));

        // For Empty Result Redirect to News Homepage
        if (empty($result)) {
            $this->redirect(url_for('@news'), 301);
        }

        // Build the Resources List
        $news->sortResourceListFromArticles();

        $this->setVar('content', $result[0]['content'], true);
        $this->setVar('imageInfo', $news->getArticleImageInformation(), true);
        $this->setVar('citationInfo', $news->getCitationInformation(), true);
        $this->setVar('result', $result[0], true);
        $response->setTitle('Powwownow News | ' . $result[0]['title']);
    }

    /**
     * Main News Old [PAGE]
     * @author Asfer Tamimi
     */
    public function executeOld()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->redirect(url_for('@news'), 301);
    }

    /**
     * News Articles Old [PAGE]
     *
     * @param sfWebRequest $request
     * @author Asfer Tamimi
     */
    public function executeShowNewsArticleOld(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        // Given URL
        $url = $request->getParameter('article_route');
        $url = htmlspecialchars($url);

        $news = new News_New($this->getUser()->getCulture());

        // Get the News Articles from the DB / Cache
        $result = $news->getNewsArticles(array('url' => $url));

        // For Empty Result Redirect to News Homepage, Otherwise Redirect to the Current URL
        if (empty($result)) {
            $this->redirect(url_for('@news'), 301);
        } else {
            $this->redirect(url_for('@news_article?article_route=' . $url), 301);
        }
    }
}
