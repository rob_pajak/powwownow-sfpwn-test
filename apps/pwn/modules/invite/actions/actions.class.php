<?php

/**
 * Invite related actions
 *
 * @package    powwownow
 * @subpackage invite
 * @author     Wiseman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class inviteActions extends sfActions
{
    /**
     * View an existing invite and gives the option to accept it
     *
     * This action just does the check to see if the hash supplied is valid, and if the user
     * is a myPwn user or not, then forwards to the relevant action
     *
     * @param sfRequest $request A request object
     */
    public function executeView(sfWebRequest $request)
    {

        /**
         * Ensure user meets the critera to be upgradable
         */
        if (!$this->isSwitchableToPlus($request->getParameter('contact_ref'))) {
            return sfView::ERROR;
        }

        /**
         * Check if the hash is valid for the supplied account_id and contact_ref
         */
        $response = Hermes_Client_Rest::call(
            'Plus.validatePlusInvitationHash',
            array(
                'account_id'  => $request->getParameter('account_id'),
                'contact_ref' => $request->getParameter('contact_ref'),
                'hash'        => $request->getParameter('hash')
            )
        );

        if ($response['valid'] !== 1) {
            $this->logMessage(
                'User tried to view an invite to join plus, but the hash is invalid: ' . serialize(
                    array(
                        'account_id'  => $request->getParameter('account_id'),
                        'contact_ref' => $request->getParameter('contact_ref'),
                        'hash'        => $request->getParameter('hash')
                    )
                ),
                'warning'
            );
            return sfView::ERROR;
        }

        /**
         * See if user has a mypwn password, they are shown a different form accordingly
         */
        $response = Hermes_Client_Rest::call(
            'getContactByRef',
            array('contact_ref' => $request->getParameter('contact_ref'))
        );

        if ($response['password_set']) {
            $this->forward('invite', 'MyPwnUserView');
        } else {
            $this->forward('invite', 'PinOnlyUserView');
        }

    }

    /**
     * Shows form to accept invite for a user who already has a password
     */
    public function executeMyPwnUserView(sfWebRequest $request)
    {
        $this->form = new plusInviteAcceptForMyPwnUser(array(
            'account_id'  => $request->getParameter('account_id'),
            'contact_ref' => $request->getParameter('contact_ref'),
            'hash'        => $request->getParameter('hash')
        ));

        $this->accountId = $request->getParameter('account_id');
    }


    /**
     * Shows form to accept invite for a user who does not have a mypowwownow login,
     * they will need to enter their name and other details
     */
    public function executePinOnlyUserView(sfWebRequest $request)
    {
        $this->form = new plusInviteAcceptForPinOnlyUser(array(
            'account_id'  => $request->getParameter('account_id'),
            'contact_ref' => $request->getParameter('contact_ref'),
            'hash'        => $request->getParameter('hash')
        ));

        $this->accountId = $request->getParameter('account_id');
    }


    /**
     * Processes accepting of invite, handles both pin only and mypwn user form
     */
    public function executePlusInviteAcceptAjax(sfWebRequest $request)
    {

        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        $this->getResponse()->setContentType('application/json');

        try {

            /**
             * Load up contact
             */
            $contact = Hermes_Client_Rest::call(
                'getContactByRef',
                array('contact_ref' => $request->getParameter('contact_ref'))
            );

            /**
             * If the user already has a password set then we are just upgrading them
             * to plus, otherwise we also need to create mypowwownow user
             */
            if ($contact['password_set']) {
                $createMyPwnUser = false;
            } else {
                $createMyPwnUser = true;
            }

            /**
             * If they already are a myPwn user, validate password, as after upgrade
             * they will be logged in to myPowwownow
             */
            if (!$createMyPwnUser) {
                if (empty($contact['password']) || ($contact['password'] !== md5($request->getParameter('password')))) {

                    $this->getResponse()->setStatusCode(500);
                    echo json_encode(
                        array(
                            'error_messages' => array(
                                array(
                                    'message'    => 'FORM_VALIDATION_WRONG_PASSWORD',
                                    'field_name' => 'password'
                                )
                            )
                        )
                    );
                    return sfView::NONE;
                }
            }

            /**
             * Check contact meets criteria to be upgradable
             */
            if (!$this->isSwitchableToPlus($request->getParameter('contact_ref'))) {
                throw new Exception('Criteria not met');
            }

            /**
             * Check supplied hash is valid
             */
            $response = Hermes_Client_Rest::call(
                'Plus.validatePlusInvitationHash',
                array(
                    'account_id'  => $request->getParameter('account_id'),
                    'contact_ref' => $request->getParameter('contact_ref'),
                    'hash'        => $request->getParameter('hash')
                )
            );
            if ($response['valid'] !== 1) {
                throw new Exception('Hash invalid');
            }

            /**
             * If we are also setting up mypwn details, then do so now
             */
            if ($createMyPwnUser) {
                $response = Hermes_Client_Rest::call(
                    'updateContact',
                    array(
                        'contact_ref' => $request->getParameter('contact_ref'),
                        'first_name'  => $request->getParameter('first_name'),
                        'last_name'   => $request->getParameter('last_name'),
                        'password'    => $request->getParameter('password'),
                    )
                );
            }

            /**
             * Perform merge
             */
            $response = Hermes_Client_Rest::call(
                'Plus.mergeEnhancedToPlusAccount',
                array(
                    'account_id'  => $request->getParameter('account_id'),
                    'contact_ref' => $request->getParameter('contact_ref'),
                    'service_ref' => '850'
                )
            );

            /**
             * Send account admin an email letting then know their invite has been
             * accepted
             */
            try {
                $response = Hermes_Client_Rest::call(
                    'DotMailer.sendPlusInvitationAcceptedEmailToAdmin',
                    array(
                        'account_id'  => $request->getParameter('account_id'),
                        'contact_ref' => $request->getParameter('contact_ref')
                    )
                );
            } catch (Exception $e) {
                $this->logMessage('Unable to send mail to Plus Admin: ' . serialize($e), 'err');
            }

            /**
             * Log user in to mypowwownow
             */
            if (Plus_Authenticate::logIn($contact['email'], $request->getParameter('password')) !== true) {
                throw new Exception('Could not log User in to mypowwownow');
            }

            /**
             * Clear Teamsite cache
             */
            try {
                PWN_Cache_Clearer::modifiedPin($request->getParameter('contact_ref'));
                PWN_Cache_Clearer::modifiedContact($request->getParameter('contact_ref'));
                $this->logMessage(
                    'Cleared Cache for contact_ref: ' . $request->getParameter(
                        'contact_ref'
                    ) . ' while switching to Plus',
                    'info'
                );
            } catch (Exception $e) {
                $this->logMessage('Unable to clear cache after switching User to plus:' . serialize($e), 'err');
            }

            /**
             * Redirect user to home page grid, it has an overlay welcoming them to mypowwownow
             */
            echo json_encode(array('redirect_url' => 'URL_MYPWN_PLUS_INVITE_ACCEPT_CONFIRMATION_PAGE'));

        } catch (Exception $e) {

            $this->logMessage(
                "User tried to switch to plus but was unable to: {$e->getMessage()} " . serialize(
                    array(
                        'Exception'   => $e,
                        'account_id'  => $request->getParameter('account_id'),
                        'contact_ref' => $request->getParameter('contact_ref'),
                        'hash'        => $request->getParameter('hash')
                    )
                ),
                'err'
            );
            $this->getResponse()->setStatusCode(500);
            echo json_encode(
                array(
                    'error_messages' => array(
                        array(
                            'message'    => 'FORM_COMMUNICATION_ERROR',
                            'field_name' => 'password'
                        )
                    )
                )
            );
            return sfView::NONE;
        }

        return sfView::NONE;
    }

    /**
     * Ajax triggered email invitation
     * to join plus account
     * @author  Vitaly Kondratiev
     * @param   int contact_ref
     * @return  json
     */
    public function executeSend(sfWebRequest $request)
    {
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        $this->getResponse()->setContentType('application/json');

        $email = $request->getParameter('email');

        if (($this->getUser()->hasCredential('PLUS_ADMIN')) && !empty($email)) {

            $response = Hermes_Client_Rest::call('getContact', array('email' => $request->getParameter('email')));

            if (empty($response['contact_ref'])) {
                throw new Exceptio('Could not send invite to ' . $request->getParameter(
                        'email'
                    ) . ' as their contact_ref was not found');
            }

            $invitee_contact_ref = $response['contact_ref'];

            try {

                $response = Hermes_Client_Rest::call(
                    'DotMailer.sendPlusJoinInvitationEmail',
                    array(
                        'contact_ref' => $invitee_contact_ref,
                        'account_id'  => $this->getUser()->getAttribute('account_id')
                    )
                );
            } catch (Exception $e) {

                $this->getResponse()->setStatusCode(500);
                echo json_encode(
                    array(
                        'error_messages' => array(
                            array(
                                'message'    => 'INVITATION_TO_PLUS_INVALID',
                                'field_name' => 'email'
                            )
                        )
                    )
                );
                return sfView::NONE;
                exit;

            }

            echo json_encode(array('sucess_message' => 'INVITATION_TO_PLUS_SUCCESS'));
            return sfView::NONE;
            exit;

        }
    }

    /**
     * Ensure user meets criterai to be switchable to plus - user should have 1
     * pin and it should be on service 801
     *
     * @param int contactRef
     * @return bool
     */
    public function isSwitchableToPlus($contactRef)
    {

        $pins = Hermes_Client_Rest::call('getContactPins', array('contact_ref' => $contactRef));

        if (count($pins) !== 1) {
            return false;
        }

        if ($pins[0]['service_ref'] != 801) {
            return false;
        }

        return true;
    }

}
