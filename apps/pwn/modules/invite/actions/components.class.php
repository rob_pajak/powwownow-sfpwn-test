<?php
/**
 * Invite related components
 * 
 * @author Wiseman
 */

class inviteComponents extends sfComponents {

    /**
     * Show invite details - the account name who the user will join and the
     * account admins name
     * Needs accountId passed to this component
     */
    public function executeViewInvite() {
        $response = Hermes_Client_Rest::call('getPlusAccount', array('account_id' => $this->accountId));
        $this->accountName = $response['account']['account_name'];

        $response = Hermes_Client_Rest::call('getPlusAccountAdmin', array('account_id' => $this->accountId));
        $this->inviterName = $response['first_name'] . ' ' . $response['last_name'];

    }

}