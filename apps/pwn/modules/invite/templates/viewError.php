<?php echo use_helper('pwnForm'); ?>
<?php include_component('commonComponents', 'header'); ?>

<div class="container_24">

    <div class="grid_24 alpha omega">
        <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Your Invite To Join Plus'), 'headingSize' => 'l')); ?>
        <?php $sf_response->setTitle(__('Your Invite To Join Plus')); ?>
    </div>

    <div class="grid_24">
        <p><?php echo __('An error has occurred viewing your invite. Please <a href="' . url_for('@contact_us') . '">Contact Us</a> and we should be able to resolve your problem.');?></p>
    </div>
    <?php include_component('commonComponents', 'footer'); ?>
</div>