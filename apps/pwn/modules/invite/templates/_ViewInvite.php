    <div class="grid_24">
        <p>
            <?php echo __('<b>%inviter_name%</b> has sent you an invite to join their Plus account <b>%account_name%</b>:',
                    array('%inviter_name%' => $inviterName, '%account_name%' => $accountName)) ?>
        </p>

        <p>
            <?php echo __('By accepting this invite your existing Powwownow PIN will be linked to a Plus account where the Account Administrator will be able to manage your PINs and purchase additional products and features for your use.'); ?>
        </p>

    </div>