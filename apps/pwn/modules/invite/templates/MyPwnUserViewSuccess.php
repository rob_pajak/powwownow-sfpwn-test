<?php echo use_helper('pwnForm'); ?>
<?php include_component('commonComponents', 'header'); ?>

<div class="container_24">

    <div class="grid_24 alpha omega">
        <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Your Invite To Join Plus'), 'headingSize' => 'l')); ?>
        <?php $sf_response->setTitle(__('Your Invite To Join Plus')); ?>
    </div>

    <?php include_component('invite', 'ViewInvite', array('accountId' => $accountId)); ?>

    <div class="auto-top-up-enabled-content">
        <div class="grid_12">
            <?php echo $form->renderFormTag(url_for('invite/plusInviteAcceptAjax'),
                                                     array('enctype' => 'application/x-www-form-urlencoded',
                                                           'id'      => 'form-plus-invite-accept',
                                                           'class'   => 'pwnform clearfix')); ?>

            <br/>
            <div class="form-field">
                <?php echo __('Enter your myPowwownow password:'); ?>
                <br/>
                <span class="mypwn-input-container">
                    <?php echo $form['password']->render(array('class' => 'mypwn-input input-large font-small', 'required' => 'required', 'pattern' => '[a-zA-Z0-9 *\W+]{6,20}'));?>
                </span>
            </div>
            
            <div class="grid_sub_24 margin-bottom-10">
                <a href="<?php echo url_for('@forgotten_password'); ?>" class="grey font-small"><?php echo __('Forgotten your password?');?></a>
            </div>

            <?php echo $form['account_id']->render(); ?>
            <?php echo $form['contact_ref']->render(); ?>
            <?php echo $form['hash']->render(); ?>

            <?php echo $form->submitButton(__('Accept Invite'), array('style' => 'text-align:left;')); ?>

            </form>
        </div>

        <div class="clear"></div>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#form-plus-invite-accept').initMypwnForm({
                    successMessagePos: 'above',
                    success: function (responseText) {
                                redirectUrl = eval(responseText.redirect_url);
                                window.location = redirectUrl ;
                             },
                    html5Validation: 'off'
                });
            });
        </script>

    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>