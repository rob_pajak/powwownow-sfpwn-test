<main class="container_10 clearfix">
    <section id="hero_unit" class="hero_unit grid_10">
        <div class="opacity_layer"></div>
        <header>
            <h1 class="font_green font_sketch_rockwell">Welcome</h1>
            <h2 class="font_blue_e">to the <span class="enlarge">Can-Do</span> club</h2>
        </header>
    </section>
    <?php if (isset($pin) && isset($mobileDialInNumber) && isset($dialInNumber)): ?>
        <section class="grid_10 get_started">
            <section class="center pin_details">
                <h2>Get Started</h2>
                <p>Just call <span class="enlarge font_green"><a href="tel:<?php echo $mobileDialInNumber; ?>"><?php echo $mobileDialInNumber; ?></a></span> <br />from a UK mobile</p>
                <p>and enter your PIN</p>
                <p><span class="enlarge font_green"><?php echo $pin; ?></span></p>
                <p>Alternatively you can call <br />
                    <span class="font_green"><a href="tel:<?php echo $dialInNumber?>"><?php echo $dialInNumber?></a></span> from a landline.
                </p>
                <p>
                    <a class="btn_blue" href="<?php echo url_for('@mobile_get_vcard')?>">Save my conference details</a>
                </p>
            </section>
        </section>
    <?php endif; ?>
    <?php if (isset($pin) && isset($dialInNumber)): ?>
        <section class="grid_10 pushed_for_time">
            <h2 class="font_green">Pushed for time?</h2>
            <p class="font_blue_e">Share your PIN with guests in seconds to schedule a call.</p>
            <div class="mail_to">
                <a class="btn_blue"
                   href="<?php include_component('mypwn', 'shareDetailsEmail', array(
                       'subject'        => 'Powwownow conference call details',
                       'pin'            => $pin,
                       'dial_in_number' => $dialInNumber
                   )); ?>">Email guests<br /> my details</a>
            </div>
        </section>
    <?php endif; ?>
    <section class="grid_10 social">
        <h2 class="font_green">Keep in touch</h2>
        <p class="font_blue_e">
            We'll be sad if this is the end. <br /> Follow us so we can help you <br /> be more productive and get the <br />
            most out of your workday.
        </p>
        <ul class="social_networks">
            <li>
                <a class="sprite_social_rounded twitter" href="https://twitter.com/powwownow" target="_blank">Twitter</a>
            </li>
            <li>
                <a class="sprite_social_rounded facebook" href="https://www.facebook.com/powwownow" target="_blank">Facebook</a>
            </li>
            <li>
                <a class="sprite_social_rounded linkedin" href="https://www.linkedin.com/company/powwownow" target="_blank">Linkedin</a>
            </li>
            <li>
                <a class="sprite_social_rounded youtube" href="http://www.youtube.com/user/MyPowwownow" target="_blank">Youtube</a>
            </li>
            <li>
                <a class="sprite_social_rounded blog" href="http://www.powwownow.co.uk/blog/" target="_blank">Blog</a>
            </li>
        </ul>
        <p class="continue">
            <a href="<?php echo url_for('@mypwn_index')?>">Continue to myPowwownow</a>
        </p>
    </section>
    <section class="grid_10">
        <a href="<?php echo url_for('@homepage')?>">
            <img src="/cx2/img/mobile/640_logo_strap_line.png" alt="Powwownow" class="logo"/>
        </a>
    </section>
</main>