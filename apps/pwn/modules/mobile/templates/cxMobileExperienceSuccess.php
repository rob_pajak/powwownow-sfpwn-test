<main class="container_10 cx_mobile_experience clearfix">
    <!-- START Hero Unit -->
    <section id="hero_unit" class="hero_unit grid_10">
        <section class="grid_5 suffix_5 header">
            <a href="<?php echo url_for('@homepage')?>">
                <img src="/cx2/img/mobile/640_logo.png" alt="Powwownow" class="logo" width="100"/>
            </a>
        </section>
        <section class="grid_5">
            <h1 class="hero_title">Instant hassle-free conference calling</h1>
            <h2 class="hero_sub_title">Just pay the cost of your own call</h2>
        </section>
        <section class="registration_form grid_10">
            <!-- START KO Template registration_form -->
            <?php include_component(
                'commonComponents',
                'jsKoCxRegistrationTemplate',
                array(
                    'koTemplateId' => 'ko.mobile.registration.container',
                    'koTemplateDataId' => 'ko.mobile.registration.data',
                    'buttonText' => 'Sign Up FREE',
                    'isLegacyCss' => false
                )
            ); ?>
            <!-- END KO Template registration_form -->
            <noscript>
                <div class="grid_10 clearfix no_script">
                    <p>
                        Powwownow makes heavy use of JavaScript and is needed to use Powwownow’s website. However, it seems
                        JavaScript is either disabled or not supported by your browser. To view our wonderful website properly
                        enable JavaScript by changing your browser options, and please try again!
                    </p>
                </div>
            </noscript>
        </section>
    </section>
    <!-- END Hero Unit -->
    <!-- START What You Get -->
    <section class="grid_10 what_you_get">
        <h2 class="header header_h2 font_blue_e">Why Powwownow</h2>

        <ul class="why_list">
            <li class="clearfix">
                <span class="sprite_icon sprite-640_icon_speaker" /></span>
                <span class="container title font_green">Amazing Call Quality</span>
                <span class="container body">Crystal clear quality delivered through fibre optic cabling(ooooooh!)</span>
            </li>
            <li class="clearfix">
                <span class="sprite_icon sprite-640_icon_chat" /></span>
                <span class="container title font_green">Available 24/7</span>
                <span class="container body">No waiting, no booking, Ever</span>
            </li>
            <li class="clearfix">
                <span class="sprite_icon sprite-640_icon_headset" /></span>
                <span class="container title font_green">FREE call recordings</span>
                <span class="container body">Record, playback and share your calls.</span>
            </li>
        </ul>
    </section>
    <!-- END What You Get -->
    <!-- START Mobile Accordion -->
    <section class="grid_10 accordion_container">
        <?php if (isset($mobileDialInNumber) && (isset($dialInNumber))):?>
            <div>
                <input id="accordion_1" name="accordion_1" type="checkbox" />
                <label for="accordion_1" class="font_blue_e">How much does a conference call cost?</label>
                <article class="accordion_small what_it_costs">
                    <div class="content">
                        <div>
                            <p class="font_blue_e bold">From a mobile:</p>
                            <p class="highlight font_blue_h"><?php echo $mobileDialInNumber; ?></p>
                            <p class="font_blue_e">Calls cost <span class="font_green bold">12.5p</span> (plus VAT) from all UK mobile providers.</p>
                        </div>
                        <div>
                            <p class="font_blue_e bold">From a landline:</p>
                            <p class="highlight font_blue_h"><?php echo $dialInNumber; ?></p>
                            <p class="font_blue_e">Calls cost <span class="font_green bold">4.3p</span> (plus VAT) from a BT landline.</p>
                        </div>
                    </div>
                </article>
            </div>
        <?php endif; ?>
        <?php if (isset($dialInNumbers)) :?>
            <div>
                <input id="accordion_2" name="accordion_2" type="checkbox" />
                <label for="accordion_2" class="font_blue_e">Check out our international dial-in numbers.</label>
                <article class="accordion_medium">
                    <div class="content">
                        <?php foreach ($dialInNumbers as $dialInNumber):?>
                            <div class="columns clearfix">
                                <div class="column_one cx_sprite sprite_<?php echo $dialInNumber['country_code']?>"></div>
                                <div class="column_two font_green"><?php echo $dialInNumber['country_en'] ?></div>
                                <?php if ($dialInNumber['country_code'] === 'GBR') : ?>
                                    <div class="column_three">
                                        <?php echo $dialInNumber['national_formatted']; ?>
                                        <br /><br />
                                        <a href="tel:<?php echo $mobileDialInNumber ?>"><?php echo $mobileDialInNumber ?></a>
                                        <span class="no_underline font_blue_e"> (from mobile)</span>
                                    </div>
                                <?php else: ?>
                                    <div class="column_three"><?php echo $dialInNumber['national_formatted']; ?></div>
                                <?php endif?>
                            </div>
                        <?php endforeach?>
                    </div>
                </article>
            </div>
        <?php endif?>
    </section>
    <!-- END Mobile Accordion -->

    <?php if (isset($trustPilotReviews) && $trustPilotReviews->count() > 0):?>
        <section class="grid_10 trust_pilot">
            <section class="grid_10">
                <?php foreach ($trustPilotReviews as $trustPilotReview): ?>
                    <div class="customer_reviews">
                        <div class="quote">
                            &lsquo;<?php echo $trustPilotReview->Title?>&rsquo;
                        </div>
                        <div class="author">
                            <?php echo $trustPilotReview->User->Name?>
                        </div>
                    </div>
                <?php endforeach?>
                <div class="read_more">
                    <a target="_blank" href="http://www.trustpilot.co.uk/review/powwownow.co.uk">read more</a>
                </div>
            </section>
            <section class="grid_10 static_trust_pilot">
                <h2>5 Star rating on </h2>
            </section>
        </section>
    <?php endif;?>
    <section class="grid_10 three_steps">
        <h2 class="header header_h2 font_blue_e">3 Steps to get started</h2>
        <div class="trapezoid">
            <div class="trapezoid_one">
                <div class="number">1.</div>
                <div class="text">Sign Up in one click</div>
            </div>
            <div class="trapezoid_two">
                2. Share <br />your <br />PIN
            </div>
            <div class="trapezoid_three">
                3. Start talking
            </div>
        </div>
    </section>
    <section class="grid_10 help">
        <h2 class="header header_h2 font_green">Still need a hand?</h2>
        <p>
            <a class="btn_dark_blue" href="tel:02033980398">Call Us</a>
        </p>
        <p class="small_text font_blue_e">
            Looking for the full desktop site? <a href="<?php echo url_for2('homepage', array('skip' => true))?>">Click here</a>
        </p>
    </section>
</main>