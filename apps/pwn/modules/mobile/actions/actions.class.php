<?php

require_once('../lib/PwnVCardGenerator.php');

class mobileActions extends sfActions
{
    /**
     * @desc
     *  Routing: @mobile_main_page
     *  Url: /Mobile
     *  CX Mobile Optimised Page.
     */
    public function executeCxMobileExperience()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));
        $localDialInNumbers = $this->getDialInNumbers();
        $dialInNumbers = DialInNumbersHelper::getGetDialInNumbers();
        $this->setVar('dialInNumber', $localDialInNumbers['local']);
        $this->setVar('mobileDialInNumber', $localDialInNumbers['mobile']);
        $this->setVar('dialInNumbers', $dialInNumbers);
        $this->setVar('trustPilotReviews', $this->getTrustPilot5StartReviews());
    }

    /**
     * @desc
     *  Routing: @mobile_on_board_page
     *  Url: /Mobile/Welcome
     */
    public function executeCxMobileOnBoardExperience()
    {
        $this->guardUnAuthenticatedRedirect();
        $user = $this->getUser();
        $pins = PinHelper::getDefaultPins($user->getAttribute('contact_ref'));
        $dialInNumbers = $this->getDialInNumbers();
        $this->setVar('pin', $pins['pin']);
        $this->setVar('email', $user->getAttribute('email'));
        $this->setVar('dialInNumber', $dialInNumbers['local']);
        $this->setVar('mobileDialInNumber', $dialInNumbers['mobile']);
    }

    /**
     * @desc
     *  Routing: @mobile_get_vcard
     *  Url: /Mobile/Welcome/VCard
     *  Downloadable .vcf file, allows user to save Powwownow details to phone.
     * @return string
     */
    public function  executeGetVCard()
    {
        $this->guardUnAuthenticatedRedirect();
        $this->setLayout(false);
        $pins = PinHelper::getDefaultPins($this->getUser()->getAttribute('contact_ref'));
        $pin = $pins['pin'];
        $data = array (
            'display_name' => 'Powwownow Conference Calling',
            'company' => 'Powwownow Conference Calling',
            'home_tel' => '08444737373',
            'office_tel' => "87373,$pin",
            'email1' => 'support@powwownow.com',
            'url' => 'http://www.powwownow.co.uk',
            'note' => "It’s cheaper to call 87373 from your mobile. Dial from your address book and you won’t even need to enter your PIN, which is $pin in case you need to give it to anyone."
        );
        $vCard = new PwnVCardGenerator($data);
        $vCard->download();
        return sfView::NONE;
    }

    /**
     * @desc
     *  Get 5 Star rating trust pilot reviews.
     * @return array
     */
    private function getTrustPilot5StartReviews()
    {
        $trustPilotReviews = false;
        try {
            $trustPilot = TrustPilot::getFeed(true);
        } catch (Exception $e) {
            $this->logMessage('TrustPilot::getFeed Failed in Mobile/getTrustPilot5StartReviews');
        }
        if (is_object($trustPilot)) {
            $trustPilotReviews = $trustPilot->Reviews;
        }
        if (is_array($trustPilotReviews)) {
            $reviews = array();

            foreach ($trustPilotReviews as $trustPilotReview) {
                if ($trustPilotReview->TrustScore->Stars === 5) {
                    $reviews[] =$trustPilotReview;
                }
            }
            return $reviews;
        }
        return array();
    }

    /**
     * @desc
     *  Check if contact_ref is set, if not redirect user
     *  back to Mobile page.
     * @return Redirect
     */
    private function guardUnAuthenticatedRedirect()
    {
        $user = $this->getUser();
        if ($user->getAttribute('contact_ref') === null) {
            return $this->redirect('@mobile_main_page');
        }
    }

    /**
     * @desc
     *  DialinNumbers helper wrapper.
     * @return array
     */
    private function getDialInNumbers()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers'));
        $user = $this->getUser();

        return $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $user->getCulture(),
                'service_ref'    => $user->getAttribute('service_ref'),
                'country'        => 'GBR',
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
    }
}
