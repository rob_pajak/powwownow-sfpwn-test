<div id="upper-content-container">
    <h2 class="rockwell white">Thank You for Registering!</h2>
    <p>We will be sending you an email with details on getting started, but why not try it out now? Your PIN and voice conference dial-in number are listed here. Simply download and install the Powwownow Web application and share your PIN and the website with your participants.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <ul class="chevron">
        <li class="font-big">Your unique PIN is: <span style="font-size: 16px;"><strong><?php echo $pin; ?></strong></span></li>
    </ul>
    <ul class="chevron">
        <li class="font-big">Conference calling number: <span style="font-size: 16px;"><strong><?php echo $dialInNumber; ?></strong></span></li>
    </ul>
    <strong></strong>
    <ul class="chevron">
        <li class="font-big">Link to share with participants: <span style="font-size: 16px;"><strong><a class="white" title="http://powwownow.yuuguu.com/" href="http://powwownow.yuuguu.com/">powwownow.yuuguu.com</a></strong></span></li>
    </ul>
    <p>&nbsp;</p>
    <p style="text-align: right;">
        <button class="button-orange web-conferencing-download-button" onclick="dataLayer.push({'event': 'Web-Conferencing - Download/onClick'}); window.location.href='/mypwn/myshowtime';">
            <span>Download</span>
        </button>
    </p>
</div>