<?php

$subject = 'Powwownow Conference Call Details';

$body = "
Please join my conference call.

Your dial-in number is:

$dialInNumber

(International Dial-in Numbers http://www.powwownow.co.uk/sfpdf/en/Powwownow-Dial-in-Numbers.pdf)

When dialling-in to conference calls using a mobile phone, use our mobile shortcode to help save you money: Dial 87373 and enter your conference PIN as usual to join your conference.

Your PIN is: $pin

Time of call:

Instructions:
1  Dial the conference number at the time indicated above
2. Enter the PIN, when prompted
3. Speak your full name when prompted and then you will join the conference. If you are the first person to arrive on the conference call, you will hear music. As others arrive on the call you will hear them being announced.

Find out how much easier your life could be with Powwownow's free conference calling service. Powwownow - get together whenever!

http://www.powwownow.co.uk
";

$mailtoLink = "mailto:?subject=" . rawurlencode($subject) . "&body=" . rawurlencode($body);
$classes = isset($classes) ? $classes : '';
$trackingCode = (!empty($tracking)) ? ' ' . $sf_data->getRaw('tracking') : '';
?>
<button type="submit" class="button-orange" id="share-details-button" style="margin-top: 5px">
    <span>SHARE DETAILS</span>
</button>
<script type="text/javascript">
$( "#share-details-button" ).click(function() {
    parent.location='<?php echo $mailtoLink; ?>'; <?php echo $trackingCode; ?>
});
</script>