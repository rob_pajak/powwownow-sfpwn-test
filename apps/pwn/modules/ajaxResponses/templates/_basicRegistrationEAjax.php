<?php use_helper('dialInNumbers'); ?>
<link rel="stylesheet" type="text/css" media="screen" href="/sfcss/pages/homepageAjax.css">
<h1 class="tab-h1">You are ready to start conference calling</h1>
<div class="green-rounded-box">
    <h2 class="rockwell white">Start your conference call now...</h2>
    <div class="subheading2">These are your key details for every time you need to conference call:</div>
    <div class="grid_sub_11" style="position: relative;">
        <div class="sprite-new sprite-phone-white floatleft png" style="position:relative; top:-6px;"></div>
        <div class="floatleft">Your UK dial-in number is:</div>
        <p style="font-size:29px; clear:both;" id='your-dial-in-number'><?php echo $dnis_local; //$mobileNumber = $dialInNumbers['mobile'];?></p>
    </div>
    <div class="grid_sub_7" style="position: relative;">
        <span class="rockwell" style="font-size: 18px;">***</span> Your PIN is:
        <div class="pin-container pin-container-response png" id="generated_pin" style="margin-top: 15px;"><?php echo $pin; ?></div>
    </div>
    <div class="grid_sub_6" style="height:100px; position:relative; padding-top: 42px; padding-left: 27px; z-index:2000">
        <div class="tooltip" style="height: 80px; position: absolute; top: -70px; width: 155px; left: 0px; padding: 10px; color: #515151">
            <div style="background: url('/sfimages/sprites/sprite-tooltip.png') no-repeat scroll -29px top transparent; height: 11px;  position: absolute; top: 100px; left: 30px; width: 29px;"></div>
            <p style="font-size: 10px; line-height: 110%; margin: 0; padding: 0 0 10px 0">In a hurry? You can start your first conference call right now.</p>
            <p style="font-size: 10px; line-height: 110%; margin: 0; padding: 0">Just share the dial-in number and PIN with the other callers and you're off!</p>
        </div>
        <?php include_component('ajaxResponses', 'shareMyPinLink',array(
            'linkContent' => '<button id="share-details" class="button-orange margin-top-10" type="submit"><span>SHARE DETAILS</span></button>',
            'pin' => $pin
        )); ?>
    </div>
</div>

<div class="pwn-november-option" style="display: block;">
    <div class="divider-container" style="top:0">
        <div class="divider-top-green"></div>
        <div class="triangle-bottom-green"></div>
        <div class="divider-bottom-green"></div>
    </div>

    <div class="clear"></div>

    <div class="reg-step-2 clearfix">
        <h2 class="rockwell white" style="margin-top: 45px;">Complete your free registration</h2>
        <div style="position:relative; color: #FFFFFF; width: 50%; float: left">
            <form enctype="application/x-www-form-urlencoded" id="form-create-log-in" method="post" action="<?php echo url_for("@full_registration_successE"); ?>" name="form-create-log-in" style="background: transparent">
                <input type="hidden" name="email" value="<?php echo $sf_data->getRaw('email'); ?>"/>
                <input type="hidden" name="contact_ref" value="<?php echo $sf_data->getRaw('contact_ref'); ?>"/>
                <input type="hidden" name="source" value="<?php echo $sf_data->getRaw('source'); ?>"/>
                <div class="margin-top-20">
                    <label for="fullRegistration_first_name">First Name *</label><br/>
                    <div class="fields">
                        <span class="mypwn-input-container">
                            <input type="text" name="fullRegistration[first_name]" class="mypwn-input input-large font-small required" autocomplete="off" maxlength="30" id="fullRegistration_first_name">
                        </span>
                    </div>

                    <label for="fullRegistration_last_name">Last Name *</label><br/>
                    <div class="margin-bottom-10">
                        <span class="mypwn-input-container">
                            <input type="text" name="fullRegistration[last_name]" class="mypwn-input input-large font-small required" autocomplete="off" maxlength="30" id="fullRegistration_last_name">
                        </span>
                    </div>

                    <label for="fullRegistration_password">Password *</label><br/>
                    <div class="margin-bottom-10">
                        <span class="mypwn-input-container">
                            <input type="password" name="fullRegistration[password]" class="mypwn-input input-large font-small required" autocomplete="off" id="fullRegistration_password">
                        </span>
                        <br/><i>(minimum 6 characters)</i>
                    </div>

                    <label for="fullRegistration_confirm_password">Confirm Password *</label><br/>
                    <div class="margin-bottom-10">
                        <span class="mypwn-input-container">
                            <input type="password" name="fullRegistration[confirm_password]" class="mypwn-input input-large font-small required" autocomplete="off" id="fullRegistration_confirm_password">
                        </span>
                    </div>

                    <div class="margin-bottom-10">
                        Is this the first conference you've ever held?<br/>
                        <div class="fullRegistration_question_one">
                            <input type="radio" name="fullRegistration[question]" value="Yes" id="full_registration_first_yes">Yes</input>&nbsp;&nbsp;&nbsp;<input type="radio" name="fullRegistration[question]" value="No" id="full_registration_first_no">No</input>
                        </div>
                    </div>
                    <div class="fullRegistration_question_two_a">
                        How did you hear about our service (tick all that apply)<br />
                        <div class="fullRegistration_question_two_b">
                            <input type="checkbox" name="fullRegistration[question2][]" value="radio" id="full_registration_second_radio">Radio</input>
                            <input type="checkbox" name="fullRegistration[question2][]" value="train_station" id="full_registration_second_train">Train station</input>
                            <input type="checkbox" name="fullRegistration[question2][]" value="taxi" id="full_registration_second_taxi">Taxi</input><br/>
                            <input type="checkbox" name="fullRegistration[question2][]" value="outdoor_billboard" id="full_registration_second_billboard">Outdoor billboard</input>
                            <input type="checkbox" name="fullRegistration[question2][]" value="in_print" id="full_registration_second_print">In print</input>
                            <input type="checkbox" name="fullRegistration[question2][]" value="tube" id="full_registration_second_tube">On the tube</input><br/>
                            <input type="checkbox" name="fullRegistration[question2][]" value="online_display" id="full_registration_second_online">Online display</input>
                            <input type="checkbox" name="fullRegistration[question2][]" value="search_engine" id="full_registration_second_search">Google (or other)</input>
                            <input type="checkbox" name="fullRegistration[question2][]" value="other" id="full_registration_second_other">Other</input>
                        </div>
                    </div>
                </div>

                <div class="margin-top-10">
                    <button class="button-orange" type="submit" id="create-account-btn"><span>CREATE ACCOUNT</span></button>
                    <div>It's free to open your account and there's no commitment whatsoever.</div>
                    <div class="clear"><!--Blank--></div>
                </div>
            </form>
        </div>

        <div style="position:relative; color: #FFFFFF; width: 50%; float: left; margin-top: 20px;">
            <div style="height: 320px; background: url('/sfimages/step2-bg.png') no-repeat; padding: 20px 40px 0 20px; color: #FFFFFF">
                <h3 class="rockwell">Get all this free:</h3>
                <ul style="list-style: none; padding: 0; margin: 20px 0 20px 0; position: relative; z-index: 600;">
                <li style="padding: 0; margin: 0 0 10px 0;">
                <span class="icon icon-blue" style="float:left; margin: 0 10px 10px 0">
                <span class="icon-arrow-white png"></span>
                </span>
                <b>Web conferencing</b><br>
                <span style="color: #515151">Share your screen with the participants on your call</span>
                </li>
                <li style="padding: 0; margin: 0 0 10px 0;">
                <span class="icon icon-blue" style="float:left; margin: 0 10px 10px 0">
                <span class="icon-speaker-white png"></span>
                </span>
                <b>Recordings</b><br>
                <span style="color: #515151">Record, download and share your conference calls</span>
                </li>
                <li style="padding: 0; margin: 0 0 10px 0;">
                <span class="icon icon-blue" style="float:left; margin: 0 10px 10px 0">
                <span class="icon-calender-white png"></span>
                </span>
                <b>Scheduler</b><br>
                <span style="color: #515151">Organise your conference calls and invite participants</span>
                </li>
                <li style="padding: 0; margin: 0 0 10px 0;">
                <span class="icon icon-blue" style="float:left; margin: 0 10px 10px 0">
                <span class="icon-wallet-card-white png"></span>
                </span>
                <b>Wallet card</b><br>
                <span style="color: #515151">Get a handy reminder of your dial-in number and PIN</span>
                </li>
                </ul>
            </div>
            <span>* Mandatory Fields</span>
        </div>
    </div>
    <script type="text/javascript">pwnApp.homepageE.FullRegistration();</script>
</div>
