<h2 class="rockwell white">Thank you!</h2>
<div class="subheading2">Get started with your conference call now:</div>
<div class="grid_sub_11" style="position: relative;">
  <div class="sprite-new sprite-phone-white floatleft png" style="position:relative; top:-6px;"></div> 
  <div class="floatleft">Your dial-in number is:</div>
  <p style="font-size:29px; clear:both;"id='your-dial-in-number'><?php echo $dnis_local;?></p> 
</div>
<div class="grid_sub_7" style="position: relative;"> 
  <span class="rockwell" style="font-size: 18px;">***</span>Your PIN is: 
  <div class="pin-container pin-container-response png" id="generated_pin" style="margin-top: 15px;"><?php echo $pin; ?></div>
</div>
<div class="grid_sub_6" style="margin-left:5px; height:100px; position:relative;"> 
  <div class="sprite-new sprite-new-people-white floatleft png" style="margin-bottom:50px; margin-right:10px;"></div>
  Share these details with your participants and you're off!
  <?php include_component('ajaxResponses', 'shareMyPinLink',array(
    'linkContent' => '<button id="share-details" class="button-orange" style="margin-top: 10px" type="submit"><span>SHARE DETAILS</span></button>',
    'pin' => $pin,
    'tracking' => "dataLayer.push({'event': 'Landing Page PIN Ajax - Share Details/onClick', 'page': 'Conference-Call'})",
  )); ?>
</div>
<div class="divider-container">
  <div class="divider-top-green"></div>
  <div class="triangle-bottom-green"></div>
  <div class="divider-bottom-green"></div>
</div>
<div class="clear"></div>
<div class="grid_sub_24" style="margin-top:40px; position:relative;">
  <h2 class="rockwell white">What do you want to do now?</h2>
  <ul id="do-now-list">
    <li class="png">
      <span class="icon icon-grey no-shadow png">
        <span class="icon-outlook-white"></span>
      </span>
      <a class="white" onClick='javascript:$("input#email_address").val("<?php echo $email; ?>"); $("#home-outlook-plugin-link").click();' style="cursor: pointer">Schedule through Outlook</a>
    </li>
    <li class="png">
      <span class="icon icon-grey no-shadow png">
        <span class="icon-phone-small-white"></span>
      </span>
      <a href="<?php echo url_for("@create_a_login"); ?>" class="white" target="_self">Manage my call settings</a>
    </li>
    <li class="png" style="width: 222px;">
      <span class="icon icon-grey no-shadow png">
        <span class="icon-wallet-card-white"></span>
      </span>
      <a href="<?php echo url_for("@create_a_login"); ?>" class="white" target="_self">Request a wallet card</a>
    </li>
    <li class="png">
      <span class="icon icon-grey no-shadow png">
        <span class="icon-calender-white"></span>
      </span>
      <a href="<?php echo url_for("@create_a_login"); ?>?d=0" class="white" target="_self">Scheduler tool</a>
    </li>
    <li class="png">
      <span class="icon icon-grey no-shadow png">
        <span class="icon-arrow-white"></span>
      </span>
      <a href="<?php echo url_for("@create_a_login"); ?>?d=1" class="white" target="_self">Hold a web conference</a>
    </li>
    <li class="png" style="width: 222px;">
      <span class="icon icon-grey no-shadow png">
        <span class="icon-globe-white"></span>
      </span>
      <a href="<?php echo url_for("@create_a_login"); ?>?d=3" class="white" target="_self">Discover international access</a>
    </li>
  </ul>
</div>
