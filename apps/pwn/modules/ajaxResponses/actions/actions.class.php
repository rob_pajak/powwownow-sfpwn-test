<?php

/**
 * ajaxResponses actions.
 *
 * @package    powwownow
 * @subpackage ajaxResponses
 * @author     Robert
 */
class ajaxResponsesActions extends sfActions
{
    /**
     * @param sfWebRequest $request
     * @return string
     */
    public function executeFreePinRegistration(sfWebRequest $request) {
        $logPath = sfConfig::get('sf_log_dir').'/registrations.log';
        $registrationLogger = new sfFileLogger(new sfEventDispatcher(), array('file' => $logPath));
        $registrationLogger->setLogLevel(7); // (Debug). 

        $registrationLogger->err("Registration Attempt: ");
        $registrationLogger->err("  POSTed vars: ". serialize($_POST));

        $this->setLayout(false);
        $this->getResponse()->setContentType('application/json');

        // Assert AJAX request.
        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            $registrationLogger->err("  not a Ajax request or not Posted - aborting");
            $this->forward404();
        }

        // Load Helpers
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Partial', 'dialInNumbers', 'Url'));

        $registrationSource = $request->getPostParameter('registration_source', 'undefined');
        $registrationType = $request->getPostParameter('registration_type', 'hp');

        $email = $request->getPostParameter('email', null);
        $tick  = ('agreed' == $request->getPostParameter('agree_tick', false));

        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";
        $locale = (isset($_SERVER['locale'])) ? $_SERVER['locale'] : "en_GB";
        $javascript = array();

        $redirectUrl = '';
        $cx2Experience = false;
        $pinRef = '';

        if ($registrationSource == 'GBR-HomepageST-Top')
        {
            //Split mechanism for Free-Conference-Pin-1 registration
            $currentVariant = apc_fetch('cx2_variant');
            
            if ($currentVariant === false) 
            {
                $currentVariant = 0;
            }

            $croVar = $currentVariant % 2;

            switch ($croVar) {
                case 0:
                  break;
                case 1:
                  $registrationSource .= '_H1';
                  $redirectUrl = url_for('@cx2_post_registration_version_h1', true);
                  $cx2Experience = true;
                  break;
                /*
                case 2:
                  $registrationSource .= '_H2';
                  $redirectUrl = url_for('@cx2_post_registration_version_h2', true);
                  $cx2Experience = true;
                  break;
                */
            }
            apc_store('cx2_variant', ++$currentVariant);
        }

        /**
         * @todo
         *  We are currently use the registration source to determine which
         *  type of registration experience will be used. We need to change this!
         */
        switch ($registrationSource) {
            case 'GBR-Free-Conference-Call-Service-Top':
            case 'GBR-Public-Recordings':
            case 'GBR-Mobile':
            case 'GBR-CX-Header-Registration':
                $redirectUrl = url_for('@cx2_post_registration_version_h1', true);
                $cx2Experience = true;
            break;
        }

        if ($cx2Experience === false) {

            // Perform Registration and Return Error or Success
            $result = Common::performFreePinRegistration($email, $tick, $registrationSource, $locale);

            if (!empty($result) && isset($result['error_messages'])) {
                $this->getResponse()->setStatusCode(400);
                // There are some pages that need the Error Message Replaced (EG. Its Your Call Page)
                if ($registrationType == 'iyc' || $registrationType === 'gid') {
                    switch($result['error_messages']['message']) {
                        case 'API_RESPONSE_EMAIL_ALREADY_TAKEN':
                            $result['error_messages']['message'] = 'API_RESPONSE_EMAIL_ALREADY_TAKEN_RESPONSIVE_OTHER';
                            break;
                        case 'API_RESPONSE_EMAIL_ALREADY_TAKEN_OTHER':
                            $result['error_messages']['message'] = 'API_RESPONSE_EMAIL_ALREADY_TAKEN_RESPONSIVE_OTHER';
                            break;
                        }
                }
    
                $registrationLogger->err("  attempt seems to be not processed  !! ");
                $registrationLogger->err("    result from  Common::performFreePinRegistration  ". serialize($result));
    
                echo json_encode($result);
                return sfView::NONE;
            }
    
            // add some values to session for IYC registration type
            if ($registrationType === 'iyc' || $registrationType === 'gid') {
                $this->getUser()->setAttribute('email',$request->getPostParameter('email'));
                $this->getUser()->setAttribute('contact_ref',$result['contact_ref']);
                $this->getUser()->setAttribute('pin',$result['pin']);
                $this->getUser()->setAttribute('pin_ref',$result['pin_ref']);
                $this->getUser()->setAttribute('registration_type',$registrationType);
            }

            $pinRef = $result['pin_ref'];

        }
        else
        {

            $registrationDetails = Common::performUnitedRegistration($email, $registrationSource);

            //dirty hackS as this method returns json_encoded array instead of throwing an exception
            if (isset($registrationDetails['error_messages'])) {
                $this->getResponse()->setStatusCode(400);
                echo json_encode($registrationDetails); 
                return sfView::NONE;
            }

            try {
                Plus_Authenticate::logIn($email, $registrationDetails['password']);
            } 
            catch (Exception $e) 
            {
                $this->logMessage('Exception occurred when authenticating contact: '. $email . ' ' . $e->getError(), 'err');
                $this->getResponse()->setStatusCode(400);

                echo json_encode(
                    array('error_messages' => array(array('message' => 'FORM_RESPONSE_LOG_IN_UNABLE', 'field_name' => 'email'))));

                return sfView::NONE;
            }

            $result = $registrationDetails;
            $pinRef = $result['pin_ref'];
        }

        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $locale,
                'service_ref'    => 801,
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373',
            )
        );

        // Shomei API And PinRefHash
        $shomei        = new Shomei();
        $shomeiResult  = $shomei->registerFreePin($pinRef);

        echo json_encode(
            array(
                'html'            => array(
                    'homepageHTML'    => ($registrationType == 'hp' && $cx2Experience === false) ?
                            get_partial(
                                'ajaxResponses/basicRegistrationEAjax',
                                array(
                                    'pin'         => $result['pin'],
                                    'email'       => $email,
                                    'contact_ref' => $result['contact_ref'],
                                    'source'      => $registrationSource,
                                    'dnis_local'  => $dialInNumbers['local'],
                                )
                            ) : false,
                    'landingpageHTML' => ($registrationType == 'lp' && $cx2Experience === false) ?
                            get_partial(
                                'ajaxResponses/landingPageResponseAjax',
                                array(
                                    'pin'         => $result['pin'],
                                    'email'       => $email,
                                    'contact_ref' => $result['contact_ref'],
                                    'source'      => $registrationSource,
                                    'dnis_local'  => $dialInNumbers['local'],
                                )
                            ) : false,
                    'iycHTML'         => ($registrationType == 'iyc' && $cx2Experience === false) ?
                            '<script type="text/javascript">window.location.assign("' . url_for(
                                "@its_your_call_step_a"
                            ) . '");</script>'
                            : false,
                ),
                'success_message' => 'Registered',
                'error_messages'  => array(),
                'javascript'      => $javascript,
                'pin'             => $result['pin'],
                'pinRefHash'      => $shomeiResult['apiResult']['responsiveBody']['transactionIdentifier'],
                'redirect_url'    => $redirectUrl
            )
        );

        $registrationLogger->err("  attempt seems to be successful !! ");

        return sfView::NONE;

    }

    /**
     * Homepage E Full Registration. Used after the Basic Registration has been completed
     * @author Asfer, Robert
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeRegistrationSecondStepEAjax(sfWebRequest $request) {

        // Set Output Format
        $this->getResponse()->setContentType('application/json');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Partial', 'dialInNumbers'));

        // Form Validation
        // @todo Move to Symfony Forms
        $form = $request->getPostParameter('fullRegistration');
        $errors = array();

        if ($form['first_name'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_NO_FIRST_NAME', 'field_name' => 'fullRegistration["first_name"]');
        } elseif ($form['last_name'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_NO_SURNAME', 'field_name' => 'fullRegistration["last_name"]');
        } elseif ($form['password'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_NO_PASSWORD', 'field_name' => 'fullRegistration["password"]');
        } elseif (strlen($form['password']) < 6) {
            $errors = array('error_message' => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS', 'field_name' => 'fullRegistration["password"]');
        } elseif ($form['password'] != $form['confirm_password']) {
            $errors = array('error_message' => 'FORM_VALIDATION_PASSWORD_MISMATCH', 'field_name' => 'fullRegistration["confirm_password"]');
        }

        if (!empty($errors)) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error_messages' => $errors));
            return sfView::NONE;
        }

        $arguments = $form;

        // Update the Contact to create a Normal Powwownow Account
        if (!empty($arguments['password'])) {

            $result = Common::doCreateOrUpdateAccount(array(
                'email'      => $request->getPostParameter('email'),
                'first_name' => $arguments['first_name'],
                'last_name'  => $arguments['last_name'],
                'password'   => $arguments['password'],
                'source'     => $request->getPostParameter('email', 'undefined'),
                'locale'     => (isset($_SERVER['locale'])) ? $_SERVER['locale'] : "en_GB",
            ));

            // Check if there was an Error from the above Hermes Call
            if (isset($result['error'])) {
                $this->getResponse()->setStatusCode(500);
                if ('email' == $result['error']['field_name']) {
                    $result['error']['field_name'] = 'alert';
                }

                $this->logMessage('doCreateOrUpdateAccount Hermes Error.' . print_r($result['error'],true), 'err');
                echo json_encode(array('error_messages' => $result['error']));
                return sfView::NONE;
            }
        }

        // Check the Optional Question
        if (!empty($arguments['question'])) {
            try {
                $response = Hermes_Client_Rest::call('saveRegistrationAnswer', array(
                    'question_id' => '1',
                    'answer' => $arguments['question'],
                    'contact_ref' => $request->getPostParameter('contact_ref', -1),
                ));
            } catch (Exception $e) {
                $this->logMessage('saveRegistrationAnswer Error Occurred: ' . $e->getMessage() . ', skipping', 'err');
            }
        }

        if (!empty($arguments['question2']) && is_array($arguments['question2']) && count($arguments['question2']) > 1)  {
            try {
                $response = Hermes_Client_Rest::call('saveRegistrationAnswer', array(
                    'question_id' => '2',
                    'answer' => implode($arguments['question2'], ','),
                    'contact_ref' => $request->getPostParameter('contact_ref', -1),
                ));
            } catch (Exception $e) {
                $this->logMessage('saveRegistrationAnswer Error Occurred: ' . $e->getMessage() . ', skipping', 'err');
            }
        }

        // Authenticate the User, by logging them in.
        try {
            $loginResponse = Plus_Authenticate::logIn($request->getPostParameter('email'), $arguments['password']);
        } catch (Exception $e) {
            $this->logMessage('Exception while authenticating newly created user (completing second step of free registration): ' . $e->getMessage(), 'err');
            $loginResponse = false;
        }

        // Obtain the PIN from the Contact Ref
        try {
            $pins = Hermes_Client_Rest::call('getContactPins', array('contact_ref' => $this->getUser()->getAttribute('contact_ref')));
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->logMessage("getContactPins Hermes call has timed out.",'err');
        } catch (Hermes_Client_Exception $e) {
            $this->logMessage("getContactPins Hermes call is faulty. Error message: " . $e->getMessage() . ', Error Code: ' . $e->getCode(),'err');
        } catch (Exception $e) {
            $this->logMessage("getContactPins Failed. Error Message: " . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode(),'err');
        }

        $user_pin = (count($pins) === 1) ?  $pins[0]['pin'] : 123456;


        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";
        $dialInNumber = DialInNumbersHelper::getLocalDialInNumbers(array(
            'locale'         => $this->getUser()->getCulture(),
            'service_ref'    => $this->getUser()->getAttribute('service_ref'),
            'country'        => $country,
            'default_local'  => '0844 4 73 73 73',
            'default_mobile' => '87373',
        ));

        echo json_encode(array(
            'html'   =>
                array(
                    'wpHTML' =>
                        get_partial('pages/fullRegistrationEAjax',
                            array(
                                'first_name' => ucfirst($arguments['first_name']),
                                'last_name' => ucfirst($arguments['last_name']),
                                'pin' => $user_pin,
                                'dialInNumber'=> $dialInNumber['local']
                            )
                        ),
                    'loginContainer' =>
                        get_partial('commonComponents/headerLoggedIn',
                            array(
                                'first_name' => $arguments['first_name'],
                                'showButton' => true
                            )
                        )
                ),
            'status' => ($loginResponse == true) ? 'success' : 'false',
        ));
        return sfView::NONE;
    }

    /**
     * Homepage E Request Welcome Pack Registration. Used after the Full Registration has been completed
     * @author Asfer
     * @param sfWebRequest $request
     * @return sfView::NONE
     */
    public function executeRequestWelcomePackEAjax(sfWebRequest $request) {
        // Set Output Format
        $this->getResponse()->setContentType('application/json');
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        // Form Validation
        // @todo Move to Symfony Forms
        $arguments = $request->getPostParameter('welcomePack');
        $errors = array();

        if ($arguments['building'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_NO_ADDRESS', 'field_name' => 'welcomePack["building"]');
        } elseif ($arguments['town'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_NO_TOWN', 'field_name' => 'welcomePack["town"]');
        } elseif ($arguments['postal_code'] == '') {
            $errors = array('error_message' => 'FORM_VALIDATION_INVALID_POSTCODE', 'field_name' => 'welcomePack["postal_code"]');
        }

        if (!empty($errors)) {
            $this->getResponse()->setStatusCode(400);
            echo json_encode(array('error_messages' => $errors));
            return sfView::NONE;
        }

        // Obtain the PIN Ref from the Contact Ref
        try {
            $pin_ref = null;
            $pins = Hermes_Client_Rest::call('getContactPins', array('contact_ref' => $this->getUser()->getAttribute('contact_ref')));
            if (count($pins) == 1) {
                $pin_ref = $pins[0]['pin_ref'];
            }
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage("getContactPins Hermes call has timed out.",'err');
            echo json_encode(array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert')));
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage("getContactPins Hermes call is faulty. Error message: " . $e->getMessage() . ', Error Code: ' . $e->getCode(),'err');
            echo json_encode(array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert')));
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage("getContactPins Failed. Error Message: " . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode(),'err');
            echo json_encode(array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert')));
            return sfView::NONE;
        }

        // Check if there is a Wallet Card Address Set (There should not be, since its a new user (Waste of step?)
        // Get the Existing Wallet Card Address + Create a Wallet Card Address
        try {
            $response = Hermes_Client_Rest::call('getWalletCardAddress',array(
                'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                'pin_ref'     => $pin_ref,
            ));

            if (!empty($response)) {
                $this->getResponse()->setStatusCode(500);
                $this->logMessage('getWalletCardAddress Returned 0 Records', 'err');
                echo json_encode(array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert')));
                return sfView::NONE;
            }
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage('getWalletCardAddress Hermes call has timed out. Contact Ref: ' . $this->getUser()->getAttribute('contract_ref'), 'err');
            echo json_encode(array('error_messages' => array(array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))));
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            // There are no Exceptions in the actual Hermes Method, so show default Error Message
            $this->getResponse()->setStatusCode(500);
            $this->logMessage('getWalletCardAddress Hermes Client Exception. ErrorCode: ' . $e->getCode() . ', Message: ' . $e->getMessage(), 'err');
            echo json_encode(array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert')));
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage('getWalletCardAddress Unknown Error Occurred: ' . $e->getMessage(), 'err');
            echo json_encode(array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert')));
            return sfView::NONE;
        }

        $arguments['address'] = (isset($arguments['building'])) ? $arguments['building'] : '';
        $arguments['address'] .= (isset($arguments['street'])) ? ', ' . $arguments['street'] : '';

        $response = Common::createWalletCardRequest(array(
            // Wallet Card Arguments
            'pin_ref'     => $pin_ref,
            'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
            'company'     => (isset($arguments['company'])) ? $arguments['company'] : '',
            'street'      => (isset($arguments['address'])) ? $arguments['address'] : '',
            'town'        => (isset($arguments['town'])) ? $arguments['town'] : '',
            'county'      => (isset($arguments['county'])) ? $arguments['county'] : '',
            'post_code'   => (isset($arguments['postal_code'])) ? $arguments['postal_code'] : '',
            'country'     => (isset($arguments['country'])) ? $arguments['country'] : '',

            // Additional Arguments
            'isAdmin'     => false,
        ));

        // Check if there was any Errors
        if (count($response['error']) > 0) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(array('error_messages' => $response['error']));
            return sfView::NONE;
        }

        echo json_encode(array(
            'html'   => get_partial('pages/requestWelcomePackEAjax'),
            'status' => 'success'
        ));
        return sfView::NONE;
    }

    /**
     * Web Conferencing [AJAX]
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer
     * @amended Robert
     */
    public function executeWebConferencingAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('dialInNumbers', 'Partial'));

        // Default Local and International Dial-in Number
        $country = (isset($_SERVER['country'])) ? $_SERVER['country'] : "GBR";
        $locale  = (isset($_SERVER['locale'])) ? $_SERVER['locale'] : "en_GB";

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new webConferencingGetStartedForm();
        $form->bind($request->getPostParameter($form->getName()));
        $arguments  = Common::getArguments($request->getPostParameter($form->getName()), array());
        $postFormValidationError = false;

        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(400);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form, 'postFormValidationError' => $postFormValidationError)
            );
        }

        // Ugly Hack because Yuuguu neeeeeds First Name and Last Name
        $tmp = explode('@', $arguments['email']);
        $tmp2 = explode('.', $tmp[0]);

        if (trim($arguments['first_name']) == '') {
            $fname = $tmp2[0];
        } else {
            $fname = $arguments['first_name'];
        }

        if (trim($arguments['last_name']) == '') {
            if (isset($tmp2[1])) {
                $lname = $tmp2[1];
            } else {
                $lname = $tmp2[0];
            }
        } else {
            $lname = $arguments['last_name'];
        }

        // Do the Web Conferencing Registration
        try {
            $result = Hermes_Client_Rest::call(
                'doWebConferencingRegistration',
                array(
                    'first_name'   => $fname,
                    'last_name'    => $lname,
                    'email'        => $arguments['email'],
                    'password'     => $arguments['password'],
                    'how_heard'    => $arguments['how_heard'],
                    'phone'        => '',
                    'organisation' => $arguments['company'],
                    'source'       => $country . '-Web-Conferencing',
                    'locale'       => $locale,
                ),
                'array',
                3600
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->logMessage('doWebConferencingRegistration Hermes call has timed out.', 'err');
            $this->getResponse()->setStatusCode(500);
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            $this->logMessage(
                'doWebConferencingRegistration Hermes call returns error: ' . $e->getMessage() .
                    ', Error Code: ' . $e->getCode(), 'err'
            );

            switch ($e->getCode()) {
                case '001:000:001':
                    $this->getResponse()->setStatusCode(409);
                    $errorArr = array(
                        'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN',
                        'field_name' => 'webConferencing[email]'
                    );
                    break;
                case '001:000:007':

                    $this->getResponse()->setStatusCode(409);
                    $errorArr = array(
                        'message'    => 'FORM_CREATE_USER_EMAIL_TAKEN_BY_INACTIVE_ACCOUNT',
                        'field_name' => 'alert'
                    );
                    break;
                default:
                    $this->getResponse()->setStatusCode(500);
                    $errorArr = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }
            echo json_encode(array('error_messages' => $errorArr));
            return sfView::NONE;
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
            $this->logMessage(
                'doWebConferencingRegistration returns error: ' . $e->getMessage() . ', Error Code: '
                    . $e->getCode(),'err'
            );
            $this->getResponse()->setStatusCode(500);
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        // Powwownow Goal Tracking
        try {
            $goal = 'web_conferencing';

            $this->logMessage(
                'pin_ref: ' . $result['pin']['pin_ref'] . " is about to be stored in internal tracking for goal: .${goal} "
                . __FILE__ . '[' . __LINE__ . ']', 'err'
            );
            PWN_Tracking_GoalReferrers::achieved('web_conferencing', $result['pin']['pin_ref'], true);

        } catch (Exception $e) {
            $this->logMessage(
                'Could not track goal referrers for pin_ref: ' . $result['pin']['pin_ref'] . ' ' . $e->getMessage()
                . __FILE__ . '[' . __LINE__ . ']',
                'err'
            );
        }

        // Authenticate the User, by Logging them in
        try {
            $loginResponse = Plus_Authenticate::logIn($arguments['email'], $arguments['password']);
            $this->logMessage('post Web conferencing User Login call status: ' . print_r($loginResponse, true), 'info');
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->logMessage("post Web conferencing User Login time-out :( " . __FILE__ . '[' . __LINE__ . ']', 'err');
        } catch (Hermes_Client_Exception $e) {
            $this->logMessage(
                'post Web conferencing User Login failure: ' . $e->getMessage() . ', Code: ' . $e->getCode()
                . __FILE__ . '[' . __LINE__ . ']',
                'err'
            );
        } catch (Exception $e) {
            $this->logMessage('post Web conferencing User Login failure: ' . $e->getMessage()
            . __FILE__ . '[' . __LINE__ . ']', 'err');
        }

        // Shomei API And PinRefHash
        $pinRef       = $result['pin']['pin_ref'];
        $shomei       = new Shomei();
        $shomeiResult = $shomei->registerFreePin($pinRef);

        // Local Dial In Numbers (Both Shared Cost and Mobile)
        $dialInNumbers = DialInNumbersHelper::getLocalDialInNumbers(
            array(
                'locale'         => $locale,
                'service_ref'    => $this->getUser()->getAttribute('service_ref'),
                'country'        => $country,
                'default_local'  => '0844 4 73 73 73',
                'default_mobile' => '87373'
            )
        );
        $dialInNumber  = $dialInNumbers['local'];

        // Return Result
        echo json_encode(
            array(
                'mainHtml'  => get_partial(
                    'ajaxResponses/webConferencingMainAjax',
                    array(
                        'pin'          => $result['pin']['pin'],
                        'dialInNumber' => $dialInNumber
                    )
                ),
                'pin'        => $result['pin']['pin'],
                'pinRefHash' => $shomeiResult['apiResult']['responsiveBody']['transactionIdentifier']
            )
        );
        return sfView::NONE;
    }

    /**
     * Im-Interested [Ajax]
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Alexander Farrow
     * @modified Asfer Tamimi
     *
     */
    public function executeImInterestedAjax(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new callBackForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments = Common::getArguments($request->getParameter($form->getName(), array()), array());
        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(500);
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('form' => $form)
            );
        }
        // Send Contact Us Email
        $response = Common::sendImInterestedEmail(
            array(
                'first_name'     => $arguments['first_name'],
                'last_name'      => $arguments['last_name'],
                'email'          => $arguments['email'],
                'contact_number' => $arguments['contact_number'],
                'comments'       => $arguments['comments'],
                'utm_campaign'   => $arguments['utm_campaign'],
            ),
            $_SERVER['SERVER_NAME']
        );

        // Check if there was an Error from the above email
        if (!$response) {
            $this->getResponse()->setStatusCode(500);
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))
            );
            return sfView::NONE;
        }

        // Success Response
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
        echo json_encode(array('success_message' => get_partial('pages/imInterestedSuccessMessage', array())));
        return sfView::NONE;
    }

    /**
     * Call-Settings [AJAX]
     * @todo Move PinHelper Methods and multi updatePin into Hermes to save time.
     *
     * @param sfWebRequest $request
     * @return sfView::NONE
     * @author Asfer Tamimi
     *
     */
    public function executeCallSettingsAjax(sfWebRequest $request)
    {
        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            $this->logMessage("User Submitted a Non Ajax Request Or it was not Posted. Forwarding to 404", 'err');
            $this->forward404();
        }

        $response = $this->getResponse();
        $response->setContentType('application/json');
        $response->setStatusCode(400);
        $users = $this->getUser();
        $errorMessages = array();
        $pinRefs = array('success' => array(), 'failed' => array());
        $userType = strtoupper($users->getAttribute('user_type'));
        $contactRef = $users->getAttribute('contact_ref');

        $form = new callSettingsForm();
        $arguments = $request->getPostParameter($form->getName());
        $form->bind($arguments);
        if (!$form->isValid()) {
            $error = $form->getFormErrors();
            if ($error) {
                if ($error['error_messages']['field_name'] === 'callSettings[pin_ref]') {
                    $error['error_messages']['field_name'] = 'alert';
                }
                echo json_encode($error);
                return sfView::NONE;
            }
        }

        $pinInformation = PinHelper::getPinInformation($userType, $contactRef, $arguments['pin_ref'], true);

        // Check if there are any Editable PIN Refs
        if (empty($pinInformation['editingRefs'])) {
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => 'FORM_VALIDATION_NO_PERMISSION_TO_EDIT_PIN',
                        'field_name' => 'alert'
                    )
                )
            );
            return sfView::NONE;
        }

        // Change the Call Settings and Update the Successful and UnSuccessful PIN_Refs Editable Arrays
        foreach ($pinInformation['editingRefs'] as $pinRef) {
            $args = array(
                'pin_ref'                      => $pinRef,
                'language_code'                => $arguments['voice_prompt_language'],
                'moh_prompt_ref'               => $arguments['on_hold_music'],
                'entry_exit_announcement'      => $arguments['entry_exit_announcements'],
                'entry_exit_announcement_type' => $arguments['announcement_types'],
                'play_participant_count'       => $arguments['play_participant_count'],
            );

            if ($users->hasCredential('PLUS_USER') || $users->hasCredential('PREMIUM_USER')) {
                $args['chairman_present'] = $arguments['chairman_present'];
                $args['description']      = $arguments['description'];
                $args['chairman_only']    = $arguments['chairperson_only'];
            } elseif (!$users->hasCredential('POWWOWNOW')) {
                $args['chairman_present'] = $arguments['chairman_present'];
                $args['description']      = $arguments['description'];
                $args['chairman_only']    = $arguments['chairperson_only'];
                $args['username']         = $arguments['cost_code'];
            }

            $result = PinHelper::updatePin($args, $contactRef);
            if (empty($result)) {
                $pinRefs['failed'][] = $pinRef;
            } else {
                $pinRefs['success'][] = $pinRef;
            }
        }

        // Final Response
        if (!empty($pinRefs['success']) && empty($pinRefs['failed'])) {
            $response->setStatusCode(200);
            $finalResponse = array(
                'success_message' => array('message' => 'MYPWN_CALLSETTINGS_SUCCESS', 'field_name' => 'alert'),
                'error_messages'  => $errorMessages
            );
        } elseif (!empty($pinRefs['success']) && !empty($pinRefs['failed'])) {
            $this->logMessage('User Contact Ref: ' . $arguments['contact_ref'] .
                ' tried to Edit PINS: ' . print_r($arguments['pin_ref'], true) .
                ', some updated but some were unable to edit :' . print_r($pinRefs['failed'], true),
                'err'
            );
            $finalResponse = array(
                'error_messages' => array(
                    'message'    => 'FORM_RESPONSE_PINS_UPDATE_SUCCESS_AND_FAILURE',
                    'field_name' => 'alert'
                )
            );
        } else {
            $this->logMessage('User Contact Ref: ' . $arguments['contact_ref'] .
                ' tried to Edit PINS: ' . print_r($arguments['pin_ref'], true) .
                ', but was unable to edit :' . print_r($pinRefs['failed'], true),
                'err'
            );
            $finalResponse = array(
                'error_messages' => array(
                    'message'    => 'FORM_RESPONSE_PINS_UPDATE_FAILURE',
                    'field_name' => 'alert'
                )
            );
        }

        echo json_encode($finalResponse);
        return sfView::NONE;
    }
}
