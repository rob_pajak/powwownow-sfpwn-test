<?php

class ajaxResponsesComponents extends sfComponents {

    /**
     * Gets a link tag with the share my pin mailto link
     *
     * Set linkContent and pin when calling get_component() to set the relevant details
     *
     * @author Wiseman
     */
    public function executeShareMyPinLink(sfWebRequest $request) {
        $this->dialInNumber = Common::getDefaultDialInNumber(801, $this->getUser()->getCulture());
    }


}
