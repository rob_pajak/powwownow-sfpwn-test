<section class="quiz-section <?php echo $device?>" data-attr-device="<?php echo $device?>">
    <div class="large-12 columns">
        <div class="row">
            <div class="large-11 large-offset-1 columns">
                <h2 class="quiz-title">Choose your own on-hold music</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <section class="quiz-asset-section <?php echo $device?>" data-attr-device="<?php echo $device?>">
                <div id="quiz.asset.section.container" data-bind="template: {name: 'quiz.asset.section.data', afterRender: initialize }"></div>
                <script type="text/html" id="quiz.asset.section.data">
                        <div id="quiz-asset" class="quiz-asset">
                            <!-- Introduction Slide -->
                            <div class="quiz-asset-slide quiz-slide-introduction">
                                <div class="row">
                                    <div class="large-6 medium-6  small-6 columns">
                                        <h2 class="quiz-asset-title">Find Your Favourite Tune...</h2>
                                        <p class="quiz-section-text">
                                            Whether you're delivering bad news, having a creative brainstorm or have totally
                                            forgotten about that conference call you booked in month ago.. take our quick
                                            and simple quiz to find the right music for your call.
                                        </p>
                                        <p class="text-center quiz-button-container">
                                            <button data-bind="click: startQuiz" class="custom-button-blue">Start the Quiz</button>
                                        </p>
                                    </div>
                                    <div class="large-4 medium-4 small-4 columns end">
                                        <div id="ri-grid" class="rotator ri-shadow">
                                            <ul>
                                                <?php for ($x=1; $x<=10; $x++):?>
                                                    <li><a><img src="/cx2/img/music.on.hold/album/mobile/<?php echo $x?>.jpg" alt=""/></a></li>
                                                <?php endfor;?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Question One Slide -->
                            <div class="quiz-asset-slide quiz-slide-one">
                                <div class="row">
                                    <div class="large-10 medium-10 small-10 columns">
                                        <div class="quiz-question">
                                            <h2 class="quiz-asset-title">Q1.<span> Which of these best describes how you're feeling?</span></h2>
                                            <div class="quiz-question-answers" data-bind="foreach: questions.one">
                                                <div class="answer">
                                                    <div class="answer-inner-container"
                                                         data-bind="
                                                            'click': $parent.questionOne,
                                                            'event':{
                                                                   'mouseover': $parent.showDescription(description),
                                                                   'mouseout': $parent.hideDescription
                                                               }">
                                                        <div data-bind="'css': cssSpriteClass"></div>
                                                        <p class="answer-description" data-bind="text: answer"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="quiz-answer-description" data-bind="text: questionDescription"></div>
                                        </div>
                                    </div>
                                    <div class="large-2 medium-2 small-2 columns end">
                                        <div class="quiz-stages">
                                            <div class="quiz-stage active">1</div>
                                            <div class="quiz-stage">2</div>
                                            <div class="quiz-stage">3</div>
                                            <div class="quiz-stage">4</div>
                                            <div class="quiz-stage">5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Question Two Slide -->
                            <div class="quiz-asset-slide quiz-slide-two">
                                <div class="row">
                                    <div class="large-10 medium-10 small-10 columns">
                                        <h2 class="quiz-asset-title">Q2.<span> How important is your conference call on a scale of 1-5?</span></h2>
                                        <div class="quiz-question">
                                            <div class="quiz-question-answers" data-bind="foreach: questions.two">
                                                <div class="answer">
                                                    <div class="answer-inner-container"
                                                         data-bind="
                                                            'click': $parent.questionTwo,
                                                            'event':{
                                                                'mouseover': $parent.showDescription(description),
                                                                'mouseout': $parent.hideDescription
                                                            }">
                                                        <div data-bind="'css': cssSpriteClass"></div>
                                                        <p class="answer-description"
                                                           data-bind="
                                                            'text': answer,
                                                            'css': { 'vertical-align-adjust': $index() == 2 }">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="quiz-answer-description" data-bind="text: questionDescription"></div>
                                        </div>
                                    </div>
                                    <div class="large-2 medium-2 small-2 columns end">
                                        <div class="quiz-stages">
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(1)">1</div>
                                            <div class="quiz-stage active">2</div>
                                            <div class="quiz-stage">3</div>
                                            <div class="quiz-stage">4</div>
                                            <div class="quiz-stage">5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Question Three Slide -->
                            <div class="quiz-asset-slide quiz-slide-three">
                                <div class="row">
                                    <div class="large-10 medium-10 small-10 columns">
                                        <h2 class="quiz-asset-title">Q3.<span> What's the subject of the call?</span></h2>
                                        <div class="quiz-question">
                                            <div class="quiz-question-answers" data-bind="foreach: questions.three">
                                                <div class="answer">
                                                    <div class="answer-inner-container"
                                                         data-bind="
                                                            'click': $parent.questionThree,
                                                            'event':{
                                                                'mouseover': $parent.showDescription(description),
                                                                'mouseout': $parent.hideDescription
                                                            }">
                                                        <div data-bind="'css': cssSpriteClass"></div>
                                                        <p class="answer-description"
                                                           data-bind="
                                                            'text': answer,
                                                            'css': {
                                                                'vertical-align-adjust-1': $index() == 1,
                                                                'vertical-align-adjust': $index() == 2
                                                            }">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="quiz-answer-description" data-bind="text: questionDescription"></div>
                                        </div>
                                    </div>
                                    <div class="large-2 medium-2 small-2 columns end">
                                        <div class="quiz-stages">
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(1)">1</div>
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(2)">2</div>
                                            <div class="quiz-stage active">3</div>
                                            <div class="quiz-stage">4</div>
                                            <div class="quiz-stage">5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Question Four Slide -->
                            <div class="quiz-asset-slide quiz-slide-four">
                                <div class="row">
                                    <div class="large-10 medium-10 small-10 columns">
                                        <h2 class="quiz-asset-title">Q4.<span> How prepared do you feel for this call?</span></h2>
                                        <div class="quiz-question">
                                            <div class="quiz-question-answers" data-bind="foreach: questions.four">
                                                <div class="answer">
                                                    <div class="answer-inner-container"
                                                         data-bind="
                                                            'click': $parent.questionFour,
                                                            'event':{
                                                               'mouseover': $parent.showDescription(description),
                                                               'mouseout': $parent.hideDescription
                                                         }">
                                                        <div data-bind="'css': cssSpriteClass"></div>
                                                        <p class="answer-description"
                                                           data-bind="
                                                            'text': answer,
                                                            'css': { 'vertical-align-adjust': $index() == 2 }
                                                           ">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="quiz-answer-description" data-bind="text: questionDescription"></div>
                                        </div>
                                    </div>
                                    <div class="large-2 medium-2 small-2 columns end">
                                        <div class="quiz-stages">
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(1)">1</div>
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(2)">2</div>
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(3)">3</div>
                                            <div class="quiz-stage active">4</div>
                                            <div class="quiz-stage">5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Question Five Slide -->
                            <div class="quiz-asset-slide quiz-slide-five">
                                <div class="row">
                                    <div class="large-10 medium-10 small-10 columns">
                                        <h2 class="quiz-asset-title">Q5.<span>What would you rather be doing on a conference call?</span></h2>
                                        <div class="quiz-question">
                                            <div class="quiz-question-answers" data-bind="foreach: questions.five">
                                                <div class="answer">
                                                    <div class="answer-inner-container"
                                                         data-bind="
                                                            'click': $parent.questionFive,
                                                            'event':{
                                                               'mouseover': $parent.showDescription(description),
                                                               'mouseout': $parent.hideDescription
                                                         }">
                                                        <div data-bind="'css': cssSpriteClass"></div>
                                                        <p class="answer-description"
                                                           data-bind="
                                                            'text': answer,
                                                            'css': {
                                                                'vertical-align-adjust': $index() == 0,
                                                                'vertical-align-adjust-2': $index() == 2,
                                                                'vertical-align-adjust-3': $index() == 3,
                                                                'vertical-align-adjust-4': $index() == 4,
                                                            }
                                                           ">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="quiz-answer-description" data-bind="text: questionDescription"></div>
                                        </div>
                                    </div>
                                    <div class="large-2 medium-2 small-2 columns end">
                                        <div class="quiz-stages">
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(1)">1</div>
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(2)">2</div>
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(3)">3</div>
                                            <div class="quiz-stage active clickable" data-bind="click: goToSlide(4)">4</div>
                                            <div class="quiz-stage active">5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Result Slide -->
                            <div class="quiz-asset-slide quiz-result">
                                <div class="row">
                                    <div class="large-11 medium-10">
                                        <h2 class="quiz-asset-title">Your perfect track is... <span data-bind="text: result().title"></span></h2>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="large-4 medium-4 columns">
                                        <div class="large-2 medium-2 small-2 columns">
                                            <div class="album-art-container-layer" data-bind="'if': result().id != 11, css: result().albumArt">
                                                <div class="album-art-layer">
                                                    <div id="quiz_music_player" class="cp-jplayer"></div>
                                                    <div id="quiz-music-player_container" class="music_catalogue_track_container">
                                                        <ul class="cp-controls">
                                                            <li><a class="cp-play" tabindex="1">play</a></li>
                                                            <li><a class="cp-pause" style="display:none;" tabindex="1">pause</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="large-6 medium-6 columns end">
                                        <p class="quiz-section-text" data-bind="text: result().description"></p>
                                        <p class="quiz-section-text">
                                            Listen to your chosen track and change your on-hold music below.
                                        </p>
                                        <p>
                                            <a href="<?php echo url_for('call_settings'); ?>" data-bind="click: endQuiz" class="custom-button-orange">Use This Track</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-11 medium-11 columns">
                                        <p class="right">
                                            <a data-bind="click: goToSlide(0)"> Start Quiz Again</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- Dummy Slide -->
                            <div class="quiz-asset-slide quiz-slide-five">
                            <div class="row">
                                <div class="large-10 medium-10 small-10 columns">
                                    <h2 class="quiz-asset-title">Q5.<span>What would you rather be doing on a conference call?</span></h2>
                                    <div class="quiz-question">
                                        <div class="quiz-question-answers">
                                            <div class="answer">
                                                <div class="answer-inner-container">
                                                    <p class="answer-description"">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="quiz-answer-description" data-bind="text: questionDescription"></div>
                                    </div>
                                </div>
                                <div class="large-2 medium-2 small-2 columns end">
                                    <div class="quiz-stages">
                                        <div class="quiz-stage active">1</div>
                                        <div class="quiz-stage active">2</div>
                                        <div class="quiz-stage active">3</div>
                                        <div class="quiz-stage active">4</div>
                                        <div class="quiz-stage active">5</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                </script>
            </section>
        </div>
    </div>
</section>
