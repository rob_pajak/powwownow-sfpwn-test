<?php $componentsClass = 'musicOnHoldCampaign'; ?>

<header class="<?php echo $device?>" data-attr-device="<?php echo $device?>">
    <div class="row">
        <section class="large-6 medium-6 small-5 columns">
            <a href="<?php echo url_for('@homepage')?>" target="_blank">
                <img class="logo" id="js_logo" width="200" src="/cx2/img/mobile/640_logo_strap_line.png" alt="Powwownow - Lets Get It Done" />
            </a>
        </section>
        <section class="large-6 medium-6 small-7 columns">
            <div class="navigations">
                <?php if (!$isMobile):?>
                <ul class="navigation">
                    <li>
                        <a href="<?php echo url_for('@homepage');?>" target="_blank">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo url_for('@conference_call');?>" target="_blank">Conference Call</a>
                    </li>
                    <li>
                        <a href="<?php echo url_for('@contact_us');?>" target="_blank">Contact Us</a>
                    </li>
                </ul>
                <?php endif; ?>
                <ul class="navigation social-navigation">
                    <li>
                        <a class="sprite-social-cx cx-twitter" href="https://twitter.com/powwownow" target="_blank"></a>
                    </li>
                    <li>
                        <a class="sprite-social-cx cx-facebook" href="https://www.facebook.com/powwownow" target="_blank"></a>
                    </li>
                    <li>
                        <a class="sprite-social-cx cx-linkedin" href="http://www.linkedin.com/company/powwownow" target="_blank"></a>
                    </li>
                    <li>
                        <a class="sprite-social-cx cx-youtube" href="https://www.youtube.com/user/MyPowwownow" target="_blank"></a>
                    </li>
                    <li class="hide-for-small-only">
                        <a class="sprite-social-cx cx-rss" href="http://www.powwownow.co.uk/blog/" target="_blank"></a>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</header>

<!-- Introduction Section -->
<section class="row introduction-section <?php echo $device?>" data-attr-device="<?php echo $device?>">
    <div class="large-12 columns introduction">
        <div class="row">
            <div class="large-7 large-offset-1 pwn-custom-medium-grid pwn-custom-medium-1-offset small-10 columns">
                <h1 class=introduction-title>Choose your own<br>on-hold music</h1>
            </div>
        </div>
        <div class="row <?php echo ($isMobile) ? 'introduction-mobile' : ''; ?>">
            <div class="large-6 large-offset-1 medium-6 pwn-custom-medium-1-offset columns">
                <p class="introduction-text">When Alfred Levy invented on-hold music in 1966, he couldn't have known
                    that his pleasant little patent would one day devolve into what basically amounts to cruel and
                    unusual punishment for conference callers the world over.
                </p>
                <p class="introduction-text">In the age of mp3 and digital streaming, there's no excuse for half the
                    rubbish that gets pumped out on conference call lines every day. That's why here at Powwownow we
                    let you choose the music that will help set the tone for your call.
                </p>
            </div>
        </div>
    </div>
</section>

<?php if ($isMobile): ?>
    <div class="sound-wave-container <?php echo $device?>">
        <div class="sound-wave-a"></div>
    </div>
<?php endif; ?>

<?php if ($isMobile): ?>
    <?php include_component($componentsClass, 'mobileQuiz'); ?>
<?php elseif (!$isMobile && !$isIE8): ?>
    <?php include_component($componentsClass, 'desktopTabletQuiz'); ?>
<?php else: ?>
    <section>
        <h2 class="section-title">Oh no, this isn’t music to my ears!</h2>
        <p class="section-text">
            Your current browser isn’t supported at present. We are currently working on this issue.
            Please either upgrade your browser to IE9, use a different browser or try again later.
        </p>
    </section>
<?php endif; ?>

<?php if (!$isIE8):?>
<div class="sound-wave-container <?php echo $device?>">
    <div class="sound-wave"></div>
</div>
<?php endif; ?>

<section class="music-catalogue-section <?php echo $device?>" data-attr-device="<?php echo $device?>">

    <?php if ($isIE8):?>
        <div class="row">
            <div class="large-12 columns">
                <div id="jp_container_1" class="jp-container">
                    <div class="jp-type-playlist">
                        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                        <div class="jp-gui">
                            <div class="jp-interface">
                                <ul class="jp-controls">
                                    <li><a tabindex="1" class="jp-stop"></a></li>
                                </ul>
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                                <div class="jp-current-time"></div>
                                <div class="jp-duration"></div>
                            </div>
                        </div>
                        <div class="jp-playlist">
                            <ul>
                                <li></li>
                            </ul>
                        </div>
                        <div class="jp-no-solution">
                            <span>Update Required</span>
                            To play the media you will need to either update your browser to a recent version or update
                            your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php else: ?>
        <div class="row background-color background-grey-k">
            <div class="large-5 large-offset-1 medium-11 pwn-custom-medium-1-offset columns">
                <h2 class="music-catalogue-title">A little of what you like - you play DJ</h2>
            </div>
            <?php if (!$isMobile): ?>
                <div class="large-10 medium-10 medium-centered columns">
                    <div class="music-catalogue-blurb">
                        <p class="music-catalogue-text">
                            Disco? Funk? Hardcore Belgian Trance? Complete silence? A series of ominous beeps? Whatever you're
                            into, we've got it covered.
                        </p>
                        <p class="music-catalogue-text">
                        <p class="music-catalogue-text">
                            Select your favourite track below to help us decide which one should be the new
                            default on-hold music. Getting it done has never sounded so damn good.
                        </p>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($isMobile) : ?>
            <div class="row">
                <div class="large-12 columns">
                    <div id="jp_container_1" class="jp-container">
                        <div class="jp-type-playlist">
                            <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                            <div class="jp-gui">
                                <div class="jp-interface">
                                    <ul class="jp-controls">
                                        <li><a tabindex="1" class="jp-stop"></a></li>
                                    </ul>
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-current-time"></div>
                                    <div class="jp-duration"></div>
                                </div>
                            </div>
                            <div class="jp-playlist">
                                <ul>
                                    <li></li>
                                </ul>
                            </div>
                            <div class="jp-no-solution">
                                <span>Update Required</span>
                                To play the media you will need to either update your browser to a recent version or update
                                your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php elseif (!$isMobile): ?>
            <?php if (isset($musicCatalogues)):?>
                <div class="row background-color background-grey-k">
                    <div class="large-12 medium-12 large-centered columns">
                        <div class="large-1 medium-1 columns not-visible">&nbsp;</div>
                        <?php foreach ($musicCatalogues as $musicCatalogue): ?>
                        <?php if ($musicCatalogue['trackId'] === 6) : ?>
                        <div class="row">
                            <div class="large-12 medium-12 columns">
                                <div class="large-1 medium-1 columns not-visible">&nbsp;</div>
                                <?php endif; ?>
                                <div class="large-2 medium-2 small-2 columns <?php echo ($musicCatalogue['trackId'] === 5 || $musicCatalogue['trackId'] === 10) ? 'end' : $musicCatalogue['trackId'] ;?>">
                                    <div class="album-art-container-layer album-art-<?php echo $musicCatalogue['trackId'] ?>">
                                        <div class="banner <?php echo ($musicCatalogue['isNew']) ? 'blue' : 'green'?>">
                                            <p class="banner-text"><?php echo ($musicCatalogue['isNew']) ? 'New' : 'Old Favourite'?></p>
                                        </div>
                                        <div class="album-art-layer">
                                            <div id="music_catalogue_track_<?php echo $musicCatalogue['trackId']?>" class="cp-jplayer"></div>
                                            <div id="music_catalogue_track_container_<?php echo $musicCatalogue['trackId']?>" class="music_catalogue_track_container">
                                                <div class="cp-buffer-holder">
                                                    <div class="cp-buffer-1"></div>
                                                    <div class="cp-buffer-2"></div>
                                                </div>
                                                <div class="cp-progress-holder">
                                                    <div class="cp-progress-1"></div>
                                                    <div class="cp-progress-2"></div>
                                                </div>
                                                <div class="cp-circle-control"></div>
                                                <ul class="cp-controls">
                                                    <li><a class="cp-play" tabindex="1">play</a></li>
                                                    <li><a class="cp-pause" style="display:none;" tabindex="1">pause</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="album-art-desc-layer">
                                            <p><?php echo $musicCatalogue['trackName']?></p>
                                        </div>
                                        <div class="album-art-favourite-layer js-favourite"
                                             data-track-id="<?php echo $musicCatalogue['trackId']; ?>"
                                             data-track-name="<?php echo $musicCatalogue['trackName']?>">
                                            <p>Favourite
                                                <?php if($musicCatalogue['isUserFavourite']):?>
                                                    <span>&#9733;</span>
                                                <?php else: ?>
                                                    <span>&#9734;</span>
                                                <?php endif; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($musicCatalogue['trackId'] === 10) : ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php endif?>
    <?php endif; ?>
</section>

<section class="registration-section <?php echo $device?>" data-attr-device="<?php echo $device?>">
    <div class="row">
        <div class="large-12 columns background-color background-blue-i">

            <div class="row">
                <div class="large-12 medium-12 small-12 columns existing-customer">
                    <div class="row">
                        <div class="large-11 large-offset-1 medium-12  columns">
                            <h2 class="existing-customer-title">Existing customer? <a href="<?php echo url_for('@call_settings')?>?music=1">Log in</a> to change your on-hold music now!</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="large-6 large-offset-1 medium-12 columns">
                    <h2 class="registration-title">Sign up and set your on-hold music today</h2>
                </div>
            </div>
            <div class="row">
                <div class="large-12 column">
                    <?php include_component(
                        'commonComponents',
                        'jsKoCxRegistrationTemplate',
                        array(
                            'koTemplateId' => 'ko.music-on-hold.registration.container',
                            'koTemplateDataId' => 'ko.music-on-hold.registration.data',
                            'buttonText' => 'Customise Now'
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="on-hold-music-misc-section <?php echo $device?>" data-attr-device="<?php echo $device?>">
    <div class="row">
        <div class="large-12 medium-12 columns">
            <div class="row">
                <div class="large-6 medium-6 columns">
                    <h2 class="on-hold-music-misc-section-title">Music is everywhere</h2>
                    <p class="on-hold-music-misc-section-text"> The days of bulky vinyl and chunky CDs are behind us as
                        the general public embraces MP3s and streaming to satisfy their daily music needs.
                        The expectation of being able to listen to what you want, when you want, has never been higher.
                    </p>
                    <p class="on-hold-music-misc-section-text mobile-float-right"> Sat at your desk, you might be able
                        to ignore the office radio by delving into your personal online music collection.
                        But what about the times you can't escape? What about the dreaded on-hold music of a conference call?
                    </p>
                </div>
                <div class="large-6 medium-6 columns <?php echo ($isMobile) ? 'float-right': '' ?>">
                    <h2 class="on-hold-music-misc-section-title">Choose yours</h2>
                    <p class="on-hold-music-misc-section-text"> Powwownow knows that it's important to go into
                        any meeting with the right mind-set, whether it's a creative brainstorm or an employee review.
                        Unfortunately, after hearing a pan-pipe rendition of Mambo Number Five, you might just find
                        yourself without the right mental attitude.
                    </p>
                    <p class="on-hold-music-misc-section-text"> That's why we offer you the opportunity to pick
                        your own on-hold music. It's your chance to choose something you can enjoy.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
var Tracks = {
    powwowTrack : {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/powwowTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/powwowTrack.mp3',
        oga: '/myPwn/music.on.hold/powwowTrack.ogg',
        m4a: '/myPwn/music.on.hold/powwowTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    porkPieDubTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/porkPieDubTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/porkPieDubTrack.mp3',
        oga: '/myPwn/music.on.hold/porkPieDubTrack.ogg',
        m4a: '/myPwn/music.on.hold/porkPieDubTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    GetOnTheFunkBusTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/GetOnTheFunkBusTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/GetOnTheFunkBusTrack.mp3',
        oga: '/myPwn/music.on.hold/GetOnTheFunkBusTrack.ogg',
        m4a: '/myPwn/music.on.hold/GetOnTheFunkBusTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    CenturyDiscoTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/CenturyDiscoTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/CenturyDiscoTrack.mp3',
        oga: '/myPwn/music.on.hold/CenturyDiscoTrack.ogg',
        m4a: '/myPwn/music.on.hold/CenturyDiscoTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    globalCelebrationTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/globalCelebrationTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/globalCelebrationTrack.mp3',
        oga: '/myPwn/music.on.hold/globalCelebrationTrack.ogg',
        m4a: '/myPwn/music.on.hold/globalCelebrationTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    timeToShineTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/timeToShineTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/timeToShineTrack.mp3',
        oga: '/myPwn/music.on.hold/timeToShineTrack.ogg',
        m4a: '/myPwn/music.on.hold/timeToShineTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    eineKleineNachtTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/eineKleineNachtTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/eineKleineNachtTrack.mp3',
        oga: '/myPwn/music.on.hold/eineKleineNachtTrack.ogg',
        m4a: '/myPwn/music.on.hold/eineKleineNachtTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    hispoaniolaTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/hispoaniolaTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/hispoaniolaTrack.mp3',
        oga: '/myPwn/music.on.hold/hispoaniolaTrack.ogg',
        m4a: '/myPwn/music.on.hold/hispoaniolaTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    calmRiverTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/calmRiverTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/calmRiverTrack.mp3',
        oga: '/myPwn/music.on.hold/calmRiverTrack.ogg',
        m4a: '/myPwn/music.on.hold/calmRiverTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    },
    stringQuartetTrack: {
        <?php if ($isMobile || $isTablet):?>
            <?php if ($isIOS):?>
                m4a: '/myPwn/music.on.hold/stringQuartetTrack.m4a'
            <?php endif;?>
        <?php else: ?>
        m3a: '/myPwn/music.on.hold/stringQuartetTrack.mp3',
        oga: '/myPwn/music.on.hold/stringQuartetTrack.ogg',
        m4a: '/myPwn/music.on.hold/stringQuartetTrack.mp3'<?php /**IE8 and Below uses m4a, but plays a mp3.**/ ?>
        <?php endif; ?>

    }
};

var Quiz = {
    questions: {
        one: [
            {
                id: 1,
                answer: 'Stressed',
                description: 'My temple vein is throbbing, one more thing and I might explode',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-faces-m cx-m-stressed'" : "'sprite-faces cx-stressed'" ?>,
                musicId: 9
            },
            {
                id: 2,
                answer: 'Sleepy',
                description: 'Soooo sleepy, is it home time yet I’m ready for bed',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-faces-m cx-m-sleepy'" : "'sprite-faces cx-sleepy'" ?>,
                musicId: 4
            },
            {
                id: 3,
                answer: 'Indifferent',
                description: 'Yeah so whatever, it\'s just work you know, who cares',
                cssSpriteClass: <?php  echo ($isMobile) ? "'sprite-faces-m cx-m-indifferent'" : "'sprite-faces cx-indifferent'"?>,
                musicId: 8
            },
            {
                id: 4,
                answer: 'Happy',
                description: 'What a lovely day, isn’t it just such a great, fabulous day',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-faces-m cx-m-happy'" : "'sprite-faces cx-happy'" ?>,
                musicId: 3
            },
            {
                id: 5,
                answer: 'Super',
                description:'I’m ready to take on the bad guys, from my office chair that is',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-faces-m cx-m-super'" : "'sprite-faces cx-super'" ?>,
                musicId: 5
            }
        ],
        two: [
            {
                id: 6,
                answer: 'I could take a nap instead',
                description: 'Seriously, no one would even notice I wasn’t there',
                cssSpriteClass: 'sprite-slide-two cx-graph1',
                musicId: 7
            },
            {
                id: 7,
                answer: 'I’m just there for show',
                description: 'I always knew I was beautiful, simply making up the numbers',
                cssSpriteClass: 'sprite-slide-two cx-graph2',
                musicId: 10
            },
            {
                id: 8,
                answer: 'It wouldn’t happen without me',
                description: 'I’m the cog that keeps this machine going, without me it would all fall apart',
                cssSpriteClass: 'sprite-slide-two cx-graph3',
                musicId: 6
            },
            {
                id: 9,
                answer: 'I’ve got my own agenda',
                description: 'I want what I want and I want it now',
                cssSpriteClass: 'sprite-slide-two cx-graph4',
                musicId: 2
            },
            {
                id: 10,
                answer: 'It’s literally life or death',
                description: 'My heart is pounding, this is a serious one',
                cssSpriteClass: 'sprite-slide-two cx-graph5',
                musicId: 1
            }
        ],
        three: [
            {
                id: 11,
                answer: 'Got some bad news to share',
                description: 'I hate this part of my job',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-three-m cx-m-thumb'" : "'sprite-slide-three cx-thumb'" ?>,
                musicId: 1
            },
            {
                id: 12,
                answer: 'It’s serious – with the bosses',
                description: 'You should never underestimate the predictability of stupidity',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-three-m cx-m-money'" : "'sprite-slide-three cx-money'" ?>,
                musicId: 2
            },
            {
                id: 13,
                answer: 'Diving head first into data',
                description: 'Sensational statistics, I love it',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-three-m cx-m-pie'" : "'sprite-slide-three cx-pie'" ?>,
                musicId: 7
            },
            {
                id: 14,
                answer: 'Crazy creative brainstorm',
                description: 'I like blue? Or maybe orange?',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-three-m cx-m-art'" : "'sprite-slide-three cx-art'" ?>,
                musicId: 4
            },
            {
                id: 15,
                answer: 'Time to get pitch perfect',
                description: 'la la la la la la, oh not that kind of pitch?',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-three-m cx-m-portrait'" : "'sprite-slide-three cx-portrait'" ?>,
                musicId: 8
            }
        ],
        four: [
            {
                id: 16,
                answer: 'Of course, always do!',
                description: 'Fail to prepare, prepare to fail',
                cssSpriteClass: 'sprite-slide-four cx-heart1',
                musicId: 3
            },
            {
                id: 17,
                answer: 'I might have done a bit',
                description: 'I started, but then got distracted',
                cssSpriteClass: 'sprite-slide-four cx-heart2',
                musicId: 10
            },
            {
                id: 18,
                answer: 'Sort of',
                description: 'Well you know…',
                cssSpriteClass: 'sprite-slide-four cx-heart3',
                musicId: 7
            },
            {
                id: 19,
                answer: 'I’m totally winging it',
                description: 'I’m flying by the seat of my pants',
                cssSpriteClass: 'sprite-slide-four cx-heart4',
                musicId: 6
            },
            {
                id: 20,
                answer: 'Meeting, what meeting',
                description: 'Oh s*#?!',
                cssSpriteClass: 'sprite-slide-four cx-heart5',
                musicId: 5
            }
        ],
        five: [
            {
                id: 21,
                answer: 'Taking 40 winks',
                description: 'ummm bed time…',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-five-m cx-m-sheep'" : "'sprite-slide-five cx-sheep'" ?>,
                musicId: 9
            },
            {
                id: 22,
                answer: 'Lying on a beach somewhere warm',
                description: 'Bora Bora baby!',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-five-m cx-m-glasses'" : "'sprite-slide-five cx-glasses'" ?>,
                musicId: 4
            },
            {
                id: 23,
                answer: 'Going on a shopping spree',
                description: 'I just want to shop till I drop!',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-five-m cx-m-cards'" : "'sprite-slide-five cx-cards'" ?>,
                musicId: 2
            },
            {
                id: 24,
                answer: 'Drinking down at the pub',
                description: 'I could really do with a nice cold one right now',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-five-m cx-m-bottles'" : "'sprite-slide-five cx-bottles'" ?>,
                musicId: 1
            },
            {
                id: 25,
                answer: 'Jumping out of an aeroplane',
                description: 'totally rad dude!!!! Ahhhhhh!',
                cssSpriteClass: <?php echo ($isMobile) ? "'sprite-slide-five-m cx-m-cloudy'" : "'sprite-slide-five cx-cloudy'" ?>,
                musicId: 6
            }
        ]},
    music : [
        {
            id: 1,
            title: 'Pow Wow',
            description: 'Like our intrinsic name-sake song, you’re a disrupter who marches to the ' +
                'beat of your own drum! You take life seriously but love adventure, meaning you ' +
                'sometimes fall wayside of the planning side to life. But who cares, take risks, ' +
                'go crazy have a Pow Wow, Now!',
            albumArt: 'album-art-1',
            trackKey: Tracks.powwowTrack
        },
        {
            id: 2,
            title: 'Pork pie Dub',
            description: 'Funky beats hyping you up for your call. You’re clearly the type ' +
                'that lives life to the full and loves some hype-tastic tunes to get you through ' +
                'the day. Go on bang on some Porkpie Dub!',
            albumArt: 'album-art-2',
            trackKey: Tracks.porkPieDubTrack

        },
        {
            id: 3,
            title: 'Get on the Funk Bus',
            description: 'It’s time to get funky… on a bus… maybe?! But if you do want to be in ' +
                'a happy mood, seat dancing and getting your grove on then this is the song ' +
                'for you… get on the bus…. the funk bus! 70s soul to make the most hardened ' +
                'person crack a smile, or even busting out some shoulder shuffling moves.',
            albumArt: 'album-art-3',
            trackKey: Tracks.GetOnTheFunkBusTrack
        },
        {
            id: 4,
            title: '21st Century Disco',
            description: 'D-I-S-C-O! You love to P-A-R-T-Y and have a good time and why should work ' +
                'be any different! Get in the party mood with a bit of a Bee-Gee esq type tune ' +
                'to make your meeting go with a bang! You could even go one step further and ' +
                'install a glitter ball in the conference room to really get you in the mood? You ' +
                'might have to get on the call five minutes early just to enjoy the beats!',
            albumArt: 'album-art-4',
            trackKey: Tracks.CenturyDiscoTrack
        },
        {
            id: 5,
            title: 'Global Celebration',
            description: 'It’s all super doper! It’s big, it’s grandiose; you can’t get bigger than a ' +
                'global celebration! It’s a bit Disney, a bit hopeful, and you’ll feel inspirational ' +
                'and ready for anything when you listen to this tune!',
            albumArt: 'album-art-5',
            trackKey: Tracks.globalCelebrationTrack
        },
        {
            id: 6,
            title: 'Time to Shine',
            description: 'You are a star! You are a champion! But feel people aren’t valuing you,' +
                'well Time to Shine will change all that! Set the tone with this circa sporting ' +
                'montage of success song demonstrating that yes you truly are the champion! ' +
                'You are a WINNER!!',
            albumArt: 'album-art-6',
            trackKey: Tracks.timeToShineTrack
        },
        {
            id: 7,
            title: 'Eine Kleine Nacht',
            description: 'Work is hard, we understand! You’d rather be at home asleep in your ' +
                'bed, wrapped up in your PJs with tommy the teddy bear, but you need to pay ' +
                'the bills! So make out to your colleagues that you are all business and ' +
                'focus with this classical number!',
            albumArt: 'album-art-7',
            trackKey: Tracks.eineKleineNachtTrack
        },
        {
            id: 8,
            title: 'Hispaniola',
            description: 'You’d rather be sunbathing then going onto your call, listening to ' +
                'these sultry Spanish beats can get you in the mood - turn the lights on bright, ' +
                'the heat up high and grab yourself a Pina Colada whilst you wait for ' +
                'everyone to join the call!',
            albumArt: 'album-art-8',
            trackKey: Tracks.hispoaniolaTrack
        },
        {
            id: 9,
            title: 'Calm River',
            description: 'And breathe! Relax! Stressed or on top of the world, you are a conscientious ' +
                'employee who always wants to perform to your best, play some calm river to ' +
                'reiterate your calm exterior, even if your legs are going crazy under ' +
                'the surface!',
            albumArt: 'album-art-9',
            trackKey: Tracks.calmRiverTrack
        },
        {
            id: 10,
            title: 'String Quartet no 23',
            description: 'Going for this classical number shows your serious side. Important meeting, ' +
                'important people, don’t mess about get straight to the point with this classical ' +
                'masterpiece.',
            albumArt: 'album-art-10',
            trackKey: Tracks.stringQuartetTrack
        },
        {
            id: 11,
            title: 'Shuffle',
            description: 'Indecisive, no idea, what’s going on? What day is it? Can’t decide on most ' +
                'things in life, then the shuffle option is for you. Every time you call you get ' +
                'a different song to set the scene! Shuffle away!',
            albumArt: 'album-art-11',
            trackKey: ''
        }
    ]
};

</script>


