<section class="quiz-section <?php echo $device?>" data-attr-device="<?php echo $device?>">
    <div class="row">
        <div class="small-12 columns">
        <div id="quiz.asset.section.container" data-bind="template: {name: 'quiz.asset.section.data', afterRender: initialize }"></div>
            <script type="text/html" id="quiz.asset.section.data">
                <div id="quiz-asset" class="quiz-asset">
                    <div class="quiz-asset-slide quiz-slide-introduction">
                    <div class="row">
                        <div class="small-12 columns">
                            <h2 class="quiz-asset-title">Find Your Favourite Tune...</h2>
                            <div id="ri-grid" class="ri-grid ri-grid-size-1 ri-shadow">
                                <img class="ri-loading-image" src="/cx2/img/throbber.gif"/>
                                <ul>
                                <?php for ($x=1; $x<=10; $x++):?>
                                    <li><a><img src="/cx2/img/music.on.hold/album/mobile/<?php echo $x?>.jpg" alt=""/></a></li>
                                <?php endfor;?>
                                </ul>
                            </div>

                            <p class="quiz-section-text">
                                Whether you're delivering bad news, having a creative brainstorm or have totally
                                forgotten about that conference call you booked in month ago.. take our quick
                                and simple quiz to find the right music for your call.
                            </p>
                            <p class="text-center quiz-button-container">
                                <button data-bind="click: startQuiz" class="custom-button-orange">Start the Quiz</button>
                            </p>
                        </div>
                    </div>
                </div>
                    <!-- Question One Slide -->
                    <div class="quiz-asset-slide quiz-slide-one">
                    <div class="row">
                        <div class="small-12 columns">
                            <div class="quiz-question">
                                <h2 class="quiz-asset-title">Q1.<span> Which of these best describes how you're feeling?</span></h2>
                                <div class="quiz-question-answers">
                                    <div class="quiz-image">
                                        <div data-bind="attr: {class: currentSelected().cssSpriteClass || 'sprite-faces-m cx-m-stressed' }"></div>
                                    </div>
                                    <div class="quiz-section-text" data-bind="text: currentSelected().description || 'My temple vein is throbbing, one more thing and I might explode' "></div>
                                    <label class="small-10 small-centered columns">
                                        <select data-bind="
                                            options: questions.one,
                                            optionsText: 'answer',
                                            optionsValue: 'id',
                                            value: answers.one">
                                        </select>
                                    </label>
                                    <p class="text-center quiz-button-container">
                                        <button data-bind="click: nextQuestion" class="custom-button-orange">Next Question</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Question Two Slide -->
                    <div class="quiz-asset-slide quiz-slide-two">
                    <div class="row">
                        <div class="small-12 columns">
                            <div class="quiz-question">
                                <h2 class="quiz-asset-title">Q2.<span> How important is your conference call on a scale of 1-5?</span></h2>

                                <div class="quiz-question-answers">
                                    <div class="small-11 small-centered columns">
                                        <ul class="chart">
                                            <li class="orange-10" style="height: 10%;"></li>
                                            <li class="orange-20" style="height: 40%;"></li>
                                            <li class="orange-30" style="height: 60%;"></li>
                                            <li class="orange-40" style="height: 80%;"></li>
                                            <li class="orange-50" style="height: 100%;"></li>
                                        </ul>
                                    </div>
                                    <div class="quiz-section-text" data-bind="text: currentSelected().description || 'My temple vein is throbbing, one more thing and I might explode' "></div>
                                    <label class="small-10 small-centered columns">
                                        <select data-bind="
                                            options: questions.two,
                                            optionsText: 'answer',
                                            optionsValue: 'id',
                                            value: answers.two">
                                        </select>
                                    </label>
                                    <p class="text-center quiz-button-container">
                                        <button data-bind="click: nextQuestion" class="custom-button-green">Next Question</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Question Three Slide -->
                    <div class="quiz-asset-slide quiz-slide-three">
                        <div class="row">
                            <div class="small-12 columns">
                                <div class="quiz-question">
                                    <h2 class="quiz-asset-title">Q3.<span> What's the subject of the call?</span></h2>

                                    <div class="quiz-question-answers">
                                        <div class="quiz-image">
                                            <div data-bind="attr: {class: currentSelected().cssSpriteClass || 'sprite-slide-three-m cx-m-thumb' }"></div>
                                        </div>
                                        <div class="quiz-section-text" data-bind="text: currentSelected().description || 'I hate this part of my job' "></div>
                                        <label class="small-10 small-centered columns">
                                            <select data-bind="
                                                options: questions.three,
                                                optionsText: 'answer',
                                                optionsValue: 'id',
                                                value: answers.three">
                                            </select>
                                        </label>
                                        <p class="text-center quiz-button-container">
                                            <button data-bind="click: nextQuestion" class="custom-button-orange">Next Question</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Question Four Slide -->
                    <div class="quiz-asset-slide quiz-slide-four">
                        <div class="row">
                            <div class="small-12 columns">
                                <div class="quiz-question">
                                    <h2 class="quiz-asset-title">Q4.<span>How prepared do you feel for this call?</span></h2>

                                    <div class="quiz-question-answers">
                                        <div class="quiz-image">
                                            <div class="sprite-slide-four-m cx-m-heart"></div>
                                        </div>
                                        <div class="quiz-section-text" data-bind="text: currentSelected().description || 'Fail to prepare, prepare to fail'"></div>
                                        <label class="small-10 small-centered columns">
                                            <select data-bind="
                                                                    options: questions.four,
                                                                    optionsText: 'answer',
                                                                    optionsValue: 'id',
                                                                    value: answers.four">
                                            </select>
                                        </label>
                                        <p class="text-center quiz-button-container">
                                            <button data-bind="click: nextQuestion" class="custom-button-purple">Next Question</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Question Five Slide -->
                    <div class="quiz-asset-slide quiz-slide-five">
                    <div class="row">
                        <div class="small-12 columns">
                            <div class="quiz-question">
                                <h2 class="quiz-asset-title">Q5.<span> What would you rather be doing than getting on a conference call?</span></h2>

                                <div class="quiz-question-answers">
                                    <div class="quiz-image">
                                        <div data-bind="attr: {class: currentSelected().cssSpriteClass || 'sprite-slide-five-m cx-m-sheep' }"></div>
                                    </div>
                                    <div class="quiz-section-text" data-bind="text: currentSelected().description || 'ummm bed time…' "></div>
                                    <label class="small-10 small-centered columns">
                                        <select data-bind="
                                            options: questions.five,
                                            optionsText: 'answer',
                                            optionsValue: 'id',
                                            value: answers.five">
                                        </select>
                                    </label>
                                    <p class="text-center quiz-button-container">
                                        <button data-bind="click: nextQuestion" class="custom-button-blue">Next Question</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Results -->
                    <div class="quiz-asset-slide quiz-slide-results">
                        <div class="row">
                            <div class="small-12 columns">
                                <h2 class="quiz-asset-title">Your perfect track is...</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-5 small-offset-1 columns">
                                <div class="album-art-container-layer" data-bind="'if': result().id != 11, css: result().albumArt">
                                    <div class="album-art-layer">
                                        <div id="mobile_quiz_result_player" class="cp-jplayer"></div>
                                        <div id="mobile_quiz_result_player_container" class="music_catalogue_track_container">
                                            <ul class="cp-controls">
                                                <li><a class="cp-play" tabindex="1">play</a></li>
                                                <li><a class="cp-pause" style="display:none;" tabindex="1">pause</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="small-6 columns end quiz-slide-results-sub-section">
                                <h2 class="quiz-asset-title" data-bind="text: result().title"></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <p class="quiz-section-text" data-bind="text: result().description"></p>
                                <p class="quiz-section-text">
                                    Listen to your chosen track and change your on-hold music below.
                                </p>
                                <p class="text-center quiz-button-container">
                                    <a href="<?php echo url_for('call_settings'); ?>" data-bind="click: endQuiz" class="custom-button-orange">Use This Track</a>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-10 small-centered columns">
                                <p class="right">
                                    <a data-bind="click: restartQuiz()"> Start Quiz Again</a>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </script>
        </div>
    </div>
</section>