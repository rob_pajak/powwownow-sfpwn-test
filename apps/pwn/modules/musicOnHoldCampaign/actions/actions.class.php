<?php

class musicOnHoldCampaignActions extends sfActions
{
    /**
     * @param sfWebRequest $request
     */
    public function executeOnMusicHoldLandingPage(sfWebRequest $request)
    {
        $mobileDetect = new Mobile_Detect();
        $browserDetect = browserDetection::detect();

        $response = $this->getResponse();
        $isIE = $browserDetect['browser_name'] === 'msie' ? true : false;
        $isIE8 = $browserDetect['browser_number'] === '8.0' ? true : false;

        if($mobileDetect->isTablet()) {
            $response->addStylesheet('/cx2/stylesheets/music.on.hold.main/tablet.core.css');
            $response->addJavaScript('/cx2/js/vendor/jquery.grid.rotator.js');
            $response->addJavaScript('/cx2/js/music.on.hold.core/desktop.tablet.core.js');
            // Tablet
            $device = 'tablet';
        } elseif($mobileDetect->isMobile() && !$mobileDetect->isTablet()) {
            $response->addStylesheet('/cx2/stylesheets/music.on.hold.main/mobile.core.css');
            $response->addJavaScript('/cx2/js/vendor/jquery.grid.rotator.js');
            $response->addJavaScript('/cx2/js/music.on.hold.core/mobile.core.js');
            $device = 'mobile';
        } else {
            // desktop
            $response->addStylesheet('/cx2/stylesheets/music.on.hold.main/desktop.core.css');
            if ($isIE8) {
                $response->addStylesheet('/cx2/stylesheets/music.on.hold.main/ie8.css');
                $response->addJavaScript('/cx2/js/music.on.hold.core/desktop.ie8.js');
            } else {
                $response->addJavaScript('/cx2/js/vendor/jquery.grid.rotator.js');
                $response->addJavaScript('/cx2/js/music.on.hold.core/desktop.tablet.core.js');
            }

            $device = 'desktop';
        }

        $isUserFavourite = $request->getCookie('currentFavourite', false);
        $musicCatalogues = array(
            array (
                'trackName' => 'Pow Wow',
                'trackId' => 1,
                'isUserFavourite' => $isUserFavourite == 1 ? true : false,
                'isNew' => true
            ),
            array(
                'trackName' => 'PorkPie Dub',
                'trackId' => 2,
                'isUserFavourite' =>  $isUserFavourite == 2 ? true : false,
                'isNew' => true
            ),
            array(
                'trackName' => 'Get on the Funk Bus',
                'trackId' => 3,
                'isUserFavourite' =>  $isUserFavourite == 3 ? true : false,
                'isNew' => true
            ),
            array(
                'trackName' => '21st Century Disco',
                'trackId' => 4,
                'isUserFavourite' =>  $isUserFavourite == 4 ? true : false,
                'isNew' => true
            ),
            array(
                'trackName' => 'Global Celebration',
                'trackId' => 5,
                'isUserFavourite' =>  $isUserFavourite == 5 ? true : false,
                'isNew' => true
            ),
            array(
                'trackName' => 'Time to Shine',
                'trackId' => 6,
                'isUserFavourite' =>  $isUserFavourite == 6 ? true : false,
                'isNew' => true
            ),
            array(
                'trackName' => 'Eine Kleine Nacht',
                'trackId' => 7,
                'isUserFavourite' =>  $isUserFavourite == 7 ? true : false,
                'isNew' => false
            ),
            array(
                'trackName' => 'Hispoaniola',
                'trackId' => 8,
                'isUserFavourite' =>  $isUserFavourite == 8 ? true : false,
                'isNew' => false
            ),
            array(
                'trackName' => 'Calm River',
                'trackId' => 9,
                'isUserFavourite' =>  $isUserFavourite == 9 ? true : false,
                'isNew' => false
            ),
            array(
                'trackName' => 'String Quartet',
                'trackId' => 10,
                'isUserFavourite' =>  $isUserFavourite == 10 ? true : false,
                'isNew' => false
            ),
        );

        $this->setVar('musicCatalogues', $musicCatalogues);
        $this->setVar('device', $device);
        $this->setVar('isTablet', $mobileDetect->isTablet());
        $this->setVar('isMobile', $mobileDetect->isMobile() && !$mobileDetect->isTablet());
        $this->setVar('isIOS', $mobileDetect->is('iOS'));
        $this->setVar('isIE', $isIE);
        $this->setVar('isIE8', $isIE8);
        $response->setSlot('device', $device);
    }
}
