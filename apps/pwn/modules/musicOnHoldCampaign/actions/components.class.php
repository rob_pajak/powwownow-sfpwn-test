<?php

class musicOnHoldCampaignComponents extends sfComponents
{
    public function executeDesktopTabletQuiz() {
        $this->getViewBaseVariablesForLayout();
    }

    public function executeMobileQuiz() {
        $this->getViewBaseVariablesForLayout();
    }

    private function getViewBaseVariablesForLayout() {
        $mobileDetect = new Mobile_Detect();
        $browserDetect = browserDetection::detect();

        if($mobileDetect->isTablet()) {
            $device = 'tablet';
        } elseif($mobileDetect->isMobile() && !$mobileDetect->isTablet()) {
            $device = 'mobile';
        } else {
            $device = 'desktop';
        }

        $isIE = $browserDetect['browser_name'] === 'msie' ? true : false;
        $isIE8 = $browserDetect['browser_number'] === '8.0' ? true : false;
        $this->setVar('device', $device);
        $this->setVar('isTablet', $mobileDetect->isTablet());
        $this->setVar('isMobile', $mobileDetect->isMobile() && !$mobileDetect->isTablet());
        $this->setVar('isIOS', $mobileDetect->is('iOS'));
        $this->setVar('isIE', $isIE);
        $this->setVar('isIE8', $isIE8);
        $this->getResponse()->setSlot('device', $device);
    }

    /**
     * What You Get Icons used in the Free-Conference-Call-Service Page
     */
    public function executeWhatYouGetFreeConferenceCallService()
    {
        /** @var sfWebResponse $response */
        $response = $this->getResponse();
        $response->addStylesheet('/cx2/stylesheets/what.you.get.css');
    }
}
