<?php

/**
 * Account related components
 * 
 * @author Marcin
 * @author Maarten Jacobs
 */
class accountComponents extends sfComponents
{
    public function executeCreditBalance()
    {
        try {
            $res = Hermes_Client_Rest::call('getPlusAccount', array('account_id' =>  $this->getUser()->getAttribute('account_id', 'na')));
            if (isset($res['account'])) {
                $this->currentBalance = $res['account']['credit_balance_formatted'];
            } else {
                $this->currentBalance = false;
            }
        } catch (Exception $e) {

            $this->currentBalance = false;

            $this->logMessage('Exception Occured for getPlusAccount: ' . serialize($e), 'err');
            $res = json_decode($e);
        }
    }

    public function executeAccountUpdateSummary()
    {
    }
}
