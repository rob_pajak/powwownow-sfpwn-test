<?php

require_once dirname(__FILE__) . '/../../login/lib/AJAXResponse.php';

/**
 * Account actions.
 *
 * @package    powwownow
 * @subpackage account
 * @author     Asfer, Marcin and Vitaly
 * @version    SVN: $Id: actions.class.php 23810 2012-03-26 11:07:44Z Asfer $
 */
class accountActions extends sfActions {

    /**
     * Enhanced to Plus Upgrade [Not Used]
     *
     * This is the Update for Enhanced Users who want to become a Plus Admin
     *
     * @author Marcin
     *
     */
    public function executeUpgradeFreeToPlus(sfWebRequest $request) {

        try {
            // init form
            $this->form = new upgradeFreeToPlusAccountForm();

            // form not posted ? job done, exit
            if (!$request->isMethod('post'))
                return;

            // trigger a validation
            $this->form->bind($request->getParameter($this->form->getName()));

            // not valid ? return errors

            if (!$this->form->isValid()) {
                $this->getResponse()->setStatusCode('409');
                //$this->getResponse()->setHttpHeader()
                return $this->renderPartial('commonComponents/formErrors', array('errors' => $this->form->pwn_getErrors()));
            }
            // update contact details

            $contact = $this->form->getValue('contact');
            $upgrade = (object) $request->getParameter('upgrade');

            // update contact (all form fields are mandatory, easy)
            Hermes_Client_Rest::call('updateContact', array(
                'contact_ref' => $contact->contact_ref,
                'first_name' => $upgrade->first_name,
                'last_name' => $upgrade->last_name,
                'business_phone' => $upgrade->business_phone,
                    // 'country_code' => $upgrade->country_code
            ));

            // upgrade account

            Hermes_Client_Rest::call('upgradeEnhancedToPlus', array(
                'contact_ref' => $contact->contact_ref,
                'service_ref' => 850
            ));
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode('500');
            return sfView::NONE;
        }

        // response

        $response = array('redirect' => $this->getController()->genUrl('s/account/UpgradeFreeToPlusThankYou'));

        return $this->renderPartial('commonComponents/formSuccess', array('response' => $response));

    }

    /**
     * Enhanced to Plus Upgrade [Not Used]
     *
     * This is the Update for Enhanced Users who want to become a Plus Admin
     *
     * @author Marcin
     *
     */
    public function executeUpgradeFreeToPlusThankYou(sfWebRequest $request) {

        // Show View Only

    }

    /**
     * Enhanced to Plus Upgrade [PAGE]
     *
     * This is the Update for Enhanced Users who want to become a Plus Admin
     *
     * @author Asfer
     *
     */
    public function executeUpgradeToPlus(sfWebRequest $request) {

        // Obtain the Form to be used.
        $this->form = new PlusUpgradeForm();

        $this->email = $this->getUser()->getAttribute('email');

    }

    /**
     * Enhanced to Plus Upgrade [AJAX]
     *
     * This is the Update for Enhanced Users who want to become a Plus Admin
     *
     * @author Asfer
     *
     */
    public function executeUpgradeToPlusAjax(sfWebRequest $request) {

        // Obtain the Form to be used.
        $this->form = new PlusUpgradeForm();

        // Check if the Ajax Call was Requested
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        // Bind Form to Inputted Form Values
        $this->form->bind($request->getParameter($this->form->getName()));

        // Check if the Form Passed with Errors
        $errors = (!$this->form->isValid()) ? array('errors' => $this->form->pwn_getErrors()) : 'No Error Found';

        $this->logMessage('Form Errors Occured: ' . serialize($errors), 'err');
        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1');

        // Check Count of the Error Messages (If 0, Continue with Form Process)
        if (isset($errors['errors']['error_messages']) && count($errors['errors']['error_messages']) > 0) {

            $errorMessages[] = array('message' => $errors['errors']['error_messages'][0]['message'], 'field_name' => $errors['errors']['error_messages'][0]['field_name']);
            $this->getResponse()->setStatusCode(500);
            $this->getResponse()->setContentType('application/json');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;

        }

        $formargs = $request->getParameter($this->form->getName());

        // Update Contact
        try {

            $response = Hermes_Client_Rest::call('updateContact', array(
                'contact_ref'    => $this->getUser()->getAttribute('contact_ref'),
                'title'          => $formargs['title'],
                'first_name'     => $formargs['first_name'],
                'last_name'      => $formargs['last_name'],
                'business_phone' => $formargs['business_phone'],
                'mobile_phone'   => $formargs['mobile_phone'],
                'organisation'   => $formargs['company_name'],
            ));

            //Update Contact was not successful
            if ($response['statusCode'] != 202) {

                $this->logMessage('Update Contact Error Occured: ' . serialize($response), 'err');
                $errorMessages[] = array('message' => 'FORM_UPDATE_CONTACT_ERROR', 'field_name' => 'alert', 'id' => '1');
                $this->getResponse()->setStatusCode(500);
                $this->getResponse()->setContentType('application/json');
                echo json_encode(array('error_messages' => $errorMessages));
                return sfView::NONE;

            }

        } catch (Exception $e) {

            $this->logMessage('Exception for Update Contact Triggered: ' . serialize($e), 'err');
            $errorMessages[] = array('message' => 'FORM_UPDATE_CONTACT_ERROR', 'field_name' => 'alert', 'id' => '2');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setContentType('application/json');
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;

        }

        // Update Enhanced User to Plus
        try {
            $source = new Source();
            $responseAccount = Hermes_Client_Rest::call('upgradeEnhancedToPlus', array(
                'contact_ref' => $this->getUser()->getAttribute('contact_ref'),
                'service_ref' => 850,
                'source'      => $source->get('switch_to_plus', $request->getReferer(), $this->getUser()->getAttribute('homePageCroVariation', null, 'cro'))
            ));

            // Plus Account was not successful
            if ($responseAccount['code'] != 205) {

                $this->logMessage('Plus Account was not Successful: ' . serialize($responseAccount), 'err');
                $errorMessages[] = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                $this->getResponse()->setStatusCode(500);
                $this->getResponse()->setContentType('application/json');
                echo json_encode(array('error_messages' => $errorMessages));
                return sfView::NONE;

            }

        } catch (Exception $e) {

            $this->logMessage('Exception Occured for upgradeEnhancedToPlus: ' . serialize($e), 'err');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setContentType('application/json');
            echo json_encode(array('error_messages' => $errorMessages));
            exit;

        }

        // Set Sessions
        $response = Hermes_Client_Rest::call('getContact', array(
            'email' => $_SESSION['email']
        ));

        if (is_array($response) && count($response) > 0) {

            // Remove Existing Credentials
            $user = sfContext::getInstance()->getUser();
            $user->setAuthenticated(false);
            $user->clearCredentials();

            // Set New Credentials
            $user->setAuthenticated(true);
            $_SESSION['authenticated']     = true;
            $_SESSION['service_user']      = isset($response['service_user']) ? $response['service_user'] : '';
            $_SESSION['contact_ref']       = isset($response['contact_ref']) ? $response['contact_ref'] : '';
            $user->setAttribute('contact_ref', $response['contact_ref']);

            $_SESSION['service_ref']       = isset($response['service_ref']) ? $response['service_ref'] : '';
            $_SESSION['first_name']        = isset($response['first_name']) ? $response['first_name'] : '';
            $_SESSION['last_name']         = isset($response['last_name']) ? $response['last_name'] : '';

            $_SESSION['time_limited_pins'] = isset($response['time_limited_pins']) ? $response['time_limited_pins'] : 'NO';
            $_SESSION['account_id']        = isset($responseAccount['account']['id']) ? $responseAccount['account']['id'] : '';

            $_SESSION['service_type']      = 'plus';
            $_SESSION['user_type']         = 'admin';

            $user->addCredentials('plus', 'admin', 'PLUS_ADMIN');
            $user->setAttribute('account_id', $responseAccount['account']['id']);
            $user->setAttribute('service', 'plus');
            $user->setAttribute('user_type', 'admin');

            echo json_encode(array('success_message' => 'PLUS_UPGRADE_SUCCESS', 'first_name' => $this->getUser()->getAttribute('first_name')));
            return sfView::NONE;

        }

        $this->logMessage('Email From Session could not return Contact Details: ' . serialize($_SESSION), 'err');
        echo json_encode(array('success_message' => 'PLUS_UPGRADE_SUCCESS', 'first_name' => ''));
        return sfView::NONE;

    }

    /**
     * Show the Form for the Account Addresses [PAGE]
     *
     * A Plus Admin can edit the Account Addresses
     *
     * @author Unknown
     *
     */
    public function executeAddress(sfWebRequest $request) {

        $this->breadcrumbs = array(
            array('Account Details','/myPwn/Account-Details')
        );

        try {
            $this->listAddresses = Hermes_Client_Rest::call('Plus.getPlusAddresses', array(
                'account_id' => $this->getUser()->getAttribute('account_id')
            ));
        } catch (Exception $e) {
            $this->logMessage('Exception for getPlusAddresses: ' . serialize($e), 'err');
        }

        $this->form = new addressForm();

    }

    /**
     * Show the Form for the Account Addresses [AJAX]
     *
     * A Plus Admin can edit the Account Addresses
     *
     * @author Unknown
     *
     */
    public function executeUpdateAddress(sfWebRequest $request) {

        //Obtain the Form POST Parameters
        $requestParams = $request->getParameterHolder()->getAll();
        unset($requestParams['module']);
        unset($requestParams['action']);

        $args = array(
            'account_id' => $this->getUser()->getAttribute('account_id'),
            'organisation' => $requestParams['plusaddress']['billing_organisation'],
            'building' => $requestParams['plusaddress']['billing_line1'],
            'street' => $requestParams['plusaddress']['billing_line2'],
            'town' => $requestParams['plusaddress']['billing_town'],
            'county' => $requestParams['plusaddress']['billing_county'],
            'postal_code' => $requestParams['plusaddress']['billing_postcode'],
            'country' => $requestParams['plusaddress']['billing_country']
        );

        try {
            $request = Hermes_Client_Rest::call('Plus.updatePlusBillingAddress', $args);
        } catch (Exception $e) {
            $this->logMessage('Exception for Plus.updatePlusBillingAddress: ' . serialize($e), 'err');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setContentType('application/json');
            echo json_encode(array('error_messages' => $errorMessages));

        }

        $args = array(
            'account_id' => $this->getUser()->getAttribute('account_id'),
            'organisation' => $requestParams['plusaddress']['admin_organisation'],
            'building' => $requestParams['plusaddress']['admin_line1'],
            'street' => $requestParams['plusaddress']['admin_line2'],
            'town' => $requestParams['plusaddress']['admin_town'],
            'county' => $requestParams['plusaddress']['admin_county'],
            'postal_code' => $requestParams['plusaddress']['admin_postcode'],
            'country' => $requestParams['plusaddress']['admin_country']
        );

        try {
            $request = Hermes_Client_Rest::call('Plus.updatePlusAdminAddress', $args);
        } catch (Exception $e) {
            $this->logMessage('Exception for updatePlusAdminAddress: ' . serialize($e), 'err');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            $this->getResponse()->setHttpHeader('Content-type: text/plain');
            echo json_encode(array('error_messages' => $errorMessages));
            exit;
        }

        $this->getResponse()->setContentType('application/json');
        echo json_encode(array('success_message' => 'PLUS_UPDATE_ADDRESS_SUCCESS'));
        return sfView::NONE;

    }

    /**
     * Purchase Credit page, now a redirect to the products page as credit is a product
     * as of May 2013
     * @author Asfer, Marcin and Mark
     */
    public function executePurchaseCredit(sfWebRequest $request) {

        $this->redirect ('products_select', 301);

    }

    /**
     * Payment Method [PAGE]
     *
     * Page with select payment method ( card, etc )
     *
     * @author Michal Macierzynski
     * @author Asfer Tamimi
     */
    public function executePaymentMethod(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user   = $this->getUser();

        $basket = new Basket($user);
        $this->handleInvalidBundlesInBasket($basket);

        $this->setVar('step', 0);

        // checking if billing address is there, if not show warning
        $accountId = $user->getAccountId();

        try {
            $billingAddress              = Hermes_Client_Rest::call(
                'Plus.getPlusBillingAddress',
                array(
                    'account_id' => $accountId
                )
            );
            $this->setVar('billingAddressWarning', count($billingAddress) == 0 ? true : false);
        } catch (Exception $e) {
            $this->logMessage('Error checking billing address for accountActions->executePurchaseBasketCredit', 'err');
            $this->setVar('billingAddressWarning', false);
        }

        $this->loadAccountInfo();
        $this->setVar('minimumCreditRequired', plusCommon::calculateMinimumCreditRequired());

        $topUpForm = new topupForm(array('cardtypes' => ''));
        $topUpForm->setDefault('affiliate-code', $request->getParameter('affiliate-code'));
        $this->setVar('topupForm', $topUpForm);
        $this->setVar('formAction', 'checkoutBasketAjax');

        $basketCostCalculator = new BasketCost();
        $totalAmount = $basketCostCalculator->totalWithBalance((float)$this->accountInfo['balance'], $basket);

        try {
            $vatInfo = Hermes_Client_Rest::call('Default.getVatRules', array('vat_code' => 'T1'));
            if (!empty($vatInfo)) {
                $vatInfo['vat_percent'] = $vatInfo['percentage'] / 100;
            } else {
                $vatInfo['vat_percent'] = 0.20;
                $vatInfo['percentage']  = 20;
            }
        } catch (Exception $e) {
            $this->logMessage('Exception called using Default.getVatRules, Exception: ' . serialize($e), 'err');
            $vatInfo['vat_percent'] = 0.20;
            $vatInfo['percentage']  = 20;
        }

        $this->setVar('vatInfo', $vatInfo);
        $this->setVar('grossAmount', $totalAmount + $totalAmount * $vatInfo['vat_percent']);
        $this->setVar('totalAmount', $totalAmount);

        // Update the Product Selector Tracking Table
        $productSelectorId = $this->retrieveProductSelectorToolId();
        if ($productSelectorId) {
            Common::updateProductSelector(
                array(
                    'id'                 => $productSelectorId,
                    'proceed_to_payment' => true,
                )
            );
            $this->setVar('productSelectorTool', 1);
        } else {
            $this->setVar('productSelectorTool', 0);
        }

        $applicationEnvironment = $user->getApplicationEnvironment();
        if (isset($applicationEnvironment) && in_array($applicationEnvironment, array('dev','sqi','staging'))) {
            $this->setVar('testModeValue', 100);
        } else {
            $this->setVar('testModeValue', 0);
        }
    }

    /**
     * Purchase Credit [PAGE]
     *
     * Make a Top-up Payment to the Payment Gateway, so that credit can be used on your account
     *
     * @author Asfer, Marcin and Mark
     *
     */
    public function executePurchaseBasketCredit(sfWebRequest $request) {
        $basket = new Basket($this->getUser());
        $hasBWM = $basket->hasBWMBeenAdded();

        $this->breadcrumbs = array(
            array('Account Details','/myPwn/Account-Details')
        );
        $this->_commonPurchaseCredit(20.0,$hasBWM);

        // checking if billing address is there, if not show warning
        $accountId = $this->getUser()->getAttribute('account_id');

        try{
            $billingAddress = Hermes_Client_Rest::call('Plus.getPlusBillingAddress', array(
                'account_id'           => $accountId
            ));
            $this->billingAddressWarning = count($billingAddress) == 0 ? true : false ;
        } catch(Exception $e){
            $this->logMessage('Error checking billing address for accountActions->executePurchaseBasketCredit', 'err');
            $this->billingAddressWarning = false;
        }

        $this->minimumCreditRequired = ($this->accountInfo['balance'] >= 5) ? plusCommon::calculateMinimumCreditRequired()-5 : plusCommon::calculateMinimumCreditRequired();

        //This is the basket flow, we set the purchase credit step in the basket flowbar
        $this->step = 0;

        $this->setTemplate('purchaseCredit');
        $this->formAction = 'purchaseBasketCreditAjax';

        $this->minTopUpAllowed = max(20,plusCommon::calculateMinimumCreditRequired() - (($this->accountInfo['balance'] >=5) ? 5 : 0 ));
    }

    /**
     * Retrieves the purchase credit selected, regardless of the source (input or radio button).
     *
     * The purchase credit should not include VAT.
     *
     * @param sfWebRequest $request
     * @return float
     */
    protected function retrieveSelectedPurchaseCredit(sfWebRequest $request) {
        $amount = $request->getPostParameter('topup[topup-amount]');
        if ($amount === 'input') {
            $amount = $request->getPostParameter('topup[topup-amount-input]');
        }
        $amount = (float) $amount;
        return $amount;
    }

    /**
     * Prepares the controller action for purchase-credit update error, due to a top-up amount lower than the basket cost.
     *
     * @param float $basketCost
     */
    protected function prepareForPurchaseCreditTooSmallError($basketCost) {
        $this->getResponse()->setContentType('application/json');
        $this->getResponse()->setStatusCode(500);
        $this->success = false;
        $errorMessage = sprintf('The minimum amount to activate your configured products is £%.2f.', $basketCost);
        $this->error_messages = array(
            'message' => $errorMessage
        );
    }

    /**
     * Checkout Basket [AJAX]
     *
     * Returns Data to be used to the WorldPay Connection.
     *
     * @author Asfer and Marcin
     *
     */
    public function executeCheckoutBasketAjax(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url', 'Tag'));

        // Post Ajax Requests are only allowed
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        // Initial Success
        $this->success = true;

        $basket = new Basket($this->getUser());
        $minimumAmount = ProductHelper::retrieveMinimumCallCreditAmount($basket);

        $this->setCheckoutBasketErrorCodes($request, $minimumAmount);
        $this->_checkCard($request);

        // Continue if No Errors Occured From above
        if ($this->success) {
            if (!$this->accountInfo) {
                $this->accountInfo = plusCommon::getAccountBalanceAndUsers($this->getUser()->getAttribute('account_id'),$this->getUser()->getAttribute('contact_ref'));
            }
            $navigation = $this->_getPurchaseCreditNavigation($minimumAmount);
            $addedProducts = serialize($basket->retrieveAddedProducts());

            $this->setUpPaymentWithBasket($request, $navigation, $addedProducts);
        } else {
            //Sets the error code and other related stuff
            $this->getResponse()->setContentType('application/json');
            $this->getResponse()->setStatusCode(500);
        }
        $this->setLayout(false);

    }

    /**
     * Purchase Credit [AJAX]
     *
     * Returns Data to be used to the WorldPay Connection.
     *
     * @author Asfer and Marcin
     *
     */
    public function executePurchaseBasketCreditAjax(sfWebRequest $request) {

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url', 'Tag'));

        // Post Ajax Requests are only allowed
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        // Initial Success
        $this->success = true;
        // Retrieve the account information, which is necessary to calculate if the minimum amount is reached.
        $this->accountInfo = plusCommon::getAccountBalanceAndUsers($this->getUser()->getAttribute('account_id'),$this->getUser()->getAttribute('contact_ref'));
        $minimumCost = plusCommon::calculateMinimumCreditRequired();

        $this->_setPurchaseBasketCreditErrorCodes($request, ($minimumCost-5));

        $this->_checkCard($request);

        // Continue if No Errors Occured From above
        if ($this->success) {

            $navigation = $this->_getPurchaseCreditNavigation($minimumCost);

            $basket = new Basket($this->getUser());
            $addedProducts = serialize($basket->retrieveAddedProducts());

            $this->_setUpPayment($request, $navigation,$addedProducts);

        } else {
            //Sets the error code and other related stuff
            $this->getResponse()->setContentType('application/json');
            $this->getResponse()->setStatusCode(500);
        }
        $this->setTemplate('purchaseCreditAjax');
        $this->setLayout(false);

    }


    /**
     * Shows if a user has auto top-up enabled, if so gives them a form to cancel it
     * If they do not have it enabled, they are given a link to the manual top up page
     * where they can enable it
     *
     * @author Wiseman
     *
     */
    public function executeAutoTopUp() {

        $user = $this->getUser();
        $this->forwardIf(plusCommon::retrievePostPayStatus($user->getAttribute('account_id')), 'bundle', 'disabledAutoTopUpPage');

        $this->breadcrumbs = array(
            array('Account Details','/myPwn/Account-Details')
        );


        // Check if the user has auto top-up is enabled
        $this->enabled = false;

        $response = Hermes_Client_Rest::call('Plus.getAutoTopupSettings', array(
            'account_id' => $user->getAttribute('account_id')
        ));

        // The API returns the current auto top up, so if id exists then auto Top-Up is enabled
        if (isset($response['id'])) {

            $this->enabled = true;
            $this->topUpAmount = $response['amount'];
            $this->currency = (!empty($response['currency']) ? $response['currency'] : 'GBP');

            // Sets up the form where a user can deactivate auto top up
            $this->autoTopUpform = new autoTopUpForm();
        }

    }

    /**
     * Processes the form which allows a user to disable Auto Top-Up
     *
     * @author Wiseman
     *
     */
    public function executeAutoTopUpAjax(sfWebRequest $request) {

        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            $this->forward404();
        }

        $this->getResponse()->setContentType('application/json');

        $user = $this->getUser();
        $accountId = $user->getAttribute('account_id');

        if (plusCommon::retrievePostPayStatus($accountId)) {
            echo json_encode(array('error_messages' => array(array('message' => $this->getContext()->getI18N()->__('Auto Top-Up is disabled for your account.'), 'field_name' => 'alert'))));
            return sfView::NONE;
        }

        try {

            $response = Hermes_Client_Rest::call('Plus.deactivateAutoTopup', array(
                'account_id' => $accountId
            ));

            echo json_encode(array('success_message' => 'MYPWN_AUTO_TOPUP_DISABLE_SUCCESS'));

        } catch (Exception $e) {

            $this->logMessage('Exception disabling auto Top-Up: ' . serialize($e), 'err');
            $this->getResponse()->setStatusCode(500);

            echo json_encode(array('error_messages' => array(array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'))));

        }
        return sfView::NONE;

    }

    /**
     * Unknown [PAGE]
     *
     * Unknown
     *
     * @author Unknown
     *
     */
    public function executeAvailableAccountCredit(sfWebRequest $request) {

        // Show View Only

    }

    /**
     *
     * Show TopUp History
     *
     * Show all Transactions, default date range starts from 6 Months Ago
     *
     * @author Vitaly [23/05/12]
     * @amend Asfer [07/06/2012 [Changed Formatting and added new parameter to Hermes Call]]
     *
     */
    public function executeTopupHistory()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');

        $this->setVar('form', new topupHistoryForm());

        $fullTopUpHistory = Hermes_Client_Rest::call(
            'Plus.getTopupHistoryForAccount',
            array(
                'account_id'       => $this->getUser()->getAttribute('account_id'),
                'date_from'        => date("Y-m-d", strtotime("-6 months")),
                'date_to'          => date('Y-m-d'),
                'transaction_type' => 'all',
                'accepted'         => 1,
            )
        );

        $topUpTable = array();

        if (isset($fullTopUpHistory)) {
            foreach ($fullTopUpHistory as $transaction) {
                if (!empty($transaction['payment_period'])) {
                    $paymentTime = $this->formatPaymentPeriod($transaction['payment_period']);
                } else {
                    $paymentTime = $transaction['payment_time'];
                }

                $viewTopUpALink  = '<a href="#view-' . $transaction['id'] . '" onclick="topUpHistoryViewTopUpDetails(';
                $viewTopUpALink .= $transaction['id'] . ', \'' . url_for('@transaction_detail_ajax') . '\', \'';
                $viewTopUpALink .= url_for('@show_invoice') . '\')">';

                $topUpTable[] = array(
                    $paymentTime,
                    $viewTopUpALink . $transaction['id'] . '</a>',
                    $transaction['transaction_type'],
                    $transaction['payment_amount'],
                    $transaction['transaction_status'] == 'Transaction Pending' ? 'Incomplete' : $transaction['transaction_status'],
                    $viewTopUpALink . 'View Details</a>'
                );
            }
        }

        $this->setVar('topUpTable', json_encode($topUpTable), true);
    }

    /**
     *
     * Filter Topup History Details
     *
     * Filter transactions by date and also the transaction_type
     *
     * @author Vitaly - created 24/05/12
     * @amend Asfer - 06/06/2012 [Added Item Type Filter]
     * @amend Asfer - 07/06/2012 [Removed Item Type Filter in the Table Array, added Hermes API Parameter Instead]
     *
     */
    public function executeFilterTopupHistory(sfWebRequest $request)
    {
        if (!($request->isXmlHttpRequest() && $request->isMethod('post'))) {
            $this->forward404();
        }

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $postData = $request->getParameterHolder()->getAll();

        try {
            $fullTopUpHistory = Hermes_Client_Rest::call(
                'Plus.getTopupHistoryForAccount',
                array(
                    'account_id'       => $this->getUser()->getAttribute('account_id'),
                    'date_from'        => date('Y-m-d', strtotime($postData['plustopuphistory']['topup_start_date'])),
                    'date_to'          => date('Y-m-d', strtotime($postData['plustopuphistory']['topup_end_date'])),
                    'transaction_type' => $postData['plustopuphistory']['topup_item_type'],
                    'accepted'         => 1,
                )
            );

            // Build the Topup History Table Array, to be used in the JS DataTable
            $topUpTable = array();
            foreach ($fullTopUpHistory as $transaction) {
                $viewTopUpALink  = '<a href="#view-' . $transaction['id'] . '" onclick="topUpHistoryViewTopUpDetails(';
                $viewTopUpALink .= $transaction['id'] . ', \'' . url_for('@transaction_detail_ajax') . '\', \'';
                $viewTopUpALink .= url_for('@show_invoice') . '\')">';

                $topUpTable[] = array(
                    $transaction['creation_time'],
                    $viewTopUpALink . $transaction['id'] . '</a>',
                    $transaction['transaction_type'],
                    $transaction['payment_amount'],
                    $transaction['transaction_status'],
                    $viewTopUpALink . 'View Details</a>'
                );

            }

            $response->setContentType('application/json');
            echo json_encode(
                array(
                    'tableData'       => $topUpTable,
                    'success_message' => __('Filters have been applied to your transaction.')
                )
            );
            return sfView::NONE;
        } catch (Exception $e) {
            $this->logMessage('Exception for Plus.getTopupHistoryForAccount: ' . serialize($e), 'info');
            $response->setStatusCode('500');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR');
            return $this->renderPartial(
                'commonComponents/formErrors',
                array('errors' => array('error_messages' => $errorMessages))
            );
        }
    }

    /**
     * Show Topup Details [JSON]
     *
     * Checks if account_id is valid
     * and returns json object for transaction details
     * sort of REST API behaviour, for example to get transaction id = 1
     * /topupDetail/transaction/1
     * This is the page from the Purchase History page, and is NOT the Worldpay Callback page
     *
     * @author Vitaly [25/05/12]
     * @amend Asfer [Changed here, and also created a Helper for this method]
     *
     */
    public function executeTopupDetail(sfWebRequest $request)
    {
        if (!($request->isXmlHttpRequest() && $request->isMethod('post'))) {
            $this->forward404();
        }

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $getData = $request->getParameterHolder()->getAll();

        sfContext::getInstance()->getConfiguration()->loadHelpers('getTopupDetails');

        if (!isset($getData['transaction'])) {
            $response->setStatusCode(500);
            $response->setContentType('application/json');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR');
            echo json_encode(array('error_messages' => $errorMessages));
        }

        $details = getTopupDetails(
            array(
                'payment_id' => $getData['transaction'],
                'account_id' => $this->getUser()->getAttribute('account_id')
            )
        );

        // Check for Error Messages
        if (isset($details['error_found'])) {
            $response->setStatusCode((isset($details['httpCode'])) ? $details['httpCode'] : 500);
            $response->setContentType('application/json');
            echo json_encode(array('error_messages' => $details['error_messages']));
        } else {
            if (!empty($details['payment_period'])) {
                $details['payment_period'] = $this->formatPaymentPeriod($details['payment_period']);
            }
            echo json_encode($details);
        }
        return sfView::NONE;
    }

    /**
     * Show Topup Details [PAGE]
     *
     * Checks if account_id is valid and returns the TopupDetails
     * To get transaction id = 1 :: /topupDetails/transaction/1
     * This page is shown from the Worldpay Callback
     *
     * @author Asfer
     *
     */
    public function executeTopupDetails(sfWebRequest $request)
    {
        $this->setVar('showSummaryUpdateButton', false);
        $this->getCommonTopupDetails($request);
        $this->setVar('flowType', "purchase");

        // Update the Product Selector Tracking Table
        $productSelectorId = $this->retrieveProductSelectorToolId();
        if ($productSelectorId) {
            Common::updateProductSelector(
                array(
                    'id'                     => $productSelectorId,
                    'transaction_successful' => true,
                )
            );
            $this->setVar('productSelectorTool', 1);
        } else {
            $this->setVar('productSelectorTool', 0);
        }

        // Update the Terms and Condition Session
        $this->getUser()->setAttribute('terms_and_conditions_ticked', array('bundle' => '', 'product' => array()));
    }

    /**
     * @param sfWebRequest $request
     */
    public function executeBasketTopupDetails(sfWebRequest $request)
    {
        $this->setTemplate('topupDetails');

        $this->setVar('showSummaryUpdateButton', true);
        $this->getCommonTopupDetails($request);
        $this->setVar('flowType', "basket");
        $this->setVar('balance', 1);
    }

    /**
     * Show Account Statement [PAGE]
     *
     * Show all ledger entries transactions, default date range starts from 01-01-2012
     *
     * @author Bob (nicked from V) - created 07/06/12
     *
     */
    public function executeAccountStatement(sfWebRequest $request) {

        $this->breadcrumbs = array(
            array('Account Details','/myPwn/Account-Details')
        );

        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1');
        $this->form = new ledgerHistoryForm();

        $startDate = date("Y-m-d", strtotime("-6 months"));

        $this->a_id = $this->getUser()->getAttribute('account_id');
        $args = array('account_id' => $this->getUser()->getAttribute('account_id'), 'date_from' => $startDate, 'date_to' => date('Y-m-d'), 'ledger_type' => 'all');
        $this->resp = 'not set';
        try {
            $fullLedgerHistory = Hermes_Client_Rest::call('Plus.getLedgerHistoryForAccount', $args);
        } catch (Exception $e) {
            $this->logMessage('Exception for Plus.getLedgerHistoryForAccount: ' . serialize($e), 'info');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            header("$protocol 404 Not Found");
            header("Content-type: application/json");
            echo json_encode(array('error_messages' => $errorMessages));
            exit;
        }


        $this->ledgerTable = array();

        if (isset($fullLedgerHistory)) {
            foreach ($fullLedgerHistory as $entry) {
                $this->ledgerTable[] = array($entry['created'],$entry['description'],$entry['operation'],$entry['amount'],$entry['balance_after']);
            }
        }

    }

    /**
     * Filter Account Statement [AJAX]
     *
     * @author Bob [07/06/12]
     * @author Asfer Tamimi [24-08-2012]
     * @author Asfer Tamimi [02-04-2013]
     *
     */
    public function executeAccountStatementAjax(sfWebRequest $request) {
        $this->getResponse()->setContentType('application/json');

        // Retrieve Form and Additional Post Values. Also do Form Validation.
        $form = new ledgerHistoryForm();
        $form->bind($request->getParameter($form->getName()));
        $arguments = Common::getArguments($request->getParameter($form->getName()),array(
            'account_id'  => $this->getUser()->getAttribute('account_id'),
        ));
        $postFormValidationError = $form->doPostValidation($arguments);
        if (!$form->isValid() || $postFormValidationError) {
            $this->getResponse()->setStatusCode(500);
            return $this->renderPartial('commonComponents/formErrors', array('form' => $form, 'postFormValidationError' => $postFormValidationError));
        }

        try {
            $fullLedgerHistory = Hermes_Client_Rest::call('Plus.getLedgerHistoryForAccount', array(
                'account_id'  => $arguments['account_id'],
                'date_from'   => date('Y-m-d', strtotime($arguments['ledger_start_date'])),
                'date_to'     => date('Y-m-d', strtotime($arguments['ledger_end_date'])),
                'ledger_type' => $arguments['ledger_type']
            ));

            $ledgerTable = array();
            if (isset($fullLedgerHistory)) {
                foreach ($fullLedgerHistory as $entry) {
                    $ledgerTable[] = array($entry['created'],$entry['description'],$entry['operation'], $entry['amount'],$entry['balance_after']);
                }
            }
            echo json_encode(array('tableData' => $ledgerTable, 'success_message' => $this->getContext()->getI18N()->__('Filters have been applied to your transaction.')));
        } catch(Exception $e) {
            $this->logMessage('Exception for Plus.getLedgerHistoryForAccount: ' . serialize($e), 'err');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode']) ? $e->responseBody['httpCode'] : 500 ));
            echo json_encode(array('error_messages' => array('message' => 'FORM_COMMUNICATION_ERROR')));
        }
        return sfView::NONE;
    }

    /**
     * Show Invoice [DOWNLOAD STREAM]
     *
     * Takes transaction id as get param, verifies the transaction belongs to logged in user and serves invoice
     *
     * @author Bob - created 25/05/12
     * @amend Asfer - 19-06-2012 [Changed the Filename, and Fixed the Formatting]
     *
     */
    public function executeInvoice(sfWebRequest $request) {

        // Obtain the Invoice for the Given Payment ID
        try{

            $invoice = Hermes_Client_Rest::call('Plus.getInvoice', array(
                'account_id' => $this->getUser()->getAttribute('account_id'),
                'payment_id' => $request->getParameter('payment_id')
            ));

            if (count($invoice) == 0) {
                $this->logMessage('No invoice returned from Hermes for Account ' . $this->getUser()->getAttribute('account_id') . 'using Payment ID:' . $request->getParameter('payment_id'),'err');
                $this->forward404();
            }

        }catch(Exception $e){

            // @todo update redirect address
            //    echo $e->getMessage();
            $this->logMessage('Exception getting invoice for transaction: ' . serialize($e), 'info');
            $this->redirect($this->generateUrl('default', array('module' => 'account', 'action' => 'index')));
            return sfView::NONE;

        }

        // Check if the File Exists
        if (!file_exists($invoice['full_path'])) {
            $this->logMessage('Invoice "' . $invoice['full_path'] . '" not found on file system', 'err');
            $this->forward404();
        }

        // Set the Final Response
        $this->getResponse()->setContentType('application/pdf');
        $this->getResponse()->setHttpHeader('Content-Disposition', 'attachment; filename="'.substr($invoice['invoice_no'], -7).'.pdf"');
        $this->getResponse()->sendHTTPHeaders();
        $this->getResponse()->setContent(readfile($invoice['full_path']));

        // Show no Other Output Stream So Exit
        exit;

    }

    /**
     * Account Update Summary [AJAX]
     *
     * This Method is only used to remove the Flow Session.
     *
     * @author Asfer
     *
     */
    public function executeAccountUpdateSummaryAjax(sfWebRequest $request) {

        Plus_Authenticate::startSession();
        $_SESSION['assigned_products']   = null;
        $_SESSION['unassigned_products'] = null;
        $_SESSION['credit_amount']       = null;
        unset($_SESSION['promocode']);

        echo json_encode(array('success_message' => 'Summary Cleared.'));
        return sfView::NONE;

    }

    /**
     * Common Purchase Credit Actions
     *
     * Sets the variables used by all the actions which are doing
     * a credit purchase
     *
     * @author Delio, Asfer, Marcin and Mark
     *
     */
    protected function _commonPurchaseCredit($topup_amount = 20.0, $auto_recharge = true, $cardtypes = '') {

        $this->accountInfo = plusCommon::getAccountBalanceAndUsers($this->getUser()->getAttribute('account_id'),$this->getUser()->getAttribute('contact_ref'));
        $this->breadcrumbs = array(
            array('Account Details','/myPwn/Account-Details')
        );

        // topup form
        topupForm::$_availiableTopups = array('20' => 20, '50' => 50, '100' => 100);
        topupForm::$_topupCurrency    = 'GBP';
        topupForm::$_topupVAT         = 0.20;
        $this->topupForm = new topupForm(array('topup-amount' => $topup_amount,'auto-recharge' => $auto_recharge, 'cardtypes' => $cardtypes));

        // Check if the user has auto top-up enabled
        try {
            $response = Hermes_Client_Rest::call('Plus.getAutoTopupSettings', array(
                'account_id' => $this->getUser()->getAttribute('account_id')
            ));

            // The API returns the current auto top up, so if id exists then auto Top-Up is enabled
            if (isset($response['id'])) {
                $this->autoTopupEnabled = true;
            }else{
                $this->autoTopupEnabled = false;
            }

        } catch (Exception $e) {
            $this->logMessage('Exception seeing if User has Auto Top-Up enabled:' . serialize($e), 'err');
            $this->autoTopupEnabled = false;
        }

        // If there are no Products in the basket, then clear the summary-details session variable
        /*if (!$addedProducts) {
            $this->getUser()->getAttributeHolder()->remove('summary-details');
        }*/

    }

    protected function setCheckoutBasketErrorCodes(sfWebRequest $request, $minimumCreditRequired)
    {
        // Check the Topup Amount If it is Set for the Input Field
        $basket = new Basket($this->getUser());

        $total = $basket->calculateTotalCost();

        $nonBwmProducts = $basket->retrieveNonExistingProducts(true);
        $creditAmount = false;
        if (count($nonBwmProducts) > 0) {
            foreach ($nonBwmProducts as $productId => $product) {
                if ($product['type'] == 'credit') {
                    $creditAmount = $product['total'];
                }
            }
        }

        if (!is_numeric($total)) {
            $this->success = false;
            $this->message = 'FORM_VALIDATION_AMOUNT_INVALID';
            $this->fieldname = 'alert';
            $errorMessages = array('message' => 'FORM_VALIDATION_AMOUNT_INVALID', 'field_name' => 'alert');
            $this->error_messages = $errorMessages;
        } elseif ($creditAmount !== false && is_numeric($creditAmount) && $creditAmount < $minimumCreditRequired) {
            $this->success = false;
            $this->message = 'FORM_VALIDATION_AMOUNT_TOO_SMALL';
            $this->fieldname = 'alert';
            $errorMessages = array('message' => 'FORM_VALIDATION_AMOUNT_TOO_SMALL', 'field_name' => 'alert');
            $this->error_messages = $errorMessages;
        }
    }

    protected function _setPurchaseBasketCreditErrorCodes(sfWebRequest $request, $minimumCreditRequired)
    {
        // Check the Topup Amount If it is Set for the Input Field
        $postAmount = $request->getPostParameter('topup[topup-amount]');
        if ($postAmount == 'input') {
            $postAmount = $request->getPostParameter('topup[topup-amount-input]');
        }

        if (!is_numeric($postAmount)) {
            $this->success = false;
            $this->message = 'FORM_VALIDATION_AMOUNT_INVALID';
            $this->fieldname = 'topup[topup-amount-input]';
            $errorMessages = array('message' => 'FORM_VALIDATION_AMOUNT_INVALID', 'field_name' => 'topup[topup-amount-input]');
            $this->error_messages = $errorMessages;
        } elseif (is_numeric($postAmount) && $postAmount < 20) {
            $this->success = false;
            $this->message = 'FORM_VALIDATION_AMOUNT_TOO_SMALL';
            $this->fieldname = 'topup[topup-amount-input]';
            $errorMessages = array('message' => 'FORM_VALIDATION_AMOUNT_TOO_SMALL', 'field_name' => 'topup[topup-amount-input]');
            $this->error_messages = $errorMessages;
        } elseif (is_numeric($postAmount) && $this->accountInfo['balance'] < 5 && ($postAmount < ($minimumCreditRequired+5))) {
            $this->success = false;
            $this->message = 'FORM_VALIDATION_AMOUNT_TOO_SMALL_CREDIT_SMALL';
            $this->fieldname = 'topup[topup-amount-input]';
            $errorMessages = array('message' => 'FORM_VALIDATION_AMOUNT_TOO_SMALL_CREDIT_SMALL', 'field_name' => 'topup[topup-amount-input]');
            $this->error_messages = $errorMessages;
        } elseif (is_numeric($postAmount) && $this->accountInfo['balance'] >= 5 && ($postAmount < $minimumCreditRequired || $postAmount < 20)) {
            $this->success = false;
            $this->message = 'FORM_VALIDATION_AMOUNT_TOO_SMALL_CREDIT_BIG';
            $this->fieldname = 'topup[topup-amount-input]';
            $errorMessages = array('message' => 'FORM_VALIDATION_AMOUNT_TOO_SMALL_CREDIT_BIG', 'field_name' => 'topup[topup-amount-input]');
            $this->error_messages = $errorMessages;
        }
    }

    protected function _checkCard(sfWebRequest $request)
    {
        // Check the Card Type Selection. Should NOT be empty
        $userCardType = $request->getPostParameter('topup[cardtypes]');
        if (empty($userCardType)) {

            $errorMessages = array('message' => 'FORM_VALIDATION_CARDTYPES_EMPTY', 'field_name' => 'topup[cardtypes]');
            $this->error_messages = $errorMessages;

            $this->getResponse()->setContentType('application/json');
            $this->getResponse()->setStatusCode(500);

            $this->success = false;
            $this->message = 'FORM_VALIDATION_CARDTYPES_EMPTY';
            $this->fieldname = 'topup[cardtypes]';

        }

    }

    protected function _getPurchaseCreditNavigation($minimumCreditRequired,$flowType = 'basket')
    {
        $managesUsers = $this->accountInfo['accountUsers'] === 'M';

        // Set the Products Navigation
        if ($managesUsers && $this->accountInfo['balance'] >= $minimumCreditRequired) {
            $navcomponent = $navcomponentCB = $this->getComponent('commonComponents','FlowBar', array('forceStepStyles' => true, 'step' => 0, 'override' => 'balance', 'minimumCredit' => $minimumCreditRequired, 'flowType' => $flowType));
        } elseif ($managesUsers && $this->accountInfo['balance'] < $minimumCreditRequired) {
            $navcomponent = $navcomponentCB = $this->getComponent('commonComponents','FlowBar', array('forceStepStyles' => true, 'step' => 0, 'minimumCredit' => $minimumCreditRequired, 'flowType' => $flowType));
        } elseif (!$managesUsers && $this->accountInfo['balance'] >= $minimumCreditRequired) {
            $navcomponent = $navcomponentCB = $this->getComponent('commonComponents','FlowBar', array('forceStepStyles' => true, 'step' => 0, 'override' => 'balance', 'minimumCredit' => $minimumCreditRequired, 'flowType' => $flowType));
        } elseif (!$managesUsers && $this->accountInfo['balance'] < $minimumCreditRequired) {
            $navcomponent = $navcomponentCB = $this->getComponent('commonComponents','FlowBar', array('forceStepStyles' => true, 'step' => 0, 'minimumCredit' => $minimumCreditRequired, 'flowType' => $flowType));
        }

        $result =  array(
            'navcomponent'   => $navcomponent,
            'navcomponentCB' => $navcomponentCB,
        );

        return $result;
    }

    /**
     * Checks if we should set up a FuturePay agreement with this payment.
     *
     * We should only create a new Recurring Payment Agreement if and only if:
     * - a BWM has been added, or,
     * - a Bundle has been added, or,
     * - the ONLY product is Call Credit with auto-recharge set to true.
     *
     * @param Basket $basket
     * @param int $accountId
     * @return bool
     * @author Maarten Jacobs
     */
    private function isFuturePaySetupWorldPayRequest(Basket $basket, $accountId)
    {
        $autoTopup = false;

        // Check if the basket contains either a Bundle or BWM, or if the basket only contains Call Credit which is
        // set for auto-topup.
        $callCredits = $basket->retrieveCallCreditProducts(true);
        if (count($basket->retrieveAddedProducts()) === 1 && $callCredits) {
            foreach ($callCredits as $product) {
                if ($product['total'] > 0 && $product['auto-recharge'] == true ) {
                    $autoTopup = true;
                }
            }
        } else {
            $autoTopup = $basket->hasBundleBeenAdded() || $basket->hasBWMBeenAdded();
        }

        // We only really need auto-topup if there is no valid recurring payment agreement set for the admin.
        if ($autoTopup) {
            $hasValidAgreement = $this->checkIfAdminHasValidAgreement($accountId);
            if ($hasValidAgreement) {
                $autoTopup = false;
            }
        }

        return $autoTopup;
    }

    /**
     * Checks if the Plus Admin has a valid Recurring Payment Agreement set.
     *
     * Note that on exception (from calling Hermes, for instance), this method will return false.
     * So we are more likely to create a Recurring Payment Agreement, just to be sure the user has an Agreement.
     *
     * @param int $accountId
     * @return bool
     * @author Maarten Jacobs
     */
    private function checkIfAdminHasValidAgreement($accountId)
    {
        $hasValidAgreement = false;
        try {
            $hasValidAgreement = plusCommon::doesPlusAdminHaveValidRecurringPaymentAgreementSetUp($accountId);
        } catch (Exception $e) {
            $this->logMessage(
                sprintf(
                    'The call to Bundle.doesPlusAdminHaveValidRecurringPaymentAgreementSetUp has failed. '
                    . 'Error message: "%s". Error code: "%s".',
                    $e->getMessage(),
                    $e->getCode()
                ),
                'error'
            );
        }
        return $hasValidAgreement;
    }

    protected function setUpPaymentWithBasket(sfWebRequest $request, $navigation, $addedProducts)
    {
        $flowSuccessUrl = 'successUrl';
        $flowCancelUrl  = 'cancelUrl';

        $user = $this->getUser();
        $email = $user->getAttribute('email');
        $accountId = $user->getAccountId();

        // Obtain the Current Plus Account Related Addresses
        $addresses = Hermes_Client_Rest::call(
            'Plus.getPlusCreditCardAddressWithFallbackToBilling',
            array('account_id' => $accountId)
        );
        $dataAddress = $addresses;

        if (empty($dataAddress)) {

            $dataAddress = array(
                'organisation' => '',
                'building'     => '',
                'street'       => '',
                'town'         => '',
                'county'       => '',
                'country'      => 'GBR',
                'postal_code'  => '',
                'email'        => $email,
                'tel'          => '',
            );

        } else {

            $dataAddress['tel']      = '';
            $dataAddress['email']    = $email;

        }

        // Local countryCodes Helper
        sfContext::getInstance()->getConfiguration()->loadHelpers('countryCodes');

        $paymentGateway        = sfConfig::get('app_payment_default','worldpay');
        $paymentGatewayDetails = sfConfig::get('app_'.$paymentGateway);

        // Check if the User Requested Any Changes to the TestMode Parameter
        $testMode = $request->getPostParameter('testMode',$paymentGatewayDetails['testMode']);

        // Calculate postAmount with basket
        $basket = new Basket($user);
        $this->loadAccountInfo();
        $basketCostCalculator = new BasketCost();
        $postAmount = $basketCostCalculator->totalWithBalance((float) $this->accountInfo['balance'], $basket);

        // Check for auto topup product
        $autoTopup = $this->isFuturePaySetupWorldPayRequest($basket, $accountId);

        $cartID = md5(uniqid($email, true));
        $session = new PaymentSession($cartID, $accountId);
        $session->setAutotopup($autoTopup)
                ->setAddedProducts(base64_encode($addedProducts))
                ->setAmountPreVAT($postAmount);
        if ($request->getPostParameter('topup[affiliate-code]')) {
            $session->setAffiliateCode($request->getPostParameter('topup[affiliate-code]'));
        }

        // Set the Worldpay Ajax Data
        $this->data = array(
            'accId1'        => $paymentGatewayDetails['accountID'],
            'instId'        => $paymentGatewayDetails['installationID'],

            'desc'          => 'Powwownow Checkout',

            'name'          => '',
            'address1'      => $dataAddress['organisation'],
            'address2'      => $dataAddress['building'],
            'address3'      => $dataAddress['street'],
            'town'          => $dataAddress['town'],
            'region'        => $dataAddress['county'],
            'country'       => three2two($dataAddress['country']),
            'postcode'      => $dataAddress['postal_code'],
            'email'         => $dataAddress['email'],
            'tel'           => $dataAddress['tel'],
            'fixContact'    => false,

            'M_action'      => 'checkout',

            'amount'        => $postAmount,

            'currency'      => "GBP", /*GBP for the First Release WEB-169*/
            'hideCurrency'  => true,

            'testMode'      => $testMode,
            'cartId'        => $cartID,
            'paymentType'   => $request->getPostParameter('topup[cardtypes]'),
        );

        if ($autoTopup) {
            $this->data['futurePayType'] = 'limited';
            $this->data['option'] = 0;
            $this->data['amountLimit'] = 0;
        }

        $this->data['signature'] = md5( "r1chm0nd:" . $this->data['cartId'] . ":". $this->data['country'] );

        if (isset($this->data['testMode']) && $this->data['testMode'] == 0) {
            $this->postLink = $paymentGatewayDetails['liveurl'];
        } else {
            $this->postLink = $paymentGatewayDetails['testurl'];
        }

        // Vat Calculations
        try {
            $vatInfo = Hermes_Client_Rest::call('Default.getVatRules', array('vat_code' => 'T1'));
            if (!empty($vatInfo)) {
                $vatInfo['vat_percent'] = $vatInfo['percentage'] / 100;
            } else {
                $vatInfo['vat_percent'] = 0.20;
            }
        } catch (Exception $e) {
            $this->logMessage('Exception called using Default.getVatRules, Exception: ' . serialize($e), 'err');
            $vatInfo['vat_percent'] = 0.20;
        }

        $session->setVATPercentage($vatInfo['vat_percent']);

        // Start the Transaction Process, so add the Database Record to the transaction_log
        try {
            $status = Hermes_Client_Rest::call('Plus.createPayment', array(
                'payment_gateway_id'  => 1,
                'account_id'          => $session->getAccountId(),
                'session_id'          => $session->getCartID(),
                'transaction_type'    => 'manual',
                'payment_amount'      => $session->getAmountPreVAT(),
                'vat_percent'         => $session->getVATPercentage(),
                'vat_value'           => $session->getAmountPreVAT() * $session->getVATPercentage(),
                'payment_amount_post' => $session->getAmountPreVAT() + ($session->getAmountPreVAT() * $session->getVATPercentage()),
                'test_mode'           => ($testMode == 100) ? 1 : 0,
            ));

            // Transaction Inserted Successfully
            if (isset($status['code']) && $status['code'] == 204) {
                $this->success = true;
                $session->setPaymentId($status['payment_id']);

                $this->data['amount']              = $session->getAmountPreVAT() + ($session->getAmountPreVAT() * $session->getVATPercentage());
                $this->data['amountLimit']         = $this->data['amountLimit'] + ($this->data['amountLimit'] * $session->getVATPercentage());

                if ($request->isSecure()) {
                    $protocol = "https://";
                } else {
                    $protocol = "http://";
                }

                $this->data['C_url_success']       = $protocol . $_SERVER['SERVER_NAME'] . url_for($paymentGatewayDetails[$flowSuccessUrl]) . '/' . $status['payment_id'];
                $this->data['C_url_cancel']        = $protocol . $_SERVER['SERVER_NAME'] . url_for($paymentGatewayDetails[$flowCancelUrl]) . '/' . $status['payment_id'];
                $this->data['MC_callback']         = $protocol . $_SERVER['SERVER_NAME'] . url_for($paymentGatewayDetails['mainCallback']);
                $this->data['MC_navList']          = str_replace('"',"'", $navigation['navcomponent']);
                $this->data['MC_navList_CB']       = str_replace('"',"'", $navigation['navcomponentCB']);

                // Store Session of the Payment Amount
                Plus_Authenticate::startSession();
                if (!isset($_SESSION['assigned_products'])) {
                    $_SESSION['assigned_products']   = null;
                    $_SESSION['unassigned_products'] = null;
                }
                $_SESSION['credit_amount'] = $session->getAmountPreVAT();

            }

            plusCommon::storePaymentSession($session);

        } catch (Exception $e) {

            // Transaction Failed, therefore Log Error and Build Error Response
            $this->logMessage('Exception for Payment Creation: ' . serialize($e), 'err');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $this->error_messages = $errorMessages;

            $this->getResponse()->setContentType('application/json');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode'])) ? $e->responseBody['httpCode'] : 500);

            $this->success = false;

        }
    }

    protected function _setUpPayment(sfWebRequest $request, $navigation, $addedProducts = '',$flowType = 'basket')
    {
        if ($flowType == "purchase") {
            $flowSuccessUrl = 'successUrl';
            $flowCancelUrl  = 'cancelUrl';
        } else {
            $flowSuccessUrl = 'basketSuccessUrl';
            $flowCancelUrl  = 'basketCancelUrl';
        }

        // Obtain the Current Plus Account Related Addresses
        $addresses = Hermes_Client_Rest::call('Plus.getPlusCreditCardAddressWithFallbackToBilling', array(
            'account_id' => $this->getUser()->getAttribute('account_id'))
        );
        $dataAddress = $addresses;

        if (empty($dataAddress)) {

            $dataAddress = array(
                'organisation' => '',
                'building'     => '',
                'street'       => '',
                'town'         => '',
                'county'       => '',
                'country'      => 'GBR',
                'postal_code'  => '',
                'email'        => $this->getUser()->getAttribute('email'),
                'tel'          => '',
            );

        } else {

            $dataAddress['tel']      = '';
            $dataAddress['email']    = $this->getUser()->getAttribute('email');

        }

        // Local countryCodes Helper
        sfContext::getInstance()->getConfiguration()->loadHelpers('countryCodes');

        $paymentGateway        = sfConfig::get('app_payment_default','worldpay');
        $paymentGatewayDetails = sfConfig::get('app_'.$paymentGateway);

        // Check if the User Requested Any Changes to the TestMode Parameter
        $testMode = $request->getPostParameter('testMode',$paymentGatewayDetails['testMode']);

        // Check Amount Given
        $postAmount = $request->getPostParameter('topup[topup-amount]');
        if ($postAmount == 'input') {
            $postAmount = $request->getPostParameter('topup[topup-amount-input]');
            if (!is_numeric($postAmount)) $postAmount = 20;
        }

        $session = new PaymentSession(md5(uniqid($this->getUser()->getAttribute('email'), true)), $this->getUser()->getAttribute('account_id'));
        $session->setAutotopup($request->getPostParameter('topup[auto-recharge]') !== null)
                ->setAddedProducts(base64_encode($addedProducts))
                ->setAmountPreVAT($postAmount);

        // Set the Worldpay Ajax Data
        $this->data = array(
            'accId1'        => $paymentGatewayDetails['accountID'],
            'instId'        => $paymentGatewayDetails['installationID'],

            'desc'          => 'Powwownow Topup',

            'name'          => '',
            'address1'      => $dataAddress['organisation'],
            'address2'      => $dataAddress['building'],
            'address3'      => $dataAddress['street'],
            'town'          => $dataAddress['town'],
            'region'        => $dataAddress['county'],
            'country'       => three2two($dataAddress['country']),
            'postcode'      => $dataAddress['postal_code'],
            'email'         => $dataAddress['email'],
            'tel'           => $dataAddress['tel'],
            'fixContact'    => false,

            'M_action'      => 'topup',

            'futurePayType' => 'limited',
            'option'        => 0,
            'amount'        => $postAmount,
            'amountLimit'   => $postAmount,

            'currency'      => "GBP", /*GBP for the First Release WEB-169*/
            'hideCurrency'  => true,

            'testMode'      => $testMode,
            'cartId'        => $session->getCartID(),
            'paymentType'   => $request->getPostParameter('topup[cardtypes]'),

        );

        $this->data['signature'] = md5( "r1chm0nd:" . $this->data['cartId'] . ":". $this->data['country'] );

        if (isset($this->data['testMode']) && $this->data['testMode'] == 0) {
            $this->postLink = $paymentGatewayDetails['liveurl'];
        } else {
            $this->postLink = $paymentGatewayDetails['testurl'];
        }

        // Vat Calculations
        try {
            $vatInfo = Hermes_Client_Rest::call('Default.getVatRules', array('vat_code' => 'T1'));
            if (!empty($vatInfo)) {
                $vatInfo['vat_percent'] = $vatInfo['percentage'] / 100;
            } else {
                $vatInfo['vat_percent'] = 0.20;
            }
        } catch (Exception $e) {
            $this->logMessage('Exception called using Default.getVatRules, Exception: ' . serialize($e), 'err');
            $vatInfo['vat_percent'] = 0.20;
        }


        // Start the Transaction Process, so add the Database Record to the transaction_log
        try {
            $status = Hermes_Client_Rest::call('Plus.createPayment', array(
                'payment_gateway_id'  => 1,
                'account_id'          => $this->getUser()->getAttribute('account_id'),
                'session_id'          => $this->data['cartId'],
                'transaction_type'    => 'manual',
                'payment_amount'      => $postAmount,
                'vat_percent'         => $vatInfo['vat_percent'],
                'vat_value'           => $postAmount * $vatInfo['vat_percent'],
                'payment_amount_post' => $postAmount + ($postAmount * $vatInfo['vat_percent']),
                'test_mode'           => ($testMode == 100) ? 1 : 0,
            ));

            // Transaction Inserted Successfully
            if (isset($status['code']) && $status['code'] == 204) {
                $this->success = true;
                $this->data['MC_paymentamountpre'] = $this->data['amount'];
                $this->data['amount']              = $this->data['amount'] + ($this->data['amount'] * $vatInfo['vat_percent']);
                $this->data['amountLimit']         = $this->data['amountLimit'] + ($this->data['amountLimit'] * $vatInfo['vat_percent']);
                $session->setPaymentId($status['payment_id']);
                $session->setVATPercentage($vatInfo['vat_percent']);
                $this->data['C_url_success']       = 'http://' . $_SERVER['SERVER_NAME'] . url_for($paymentGatewayDetails[$flowSuccessUrl]) . '/' . $status['payment_id'];
                $this->data['C_url_cancel']        = 'http://' . $_SERVER['SERVER_NAME'] . url_for($paymentGatewayDetails[$flowCancelUrl]) . '/' . $status['payment_id'];
                $this->data['MC_callback']         = 'http://' . $_SERVER['SERVER_NAME'] . url_for($paymentGatewayDetails['mainCallback']);
                $this->data['MC_navList']          = str_replace('"',"'", $navigation['navcomponent']);
                $this->data['MC_navList_CB']       = str_replace('"',"'", $navigation['navcomponentCB']);

                // Store Session of the Payment Amount
                Plus_Authenticate::startSession();
                if (!isset($_SESSION['assigned_products'])) {
                  $_SESSION['assigned_products']   = null;
                  $_SESSION['unassigned_products'] = null;
                }
                $_SESSION['credit_amount'] = $this->data['MC_paymentamountpre'];

            }

            plusCommon::storePaymentSession($session);

        } catch (Exception $e) {

            // Transaction Failed, therefore Log Error and Build Error Response
            $this->logMessage('Exception for Payment Creation: ' . serialize($e), 'err');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            $this->error_messages = $errorMessages;

            $this->getResponse()->setContentType('application/json');
            $this->getResponse()->setStatusCode((!empty($e->responseBody['httpCode'])) ? $e->responseBody['httpCode'] : 500);

            $this->success = false;

        }
    }

    /**
     * @param sfWebRequest $request
     */
    protected function getCommonTopupDetails(sfWebRequest $request)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('getTopupDetails');
        $transaction = $request->getParameter('transaction');
        $this->setVar('transaction', $transaction);

        // Check the Required Parameters are Set
        if (!isset($transaction)) {
            $this->logMessage('Transaction ID Not Found in the Post', 'err');
            $this->setVar('details', array(
                'error_found' => true,
                'error_message' => 'Transaction ID Not Found'
            ));
            return;
        }

        /** @var myUser $user */
        $user       = $this->getUser();

        /** @var sfWebResponse $response */
        $response = $this->getResponse();

        $accountId  = $user->getAccountId();
        $contactRef = $user->getContactRef();

        $session = plusCommon::retrievePaymentSessionByPaymentId($transaction, $this->getLogger());
        $summaryDetails = array();
        if ($session) {
            $this->forward404If($session->getAccountId() !== $accountId);
            $this->setVar('hasRecentSession', true);
            $this->setVar('hasProcessedPayment', $session->hasProcessedPayment());
            if ($this->getVar('hasProcessedPayment')) {
                $basket = new Basket($user);
                $basket->clearBasket();
                $summaryDetails = unserialize(base64_decode($session->getProcessedPayment()));
            }
        }

        // Change the balance and minimum cost which is used by the template.
        // This allows the FlowBar to be shown as it was before the basket changes.
        $balance = isset($summaryDetails['balance_before']) ? $summaryDetails['balance_before'] : 0;
        $this->setVar('minimumCost', $balance + 1);
        $this->setVar('step', "last");
        $this->setVar('balance', $balance);

        // Obtain the TopupDetails
        $details = getTopupDetails(array(
            'payment_id'  => $request->getParameter('transaction'),
            'account_id'  => $accountId
        ));

        $technicalErrorMessage = 'Sorry, we are currently experiencing technical difficulties. Please try again later';

        // Check for Error Messages
        if (isset($details['error_found'])) {
            $this->logMessage('Error Found for TopUpDetails for transaction: ' . $transaction, 'err');

            if (isset($details['error_messages'][0]['message'])
                && $details['error_messages'][0]['message'] === 'FORM_COMMUNICATION_ERROR') {
                $details['error_message'] = $technicalErrorMessage;
            }
            $this->setVar('details', $details);
            return;

        } elseif (0 == count($details)) {
            $this->logMessage('TopUpDetails Returned an Empty Array for transaction: ' . $transaction, 'err');
            $this->setVar('details', array('error_found' => true,'error_message' => $technicalErrorMessage));
            return;
        }

        if (isset($details['invoice_id']) && !is_null($details['invoice_id']) && $details['invoice_id'] !== '') {
            $this->setVar('invoiceDownload', true);
        } else {
            $this->setVar('invoiceDownload', false);
        }

        // Attach a label for the top-up amount.
        $this->setVar('topupLabel', $details['transaction_type']);
        if ($details['transaction_type'] === 'Manual Topup') {
            $this->setVar('topupLabel', 'Product Purchase');
        }

        $accountInfo = plusCommon::getAccountBalanceAndUsers($accountId, $contactRef);
        $this->setVar('managesUsers', $accountInfo['accountUsers'] === 'M');
        $this->setVar('accountInfo', $accountInfo);

        if ($summaryDetails) {
            $this->determineMessageAndButtonsToShow($summaryDetails['added_products'], $this->getVar('managesUsers'));
            $basketDisplay = new BasketDisplay();
            $this->setVar(
                'addedProducts',
                $basketDisplay->prepareProductsForBasketDisplay($summaryDetails['added_products']),
                true
            );
        } else {
            $this->setVar('addedProducts', array(), true);
        }

        $this->setVar('summaryDetails', $summaryDetails);
        $this->setVar('details', $details);

        $response->addJavascript('/sfjs/pages/myPwn/purchase-confirmation', 'last');
    }

    private function determineMessageAndButtonsToShow($addedProducts, $managesUsers)
    {
        $this->showManageUsersButton = true && $managesUsers;
        $addedProducts = array_values($addedProducts);

        if ($this->isCallCreditSoleProduct($addedProducts)) {
            $this->showManageUsersButton = false;
            $this->confirmationMessage = false;
        } elseif ($this->isIndividualBundleSoleProduct($addedProducts)) {
            $this->showManageUsersButton = false;

            /** @var Bundle $bundle */
            $bundle = $addedProducts[0];
            $this->addIndividualBundleConfirmationMessage(
                $bundle->getAssignableProductGroupId(),
                $this->getUser()->getContactRef(),
                "Your Purchased All You Can Meet bundle has been assigned to PIN pair %s/%s."
            );
        } elseif ($this->areLandlineBundlesAndOrBWMsOnlyAddedProducts($addedProducts)) {
            $this->confirmationMessage = 'Your Purchased Products have been automatically assigned to all of your users.';
        } else {
            $bundle = $this->retrieveIndividualBundleFromAddedProducts($addedProducts);
            $this->addIndividualBundleConfirmationMessage(
                $bundle->getProductGroupId(),
                $this->getUser()->getContactRef(),
                'Your All You Can Meet bundle has been assigned to PIN pair %s/%s. Any additional purchased products have been assigned to all of your users.'
            );
        }
    }

    private function addIndividualBundleConfirmationMessage($productGroupId, $contactRef, $messagePattern)
    {
        $assignedPinPair = plusCommon::retrieveIndividualBundleAssignedPinPair($productGroupId, $contactRef);
        if ($assignedPinPair) {
            $assignedPinPair = array_values($assignedPinPair['pin_pair']);
            $this->confirmationMessage = sprintf($messagePattern, $assignedPinPair[0]['pin'], $assignedPinPair[1]['pin']);
        } else {
            $this->confirmationMessage = 'Purchased All You Can Meet bundle has not been assigned to any PIN pair.';
        }
    }

    private function retrieveIndividualBundleFromAddedProducts(array $addedProducts)
    {
        foreach ($addedProducts as $product) {
            if ($product instanceof Bundle && $product->isIndividualBundle()) {
                return $product;
            }
        }
        return false;
    }

    private function areLandlineBundlesAndOrBWMsOnlyAddedProducts(array $addedProducts)
    {
        return $this->retrieveIndividualBundleFromAddedProducts($addedProducts) === false;
    }

    private function isCallCreditSoleProduct(array $addedProducts)
    {
        return count($addedProducts) === 1 && is_array($addedProducts[0]) && isset($addedProducts[0]['type']) && $addedProducts[0]['type'] === 'credit';
    }

    private function isIndividualBundleSoleProduct(array $addedProducts)
    {
        return count($addedProducts) === 1 && $addedProducts[0] instanceof Bundle && $addedProducts[0]->isIndividualBundle();
    }

    /**
     * AJAX action to assign a product to all users of the current admin.
     *
     * @param sfWebRequest $request
     * @return array
     */
    public function executeAssignProductToAllUsersAJAX(sfWebRequest $request)
    {
        $this->forward404Unless($request->isXmlHttpRequest());

        /** @var sfUser $user */
        $user = $this->getUser();
        $locale = $user->getCulture();
        $response = new AJAXResponse($this->getResponse());

        $productIds = $this->retrievePinAssignmentProductIdsFromRequest($request);
        $accountProducts = plusCommon::retrieveAssignedProducts($user->getAttribute('account_id'), $locale);
        $bundles = plusCommon::retrieveBundleProducts($locale);
        if (!$this->validateProductIds($productIds, $accountProducts, $bundles)) {
            $response->addAlertErrorMessage('Invalid product id(s).');
        } else {
            plusCommon::assignAllProductsToAllPinsOfContact($user->getAttribute('contact_ref'), $productIds);
            $response->addContent('success', true);
        }

        return $response->outputResponse();
    }

    public function executeAssignIndividualBundleAJAX(sfWebRequest $request)
    {
        $this->forward404Unless($request->isXmlHttpRequest());

        /** @var sfUser $user */
        $user = $this->getUser();
        $locale = $user->getCulture();
        $response = new AJAXResponse($this->getResponse());
        $chairmanPin = $request->getPostParameter('user');
        $bundleId = $request->getPostParameter('product-id');
        $accountProducts = plusCommon::retrieveAssignedProducts($user->getAttribute('account_id'), $locale);
        $bundles = plusCommon::retrieveBundleProducts($locale);
        $contactPinMap = plusCommon::retrieveContactPinMap($user->getAttribute('contact_ref'));

        if ($chairmanPin === null || !isset($contactPinMap[$chairmanPin])) {
            $response->addAlertErrorMessage('Invalid chairman pin.');
        } else if ($bundleId === null || !isset($accountProducts[$bundleId]) || !$this->isIndividualBundle($bundles, $bundleId)) {
            $response->addAlertErrorMessage('Invalid bundle id.');
        } else {
            $contactPin = $contactPinMap[$chairmanPin];
            plusCommon::assignProductToPins(array($contactPin['chairman_pin_ref'], $contactPin['participant_pin_ref']), $bundleId);
            $response->addContent('success', true);
        }

        return $response->outputResponse();
    }

    private function handleInvalidBundlesInBasket(Basket $basket)
    {
        if ($basket->hasInvalidBundles()) {
            $this->redirect('basket');
        }
    }

    /**
     * Page to start the Recurring Payment Agreement setup.
     *
     * @param sfWebRequest $request
     * @author Maarten Jacobs
     * @author Asfer Tamimi
     */
    public function executeRecurringPaymentAgreementSetupPage(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user = $this->getUser();

        $accountId = $user->getAccountId();
        $basket    = new Basket($user);

        $this->handleInvalidBundlesInBasket($basket);

        $this->setVar('topupForm', new topupForm());

        $address = Hermes_Client_Rest::call(
            'Plus.getPlusCreditCardAddressWithFallbackToBilling',
            array(
                'account_id' => $accountId
            )
        );
        if (isset($address['country'])) {
            $country = $address['country'];
        } else {
            $country = 'GBR';
        }
        if (isset($address['email'])) {
            $email = $address['email'];
        } else {
            $email = $user->getAttribute('email');
        }

        $paymentGatewayDetails = sfConfig::get('app_futurePay');
        $this->setVar('cartId', md5(uniqid($user->getAttribute('email'), true)));

        $applicationEnvironment = $user->getApplicationEnvironment();
        $testMode = ($applicationEnvironment && in_array($applicationEnvironment, array('dev', 'sqi', 'staging')))
            ? 100 : $paymentGatewayDetails['testMode'];

        sfContext::getInstance()->getConfiguration()->loadHelpers('countryCodes');
        $this->setupFuturePayFormData(
            $this->getVar('cartId'),
            $paymentGatewayDetails,
            $email,
            three2two($country),
            $testMode
        );

        $session = new PaymentSession($this->getVar('cartId'), $accountId);
        $session->setAutotopup(true);
        if ($request->getParameter('skip-purchase') === null) {
            $session->setAddedProducts(base64_encode(serialize($basket->retrieveAddedProducts())));
        }
        if (is_string($request->getParameter('affiliate-code'))) {
            $session->setAffiliateCode($request->getParameter('affiliate-code'));
        }
        plusCommon::storePaymentSession($session);

        // Update the Product Selector Tracking Table
        $productSelectorId = $this->retrieveProductSelectorToolId();
        if ($productSelectorId) {
            Common::updateProductSelector(
                array(
                    'id' => $productSelectorId,
                    'proceed_to_payment' => true,
                )
            );
            $this->setVar('productSelectorTool', 1);
        } else {
            $this->setVar('productSelectorTool', 0);
        }
    }

    private function setupFuturePayFormData($cartID, array $paymentGatewayDetails, $email, $country, $testMode)
    {
        $this->postLink = $paymentGatewayDetails['liveurl'];
        $testModeVal = (int) $testMode;
        if ($testModeVal !== 0) {
            $this->postLink = $paymentGatewayDetails['testurl'];
        }

        $this->accountId = $paymentGatewayDetails['accountID'];
        $this->installationId = $paymentGatewayDetails['installationID'];
        $this->cardTypes = array(
            ''     => 'Select...',
            'AMEX' => 'American Express',
            'MSCD' => 'MasterCard',
            'JCB'  => 'JCB',
            'MAES' => 'Maestro',
            'VISA' => 'Visa'
        );
        $this->country = $country;
        $this->signature = md5("r1chm0nd:{$this->cartId}:$country");
        $this->email = $email;
        $this->testMode = $testMode;
        $this->successUrl = $this->generateUrl($paymentGatewayDetails['successUrl'], array('cart_id' => $cartID), true);
        $this->cancelUrl = $this->generateUrl($paymentGatewayDetails['cancelUrl'], array(), true);
        $this->callbackUrl = $this->generateUrl($paymentGatewayDetails['mainCallback'], array(), true);
    }

    /**
     * @param sfWebRequest $request
     * @author Maarten
     * @author Asfer
     */
    public function executeRecurringPaymentAgreementSetupSuccess(sfWebRequest $request)
    {
        /** @var myUser $user */
        $user    = $this->getUser();

        $cartId  = $request->getParameter('cart_id');
        $session = plusCommon::retrievePaymentSession($cartId);
        $this->forward404If($session->getAccountId() !== $user->getAccountId());

        if ($session) {
            $this->setVar('hasRecentSession', true);
            $this->setVar('hasProcessedPayment', $session->hasProcessedPayment());
            if ($this->getVar('hasProcessedPayment')) {
                $basket = new Basket($user);
                $basket->clearBasket();

                $summaryDetails      = unserialize(base64_decode($session->getProcessedPayment()));
                $basketDisplay       = new BasketDisplay();
                $this->setVar('addedProducts', $basketDisplay->prepareProductsForBasketDisplay(
                    $summaryDetails['added_products']
                ));
                $this->setVar('paymentTime', $session->getCreationTime());

                $accountInfo  = plusCommon::getAccountBalanceAndUsers(
                    $user->getAccountId(),
                    $user->getContactRef()
                );

                $this->setVar('managesUsers', ($accountInfo['accountUsers'] === 'M'));
                $this->setVar('accountInfo', $accountInfo);
                $this->determineMessageAndButtonsToShow(
                    $summaryDetails['added_products'],
                    $this->getVar('managesUsers')
                );
            }
        }

        // Update the Product Selector Tracking Table
        $productSelectorId = $this->retrieveProductSelectorToolId();
        if ($productSelectorId) {
            Common::updateProductSelector(array('id' => $productSelectorId, 'transaction_successful' => true));
            $this->setVar('productSelectorTool', 1);
        } else {
            $this->setVar('productSelectorTool', 0);
        }
    }

    public function executeRecurringPaymentAgreementSetupCancelled()
    {

    }

    private function validateProductIds(array $productIds, array $assignedProducts, array $bundleProducts) {
        foreach ($productIds as $productId) {
            if ($productId === null || !isset($assignedProducts[$productId]) || $this->isIndividualBundle($bundleProducts, $productId)) {
                return false;
            }
        }
        return true;
    }

    private function retrievePinAssignmentProductIdsFromRequest(sfWebRequest $request)
    {
        $productIdsParam = explode(',', $request->getPostParameter('product-ids', ''));
        $productIds = $request->getPostParameter('product-id', $productIdsParam);
        if ($productIds !== null && !is_array($productIds)) {
            $productIds = array($productIds);
        }
        return $productIds;
    }

    private function isIndividualBundle(array $bundleProducts, $productId)
    {
        return isset($bundleProducts[$productId]) && $bundleProducts[$productId]['is_individual_bundle'];
    }

    private function loadAccountInfo()
    {
        if (!isset($this->accountInfo)) {
            $user = $this->getUser();
            $this->accountInfo = plusCommon::getAccountBalanceAndUsers(
                $user->getAttribute('account_id'),
                $user->getAttribute('contact_ref'),
                true
            );
        }
    }

    /**
     * @return myUser
     */
    public function getUser()
    {
        return parent::getUser();
    }

    /**
     * Returns the Product Selector ID
     * @return int|null
     */
    private function retrieveProductSelectorToolId()
    {
        return isset($_SESSION['ProductSelector_id']) ? $_SESSION['ProductSelector_id'] : null;
    }

    private function formatPaymentPeriod($paymentPeriod)
    {
        return date('F Y', strtotime(sprintf('%d-%d-01', $paymentPeriod['year'], $paymentPeriod['month'])));
    }
}
