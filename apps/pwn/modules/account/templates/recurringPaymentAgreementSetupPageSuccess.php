<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix recurring-payment-agreement-setup">
    <div class="grid_24">
        <h1 class="mypwn_sub_page_heading mypwn_sub_page_heading_l mypwn_sub_page_heading_plus">Future Payment</h1>
        <p>Payment for this bundle will be taken from the outstanding available balance on your account. We are requesting your credit card type and details for future monthly payments of your bundles.</p>

        <h3 class="blue"><?php echo __('Select credit card type:'); ?></h3>
        <form action="<?php echo $postLink ?>" method="post" id="frm-payment-method">
            <select name="paymentType">
                <?php foreach ($cardTypes as $key => $card): ?>
                <option value="<?php echo $key ?>"><?php echo $card ?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="accId1" value="<?php echo $accountId ?>"/>
            <input type="hidden" name="instId" value="<?php echo $installationId ?>">
            <input type="hidden" name="testMode" value="<?php echo $testMode ?>">
            <input type="hidden" name="currency" value="GBP">
            <input type="hidden" name="cartId" value="<?php echo $cartId ?>">
            <input type="hidden" name="option" value="0">
            <input type="hidden" name="futurePayType" value="limited">
            <input type="hidden" name="signature" value="<?php echo $signature ?>"/>
            <input type="hidden" name="country" value="<?php echo $country ?>"/>
            <input type="hidden" name="email" value="<?php echo $email ?>"/>
            <input type="hidden" name="MC_callback" value="<?php echo $callbackUrl ?>"/>
            <input type="hidden" name="C_url_success" value="<?php echo $successUrl ?>"/>
            <input type="hidden" name="C_url_cancel" value="<?php echo $cancelUrl ?>"/>
            <input type="submit" value="Make Purchase">
        </form>
    </div>
    <div id="product-selector-tracking" style="display: none;" data-dotracking="<?php echo $productSelectorTool; ?>"></div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>

<script>
    $(document).ready(function(){
        $('#frm-payment-method').click(function(){
            var doPstTracking = $('#product-selector-tracking').data('dotracking');
            if (doPstTracking) {
                dataLayer.push({'event': 'ProductSelectorTool/ProceedToPayment'});
            }
        });
    });
</script>