
<?php use_javascript('/shared/jQuery/jquery.pwnform.js'); ?>

<?php use_javascript('/sfjs/forms/formUpgradeFreeToPlus.js'); ?>

<?php echo form_tag(url_for('account/UpgradeFreeToPlus'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'upgradeFreeToPlus', 'class' => 'pwn-form pwnform')) ?>  

<h3><?php echo __('Upgrade free to plus account'); ?></h3>

<?php echo use_helper('pwnForm'); ?>

<div>
    <?php echo render_form_field($form, 'email'); ?>
</div>
<div>
    <?php echo render_form_field($form, 'password'); ?>
</div>
<div>
    <?php echo render_form_field($form, 'first_name'); ?>
</div>
<div>
    <?php echo render_form_field($form, 'last_name'); ?>
</div>
<div>
    <?php echo render_form_field($form, 'business_phone'); ?>
</div>
<?php echo $form->renderHiddenFields() ?>
<button id="frm-plus-registration-submit" type="submit" class="button-orange"><span><?php echo __('Upgrade account to plus now'); ?></span></button>

</form>
