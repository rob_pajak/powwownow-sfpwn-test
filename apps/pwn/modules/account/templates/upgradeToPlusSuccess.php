<?php include_component('commonComponents', 'header'); ?>
<div class="container_24">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => 'Powwownow Plus', 'user_type' => 'plus', 'headingSize' => 'l', 'disableBreadcrumbs' => true)); ?>
    <?php include_partial('UpgradeToPlusForm', array('form' => $form, 'email' => $email)); ?>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
