<?php include_component('commonComponents', 'header'); ?>

<div class="container_24">

    <div class="grid_24 alpha omega">
        <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Account Upgraded'), 'disableBreadcrumbs' => true)); ?>
    </div>

    <div class="grid_5 alpha">
        <?php include_component('commonComponents', 'leftNav'); ?>
    </div>

    <div class="grid_19 omega">  
        <p><?php echo __('Account has been upgraded successfully.'); ?></p>
    </div> 
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div> 