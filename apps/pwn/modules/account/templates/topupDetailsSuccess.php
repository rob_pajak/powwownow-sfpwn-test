<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix purchase-confirmation">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('Purchase Confirmation'), 'headingSize' => 'l', 'disableBreadcrumbs' => true)
    ); ?>

    <div class="grid_24 clearfix">
        <?php if (isset($details['error_found']) && $details['error_found'] === true) : ?>
            <div class="message error">
                <?php echo $details['error_message'];?>
            </div>
        <?php else : ?>
            <?php
                $flowBarData = array('step' => $step);

                // Override balance and/or minimum cost if set.
                if (isset($minimumCost)) {
                    $flowBarData['minimumCredit'] = $minimumCost;
                }
                if (isset($balance)) {
                    $flowBarData['balance'] = $balance;
                }
                if (isset($flowType)) {
                    $flowBarData['flowType'] = $flowType;
                }
                include_component('commonComponents', 'FlowBar', $flowBarData);
            ?>
            <h2 class="rockwell blue"><?php echo __('Order No. ') . $transaction; ?></h2><br/>
            <form class="pwnform clearfix" id="topUpDetailsForm" enctype="application/x-www-form-urlencoded" novalidate="novalidate">
                <div class="grid_12">
                    <h3><?php echo __('Purchase information');?></h3>
                    <div class="form-field">
                        <label><?php echo __('Purchased by');?></label>
                        <span class="mypwn-input-container input-large">
                            <?php echo $details['organisation']; ?>
                        </span>
                    </div>
                    <div class="form-field">
                        <label><?php echo __('Transaction Date');?></label>
                        <span class="mypwn-input-container input-large">
                            <?php echo $details['order_creation_time']; ?>
                        </span>
                    </div>
                    <div class="form-field">
                        <label><?php echo __('Payment Method');?></label>
                        <span class="mypwn-input-container input-large">
                            &nbsp;
                        </span>
                    </div>
                    <div class="form-field">
                        <label><?php echo __('Status');?></label>
                        <span class="mypwn-input-container input-large">
                            <?php echo $details['transaction_status']; ?>
                        </span>
                    </div>
                </div>

                <div class="grid_12">
                    <h3><?php echo __('Items in this order');?></h3>
                    <div class="form-field">
                        <label><?php echo $topupLabel ?></label>
                        <span class="mypwn-input-container input-large">
                            £<?php echo $details['payment_amount']; ?>
                        </span>
                    </div>
                    <div class="form-field">
                        <label>VAT <?php echo ($details['vat_percent']*100).'%'; ?></label>
                        <span class="mypwn-input-container input-large">
                            £<?php echo $details['vat_value']; ?>
                        </span>
                    </div>
                    <div class="form-field">
                        <label><?php echo __('Grand Total');?></label>
                        <span class="mypwn-input-container input-large">
                            £<?php echo $details['payment_amount_post']; ?>
                        </span>
                    </div>
                </div>
            </form>
            <?php include_component('account', 'accountUpdateSummary', array('addedProducts' => $addedProducts)) ?>
            <div class="grid_sub_24">
                <?php if (!empty($confirmationMessage)): ?>
                    <p class="confirmation-message">
                        <?php echo __($confirmationMessage) ?>
                    </p>
                <?php endif; ?>

                <?php if (!empty($showManageUsersButton)): ?>
                    <button class="button-orange floatright" id="assign-products" type="button" data-url="<?php echo url_for('@products_assign'); ?>">
                       <span><?php echo __('Manage Users');?></span>
                    </button>
                <?php endif; ?>

                <?php if ($invoiceDownload) : ?>
                    <button class="button-orange floatright" id="invoice" type="button" data-url="<?php echo url_for('@show_invoice_with_param?payment_id=' . $transaction); ?>">
                        <span><?php echo __('Download Invoice');?></span>
                    </button>
                <?php endif; ?>
            </div>

            <div id="product-selector-tracking" style="display: hidden;" data-dotracking="<?php echo $productSelectorTool; ?>"></div>
            <script>
            $(document).ready(function() {
                $('#topUpDetailsForm').initMypwnForm({
                    successMessagePos: 'none',
                    debug: 'on',
                    html5Validation: 'off',
                    requiredAsteriks: 'off'
                });

                var doPstTracking = $('#product-selector-tracking').data('dotracking');
                if (doPstTracking) {
                    dataLayer.push({'event': 'ProductSelectorTool/TransactionSuccessful'});
                }
            });
            </script>
        <?php endif; ?>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
