<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Account Balance'), 'headingSize' => 'l', 'disableBreadcrumbs' => true)); ?>
    <?php $sf_response->setTitle(__('Account Balance')); ?>

    <?php include_component('account', 'creditBalance'); ?>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>