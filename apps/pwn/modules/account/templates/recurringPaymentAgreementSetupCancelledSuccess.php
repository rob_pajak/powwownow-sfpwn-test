<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix recurring-payment-agreement-setup">
    <h1>Cancelled the setup of the Recurring Payment Agreement.</h1>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
