<?php foreach ($listAddresses as $address) : ?>
<?php if ($address['address_type'] == 'billing') $billingAddress = $address ?>
<?php if ($address['address_type'] == 'admin') $adminAddress = $address ?>
<?php endforeach; ?>

<p>Please complete the following information:</p>

<?php echo form_tag(url_for('account/updateAddress'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-plus-address', 'class' => 'pwnform clearfix')); ?>  
    <div class="grid_12">
        <h3 class="rockwell blue"><?php echo __('Billing Address');?></h3>
        <p><?php echo __('Please enter the billing address.');?></p>

            <div class="form-field">
                <?php echo $form['billing_organisation']->renderLabel('Organisation'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['billing_organisation']->render(array('class' => 'mypwn-input input-large font-small required', 'value' => (isset($billingAddress)) ? $billingAddress['organisation'] : '' ));?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['billing_line1']->renderLabel('Line 1'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['billing_line1']->render(array('class' => 'mypwn-input input-large font-small required', 'value' => (isset($billingAddress)) ? $billingAddress['building'] : '' ));?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['billing_line2']->renderLabel('Line 2'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['billing_line2']->render(array('class' => 'mypwn-input input-large font-small', 'value' => (isset($billingAddress)) ? $billingAddress['street'] : '' ));?>
                </span>
            </div>    
            <div class="form-field">
                <?php echo $form['billing_town']->renderLabel('Town'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['billing_town']->render(array('class' => 'mypwn-input input-large font-small required', 'value' => (isset($billingAddress)) ? $billingAddress['town'] : '' ));?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['billing_county']->renderLabel('County'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['billing_county']->render(array('class' => 'mypwn-input input-large font-small', 'value' => (isset($billingAddress)) ? $billingAddress['county'] : '' ));?>
                </span>
            </div>    
            <div class="form-field">
                <?php echo $form['billing_postcode']->renderLabel('Postcode'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['billing_postcode']->render(array('class' => 'mypwn-input input-large font-small required', 'pattern' => '^([A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKS-UWa-hjks-uw])\ {0,1}[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|([Gg][Ii][Rr]\ 0[Aa][Aa])|([Ss][Aa][Nn]\ {0,1}[Tt][Aa]1)|([Bb][Ff][Pp][Oo]\ {0,1}([Cc]\/[Oo]\ )?[0-9]{1,4})|(([Aa][Ss][Cc][Nn]|[Bb][Bb][Nn][Dd]|[BFSbfs][Ii][Qq][Qq]|[Pp][Cc][Rr][Nn]|[Ss][Tt][Hh][Ll]|[Tt][Dd][Cc][Uu]|[Tt][Kk][Cc][Aa])\ {0,1}1[Zz][Zz]))$', 'value' => (isset($billingAddress)) ? $billingAddress['postal_code'] : '' ));?>
                </span>
            </div>  

            <div class="form-field">
                <?php echo $form['billing_country']->renderLabel('Country'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['billing_country']->render(array('class' => 'mypwn-input input-large font-small required'));?>
                </span>
            </div>  

    </div>


    <div class="grid_12">
        <h3 class="rockwell blue"><?php echo __('Admin Address');?></h3>
        <p><?php echo __('Please enter the address of your Account Administrator.');?></p>

            <div class="form-field">
                <?php echo $form['admin_organisation']->renderLabel('Organisation'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['admin_organisation']->render(array('class' => 'mypwn-input input-large font-small required', 'value' => (isset($adminAddress)) ? $adminAddress['organisation'] : '' ));?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['admin_line1']->renderLabel('Line 1'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['admin_line1']->render(array('class' => 'mypwn-input input-large font-small required', 'value' => (isset($adminAddress)) ? $adminAddress['building'] : ''));?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['admin_line2']->renderLabel('Line 2'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['admin_line2']->render(array('class' => 'mypwn-input input-large font-small', 'value' => (isset($adminAddress)) ? $adminAddress['street'] : ''));?>
                </span>
            </div>    
            <div class="form-field">
                <?php echo $form['admin_town']->renderLabel('Town'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['admin_town']->render(array('class' => 'mypwn-input input-large font-small required', 'value' => (isset($adminAddress)) ? $adminAddress['town'] : ''));?>
                </span>
            </div>
            <div class="form-field">
                <?php echo $form['admin_county']->renderLabel('County'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['admin_county']->render(array('class' => 'mypwn-input input-large font-small', 'value' => (isset($adminAddress)) ? $adminAddress['county'] : ''));?>
                </span>
            </div>    
            <div class="form-field">
                <?php echo $form['admin_postcode']->renderLabel('Postcode'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['admin_postcode']->render(array('class' => 'mypwn-input input-large font-small required', 'pattern' => '^([A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKS-UWa-hjks-uw])\ {0,1}[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|([Gg][Ii][Rr]\ 0[Aa][Aa])|([Ss][Aa][Nn]\ {0,1}[Tt][Aa]1)|([Bb][Ff][Pp][Oo]\ {0,1}([Cc]\/[Oo]\ )?[0-9]{1,4})|(([Aa][Ss][Cc][Nn]|[Bb][Bb][Nn][Dd]|[BFSbfs][Ii][Qq][Qq]|[Pp][Cc][Rr][Nn]|[Ss][Tt][Hh][Ll]|[Tt][Dd][Cc][Uu]|[Tt][Kk][Cc][Aa])\ {0,1}1[Zz][Zz]))$', 'value' => (isset($adminAddress)) ? $adminAddress['postal_code'] : ''));?>
                </span>
            </div>  

            <div class="form-field">
                <?php echo $form['admin_country']->renderLabel('Country'); ?>
                <span class="mypwn-input-container">
                    <?php echo $form['admin_country']->render(array('class' => 'mypwn-input input-large font-small required'));?>
                </span>
            </div>  

    </div>


    <div class="form-action margin-top-10 grid_24">
        <button class="button-orange" type="submit">
            <span><?php echo __('Save Addresses'); ?></span>
        </button>
    </div>   
</form>

<script type="text/javascript">
$(document).ready(function () {
    var options = {
        successMessagePos: 'above',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'on'
    };
    $('#frm-plus-address').initMypwnForm(options);
});

</script>
