<h3 class="blue"><?php echo __('Current account balance'); ?></h3>
<p class="account-balance"><?php echo (!$currentBalance) ? __('Currently Unavailable') : format_currency($currentBalance, 'GBP'); ?></p>
