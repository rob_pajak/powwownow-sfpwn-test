<?php include_component('commonComponents', 'header'); ?>
<!-- hidden redirect popup -->
<?php include_partial('redirectPopup') ?>

<div class="container_24 clearfix">
    <!-- page header -->

    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Auto Top-up'), 'headingSize' => 'm','breadcrumbs' => $breadcrumbs, 'disableBreadcrumbs' => true)); ?>
    <?php $sf_response->setTitle(__('Auto Top-up')); ?>

    <!-- page content -->
        <?php if ($enabled): ?>
            <div class="auto-top-up-enabled-content">
                <div class="grid_24">
                    <p><?php echo __('Auto Top-up is currently enabled on your account. By keeping this enabled on your account you will be able to make conference calls whenever you like without your account falling below the minimum balance of &pound;5.00, which is required to make a call.',
                                     array('%1%' => format_currency($topUpAmount, $currency))); ?>
                    </p>
                </div>
                <div class="grid_12">
                    <?php echo $autoTopUpform->renderFormTag(url_for('account/autoTopUpAjax'),
                                                             array('enctype' => 'application/x-www-form-urlencoded',
                                                                   'id'      => 'form-auto-topup',
                                                                   'class'   => 'pwnform clearfix')); ?>

                        <p style="margin-bottom:10px;"><?php echo __('If you wish to disable Auto Top-up on your account then click below.'); ?></p>                                               
                        <?php echo $autoTopUpform->submitButton('Disable Auto Top-Up', array('style' => 'text-align:left;')); ?>

                    </form>
                </div> 

                <div class="clear"></div>

                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#form-auto-topup').initMypwnForm({
                            successMessagePos: 'off',
                            success: function (responseText) { 
                                        var success_message = responseText.success_message;
                                         $('#topup-status').html(window[success_message]);
                                         $('.auto-top-up-enabled-content').hide();
                                         $('.auto-top-up-disabled-content').show();
                                     },
                            beforeSubmit: function () {return confirm(MYPWN_AUTO_TOPUP_DISABLE_CONFIRM)},
                            html5Validation: 'off'
                        });
                    });
                </script>

            </div>
        <?php endif; ?>

        <div class="auto-top-up-disabled-content" <?php if($enabled){echo 'style="display:none;"';}?>>
            <div class="grid_24">
                <p><?php echo __('Auto Top-up is currently disabled on your account. To help save you time in the future and allow you to conference call whenever you wish you can enable Auto Top-up now or next time you %1%.', array('%1%' => link_to(_('purchase credit'), 'account/purchaseCredit')));?></p>
                <p><?php echo __('Auto Top-up is a quick and easy to enable and works by topping up your account automatically when your account balance falls below &pound;5.00.');?></p>
                <p><?php echo __('Your balance will be automatically topped up using the same amount and payment method as your most recent transaction. Your Auto Top-up settings can be cancelled at any time.');?></p>
            </div>
        </div>

    <!-- page footer -->
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
