<?php echo use_helper('pwnForm'); ?>
<?php include_component('commonComponents', 'header'); ?>

<div class="container_24">

    <div class="grid_24 alpha omega">
        <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Account Addresses'), 'headingSize' => 'l','breadcrumbs' => $breadcrumbs, 'disableBreadcrumbs' => true)); ?>
        <?php $sf_response->setTitle(__('Account Addresses')); ?>
    </div>

    <div class="grid_24">  
        <?php include_partial('addressForm', array('form' => $form, 'listAddresses' => $listAddresses)); ?>
    </div> 

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>