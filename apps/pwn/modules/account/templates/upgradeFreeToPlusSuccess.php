<?php include_component('commonComponents', 'header'); ?>

<div class="container_24">

    <div class="grid_24 alpha omega">
        <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Upgrade'), 'disableBreadcrumbs' => true)); ?>
    </div>

    <div class="grid_5 alpha">
        <?php include_component('commonComponents', 'leftNav'); ?>
    </div>

    <div class="grid_19 omega">  
        <?php include_partial('upgradeFreeToPlusForm', array('form' => $form)); ?>
    </div> 
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div> 







