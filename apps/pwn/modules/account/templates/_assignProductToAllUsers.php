<?php if (empty($product['extra_data']['is_individual_bundle'])): ?>
    <?php if (!$product['assigned_to_all_users']): ?>
        <?php echo form_tag('assign_product_to_all_users', array('class' => 'assign-product-to-all-users')) ?>
            <button class="button-green" type="submit" name="product-id" value="<?php if (!empty($product['extra_data']['has_addon']))
                                                                                          echo $product['extra_data']['addon']['product_id'];
                                                                                      else
                                                                                          echo $product['product_id'] ?>">
                <span><?php echo __('Assign to all users') ?></span>
            </button>
        </form>
        <p class="assigned-to-all-users message success not-show"><?php echo __('Assigned to all users') ?></p>
    <?php else: ?>
        <p class="assigned-to-all-users message success"><?php echo __('Assigned to all users') ?></p>
    <?php endif; ?>
<?php else: ?>
    <?php if (!$individualBundleIsAssigned): ?>
        <div id="individual-bundle-forms">
            <?php echo $individualBundleForm->renderFormTag(url_for('@assign_individual_bundle'), array('id' => 'assign-individual-bundle')); ?>
                <?php echo $individualBundleForm['user']->render() ?>
                <input type="hidden" name="product-id" value="<?php echo $product['product_id'] ?>" />
                <button class="button-green" type="submit">
                    <span><?php echo __('Assign to User'); ?></span>
                </button>
            </form>
            <button class="button-orange" type="button" id="create-user-for-individual-bundle" data-url="<?php echo url_for('@assign_individual_bundle') ?>">
                <span><?php echo __('Create user to assign bundle'); ?></span>
            </button>
        </div>
        <p class="assigned-to-user message success not-show"><?php echo __('Assigned to user') ?></p>
    <?php else: ?>
        <p class="assigned-to-user message success"><?php echo __('Assigned to user') ?></p>
    <?php endif; ?>
<?php endif; ?>