<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix recurring-payment-agreement-setup">
    <h1 class="mypwn_sub_page_heading mypwn_sub_page_heading_xl mypwn_sub_page_heading_plus">
        Future Payment Confirmation
    </h1>
     <p>Your future payment has been successfully set-up. You have purchase the following products using the outstanding balance on your account.</p>

    <?php if ($hasRecentSession): ?>
        <?php if ($hasProcessedPayment && !empty($addedProducts)): ?>
            <?php include_partial(
                'products/basketProductTable',
                array(
                    'addedProducts' => $addedProducts,
                    'disableProductActions' => true,
                )
            ); ?>

            <?php if (!empty($confirmationMessage)): ?>
                <p class="confirmation-message">
                    <?php echo __($confirmationMessage) ?>
                </p>
            <?php endif; ?>

            <?php if (!empty($showManageUsersButton)): ?>
                <button class="button-orange floatright" id="assign-products" type="button" data-url="<?php echo url_for('@products_assign'); ?>">
                    <span><?php echo __('Manage Users');?></span>
                </button>
            <?php endif; ?>
        <?php else: ?>
            <?php // to be considered ?>
        <?php endif; ?>
    <?php endif; ?>

    <div id="product-selector-tracking" style="display: none;" data-dotracking="<?php echo $productSelectorTool; ?>"></div>

    <script>
        $(document).ready(function() {
            var doPstTracking = $('#product-selector-tracking').data('dotracking');
            if (doPstTracking) {
                dataLayer.push({'event': 'ProductSelectorTool/TransactionSuccessful'});
            }

            $('#assign-products').one('click', function() {
                window.location = $(this).data('url');
            });
        });
    </script>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
