<?php
if ($success === true){
    echo json_encode(array(
        'success_message' => '',
        'pleaseWaitMsg' => 'Redirecting to Payment Gateway...',
        'data' => $sf_data->getRaw('data'),
        'postLink' => $postLink
    ));
}else{
    echo json_encode(array(
        'error_messages' => $sf_data->getRaw('error_messages')
    ));
}