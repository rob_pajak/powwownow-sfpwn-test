<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <div class="grid_24 clearfix">
        <?php include_component(
            'commonComponents',
            'subPageHeader',
            array(
                'title'              => __('Purchase History'),
                'headingSize'        => 'l',
                'breadcrumbs'        => array('Account Details', '/myPwn/Account-Details'),
                'disableBreadcrumbs' => true
            )
        ); ?>
    </div>
    <div class="grid_24 clearfix">
        <p><?php echo __('Here you can view your purchase history as far back as 6 months; and product purchases can be filtered by date and type.');?></p>
        <p><?php echo __('Your purchase history can also be sorted by number, item, amount or status with the option to view or download past purchases and invoices. ');?></p>
        <?php echo form_tag(url_for('account/filterTopupHistory'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-plus-topup-history', 'class' => 'pwnform clearfix')); ?>
            <div class="grid_24">
                <h3 class="rockwell blue"><?php echo __('Filters');?></h3>
            </div>
            <div class="clear"></div>
            <div class="grid_sub_12">
                <div class="form-field">
                    <?php echo $form['topup_start_date']->renderLabel('From Date'); ?>
                    <span class="mypwn-input-container">
                    <?php echo $form['topup_start_date']->render(
                        array(
                            'class'    => 'mypwn-input input-large font-small',
                            'type'     => 'date',
                            'value'    => date("Y-m-d", strtotime("-6 months")),
                            'data-min' => date("Y-m-d", strtotime("-6 months")),
                            'data-max' => date('Y-m-d'),
                            'required' => 'required'
                        )
                    ); ?>
                </span>
                </div>
            </div>
            <div class="grid_sub_12">
                <div class="form-field">
                    <?php echo $form['topup_end_date']->renderLabel('To Date'); ?>
                    <span class="mypwn-input-container">
                    <?php echo $form['topup_end_date']->render(
                        array(
                            'class'    => 'mypwn-input input-large font-small',
                            'type'     => 'date',
                            'value'    => date("Y-m-d"),
                            'min'      => date("Y-m-d", strtotime("-6 months")),
                            'max'      => date('Y-m-d'),
                            'required' => 'required'
                        )
                    ); ?>
                </span>
                </div>
            </div>
            <div class="grid_sub_24">
                <div class="form-field">
                    <?php echo $form['topup_item_type']->renderLabel('Transaction Type'); ?>
                    <span class="mypwn-input-container">
                    <?php echo $form['topup_item_type']->render(array('class' => 'mypwn-input input-large font-small'));?>
                </span>
                </div>
            </div>
            <div class="form-action margin-top-10 grid_sub_24">
                <button class="button-orange" type="submit">
                    <span><?php echo __('Filter'); ?></span>
                </button>
            </div>
        </form>
    </div>
    <div class="grid_24 clearfix margin-top-20">
        <table class="mypwn plus floatleft" id="topup_history_screen"></table>
        <div id="transaction">
            <h2 id="transaction_title"></h2>
            <div style="width: 50%; float: left;">
                <h3 class="blue"><?php echo __('Purchase information') ?></h3>
                <table id="topup_info" class="manual-invoice">
                    <tr>
                        <td class="tablelabel"><?php echo __('Purchased by') ?></td>
                        <td id="table_purchased_by"></td>
                    </tr>

                    <tr class="auto-invoice">
                        <td class="tablelabel"><?php echo __('Invoice Date') ?></td>
                        <td id="table_invoice_date"></td>
                    </tr>
                    <tr class="manual-invoice">
                        <td class="tablelabel"><?php echo __('Transaction Date') ?></td>
                        <td id="table_transaction_date"></td>
                    </tr>
                    <tr>
                        <td class="tablelabel"><?php echo __('Status') ?></td>
                        <td id="table_transaction_status"></td>
                    </tr>
                </table>
            </div>
            <div style="width: 50%; float: left;">
                <h3 class="blue"><?php echo __('Items in this order') ?></h3>
                <table id="topup_items_info">
                    <tr>
                        <td class="tablelabel" id="table_topup_method">Topup Method</td>
                        <td id="table_topup_amount"></td>
                    </tr>
                    <tr>
                        <td class="tablelabel" id="table_topup_vat_rate"></td>
                        <td id="table_topup_vat_amount"></td>
                    </tr>
                    <tr>
                        <td class="tablelabel"><?php echo __('Grand Total') ?></td>
                        <td id="table_topup_total"></td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="topup_history_print_wrapper_outer">
            <table id="topup_history_print"></table>
        </div>

        <script>
            $(document).ready(function () {
                topUpHistoryFormInitialise('#frm-plus-topup-history');
                topUpHistoryDataTableInitialise(<?php echo $topUpTable; ?>);
            });
        </script>
    </div>
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>
