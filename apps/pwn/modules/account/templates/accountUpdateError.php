<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <h1>Error</h1>
    <p>We apologise, but an error has occurred whilst updating your account.</p>

    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>