<div class="grid_24">
    <h3 class="rockwell blue"><?php echo __('Get the Best of Powwownow');?></h3>
    <p><?php echo __('To get Powwownow Plus, please complete and confirm your details:');?></p>
    <?php echo form_tag(url_for('account/upgradeToPlusAjax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-plus-upgrade', 'class' => 'pwnform clearfix')); ?>  
        <div class="form-field grid_sub_12">
            <label for="email" novalidate="novalidate"><?php echo __('Email');?></label>
            <span class="mypwn-input-container input-large font-small textonly" style="height: 16px;">
                <?php echo $email; ?>
            </span>
        </div>
        <div class="form-field grid_sub_12">
            <?php echo $form['title']->renderLabel('Title'); ?>
            <span class="mypwn-input-container">
                <?php echo $form['title']->render(array('class' => 'mypwn-input input-large font-small'));?>
            </span>
        </div>            
        <div class="form-field grid_sub_12">
            <?php echo $form['first_name']->renderLabel('First Name'); ?>
            <span class="mypwn-input-container">
                <?php echo $form['first_name']->render(array('class' => 'mypwn-input input-large font-small', 'required' => 'required'));?>
            </span>
        </div>
        <div class="form-field grid_sub_12">
            <?php echo $form['last_name']->renderLabel('Last Name'); ?>
            <span class="mypwn-input-container">
                <?php echo $form['last_name']->render(array('class' => 'mypwn-input input-large font-small', 'required' => 'required'));?>
            </span>
        </div>
        <div class="form-field grid_sub_12">
            <?php echo $form['business_phone']->renderLabel('Business Phone'); ?>
            <span class="mypwn-input-container">
                <?php echo $form['business_phone']->render(array('class' => 'mypwn-input input-large font-small', 'required' => 'required'));?>
            </span>
        </div>
        <div class="form-field grid_sub_12">
            <?php echo $form['mobile_phone']->renderLabel('Mobile Phone'); ?>
            <span class="mypwn-input-container">
                <?php echo $form['mobile_phone']->render(array('class' => 'mypwn-input input-large font-small'));?>
            </span>
        </div>
        <div class="form-field grid_sub_24">
            <?php echo $form['company_name']->renderLabel('Company Name'); ?>
            <span class="mypwn-input-container">
                <?php echo $form['company_name']->render(array('class' => 'mypwn-input input-large font-small', 'required' => 'required'));?>
            </span>
        </div>
        <div class="grid_sub_24 clear-both padding-bottom-10">
            <i class="floatright"><?php echo __('*Required Fields');?></i>
        </div>
        <div class="form-hidden">
            <?php echo $form->renderHiddenFields(); ?>
        </div>
        <div class="grid_sub_24 clear-both">
            <div class="message info">
                <h3 class="rockwell"><?php echo __('IMPORTANT NOTICE: Your PIN is changing!'); ?></h3>
                <p><?php echo __('Please note that when clicking CONFIRM we will generate a Chairperson PIN for you, and your current PIN will be converted into your Participant PIN from now on.'); ?></p>
                <p><?php echo __('And because you are creating a new account, any recordings you might have saved in the past will be lost.'); ?></p>
                <p><?php echo __('If you have any questions please contact us on 020 3398 0398.'); ?></p>
            </div>
        </div>
        <div class="form-action grid_sub_24 clear-both margin-top-10">
            <button class="button-orange floatright" type="submit">
                <span><?php echo __('Confirm'); ?></span>
            </button>
        </div>            
    </form>
</div>
<script type="text/javascript">
$(document).ready(function () {
    var options = {
        success: upgradeSuccess,
        successMessagePos: 'above',
        debug: 'off',
        html5Validation: 'off',
        requiredAsteriks: 'on'
    };
    $('#frm-plus-upgrade').initMypwnForm(options);
});

/*This function is called when the Upgrade is Successful*/
function upgradeSuccess(responseText) {
    responseArr = jQuery.parseJSON(responseText);
    window.setTimeout(function (){ window.location = URL_MYPWN_PLUS_CONFIRMATION_PAGE; }, 1000);
}
</script>
