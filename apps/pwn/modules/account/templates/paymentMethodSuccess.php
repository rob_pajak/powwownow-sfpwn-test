<?php include_component('commonComponents', 'header'); ?>
<?php include_partial('redirectPopup'); ?>
<div class="container_24 clearfix">
    <?php include_component(
        'commonComponents',
        'subPageHeader',
        array('title' => __('Payment'), 'headingSize' => 's', 'breadcrumbs' => array(), 'disableBreadcrumbs' => true)
    ); ?>
    <div class="grid_24">
        <?php
        $flowBarArgs = array('step' => $step, 'minimumCredit' => $minimumCreditRequired);
        if (!empty($flowType)) {
            $flowBarArgs['flowType'] = $flowType;
        }
        include_component('commonComponents','FlowBar', $flowBarArgs);
        ?>

        <div class="grid_sub_24 clearfix">
            <div style="width: 44.75%; float: left;">
                <h3 class="blue">Total:</h3>
                <p style="margin-left: 15px;" class="account-balance">&pound;<?php echo sprintf('%01.2f',$totalAmount); ?></p>
                <span> +<?php echo $vatInfo['percentage']; ?>% VAT (&pound;<?php echo sprintf('%01.2f',$grossAmount); ?>)</span>
            </div>
            <?php if($billingAddressWarning): ?>
                <div style="margin-left: 49%; margin-right: 3%;">
                    <p>
                        <span style="color: red; font-size:36px; font-weight:bold;">!</span>
                        We do not have a record of your billing address, if you wish to have this address displayed on your invoices, please complete your details <a href="<?php echo url_for('@account_address'); ?>" title="Billing Address">here</a>.
                    </p>
                </div>
            <?php endif; ?>
        </div>

        <div class="grid_sub_11">
            <h3 class="blue"><?php echo __('Select credit card type:'); ?></h3>
            <?php echo $topupForm->renderFormTag(url_for('account/'.$formAction), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-payment-method', 'class' => 'pwnform clearfix'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'frm-payment-method', 'class' => 'pwnform clearfix')); ?>
                <div class="form-field">
                    <?php echo $topupForm['cardtypes']->render();?>
                </div>
                <?php echo $topupForm['affiliate-code']->render() ?>
                <?php echo $topupForm->submitButton('Continue',array('style' => 'text-align: left; padding-top: 15px;', 'id' => 'topup_account')); ?>
                <input type="hidden" name="testMode" value="<?php echo $testModeValue; ?>"/>
            </form>
        </div>

        <div class="grid_sub_12 push_1">
            <h3 class="blue"><?php echo __('Card types accepted:'); ?></h3>
            <div class="grid_sub_24 card-types">
                <table style="text-align: center; border-spacing: 2px; border-collapse: separate; border: 0;">
                    <tbody>
                        <tr>
                            <td><img style="border: 0;" alt="American Express" src="https://secure.worldpay.com/jsp/shopper/icons/WP_AMEX.gif"></td>
                            <td><img style="border: 0;" alt="MasterCard" src="https://secure.worldpay.com/jsp/shopper/icons/WP_ECMC.gif"></td>
                            <td><img style="border: 0;" alt="JCB" src="https://secure.worldpay.com/jsp/shopper/icons/WP_JCB.gif"></td>
                            <td><img style="border: 0;" alt="Maestro" src="https://secure.worldpay.com/jsp/shopper/icons/WP_MAESTRO.gif"></td>
                            <td><img style="border: 0;" alt="Visa" src="https://secure.worldpay.com/jsp/shopper/icons/WP_VISA.gif"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" colspan="5">
                                <a target="_blank" href="http://www.worldpay.com/support/index.php?CMP=BA22713">
                                    <img style="border: 0;" alt="Powered by WorldPay" src="https://secure.worldpay.com/jsp/shopper/icons/../pictures/poweredByWorldPay.gif">
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <h3 class="blue"><?php echo __('Refund policy'); ?></h3>
            <p><?php echo __('Refunds may be given at the discretion of the management by contacting Customer Services on 020 3398 0398. Our full terms &amp; conditions can be seen <a href="' . url_for('@terms_and_conditions') . '">here</a>'); ?></p>
        </div>

        <div id="product-selector-tracking" style="display: none;" data-dotracking="<?php echo $productSelectorTool; ?>"></div>

        <?php include_component('commonComponents', 'socialMediaFooter'); ?>
        <?php include_component('commonComponents', 'footer'); ?>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#frm-payment-method').initMypwnForm({
            successMessagePos: 'off',
            debug: 'on',
            requiredAsterisk: 'off',
            html5Validation: 'off',
            success: function(responseText) {
                var doPstTracking = $('#product-selector-tracking').data('dotracking');
                if (doPstTracking) {
                    dataLayer.push({'event': 'ProductSelectorTool/ProceedToPayment'});
                }

                var responseArr = $.parseJSON(responseText);
                $('#FadeBox').show();
                $('div#PopupDialog')
                    .prepend('<h2>'+responseArr['pleaseWaitMsg']+'</h2>')
                    .show();
                postToUrl(responseArr['postLink']+'?',responseArr['data'],'POST');
            }
        });
    });
</script>
