<?php include_component('commonComponents', 'header'); ?>
<?php $tmpDesc = $sf_data->getRaw('tmpDesc'); ?>
<div class="container_24 clearfix">
   <?php if (false === $promocode) : ?>
      <?php $sf_response->setTitle(__('Account Update')); ?>
      <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Account Update'), 'headingSize' => 'l','breadcrumbs' => $breadcrumbs, 'disableBreadcrumbs' => true)); ?>
   <?php else : ?>
      <?php $sf_response->setTitle(__('Account Summary')); ?>
      <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Account Summary'), 'headingSize' => 'l','breadcrumbs' => $breadcrumbs, 'disableBreadcrumbs' => true)); ?>
   <?php endif; ?>
   <div class="grid_24">

       <?php if ($errors->count()): ?>
           <div class="errors">
               <p>There were a number of errors during the purchase phase:</p>
               <ul>
                    <?php foreach ($errors as $error): ?>
                        <li class="error"><?php echo $error ?></li>
                    <?php endforeach; ?>
               </ul>
           </div>
       <?php endif; ?>

       <?php if ($messages->count()): ?>
           <div>
               <p>A number of changes were made to your account, as requested:</p>
               <ul>
                   <?php foreach ($messages as $message): ?>
                   <li><?php echo $message ?></li>
                   <?php endforeach; ?>
               </ul>
           </div>
       <?php endif; ?>

      <?php if ((isset($assignedProducts) && count($assignedProducts)>0) || (isset($unassignedProducts) && count($unassignedProducts)>0) || (isset($topupAmount) && $topupAmount>0)) : ?>
         <?php if ((!isset($topupAmount) || $topupAmount == 0) && (false === $promocode)) : ?>
            <?php include_component('commonComponents','FlowBar',array('step' => 'last')); ?>
         <?php endif; ?>
         <?php if (false === $promocode) : ?>
            <p class="margin-bottom-10" style="margin-left:15px;"><?php echo __('You have made the following changes to your account:'); ?></p>
         <?php else : ?>
            <p class="margin-bottom-10 margin-top-10" style="margin-left:15px;"><?php echo __('Your account top-up was successful. Take a look around and get to know your new Plus account. '); ?></p>
         <?php endif; ?>
      <?php endif; ?>

      <?php if (isset($assignedProducts) && count($assignedProducts)>0 && false === $promocode) : ?>
         <div class="grid_sub_24 clearfix">
            <h2 class="margin-bottom-10 rockwell blue"><?php echo __('Enabled Products:'); ?></h2>
            <ul class="chevron-green margin-left-20">
               <?php foreach ($assignedProducts as $i => $productInfo) : ?>
                  <li class="png">
                     <h3 class="rockwell blue"><?php echo $productInfo['group_name']; ?></h3>
                     <p><?php echo $tmpDesc[$productInfo['product_group_id']]; ?></p>
                  </li>
               <?php endforeach; ?>
            </ul>
         </div>
      <?php endif; ?>

      <?php if (isset($unassignedProducts) && count($unassignedProducts)>0) : ?>
         <div class="grid_sub_24 margin-top-50 clearfix">
            <h2 class="margin-bottom-10 rockwell blue"><?php echo __('Disabled Products:'); ?></h2>
            <ul class="chevron-green margin-left-20">
            <?php foreach ($unassignedProducts as $i => $productInfo) : ?>
               <li class="png">
                  <h3 class="rockwell blue"><?php echo $productInfo['group_name']; ?></h3>
               </li>
            <?php endforeach; ?>
            </ul>
         </div>
      <?php endif; ?>

      <?php if (isset($topupAmount) && $topupAmount>0) : ?>
         <div class="grid_sub_24 margin-top-50 clearfix">
            <h2 class="margin-bottom-10 rockwell blue"><?php echo __('Credit Purchased:'); ?></h2>
            <ul class="chevron-green margin-left-20">
               <li class="png">
                  <h3 class="rockwell blue"><?php echo __('You have purchased £' . $topupAmount . ' Credit.'); ?></h3>
                  <p><?php echo __('To view this transaction visit your <a href="'. url_for('account/accountStatement') . '">Account Statement</a> page.'); ?></p>
               </li>
            </ul>
         </div>
      <?php endif; ?>

      <?php if (isset($assignedProducts) && count($assignedProducts)>0 && true === $promocode) : ?>
         <div class="grid_sub_24 clearfix">
            <h2 class="margin-bottom-10 rockwell blue"><?php echo __('Enabled Products:'); ?></h2>
            <ul class="chevron-green margin-left-20 margin-top-20">
               <?php foreach ($assignedProducts as $i => $productInfo) : ?>
                  <li class="png">
                     <h3 class="rockwell blue"><?php echo $productInfo['group_name']; ?></h3>
                  </li>
               <?php endforeach; ?>
            </ul>
         </div>
      <?php endif; ?>

      <?php if (true === $promocode) : ?>
         <div class="grid_sub_24 clearfix">
            <p class="margin-bottom-10 margin-top-10" style="margin-left:15px;"><?php echo __('Want to make a call straight away - go to your dial-in numbers page, or not sure what number is best to use, view your products.'); ?></p>
         </div>
      <?php endif; ?>

      <?php if ((isset($assignedProducts) && count($assignedProducts)>0) || (isset($unassignedProducts) && count($unassignedProducts)>0) || (isset($topupAmount) && $topupAmount>0) || (true === $promocode)) : ?>
         <div class="grid_sub_24">
            <?php echo form_tag(url_for('account/accountUpdateSummaryAjax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-account-update-summary', 'class' => 'pwnform clearfix', 'style' => 'background: none;')); ?>
               <?php if (false === $promocode) : ?>
                  <button type="submit" class="button-orange floatright" id="continue">
                     <span><?php echo __('Finish'); ?></span>
                  </button>
               <?php else : ?>
                  <button class="button-orange" onclick="window.location.href='<?php echo url_for('@products_select') ?>'; return false;" id="promo-select-products">
                     <span>Your Products</span>
                  </button>
                  <button class="button-orange" onclick="window.location.href='/myPwn/Dial-In-Numbers'; return false;" id="promo-dial-in-numbers">
                     <span>Dial-in Numbers</span>
                  </button>
               <?php endif; ?>
            </form>
         </div>
         <script type="text/javascript">
         $(document).ready(function () {
            $('#form-account-update-summary').initMypwnForm({
               successMessagePos: 'off',
               debug: 'off',
               html5Validation: 'off',
               requiredAsterisk: 'off',
               success: function(responseText) {
                  window.setTimeout(function (){ window.location = "/myPwn"; }, 2000);
            }});

            $('#promo-select-products, #promo-dial-in-numbers').click(function () {
               $.ajax({
                  url: '/s/Account-Update-Ajax',
                  type: 'POST',
                  success: function(responseText) {
                     return true;
                  }
               });
            });
         });
         </script>
      <?php endif; ?>

   </div>
   <?php include_component('commonComponents', 'socialMediaFooter'); ?>
   <?php include_component('commonComponents', 'footer'); ?>
</div>
