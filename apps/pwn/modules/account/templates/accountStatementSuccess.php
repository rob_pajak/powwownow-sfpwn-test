<?php echo use_helper('pwnForm'); ?>
<?php $sf_response->addStylesheet('/sfcss/pages/print/print-topupHistory.css','',array('media' => 'print')); ?>
<?php $sf_response->setTitle(__('Account Statement')); ?>

<?php include_component('commonComponents', 'header'); ?>
<div class="container_24 clearfix">
    <?php include_component('commonComponents', 'subPageHeader', array('title' => __('Account Statement'), 'headingSize' => 'l','breadcrumbs' => $breadcrumbs, 'disableBreadcrumbs' => true)); ?>

    <div class="grid_24">  
        <p><?php echo __('You can view your account statement as far back as 6 months and transactions can be filtered by date and type. You can also sort your statement by description, type and amount.');?></p>

        <?php echo form_tag(url_for('account/accountStatementAjax'), array('enctype' => 'application/x-www-form-urlencoded', 'id' => 'form-account-statement', 'class' => 'pwnform clearfix')); ?>
            <div class="grid_sub_24 clearfix">
                <h3 class="rockwell blue"><?php echo __('Filters');?></h3>
            </div>

            <div class="grid_sub_12">
                <div class="form-field">
                    <?php echo $form['ledger_start_date']->renderLabel(); ?>
                    <span class="mypwn-input-container" style="z-index:750;">
                        <?php echo $form['ledger_start_date']->render();?>
                    </span>
                </div>
            </div>

            <div class="grid_sub_12">
                <div class="form-field">
                    <?php echo $form['ledger_end_date']->renderLabel(); ?>
                    <span class="mypwn-input-container"  style="z-index:740;">
                        <?php echo $form['ledger_end_date']->render();?>
                    </span>
                </div>
            </div>

            <div class="grid_sub_24">
                <div class="form-field">
                    <?php echo $form['ledger_type']->renderLabel(); ?>
                    <span class="mypwn-input-container">
                        <?php echo $form['ledger_type']->render();?>
                    </span>
                </div>
            </div>

            <div class="form-action margin-top-10 grid_sub_24">
                <button class="button-orange" type="submit">
                    <span><?php echo __('Filter'); ?></span>
                </button>
            </div>
        </form>
    </div>
    <div class="grid_24 margin-top-20">
        <table cellpadding="0" cellspacing="0" border="0" class="mypwn plus floatleft" id="ledger_history_screen"></table>
        <div id="ledger_history_print_wrapper_outer" style="display:none;">
            <table cellpadding="0" cellspacing="0" border="0" id="ledger_history_print"></table>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function () {
        $('#form-account-statement').initMypwnForm({
            successMessagePos: 'off',
            debug: 'on',
            html5Validation: 'off',
            requiredAsterisk: 'off',
            beforeSubmit: function() {
                var sd = $('#plusledgerhistory_ledger_start_date').val().split("-");
                var ed = $('#plusledgerhistory_ledger_end_date').val().split("-");

                var startDate = new Date( sd[2] + '-' + sd[1] + '-' + sd[0]);
                var endDate = new Date(ed[2] + '-' + ed[1] + '-' + ed[0]);

                if (startDate > endDate) {
                    var errors = [];
                    errors.push({'message': FORM_VALIDATION_INVALID_DATE_RANGE, 'field_name'  : 'plusledgerhistory[ledger_start_date]'});
                    showInputErrorsBoxes(errors, 'form-account-statement');
                    return false;
                }

                return true;
            },
            success: function (responseArr) {
                oTableScreen.fnClearTable();
                oTableScreen.fnAddData(responseArr.tableData);
                oTableScreen.fnSort([[0,'desc']]);

                oTablePrint.fnClearTable();
                oTablePrint.fnAddData(responseArr.tableData);
                oTablePrint.fnSort([[0,'desc']]);

                $('form#form-account-statement').prepend('<div class="success message"><span class="floatleft" style="margin-right: .3em;"></span>' + FORM_VALIDATION_SUCCESS_MESSAGE + '</div>');
                $('form#form-account-statement').find('.message').delay(10000).fadeOut('slow');
            }
        });
    });

    var oTableScreen = '';
    var oTablePrint = '';

    var oLanguage = {
        "sProcessing": DATA_TABLES_sProcessing,
        "sLengthMenu": DATA_TABLES_sLengthMenu,
        "sZeroRecords": DATA_TABLES_sZeroRecords,
        "sEmptyTable": DATA_TABLES_sEmptyTable,
        "sLoadingRecords": DATA_TABLES_sLoadingRecords,
        "sInfo": DATA_TABLES_sInfo,
        "sInfoEmpty": DATA_TABLES_sInfoEmpty,
        "sInfoFiltered": DATA_TABLES_sInfoFiltered,
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sSearch": DATA_TABLES_sSearch,
        "sUrl": "",
        "fnInfoCallback": null,
        "oPaginate": {
            "sFirst":    DATA_TABLES_sFirst,
            "sPrevious": DATA_TABLES_sPrevious,
            "sNext":     DATA_TABLES_sNext,
            "sLast":     DATA_TABLES_sLast
        }
    };

    var aoColumns = [                          
        { "sTitle": "Date" },
        { "sTitle": "Transaction Description" },
        { "sTitle": "Transaction Type" },
        { "sTitle": "Amount" },                             
        { "sTitle": "Balance",  "sWidth" : "100px" }
    ];

    var aaData = <?php echo json_encode($sf_data->getRaw('ledgerTable')) ?>;

    yepnope({
        test : jQuery.fn.dataTable,
        nope : [
            '/shared/jQuery/jquery.dataTables.min.js',
            '/sfcss/vendor/jQuery/datatable.css'
        ],
        complete: function () {
            oTableScreen = $('#ledger_history_screen').dataTable({
                "aaData": aaData,
                "aoColumns": aoColumns,
                "oLanguage" : oLanguage,
                "sPaginationType": "full_numbers"
            });

            oTableScreen.fnSort([[0,'desc']]);

            oTablePrint = $('#ledger_history_print').dataTable({
                "bPaginate":false,
                "bFilter":false,
                "bInfo":false,
                "aaData": aaData,
                "aoColumns": aoColumns,
                "oLanguage": oLanguage,
                "bSort": false
            });

            oTablePrint.fnSort([[0,'desc']]);

            $('#ledger_history th').append('<div></div>').css('text-decoration', 'underline').css('cursor', 'pointer');
        } 
    });
    </script>    
    <?php include_component('commonComponents', 'socialMediaFooter'); ?>
    <?php include_component('commonComponents', 'footer'); ?>
</div>