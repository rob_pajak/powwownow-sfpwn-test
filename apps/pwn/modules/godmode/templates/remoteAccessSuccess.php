<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js ie-oldie" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
<link rel="shortcut icon" href="/favicon.ico"/>
<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<?php include_versioned_stylesheets() ?>
<?php pwn_include_javascript_header(); ?>
<!--[if IE 7]><style type="text/css">.container_24 .grid_6 {width: 23%;}/* hack for IE7 */</style><![endif]-->
</head>
<body>
<div id="gm"></div>
<div id="midbox">
    <h2>myPwn God mode</h2>

    <form id="form-god-mode" action="/Remote-Access-Ajax" method="post" enctype="application/x-www-form-urlencoded" class="pwnform h5form clearfix">
        <input type="hidden" name="contact_ref" value="<?php echo $contact_ref; ?>"/>
        <input type="hidden" name="loginEmail" value="<?php echo $loginEmail; ?>"/>
        <input type="hidden" name="accessorUserReference" value="<?php echo $accessorUserReference ?>"/>

        <label for="country_name">Select a Site</label>
        <select name="country_name" id="country_name">
            <option value="AUT" data-image="/sfimages/blank.gif" data-imagecss="flag flag-aut" data-title="Austria">Austria</option>
            <option value="BEL" data-image="/sfimages/blank.gif" data-imagecss="flag flag-bel" data-title="Belgium">Belgium</option>
            <option value="CAN" data-image="/sfimages/blank.gif" data-imagecss="flag flag-can" data-title="Canada">Canada</option>
            <option value="FRA" data-image="/sfimages/blank.gif" data-imagecss="flag flag-fra" data-title="France">France</option>
            <option value="DEU" data-image="/sfimages/blank.gif" data-imagecss="flag flag-deu" data-title="Germany">Germany</option>
            <option value="IRL" data-image="/sfimages/blank.gif" data-imagecss="flag flag-irl" data-title="Ireland">Ireland</option>
            <option value="ITA" data-image="/sfimages/blank.gif" data-imagecss="flag flag-ita" data-title="Italy">Italy</option>
            <option value="NLD" data-image="/sfimages/blank.gif" data-imagecss="flag flag-nld" data-title="Netherlands">Netherlands</option>
            <option value="POL" data-image="/sfimages/blank.gif" data-imagecss="flag flag-pol" data-title="Poland">Poland</option>
            <option value="ZAF" data-image="/sfimages/blank.gif" data-imagecss="flag flag-zaf" data-title="South Africa">South Africa</option>
            <option value="ESP" data-image="/sfimages/blank.gif" data-imagecss="flag flag-esp" data-title="Spain">Spain</option>
            <option value="SWE" data-image="/sfimages/blank.gif" data-imagecss="flag flag-swe" data-title="Sweden">Sweden</option>
            <option value="CHE" data-image="/sfimages/blank.gif" data-imagecss="flag flag-che" data-title="Switzerland">Switzerland</option>
            <option value="GBR" data-image="/sfimages/blank.gif" data-imagecss="flag flag-gbr" data-title="United Kingdom" selected="selected">United Kingdom</option>
            <option value="USA" data-image="/sfimages/blank.gif" data-imagecss="flag flag-usa" data-title="United States">United States</option>
        </select><br/><br/>

        <label for="god_mode_password">God mode Password</label>
        <input type="password" name="god_mode_password" id="god_mode_password" value=""/><br/><br/>

        <button type="submit" value="Let me in" class="button-green" id="form-create-pin-submit">Let me in</button>
    </form>
    <div id="ip">Your IP address is <strong><?php echo $_SERVER['REMOTE_ADDR']; ?></strong></div>
</div>
<?php pwn_include_javascript_bof(); ?>
<form id="form-god-mode-hidden" action="" method="post" enctype="application/x-www-form-urlencoded">
    <input type="hidden" name="contact_ref" value="<?php echo $contact_ref; ?>"/>
    <input type="hidden" name="loginEmail" value="<?php echo $loginEmail; ?>"/>
    <input type="hidden" name="god_mode_password" id="god_mode_password" value=""/>
</form>
</body></html>