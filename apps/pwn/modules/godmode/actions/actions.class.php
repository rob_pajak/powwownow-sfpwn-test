<?php
/**
 * God Mode - Remote Access Page
 *
 * @package    powwownow
 * @subpackage pages
 * @author     Asfer Tamimi
 *
 */
class godmodeActions extends sfActions
{

    /**
     * Remote-Access [PAGE]
     *
     * Page was put into separate Module due to Filter Conflicts
     *
     * @param sfWebRequest $request
     * @return sfView::SUCCESS
     * @author Asfer Tamimi
     *
     */
    public function executeRemoteAccess(sfWebRequest $request)
    {
        $this->loginEmail            = $request->getPostParameter('loginEmail', null);
        $this->contact_ref           = $request->getPostParameter('loginRef', null);
        $this->accessorUserReference = $request->getPostParameter('accessorUserReference', null);

//        $this->loginEmail            = "stewart.millard@powwownow.com";
//        $this->contact_ref           = 94498;
//        $this->accessorUserReference = '1';

        // Ensure users IP is in our white list
        $ipValidator = new PWNSafeIp();
        $userIp      = $request->getHttpHeader('addr', 'remote');
        try {
            $ipValidator->clean($userIp);
        } catch (Exception $e) {
            $this->logMessage("IP $userIp tried to log in via god mode but is not in the 'allowed IP' list", 'err');
            $this->forward404('Page Not Found');
        }
        return sfView::SUCCESS;
    }

}
