<?php

/**
 * socialMedia actions.
 *
 * @package    powwownow
 * @subpackage socialMedia
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class socialMediaActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {

    $this->getResponse()->setContentType('application/json');
    $this->setLayout(false);
    

  }
}
