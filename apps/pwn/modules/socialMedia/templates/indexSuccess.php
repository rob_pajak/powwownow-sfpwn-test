<?php
if (!cache('socialMediaFooterResponse', 86400)) {
    $data = PWN_Fetch_Feeds::execute();
    include_partial('socialMediaResponsePartial', array('feedData' => $data));
    cache_save();
    $tmp = include_partial('socialMedia/blogItemsPartial', array('feedData' => $data));
}
