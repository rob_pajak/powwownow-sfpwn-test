<?php 
foreach ($sf_data->getRaw('feedData') as $type => $data) {
    $tmp = '';
    $item = 0;
    foreach($data as $i => $k) {
        $tmp .= "<li";
        if ($item++ != 0) { 
            $tmp .= " style=\"display:none;\""; 
        }
        $tmp .= ">".$k['text']."<br /><a href=\"".$k['url']."\" target=\"_blank\">more</a></li>";
    }
    $rsp[$type] = $tmp;
} ?>
<?php echo json_encode($rsp); ?>
