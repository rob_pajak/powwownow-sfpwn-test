<?php

class pwnConfiguration extends sfApplicationConfiguration
{
    public function setup()
    {
        $this->enablePlugins(array('sfSslRequirementPlugin', 'sfDoctrinePlugin'));
    }

    public function configure()
    {
        $this->dispatcher->connect('debug.web.load_panels', array(
          'acWebDebugPanelHermesApi',
          'listenToLoadDebugWebPanelEvent'
        ));
    }
}
