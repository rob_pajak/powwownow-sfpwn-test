<!DOCTYPE html>
<!--[if IE 8]><html class="lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include_http_metas() ?>
    <?php include_title() ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.min.js"></script>

    <?php /** Canonical Links - Slot Overrides default behaviour */ ?>
    <?php if (has_slot('page_canonical')) : ?>
        <?php include_slot('page_canonical'); ?>
    <?php else: ?>
        <?php echo include_canonical_tag(); ?>
    <?php endif; ?>
    <?php /** This are the Social Media Meta Data used for their sites */ ?>
    <?php if (has_slot('page_metaproperties')): ?>
        <?php include_slot('page_metaproperties'); ?>
    <?php endif; ?>

    <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
        <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
        <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
    <![endif]-->

    <?php include_versioned_stylesheets() ?>
    <link rel="stylesheet" type="text/css" media="screen" href="/cx2/stylesheets/components/foundation.5.layout.no.js.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/cx2/stylesheets/components/foundation.5.layout.cookie.policy.css" />
    
</head>
    <body>
    <?php include_partial('commonComponents/nojs', array('template' => 'foundation-5-layout')); ?>
    <?php include_component('commonComponents', 'cookiePolicy', array('template' => 'foundation-5-layout')); ?>

    <?php echo $sf_content; ?>
    <script src="/cx2/js/vendor/prototype.js" type="text/javascript"></script>
    <script src="/shared/jQuery/jquery-1.10.2.js" type="text/javascript"></script>
    <script>jQuery.noConflict();</script>
    <script src="/cx2/js/vendor/knockout-min.js" type="text/javascript"></script>
    <script src="/shared/ext/underscore-min.js" type="text/javascript"></script>
    <script src="/shared/jQuery/jquery.cookie.js" type="text/javascript"></script>
    <script src="/cx2/js/vendor/jquery.velocity.min.js" type="text/javascript"></script>
    <script src="/cx2/js/vendor/velocity.ui.js" type="text/javascript"></script>
    <script src="/shared/ext/placeholders.jquery.min.js" type="text/javascript"></script>
    <script src="/cx2/js/cx.registration.core.js" type="text/javascript"></script>
    <script src="/cx2/foundation.master/js/foundation/foundation.js"></script>
    <script src="/sfjs/components/commonComponents.cookiePolicy.js" type="text/javascript"></script>
    <?php pwn_include_javascript_bof(); ?>
    <!--[if lt IE 9]>
        <script src="/cx2/js/vendor/rem.min.js"></script>
    <![endif]-->
    </body>

</html>