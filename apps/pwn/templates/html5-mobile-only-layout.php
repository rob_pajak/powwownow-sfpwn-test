<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php include_versioned_stylesheets() ?>
    <?php pwn_include_javascript_header(); ?>
</head>
<body>
<?php echo $sf_content; ?>
<?php pwn_include_javascript_bof(); ?>
</body>
</html>
