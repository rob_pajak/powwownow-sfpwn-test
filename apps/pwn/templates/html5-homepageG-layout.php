<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <link rel="shortcut icon" href="/favicon.ico" />
    
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <?php include_title() ?>
    <?php include_versioned_stylesheets() ?>
    <?php pwn_include_javascript_header(); ?>
   
</head>
<body>
<?php echo $sf_content; ?>
<?php pwn_include_javascript_bof(); ?>
</body>
</html>