<?php $device = get_slot('device', 'desktop'); ?>
<!DOCTYPE html>
<!--[if IE 8]><html class="lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include_http_metas() ?>
        <?php include_title() ?>
        <?php include_metas() ?>

        <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.min.js"></script>

        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
            <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
            <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js">ss
        <![endif]-->

        <?php include_versioned_stylesheets() ?>
    </head>
    <body data-attr-device="<?php echo $device?>">
        <?php echo $sf_content; ?>

        <script src="/shared/jQuery/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="/cx2/js/vendor/knockout-min.js" type="text/javascript"></script>
        <script src="/shared/ext/underscore-min.js" type="text/javascript"></script>

        <?php if ($device === 'desktop'):?>
            <script src="/cx2/js/vendor/jquery.velocity.min.js" type="text/javascript"></script>
            <script src="/cx2/js/vendor/velocity.ui.js" type="text/javascript"></script>
            <script src="/shared/ext/placeholders.jquery.min.js" type="text/javascript"></script>
        <?php endif;?>

        <script src="/cx2/js/cx.registration.core.js" type="text/javascript"></script>
        <script src="/cx2/foundation.master/js/foundation/foundation.js"></script>

        <?php pwn_include_javascript_bof(); ?>
        <script>
            $(document).foundation();

            /*global RegistrationInterface, document, ko*/
            ;(function (ko) {
                "use strict";
                var trackingEvents = {
                        'FormSubmitEvent' : 'Generate Pin - Music-on-Hold/onClick',
                        'registrationSuccessEvent': '/Music-on-Hold/RegistrationSuccess'
                    },
                    registrationSource = 'GBR-Music-on-Hold',
                    isLegacy = false;

                var MainRegistration = new RegistrationInterface(trackingEvents, registrationSource, isLegacy);

                ko.applyBindings(MainRegistration, document.getElementById('ko.music-on-hold.registration.container'));
            })(ko);
        </script>

        <!--[if lt IE 9]>
            <script src="/cx2/js/vendor/rem.min.js"></script>
        <![endif]-->
    </body>
</html>