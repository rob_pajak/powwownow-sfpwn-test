<!DOCTYPE html>
<html class="html">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="generator" content="7.2.232.244"/>
    <?php include_title() ?>
    <link rel="canonical" href="http://www.powwownow.co.uk"/>
    <!-- CSS -->
    <?php include_versioned_stylesheets() ?>
    <!-- Other scripts -->
    <script type="text/javascript">
        document.documentElement.className += ' js';
    </script>
    <!--custom head HTML-->
    <?php include_metas() ?>
    <!-- Twitter Card data -->
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:site" content="@powwownow" />
    <meta property="twitter:title" content="The Workplace Can’t – Be a Can with Powwownow, Let’s Get it Done" />
    <meta property="twitter:description" content="We completed a survey in association with OnePoll to find out the identity of ‘Can’ts’ within the UK. Find out the characteristics of a Can’t and where in the UK to find them." />
    <meta property="twitter:creator" content="@powwownow" />
    <meta property="twitter:image" content="http://www.powwownow.co.uk/sfimages/the-workplace-cant/twittercard.jpg" />

    <!-- Open Graph data -->
    <meta property="og:url" content="http://www.powwownow.co.uk/Conference-Call/The-Workplace-Cant" />
    <meta property="og:site_name" content="www.powwownow.co.uk/Conference-Call/The-Workplace-Cant" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="The Workplace Can’t – Be a Can with Powwownow, Let’s Get it Done" />
    <meta property="og:image" content="http://www.powwownow.co.uk/sfimages/the-workplace-cant/ogsocialis.jpg" />
    <meta property="og:description" content="We completed a survey in association with OnePoll to find out the identity of ‘Can’ts’ within the UK. Find out the characteristics of a Can’t and where in the UK to find them." />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />

    <link rel='icon' type='image/x-icon' href='/sfimages/the-workplace-cant/favicon.ico' />

    <link rel='shortcut icon' href='/sfimages/the-workplace-cant/icon57.png' />

    <link rel='apple-touch-icon' sizes='114x114' href='http://www.powwownow.co.uk/sfimages/the-workplace-cant/touch-icon-114x114.png' />
    <link rel='apple-touch-icon' sizes='72x72' href='http://www.powwownow.co.uk/sfimages/the-workplace-cant/touch-icon-72x72.png' />
    <link rel='apple-touch-icon' href='http://www.powwownow.co.uk/sfimages/the-workplace-cant/touch-icon-iphone.png' />

    <script type="text/javascript">var switchTo5x=true;</script>


</head>
<body>
<?php echo $sf_content ?>
</body>
</html>