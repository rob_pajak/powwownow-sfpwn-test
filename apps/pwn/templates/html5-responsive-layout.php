<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js ie lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js ie lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js ie lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js ie"><!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php include_versioned_stylesheets() ?>
    <?php pwn_include_javascript_header(); ?>

</head>
<body>
<?php //include_component('commonComponents', 'cookiePolicy'); ?>
    <noscript>
        <div class="alert-box">
            Powwownow makes heavy use of JavaScript and is needed to use Powwownow’s website. However, it seems
            JavaScript is either disabled or not supported by your browser. To view our wonderful website properly
            enable JavaScript by changing your browser options, and please try again!
        </div>
    </noscript>
    <?php echo $sf_content; ?>
    <?php pwn_include_javascript_bof(); ?>
    <script>
        if(typeof console === "undefined") {
            console = {
                log  : function () {},
                info : function () {},
                error: function () {},
                warn : function () {}
            }
        }

        try {
            $(document).foundation();
        } catch (e) {
            console.info('Foundation.js, html5-responsive-layout', e);
        }
    </script>
</body>
</html>
