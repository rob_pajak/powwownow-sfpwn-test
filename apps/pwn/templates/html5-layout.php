<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie ie-oldie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <link rel="shortcut icon" href="/favicon.ico"/>

    <?php include_http_metas() ?>
    <meta name="format-detection" content="telephone=no" />
    <?php include_title() ?>
    <?php include_metas() ?>
    <?php /** Canonical Links - Slot Overrides default behaviour */ ?>
    <?php if (has_slot('page_canonical')) : ?>
        <?php include_slot('page_canonical'); ?>
    <?php else: ?>
        <?php echo include_canonical_tag(); ?>
    <?php endif; ?>
    <?php /** This are the Social Media Meta Data used for their sites */ ?>
    <?php if (has_slot('page_metaproperties')): ?>
        <?php include_slot('page_metaproperties'); ?>
    <?php endif; ?>

    <?php include_versioned_stylesheets() ?>
    <style>
        @font-face {
            font-family: 'RockwellStdRegular';
            src: url('../../../shared/fonts/rockwellbold-webfont.eot');
            src: url('../../../shared/fonts/rockwellstd-webfont.eot?#iefix') format('embedded-opentype'),
            url('../../../shared/fonts/rockwellstd-webfont.woff') format('woff'),
            url('../../../shared/fonts/rockwellstd-webfont.ttf') format('truetype'),
            url('../../../shared/fonts/rockwellstd-webfont.svg#RockwellStdRegular') format('svg');
            font-weight: 400;
            font-style: normal;
        }
    </style>
    <?php /** These are the Google Content Experiment Slot */ ?>
    <?php if (has_slot('page_gcx')): ?>
        <?php include_slot('page_gcx'); ?>
    <?php endif; ?>

    <?php pwn_include_javascript_header(); ?>
</head>
<body>
    <?php include_partial('commonComponents/nojs'); ?>
    <?php include_component('commonComponents', 'cookiePolicy'); ?>
    <?php include_component('commonComponents', 'nonSupportedBrowser'); ?>
    <?php echo $sf_content; ?>
    <?php /**  Required JS libraries, do not delete these.*/ ?>
    <script src="/cx2/js/vendor/knockout-min.js" type="text/javascript"></script>
    <script src="/shared/ext/underscore-min.js" type="text/javascript"></script>
    <script src="/cx2/js/vendor/jquery.velocity.min.js" type="text/javascript"></script>
    <script src="/cx2/js/vendor/velocity.ui.js" type="text/javascript"></script>
    <script src="/shared/ext/placeholders.jquery.min.js" type="text/javascript"></script>
    <script src="/cx2/js/cx.registration.core.js" type="text/javascript"></script>
    <?php pwn_include_javascript_bof(); ?>
</body>
</html>
