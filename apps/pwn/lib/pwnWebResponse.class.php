<?php
class pwnWebResponse extends sfWebResponse  
{     
    public function getMetas()  
    {  
        $metas = parent::getMetas();  
        if(array_key_exists('title',$metas)){  
            unset($metas['title']);  
        }  
        return $metas;  
    }  
}