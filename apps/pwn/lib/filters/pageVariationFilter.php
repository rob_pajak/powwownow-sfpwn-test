<?php
/**
 * Custom symfony filter can be used to filter(forwards) users to specific
 * page variation.
 *
 * @package    PowWowNow
 * @subpackage Filters
 * @author     Kamil Skowron <kamil.skowron@powwownow.com>
 * @version    1.0
 * @see        jnotes.jonasfischer.net/2009/11/symfony-filter-redirect-useragent/
 *
 * @fixme      filter not applied if module/action is accessed directly via url.
 *             e.g. /s/homepageG/nextSteps will reset homepageCro variation to default.
 */
class pageVariationFilter extends sfFilter
{
    
    CONST DEFAULT_VARIATION = 'E';

    static $alreadyExecuted = false;

    /**
     * Placeholder for variations weights used to pick next to be assigned
     * to user
     * 
     * @var array
     */
    private $variationsWeights = null;
    
    /**
     * Placeholder for request object which will be assigned on construct
     * 
     * @var sfRequest
     */
    private $requestObj = null;
    
    /**
     * Placeholder for context object which will be assigned on construct
     * 
     * @var sfContext 
     */
    private $contextObj = null;    
    
    /**
     * Initializes this Filter. It follows parent filter constructor but 
     * also assigns useful variables.
     *
     * @param sfContext $context    The current application context
     * 
     * @param array     $parameters An associative array of initialization
     * parameters
     * 
     * @return self $this Instance of filter class
     * 
     * @author Kamil Skowron <kamil.skowron@powwownow.com>
     */
    public function __construct($context, $parameters = array())
    {
        parent::__construct($context, $parameters);
        
        $this->contextObj = $this->getContext();        
        $this->requestObj = $this->contextObj->getRequest();
    }    
    
    /**
     * Public method called when filter is executed. Method checks is there
     * any non-default variation set for user in session - if yes it forwards
     * to variation specific module/action.
     * 
     * NOTE: Filter assumes that if variation to display is default one it
     * simply letting action be disptched.
     * 
     * @param sfFilterChain $filterChain Filter chain passed by symfony to
     * execute method
     * 
     * @return void
     * 
     * @author Kamil Skowron <kamil.skowron@powwownow.com>
     */
    public function execute($filterChain)
    {
        // getting current module and action
        $module = $this->requestObj->getParameter('module');
        $action = $this->contextObj->getActionName();

        $validGVersionLinks = array(
            'homepageCro:indexG',
            'homepageG:plus',
            'homepageG:premium',
            'homepageG:webConferencing',
            'homepageG:videoConferencing',
            'homepageG:engage',
            'homepageG:events',
            'homepageG:pinReminder',
        );

        $currentHash = "$module:$action";

        if (in_array($currentHash, $validGVersionLinks)) {

            // execute rest of filters in chain
            return $filterChain->execute();
        }

        if (self::$alreadyExecuted) {
            // execute rest of filters in chain
            return $filterChain->execute();
        }

        $variation = $this->getVariation();

        // Add variation to the request so the GA filter knows which
        // variation is being displayed
        $request = $this->requestObj;        
        $request->setParameter('variation', $variation);

        // Add variation to session so the user always gets the same variation
        $session = $this->contextObj->getUser();        
        $session->setAttribute('homePageCroVariation', $variation, 'cro');

        $this->resolveForward($variation);

        // execute rest of filters in chain
        return $filterChain->execute();
    }

    /**
     * Method returns variation string. It's quite complicated so this is the
     * process:
     * - if 'reEvaluateVar' request param is defined it forces system to
     *   re-choose a variation for the user
     * - if 'forceVar' it overrides first above and it's the one that will be
     *   used
     * - check is forced variable valid option
     * - if none of above was true it picks up next available variation 
     * 
     * @author Mark Wiseman <mark.wiseman@powwownow.com>
     * 
     * @amend Kamil Skowron <kamil.skowron@powwownow.com>
     * 
     * @return string
     */
    protected function getVariation()
    {
        // Update of business rules!!
        // only E variation should be displayed to user from now on
        return self::DEFAULT_VARIATION;

        $request = $this->requestObj;
        $session = $this->contextObj->getUser();
        
        // If reEvaluateVar is specified as a url param, then forces the system
        // to re-choose a variation for the user (for testing / debugging)
        if ($request->hasParameter('reEvaluateVar')) {
            $session->setAttribute('homePageCroVariation', null, 'cro');
        }
        
        // If forceVar is specified, then we send the user to that variation
        if ($request->hasParameter('forceVar')) {
            
            $variation = ($request->getParameter('forceVar') != 'clear') 
                ? $request->getParameter('forceVar') 
                : null;
            
            $session->setAttribute('homePageCroVariation', $variation, 'cro');
        }
        
        // If a variation has already been chosen in a previous visit then
        // send them back to the same variation
        if ($session->hasAttribute('homePageCroVariation', 'cro')) {
            $variation = $session->getAttribute('homePageCroVariation', null, 'cro');
        }

        // If the user has a now deprecated version in the session then
        // re-evaluate version
        $variations = $this->getVariationsWeights();
        if (isset($variation) && !array_key_exists($variation, $variations)) {
            unset($variation);
        }
        
        // If we dont have a variation, then choose one
        if (!isset($variation)) {
            $variation = $this->pickNextVariation($variations);
        }

        // If a matching variation was not found, then default to E
        if (empty($variation)) {
            
            $variation = self::DEFAULT_VARIATION;
            sfContext::getInstance()->getLogger()->info(
                'Homepage variation weightings were not specified, '.
                'defaulting to ' . $variation
            );
        }
        
        return $variation;
    }

    /**
     * Figures out the variation to send the user to
     *
     * Its not too obvious about what is going on here, but it works like this:
     *
     * The $variations array has the following format:
     * array('A' => 60, 'B' => 20, 'C' => 20)
     * This means that out of 100 visitors, 60 should go to A, 20 to B and
     * 20 to C
     *
     * $modulus is an int between 0 and the total number of weights in the 
     * $variations array.
     *
     * This foreach loop takes the $variations array, and converts the weights
     * to be cumalitive, so we have something like this:
     * array('A' => 60, 'B' => 80, 'C' => 100)
     *
     * The variation is then picked by choosing the first variation where
     * $modulus is less than its cumalative weight
     *
     * @return null|string Variation, or null if none suitable found
     * 
     * @author Mark Wiseman <mark.wiseman@powwownow.com>
     * 
     * @amend Kamil Skowron <kamil.skowron@powwownow.com>
     */    
    protected function pickNextVariation()
    {
        $variations = $this->getVariationsWeights();        
        
        // Fetch unique visit number from APC, then increment and save
        $croVisit = apc_fetch('homePageCroVisit') +1;

        apc_store('homePageCroVisit', $croVisit);

        // Get modulus of the total number of weightings
        $modulus = $croVisit % array_sum($variations);

        $cumulativeWeight = 0;

        foreach ($variations as $variation => $weight) {

            $cumulativeWeight += $weight;

            if ($modulus < $cumulativeWeight) {
                return $variation;
            }
        }

        return null;        
    }
    
    /**
     * Array of variation weights from apache host configuration file or if
     * it's not defined there it uses default 50/50 value.
     * 
     * @return array
     * 
     * @author Mark Wiseman <mark.wiseman@powwownow.com>
     * 
     * @amend Kamil Skowron <kamil.skowron@powwownow.com>
     */
    protected function getVariationsWeights()
    {
        if ($this->variationsWeights === null) {
            
            $request     = $this->requestObj;
            $pathInfoArr = $request->getPathInfoArray();

            // Fallback when cro_variations var isn't defined in apache config
            if (!isset($pathInfoArr['cro_variations'])) {
                $pathInfoArr['cro_variations'] = '{"E":1,"G":1}';
            }

            $this->variationsWeights = json_decode(
                $pathInfoArr['cro_variations'],
                true
            );
        }

        return $this->variationsWeights;
    }
    
    /**
     * Method resolves requested variant to module/action that should be used.
     * It forwards user to variant action.
     * 
     * @param string $variation Requested variant
     * 
     * @return void
     * 
     * @author Kamil Skowron <kamil.skowron@powwownow.com>
     */
    protected function resolveForward($variation)
    {
        $config  = sfConfig::get('app_page_variations');
        $request = $this->requestObj;
        
        // getting current module and action
        $module = $request->getParameter('module');
        $action = $this->contextObj->getActionName();
        
        $configKey = "{$module}:{$action}";

        // if configuration for specific module:action doesn't exists simply
        // display default template
        if (!isset($config[$configKey])) {
            return false;
        }
         
        $actionConfig = $config[$configKey];
        // if configuration for specific module:action AND variation doesn't 
        // exists simply display default template        
        if (!isset($actionConfig[$variation])) {
            return false;
        }

        $variationSettings = $actionConfig[$variation];
        
        // if module or action is not defined go from here
        if (!isset($variationSettings['module'])
            || !isset($variationSettings['action'])
        ) {
            return false;
        }
        
        $forwardModule = $variationSettings['module'];
        $forwardAction = $variationSettings['action'];        
        
        if ($action === $forwardAction
            && $module === $forwardModule    
        ) {
            return false;
        }
        
        if (!$this->isValidForward($forwardModule, $forwardAction)) {
            return false;
        }
        
        $controller = $this->contextObj->getController();
        self::$alreadyExecuted = true;
        $controller->forward($forwardModule, $forwardAction);
        throw new sfStopException(); // the solution
    }
    
    /**
     * Method checks is specified module and action exists.
     * 
     * @param string $forwardModule Name of module without 'Actions' suffix
     * 
     * @param string $forwardAction Name of action without 'execute' prefix
     * 
     * @return boolean $result Check result; TRUE if class exists and contains
     * action.
     * 
     * @author Kamil Skowron <kamil.skowron@powwownow.com>
     */
    protected function isValidForward($forwardModule, $forwardAction)
    {
        // check is file exists 
        $classPath = realpath(
            dirname(__FILE__) 
            . '/../../modules/' 
            . $forwardModule 
            . '/actions/actions.class.php'
        );
        
        if ($classPath === false) {
            return false;
        }

        require_once($classPath);

        $moduleClassReflect = new ReflectionClass("{$forwardModule}Actions");
        return $moduleClassReflect->hasMethod("execute{$forwardAction}");
    }
}