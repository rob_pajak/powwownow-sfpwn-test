<?php 

/**
 * sfGoogleAnalyticsFilter
 * 
 * applies rendering filter to all templates by default to add GA code to doc <head>
 * 
 * @author bob
 *
 */
class sfGoogleAnalyticsFilter extends sfFilter
{
    public function execute($filterChain)
    {
        // Nothing to do before the action
        $filterChain->execute();
        $context = $this->getContext();
        $response = $context->getResponse();

        if(!$context->getRequest()->isXmlHttpRequest() )
        {
            // Decorate the response with the tracker code
            $googleCode = "<script type='text/javascript'>" . PHP_EOL;
            $googleCode .= "    var _gaq = _gaq || [];" . PHP_EOL;
            $googleCode .= "    _gaq.push(['_setAccount', 'UA-1850270-17']);" . PHP_EOL;

            /**
             * Our CRO testing serves up different page version on the same url, so we use a customer var google analytics
             * differentiate between pages
             *
             * op_scope is set to 2, indicating that scope should be set on session-level
             *
             * @url https://developers.google.com/analytics/devguides/collection/gajs/methods/gaJSApiBasicConfiguration#_gat.GA_Tracker_._setCustomVar
             */
            $variation = $context->getRequest()->getParameter('variation');
            if (!empty($variation)) {
                $googleCode .= "    _gaq.push(['_setCustomVar', 5, 'CroPage', '" . $variation . "' , 2]);" . PHP_EOL;
            }

            $googleCode .= "    _gaq.push(['_trackPageview']);" . PHP_EOL;
            $googleCode .= "    (function() {" . PHP_EOL;
            $googleCode .= "        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" . PHP_EOL;
            $googleCode .= "        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" . PHP_EOL;
            $googleCode .= "        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);" . PHP_EOL;
            $googleCode .= "    })();" . PHP_EOL;
            $googleCode .= "</script>" . PHP_EOL;

            $response = $context->getResponse();

            $response->setContent(str_ireplace('</head>', $googleCode."\n\n</head>",$response->getContent()));
           
        } 
    }
}
