<?php 

/**
 * WhosOn Tracking Filter
 * 
 * The javascript code from Whoson checks for an ID with the name of whoson_chat_link. 
 * If it does not exist, depending on brower, it will replace the body contents with some whoson html.
 * So please dont remove the a link, unless you know what you are doing.
 * 
 * @author Asfer Tamimi
 *
 */
class whosOnFilter extends sfFilter
{
    public function execute($filterChain)
    {
        // Nothing to do before the action
        $filterChain->execute();
        $context = $this->getContext();
        $response = $context->getResponse();

        if (!$context->getRequest()->isXmlHttpRequest()) {

            $whosonCode = '<!-- Start WhosOn Chat -->' . PHP_EOL;
            $whosonCode .= '<a id="whoson_chat_link" style="visibility: hidden; display: none;"></a>' . PHP_EOL;
            $whosonCode .= '<script type="text/javascript">' . PHP_EOL;
            $whosonCode .= '(function($) {' . PHP_EOL;
            $whosonCode .= "'use strict';" . PHP_EOL;
            $whosonCode .= '$(function() {' . PHP_EOL;

            // WhosOn Chat Code - Old
            //$whosonCode .= '$.getScript("http://gateway3.whoson.com/include.js?domain=www.powwownow.co.uk",function () {sWOInvite="N"; sWOTrackPage(); $.getScript("http://gateway3.whoson.com/invite.js?domain=www.powwownow.co.uk");});' . PHP_EOL;

            // WhosOn Chat Code - New
            $whosonCode .= '$.ajax({method: "get", url: "//gateway3.whoson.com/include.js?domain=www.powwownow.co.uk", data: undefined, dataType: "script", timeout: 1000, success: function () {sWOInvite = "N";sWOTrackPage();$.ajax({method: "get", url: "//gateway3.whoson.com/invite.js?domain=www.powwownow.co.uk", data: undefined, dataType: "script", timeout: 1000});}});' . PHP_EOL;

            $whosonCode .= '});' . PHP_EOL;
            $whosonCode .= '})(jQuery);' . PHP_EOL;
            $whosonCode .= '</script>' . PHP_EOL;
            $whosonCode .= '<!-- End WhosOn Chat -->' . PHP_EOL;

            $response->setContent(str_ireplace('</body>', PHP_EOL . $whosonCode . PHP_EOL . "</body>",$response->getContent()));

        }
    }
}