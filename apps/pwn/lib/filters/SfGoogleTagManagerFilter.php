<?php
/**
 * SfGoogleTaskManagerFilter
 *
 * Adds the Tag Manager Filter At the Start of the Body Tags
 *
 * @author Asfer Tamimi
 *
 */
class SfGoogleTagManagerFilter extends sfFilter
{
    /**
     * Execute Filter
     * @param object $filterChain
     */
    public function execute($filterChain)
    {
        // Nothing to do before the action
        $filterChain->execute();
        $context = $this->getContext();
        $response = $context->getResponse();

        // Only run the filter once
        if (!$context->getRequest()->isXmlHttpRequest()) {
            // Google Tag Manager Code Selection
            if (isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] === 'production') {
                $gtmCode = 'GTM-XMFX';
            } else {
                $gtmCode = 'GTM-P7W7QC';
            }

            // Google Tag Manager Code
            $googleTagCode = '<!-- Start Google Tag Manager -->' . PHP_EOL;
            $googleTagCode .= '<noscript><iframe src="//www.googletagmanager.com/ns.html?id=' . $gtmCode . '" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>' . PHP_EOL;
            $googleTagCode .= '<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);})(window,document,\'script\',\'dataLayer\',\'' . $gtmCode . '\');</script>' . PHP_EOL;
            $googleTagCode .= '<!-- End Google Tag Manager -->' . PHP_EOL;

            // Update Response
            $response->setContent(
                str_ireplace('</body>', $googleTagCode . PHP_EOL . '</body>', $response->getContent())
            );
        }
    }
}
