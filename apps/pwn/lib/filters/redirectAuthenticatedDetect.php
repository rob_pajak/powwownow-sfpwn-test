<?php

/**
 * Class redirectAuthenticatedDetect
 */
class redirectAuthenticatedDetect extends sfFilter
{
    const COOKIE_REDIRECT_PARAM_KEY = 'RM_REDIRECT_COMPLETED';
    const COOKIE_REDIRECT_PARAM_VALUE = 'RM_REDIRECTED_DONE';

    /**
     * @var sfUser
     */
    private $user;

    /**
     * @var sfRequest
     */
    protected $request;

    /**
     * @var sfContext
     */
    protected  $context;

    /**
     * @param $context
     * @param array $parameters
     */
    public function __construct($context, $parameters = array())
    {
        parent::__construct($context, $parameters);
        $this->context = $context;
        $this->request = $this->context->getRequest();
        $this->user = $this->context->getUser();
    }

    /**
     * @param $filterChain
     * @return mixed
     * @throws sfStopException
     */
    public function execute($filterChain)
    {
        if ($this->user->isAuthenticated()
            && $this->request->getCookie(self::COOKIE_REDIRECT_PARAM_KEY) !== self::COOKIE_REDIRECT_PARAM_VALUE) {
            if ($this->getRoutingHash() === 'pages:indexE') {
                $this->setRedirectCookie();
                $this->context->getController()->redirect('mypwn/index');
                throw new sfStopException();
            }
        }
        return $filterChain->execute();
    }

    /**
     * @return string
     */
    private function getRoutingHash()
    {
        $module = $this->request->getParameter('module');
        $action = $this->context->getActionName();
        return "$module:$action";
    }

    /**
     * @return bool
     */
    private function setRedirectCookie()
    {
        return setcookie(self::COOKIE_REDIRECT_PARAM_KEY, self::COOKIE_REDIRECT_PARAM_VALUE, 0);
    }
}
