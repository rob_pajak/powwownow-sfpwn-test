<?php

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfSslRequirementFilter.class.php 9095 2008-05-20 07:32:16Z fabien $
 *
 * Formatted the Code to 4 Line Spaces and also updated the redirect to react differently to the News Module.
 *
 */
class sfSslRequirementFilter extends sfFilter
{
    public function execute($filterChain)
    {
        // execute only once and only if not using an environment that is disabled for SSL
        if ($this->isFirstCall() && !sfConfig::get('app_disable_sslfilter')) {
            // get the cool stuff
            $context = $this->getContext();
            $request = $context->getRequest();

            // only redirect if not posting and we actually have an http(s) request
            if ($request->getMethod() != sfRequest::POST && substr($request->getUri(), 0, 4) == 'http') {
                $controller = $context->getController();

                // get the current action instance
                $actionEntry    = $controller->getActionStack()->getLastEntry();
                $actionInstance = $actionEntry->getActionInstance();

                // request is SSL secured
                if ($request->isSecure()) {
                    // but SSL is not allowed
                    if (!$actionInstance->sslAllowed() && $this->redirectToHttp()) {
                        $controller->redirect($actionInstance->getNonSslUrl());
                        exit();
                    }
                } // request is not SSL secured, but SSL is required
                elseif ($actionInstance->sslRequired() && $this->redirectToHttps()) {

                    // Need to do a '301 Not Found' Redirect for the News Module. By Default it uses '302 Found'
                    if ($context->getModuleName() === 'news') {
                        $controller->redirect($actionInstance->getSslUrl(), 0, 301);
                    } else {
                        $controller->redirect($actionInstance->getSslUrl());
                    }
                    exit();
                }
            }
        }

        $filterChain->execute();
    }

    protected function redirectToHttps()
    {
        return true;
    }

    protected function redirectToHttp()
    {
        return true;
    }
}
