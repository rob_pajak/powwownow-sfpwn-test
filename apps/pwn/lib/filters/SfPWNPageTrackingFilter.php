<?php 
/**
 * sfPWNPageTrackingFilter
 * 
 * Adds the Powwownow Page Tracking, so it stores it in the Session.
 * 
 * @author Asfer
 *
 */
class sfPWNPageTrackingFilter extends sfFilter
{
    public function execute($filterChain)
    {
        $visitLogger = new PWN_Tracking_PageVisitLog();
        $visitLogger->logVisit();

        $filterChain->execute();
    }
}
