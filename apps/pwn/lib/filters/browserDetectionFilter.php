<?php

class browserDetectionFilter extends sfFilter
{
    public function execute($filterChain)
    {
        $bd = sfContext::getInstance()->getUser()->getAttribute('browser_detection', null);

        if (!isset($bd) || (!isset($bd['browser_detection_datetime'])) ||
            ($bd['browser_detection_datetime'] < strtotime("-1 days"))
        ) { // do browser detection & pwn_support_level check once a day

            sfContext::getInstance()->getUser()->setAttribute(
                'browser_detection',
                array('browser_detection_datetime' => strtotime("now"), 'data' => browserDetection::detect())
            );
        }

        $filterChain->execute();
    }
}
