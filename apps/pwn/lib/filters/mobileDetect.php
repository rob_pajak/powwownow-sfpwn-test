<?php
require_once(dirname(__FILE__) . '/../../../../lib/Mobile_Detect.php');

/**
 * Class mobileDetect
 */
class mobileDetect extends sfFilter
{
    /**
     * @var
     */
    private $contextObj;

    /**
     * @var
     */
    private $requestObj;

    /**
     * @var
     */
    private $detector;

    /**
     * @param $context
     * @param array $parameters
     * @throws sfStopException
     */
    public function __construct($context, $parameters = array())
    {
        parent::__construct($context, $parameters);

        $this->contextObj = $this->getContext();
        $this->requestObj = $this->contextObj->getRequest();

        if (class_exists('Mobile_Detect')) {
            $this->detector = new Mobile_Detect();
        }else {
            sfContext::getInstance()->getLogger()->err(
                'Mobile_Detect class not loaded'
            );
            throw new sfStopException();
        }
    }

    /**
     * @param $filterChain
     * @throws sfStopException
     */
    public function execute ($filterChain)
    {
        // get current module and action
        $module = $this->requestObj->getParameter('module');
        $action = $this->contextObj->getActionName();

        if ($this->requestObj->getParameter('skip') == true) {
            return $filterChain->execute();
        }

        /**
         * array(
         *  ListenFor {'module:action'} =>  RedirectTo{'module:action'}
         * )
         */
        $mobileRedirectUrls = array(
            'pages:indexE' => array(
                'module' => 'mobile',
                'action' => 'cxMobileExperience'
            ),
            'pages:homepagePPCShortTerm' => array(
                'module' => 'mobile',
                'action' => 'cxMobileExperience'
            ),
            'pages:homepagePPCLongTerm' => array(
                'module' => 'mobile',
                'action' => 'cxMobileExperience'
            ),
            'pages:conferenceCall' => array(
                'module' => 'mobile',
                'action' => 'cxMobileExperience'
            ),
            'pages:internationalNumberRates' => array(
                'module' => 'mobile',
                'action' => 'cxMobileExperience'
            )
        );

        $currentHash = "$module:$action";
        if( $this->detector->isMobile() && !$this->detector->isTablet() ){
            if (array_key_exists($currentHash, $mobileRedirectUrls)) {
                $module = $mobileRedirectUrls[$currentHash]['module'];
                $action = $mobileRedirectUrls[$currentHash]['action'];
                if ($this->isValidForward($module, $action)) {
                    $this->contextObj->getController()->forward($module, $action);
                    throw new sfStopException();
                }
            }
        }
        return $filterChain->execute();
    }

    /**
     * @param $forwardModule
     * @param $forwardAction
     * @return bool
     */
    protected function isValidForward($forwardModule, $forwardAction)
    {
        // check is file exists
        $classPath = realpath(
            dirname(__FILE__)
            . '/../../modules/'
            . $forwardModule
            . '/actions/actions.class.php'
        );

        if ($classPath === false) {
            return false;
        }

        require_once($classPath);

        $moduleClassReflect = new ReflectionClass("{$forwardModule}Actions");
        return $moduleClassReflect->hasMethod("execute{$forwardAction}");
    }
}
