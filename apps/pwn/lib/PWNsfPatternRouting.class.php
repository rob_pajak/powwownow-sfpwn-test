<?php 
/**
 * This custom class provides functionality to use sf_method in routing.yml 
 * default symfony routing implementation just ignores requirements sf_methods,
 * 
 *  
 * @author Robert
 * 
 */
class PWNsfPatternRouting extends sfPatternRouting {

    protected function getRouteThatMatchesUrl($url) {

        $this->ensureDefaultParametersAreSet();
        foreach ($this->routes as $name => $route)
        {

            if (false === $parameters = $route->matchesUrl($url, $this->options['context']))
            {
                continue;
            }

            // addtion START
            $req = $route->getRequirements();

            if (isset($req['sf_method']) && is_array($req['sf_method'])) {
                if (!in_array($this->options['context']['method'], $req['sf_method'])) {
                    continue;
                }
            }
            // addtion END

          return array('name' => $name, 'pattern' => $route->getPattern(), 'parameters' => $parameters);
        }

        return false;
    }
}

?>
