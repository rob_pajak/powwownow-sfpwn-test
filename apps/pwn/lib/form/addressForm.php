<?php
/**
* Plus Upgrade Form
* 
* This will be used for both the Authenticated and Non-Authenticated (Enhanced/Open) Customer who wants to Upgrade to Plus.
* 
* @author Vitaly
*/
class addressForm extends PWNForm
{
    public function configure()
    {

        $c = new sfCultureInfo(sfContext::getInstance()->getUser()->getCulture());
        //$countries = $c->getCountries();
        $countries = array('GBR' => 'United Kingdom');

        /*Form Elements*/
        $this->setWidgets(array(
            'billing_organisation'  => new sfWidgetFormInputText(),
            'billing_line1'         => new sfWidgetFormInputText(),
            'billing_line2'         => new sfWidgetFormInputText(),
            'billing_town'          => new sfWidgetFormInputText(),
            'billing_county'          => new sfWidgetFormInputText(),
            'billing_postcode'      => new sfWidgetFormInputText(),
            'billing_country'     => new sfWidgetFormSelect(array("choices" => $countries, "default" => "GBR")),

            'admin_organisation'  => new sfWidgetFormInputText(),
            'admin_line1'         => new sfWidgetFormInputText(),
            'admin_line2'         => new sfWidgetFormInputText(),
            'admin_town'          => new sfWidgetFormInputText(),
            'admin_county'          => new sfWidgetFormInputText(),
            'admin_postcode'      => new sfWidgetFormInputText(),
            'admin_country'     => new sfWidgetFormSelect(array("choices" => $countries, "default" => "GBR")),
        ));


        /*Form Validation*/
        $this->setValidators(array(
            'billing_organisation' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true),
                array('required'   => 'FORM_VALIDATION_NO_BILLING_ORGANISATION', 'min_length' => 'FORM_VALIDATION_NO_BILLING_ORGANISATION')
            ),
            'billing_line1' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_BILLING_LINE1', 'min_length' => 'FORM_VALIDATION_NO_BILLING_LINE1')
            ),
            'billing_town' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_BILLING_TOWN', 'min_length' => 'FORM_VALIDATION_NO_BILLING_TOWN')
            ),
            'billing_postcode' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_BILLING_POSTCODE', 'min_length' => 'FORM_VALIDATION_NO_BILLING_POSTCODE')
            ),
            'billing_country' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_BILLING_COUNTRY', 'min_length' => 'FORM_VALIDATION_NO_BILLING_COUNTRY')
            ),

            'admin_organisation' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true),
                array('required'   => 'FORM_VALIDATION_NO_admin_ORGANISATION', 'min_length' => 'FORM_VALIDATION_NO_admin_ORGANISATION')
            ),
            'admin_line1' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_admin_LINE1', 'min_length' => 'FORM_VALIDATION_NO_admin_LINE1')
            ),
            'admin_town' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_admin_TOWN', 'min_length' => 'FORM_VALIDATION_NO_admin_TOWN')
            ),
            'admin_postcode' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_admin_POSTCODE', 'min_length' => 'FORM_VALIDATION_NO_admin_POSTCODE')
            ),
            'admin_country' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_admin_COUNTRY', 'min_length' => 'FORM_VALIDATION_NO_admin_COUNTRY')
            ),

        ));

        /*Form Naming Convention*/
        $this->widgetSchema->setNameFormat('plusaddress[%s]');
    }


}
