<?php
/**
 * The Call History Form
 *
 * @author Asfer Tamimi
 *
 */
class CallHistoryForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        $countries = Common::getCountriesList($this->user->getCulture());
        $countries = array_merge(array('' => 'All'),$countries);
        $reportList = array(
            'detail'             => 'Detail',
            'summary_by_service' => 'Summary by Service', 
            'summary_by_dialin'  => 'Summary by Dial-in'
        );
        $called_number = array('' => 'All');

        // Set the Form Elements
        $this->setWidgets(array(
            'service'                    => new sfWidgetFormSelect(array('choices' => $this->getServiceRefs(), 'default' => $this->user->getAttribute('service_ref'))),
            'called_number'              => new sfWidgetFormSelect(array('choices' => $called_number, 'default' => '')),
            'country'                    => new sfWidgetFormSelect(array('choices' => $countries, 'default' => '')),
            'pin_type'                   => new sfWidgetFormInputText(),
            'from_date'                  => new sfWidgetFormInputText(array('default' => date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y"))))),
            'to_date'                    => new sfWidgetFormInputText(array('default' => date("Y-m-d"))),
            'callers_number'             => new sfWidgetFormInputText(),
            'report'                     => new sfWidgetFormSelect(array('choices' => $reportList, 'default' => 'detail')),
            'include_unsuccessful_calls' => new sfWidgetFormInputCheckbox(),
            'show_all_account'           => new sfWidgetFormInputCheckbox(),
            'cost_covered'               => new sfWidgetFormSelect(
                    array(
                        'choices' => array(
                            'indifferently' => 'All',
                            'fully' => 'Bundled',
                            'notAndPartly' => 'Outside bundle',
                        ),
                        'default' => 'indefinitely'
                    )
                ),
        ));

        // Set the Form Labels
        $this->widgetSchema->setLabels(array(
            'service'                    => 'Service',
            'called_number'              => 'Called Number',
            'country'                    => 'Dial-in Country',
            'pin_type'                   => 'PIN / PIN Description / Cost Code',
            'from_date'                  => 'From Date',
            'to_date'                    => 'To Date',
            'callers_number'             => 'Caller\'s Number',
            'report'                     => 'Report Level',
            'include_unsuccessful_calls' => 'Include Unsuccessful Calls',
            'show_all_account'           => 'Show All Account Users',
            'cost_covered'               => 'Type of Call Charge'
        ));

        // Validation Factory
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'from_date'                  => $validationFactory->fromDate(),
            'to_date'                    => $validationFactory->toDate(),
            'country'                    => $validationFactory->country(false),
            'service'                    => $validationFactory->genericField(),
            'called_number'              => $validationFactory->genericField(),
            'pin_type'                   => $validationFactory->genericField(),
            'callers_number'             => $validationFactory->genericField(),
            'report'                     => $validationFactory->genericField(),
            'show_all_account'           => $validationFactory->genericField(),
            'include_unsuccessful_calls' => $validationFactory->genericField(),
            'cost_covered'               => $validationFactory->genericField(),
        ));            

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('callhistory[%s]');        

    }

    protected function getServiceRefs() {
        $serviceRefs = Common::getServiceRefs(array('contact_ref' => $this->user->getAttribute('contact_ref')));
        $serviceRefsList = array();
        foreach ($serviceRefs as $k => $v) {
            $serviceRefsList[$v['service_ref']] = $v['service_name'];
        }
        return $serviceRefsList;
    }

    public function doPostValidation($arguments) {
        return false;
    }
}
