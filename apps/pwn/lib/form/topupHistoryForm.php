<?php
/**
* Topup History Form
* 
* Form is to specify filters for topup history list
* 
* @author Vitaly
*/
class topupHistoryForm extends PWNForm
{
    public function configure()
    {
        $topupType = array(
            'all' => 'All top-up transactions',
            'manual' => 'Manual Top-up',
            'auto' => 'Auto Top-up',
            'monthly_invoice' => 'Monthly Invoice',
        );

        /*Form Elements*/
        $this->setWidgets(array(
            'topup_start_date' => new sfWidgetFormInputText(),
            'topup_end_date'   => new sfWidgetFormInputText(),
            'topup_item_type'  => new sfWidgetFormSelect(array(
                "choices" => $topupType, 
                "default" => ""
            )),
        ));


        /*Form Validation*/
        $this->setValidators(array(


        ));

        /*Form Naming Convention*/
        $this->widgetSchema->setNameFormat('plustopuphistory[%s]');
    }


}
 