<?php

/**
 * TEST / TUTORIAL / SAMPLE
 *
 * @author marcin
 */
class upgradeFreeToPlusAccountForm extends PWNForm {

    public function configure() {

        $this->setWidgets(array(
            'first_name' => new sfWidgetFormInputText(array(),
                    array('class' => 'mypwn-input input-large font-small', 'required' => 'required')
            ),
            'last_name' => new sfWidgetFormInputText(array(),
                    array('class' => 'mypwn-input input-large font-small', 'required' => 'required')
            ),
            'business_phone' => new sfWidgetFormInputText(array(),
                    array('class' => 'mypwn-input input-large font-small', 'required' => 'required')
            ),

            'password' => new sfWidgetFormInputPassword(array(),
                    array('class' => 'mypwn-input input-large font-small', 'required' => 'required','pattern' => '[a-zA-Z0-9 *\W+]{6,20}')
            ),
            'email' => new sfWidgetFormInputText(array(),
                    array('class' => 'mypwn-input input-large font-small', 'required' => 'required', 'type' => 'email')
            )
        ));

        //$this->setDefault('email', 'Your Email Here');

        $this->setValidators(array(
            'first_name' => new sfValidatorString(),
            'last_name' => new sfValidatorString(),
            'business_phone' => new sfValidatorString(),
            // 'country_code' => new sfValidatorString(),
            'email' => new sfValidatorEmail(),
            'password' => new sfValidatorString(),
        ));


        $this->widgetSchema->setNameFormat('upgrade[%s]');

        $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array('callback' => array($this, 'checkEmailIsRegistered')))
        );
    }

    public function checkEmailIsRegistered($validator, $values) {

        // proceed only on email and password
        if (empty($values['email']) || empty($values['password']))
            return $values;

        // check contact (must exists)
        $contact = json_decode(Hermes_Client_Rest::callJson('getContact', array('email' => $values['email'])));
        if (empty($contact)) {
            $error = new sfValidatorError($validator, 'FORM_EMAIL_CHANGED_WARNING');
            throw new sfValidatorErrorSchema($validator, array('email' => $error));
        }

        // password check
        if ($contact->password != md5($values['password'])) {
            $error = new sfValidatorError($validator, 'hermis says: invalid password');
            throw new sfValidatorErrorSchema($validator, array('password' => $error));
        }

        // add hermis response to the values set
        $values['contact'] = $contact;

        // check service (must be 801)

        $_SESSION['service_type'] = 'powwownow';

        if (@$_SESSION['service_type'] != 'powwownow') {
            $error = new sfValidatorError($validator, 'registered email address, but not powwownow service');
            throw new sfValidatorErrorSchema($validator, array('email' => $error));
        };

        return $values;
    }

}

