<?php
/**
* Plus Upgrade Form
* 
* This will be used for both the Authenticated and Non-Authenticated (Enhanced/Open) Customer who wants to Upgrade to Plus.
* 
* @author Asfer
*/
class PlusUpgradeForm extends PWNForm
{
    public function configure()
    {
        $titles = array();
        $titles['']      = '';
        $titles['Mr.']   = 'Mr.';
        $titles['Mrs.']  = 'Mrs.';
        $titles['Miss.'] = 'Miss.';
        $titles['Ms.']   = 'Ms.';

        /*Form Elements*/
        $this->setWidgets(array(
            //'title'          => new sfWidgetFormInputText(),
            'first_name'     => new sfWidgetFormInputText(),
            'last_name'      => new sfWidgetFormInputText(),
            'business_phone' => new sfWidgetFormInputText(),
            'mobile_phone'   => new sfWidgetFormInputText(),
            'company_name'   => new sfWidgetFormInputText(),
            'title'          => new sfWidgetFormSelect(array("choices" => $titles)),
        ));

        /*Set Default Values*/
        $this->setUpgradeDefaults();

        /*Form Validation*/
        $this->setValidators(array(
            'first_name' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_FIRST_NAME', 'min_length' => 'FORM_VALIDATION_NO_FIRST_NAME')
            ),
            'last_name' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_SURNAME', 'min_length' => 'FORM_VALIDATION_NO_SURNAME')
            ),
            'business_phone' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_INVALID_PHONE_NUMBER', 'min_length' => 'FORM_VALIDATION_INVALID_PHONE_NUMBER')
            ),
            'company_name' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_COMPANY_NAME', 'min_length' => 'FORM_VALIDATION_NO_COMPANY_NAME')
            ),
        ));

        /*Form Naming Convention*/
        $this->widgetSchema->setNameFormat('plusupgrade[%s]');
    }

    public function setUpgradeDefaults()
    {
        //Find the Initial Contact Information for the Form, Firstly from the Session, if not there then the 
        $response = Hermes_Client_Rest::call('getContactByRef', array('contact_ref' => $_SESSION['contact_ref']));
        if (is_array($response) && count($response)>0) {
            $this->setDefault('title',$response['title']);
            $this->setDefault('first_name',$response['first_name']);
            $this->setDefault('last_name',$response['last_name']);            
            $this->setDefault('business_phone',$response['business_phone']);
            $this->setDefault('mobile_phone',$response['mobile_phone']);
            $this->setDefault('company_name',$response['organisation']);
        }
    }
}
