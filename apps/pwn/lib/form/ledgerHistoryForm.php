<?php
/**
 * The Ledger History Form
 *
 * @author Vitaly
 * @author Asfer Tamimi
 *
 */
class ledgerHistoryForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        $ledgerOperations = Common::getLedgerOperationTypes();
        $ledgerOperationsArr = array('all' => 'All transactions');
        foreach ($ledgerOperations as $operation) {
            $ledgerOperationsArr[$operation['acronym']] = $operation['operation'];
        }

        // Form Elements
        $this->setWidgets(array(
            'ledger_start_date' => new sfWidgetFormInputText(array(),array('class' => 'mypwn-input input-large font-small', 'value' => date('Y-m-d', strtotime("-6 months")), 'type' => 'date', 'min' => date("Y-m-d", strtotime("-6 months")), 'max' => date('Y-m-d'))),
            'ledger_end_date'   => new sfWidgetFormInputText(array(),array('class' => 'mypwn-input input-large font-small', 'value' => date('Y-m-d'), 'type' => 'date', 'min' => date("Y-m-d", strtotime("-6 months")), 'max' => date('Y-m-d'))),
            'ledger_type'       => new sfWidgetFormSelect(array(
                "choices" => $ledgerOperationsArr,
                "default" => "all"
            ), array('class' => 'mypwn-input input-large font-small')),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'ledger_start_date' => 'From Date *',
            'ledger_end_date'   => 'To Date *',
            'ledger_type'       => 'Transaction Type'
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'ledger_start_date' => $validationFactory->fromDate(),
            'ledger_end_date'   => $validationFactory->toDate(),
            'ledger_type'       => $validationFactory->commonField()
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('plusledgerhistory[%s]');

    }

    public function doPostValidation($arguments) {
        return false;
    }

}
