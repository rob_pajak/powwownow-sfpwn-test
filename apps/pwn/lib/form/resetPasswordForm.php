<?php
/**
 * The Public Contact Us Form
 *
 * @author Asfer Tamimi
 *
 */
class ResetPasswordForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements

        $this->setWidgets(array(
            'password'     => new sfWidgetFormInputPassword(),
            'confirm_password'     => new sfWidgetFormInputPassword(),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'password'          => 'Password',
            'confirm_password'  => 'Confirm Password',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'password'          => $validationFactory->passwordNew(),
            'confirm_password'  => $validationFactory->confirmPassword(),
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('resetpassword[%s]');

        // Post Validation - Did not Work
        // $this->validatorSchema->setPostValidator($validationFactory->confirmEmail());
    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
//    public function doPostValidation(Array $args) {
//        if ($args['email'] !== $args['confirm_email']) {
//            return array('message' => 'FORM_VALIDATION_EMAIL_MISMATCH', 'field_name' => 'contactus[confirm_email]');
//        }
//        return false;
//    }
}
