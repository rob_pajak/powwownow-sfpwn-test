<?php

/**
 * Represents the contact details form on the Account Details page.
 * @author Maarten Jacobs
 */
class ContactDetailsForm extends AjaxForm {

    /**
     * Default contact email, in case the email form element is not shown.
     *
     * @var string
     */
    public $defaultEmail;

    /**
     * @var sfUser
     */
    protected $user;

    /**
     * Passes default values and form configuration to the new form.
     *
     * @param array $defaults
     * @param array $options
     * @param string $CSRFSecret
     */
    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    /**
     * Add the widgets and the validation.
     *
     * @author Maarten Jacobs
     */
    public function configureForm() {
        $locale = $this->user->getCulture();
        $countries = Common::getCountriesList($locale);
        $hasEditableEmail = Common::hasEditableEmail($this->user->getAttribute('service_user'), $this->user->getAttribute('contact_ref'));

        // Create the widgets.
        $widgets = array(
            'title'             => new sfWidgetFormInputText(
                array(),
                array('maxlength' => 10)
            ),
            'first_name'        => new sfWidgetFormInputText(
                array(),
                array('maxlength' => 30)
            ),
            'middle_initials'   => new sfWidgetFormInputText(
                array(),
                array('maxlength' => 10)
            ),
            'last_name'         => new sfWidgetFormInputText(
                array(),
                array('maxlength' => 30)
            ),
            'business_phone'    => new sfWidgetFormInputText(
                array(),
                array('maxlength' => 20)
            ),
            'mobile_phone'      => new sfWidgetFormInputText(
                array(),
                array('maxlength' => 20)
            ),
            'country'           => new sfWidgetFormSelect(array('choices' => $countries)),
            'company'           => new sfWidgetFormInputText(
                array(),
                array('maxlength' => 50)
            ),
        );
        if ($hasEditableEmail) {
            $widgets['email'] = new sfWidgetFormInputText(
                array(),
                array('maxlength' => 30)
            );
        }
        $this->setWidgets($widgets);

        // Set the labels to reflect required items.
        $labels = array(
            'first_name'        => 'First Name*',
            'last_name'         => 'Last name*',
        );
        if ($hasEditableEmail) {
            $labels['email'] = 'Email*';
        }
        $this->widgetSchema->setLabels($labels);

        // Set the validators
        $validatorFactory = new validationGroupFactory();
        $validators = array(
            // Required elements.
            'first_name'        => $validatorFactory->firstName()->setOption('max_length', 30)->setMessage('max_length', 'FORM_VALIDATION_INVALID_CONTACT_FIRST_NAME'),
            'last_name'         => $validatorFactory->lastName()->setOption('max_length', 30)->setMessage('max_length', 'FORM_VALIDATION_INVALID_CONTACT_LAST_NAME'),

            // Optional elements.
            'title'             => new sfValidatorString(
                array('required' => false, 'max_length' => 10),
                array('max_length' => 'FORM_VALIDATION_INVALID_CONTACT_TITLE')
            ),
            'middle_initials'   => new sfValidatorString(
                array('required' => false, 'max_length' => 10),
                array('max_length' => 'FORM_VALIDATION_INVALID_MIDDLE_INITIALS')
            ),
            'business_phone'    => $validatorFactory->phoneNumber()->setOption('max_length', 20)->setMessage('max_length', 'FORM_VALIDATION_INVALID_PHONE_NUMBER'),
            'mobile_phone'      => $validatorFactory->mobileNumber()->setOption('max_length', 20)->setMessage('max_length', 'INVALID_MOBILE_NUMBER'),
            'country'           => new sfValidatorChoice(
                array('required' => false, 'choices' => array_keys($countries)),
                array('invalid' => 'FORM_VALIDATION_INVALID_CONTACT_COUNTRY')
            ),
            'company'           => $validatorFactory->company()->setMessage('max_length', 'FORM_VALIDATION_NO_COMPANY_NAME'),
        );
        if ($hasEditableEmail) {
            $validators['email'] = $validatorFactory->email();
        }
        $this->setValidators($validators);

        // Make sure all field names are unique by grouping under contact.
        $this->widgetSchema->setNameFormat('contact[%s]');
    }

}
