<?php
/**
 * The Public Contact Us Form
 *
 * @author Asfer Tamimi
 *
 */
class ContactUsForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements

        $this->setWidgets(array(
            'first_name'     => new sfWidgetFormInputText(),
            'last_name'      => new sfWidgetFormInputText(),
            'company'        => new sfWidgetFormInputText(),
            'contact_number' => new sfWidgetFormInputText(),
            'email'          => new sfWidgetFormInputText(),
            'confirm_email'  => new sfWidgetFormInputText(),
            'comments'       => new sfWidgetFormTextarea(),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'first_name'     => 'First Name *',
            'last_name'      => 'Last Name *',
            'company'        => 'Company',
            'contact_number' => 'Contact Number *',
            'email'          => 'Email *',
            'confirm_email'  => 'Confirm Email *',
            'comments'       => 'Comments *',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'first_name'     => $validationFactory->firstName(),
            'last_name'      => $validationFactory->lastName(),
            'company'        => $validationFactory->company(),
            'contact_number' => $validationFactory->phoneNumberNotNull(),
            'email'          => $validationFactory->email(),
            'confirm_email'  => $validationFactory->email(),
            'comments'       => $validationFactory->comments(),
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('contactus[%s]');

        // Post Validation - Did not Work
        // $this->validatorSchema->setPostValidator($validationFactory->confirmEmail());
    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
    public function doPostValidation(Array $args) {
        if ($args['email'] !== $args['confirm_email']) {
            return array('message' => 'FORM_VALIDATION_EMAIL_MISMATCH', 'field_name' => 'contactus[confirm_email]');
        }
        return false;
    }    
}
