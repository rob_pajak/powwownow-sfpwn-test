<?php
/**
* Plus Login - Create A Login Form
* 
* @author Asfer
*/
class PlusLoginCreateALoginForm extends PWNForm
{
    public function configure()
    {
        /*Form Elements*/
        $this->setWidgets(array(
            'first_name'       => new sfWidgetFormInputText(),
            'last_name'        => new sfWidgetFormInputText(),
            'email'            => new sfWidgetFormInputText(),
            'password'         => new sfWidgetFormInputPassword(),
            'confirm_password' => new sfWidgetFormInputPassword(),
        ));

        /*Form Validation*/
        $this->setValidators(array(
            'first_name' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required' => 'FORM_VALIDATION_NO_FIRST_NAME', 'min_length' => 'FORM_VALIDATION_NO_FIRST_NAME')
            ),
            'last_name' => new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required' => 'FORM_VALIDATION_NO_SURNAME', 'min_length' => 'FORM_VALIDATION_NO_SURNAME')
            ),
            'email' => new sfValidatorEmail(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required' => 'FORM_VALIDATION_NO_EMAIL', 'invalid' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'min_length' => 'FORM_VALIDATION_NO_EMAIL')
            ),
            'password' => new sfValidatorString(
                array('min_length' => 6, 'trim' => true, 'required' => true),
                array('required' => 'FORM_VALIDATION_NO_PASSWORD', 'min_length' => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS')
            ),
            'confirm_password' => new sfValidatorString(
                array('min_length' => 6, 'trim' => true, 'required' => true),
                array('required' => 'FORM_VALIDATION_NO_PASSWORD', 'min_length' => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS')
            ),
        ));

        /*Password Match Validation*/
        $this->validatorSchema->setPostValidator(
            new sfValidatorSchemaCompare('confirm_password', sfValidatorSchemaCompare::EQUAL, 'password', array(), array('invalid' => 'FORM_VALIDATION_PASSWORD_MISMATCH'))
        );      

        /*Form Naming Convention*/
        $this->widgetSchema->setNameFormat('createlogin[%s]');
    }
}
