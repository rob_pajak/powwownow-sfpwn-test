<?php

class topupForm extends PWNForm {

    public static $_availiableTopups = array(1 => 1, 2 => 2);
    public static $_topupCurrency = 'GBP';
    public static $_topupVAT = 0.20;

    private function _topUpChoices() {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Number');
        $arr = array();
        foreach (self::$_availiableTopups as $k => $v) {
            if (self::$_topupVAT == 0) {
                $arr[$k] = format_currency($v, self::$_topupCurrency);
            } else {
                $arr[$k] = format_currency($v, self::$_topupCurrency) . ' +' . (self::$_topupVAT * 100) . '% VAT (' . format_currency(($v + ($v * self::$_topupVAT)), self::$_topupCurrency) . ')';
            }
        }
        return $arr;
    }

    public function configure() {
        
        // Card Types
        $cardTypes = array(
            ''     => 'Select...',
            'AMEX' => 'American Express',
            'MSCD' => 'MasterCard',
            'JCB'  => 'JCB',
            'MAES' => 'Maestro',
            'VISA' => 'Visa'
        );

        $this->setWidgets(
            array(
                'topup-amount'  => new sfWidgetFormChoice(array(
                    'expanded'          => true,
                    'choices'           => $this->_topupChoices(),
                    'translate_choices' => false,
                )),
                'auto-recharge' => new sfWidgetFormInputCheckbox(array(
                    'label' => 'Recharge my account automatically'
                )),
                'cardtypes'     => new sfWidgetFormSelect(array(
                    'choices'           => $cardTypes,
                    'translate_choices' => false
                ), array('required' => 'required')),
                'affiliate-code' => new sfWidgetFormInputHidden(),
            )
        );

        unset($cardTypes['']);

        $this->setValidators(array(
            'cardtypes' => new sfValidatorChoice(array('choices' => array_keys($cardTypes), 'required' => true)),
        ));

        $this->widgetSchema->setNameFormat('topup[%s]');
    }

}
