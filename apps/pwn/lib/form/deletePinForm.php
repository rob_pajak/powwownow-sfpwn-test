<?php

/**
 * Delete Pin Form
 *
 * @author Alexander Farrow
 *
 */
class deletePinForm extends ajaxForm {

    public function configureForm() {
        // Form Elements
        $this->setWidgets(array(
            'email' => new sfWidgetFormInputText(),
            'pin'   => new sfWidgetFormInputText(),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'email' => 'Email:',
            'pin'   => 'Pin:',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'email' => $validationFactory->email(),
            'pin'   => $validationFactory->pinRef(),
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('deletepin[%s]');

    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
    public function doPostValidation(array $args) {
        // Email Check
        if (empty($args['email']) || !filter_var($args['email'], FILTER_VALIDATE_EMAIL)) {
            return array('message' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'field_name' => 'unsubscribe[email]');
        }
        return false;
    }

}
