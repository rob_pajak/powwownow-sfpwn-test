<?php
/**
 * Login - Create-A-Login Form
 * 
 * @author Asfer
 *
 */
class LoginCreateALoginForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements
        $this->setWidgets(array(
            'first_name'       => new sfWidgetFormInputText(),
            'last_name'        => new sfWidgetFormInputText(),
            'email'            => new sfWidgetFormInputText(),
            'password'         => new sfWidgetFormInputPassword(),
            'confirm_password' => new sfWidgetFormInputPassword(),
            'redirect'         => new sfWidgetFormInputHidden(),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'first_name'       => 'First Name *',
            'last_name'        => 'Last Name *',
            'email'            => 'Email *',
            'password'         => 'Password *',
            'confirm_password' => 'Confirm Password *',
            'redirect'         => 'Redirect',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'first_name'       => $validationFactory->firstName(),
            'last_name'        => $validationFactory->lastName(),
            'email'            => $validationFactory->email(),
            'password'         => $validationFactory->passwordNew(true),
            'confirm_password' => new sfValidatorString(array('required' => false, 'trim' => true)),
            'redirect'         => new sfValidatorString(array('required' => false, 'trim' => true)),
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('createalogin[%s]');

        // Post Form Validation - Password Check
        $this->validatorSchema->setPostValidator($validationFactory->confirmPassword());
    }

    /**
     * Post Validation
     *
     * Additional Password Check
     *
     */
    public function doPostValidation(Array $args) {
        $passwordCheck = Common::checkifInvalidPassword($args['password']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'password');
        }
        $passwordCheck = Common::checkifInvalidPassword($args['confirm_password']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'confirm_password');
        }        

        return false;
    }
}