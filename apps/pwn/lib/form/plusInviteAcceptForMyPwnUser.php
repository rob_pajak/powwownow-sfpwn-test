<?php
/**
 * 
 * @author Wiseman
 */
class plusInviteAcceptForMyPwnUser extends PWNForm
{
    public function configure()
    {
        $this->setWidgets(array(
            'password'         => new sfWidgetFormInputPassword(),
            'account_id'       => new sfWidgetFormInputHidden(),
            'contact_ref'      => new sfWidgetFormInputHidden(),
            'hash'             => new sfWidgetFormInputHidden(),
        ));
    }

}