<?php
/**
 * The Request Welcome Pack Form. This is used on the second step of the Its-Your-Call page
 *
 * @author Asfer Tamimi
 *
 */
class RequestWelcomePackFormItsYourCall extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements

        $this->setWidgets(array(
            'first_name'      => new sfWidgetFormInputText(),
            'last_name'       => new sfWidgetFormInputText(),
            'company'         => new sfWidgetFormInputText(),
            'address'         => new sfWidgetFormInputText(),
            'town'            => new sfWidgetFormInputText(),
            'county'          => new sfWidgetFormInputText(),
            'postcode'        => new sfWidgetFormInputText(),
            'country'         => new sfWidgetFormInputText(),
            'password'        => new sfWidgetFormInputPassword(),
            'password_retype' => new sfWidgetFormInputPassword(),
            'question'        => new sfWidgetFormSelect(array('choices' => array('' => 'Please select below', 'Yes' => 'Yes', 'No' => 'No'),'default' => '')),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'first_name'      => 'First Name',
            'last_name'       => 'Last Name',
            'company'         => 'Company',
            'address'         => 'Address',
            'town'            => 'Town',
            'county'          => 'County',
            'postcode'        => 'Postcode',
            'country'         => 'Country',
            'password'        => 'Choose a password (optional)',
            'password_retype' => 'Retype your password',
            'question'        => 'Is this the first conference you\'ve ever held?',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'first_name'      => $validationFactory->firstName(),
            'last_name'       => $validationFactory->lastName(),
            'company'         => $validationFactory->company(),
            'address'         => $validationFactory->address(),
            'town'            => $validationFactory->town(),
            'county'          => $validationFactory->county(),
            'postcode'        => $validationFactory->postcode(),
            'country'         => $validationFactory->country(),
            'password'        => $validationFactory->commonField(), // Not Working as Password Check
            'password_retype' => $validationFactory->commonField(), // Not Working as Password Check
            'question'        => $validationFactory->question(true)
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('requestWelcomePack[%s]');
    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
    public function doPostValidation($args) {
        // Since the Password Fields are Optional, Have a check here to see that both the password fields are empty
        // If they are empty, skip validation and pass it.
        if (empty($args['password']) && empty($args['password_retype'])) {
            return false;
        }

        // Password Checks
        $passwordCheck = Common::checkifInvalidPassword($args['password']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'requestWelcomePack[password]');
        }
        $passwordCheck = Common::checkifInvalidPassword($args['password_retype']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'requestWelcomePack[password_retype]');
        }
        if ($args['password'] != $args['password_retype']) {
            return array('message' => 'FORM_VALIDATION_PASSWORD_MISMATCH', 'field_name' => 'requestWelcomePack[password_retype]');
        }

        return false;
    }


    /**
     * Add the widgets and the validation.
     *
     * @param object  $widget
     * @param object  $inputs
     * @return string $formatting
     * @author Asfer Tamimi
     *
     */
    public function setRadioFormatting($widget, $inputs) {
        $formatting = '';
        foreach ($inputs as $input) {
            $formatting.= $input['input'] . $input['label'];
        }
        return $formatting;
    }
}
