<?php
/**
 * Plus Login - Login Form
 * 
 * @author Asfer
 */
class PlusLoginLoginForm extends PWNForm
{
    public function configure()
    {
        /*Form Elements*/
        $this->setWidgets(array(
            'email'    => new sfWidgetFormInputText(),
            'password' => new sfWidgetFormInputPassword()
        ));

        /*Form Validation*/
        $this->setValidators(array(
            'email' => new sfValidatorEmail(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required' => 'FORM_VALIDATION_NO_EMAIL', 'invalid' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'min_length' => 'FORM_VALIDATION_NO_EMAIL')
            ),
            'password' => new sfValidatorString(
                array('min_length' => 6, 'trim' => true, 'required' => true),
                array('min_length' => 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS')
            ),
        ));

        /*Form Naming Convention*/
        $this->widgetSchema->setNameFormat('login[%s]');
    }
}
