<?php
/**
 * Pin Reminder Form
 */
class PlusSwitchRegistrationForm extends ajaxForm
{
    public function configureForm()
    {
        $this->setWidgets(array(
            'email'                 => new sfWidgetFormInputText(),
            'terms_and_conditions'  => new sfWidgetFormInputText(),
            'first_name'            => new sfWidgetFormInputText(),
            'last_name'             => new sfWidgetFormInputText(),
            'company_name'          => new sfWidgetFormInputText(),
            'business_phone'        => new sfWidgetFormInputText(),
            'password'              => new sfWidgetFormInputPassword(),
            'confirm_password'      => new sfWidgetFormInputPassword(),
        ));
    }

    
}
