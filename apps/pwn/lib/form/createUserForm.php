<?php

/**
 * The Create User Form
 *
 * @author Asfer Tamimi
 *
 */
class createUserForm extends ajaxForm {

    public function configureForm() {
        // Form Elements
        $this->setWidgets(array(
            'email'               => new sfWidgetFormInputText(),
            'cost_code'           => new sfWidgetFormInputText(),
            'title'               => new sfWidgetFormInputText(),
            'first_name'          => new sfWidgetFormInputText(),
            'last_name'           => new sfWidgetFormInputText(),
            'phone_number'        => new sfWidgetFormInputText(),
            'password'            => new sfWidgetFormInputPassword(),
            'confirm_password'    => new sfWidgetFormInputPassword(),
            'notify_user'         => new sfWidgetFormInputCheckbox(array(),array('checked' => 'checked')),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'email'               => 'Email *',
            'cost_code'           => 'Cost code',
            'title'               => 'Title',
            'first_name'          => 'First name *',
            'last_name'           => 'Last name *',
            'phone_number'        => 'Phone number',
            'password'            => 'Password *',
            'confirm_password'    => 'Confirm password *',
            'notify_user'         => 'Notify user by email'
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'first_name'          => $validationFactory->firstName(),
            'last_name'           => $validationFactory->lastName(),
            'password'            => $validationFactory->password(),
            'confirm_password'    => $validationFactory->genericField(),
            'email'               => $validationFactory->email(),
            
            'cost_code'           => $validationFactory->genericField(),
            'title'               => $validationFactory->genericField(),
            'phone_number'        => $validationFactory->genericField(),
            'notify_user'         => $validationFactory->genericField(),
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('createUser[%s]');

        $this->validatorSchema->setPostValidator($validationFactory->confirmPassword());
    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
    public function doPostValidation(array $args) {
        // Password Checks
        $passwordCheck = Common::checkifInvalidPassword($args['password']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'createUser[password]');
        }
        $passwordCheck = Common::checkifInvalidPassword($args['confirm_password']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'createUser[confirm_password]');
        }
        
        // Email Check
        if (empty($args['email']) || !filter_var($args['email'], FILTER_VALIDATE_EMAIL)) {
            return array('message' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'field_name' => 'createUser[email]');
        }

        return false;
    }  

}
