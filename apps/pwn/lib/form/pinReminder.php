<?php
/**
 * Pin Reminder Form
 */
class pinReminderForm extends ajaxForm
{
    public function configureForm()
    {
        $this->setWidgets(array(
            'email'    => new sfWidgetFormInputText(),
            'mobile_number' => new sfWidgetFormInputText()
        ));

        $validationFactory = new validationGroupFactory();

        $this->setValidators(array(
            'email'         => $validationFactory->email(),
            'mobile_number' => $validationFactory->mobileNumber()
        ));

        $this->widgetSchema->setNameFormat('pinReminder[%s]');

    }

    /**
     * Calls Hermes to 'persist' the form
     *
     * locale is required here but is not part of the form, so is required as an $additionalArgs element
     *
     * @param array $additionalArgs Additional arguments required to persist the data. If a value already exists in the form
     * then they can be over written here
     * @return bool
     * @throws AjaxFormException
     */
    public function save(array $additionalArgs = null)
    {
        $args = array_merge($additionalArgs, $this->getValues());
        $args['request_source'] = null;
        if (!empty($additionalArgs['request_source'])) {
            $args['request_source'] = $additionalArgs['request_source'];
        }

        try {
            Hermes_Client_Rest::call('sendPinReminderEmail',array(
                    'email' => $args['email'],
                    'locale' => $args['locale'],
                    'mobile_number' => $args['mobile_number'],
                    'request_source' => $args['request_source'],
            ));
        } catch (Hermes_Client_Exception $e) {
            $errorArr = null;
            if ($e->getResponseBody()) {
                switch ($e->getCode()) {
                    case '001:000:000': // CONTACT_DOES_NOT_EXIST
                        $errorArr = array('message' => 'FORM_VALIDATION_EMAIL_NOT_FOUND', 'field_name' => 'alert');
                        break;
                    case '001:006:001': // SMS_NOT_SUPPORTED_FOR_LOCALE
                        $errorArr = array('message' => 'LOCALE_NOT_SUPPORTED', 'field_name' => 'alert');
                        break;
                    case '001:000:029':
                        $errorArr = array('message' => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE1', 'field_name' => 'alert');
                        break;
                    case '001:000:030':
                    case '001:000:031':
                        $errorArr = array('message' => 'API_RESPONSE_CONTACT_IS_NOT_STANDARD_POWWOWNOW_USER1', 'field_name' => 'alert');
                        break;
                    default:
                        $errorArr = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                }
            }
            // Rethrow exception as a FormException with relevant error message array
            throw new AjaxFormException('Hermes returned an exception', 0, $e, $errorArr);
        } catch (Exception $e) {
            throw new AjaxFormException('Hermes returned an exception', 0, $e);
        }

        return true;
    }
}
