<?php

/**
 * The Request Welcome Pack Form Basic - this version currently only supports 'Basic' users and
 * does not show the country field to the user
 */
class RequestWelcomePackBasicForm extends ajaxForm
{

    public function configureForm()
    {

        $this->setWidgets(array(
            'first_name' => new sfWidgetFormInputText(),
            'last_name'  => new sfWidgetFormInputText(),
            'company'    => new sfWidgetFormInputText(),
            'address'    => new sfWidgetFormInputText(),
            'town'       => new sfWidgetFormInputText(),
            'county'     => new sfWidgetFormInputText(),
            'postcode'   => new sfWidgetFormInputText(),
        ));

        $validationFactory = new validationGroupFactory();

        $this->setValidators(array(
            'first_name' => $validationFactory->firstName(),
            'last_name'  => $validationFactory->lastName(),
            'address'    => $validationFactory->address(),
            'town'       => $validationFactory->town(),
            'postcode'   => $validationFactory->postcode(),
            'company'    => $validationFactory->company(),
            'county'     => $validationFactory->county()
        ));

        $this->widgetSchema->setNameFormat('requestWelcomePackForm[%s]');

    }

    /**
     * Calls Hermes to update contact details and then order the wallet card
     *
     * @param $contactRef
     * @param $pinRef
     * @param $countryCode
     * @return bool
     * @throws AjaxFormException
     */
    public function save($contactRef, $pinRef, $countryCode)
    {
        try {
            /**
             * Update contact details
             */
            Hermes_Client_Rest::call(
                'updateContact',
                array(
                    'contact_ref' => $contactRef,
                    'first_name'  => $this->getValue('first_name'),
                    'last_name'   => $this->getValue('last_name')
                )
            );

            /**
             * Order Welcome pack
             */
            Hermes_Client_Rest::call(
                'createWalletCardRequest',
                array(
                    'contact_ref'  => $contactRef,
                    'pin_ref'      => $pinRef,
                    'country_code' => $countryCode,
                    'organisation' => $this->getValue('company'),
                    'building'     => '',
                    'street'       => $this->getValue('address'),
                    'town'         => $this->getValue('town'),
                    'county'       => $this->getValue('county'),
                    'post_code'    => $this->getValue('postcode')
                )
            );

        } catch (Hermes_Client_Exception $e) {
            $errorArr = null;
            if ($e->getResponseBody()) {
                switch ($e->getCode()) {
                    case '001:001:001':
                        $errorArr = array('message' => 'WALLET_CARD_REQUEST_EXCEEDED_MAX', 'field_name' => 'alert');
                        break;
                    case '001:000:004':
                        $errorArr = array('message' => 'WALLET_CARD_REQUEST_NO_PERMISSION', 'field_name' => 'alert');
                        break;
                    default:
                        $errorArr = array('message' => 'WALLET_CARD_REQUEST_FAILURE', 'field_name' => 'alert');
                        break;
                }
            }

            // Rethrow exception as a FormException with relevant error message array
            throw new AjaxFormException('Hermes returned an exception', 0, $e, $errorArr);
        } catch (Exception $e) {
            throw new AjaxFormException('Hermes returned an exception', 0, $e);
        }

        return true;

    }

}
