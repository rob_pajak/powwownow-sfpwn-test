<?php
/**
 * Represents the organisation details form on the Account Details page.
 * Note that this form is specifically for Plus admins.
 *
 * @author Maarten Jacobs
 */
class OrganisationDetailsForm extends AjaxForm {

    /**
     * @var sfUser
     */
    protected $user;

    /**
     * Passes default values and form configuration to the new form.
     *
     * @param array $defaults
     * @param array $options
     * @param string $CSRFSecret
     */
    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    /**
     * Add the widgets and the validation.
     *
     * @author Maarten Jacobs
     */
    public function configureForm() {
        // Create the widgets.
        $widgets = array(
            'account_name'  => new sfWidgetFormInputText(),
            'organisation'  => new sfWidgetFormInputText(),
            'vat_number'    => new sfWidgetFormInputText(),
        );
        $this->setWidgets($widgets);

        // Set the labels to reflect required items.
        $labels = array(
            'account_name'  => 'Account Name*',
            'organisation'  => 'Organisation*',
            'vat_number'    => 'VAT Number',
        );
        $this->widgetSchema->setLabels($labels);

        // Set the validators.
        // Note that I'm passing the validation constant as a message.
        $validators = array(
            // Required elements.
            'account_name'  => new sfValidatorString(
                array(
                    'required' => true,
                    'max_length' => 60
                ),
                array(
                    'required' => 'FORM_VALIDATION_NO_ACCOUNT_NAME',
                    'max_length' => 'FORM_VALIDATION_MAXLENGTH_ACCOUNT_NAME'
                )
            ),
            'organisation'  => new sfValidatorString(
                array(
                    'required' => true,
                    'max_length' => 100
                ),
                array(
                    'required' => 'FORM_VALIDATION_NO_ORGANISATION',
                    'max_length' => 'FORM_VALIDATION_MAXLENGTH_ORGANISATION',
                )
            ),

            // Optional elements.
            'vat_number'    => new sfValidatorString(
                array('required' => false, 'max_length' => 20),
                array('max_length' => 'FORM_VALIDATION_MAXLENGTH_VAT_NUMBER')
            ),
        );
        $this->setValidators($validators);

        $this->widgetSchema->setNameFormat('organisation[%s]');
    }
}
