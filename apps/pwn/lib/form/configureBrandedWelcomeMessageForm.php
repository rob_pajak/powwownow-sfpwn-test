<?php

/**
 * Represents the form for adding and configuring a Branded Welcome Message.
 */
class ConfigureBrandedWelcomeMessageForm extends BaseForm
{

    /**
     * Available DNISes for the client to select for the new Branded Welcome Message.
     *
     * @var array
     */
    protected $availableDNISes;

    /**
     * The name format used to collect the form data.
     *
     * In an HTTP POST request, this means that variables are enclosed in $nameFormat[(Form field key)].
     *
     * @var string
     */
    protected $nameFormat = 'bwm';

    /**
     * Returns the default name format used, if none is explicitly set after construction of the object.
     *
     * @return string
     */
    public function defaultNameFormat()
    {
        return 'bwm';
    }

    /**
     * Converts a list of available of product DNISes (returned from BWM.getProductDialInNumbersWithRates into a
     * map of product id to description.
     *
     * @param array $dnises
     * @return array
     */
    protected function processAvailableDNISesForPresentation(array $dnises)
    {
        $processedDNISes = array();

        foreach ($dnises as $dnis) {
            $processedDNISes[$dnis['product_id']] = sprintf("%s (%s). Cost: £%d.", $dnis['national_formatted'], $dnis['country_code'], $dnis['cyclic_cost']);
        }

        return $processedDNISes;
    }

    /**
     * Sets the DNISes which will be shown on the form as selectable DNISes.
     *
     * @param array $dnises
     */
    protected function setAvailableDNISes(array $dnises)
    {
        // First convert the DNISes to a more presentable state.
        $dnises = $this->processAvailableDNISesForPresentation($dnises);

        $this->availableDNISes = $dnises;
    }

    /**
     * Initialise and configure the form.
     *
     * The options parameter takes the available_dnises key, which is the list of available DNISes for the current user.
     *
     * @param array $defaults
     * @param array $options
     * @param null $CSRFSecret
     */
    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (isset($options['available_dnises'])) {
            $this->setAvailableDNISes($options['available_dnises']);
            unset($options['available_dnises']);
        }

        if (isset($options['available_dnises'])) {
            $this->setAvailableDNISes($options['available_dnises']);
            unset($options['available_dnises']);
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    /**
     * Adds the required widgets to the form.
     */
    public function configure()
    {
        $this->setWidgets(array(
            'product_name' => new sfWidgetFormInputText(),
            'available_DNISes' => new sfWidgetFormSelectMany(array('choices' => $this->availableDNISes)),
            'transcript' => new sfWidgetFormTextarea(),
        ));

        if (!empty($this->nameFormat)) {
            $this->widgetSchema->setNameFormat($this->nameFormat . '[%s]');
        }

        $this->setValidators(array(
            'product_name' => new sfValidatorString(array('min_length' => 1)),
            'available_DNISes' => new sfValidatorChoice(array('choices' => array_keys($this->availableDNISes), 'multiple' => true)),
            'transcript' => new sfValidatorString(array('min_length' => 1)),
        ));
    }

}
