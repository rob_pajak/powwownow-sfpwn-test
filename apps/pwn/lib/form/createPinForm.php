<?php
/**
 * The Create Pin Form
 *
 * @author Asfer Tamimi
 *
 */
class CreatePinForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        $contactsAll = PinHelper::getContactPinPairs($this->user->getAttribute('contact_ref'),'ADMIN');
        
        // Filter Contacts by Unique Contact Ref
        $contacts = array();
        $contacts[''] = 'Create a new user';
        foreach ($contactsAll as $masterPin => $contact) {
            $contacts[$contact['chair_contact_ref']] = $contact['chair_email'];
        }

        //Form Elements
        $this->setWidgets(array(
            'contact_ref'         => new sfWidgetFormSelect(array('choices' => $contacts, 'default' => '')),
            'email'               => new sfWidgetFormInputText(),
            'cost_code'           => new sfWidgetFormInputText(),
            'title'               => new sfWidgetFormInputText(),
            'first_name'          => new sfWidgetFormInputText(),
            'middle_initials'     => new sfWidgetFormInputText(),
            'last_name'           => new sfWidgetFormInputText(),
            'phone_number'        => new sfWidgetFormInputText(),
            'mobile_phone_number' => new sfWidgetFormInputText(),
            'password'            => new sfWidgetFormInputPassword(),
            'confirm_password'    => new sfWidgetFormInputPassword(),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'contact_ref'         => 'Choose existing user',
            'email'               => 'Email *',
            'cost_code'           => 'Cost code *',
            'title'               => 'Title',
            'first_name'          => 'First name *',
            'middle_initials'     => 'Middle initials',
            'last_name'           => 'Last name *',
            'phone_number'        => 'Phone number',
            'mobile_phone_number' => 'Mobile phone number',
            'password'            => 'Password *',
            'confirm_password'    => 'Confirm password *',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Configure the validation element for the cost code field.
        $costCodeValidation = $validationFactory->genericField();
        $costCodeValidation->setMessage('max_length', 'FORM_VALIDATION_INVALID_COST_CODE');

        // Form Validation
        $this->setValidators(array(
            'contact_ref'         => $validationFactory->genericField(),
            'email'               => $validationFactory->genericField(),
            'cost_code'           => $costCodeValidation,
            'title'               => $validationFactory->genericField(),
            'first_name'          => $validationFactory->genericField(),
            'middle_initials'     => $validationFactory->genericField(),
            'last_name'           => $validationFactory->genericField(),
            'phone_number'        => $validationFactory->genericField(),
            'mobile_phone_number' => $validationFactory->genericField(),
            'password'            => $validationFactory->genericField(),
            'confirm_password'    => $validationFactory->genericField(),
        ));        

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('createPin[%s]');

        $this->validatorSchema->setPostValidator($validationFactory->confirmPassword());
    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
    public function doPostValidation(array $args) {
        // Check which Sub Form was Submitted:
        // 1 - Existing User
        // 2 - New User
        if (empty($args['contact_ref'])) {
            // Other Checks
            if (empty($args['email']) || !filter_var($args['email'], FILTER_VALIDATE_EMAIL)) {
                return array('message' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'field_name' => 'createPin[email]');
            } elseif (empty($args['first_name'])) {
                return array('message' => 'FORM_VALIDATION_FIRST_NAME_EMPTY', 'field_name' => 'createPin[first_name]');
            } elseif (empty($args['last_name'])) {
                return array('message' => 'FORM_VALIDATION_LAST_NAME_EMPTY', 'field_name' => 'createPin[last_name]');
            }

            // Password Checks
            $passwordCheck = Common::checkifInvalidPassword($args['password']);
            if (false!==$passwordCheck) {
                if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                    $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
                }
                return array('message' => $passwordCheck, 'field_name' => 'createPin[password]');
            }
            $passwordCheck = Common::checkifInvalidPassword($args['confirm_password']);
            if (false!==$passwordCheck) {
                if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                    $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
                }
                return array('message' => $passwordCheck, 'field_name' => 'createPin[confirm_password]');
            }
        } else {
            // Existing User Checks
            $contacts = PinHelper::getContactPinPairs($args['current_contact_ref'],'ADMIN');
            
            // In Valid Contact Ref Check
            $validContactRef = false;
            foreach($contacts as $contact) {
                if ($args['contact_ref'] == $contact['chair_contact_ref']) {
                    $validContactRef = true;
                    break;
                }
            }

            if(!$validContactRef) {
                return array('message' => 'FORM_VALIDATION_INVALID_CONTACT', 'field_name' => 'createPin[contact_ref]');
            }
        }

        return false;
    }
}
