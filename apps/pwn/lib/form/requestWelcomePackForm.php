<?php
/**
 * The Request Welcome Pack Form
 *
 * @author Asfer Tamimi
 *
 */
class RequestWelcomePackForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements

        $this->setWidgets(array(
            'first_name' => new sfWidgetFormInputText(),
            'last_name'  => new sfWidgetFormInputText(),
            'company'    => new sfWidgetFormInputText(),
            'address'    => new sfWidgetFormInputText(),
            'town'       => new sfWidgetFormInputText(),
            'county'     => new sfWidgetFormInputText(),
            'postcode'   => new sfWidgetFormInputText(),
            'country'    => new sfWidgetFormSelect(array('choices' => array())),
            'pin_ref'    => new sfWidgetFormChoice(array('choices' => array(), 'multiple' => true, 'expanded' => true)),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'first_name' => 'First Name *',
            'last_name'  => 'Last Name *',
            'company'    => 'Company',
            'address'    => 'Address *',
            'town'       => 'Town *',
            'county'     => 'County',
            'postcode'   => 'Postcode *',
            'country'    => 'Country *',
            'pin_ref'    => 'Pin Ref',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        if (!$this->user->hasCredential('admin')) {
            $this->setValidators(array(
                'first_name' => $validationFactory->firstName(),
                'last_name'  => $validationFactory->lastName(),
                'address'    => $validationFactory->address(),
                'town'       => $validationFactory->town(),
                'postcode'   => $validationFactory->postcode(),
                'company'    => $validationFactory->company(),
                'county'     => $validationFactory->county(),
                'country'    => $validationFactory->country(),
                'pin_ref'    => $validationFactory->pinRef(false),
            ));
        } else {
            $this->setValidators(array(
                'address'    => $validationFactory->address(),
                'town'       => $validationFactory->town(),
                'postcode'   => $validationFactory->postcode(),
                'company'    => $validationFactory->company(),
                'county'     => $validationFactory->county(),
                'country'    => $validationFactory->country(),
                'pin_ref'    => $validationFactory->pinRef(false),
            ));
        }

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('requestWelcomePack[%s]');  
    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
    public function doPostValidation(Array $args) {
        // NO PINs Selected
        if (empty($args['pin_ref'])) {
            return array('message' => 'FORM_VALIDATION_NO_PIN', 'field_name' => 'alert');
        }

        // Non Admin, with More than 1 PIN Selected
        if (count($args['pin_ref']) > 1 && !$this->user->hasCredential('admin')) {
            return array('message' => 'WALLET_CARD_REQUEST_ONE_PIN', 'field_name' => 'alert');
        }

        return false;
    }    
}
