<?php

/**
 * Represents the password form on the Account Details + User Details page.
 * @todo Use the New Form Validation and Form Class + Also set a Form Schema
 * @author Maarten Jacobs
 */
class PasswordForm extends AjaxForm {

    /**
     * Add the widgets and the validation.
     *
     * @author Maarten Jacobs
     */
    public function configureForm() {
        // Create the widgets.
        $widgets = array(
            'password'          => new sfWidgetFormInputPassword(),
            'confirm_password'  => new sfWidgetFormInputPassword(),
        );
        $this->setWidgets($widgets);

        // Set the labels to reflect required items.
        $labels = array(
            'password'          => 'Password*',
            'confirm_password'  => 'Confirm Password*',
        );
        $this->widgetSchema->setLabels($labels);

        // Set the validators.
        $validatorFactory = new validationGroupFactory();
        $validators = array(
            'password'         => $validatorFactory->password()->setOption('required', true)
                ->setMessage('min_length', 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS')
                ->setMessage('max_length', 'FORM_VALIDATION_PASSWORD_TOO_LONG'),
            'confirm_password' => $validatorFactory->password()->setOption('required', true)
                ->setMessage('min_length', 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS')
                ->setMessage('max_length', 'FORM_VALIDATION_PASSWORD_TOO_LONG'),
        );
        $this->setValidators($validators);
        // Pass a Post-validator that will compare password to confirm_password.
        $this->validatorSchema->setPostValidator(
            $validatorFactory->confirmPassword(),
            array('throw_global_error' => true),
            array('invalid' => 'FORM_VALIDATION_PASSWORD_MISMATCH')
        );

        $this->widgetSchema->setNameFormat('authentication[%s]');
    }

}
