<?php
/**
 * The Web Conferencing (Auth) Form
 *
 * @author Asfer Tamimi
 *
 */
class WebConferencingForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        $first_name = $this->user->getAttribute('first_name',NULL);
        $last_name  = $this->user->getAttribute('last_name',NULL);

        // Form Elements
        $this->setWidgets(array(
            'first_name' => new sfWidgetFormInputText(array('default' => $first_name)),
            'last_name'  => new sfWidgetFormInputText(array('default' => $last_name)),
            'password'   => new sfWidgetFormInputPassword(),
            'filetype'   => new sfWidgetFormInputHidden(),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'first_name' => 'First Name *',
            'last_name'  => 'Last Name *',
            'password'   => 'Password *',
            'filetype'   => 'Filetype *',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        if (empty($first_name) || empty($last_name)) {
            $this->setValidators(array(
                'first_name' => $validationFactory->firstName(),
                'last_name'  => $validationFactory->lastName(),
                'password'   => $validationFactory->password(),
                'filetype'   => $validationFactory->commonField(true,3,false),
            ));
        } else {
            $this->setValidators(array(
                'password'   => $validationFactory->password(),
                'filetype'   => $validationFactory->commonField(true,3,false),
            ));
        }

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('webConferencing[%s]');
    }

    /**
     * Post Validation
     *
     * Password Check + Authenticate Check
     * @param array $args
     *
     */
    public function doPostValidation(array $args) {
        $passwordCheck = Common::checkifInvalidPassword($args['password']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'password');
        }

        return false;
    }    
}
