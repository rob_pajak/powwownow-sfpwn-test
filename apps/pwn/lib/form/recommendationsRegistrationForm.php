<?php

class recommendationsRegistrationForm extends PWNForm {

    public function configure() {

        $countries = array(
            'GBR' => 'United Kingdom'
        );

        $this->setWidgets(array(
            'first_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small required', 'tabindex' => 1)),            
            'last_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small required', 'tabindex' => 2)),  
            'company' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small required', 'tabindex' => 3)),            
            'industry' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small','tabindex' => 4)),   
            'address_line_one' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small required', 'tabindex' => 5)),   
            'address_line_two' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small', 'tabindex' => 6)),   
            'town' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small required', 'tabindex' => 7)),   
            'postcode' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-medium font-small required',
                
                'tabindex' => 8
                )),   
            
            'country' => new sfWidgetFormI18nChoiceCountry(array(),array('class' => 'mypwn-input input-large font-small required', 'tabindex' => 9)),
            //'country' => new sfWidgetFormSelect(array("choices" => $countries, "default" => "GBR"), array('class' => 'mypwn-input input-large font-small', 'tabindex' => 9)),   
            'phone' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small', 'tabindex' => 10)),   
            'email' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small required',  'type' => 'email', 'tabindex' => 11)),   

        ));
  
        
        $this->widgetSchema->setLabels(array(
            'friend1_first_name' => '<b>Friend 1</b> First name',
            'friend1_last_name' => 'Last name',
            'friend1_email' => 'Email',
            'friend2_first_name' => '<b>Friend 2</b> First name',
            'friend2_last_name' => 'Last name',
            'friend2_email' => 'Email',
            'friend3_first_name' => '<b>Friend 3</b> First name',
            'friend3_last_name' => 'Last name',
            'friend3_email' => 'Email',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email'
        ));



        $this->widgetSchema->setNameFormat('registration[%s]');
    }

}