<?php

class recommendationsForm extends PWNForm {

    public function configure() {

        $this->setWidgets(array(
            'friend1_first_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small persist-required required','tabindex' => 1)),
            'friend1_last_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small persist-required required','tabindex' => 2)),
            'friend1_email' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small persist-required required','tabindex' => 3, 'type' => 'email')),
            'friend2_first_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small','tabindex' => 4)),
            'friend2_last_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small','tabindex' => 5)),
            'friend2_email' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small','tabindex' => 6, 'type' => 'email')),
            'friend3_first_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small','tabindex' => 7)),
            'friend3_last_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small','tabindex' => 8)),
            'friend3_email' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-large font-small','tabindex' => 9, 'type' => 'email')),
            'first_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-medium font-small required','tabindex' => 10)),
            'last_name' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-medium font-small required','tabindex' => 11)),
            'email' => new sfWidgetFormInputText(array(), array('class' => 'mypwn-input input-medium font-small required', 'type' => 'email','tabindex' => 12)),
            'contact_ref' => new sfWidgetFormInputHidden(),
        ));
        $this->widgetSchema->setLabels(array(
            'friend1_first_name' => '<strong>Friend 1</strong> First name',
            'friend1_last_name' => 'Last name',
            'friend1_email' => 'Email address',
            'friend2_first_name' => '<strong>Friend 2</strong> First name',
            'friend2_last_name' => 'Last name',
            'friend2_email' => 'Email address',
            'friend3_first_name' => '<strong>Friend 3</strong> First name',
            'friend3_last_name' => 'Last name',
            'friend3_email' => 'Email address',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email'
        ));



        $this->widgetSchema->setNameFormat('recommendations[%s]');
    }

}