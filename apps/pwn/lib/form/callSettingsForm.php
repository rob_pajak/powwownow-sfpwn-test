<?php

/**
 * Class CallSettingsForm
 * Used in the CallSettings Page
 */
class CallSettingsForm extends PowwownowBaseForm
{
    public function configure()
    {
        $lists    = $this->getFormLists();
        $values   = $this->getFormDefaultValues();
        $class    = 'input-large font-small mypwn-input';
        $onHoldJS = 'javascript:callSettingsSetupMusicOnHold(this.options[selectedIndex].value, true); return false;';
        $costCodeReadOnly = '';
        $formValidation = new PowwownowFormValidation();

        if ($this->user->hasCredential('PLUS_USER') || $this->user->hasCredential('PREMIUM_USER')) {
            $costCodeReadOnly = 'readonly';
        }

        $formArray = array(
            'pin_ref' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormChoice',
                    'params' => array(
                        'choices' => $lists['pin_ref'],
                        'default' => $values['pin_ref']
                    ),
                    'attributes' => array('class' => $class)
                ),
                'label' => 'Pin Ref',
                'validator' => $formValidation->getValidationForPinRef($lists['pin_ref'])
            ),
            'voice_prompt_language' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormSelect',
                    'params' => array(
                        'choices' => $lists['voice_prompt_language'],
                        'default' => $values['voice_prompt_language']
                    ),
                    'attributes' => array('class' => $class)
                ),
                'label' => 'Voice prompt language *',
                'validator' => $formValidation->getValidationForVoicePromptLanguage(
                    array_keys($lists['voice_prompt_language'])
                )
            ),
            'on_hold_music' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormSelect',
                    'params' => array(
                        'choices' => $lists['on_hold_music'],
                        'default' => $values['on_hold_music'],
                    ),
                    'attributes' => array(
                        'class' => $class,
                        'onChange' => $onHoldJS
                    )
                ),
                'label' => 'On-hold music',
                'validator' => $formValidation->getValidationForOnHoldMusic(array_keys($lists['on_hold_music']))
            ),
            'entry_exit_announcements' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormSelect',
                    'params' => array(
                        'choices' => $lists['entry_exit_announcements'],
                        'default' => $values['entry_exit_announcements'],
                    ),
                    'attributes' => array('class' => $class)
                ),
                'label' => 'Entry and exit announcements *',
                'validator' => $formValidation->getValidationForEntryExitAnnouncements(
                    array_keys($lists['entry_exit_announcements'])
                )
            ),
            'announcement_types' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormSelect',
                    'params' => array(
                        'choices' => $lists['announcement_types'],
                        'default' => $values['announcement_types'],
                    ),
                    'attributes' => array('class' => $class)
                ),
                'label' => 'Announcement type *',
                'validator' => $formValidation->getValidationForAnnouncementType(
                    array_keys($lists['announcement_types'])
                )
            ),
            'play_participant_count' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormSelect',
                    'params' => array(
                        'choices' => $lists['play_participant_count'],
                        'default' => $values['play_participant_count'],
                    ),
                    'attributes' => array('class' => $class)
                ),
                'label' => 'Play Participant count on entry *',
                'validator' => $formValidation->getValidationForPlayParticipantCount(
                    array_keys($lists['play_participant_count'])
                )
            ),
            'chairman_present' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormSelect',
                    'params' => array(
                        'choices' => $lists['chairman_present'],
                        'default' => $values['chairman_present'],
                    ),
                    'attributes' => array('class' => $class)
                ),
                'label' => 'Chairperson Present *',
                'validator' => $formValidation->getValidationForChairmanPresent(
                    array_keys($lists['chairman_present'])
                )
            ),
            'description' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormInputText',
                    'params' => array('default' => $values['description']),
                    'attributes' => array('class' => $class)
                ),
                'label' => 'Description',
                'validator' => $formValidation->getValidationGenericNotRequired()
            ),
            'chairperson_only' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormSelectRadio',
                    'params' => array(
                        'choices' => $lists['chairperson_only'],
                        'default' => $values['chairperson_only'],
                        'formatter' => array($this, 'setRadioFormatting')
                    ),
                ),
                'label' => 'Chairperson Only',
                'validator' => $formValidation->getValidationGenericNotRequired()
            ),
            'cost_code' => array(
                'widget' => array(
                    'function' => 'sfWidgetFormInputText',
                    'params' => array('default' => $values['cost_code']),
                    'attributes' => array(
                        'class' => $class,
                        'readonly' => $costCodeReadOnly
                    )
                ),
                'label' => 'Cost code',
                'validator' => $formValidation->getValidationGenericNotRequired()
            ),
        );

        if ($this->user->getAttribute('service_user') === 'POWWOWNOW') {
            unset($formArray['chairman_present']);
            unset($formArray['description']);
            unset($formArray['chairperson_only']);
            unset($formArray['cost_code']);
        }

        $this->setFormArray($formArray);
        $this->finalConfiguration('callSettings');
    }

    /**
     * Add the widgets and the validation.
     *
     * @param object  $widget
     * @param object  $inputs
     * @return string $formatting
     * @author Asfer Tamimi
     *
     */
    public function setRadioFormatting($widget, $inputs) {
        $formatting = '';
        foreach ($inputs as $input) {
            $formatting.= $input['input'] . $input['label'];
        }
        return $formatting;
    }

    /**
     * Return the List Boxes Values
     *
     * @return array
     * @author Asfer Tamimi
     *
     */
    protected function getFormLists() {
        $voice_prompt_language = array(
            ''      => 'Use default',
            'nl_NL' => 'Dutch',
            'en_GB' => 'English',
            'fr_FR' => 'French',
            'de_DE' => 'German',
            'it_IT' => 'Italian',
            'pl_PL' => 'Polish',
            'es_ES' => 'Spanish',
            'sv_SE' => 'Swedish'
        );

        $on_hold_music = array(
            ''     => 'Default',
            '2160' => 'Mute',
            '-1'   => 'Shuffle *',
            '2162' => 'Calm River',
            '8'    => 'Eine Kleine Nacht',
            '7'    => 'Hispaniola',
            '1944' => 'Silent with beeps',
            '1941' => 'String Quartet no 23',
            '8277' => 'Pow Wow',
            '8276' => 'Pork Pie Dub',
            '8275' => 'Time To Shine',
            '8273' => 'Get On The Funk Bus',
            '8274' => 'Global Celebration',
            '8272' => '21st Century Disco',
            '4325' => 'Jingle Bells',
            '7488' => 'Christmas Jammin'
        );

        $entry_exit_announcements = array(
            '0' => 'None',
            '1' => 'Entry',
            '2' => 'Entry and Exit',
            '3' => 'Exit'
        );

        $announcement_types = array(
            '0' => 'Name',
            '1' => 'Tone'
        );

        $play_participant_count = array(
            '0' => 'Off',
            '1' => 'Only for Chairperson',
            '2' => 'Play for all'
        );

        $chairman_present = array(
            ''           => 'Please Select',
            'Optional'   => 'Optional',
            'Start'      => 'Start',
            'Throughout' => 'Throughout'
        );

        $chairperson_only = array(
            'Yes' => 'Yes',
            'No'  => 'No'
        );

        $pinRefArray = array();
        $pinInformation = PinHelper::getContactPinPairs(
            $this->user->getAttribute('contact_ref'),
            strtoupper($this->user->getAttribute('user_type'))
        );
        foreach ($pinInformation as $pinRef) {
            $pinRefArray[$pinRef['chair_pin_ref']] = $pinRef['chair_pin_ref'];
        }
        $pin_ref = $pinRefArray;

        return array(
            'voice_prompt_language'     => $voice_prompt_language,
            'on_hold_music'             => $on_hold_music,
            'entry_exit_announcements'  => $entry_exit_announcements,
            'announcement_types'        => $announcement_types,
            'play_participant_count'    => $play_participant_count,
            'chairman_present'          => $chairman_present,
            'chairperson_only'          => $chairperson_only,
            'pin_ref'                   => $pin_ref
        );
    }

    /**
     * Return the Form Default Values
     *
     * @return array
     * @author Asfer Tamimi
     *
     */
    protected function getFormDefaultValues() {
        $pinInformation = PinHelper::getPinInformation(
            strtoupper($this->user->getAttribute('user_type')),
            $this->user->getAttribute('contact_ref'),
            $this->request->getGetParameter('r',array())
        );

        $result = array(
            'voice_prompt_language'     => $pinInformation['pin']['chair_language_code'],
            'on_hold_music'             => $pinInformation['pin']['chair_moh_prompt_ref'],
            'entry_exit_announcements'  => $pinInformation['pin']['chair_entry_exit_announcement'],
            'announcement_types'        => $pinInformation['pin']['chair_entry_exit_announcement_type'],
            'play_participant_count'    => $pinInformation['pin']['chair_play_participant_count'],
            'pin_ref'                   => $pinInformation['pin']['chair_pin_ref'],
        );

        if (!$this->user->hasCredential('POWWOWNOW')) {
            $result['chairman_present'] = $pinInformation['pin']['chair_chairman_present'];
            $result['description']      = $pinInformation['pin']['chair_description'];
            $result['chairperson_only'] = $pinInformation['pin']['chair_chairman_only'];
            $result['cost_code']        = $pinInformation['pin']['chair_username'];
        } else {
            $result['chairman_present'] = '';
            $result['description']      = '';
            $result['chairperson_only'] = '';
            $result['cost_code']        = '';
        }

        return $result;
    }

    /**
     * Post Validation
     *
     * PIN Check
     * @param array $args
     * @return array|false
     */
    public function doPostValidation(array $args) {
        if (empty($args['pin_ref']) || !is_array($args['pin_ref'])) {
            return array('message' => 'FORM_VALIDATION_NO_PIN', 'field_name' => 'alert');
        }
        return false;
    }
}
