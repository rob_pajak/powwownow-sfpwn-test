<?php
/**
 * The Web Conferencing Get Started Landing Page Form
 *
 * @author Asfer Tamimi
 *
 */
class WebConferencingGetStartedForm extends ajaxForm
{
    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null)
    {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');

        // How Heard List Options
        $howHeardOptions = array(
            ''                          => ' -- Please Select --',
            'Radio'                     => 'Radio',
            'Train station'             => 'Train station',
            'Taxi'                      => 'Taxi',
            'Outdoor billboard'         => 'Outdoor billboard',
            'In print'                  => 'In print',
            'On the tube'               => 'On the tube',
            'Online display'            => 'Online display',
            'Google'                    => 'Google',
            'Other'                     => 'Other',
        );

        // Set Form Fields
        $this->setWidgets(
            array(
                'first_name'       => new sfWidgetFormInputText(),
                'last_name'        => new sfWidgetFormInput(),
                'company'          => new sfWidgetFormInput(),
                'email'            => new sfWidgetFormInput(),
                'confirm_email'    => new sfWidgetFormInput(),
                'password'         => new sfWidgetFormInputPassword(),
                'confirm_password' => new sfWidgetFormInputPassword(),
                'how_heard'        => new sfWidgetFormSelect(array('choices' => $howHeardOptions)),
                'tick'             => new sfWidgetFormInputHidden(array('default' => 'on')),
            )
        );

        // Form Labels
        $this->widgetSchema->setLabels(
            array(
                'first_name'       => 'First Name',
                'last_name'        => 'Surname',
                'company'          => 'Company',
                'email'            => 'Email*',
                'confirm_email'    => 'Confirm Email*',
                'password'         => 'Password*',
                'confirm_password' => 'Confirm Password*',
                'how_heard'        => 'How did you hear of Powwownow?',
                'tick'             => 'By clicking Register, you agree to the <a class="white" href="' . url_for(
                        '@terms_and_conditions'
                    ) . '" target="_blank">terms &amp; conditions</a>'
            )
        );

        // Set up Form Validation
        $validationFactory = new validationGroupFactory();

        $this->setValidators(
            array(
                'first_name'       => new sfValidatorString(array('required' => false)),
                'last_name'        => new sfValidatorString(array('required' => false)),
                'company'          => new sfValidatorString(array('required' => false)),
                'email'            => $validationFactory->email(),
                'confirm_email'    => $validationFactory->commonField(),
                'password'         => $validationFactory->password(),
                'confirm_password' => $validationFactory->commonField(),
                'how_heard'        => $validationFactory->commonField(),
                'tick'             => $validationFactory->commonField(),
            )
        );

        $this->widgetSchema->setNameFormat('webConferencing[%s]');

        // Not Sure we can do Double Post Validators
        $this->validatorSchema->setPostValidator($validationFactory->confirmPassword());
        $this->validatorSchema->setPostValidator($validationFactory->confirmEmail());
    }
}
