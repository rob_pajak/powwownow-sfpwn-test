<?php
/**
 * The Public Tell A Friend Form
 *
 * @author Asfer Tamimi
 *
 */
class TellAFriendForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements

        $this->setWidgets(
            array(
                'your_name'     => new sfWidgetFormInputText(),
                'your_email'    => new sfWidgetFormInputText(),
                'friends_name'  => new sfWidgetFormInputText(),
                'friends_email' => new sfWidgetFormInputText(),
            )
        );

        // Form Labels
        $this->widgetSchema->setLabels(
            array(
                'your_name'     => 'Your name *',
                'your_email'    => 'Your email address *',
                'friends_name'  => 'Friend\'s name *',
                'friends_email' => 'Friend\'s email address *',
            )
        );

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(
            array(
                'your_name'     => $validationFactory->yourName(),
                'your_email'    => $validationFactory->email(),
                'friends_name'  => $validationFactory->friendsName(),
                'friends_email' => $validationFactory->friendsEmail(),

            )
        );

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('tellAFriend[%s]');
    }
}
