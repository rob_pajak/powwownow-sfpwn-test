<?php
/**
 * The Recordings Form
 *
 * @author Asfer Tamimi
 *
 */
class RecordingsForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements
        $this->setWidgets(array(
            'from_date'      => new sfWidgetFormInputText(),
            'to_date'        => new sfWidgetFormInputText(),
            'description'    => new sfWidgetFormInputText(),
            'pin'            => new sfWidgetFormInputText(),
            'show_all_users' => new sfWidgetFormInputCheckbox(array('value_attribute_value' => 1)),
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'from_date'      => 'From Date *',
            'to_date'        => 'To Date *',
            'description'    => 'Description',
            'pin'            => 'PIN',
            'show_all_users' => 'Show All Users',
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        if ($this->user->hasCredential('admin')) {
            $this->setValidators(array(
                'from_date'      => $validationFactory->fromDate(),
                'to_date'        => $validationFactory->toDate(),
                'description'    => $validationFactory->genericField(),
                'pin'            => $validationFactory->genericField(),
                'show_all_users' => $validationFactory->genericField(),
            ));
        }

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('recordings[%s]');  
    }

    /**
     * Post Validation
     *
     * Standard Symfony Post Validation does not work, for an ARRAY being passed back, so manually doing a Post Validation
     *
     */
    public function doPostValidation(Array $args) {
        // Year range
        $years = array(
            date('Y'), 
            date('Y',mktime(0,0,0,1,1,date('Y')-1))
       );

        // Check Individual Date Components
        $todate = explode('-',$args['to_date']);
        if ($todate[2] == '00' || $todate[2] > 31) {
            return array('message' => 'FORM_VALIDATION_INVALID_TO_DATE_DAY', 'field_name' => 'recordings[to_date]');
        } elseif ($todate[1] == '00' || $todate[1] > 12) {
            return array('message' => 'FORM_VALIDATION_INVALID_TO_DATE_MONTH', 'field_name' => 'recordings[to_date]');
        } elseif (!in_array($todate[0],$years)) {
            return array('message' => 'FORM_VALIDATION_INVALID_TO_DATE_YEAR', 'field_name' => 'recordings[to_date]');
        } else {
            // Check Combined Date
            if (!is_numeric($todate[0]) || !is_numeric($todate[1]) || !is_numeric($todate[2])) {
                return array('message' => 'FORM_VALIDATION_INVALID_TO_DATE', 'field_name' => 'recordings[to_date]');
            } elseif (!checkdate($todate[1], $todate[2], $todate[0])) {
                return array('message' => 'FORM_VALIDATION_INVALID_TO_DATE', 'field_name' => 'recordings[to_date]');
            }
        }

        $fromdate = explode('-',$args['from_date']);
        if ($fromdate[2] == '00' || $fromdate[2] > 31 && empty($errorMessages)) {
            return array('message' => 'FORM_VALIDATION_INVALID_FROM_DATE_DAY', 'field_name' => 'recordings[from_date]');
        } elseif ($fromdate[1] == '00' || $fromdate[1] > 12 && empty($errorMessages)) {
            return array('message' => 'FORM_VALIDATION_INVALID_FROM_DATE_MONTH', 'field_name' => 'recordings[from_date]');
        } elseif (!in_array($fromdate[0],$years) && empty($errorMessages)) {
            return array('message' => 'FORM_VALIDATION_INVALID_FROM_DATE_YEAR', 'field_name' => 'recordings[from_date]');
        } elseif (empty($errorMessages)) {
            // Check Combined Date
            if (!is_numeric($fromdate[0]) || !is_numeric($fromdate[1]) || !is_numeric($fromdate[2])) {
                return array('message' => 'FORM_VALIDATION_INVALID_FROM_DATE', 'field_name' => 'recordings[from_date]');
            } elseif (!checkdate($fromdate[1], $fromdate[2], $fromdate[0])) {
                return array('message' => 'FORM_VALIDATION_INVALID_FROM_DATE', 'field_name' => 'recordings[from_date]');
            }
        }

        // Check both dates together
        if (empty($errorMessages)) {
            $fromdate = strtotime($args['from_date']);
            $todate   = strtotime($args['to_date']);
            if ($fromdate >= $todate) {
                return array('message' => 'FORM_VALIDATION_INVALID_DATE_RANGE', 'field_name' => 'recordings[from_date]');
            }
        }

        return false;
    }  

}
