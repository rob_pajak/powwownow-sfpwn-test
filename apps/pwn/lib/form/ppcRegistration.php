<?php

class ppcRegistration extends ajaxForm {

    public function configureForm() {

        $this->setWidgets(array(
            'email'    => new sfWidgetFormInputText()
        ));

        $validationFactory = new validationGroupFactory();

        $this->setValidators(array(
            'email'         => $validationFactory->email(),
        ));
    }

    public function save() {

    }
}