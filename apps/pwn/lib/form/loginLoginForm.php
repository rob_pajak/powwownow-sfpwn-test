<?php
/**
 * Login - Login Form
 * 
 * @author Asfer
 *
 */
class LoginLoginForm extends ajaxForm {

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm() {
        // Form Elements
        $this->setWidgets(array(
            'email'    => new sfWidgetFormInputText(),
            'password' => new sfWidgetFormInputPassword(),
            'redirect' => new sfWidgetFormInputHidden(),
            'remember' => new sfWidgetFormInputCheckbox()
        ));

        // Form Labels
        $this->widgetSchema->setLabels(array(
            'email'    => 'Email *',
            'password' => 'Password *',
            'redirect' => 'Redirect',
            'remember' => 'Remember me'
        ));

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(array(
            'email'    => $validationFactory->email(),
            'password' => $validationFactory->password(),
            'redirect' => new sfValidatorString(array('required' => false, 'trim' => true)),
            'remember' => new sfValidatorString(array('required' => false, 'trim' => true)),
        ));

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('login[%s]');  
    }

    /**
     * Post Validation
     *
     * Password Check
     *
     */
    public function doPostValidation(Array $args) {
        $passwordCheck = Common::checkifInvalidPassword($args['password']);
        if (false!==$passwordCheck) {
            if ($passwordCheck == 'FORM_VALIDATION_NEW_PASSWORD_EMPTY') {
                $passwordCheck = 'FORM_VALIDATION_NO_PASSWORD';
            }
            return array('message' => $passwordCheck, 'field_name' => 'password');
        }

        return false;
    }
}