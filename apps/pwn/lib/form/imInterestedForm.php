<?php

/**
 * Call Back Form
 *
 * @author Alexander Farrow
 * @modified Asfer Tamimi
 *
 */
class callBackForm extends ajaxForm
{
    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null)
    {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    public function configureForm()
    {
        // Form Elements
        $this->setWidgets(
            array(
                'first_name'     => new sfWidgetFormInputText(),
                'last_name'      => new sfWidgetFormInputText(),
                'contact_number' => new sfWidgetFormInputText(),
                'email'          => new sfWidgetFormInputText(),
                'comments'       => new sfWidgetFormTextarea(),
                'utm_campaign'   => new sfWidgetFormInputHidden(),
            )
        );

        // Form Labels
        $this->widgetSchema->setLabels(
            array(
                'first_name'     => 'First Name *',
                'last_name'      => 'Last Name',
                'contact_number' => 'Contact Number',
                'email'          => 'Email *',
                'comments'       => 'Please let us know when to call you back:',
            )
        );

        // Form Validation Class
        $validationFactory = new validationGroupFactory();

        // Form Validation
        $this->setValidators(
            array(
                'first_name'     => $validationFactory->firstName(),
                'last_name'      => $validationFactory->lastName(false),
                'contact_number' => $validationFactory->phoneNumber(false),
                'email'          => $validationFactory->email(),
                'comments'       => $validationFactory->comments(false),
                'utm_campaign'   => $validationFactory->genericField(),
            )
        );

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat('contactus[%s]');
    }
}