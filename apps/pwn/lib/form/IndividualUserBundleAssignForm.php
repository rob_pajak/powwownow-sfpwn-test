<?php

class IndividualUserBundleAssignForm extends ajaxForm
{
    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null)
    {
        parent::__construct($defaults, $options, $CSRFSecret);
    }

    /**
     * {@inheritDoc}
     */
    public function configureForm()
    {
        $this->setWidget('user', new sfWidgetFormSelect(array('choices' => $this->getUserChoices())));
        $this->widgetSchema->setLabel('user', 'Select PIN pair to assign to product:');
    }

    protected function prepareContactPinForChoice(&$contactPin)
    {
        $contactPin = sprintf(
            "%s %s (%s/%s)",
            $contactPin['first_name'],
            $contactPin['last_name'],
            $contactPin['chairman_pin'],
            $contactPin['participant_pin']
        );
    }

    private function getUserChoices()
    {
        $contactPinMap = plusCommon::retrieveContactPinMap(sfContext::getInstance()->getUser()->getAttribute('contact_ref'));
        array_walk($contactPinMap, array($this, 'prepareContactPinForChoice'));
        return $contactPinMap;
    }
}
