<?php
/**
 * The Web Conferencing Sign up Form
 *
 * @author Wiseman
 *
 */
class WebConferencingSignupForm extends ajaxForm
{
    public function configureForm()
    {

        $howHeaderOptions = array(
            'Please select...'         => 'Please select...',
            'Attended a Powwownow'     => 'Attended a Powwownow',
            'Bing search'              => 'Bing search',
            'Colleague recommendation' => 'Colleague recommendation',
            'Email'                    => 'Email',
            'Facebook'                 => 'Facebook',
            'Google search'            => 'Google search',
            'iPhone App'               => 'iPhone App',
            'Linkedin'                 => 'Linkedin',
            'Love/Hate Travel?'        => 'Love/Hate Travel?',
            'Powwownow Blog'           => 'Powwownow Blog',
            'Telemarketing Call'       => 'Telemarketing Call',
            'Twitter'                  => 'Twitter',
            'Word of mouth'            => 'Word of mouth',
            'Yahoo search'             => 'Yahoo search',
            'YouTube'                  => 'YouTube',
            'Other web search'         => 'Other web search',
            'Other'                    => 'Other');

        $this->setWidgets(array(
            'first_name'        => new sfWidgetFormInputText(),
            'last_name'         => new sfWidgetFormInput(),
            'email'             => new sfWidgetFormInput(),
            'password'          => new sfWidgetFormInputPassword(),
            'confirm_password'  => new sfWidgetFormInputPassword(),
            'how_heard'         => new sfWidgetFormSelect(array('choices' => $howHeaderOptions))
        ));

        $validationFactory = new validationGroupFactory();

        $this->setValidators(array(
            'first_name'        => $validationFactory->firstName(),
            'last_name'         => $validationFactory->lastName(),
            'email'             => $validationFactory->email(),
            'password'          => $validationFactory->password(),
            'confirm_password'  => $validationFactory->commonField(),
            'how_heard'         => $validationFactory->commonField(),
        ));

        $this->widgetSchema->setNameFormat('webConferencingSignup[%s]');

        $this->validatorSchema->setPostValidator($validationFactory->confirmPassword());

    }

    /**
     * Calls Hermes to 'persist' the form
     *
     * @param $locale
     * @param $source
     * @param $goal Goal name to pass to referrer tracking
     * @return API response of doWebConferencingRegistration
     */
    public function save($locale, $source, $goal = 'conferencing')
    {
        try {
            $apiResponse = Hermes_Client_Rest::call(
                'doWebConferencingRegistration',
                array(
                    'first_name'   => $this->getValue('first_name'),
                    'last_name'    => $this->getValue('last_name'),
                    'email'        => $this->getValue('email'),
                    'password'     => $this->getValue('password'),
                    'how_heard'    => $this->getValue('how_heard'),
                    'phone'        => '',
                    'organisation' => '',
                    'source'       => $source,
                    'locale'       => $locale
                )
            );
        } catch (Hermes_Client_Exception $e) {
            $errorArr = null;

            if ($e->getResponseBody()) {
                // If hermes returned a specific error extract it to a more friendly error
                switch ($e->getCode()) {
                    case '001:000:001': // EMAIL_TAKEN
                        $errorArr = array('message' => 'API_RESPONSE_EMAIL_ALREADY_TAKEN', 'field_name' => $this->getName() . '[existing_password]');
                        break;
                    case '001:000:007': // EMAIL_TAKEN_BY_INACTIVE_ACCOUNT
                        $errorArr = array('message' => 'FORM_CREATE_USER_EMAIL_TAKEN_BY_INACTIVE_ACCOUNT', 'field_name' => $this->getName() . '[existing_password]');
                        break;
                }
            }

            // Rethrow exception as a FormException with relevant error message array
            throw new AjaxFormException('Hermes returned an exception', 0, $e, $errorArr);
        } catch (Exception $e) {
            throw new AjaxFormException('Hermes returned an exception', 0, $e);
        }

        try {
            try{
                PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                PWN_Logger::log('pin:'.$apiResponse['pin']['pin_ref'].' goal: '.$goal.' '.__FILE__ . '['.__LINE__.']', PWN_Logger::INFO, 'trackerLog');
            } catch(Exception $e) {

            }
            PWN_Tracking_GoalReferrers::achieved($goal, $apiResponse['pin']['pin_ref'], true);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Could not track goal referrers: ' . serialize($e));
        }

        return $apiResponse;
    }
}
