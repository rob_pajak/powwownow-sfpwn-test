<?php
/**
 * Update password form
 */
class updatePasswordForm extends ajaxForm
{
    public function configureForm()
    {
        $this->setWidgets(array(
            'email'             => new sfWidgetFormInputText(),
            'existing_password' => new sfWidgetFormInputPassword(),
            'password'          => new sfWidgetFormInputPassword(),
            'confirm_password'  => new sfWidgetFormInputPassword(),
        ));

        $validationFactory = new validationGroupFactory();

        $this->setValidators(array(
            'email'             => $validationFactory->email(),
            'existing_password' => $validationFactory->password(),
            'password'          => $validationFactory->password(),
            'confirm_password'  => $validationFactory->password()
        ));

        $this->widgetSchema->setNameFormat('updatePasswordForm[%s]');

        $this->validatorSchema->setPostValidator($validationFactory->confirmPassword());

    }

    /**
     * Calls Hermes to 'persist' the form
     *
     * @param array $additionalArgs Additional arguments required to persist the data. If a value already exists in the form
     * then they can be over written here
     */
    public function save()
    {
        try {
            Hermes_Client_Rest::call(
                'doUpdatePassword',
                array(
                    'email'             => $this->getValue('email'),
                    'existing_password' => $this->getValue('existing_password'),
                    'new_password'      => $this->getValue('password')
                )
            );
        } catch (Hermes_Client_Exception $e) {
            $errorArr = null;

            if ($e->getResponseBody()) {
                // If hermes returned a specific error extract it to a more friendly error
                switch ($e->getCode()) {
                    case '000:011:001': // AUTHENTICATION_FAILURE
                        $errorArr = array('message' => 'FORM_VALIDATION_WRONG_PASSWORD', 'field_name' => 'updatePasswordForm[existing_password]');
                        break;
                }
            }

            // Rethrow exception as a FormException with relevant error message array
            throw new AjaxFormException('Hermes returned an exception', 0, $e, $errorArr);
        } catch (Exception $e) {
            throw new AjaxFormException('Hermes returned an exception', 0, $e);
        }

        return true;
    }
}
