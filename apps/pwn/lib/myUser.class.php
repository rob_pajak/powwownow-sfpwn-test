<?php

class myUser extends sfBasicSecurityUser
{
    /**
     * @var Balance
     */
    private $balance;

    /**
     * @var bool
     */
    private $hasUsers;

    /**
     * @param sfEventDispatcher $dispatcher
     * @param sfStorage $storage
     * @param array $options
     * @return bool|void
     */
    public function initialize(sfEventDispatcher $dispatcher, sfStorage $storage, $options = array())
    {
        /**
         * The "Keep Me Signed In" requirements are odd for this project, which require different timeouts
         * compared to traditional "Keep Me Signed In".
         *
         * Here We override the sfBasicSecurityUser initialize method and change the Symfony session NOT PHP session
         * timeout value.
         *
         * Rule are:
         *  - If user clicks "Keep Me Signed In" - logout after 30 days
         *  - If user does not click "Keep Me Signed In" - logout after 15 minutes
         */
        $timeOut = 2592000000; // 30 Days

        if (sfContext::getInstance()->getRequest()->getCookie('KEEP_ME_SIGNED_IN') == 'false') {
            $timeOut = 900000; // 15 minutes
        }

        $options['timeout'] = $timeOut;
        parent::initialize($dispatcher, $storage, $options);

    }

    /**
     * Checks if the user is a Post-Pay customer.
     *
     * @param bool $reset
     * @return bool
     *
     * @author Maarten Jacobs
     */
    public function isPostPayCustomer($reset = false)
    {
        return plusCommon::retrievePostPayStatus($this->getAccountId(), $reset);
    }

    /**
     * Retrieves the account id of the user. If not found, returns false.
     *
     * @return int|bool
     *
     * @author Maarten Jacobs
     */
    public function getAccountId()
    {
        return $this->getAttribute('account_id', false);
    }

    /**
     * Retrieves the contact ref of the user. If not found, returns false.
     *
     * @return int|bool
     *
     * @author Maarten Jacobs
     */
    public function getContactRef()
    {
        return $this->getAttribute('contact_ref', false);
    }

    /**
     * Retrieves the balance of the user, wrapped in a Balance object.
     *
     * @param bool $reset
     * @return Balance
     */
    public function getBalance($reset = false)
    {
        if (!isset($this->balance) || $reset) {
            $this->cacheBalanceAndUsers($reset);
        }
        return $this->balance;
    }

    /**
     * Retrieves the raw (float) credit balance of the Plus Admin.
     *
     * @param bool $reset
     * @return float
     */
    public function getRawBalance($reset = false)
    {
        $balance = $this->getBalance($reset);
        return $balance->raw();
    }

    /**
     * States whether the user manages users.
     *
     * @param bool $reset
     * @return bool
     */
    public function hasUsers($reset = false)
    {
        if (!isset($this->hasUsers) || $reset) {
            $this->cacheBalanceAndUsers($reset);
        }
        return $this->hasUsers;
    }

    /**
     * Retrieves the current account bundle linked to the account.
     *
     * @param bool $reset
     * @return array
     */
    public function getCurrentAccountBundle($reset = false)
    {
        return plusCommon::retrieveAccountBundle($this->getAccountId(), null, null, $reset);
    }

    /**
     * @param bool $reset
     */
    private function cacheBalanceAndUsers($reset = false)
    {
        $balanceAndUsers = plusCommon::getAccountBalanceContainerAndUsersWithReset(
            $this->getAccountId(),
            $this->getContactRef(),
            $reset
        );
        $this->hasUsers = $balanceAndUsers['accountUsers'] === 'M';
        $this->balance = $balanceAndUsers['balance'];
    }

    /**
     * Returns the Application Environment. Possible Values are: 'dev', 'sqi', 'staging', 'prod'|'live'
     * @return bool|string
     */
    public function getApplicationEnvironment()
    {
        return (isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : false);
    }
}
