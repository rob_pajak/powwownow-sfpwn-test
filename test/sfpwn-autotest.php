#!/usr/bin/php -c/etc/pwnscripts/php.ini
<?php
$emailList = 'webteam@powwownow.com';
// $emailList = 'marcin.kamycki@powwownow.com';

$body = array();

$lasttested = @file_get_contents('sfpwn-autotest.last');

exec('hg parents', $resLog, $retLog);
preg_match('/changeset:[^0-9]*(.*)/', $resLog[0], $m);
$changeset = $m[1];

if ($lasttested && trim($lasttested) == $changeset) {
    die("Already tested according to sfpwn-autotest.last lock file.\n");
}

exec('hg branch', $branchResLog, $branchRetLog);

exec('hostname -f', $hostLog, $hostRetLog);

echo $body[] = date('Y-m-d H:i');
$body[] = $changeset;
$body[] = "\n**********************************";
$body[] = "Hostname: {$hostLog[0]}";
$body[] = "Autotest is using branch: {$branchResLog[0]}";
$body[] = "Previously tested Rev: {$lasttested}";
$body[] = "Most recent revision: {$changeset}";
$body[] = "**********************************";
$body[] = "\nChanges since last test:\n";

$latestTestedRevision = explode(':', $lasttested);

exec("hg log --follow --template '[{date|isodate}]: {rev}:{node|short} {author} {desc|escape}\n\n' --rev {$branchResLog[0]}:{$latestTestedRevision[1]}", $changesLog, $changesResLog);

foreach ($changesLog as $k => $row)
    if (trim($row) != '')
        $body[] = trim($row);

//exec('cd .. && symfony cc && cd test', $ccResLog, $ccRetLog);
exec('cd .. && symfony cc && sudo /etc/init.d/apache2 restart && cd test', $ccResLog, $ccRetLog);

$body[] = "\nClear cache:\n";
foreach ($ccResLog as $k => $line) {
    $body[] = trim($line);
};

exec('cd .. && symfony test:functional pwn && cd test', $ftLog, $ftRetLog);

exec('cd .. && symfony phpunit:unit', $unitTestLog);
array_unshift($unitTestLog, "\nUnit tests:\n");

$subject = $hostLog[0] . ' sfpwn autotest: ' . ($ftLog[count($ftLog) - 2] == ' All tests successful.' ? 'pass' : 'fail');

$body[] = "\nFunctional tests:\n";

$body = array_merge($body, $ftLog, $unitTestLog);

$body = escapeshellarg(implode("\n", $body));

echo $body;

`echo $body | mail -s "$subject" $emailList`;

file_put_contents('sfpwn-autotest.last', "$changeset\n");
