<?php

array(
    'email' => (isset($email) ? $email : 'test') . '+' . date('ymdHms'). '_'.rand(1000,9999) . '@powwownow.com',
    'title' => 'Mr',
    'first_name' => 'functional',
    'last_name' => 'tester',
    'phone' => '0000000000',
    'mobile_phone' => '0700000000',
    'organisation' => 'testing inc.',
    'location' => 'UK',
    'password' => 'r1chm0nd',
    'service_ref' => '850',
    'source' => 'functional Test',
    'locale' => 'en_GB'
);
