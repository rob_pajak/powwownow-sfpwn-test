<?php
/**
 * This is the Bootstrap for the PHPUnit Unit Testing for the Symfony 1.4 Projects
 *
 * Hopefully the First thing to be tested will be the CreateUser Task, after a dummy getVersion Test is done, to check
 * the Whole Hermes Client Rest with some Logging
 *
 * @author Asfer
 */

// Project Configuration Paths
$testDir    = realpath(dirname(__FILE__) . '/..');
$projectDir = realpath(dirname(__FILE__) . '/../../');

// Load Configuration
require_once $projectDir . '/config/ProjectConfiguration.class.php';
$configuration = ProjectConfiguration::hasActive() ? ProjectConfiguration::getActive() : new ProjectConfiguration($projectDir);

// Load the Auto loader
$autoload = sfSimpleAutoload::getInstance(sfConfig::get('sf_cache_dir').'/project_autoload.cache');
$autoload->loadConfiguration(sfFinder::type('file')->name('autoload.yml')->in(array(
    sfConfig::get('sf_symfony_lib_dir').'/config/config',
    sfConfig::get('sf_config_dir'),
)));
$autoload->register();
