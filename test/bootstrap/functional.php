<?php
// guess current application
if (!isset($app)) {
    $traces = debug_backtrace();
    $caller = $traces[0];

    $dirPieces = explode(DIRECTORY_SEPARATOR, dirname($caller['file']));
    $app       = array_pop($dirPieces);
}

require_once dirname(__FILE__) . '/../../config/ProjectConfiguration.class.php';
$configuration = ProjectConfiguration::getApplicationConfiguration($app, 'test', isset($debug) ? $debug : true);
sfContext::createInstance($configuration);
Hermes_Client_Rest::init(sfConfig::get('app_hermesurl', 'hermes.via-vox.net/rest.php'));

// Include functional test helpers.
$testRoot = dirname(__FILE__);
require_once $testRoot . '/../helper/testingHelper.php';
require_once $testRoot . '/../helper/genericHelper.php';
require_once $testRoot . '/../helper/bwmConfig.php';
require_once $testRoot . '/../helper/basketHelper.php';
require_once $testRoot . '/../helper/authenticateHelper.php';
require_once $testRoot . '/../helper/accountDetailsHelper.php';
require_once $testRoot . '/../helper/plusSignupHelper.php';
require_once $testRoot . '/../helper/enhancedSignupHelper.php';
require_once $testRoot . '/../helper/bundleHelper.php';
require_once $testRoot . '/../helper/ConferenceCallGenerator.php';
require_once $testRoot . '/../helper/freePinRegistrationHelper.php';

// Setting default remote address to stop IP-retrieving methods from complaining.
$_SERVER['REMOTE_ADDR'] = 'localhost';

// remove all cache
sfToolkit::clearDirectory(sfConfig::get('sf_app_cache_dir'));

$b = new sfPwnPlusFunctionalTest(new sfBrowser());