<?php
/**
 * Make a POST Ajax Request
 * It creates the correct Headers and Sends a Post Request to a Given Route along with Parameters
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param string $route
 * @param array $args
 */
function makePostAJAXRequest(sfPwnPlusFunctionalTest $b, $route, array $args)
{
    $b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')->post($route, $args);
}

/**
 * Test a Post Ajax Request For a Status Code.
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param integer $code
 */
function testPostAjaxRequestForStatusCode(sfPwnPlusFunctionalTest $b, $code)
{
    $b->with('response')->isStatusCode($code);
}

/**
 * Test a Response Message against the User Given Message
 * If the Debug Parameter is set to True, the function will also return detailed Response Information
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param $userMessage
 * @param bool $debug
 * @return string
 */
function testPostAjaxRequestForResponseMessage(sfPwnPlusFunctionalTest $b, $userMessage, $debug = false)
{
    // Find the Response Message
    $response = json_decode($b->getResponse()->getcontent(), true);
    if (isset($response['error_messages'][0]['message'])) {
        $responseMessage = $response['error_messages'][0]['message'];
    } elseif (isset($response['error_messages']['message'])) {
        $responseMessage = $response['error_messages']['message'];
    } else {
        $responseMessage = null;
    }

    // Create the Debug Response
    $debugResponse = '';
    if ($debug) {
        $debugResponse .= "Full Response:\n" . var_export($response) . "\n";
        $debugResponse .= "Response Message Extracted:\n" . print_r($responseMessage,true) . "\n";
        $debugResponse .= "Error Message Given:\n" . print_r($userMessage,true) . "\n";
    }

    // Test the User Given Error Message to the Response Message
    if ($userMessage) {
        $b->test()->ok(
            $responseMessage,
            $userMessage,
            'Error Message Did not Match. ' . $responseMessage . ' :::' . $userMessage
        );
    }
    return $debugResponse;
}

/**
 * Test a Post Ajax Request for an Error or an Success
 * If the Debug Parameter is set to True, the function will also return detailed Response Information
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param $route
 * @param $statusCode
 * @param array $args
 * @param bool $debug
 * @param bool $userMessage
 * @return string
 */
function testPostAjaxRequest(
    sfPwnPlusFunctionalTest $b,
    $route,
    $statusCode,
    array $args,
    $debug = false,
    $userMessage = false
) {
    // Setup a Ajax Post
    makePostAJAXRequest($b, $route, $args);

    // Do a Status Code Test
    testPostAjaxRequestForStatusCode($b, $statusCode);

    // Response Message Check
    $debugResponse = testPostAjaxRequestForResponseMessage($b, $userMessage, $debug);

    return $debugResponse;
}

