<?php
/**
 * Assert that the current user can modify the contact linked to user.
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param $info
 * @param $prefix
 *   Prefix for the test values to make debugging easier.
 *   Note that you should use short prefixes (preferably <= 10), because the validation is done on maximum length.
 * @param array $additionalDetails
 */
function testContactDetailsForm(sfPwnPlusFunctionalTest $b, $info, $prefix, array $additionalDetails = array())
{
    if (isset($additionalDetails['contact_ref'])) {
        $contactRef = $additionalDetails['contact_ref'];
        unset($additionalDetails['contact_ref']);
    } else {
        $contactRef = false;
    }
    clearContactCache($contactRef);

    // Keep a list of test values.
    $contactDetails = array(
        'contact' => $additionalDetails + array(
                'title'           => 'Testir',
                'first_name'      => $prefix . 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name'       => $prefix . 'TestMyLastName',
                'business_phone'  => '12345678',
                'mobile_phone'    => '01234567',
                'country'         => 'ALB',
                'company'         => $prefix . 'Testers United',
            )
    );

    $b->info($info);
    testPostAjaxRequest($b, '/myPwn/Account-Details/submit/contact', 200, $contactDetails, false, false);

    // Assert that all details match.
    assertContactDetailsMatch($b, $contactDetails, array('contact_ref' => $contactRef));
}

/**
 * Assert that validation prevents the contact details form from submitting for all allowed user roles.
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param string $info
 * @param string $prefix
 * @param array $additionalDetails
 */
function testContactDetailsFormFailure(sfPwnPlusFunctionalTest $b, $info, $prefix, array $additionalDetails = array()) {
    if (isset($additionalDetails['contact_ref'])) {
        $contactRef = $additionalDetails['contact_ref'];
        unset($additionalDetails['contact_ref']);
    } else {
        $contactRef = false;
    }

    clearContactCache($contactRef);

    $b->info('Testing invalid contact details form submission as ' . $prefix . '.');

    // 1. Change the contact details to reference details.

    // Keep a list of correct test values (within the bounds of validation).
    $validDetails = array(
        'contact' => $additionalDetails + array(
            'title' => 'Testir',
            'first_name' => $prefix . 'TestMyFirstName',
            'middle_initials' => 'TT',
            'last_name' => $prefix . 'TestMyLastName',
            'business_phone' =>  '12345678',
            'mobile_phone' => '01234567',
            'country' => 'ALB',
            'company' => $prefix . 'Testers United',
        )
    );

    // Change all values of elements on the contact details form that can be changed.
    testPostAjaxRequest($b, '/myPwn/Account-Details/submit/contact', 200, $validDetails, false, false);

    // 2. Change the test values per field to an invalid field and test that the details remain unchanged.

    $invalidDetailLists = array(
        // Title length greater than 10.
        'INVALID_CONTACT_TITLE' => array(
            'contact' => $additionalDetails + array(
                'title' => '01234567891',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name' => 'TestMyLastName',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // First name is empty.
        'FORM_VALIDATION_NO_FIRST_NAME' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => '',
                'middle_initials' => 'TT',
                'last_name' => 'TestMyLastName',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // First name length is over 30.
        'FORM_VALIDATION_INVALID_CONTACT_FIRST_NAME' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'aaaaabbbbbcccccdddddeeeeefffffg',
                'middle_initials' => 'TT',
                'last_name' => 'TestMyLastName',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // Middle initials length is over 10.
        'FORM_VALIDATION_INVALID_MIDDLE_INITIALS' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'aaaaabbbbbc',
                'last_name' => 'TestMyLastName',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // Last name is empty.
        'FORM_VALIDATION_NO_SURNAME' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name' => '',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // Last name length is over 30.
        'FORM_VALIDATION_INVALID_CONTACT_LAST_NAME' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name' => 'aaaaabbbbbcccccdddddeeeeefffffg',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // Business phone length is over 20.
        'FORM_VALIDATION_INVALID_PHONE_NUMBER' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name' => 'TestMyLastName',
                'business_phone' => '111112222233333444445',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // Mobile phone length is over 20.
        'INVALID_MOBILE_NUMBER' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name' => 'TestMyLastName',
                'business_phone' => '12345678',
                'mobile_phone' => '111112222233333444446',
                'country' => 'ALB',
                'company' => 'Testers United',
            )
        ),
        // Country code is not in list of countries.
        'FORM_VALIDATION_INVALID_CONTACT_COUNTRY' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name' => 'TestMyLastName',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ZZZZ',
                'company' => 'Testers United',
            )
        ),
        // Company length is over 50.
        'FORM_VALIDATION_NO_COMPANY_NAME' => array(
            'contact' => $additionalDetails + array(
                'title' => 'Testir',
                'first_name' => 'TestMyFirstName',
                'middle_initials' => 'TT',
                'last_name' => 'TestMyLastName',
                'business_phone' => '12345678',
                'mobile_phone' => '01234567',
                'country' => 'ALB',
                'company' => 'aaaaabbbbbcccccdddddeeeeefffffaaaaabbbbbcccccddddde',
            )
        ),
    );

    foreach ($invalidDetailLists as $errorMessage => $details) {
        // Prefix all values prefixable values; this makes debugging easier.
        foreach (array('first_name', 'last_name', 'company') as $optionalParam) {
            if (!empty($details['contact'][$optionalParam])) {
                $details['contact'][$optionalParam] = $prefix . $details['contact'][$optionalParam];
            }
        }

        // Try to submit the form with invalid details.
        $b->info($info);
        testPostAjaxRequest($b, '/myPwn/Account-Details/submit/contact', 500, $details, false, $errorMessage);

        // Test that the details remain unchanged.
        assertContactDetailsMatch($b, $validDetails);
    }
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @param array $contactDetails
 * @param array $additionalDetails
 */
function assertContactDetailsMatch(sfPwnPlusFunctionalTest $b, array $contactDetails, array $additionalDetails = array()) {
    if (isset($additionalDetails['contact_ref'])) {
        $contactRef = $additionalDetails['contact_ref'];
    } else {
        $contactRef = false;
    }
    clearContactCache($contactRef);

    // Assert that all values match.
    assertDetailsMatch($b, $contactDetails['contact']);
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @param array $organisationDetails
 * @param array $additionalDetails
 */
function assertOrganisationDetailsMatch(sfPwnPlusFunctionalTest $b, array $organisationDetails, array $additionalDetails = array()) {
    if (isset($additionalDetails['account_id'])) {
        $account_id = $additionalDetails['account_id'];
    } else {
        $account_id = false;
    }
    clearPlusAccountCache($account_id);

    // Assert that all values match.
    assertDetailsMatch($b, $organisationDetails['organisation']);
}

function assertDetailsMatch(sfPwnPlusFunctionalTest $b, array $details) {
    // Assert that all values match.
    $matcher = $b->get('/s/mypwn/accountDetails')->with('response');
    foreach ($details as $contactValue) {
        $contactValue = preg_quote($contactValue);
        $matcher->matches("/$contactValue/");
    }
}

/**
 * @param bool $contact_ref
 */
function clearContactCache($contact_ref = false)
{
    if (!$contact_ref && isset($_SESSION['contact_ref'])) {
        $contact_ref = $_SESSION['contact_ref'];
    }

    Common::getContactByContactRef($contact_ref, true);
}

/**
 * @param bool $account_id
 */
function clearPlusAccountCache($account_id = false)
{
    if ($account_id || isset($_SESSION['account_id'])) {
        plusCommon::getPlusAccount(
            ($account_id) ? $account_id : $_SESSION['account_id'],
            true
        );
    }
}

/**
 * Assert that the password can be changed for a user.
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param $email
 * @param $currentPassword
 * @param $info
 * @internal param array $credentials
 */
function testChangePassword(sfPwnPlusFunctionalTest $b, $email, $currentPassword, $info) {
    // Login as the user.
    loginUser($b, $email, $currentPassword);

    // Change the password for the current user.
    $testPassword = 'test_password';
    $formArguments = array(
        'authentication' => array(
            'password' => $testPassword,
            'confirm_password' => $testPassword,
        ),
    );
    $b->info($info)
        ->get('/s/mypwn/accountDetails')
            ->click('#frm_account_details_my_password button', $formArguments);

    // Logout and login with the new password.
    logoutUser($b);
    loginUser($b, $email, $testPassword);

    // Change the password back for the current user.
    $formArguments = array(
        'authentication' => array(
            'password' => $currentPassword,
            'confirm_password' => $currentPassword,
        ),
    );
    $b->info('Reverting the password for ' . $email)
        ->get('/s/mypwn/accountDetails')
            ->click('#frm_account_details_my_password button', $formArguments);
}

/**
 * Assert that the password remains unchanged after posting invalid password parameters.
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param string $email
 * @param string $currentPassword
 * @param string $prefix
 * @param string $info
 */
function testChangePasswordFailure(sfPwnPlusFunctionalTest $b, $email, $currentPassword, $prefix, $info) {
    // Login as the user.
    loginUser($b, $email, $currentPassword);

    // Keep a list of invalid passwords.
    $invalidParameters = array(
        // Empty passwords.
        array(
            'password' => '',
            'confirm_password' => '',
        ),
        // Empty password.
        array(
            'password' => '',
            'confirm_password' => $prefix . 'not-empty',
        ),
        // Empty confirm.
        array(
            'password' => $prefix . 'not-empty',
            'confirm_password' => '',
        ),
        // Non-matching passwords.
        array(
            'password' => $prefix . 'not',
            'confirm_password' => $prefix . 'matching',
        ),
        // Password length over maximum length.
        array(
            'password' => $prefix . 'abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde',
            'confirm_password' => $prefix . 'not-empty',
        ),
        // Confirm password length over maximum length.
        array(
            'password' => 'not-empty',
            'confirm_password' => 'abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde',
        ),
    );

    foreach ($invalidParameters as $formArguments) {
        // Try to submit the password form with invalid parameters.
        $b->info($info)
            ->get('/s/mypwn/accountDetails')
                ->click('#frm_account_details_my_password button', $formArguments);

        // Verify that the password remains unchanged.
        logoutUser($b);
        loginUser($b, $email, $currentPassword);
    }
}

function testValidOrganisationDetails(sfPwnPlusFunctionalTest $b, $info) {
    $validParameters = array(
        'organisation' => array(
            'account_name' => 'Scorpio',
            'organisation' => 'Globex',
            'vat_number' => 'JKQKES234NW',
        ),
    );

    $b->info($info)
        ->post('/myPwn/Account-Details/submit/organisation', $validParameters);

    $content = $b->getResponse()->getContent();
    $b->test()->ok(empty($content), 'Response content is empty.');

    assertOrganisationDetailsMatch($b, $validParameters);
}

function testInvalidOrganisationDetails(sfPwnPlusFunctionalTest $b, $info) {
    // Set control values.
    $validParameters = array(
        'organisation' => array(
            'account_name' => 'CLEAN AC NAME',
            'organisation' => 'CLEAN ORG NAME',
            'vat_number' => 'CLEAN VAT NUMBER',
        ),
    );
    $b->post('/myPwn/Account-Details/submit/organisation', $validParameters);

    $invalidParameters = array(
        // Empty account name
        'FORM_VALIDATION_NO_ACCOUNT_NAME' => array(
            'organisation' => array(
                'account_name' => '',
                'organisation' => 'Globex',
                'vat_number' => 'JKQKES234NW',
            ),
        ),
        // Account name length > 60
        'FORM_VALIDATION_MAXLENGTH_ACCOUNT_NAME' => array(
            'organisation' => array(
                'account_name' => 'HankScorpioHankScorpioHankScorpioHankScorpioHankScorpioHankScorpio',
                'organisation' => 'Globex',
                'vat_number' => 'JKQKES234NW',
            ),
        ),
        // Empty organisation
        'FORM_VALIDATION_NO_ORGANISATION' => array(
            'organisation' => array(
                'account_name' => 'Scorpio',
                'organisation' => '',
                'vat_number' => 'JKQKES234NW',
            ),
        ),
        // Organisation length > 100
        'FORM_VALIDATION_MAXLENGTH_ORGANISATION' => array(
            'organisation' => array(
                'account_name' => 'Scorpio',
                'organisation' => 'GlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobexGlobex',
                'vat_number' => 'JKQKES234NW',
            ),
        ),
        // Vat number length > 20
        'FORM_VALIDATION_MAXLENGTH_VAT_NUMBER' => array(
            'organisation' => array(
                'account_name' => 'Scorpio',
                'organisation' => 'Globex',
                'vat_number' => 'JKQKES234NWWAYtOOLONGFORAVATNUMER',
            ),
        ),
    );

    foreach ($invalidParameters as $errorMessage => $details) {
        // Try to submit the form with invalid details.
        $b->info($info);
        testPostAjaxRequest($b, '/myPwn/Account-Details/submit/organisation', 500, $details, false, $errorMessage);

        // Test that the details remain unchanged.
        assertOrganisationDetailsMatch($b, $validParameters);
    }
}
