<?php

function retrieveBundleProductGroupId(sfPwnPlusFunctionalTest $b, $bundleId = '5000')
{
    $b->get('/myPwn/Products');
    /** @var DOMDocument $productsDOM */
    $productsDOM = $b->getResponseDom();
    $result = array();
    $result['bundle_id'] = $productsDOM->getElementById('add-' . $bundleId)->getAttribute('value');
    if ($bundleId !== 'individual') {
        $result['bundle_upgrade_id'] = $productsDOM->getElementById('upgrade-' . $bundleId)->getAttribute('value');
    }
    return $result;
}

function assignBundleToUser($bundleId)
{
    $accountId = sfContext::getInstance()->getUser()->getAttribute('account_id');
    Hermes_Client_Rest::call('Bundle.assignNewBundle', array('account_id' => $accountId, 'product_group_id' => $bundleId));
    plusCommon::retrievePostPayStatus($accountId, true);
}
