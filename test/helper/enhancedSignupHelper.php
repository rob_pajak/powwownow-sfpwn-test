<?php

function registerNewEnhancedUser(sfPwnPlusFunctionalTest $b)
{
    $user = array(
        'first_name'        => 'functional',
        'last_name'         => 'tester',
        'email'             => generateTestEmail(),
        'password'          => 'r1chm0nd',
        'source'            => '/Create-A-Login-Functional-Test',
        'locale'            => 'en_GB',
        'ip'                => '127.0.0.1',
    );
    Common::doCreateOrUpdateAccount($user);

    $b->info("login user {$user['email']} with password {$user['password']}")
        ->post('/s/test/login', array('user_email' => $user['email'], 'user_password' => $user['password']))
        ->with('response')->begin()
            ->isStatusCode(200)
        ->end();
}
