<?php

$emailIt = 1;

function plusSignupResponseContainsErrorMessages(sfPwnPlusFunctionalTest $b, $expectedErrors) {
    $response = json_decode($b->getResponse()->getcontent(), true);
    if (!is_array($expectedErrors)) {
        $expectedErrors = array($expectedErrors);
    }

    $messages = array();

    foreach ($response['error_messages'] as $error) {
        if (isset($error['field_name'])) {
            $messages[$error['field_name']] = $error['message'];
        } else {
            $messages[] = $error['message'];
        }
    }

    foreach ($expectedErrors as $fieldName => $errorMessage) {
        if (!is_numeric($fieldName)) {
            $b->test()->ok($messages[$fieldName] === $errorMessage, "Plus signup response contains error message $fieldName => $errorMessage.");
        } else {
            $b->test()->ok(in_array($errorMessage, $messages), "Plus signup response contains error message $errorMessage.");
        }
    }
}

function generateTestEmail() {
    global $emailIt;
    return 'valid-func-test-' . time() . $emailIt++ . '@powwownow.com';
}

function createNewPlusUser(sfPwnPlusFunctionalTest $b) {
    $userEmail = generateTestEmail();
    $routecreateUser = '/s/Create-User-Ajax';
    $args = array(
        'createUser' => array(
            'email' => $userEmail,
            'cost_code' => '',
            'title' => '',
            'first_name' => 'asd',
            'last_name' => 'asd',
            'phone_number' => '',
            'password' => 'r1chm0nd',
            'confirm_password' => 'r1chm0nd'
        )
    );
    makePostAJAXRequest($b, $routecreateUser, $args);
    return $userEmail;
}

function retrieveContactRefByEmail($email)
{
    $contact = Hermes_Client_Rest::call('getContactByMultipleParameters', array('email' => $email));
    return $contact['contact_ref'];
}
