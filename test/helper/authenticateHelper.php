<?php

/**
 * Login wit the given email and password.
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param string $email
 * @param string $password
 * @param string $message
 */
function loginUser(sfPwnPlusFunctionalTest $b, $email, $password, $message = '') {
    $b->info('login user ' . $email . ' with password ' . $password)
        ->post('/s/test/login', array('user_email' => $email, 'user_password' => $password), $message)
        ->with('response')
        ->isStatusCode(200);
}

/**
 * Logs out the current user.
 *
 * @param sfPwnPlusFunctionalTest $b
 */
function logoutUser(sfPwnPlusFunctionalTest $b) {
    $b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
        ->get('/Logout');
}
