<?php

/**
 * @param sfPwnPlusFunctionalTest $b
 * @return array
 */
function getPremiumAdminWithContactRef(sfPwnPlusFunctionalTest $b)
{
    $args = $b->args('doPremiumAdminLogin.php');
    $args['contact_ref'] = '94498';
    return $args;
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @return array
 */
function getPremiumUserWithContactRef(sfPwnPlusFunctionalTest $b)
{
    $args = $b->args('doPremiumUserLogin.php');
    $args['contact_ref'] = '100139';
    return $args;
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @return array
 */
function getPlusAdminWithContactRef(sfPwnPlusFunctionalTest $b)
{
    $args = $b->args('doPlusAdminLogin.php');
    $args['contact_ref'] = '292069';
    $args['account_id'] = '6';
    return $args;
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @return array
 */
function getPlusUserWithContactRef(sfPwnPlusFunctionalTest $b)
{
    $args = $b->args('doPlusUserLogin.php');
    $args['contact_ref'] = '312289';
    return $args;
}

/**
 * Creates a new Enhanced user, if you don't really care what the email of the new user is.
 *
 * @return string[]
 *   A map of email and password.
 */
function newEnhancedUser()
{
    return createNewEnhancedUser('functional.enhanced+' . uniqid() . '@powwownow.com');
}

/**
 * Creates a new Enhanced user, allowing you to specify the email.
 *
 * @param string $email
 *   The email for the new enhanced user.
 * @return string[]
 *   A map of email and password.
 */
function createNewEnhancedUser($email)
{
    $password = 'r1chm0nd';
    Hermes_Client_Rest::call(
        'doBasicRegistration',
        array(
            'email'  => $email,
            'locale' => 'en_GB',
            'source' => 'FunctionalTesting'
        )
    );
    $authentication = json_decode(
        Hermes_Client_Rest::callJson(
            'authenticateContact',
            array(
                'email'    => $email,
                'password' => 'default_empty_password_{}_~#########'
            )
        ),
        true
    );
    Hermes_Client_Rest::call(
        'updateContact',
        array(
            'contact_ref' => $authentication['contact_ref'],
            'password'    => 'r1chm0nd'
        )
    );
    return array(
        'contact_ref' => $authentication['contact_ref'],
        'email'       => $email,
        'password'    => $password,
    );
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @param $expected
 * @param string $message
 */
function testResponseContentMatches(sfPwnPlusFunctionalTest $b, $expected, $message = '')
{
    $b->test()->is($b->getResponse()->getContent(), $expected, $message);
}

/**
 * @return array
 */
function retrieveAllFeatureProductGroupIds()
{
    $featureProducts = Hermes_Client_Rest::call('getAllPlusProductGroups', array('feature' => true));
    $featureIds      = array();
    foreach ($featureProducts['product_list'] as $product) {
        $featureIds[] = $product['product_group_id'];
    }
    return $featureIds;
}

/**
 * @return array
 */
function retrieveAllBundleProductGroupIds()
{
    $bundleProducts = Hermes_Client_Rest::call('getAllPlusProductGroups', array('bundle' => true));
    $bundleIds      = array(
        'landline'   => array(),
        'individual' => array(),
    );
    foreach ($bundleProducts['product_list'] as $bundle) {
        if (!empty($bundle['is_individual_bundle'])) {
            $bundleIds['individual'][] = $bundle['product_group_id'];
        } else {
            $bundleIds['landline'][] = $bundle['product_group_id'];
        }
    }
    return $bundleIds;
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @param $contactRef
 * @param array $productGroupIds
 */
function testUserIsAssignedToProducts(sfPwnPlusFunctionalTest $b, $contactRef, array $productGroupIds)
{
    testProductAssignment($b, $contactRef, $productGroupIds, true);
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @param $contactRef
 * @param array $productGroupIds
 */
function testUserIsNotAssignedToProducts(sfPwnPlusFunctionalTest $b, $contactRef, array $productGroupIds)
{
    testProductAssignment($b, $contactRef, $productGroupIds, false);
}

/**
 * @param sfPwnPlusFunctionalTest $b
 * @param $contactRef
 * @param array $productGroupIds
 * @param bool $allAssigned
 */
function testProductAssignment(sfPwnPlusFunctionalTest $b, $contactRef, array $productGroupIds, $allAssigned = true)
{
    $tester            = $b->get('/s/Assign-Products')
        ->with('response')->begin();
    $productExpression = 'input[name="p%d[]"][value="' . $contactRef . '"][checked="checked"]';
    foreach ($productGroupIds as $productGroupId) {
        $tester->checkElement(sprintf($productExpression, $productGroupId), $allAssigned);
    }
    $tester->end();
}
