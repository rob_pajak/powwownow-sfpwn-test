<?php

/**
 * Basket helper functions to assert the state of the Basket.
 *
 * @author Maarten Jacobs
 */

/**
 * Assert that the Basket contains the given keys (product IDs).
 *
 * @param array $keys
 * @return bool
 */
function assertBasketHasKeys(array $keys) {
    $basket = new Basket(sfContext::getInstance()->getUser());
    $addedProducts = $basket->retrieveAddedProducts();
    $matchingProducts = array_intersect_key($addedProducts, array_flip($keys));
    return count($matchingProducts) === count($keys);
}

/**
 * Assert that the Basket contains only the given keys.
 *
 * @param array $keys
 * @return bool
 */
function assertBasketHasOnlyKeys(array $keys) {
    $basket = new Basket(sfContext::getInstance()->getUser());
    $addedProducts = $basket->retrieveAddedProducts();
    $matchingProducts = array_intersect_key($addedProducts, array_flip($keys));
    return count($matchingProducts) === count($keys) && count($addedProducts) === count($keys);
}

/**
 * Assert the basket does not have product IDs of the given keys.
 *
 * @param array $keys
 * @return bool
 */
function assertBasketDoesNotHaveKeys(array $keys) {
    $basket = new Basket(sfContext::getInstance()->getUser());
    $addedProducts = $basket->retrieveAddedProducts();
    $matchingProducts = array_intersect_key($addedProducts, array_flip($keys));
    return count($matchingProducts) === 0;
}

/**
 * Calls the AJAX route to remove an item from the basket.
 *
 * @param $b
 * @param int $productGroupId
 */
function removeProductFromBasket($b, $productGroupId) {
    $removeParameters = array(
        'remove-product' => $productGroupId,
    );
    $b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
        ->post('/s/Remove-From-Basket', $removeParameters);
}
