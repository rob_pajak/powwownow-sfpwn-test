<?php

/**
 * Generates Conference Calls by using an instance of Apollo.
 *
 * Note that we have no control of the Apollo service through this class; we can only push a simple command to it.
 *
 * @author Maarten Jacobs
 */
class ConferenceCallGenerator
{
    private $port;

    // @maybe: move this to app.yml.
    private $apolloIP = '10.170.5.120';

    private $conferenceSeq;

    private $serverSeq;

    /**
     * Constructs the generator, with the given port.
     *
     * @param int $port
     *   One of the ports the Apollo instance is listening to.
     *   Depending on the port, Apollo will insert the data into a different database.
     * @param int $initialConferenceId
     *   The initial conference id, which is incremented after inserting a new conference.
     * @param int $initialServerId
     *   The initial server id, which is incremented after inserting a new conference.
     */
    public function __construct($port, $initialConferenceId, $initialServerId)
    {
        $this->port = $port;
        $this->conferenceSeq = $initialConferenceId;
        $this->serverSeq = $initialServerId;
    }

    /**
     * Calls Apollo (an external service) to insert a conference call into the database.
     *
     * Careful: if the PIN does not exist, the service ref of the DNIS is used instead.
     *
     * @param string $pin
     * @param int $serviceRef
     * @param int $callStartTS
     *   The start time of the conference call; must be a timestamp.
     * @param int $callEndTS
     *   The end time of the conference call; must be a timestamp.
     * @param string $dnisNumber
     *   A valid dnis_number (see powwownow.dnis_number). By default set to an DNIS of the Enhanced users.
     * @return bool|string
     *   Returns true on success. On error, returns an error message.
     */
    public function generateConferenceCall($pin, $serviceRef, $callStartTS, $callEndTS, $dnisNumber = '08444737373')
    {
        $roomId = "$serviceRef$pin";

        $result = $this->insertApolloLine(
            $roomId,
            $this->conferenceSeq,
            $this->serverSeq,
            $pin,
            date('Y-m-d\TH:i:s.000\Z', $callStartTS),
            date('Y-m-d\TH:i:s.000\Z', $callEndTS),
            $callEndTS - $callStartTS,
            $dnisNumber
        );
        if ($result === true) {
            $result = $this->insertConference($this->conferenceSeq, $callStartTS, $callEndTS, $serviceRef, $pin);
        }

        $conferenceData = array(
            'result' => $result,
            'room_id' => $roomId,
            'conference_id' => $this->conferenceSeq,
            'server_seq' => $this->serverSeq,
        );
        $this->prepareForNextConference();
        return $conferenceData;
    }

    private function prepareForNextConference()
    {
        $this->conferenceSeq++;
        $this->serverSeq++;
    }

    private function insertConference($conferenceSeq, $callStartTS, $callEndTS, $serviceRef, $pin)
    {
        Hermes_Client_Rest::call(
            'insertTestConference',
            array(
                'conference_id' => $conferenceSeq,
                'server' => 'sbc98',
                'conference_start_date' => date('Y-m-d', $callStartTS),
                'conference_start_time' => date('H:i:s', $callStartTS),
                'conference_end_date' => date('Y-m-d', $callEndTS),
                'conference_end_time' => date('H:i:s', $callEndTS),
                'service_ref' => $serviceRef,
                'pin' => $pin,
            )
        );
        return true;
    }

    private function insertApolloLine($roomId, $conferenceSeq, $serverSeq, $pin, $callStart, $callEnd, $duration, $dnisNumber)
    {
        // Apollo expects a command with the following format:
        $line = array (
            'LINE' =>
            array (
                // In the database, this is the column conference_id of the powwownow.cdr table.
                'conf_seq' => $conferenceSeq,
                // In the database, this is the column server_seg of the cdrs.gcdr table.
                'seq' => $serverSeq,

                // Both of the following values use the pin and service ref.
                // Note that the pin and service ref are NOT explicitly passed.
                'ruri' => "mix:$dnisNumber@10.170.6.230:5060;ConfID=$roomId;ConfCtrls=512;ACode=$pin",
                'room_id' => $roomId,

                'duration' => $duration,
                'beg' => $callStart,
                'end' => $callEnd,

                // Both timestamps are not required to be the same as beg and end:
                // Apollo will use the formatted version regardless of their values.
                'beg_ts' => null,
                'end_ts' => null,

                // Hardcoded values: do not change unless Steve Lawson tells you to change it.
                'server' => 'sbc98',
                'dir' => 'o',
                'type' => 'mix',
                'codec' => 'pcmu/8000',
                'id' => 'peqx8b10',
                'by' => 'L',
                'code' => 200,
                'reason' => 'OK',
                'port' => 10013,
                'cli' => 'sip@10.160.9.25',
            ),
        );

        $apolloCommand = 'CDR> ' . json_encode($line);
        return $this->writeApolloCommandToSocket($apolloCommand, __METHOD__);
    }

    private function writeApolloCommandToSocket($apolloCommand, $method)
    {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($socket === FALSE) {
            return "$method: Socket create failed";
        }
        if (!socket_connect($socket, $this->apolloIP, $this->port)) {
            return "$method: Socket connect failed";
        }
        $apolloCommand = $apolloCommand . "\n";
        $bytesWritten = socket_write($socket, $apolloCommand, strlen($apolloCommand));
        if ($bytesWritten === FALSE) {
            return "$method: Socket write error";
        }
        socket_close($socket);

        return true;
    }
}
