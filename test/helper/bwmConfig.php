<?php
/**
 * Helper functions for the BWM Config functional tests.
 *
 * @author Maarten Jacobs
 */

/**
 * Return a number of sample DNIS for step 2 of BWM configuration.
 *
 * @param int $count
 *   The number of sample DNISes to return.
 * @return array
 *   An list of DNIS data structures with the keys:
 *      - dnis_ref
 *      - country
 *      - city
 *      - dnis_type
 *      - rate
 *      - country_code
 */
function retrieveSampleDNISes($count = 1) {
    if (!is_int($count) || $count < 1) {
        $count = 1;
    }
    $sampleDNISes = array();

    $bwmProductRates = Hermes_Client_Rest::call('BWM.getProductDialInNumbersWithRates', array(
        'dedicated' => 1,
        'allocated' => 0,
        'service_ref' => 850,
        'product_type' => 'VOICE',
        'grouped' => 1
    ));
    foreach ($bwmProductRates['products'] as $prd) {

        $sampleDNISes[] = array(
            $prd['dnis_ref'],
            $prd['country'],
            $prd['city'],
            ($prd['dnis_type'] === 'Geographic') ? 'Landline' : 'Freephone',
            ($prd['rate'] * 100) . ' pence',
            $prd['country_code']
        );

        $count--;
        if (!$count) {
            break;
        }
    }

    return $sampleDNISes;
}

/**
 * Performs a simple test for present error messages after invalid AJAX post request.
 *
 * @param $b
 *   Lime browser.
 * @param string $route
 *   The route to post.
 * @param array $faultyParameterLists
 */
function simpleAJAXPostErrorMessageCheck($b, $route, $faultyParameterLists) {
    $testCount = count($faultyParameterLists) * 5;
    $test = new lime_test($testCount, new lime_output_color());
    foreach ($faultyParameterLists as $parameters) {
        // Call the add-numbers routes.
        // This route returns the available numbers that can be assigned to the product.
        $b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
            ->post($route, $parameters);
        $response = $b->getResponse()->getContent();

        // Assert that the response is a valid JSON string.
        $test->ok(is_string($response), 'The response is a string.');
        $decodedResponse = json_decode($response);
        $test->ok(is_object($decodedResponse), 'The decoded response is a valid object.');

        // Assert that the decoded response has an error_messages property which is an array.
        $test->ok(isset($decodedResponse->error_messages), 'The decoded response has a property named "error_messages".');
        $test->ok(is_array($decodedResponse->error_messages), 'The decoded response has a property named "error_messages" which is an array.');

        // Assert that the error_messages property has at least one error message.
        $test->ok(count($decodedResponse->error_messages) > 0, 'The error messages is a non-empty array.');
    }
}

/**
 * Configure a new BWM for basket tests.
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param array $parameters
 *   A map of parameters for every step of the BWM flow, i.e.:
 *   - step-1: parameters for step 1.
 *   - step-2: parameters for step 2.
 *   - step-3: parameters for step 3.
 * @return array
 *   The actual used parameters.
 */
function configureBWM(sfPwnPlusFunctionalTest $b, array $parameters = array()) {
    // Step 1.
    $step1Parameters = array(
        'company' => 'Successful test company',
        'contact_name' => 'Successful Tester',
        'contact_number' => "2324124",
        'script_accept' => "1",
    );
    if (isset($parameters['step-1'])) {
        $step1Parameters = $parameters['step-1'] + $step1Parameters;
    }
    $b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
        ->post('/s/products/BwmAddNumbersAjax', $step1Parameters);

    // Step 2.
    $step2Parameters = array(
        'json' => json_encode(retrieveSampleDNISes(2)),
    );
    if (isset($parameters['step-2'])) {
        $step2Parameters = $parameters['step-2'] + $step2Parameters;
    }
    $b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
        ->post('/s/products/BwmSelectedNumbersAjax', $step2Parameters);

    // Step 3.
    $step3Parameters = array(
        'script_accept' => '1',
        'bwm_label' => 'Arbitrary label',
    );
    if (isset($parameters['step-3'])) {
        $step3Parameters = $parameters['step-3'] + $step3Parameters;
    }
    $b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
        ->post('/s/products/BwmSummaryAjax', $step3Parameters);

    return array(
        'step-1' => $step1Parameters,
        'step-2' => $step2Parameters,
        'step-3' => $step3Parameters,
    );
}

/**
 * Generic function to assert that the given text exists on the page served on the route.
 *
 * @param $b
 * @param string $route
 * @param string $text
 */
function assertTextExistsOnPage($b, $route, $text) {
    $b->get($route)
        ->with('response')
            ->matches("/$text/");
}

/**
 * Generic function to assert that the given texts exist on the page served on the route.
 *
 * @param $b
 * @param string $route
 * @param array $texts
 */
function assertTextsExistsOnPage($b, $route, array $texts) {
    $matcher = $b->get($route)
        ->with('response');
    foreach ($texts as $text) {
        $matcher->matches("/$text/");
    }
}
