<?php

/**
 * Check the Page Elements
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param array $pageElements
 */
function freePinTestPageElements(sfPwnPlusFunctionalTest $b, Array $pageElements)
{
    foreach ($pageElements as $element) {
        $b->with('response')->checkElement($element);
    }
}

/**
 * Check the Initial Form
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param array $pageIds
 * @return string
 */
function freePinTestInitialForm(sfPwnPlusFunctionalTest $b, Array $pageIds)
{
    $response = '';
    // No Email
    $args = array(
        'email'               => '',
        'registration_source' => $pageIds['registration_source'],
        'registration_type'   => $pageIds['registration_type'],
        'agree-tick'          => ''
    );
    $response .= testPostAjaxRequest($b, '/Free-Pin-Registration', 400, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

    // Invalid Email
    $args = array(
        'email'               => 'test+' . $pageIds['registration_source'] . uniqid(),
        'registration_source' => $pageIds['registration_source'],
        'registration_type'   => $pageIds['registration_type'],
        'agree-tick'          => ''
    );
    $response .= testPostAjaxRequest($b, '/Free-Pin-Registration', 400, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

    // Valid Email, Not Agreed
    $args = array(
        'email'               => 'test+' . $pageIds['registration_source'] . uniqid() . '@powwownow.com',
        'registration_source' => $pageIds['registration_source'],
        'registration_type'   => $pageIds['registration_type'],
        'agree-tick'          => ''
    );
    $response .= testPostAjaxRequest($b, '/Free-Pin-Registration', 400, $args, false, 'FORM_VALIDATION_T_AND_C');

    return $response;
}

/**
 * Check the Success of the Form
 *
 * @param sfPwnPlusFunctionalTest $b
 * @param array $pageIds
 * @return string
 */
function freePinTestSuccessForm(sfPwnPlusFunctionalTest $b, Array $pageIds)
{
    // Valid Arguments
    $args = array(
        'email'               => 'test+' . $pageIds['registration_source'] . uniqid() . '@powwownow.com',
        'registration_source' => $pageIds['registration_source'],
        'registration_type'   => $pageIds['registration_type'],
        'agree_tick'          => 'agreed'
    );
    $response = testPostAjaxRequest($b, '/Free-Pin-Registration', 200, $args, false, false);
    return $response;
}