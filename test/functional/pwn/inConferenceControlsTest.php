<?php
/**
 * This is the Test for the New In-Conference-Controls Test
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/inConferenceControls')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'inConferenceControls')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'In-Conference Controls')
            ->checkElement('h1.mypwn_sub_page_heading_powwownow')
            ->checkElement('div.inconferencecontrols h6:nth-child(1)','# = Skip Intro')
            ->checkElement('div.inconferencecontrols h6:nth-child(3)','#6 = Mute')
            ->checkElement('div.inconferencecontrols h6:nth-child(5)','#1 = Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(7)','#2 = Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(9)','#3 = Lock')
            ->checkElement('div.inconferencecontrols h6:nth-child(11)','#8 = Record')
            ->checkElement('button[href="/sfpdf/en/Powwownow-User-Guide.pdf"]')
            ->checkElementWithoutWhitespace('button.button-green-schedule','DOWNLOAD PDF')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/inConferenceControls')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'inConferenceControls')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'In-Conference Controls')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('div.inconferencecontrols h6:nth-child(1)','# = Skip Intro')
            ->checkElement('div.inconferencecontrols h6:nth-child(3)','#1 = Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(5)','## = Mute All')
            ->checkElement('div.inconferencecontrols h6:nth-child(7)','#2 = Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(9)','#6 = Mute')
            ->checkElement('div.inconferencecontrols h6:nth-child(11)','#3 = Lock')
            ->checkElement('div.inconferencecontrols h6:nth-child(13)','#7 = Private Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(15)','#9 = Private Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(17)','#8 = Record')
            ->checkElement('button[href="/sfpdf/en/Powwownow-User-Guide.pdf"]')
            ->checkElementWithoutWhitespace('button.button-green-schedule','DOWNLOAD PDF')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/inConferenceControls')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'inConferenceControls')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'In-Conference Controls')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('div.inconferencecontrols h6:nth-child(1)','# = Skip Intro')
            ->checkElement('div.inconferencecontrols h6:nth-child(3)','#1 = Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(5)','## = Mute All')
            ->checkElement('div.inconferencecontrols h6:nth-child(7)','#2 = Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(9)','#6 = Mute')
            ->checkElement('div.inconferencecontrols h6:nth-child(11)','#3 = Lock')
            ->checkElement('div.inconferencecontrols h6:nth-child(13)','#7 = Private Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(15)','#9 = Private Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(17)','#8 = Record')
            ->checkElement('button[href="/sfpdf/en/Powwownow-User-Guide.pdf"]')
            ->checkElementWithoutWhitespace('button.button-green-schedule','DOWNLOAD PDF')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/inConferenceControls')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'inConferenceControls')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'In-Conference Controls')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('div.inconferencecontrols h6:nth-child(1)','# = Skip Intro')
            ->checkElement('div.inconferencecontrols h6:nth-child(3)','#6 = Mute')
            ->checkElement('div.inconferencecontrols h6:nth-child(5)','#1 = Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(7)','#2 = Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(9)','#3 = Lock')
            ->checkElement('div.inconferencecontrols h6:nth-child(11)','## = Mute All')
            ->checkElement('div.inconferencecontrols h6:nth-child(13)','#7 = Private Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(15)','#9 = Private Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(17)','#8 = Record')
            ->checkElement('button[href="/sfpdf/en/Powwownow-Premium-User-Guide.pdf"]')
            ->checkElementWithoutWhitespace('button.button-green-schedule','DOWNLOAD PDF')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/inConferenceControls')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'inConferenceControls')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'In-Conference Controls')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('div.inconferencecontrols h6:nth-child(1)','# = Skip Intro')
            ->checkElement('div.inconferencecontrols h6:nth-child(3)','#6 = Mute')
            ->checkElement('div.inconferencecontrols h6:nth-child(5)','#1 = Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(7)','#2 = Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(9)','#3 = Lock')
            ->checkElement('div.inconferencecontrols h6:nth-child(11)','## = Mute All')
            ->checkElement('div.inconferencecontrols h6:nth-child(13)','#7 = Private Roll Call')
            ->checkElement('div.inconferencecontrols h6:nth-child(15)','#9 = Private Head Count')
            ->checkElement('div.inconferencecontrols h6:nth-child(17)','#8 = Record')
            ->checkElement('button[href="/sfpdf/en/Powwownow-Premium-User-Guide.pdf"]')
            ->checkElementWithoutWhitespace('button.button-green-schedule','DOWNLOAD PDF')
        ->end();