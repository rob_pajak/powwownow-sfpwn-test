<?php

/**
 * Assert that the Organisation Details form allows manipulation of the Plus admin contact.
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

// Create and test as a new Plus admin.
require_once 'registerPlusUserTest.php';
testValidOrganisationDetails($b, 'Testing the Contact details form as a Plus admin.');
logoutUser($b);

// Test as an Enhanced user; no access.
$enhancedCredentials = $b->args('doEnhancedLogin.php');
loginUser($b, $enhancedCredentials['email'], $enhancedCredentials['password']);
// @todo: no access results in HTTP status 200?!
testPostAjaxRequest($b, '/myPwn/Account-Details/submit/organisation', 200, array(), false, false);
$b->with('response')
    ->begin()
        ->checkElement('h1', 'No Access')
        ->checkElement('h2', 'You do not have permission to access this page!')
    ->end();
logoutUser($b);

// Test as a Plus user; no access.
$plusUserCredentials = $b->args('doPlusUserLogin.php');
loginUser($b, $plusUserCredentials['email'], $plusUserCredentials['password']);
testPostAjaxRequest($b, '/myPwn/Account-Details/submit/organisation', 200, array(), false, false);
$b->with('response')
    ->begin()
        ->checkElement('h1', 'No Access')
        ->checkElement('h2', 'You do not have permission to access this page!')
    ->end();
logoutUser($b);

// Test as a Premium user; no access.
$premiumUserCredentials = $b->args('doPremiumUserLogin.php');
loginUser($b, $premiumUserCredentials['email'], $premiumUserCredentials['password']);
testPostAjaxRequest($b, '/myPwn/Account-Details/submit/organisation', 200, array(), false, false);
$b->with('response')
    ->begin()
        ->checkElement('h1', 'No Access')
        ->checkElement('h2', 'You do not have permission to access this page!')
    ->end();
logoutUser($b);

// Test as a Premium admin; no access.
$premiumAdminCredentials = $b->args('doPremiumAdminLogin.php');
loginUser($b, $premiumAdminCredentials['email'], $premiumAdminCredentials['password']);
testPostAjaxRequest($b, '/myPwn/Account-Details/submit/organisation', 200, array(), false, false);
$b->with('response')
    ->begin()
        ->checkElement('h1', 'No Access')
        ->checkElement('h2', 'You do not have permission to access this page!')
    ->end();
logoutUser($b);
