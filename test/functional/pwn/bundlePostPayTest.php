<?php

// Assert that Post-Pay customers cannot add Call Credit cannot be added to the basket, and that the button to add the call credit is disabled,
// Assert that Post-Pay customers cannot add Bundles to their basket, and that the relevant buttons are disabled,
// Assert that the purchased Landline bundle can be reassigned on the "Assign Users" page,

require_once 'test/bootstrap/functional.php';

// create new plus account
$registrationData = $b->args('doPlusRegistrationRegister.php', array('email' => 'bundler'));
$registrationResponse = Hermes_Client_Rest::call('doPlusRegistrationRegister', $registrationData);

// print_r($registrationResponse);

// make account post-pay by adding bundle
$assignedBundle = Hermes_Client_Rest::call('Bundle.assignNewBundle',
    array(
        'account_id'        => $registrationResponse['account']['id'],
        'product_group_id'  => 222,
    )
);

// login
$b->info("login user {$registrationData['email']} with password {$registrationData['password']}")
    ->post('/s/test/login', array('user_email' => $registrationData['email'], 'user_password' => $registrationData['password']))
    ->with('response')
        ->begin()
            ->isStatusCode(200)
        ->end();

// test
$b->get('/myPwn/Products')
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('button[id="header-purchase-credit"]', false)
            ->checkElement('tr[class="credit-row"]', false)
            ->checkElement('#bundle-row-aycm button[class="button-green bundle-configure "]', false)
            ->checkElement('#bundle-row-1000 button[class="button-green bundle-configure "]', false)
            ->checkElement('#bundle-row-2500 button[class="button-green bundle-configure "]', false)
            ->checkElement('#bundle-row-5000 button[class="button-green bundle-configure "]', false)
        ->end();

// add user to account
Hermes_Client_Rest::call(
    'Default.doPlusAddContactAndPins',
    array(
        'account_id'    => $registrationResponse['account']['id'],
        'email'         => 'bundleTest.user-notAssigned+'.uniqid().'@test.pl',
        'first_name'    => 'test',
        'last_name'     => 'test',
        'password'      => 'test123',
        'source'        => 'PHPUNIT',
        'locale'        => 'en_GB',
        'service_ref'   => '850',
    )
);

// test
$b->get('/s/Assign-Products')
    ->with('response')
        ->begin()
            ->isStatusCode(302)
        ->end();

//@Todo rewrite this part of test
//
//$b->get('/s/Assign-Products')
//    ->with('response')
//    ->begin()
//         ->isStatusCode(200)
//        ->checkElement('td.pinproduct.pinproduct-222 input')
//        ->checkElement('td.pinproduct.pinproduct-222 input[value=' . $registrationResponse['contact']['contact_ref'] . ']')
//    ->end();
//
//
//$b->post('/s/Assign-Products', array(
//    'p222[]' => $registrationResponse['contact']['contact_ref'],
//    'p3[]' => $registrationResponse['contact']['contact_ref'],
//    'p4[]' => $registrationResponse['contact']['contact_ref'],
//    'p5[]' => $registrationResponse['contact']['contact_ref'],
//    'p6[]' => $registrationResponse['contact']['contact_ref']
//));

// td.pinproduct pinproduct-222 input#p222[]
// td.pinproduct.pinproduct-222 input