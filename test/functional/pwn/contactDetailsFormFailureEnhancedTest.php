<?php

/**
 * Assert that the Contact Details form allows manipulation of the Plus admin contact, and fails if the input is incorrect.
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

// Test as an Enhanced user.
$enhancedCredentials = newEnhancedUser();
loginUser($b, $enhancedCredentials['email'], $enhancedCredentials['password']);
$_SESSION['contact_ref'] = $enhancedCredentials['contact_ref'];
testContactDetailsFormFailure(
    $b,
    'Testing the Contact details form failure as an enhanced user.',
    'en_user_f ',
    array(
        'email' => $enhancedCredentials['email'],
        'contact_ref' => $enhancedCredentials['contact_ref']
    )
);
logoutUser($b);
