<?php
/**
 * Asserting The Bundle All You Can Eat on the Products Page
 *
 * @author Asfer Tamimi
 */
require_once 'registerPlusUserTest.php';

// Necessary setup.
$test         = new lime_test(null, new lime_output_color());
$productsPage = '/myPwn/Products';
$bundlesAjax  = '/s/Add-Bundle-To-Basket-Ajax';
$creditAjax   = '/s/Add-Credit-To-Basket-Ajax';
$removeAjax   = '/s/Remove-From-Basket';

// Add A Bundle
/** @var DOMDocument $productsDOM */
$b->get($productsPage);
$productsDOM = $b->getResponseDom();
$bundleAYCMProductId        = $productsDOM->getElementById('add-aycm')->getAttribute('value');
$bundleAYCMUpgradeProductId = $productsDOM->getElementById('upgrade-aycm')->getAttribute('value');

// PIN Pair Value Hack, since DOM does not allow me to actually look at the Option Value
$bundlePinPair = $productsDOM->getElementsByTagName('option')->item(0)->nodeValue;
$bundlePinPair = explode('/',str_replace(array('(',')'),array('',''),strstr($bundlePinPair,'(')));
$bundlePinPair = $bundlePinPair[0];

// No Params Post Test
$b->get($bundlesAjax)->with('response')->begin()->isStatusCode(404)->end();
$b->get($bundlesAjax)->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')->with('response')->begin()->isStatusCode(404)->end();

// New Bundle Post Test
$testValuesFail = array('add-bundle' => $bundleAYCMProductId, 'bundle-tc' => '1');
$testValuesPass = array('add-bundle' => $bundleAYCMProductId, 'bundle-tc' => '1', 'user' => $bundlePinPair);
testPostAjaxRequest($b, $bundlesAjax, 400, $testValuesFail, false, false);
testPostAjaxRequest($b, $bundlesAjax, 200, $testValuesPass, false, false);


assertBasketHasOnlyKeys(array($bundleAYCMProductId));
removeProductFromBasket($b, $bundleAYCMProductId);

// New Bundle + Upgrade Option Post Test
$testValuesFail = array(
    'add-bundle' => $bundleAYCMProductId,
    'upgrade-bundle' => $bundleAYCMUpgradeProductId,
    'bundle-tc' => '1'
);
$testValuesPass = array(
    'add-bundle' => $bundleAYCMProductId,
    'upgrade-bundle' => $bundleAYCMUpgradeProductId,
    'user' => $bundlePinPair,
    'bundle-tc' => '1'
);
testPostAjaxRequest($b, $bundlesAjax, 400, $testValuesFail, false, false);
testPostAjaxRequest($b, $bundlesAjax, 200, $testValuesPass, false, false);
assertBasketHasOnlyKeys(array($bundleAYCMProductId,$bundleAYCMUpgradeProductId));
