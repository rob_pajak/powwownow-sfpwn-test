<?php

/**
 * Makes assertions about the Bundle skeleton functionality.
 *
 * @author Maarten Jacobs
 */

require_once 'registerPlusUserTest.php';

$bundleProductIds = retrieveAllBundleProductGroupIds();
$bundle1000Id = $bundleProductIds['landline'][0];
$aycmBundleId = $bundleProductIds['individual'][0];

function addBundleSkeleton(sfPwnPlusFunctionalTest $b, $bundleId)
{
    $_SESSION['ProductSelector_bundle_id'] = $bundleId;
    makePostAJAXRequest($b, '/s/Basket', array());
}

function assertContainsBundleSkeleton(sfPwnPlusFunctionalTest $b, $isAYCMBundleSkeleton = false)
{
    $asserter = $b->get('/s/Basket')
        ->with('response')->begin()
            ->checkElement('.addon-bundle-form-row.addon-bundle-field')
            ->checkElement('#bundle-tc');
    if ($isAYCMBundleSkeleton) {
        $asserter->checkElement('.pin-pair-selection');
    }
    $asserter->end();
}

function assertBundleSkeletonCanBeUpgraded(sfPwnPlusFunctionalTest $b)
{
    testPostAjaxRequest($b, '/s/Upgrade-Bundle', 200, array(), false, false);
    $b->get('/s/Basket')->with('response')->checkElement('.addon-bundle-form-row.addon-bundle-field', false);
}

function assertBundleSkeletonCompletion(sfPwnPlusFunctionalTest $b, $bundleId)
{
    $_SESSION['ProductSelector_id'] = $bundleId;
    testPostAjaxRequest($b, '/s/Agree-To-Bundle-Terms-And-Conditions', 200, array(), false, false);
    $b->get('/s/Basket')->with('response')->checkElement('.bundle-actions', false);
}

function assertAYCMPinPairSelection($b, $initialChairmanPin, $targetChairmanPin)
{
    $b->get('/s/Basket')->with('response')->checkElement('#user option[selected="selected"]', "/$initialChairmanPin/");
    testPostAjaxRequest($b, '/s/Select-AYCM-Pin-Pair', 200, array('user' => $targetChairmanPin), false, false);
    $b->get('/s/Basket')->with('response')->checkElement('#user option[selected="selected"]', "/$targetChairmanPin/");
}

function retrieveAdminChairmanPins()
{
    $user = sfContext::getInstance()->getUser();
    PinHelper::doPlusAddPins(
        array(
            'service_ref' => $user->getAttribute('service_ref'),
            'contact_ref' => $user->getAttribute('contact_ref'),
            'account_id'  => $user->getAttribute('account_id'),
        )
    );
    $pinMap = plusCommon::retrieveContactPinMap($user->getAttribute('contact_ref'));
    return array_keys($pinMap);
}

$b->info('Assert that the Landline 1000 Bundle skeleton can be added to the Basket.');
addBundleSkeleton($b, $bundle1000Id);
assertContainsBundleSkeleton($b);
removeProductFromBasket($b, $bundle1000Id);
$b->info('Assert that the Landline 1000 Bundle skeleton can be upgraded.');
addBundleSkeleton($b, $bundle1000Id);
assertBundleSkeletonCanBeUpgraded($b);
removeProductFromBasket($b, $bundle1000Id);
$b->info('Assert that the Landline 1000 Bundle skeleton can be completed by agreeing to the Terms and Conditions.');
addBundleSkeleton($b, $bundle1000Id);
assertBundleSkeletonCompletion($b, $bundle1000Id);
removeProductFromBasket($b, $bundle1000Id);

$b->info('Assert that the AYCM Bundle skeleton can be added to the Basket.');
addBundleSkeleton($b, $aycmBundleId);
assertContainsBundleSkeleton($b, true);
removeProductFromBasket($b, $aycmBundleId);
$b->info('Assert that the AYCM Bundle skeleton can be upgraded.');
addBundleSkeleton($b, $aycmBundleId);
assertBundleSkeletonCanBeUpgraded($b);
removeProductFromBasket($b, $aycmBundleId);
$b->info('Assert that the PIN pair for the AYCM Bundle skeleton can be selected.');
addBundleSkeleton($b, $aycmBundleId);
list($initialChairmanPin, $targetChairmanPin) = retrieveAdminChairmanPins();
assertAYCMPinPairSelection($b, $initialChairmanPin, $targetChairmanPin);
removeProductFromBasket($b, $aycmBundleId);
$b->info('Assert that the AYCM Bundle skeleton can be completed by agreeing to the Terms and Conditions.');
addBundleSkeleton($b, $aycmBundleId);
assertBundleSkeletonCompletion($b, $aycmBundleId);
removeProductFromBasket($b, $aycmBundleId);
