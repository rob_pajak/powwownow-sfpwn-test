<?php

// Assert that when checking out the basket with a customer who has a balance greater (1) than the basket cost (2) and there is a bundle in the basket
// the customer is redirected to the Recurring Payment Agreement page (3) and all required parameters are set,

require_once 'test/bootstrap/functional.php';

// create new plus account
$registrationData = $b->args('doPlusRegistrationRegister.php', array('email' => 'bundler'));
$registrationResponse = Hermes_Client_Rest::call('doPlusRegistrationRegister', $registrationData);

// 1 add greater balance
Hermes_Client_Rest::call('Default.updatePlusAccountBalance', array(
        'account_id' => $registrationResponse['account']['id'],
        'credit_balance_diff' => 1000
    )
);

// login
$b->info("login user {$registrationData['email']} with password {$registrationData['password']}")
    ->post('/s/test/login', array('user_email' => $registrationData['email'], 'user_password' => $registrationData['password']))
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->end();


// 2 add bundle to the basket
$b->get('/myPwn/Products')
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('#bundle-row-5000 button[class="button-green bundle-configure "]')
    ->checkElement('#bundle-row-5000 button.button-green.bundle-configure.not-show', false)
    ->end();
$productsDOM = $b->getResponseDom();
$bundle5000ProductId = $productsDOM->getElementById('add-5000')->getAttribute('value');
$bundleArr = array('add-bundle' => $bundle5000ProductId, 'bundle-tc' => '1');
testPostAjaxRequest($b, '/s/Add-Bundle-To-Basket-Ajax', 200, $bundleArr, false, false);

assertBasketHasOnlyKeys(array($bundle5000ProductId));

// 3 test
$b->get('/s/Basket-Submit')->followRedirect()
    ->with('request')
        ->begin()
            ->isParameter('module', 'account')
            ->isParameter('action', 'recurringPaymentAgreementSetupPage')
        ->end();
