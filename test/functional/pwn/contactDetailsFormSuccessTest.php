<?php

/**
 * Assert that the Contact Details form allows manipulation of the Enhanced, Premium and Plus contact.
 * @todo Full User Tests Required
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

// Test as an Enhanced user.
$enhancedCredentials = newEnhancedUser();
loginUser($b, $enhancedCredentials['email'], $enhancedCredentials['password']);
testContactDetailsForm(
    $b,
    'Testing the Contact details form as an enhanced user.',
    'enhanced_user ',
    array(
        'email' => $enhancedCredentials['email'],
        'contact_ref' => $enhancedCredentials['contact_ref']
    )
);
logoutUser($b);

// Test as a Plus user.
$plusUserCredentials = getPlusUserWithContactRef($b);
loginUser($b, $plusUserCredentials['email'], $plusUserCredentials['password']);
testContactDetailsForm(
    $b,
    'Testing the Contact details form as a Plus user.',
    'plus_user ',
    array(
        'contact_ref' => $plusUserCredentials['contact_ref']
    )
);
logoutUser($b);

// Test as a Plus Admin.
$plusAdminCredentials = getPlusAdminWithContactRef($b);
loginUser($b, $plusAdminCredentials['email'], $plusAdminCredentials['password']);
testContactDetailsForm(
    $b,
    'Testing the Contact details form as a Plus admin.',
    'plus_admin ',
    array(
        'contact_ref' => $plusAdminCredentials['contact_ref']
    )
);
logoutUser($b);

// Test as a Premium user.
$premiumUserCredentials = getPremiumUserWithContactRef($b);
loginUser($b, $premiumUserCredentials['email'], $premiumUserCredentials['password']);
testContactDetailsForm(
    $b,
    'Testing the Contact details form as a Premium user.',
    'premium_user ',
    array(
        'contact_ref' => $premiumUserCredentials['contact_ref']
    )
);
logoutUser($b);

// Test as a Premium admin.
$premiumAdminCredentials = getPremiumAdminWithContactRef($b);
loginUser($b, $premiumAdminCredentials['email'], $premiumAdminCredentials['password']);
testContactDetailsForm(
    $b,
    'Testing the Contact details form as a Premium admin.',
    'premium_admin ',
    array(
        'contact_ref' => $premiumAdminCredentials['contact_ref']
    )
);
logoutUser($b);
