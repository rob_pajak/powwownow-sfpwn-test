<?php

/**
 * Assert that the password remains unchanged after submitting invalid parameters.
 * @todo Full User Tests Required
 */

require_once 'test/bootstrap/functional.php';

// Create and test as a new Plus admin.
$plusAdminCredentials = $b->args('doPlusAdminLogin.php');
testChangePasswordFailure($b, $plusAdminCredentials['email'], $plusAdminCredentials['password'], 'Testing the Password form as a Plus admin.', 'pl_admin_f');
logoutUser($b);

// Test as an Enhanced user.
$enhancedCredentials = $b->args('doEnhancedLogin.php');
testChangePasswordFailure($b, $enhancedCredentials['email'], $enhancedCredentials['password'], 'Testing the Password form as an enhanced user.', 'en_user_f');
logoutUser($b);

// Test as a Plus user.
$plusUserCredentials = $b->args('doPlusUserLogin.php');
testChangePasswordFailure($b, $plusUserCredentials['email'], $plusUserCredentials['password'], 'Testing the Password form as a Plus user.', 'pl_user_f');
logoutUser($b);

// Test as a Premium user.
$premiumUserCredentials = $b->args('doPremiumUserLogin.php');
testChangePasswordFailure($b, $premiumUserCredentials['email'], $premiumUserCredentials['password'], 'Testing the Password form as a Premium user.', 'pr_user_f');
logoutUser($b);

// Test as a Premium admin.
$premiumAdminCredentials = $b->args('doPremiumAdminLogin.php');
testChangePasswordFailure($b, $premiumAdminCredentials['email'], $premiumAdminCredentials['password'], 'Testing the Password form as a Premium admin.', 'pr_admin_f');
logoutUser($b);