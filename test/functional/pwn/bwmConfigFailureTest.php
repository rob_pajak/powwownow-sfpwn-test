<?php

require_once 'registerPlusUserTest.php';

// 1. Test an invalid first step.
// The first step requires 4 parameters.
// All of the faulty parameters should return a JSON response with error messages.
$faultyParameterLists = array(
    // Missing parameters.
    array(
        'company' => 'Successful test company',
        'contact_name' => 'Successful Tester',
        'script_accept' => "1",
    ),
    array(
        'company' => 'Successful test company',
        'contact_name' => 'Successful Tester',
    ),
    array(
        'company' => 'Successful test company',
    ),
    array(
    ),
    // Faulty types of parameters.
    array(
        'company' => 'Successful test company',
        'contact_name' => 'Successful Tester',
        'contact_number' => "2324124",
        // script_accept must be a string.
        'script_accept' => 1,
    ),
    // Empty parameters.
    array(
        'company' => 'Successful test company',
        'contact_name' => 'Successful Tester',
        'contact_number' => '',
        // script_accept must be a string.
        'script_accept' => 1,
    ),
    array(
        'company' => 'Successful test company',
        'contact_name' => '',
        'contact_number' => "2324124",
        'script_accept' => "1",
    ),
    array(
        'company' => '',
        'contact_name' => 'Successful Tester',
        'contact_number' => "2324124",
        'script_accept' => "1",
    ),
);

// Run invalid tests.
simpleAJAXPostErrorMessageCheck($b, '/s/products/BwmAddNumbersAjax', $faultyParameterLists);

// 2. Test invalid second steps.
$route = '/s/products/BwmSelectedNumbersAjax';
$faultyParameterLists = array(
    // 5.a. Missing arguments.
    array(),
    // 5.b. Empty arguments.
    array(
        'json' => json_encode('')
    ),
    // 5.c. Missing inner parameters.
    // @todo: improve error checking in Products.executeBwmSelectedNumbersAjax by checking for invalid DNISes.
);
// Run invalid tests.
simpleAJAXPostErrorMessageCheck($b, $route, $faultyParameterLists);

// 3. Test invalid third steps.
$route = '/s/products/BwmSummaryAjax';
$faultyParameterLists = array(
    // 3.a. Missing parameters.
    array(),
    array(
        'script_accept' => '1',
    ),
    array(
        'bwm_label' => 'Arbitrary label',
    ),
    array(
        'script_accept' => '1',
        'bwm_label' => 'Arbitrary label',
    ),
    // 3.b. Empty/invalid parameters.
    array(
        // Not accepting anything here!
        'script_accept' => '0',
        'bwm_label' => 'Arbitrary label',
    ),
    array(
        'script_accept' => array(),
        'bwm_label' => 'Arbitrary label',
    ),
    array(
        'script_accept' => '1',
        'bwm_label' => '',
    ),
    array(
        'script_accept' => '1',
        'bwm_label' => null,
    ),
    array(
        'script_accept' => '1',
        'bwm_label' => 'Arbitrary label',
    ),
);
simpleAJAXPostErrorMessageCheck($b, $route, $faultyParameterLists);
