<?php
/**
 * Assert that the Contact Details form allows manipulation of the Plus admin contact, and fails if the input is incorrect.
 */

require_once 'test/bootstrap/functional.php';

// Plus Admin
$plusAdminCredentials = getPlusAdminWithContactRef($b);
testContactDetailsFormFailure(
    $b,
    'Testing the Contact details form failure as a Plus admin.',
    'pl_admin_f ',
    array(
        'contact_ref' => $plusAdminCredentials['contact_ref']
    )
);
logoutUser($b);

// Test as a Plus user.
$plusUserCredentials = getPlusUserWithContactRef($b);
loginUser($b, $plusUserCredentials['email'], $plusUserCredentials['password']);
testContactDetailsFormFailure(
    $b,
    'Testing the Contact details form failure as a Plus user.',
    'pl_user_f ',
    array(
        'contact_ref' => $plusUserCredentials['contact_ref']
    )
);
logoutUser($b);
