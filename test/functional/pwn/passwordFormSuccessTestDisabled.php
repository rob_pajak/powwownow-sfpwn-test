<?php

/**
 * Assert that the password form allows changing of the user's password.
 * @todo Full User Tests Required
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

// Create and test as a new Plus admin.
$plusAdminCredentials = $b->args('doPlusAdminLogin.php');
testChangePassword($b, $plusAdminCredentials['email'], $plusAdminCredentials['password'], 'Testing the Password form as a Plus admin.', 'pl_admin ');
logoutUser($b);

// Test as an Enhanced user.
$enhancedCredentials = $b->args('doEnhancedLogin.php');
testChangePassword($b, $enhancedCredentials['email'], $enhancedCredentials['password'], 'Testing the Password form as an enhanced user.', 'en_user_f ');
logoutUser($b);

// Test as a Plus user.
$plusUserCredentials = $b->args('doPlusUserLogin.php');
testChangePassword($b, $plusUserCredentials['email'], $plusUserCredentials['password'], 'Testing the Password form as a Plus user.', 'pl_user_f ');
logoutUser($b);

// Test as a Premium user.
$premiumUserCredentials = $b->args('doPremiumUserLogin.php');
testChangePassword($b, $premiumUserCredentials['email'], $premiumUserCredentials['password'], 'Testing the Password form as a Premium user.', 'pr_user_f ');
logoutUser($b);

// Test as a Premium admin.
$premiumAdminCredentials = $b->args('doPremiumAdminLogin.php');
testChangePassword($b, $premiumAdminCredentials['email'], $premiumAdminCredentials['password'], 'Testing the Password form as a Premium admin.', 'pr_admin_f ');
logoutUser($b);
