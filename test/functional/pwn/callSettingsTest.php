<?php
/**
 * This is the Test for the Call Settings Test
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 *
 */
$routeCallSettings = '/myPwn/Call-Settings';
$routeCallSettingsAjax = '/s/Call-Settings-Ajax';
$debug = false;


// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get($routeCallSettings)
    ->with('request')
    ->begin()
    ->isParameter('module', 'mypwn')
    ->isParameter('action', 'callSettings')
    ->end()
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'Call Settings')
    ->checkElement('h1.mypwn_sub_page_heading_powwownow')
    ->checkElement('form#frm-call-settings')
    ->checkElement('select#callSettings_voice_prompt_language')
    ->checkElement('select#callSettings_on_hold_music')
    ->checkElement('select#callSettings_entry_exit_announcements')
    ->checkElement('select#callSettings_announcement_types')
    ->checkElement('select#callSettings_play_participant_count')
    ->checkElement('button', 'UPDATE CALL SETTINGS')
    ->end();
$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(148969),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_types'       => '0',
        'play_participant_count'   => '2'
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 200, $args, $debug, false);

// Plus User Login
require_once 'loginPlusUser.php';

$b->get($routeCallSettings)
    ->with('request')
    ->begin()
    ->isParameter('module', 'mypwn')
    ->isParameter('action', 'callSettings')
    ->end()
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'Call Settings')
    ->checkElement('h1.mypwn_sub_page_heading_plus')
    ->checkElement('table#tblCallSettings')
    ->checkElement('form#frm-call-settings')
    ->checkElement('select#callSettings_voice_prompt_language')
    ->checkElement('select#callSettings_on_hold_music')
    ->checkElement('select#callSettings_entry_exit_announcements')
    ->checkElement('select#callSettings_announcement_types')
    ->checkElement('select#callSettings_play_participant_count')
    ->checkElement('select#callSettings_chairman_present')
    ->checkElement('input#callSettings_description')
    ->checkElement('input#callSettings_chairperson_only_Yes')
    ->checkElement('input#callSettings_chairperson_only_No')
    ->checkElement('button', 'UPDATE CALL SETTINGS')
    ->end();
$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PIN');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(410928),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_LANGUAGE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(410928),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(410928),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(410928),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(410928),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_types'       => '0',
        'play_participant_count'   => '2',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 200, $args, $debug, false);

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get($routeCallSettings)
    ->with('request')
    ->begin()
    ->isParameter('module', 'mypwn')
    ->isParameter('action', 'callSettings')
    ->end()
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'Call Settings')
    ->checkElement('h1.mypwn_sub_page_heading_plus')
    ->checkElement('table#tblCallSettings')
    ->checkElement('form#frm-call-settings')
    ->checkElement('select#callSettings_voice_prompt_language')
    ->checkElement('select#callSettings_on_hold_music')
    ->checkElement('select#callSettings_entry_exit_announcements')
    ->checkElement('select#callSettings_announcement_types')
    ->checkElement('select#callSettings_play_participant_count')
    ->checkElement('select#callSettings_chairman_present')
    ->checkElement('input#callSettings_description')
    ->checkElement('input#callSettings_chairperson_only_Yes')
    ->checkElement('input#callSettings_chairperson_only_No')
    ->checkElement('input#callSettings_cost_code')
    ->end();
$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PIN');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(382076),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_LANGUAGE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(382076),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(382076),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(382076),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(382076),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_types'       => '0',
        'play_participant_count'   => '2',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 200, $args, $debug, false);


// Premium User Login
require_once 'loginPremiumUser.php';

$b->get($routeCallSettings)
    ->with('request')
    ->begin()
    ->isParameter('module', 'mypwn')
    ->isParameter('action', 'callSettings')
    ->end()
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'Call Settings')
    ->checkElement('h1.mypwn_sub_page_heading_premium')
    ->checkElement('table#tblCallSettings')
    ->checkElement('form#frm-call-settings')
    ->checkElement('select#callSettings_voice_prompt_language')
    ->checkElement('select#callSettings_on_hold_music')
    ->checkElement('select#callSettings_entry_exit_announcements')
    ->checkElement('select#callSettings_announcement_types')
    ->checkElement('select#callSettings_play_participant_count')
    ->checkElement('select#callSettings_chairman_present')
    ->checkElement('input#callSettings_description')
    ->checkElement('input#callSettings_chairperson_only_Yes')
    ->checkElement('input#callSettings_chairperson_only_No')
    ->checkElement('button', 'UPDATE CALL SETTINGS')
    ->end();
$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PIN');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154499),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_LANGUAGE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154499),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154499),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154499),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154499),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_types'       => '0',
        'play_participant_count'   => '2',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 200, $args, $debug, false);

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get($routeCallSettings)
    ->with('request')
    ->begin()
    ->isParameter('module', 'mypwn')
    ->isParameter('action', 'callSettings')
    ->end()

    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'Call Settings')
    ->checkElement('h1.mypwn_sub_page_heading_premium')
    ->checkElement('table#tblCallSettings')
    ->checkElement('form#frm-call-settings')
    ->checkElement('select#callSettings_voice_prompt_language')
    ->checkElement('select#callSettings_on_hold_music')
    ->checkElement('select#callSettings_entry_exit_announcements')
    ->checkElement('select#callSettings_announcement_types')
    ->checkElement('select#callSettings_play_participant_count')
    ->checkElement('select#callSettings_chairman_present')
    ->checkElement('input#callSettings_description')
    ->checkElement('input#callSettings_chairperson_only_Yes')
    ->checkElement('input#callSettings_chairperson_only_No')
    ->checkElement('input#callSettings_cost_code')
    ->checkElement('button', 'UPDATE CALL SETTINGS')
    ->end();
$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PIN');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154118),
        'voice_prompt_language'    => '',
        'on_hold_music'            => '',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_LANGUAGE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154118),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154118),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => '',
        'description'              => '',
        'chairperson_only'         => '',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154118),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_type'        => '',
        'play_participant_count'   => '',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 400, $args, $debug, 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT');

$args = array(
    'callSettings' => array(
        'pin_ref'                  => array(154118),
        'voice_prompt_language'    => 'en_GB',
        'on_hold_music'            => '8',
        'entry_exit_announcements' => '2',
        'announcement_types'       => '0',
        'play_participant_count'   => '2',
        'chairman_present'         => 'Optional',
        'description'              => '',
        'chairperson_only'         => 'No',
        'cost_code'                => ''
    )
);
testPostAjaxRequest($b, $routeCallSettingsAjax, 200, $args, $debug, false);