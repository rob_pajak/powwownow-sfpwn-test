<?php
/**
 * This is the Test for the New Request Welcome Pack Test
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/requestWelcomePack')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'requestWelcomePack')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Request Welcome Pack')
            ->checkElement('h1.mypwn_sub_page_heading_powwownow')
            ->checkElement('form#frm-request-welcome-pack')
            ->checkElement('input#requestWelcomePack_first_name')
            ->checkElement('input#requestWelcomePack_last_name')
            ->checkElement('input#requestWelcomePack_company')
            ->checkElement('input#requestWelcomePack_address')
            ->checkElement('input#requestWelcomePack_town')
            ->checkElement('input#requestWelcomePack_county')
            ->checkElement('input#requestWelcomePack_postcode')
            ->checkElement('input#requestWelcomePack_country')
            ->checkElement('button','REQUEST WELCOME PACK')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/requestWelcomePack')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'requestWelcomePack')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Request Welcome Pack')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('form#frm-request-welcome-pack')
            ->checkElement('input#requestWelcomePack_first_name')
            ->checkElement('input#requestWelcomePack_last_name')
            ->checkElement('input#requestWelcomePack_company')
            ->checkElement('input#requestWelcomePack_address')
            ->checkElement('input#requestWelcomePack_town')
            ->checkElement('input#requestWelcomePack_county')
            ->checkElement('input#requestWelcomePack_postcode')
            ->checkElement('input#requestWelcomePack_country')
            ->checkElement('button','REQUEST WELCOME PACK')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/requestWelcomePack')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'requestWelcomePack')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Request Welcome Pack')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('form#frm-request-welcome-pack')
            ->checkElement('input#requestWelcomePack_company')
            ->checkElement('input#requestWelcomePack_address')
            ->checkElement('input#requestWelcomePack_town')
            ->checkElement('input#requestWelcomePack_county')
            ->checkElement('input#requestWelcomePack_postcode')
            ->checkElement('input#requestWelcomePack_country')
            ->checkElement('button.walletcard-sendbtn','REQUEST WELCOME PACK')            
            ->checkElement('table#tblRequestWelcomePack')
            ->checkElement('table.mypwn')
            ->checkElement('table.plus')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/requestWelcomePack')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'requestWelcomePack')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Request Welcome Pack')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('form#frm-request-welcome-pack')
            ->checkElement('input#requestWelcomePack_first_name')
            ->checkElement('input#requestWelcomePack_last_name')
            ->checkElement('input#requestWelcomePack_company')
            ->checkElement('input#requestWelcomePack_address')
            ->checkElement('input#requestWelcomePack_town')
            ->checkElement('input#requestWelcomePack_county')
            ->checkElement('input#requestWelcomePack_postcode')
            ->checkElement('select#requestWelcomePack_country')
            ->checkElement('button','REQUEST WELCOME PACK')            
            ->checkElement('table#tblRequestWelcomePack')
            ->checkElement('table.mypwn')
            ->checkElement('table.premium')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/requestWelcomePack')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'requestWelcomePack')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Request Welcome Pack')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('form#frm-request-welcome-pack')
            ->checkElement('input#requestWelcomePack_company')
            ->checkElement('input#requestWelcomePack_address')
            ->checkElement('input#requestWelcomePack_town')
            ->checkElement('input#requestWelcomePack_county')
            ->checkElement('input#requestWelcomePack_postcode')
            ->checkElement('select#requestWelcomePack_country')
            ->checkElement('button','REQUEST WELCOME PACK')            
            ->checkElement('table#tblRequestWelcomePack')
            ->checkElement('table.mypwn')
            ->checkElement('table.premium')
        ->end();