<?php
/**
 * Assert that the BWM config methods respond as expected.
 *
 * The tests are executed as Plus admin.
 */

require_once 'registerPlusUserTest.php';

// 1. Test the BWM form request route.
$test = new lime_test(4, new lime_output_color());

// Check the create new BWM tooltip has the correct pricing
$b->get('/myPwn/Products')
  ->with('request')
  ->begin()
  ->end()
  ->with('response')
  ->begin()
      ->isStatusCode(200)
      ->checkElement('table[id=configured-bwm-prices]', '*Branded Welcome Message*')
      ->checkElement('table[id=configured-bwm-prices]', '*£50*')
      ->checkElement('table[id=configured-bwm-prices]', '*Annual charge per dedicated number*')
      ->checkElement('table[id=configured-bwm-prices]', '*£60*')
  ->end();

// Request the BWM form based on the account.
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->get('/s/products/BwmScriptConfigAjax');
$response = $b->getResponse()->getContent();

// Assert the response is a JSON string.
$test->ok(is_string($response), 'The response is a string.');
$decodedResponse = json_decode($response);
$test->ok(is_object($decodedResponse), 'The decoded response is a valid object.');

// Assert that the decoded JSON has a key named html
$test->ok(isset($decodedResponse->html), 'The decoded response has a key named "html".');

// Assert that value assigned to the 'html' key is an HTML string.
$test->ok(is_string($decodedResponse->html), 'The decoded response has a key named "html" which is a string.');

// 2. Test valid first step.
// The first step allows adding a product label, company name and contact number.
$test = new lime_test(12, new lime_output_color());
$successParameters = array(
    'company' => 'Successful test company',
    'contact_name' => 'Successful Tester',
    'contact_number' => "2324124",
    'script_accept' => "1",
);

// Call the add-numbers routes.
// This route returns the available numbers that can be assigned to the product.
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->post('/s/products/BwmAddNumbersAjax', $successParameters);
$response = $b->getResponse()->getContent();

// Assert that the response is a valid JSON string.
$test->ok(is_string($response), 'The response is a string.');
$decodedResponse = json_decode($response);
$test->ok(is_object($decodedResponse), 'The decoded response is a valid object.');

// Assert that the decoded response has an 'html' property, which is a string.
$test->ok(isset($decodedResponse->html), 'The decoded response has a key named "html".');
$test->ok(is_string($decodedResponse->html), 'The decoded response has a key named "html" which is a string.');

// Assert that the decoded response has an 'bwmNumbersJson' property, which is an array.
$test->ok(isset($decodedResponse->bwmNumbersJson), 'The decoded response has a key named "bwmNumbersJson".');
$test->ok(is_array($decodedResponse->bwmNumbersJson), 'The decoded response has a key named "bwmNumbersJson" which is an array.');

// Assert that the decoded response has an 'bwmCharges' property, which is an object with two properties:
$test->ok(isset($decodedResponse->bwmCharges), 'The decoded response has a key named "bwmCharges".');
$test->ok(is_object($decodedResponse->bwmCharges), 'The decoded response has a key named "bwmCharges" which is an object.');
// - connectionFee, which is an integer, and,
$test->ok(isset($decodedResponse->bwmCharges->connectionFee), 'The bwmCharges property has a property named "connectionFee".');
$test->ok(is_int($decodedResponse->bwmCharges->connectionFee), 'The bwmCharges property has a property named "connectionFee" which is an integer.');
// - dnisPerRate, which is an integer.
$test->ok(isset($decodedResponse->bwmCharges->dnisPerRate), 'The bwmCharges property has a property named "dnisPerRate".');
$test->ok(is_int($decodedResponse->bwmCharges->dnisPerRate), 'The bwmCharges property has a property named "dnisPerRate" which is an integer.');

// 3. Test valid second steps.
// The second step consists of selecting available numbers.
// The AJAX route /s/products/BwmSelectedNumbersAjax is expecting a parameter with -sigh- keyname 'json'.
$route = '/s/products/BwmSelectedNumbersAjax';
$successParameters = array(
    'json' => json_encode(retrieveSampleDNISes(2)),
);
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->post($route, $successParameters);
$test = new lime_test(4, new lime_output_color());
$response = $b->getResponse()->getContent();

// Assert that the response is a JSON string.
$test->ok(is_string($response), 'The response is a JSON string.');
$decodedResponse = json_decode($response);

// Assert that the decoded response is a valid object.
$test->ok(is_object($decodedResponse), 'The decoded response is a valid object.');

// Assert that the decoded response has an html key.
$test->ok(!empty($decodedResponse->html), 'The decoded response has a key named "html".');
$test->ok(is_string($decodedResponse->html), 'The decoded response has a string key named "html".');

// 4. Test valid third steps.
// This step is reliant on the Session manipulations of the previous steps.
$successParameters = array(
    'script_accept' => '1',
    'bwm_label' => 'Arbitrary label',
);
$route = '/s/products/BwmSummaryAjax';
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->post($route, $successParameters);

// Assert that nothing is returned and the response is successful.
$test = new lime_test(2, new lime_output_color());
$b->with('response')->begin()
    ->isStatusCode(200)
  ->end();
$response = $b->getResponse()->getContent();
$test->ok(!empty($response), 'The response is not empty.');
// Assert that the basket has a product id with -1 in it.
$basket = new Basket(sfContext::getInstance()->getUser());
$addedProducts = $basket->retrieveAddedProductS();
$test->ok(isset($addedProducts[-1]), 'A product with id -1 has been added to the basket.');


// View the basket page to see if BWM was successfully put in the basket
$b->get('/s/Basket')
    ->with('request')
    ->begin()
    ->end()
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('tr[data-cost="£170.00"] div[class="product-name product-1"]', '*Arbitrary label*')
    ->checkElement('tr[data-cost="£170.00"] td[class="one-off charge"]', '£50.00')
    ->checkElement('tr[data-cost="£170.00"] td[class="yearly charge"]', '£120.00')
    ->end();
// Removing whitespace for the total price test.
$domSelector = new sfDomCssSelector($b->getResponseDOM());
$val = $domSelector->matchAll('tr[data-cost="£170.00"] td[class="total charge"]')->getValues();
$b->test()->is(trim(array_shift($val)), '£170.00');