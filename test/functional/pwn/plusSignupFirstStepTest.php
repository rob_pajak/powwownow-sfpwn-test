<?php

/**
 * Assert that the first step of the Plus Signup is only accessible to anonymous users via POST, AJAX requests.
 *
 * The first form consists of submitting an email and agreeing to terms and conditions.
 */

require_once 'test/bootstrap/functional.php';
$firstStepRoute = '/s/ajax/plus-signup/first-step';
$enhancedCredentials = $b->args('doEnhancedLogin.php');
$emailIt = 1;

// Assert GET requests are not allowed, even if they are AJAX.
$b->info('Asserting 404 for GET requests to first step of plus-signup.')
    ->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->get($firstStepRoute)
    ->with('response')->begin()
        ->isStatusCode(404)
    ->end();

// Assert that anonymous requests including email, and terms and conditions set to agreed, result in a 200 status code.
$email = 'valid-non-existing-email-' . $emailIt++ . '@powwownow.com';
testPostAjaxRequest($b, $firstStepRoute, 200, array('email' => $email, 'terms_and_conditions' => 1));
$responseContent = $b->getResponse()->getContent();
$b->test()->ok(!empty($responseContent));
$response = json_decode($responseContent, true);
$b->test()->is($email, $response['form_result']['email'], 'First step of plus signup response contains given email.');

// Assert that anonymous requests including email, but not agreeing to terms and conditions, results in error message TERMS_ARE_REQUIRED and 400 status code.
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => 'valid-non-existing-email-' . $emailIt++ . '@powwownow.com'));
plusSignupResponseContainsErrorMessages($b, array('terms_and_conditions' => 'TERMS_ARE_REQUIRED'));

// Invalid terms and conditions value.
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => 'valid-non-existing-email-' . $emailIt++ . '@powwownow.com', 'terms_and_conditions' => 0));
plusSignupResponseContainsErrorMessages($b, array('terms_and_conditions' => 'TERMS_ARE_REQUIRED'));

// Assert that anonymous requests including an invalid email results in error message 'INVALID_EMAIL' and 400 status code.
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => "ceci n'est pas un email.", 'terms_and_conditions' => 1));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS'));

// Assert empty email results in error message 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS' and 400 status code.
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => "", 'terms_and_conditions' => 1));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS'));

// Assert empty email and not agreeing to terms and conditions results in 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'TERMS_ARE_REQUIRED' and 400 status code.
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => "", 'terms_and_conditions' => ''));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'terms_and_conditions' => 'TERMS_ARE_REQUIRED'));

// Tests for Enhanced users.
$enhancedUser = createNewEnhancedUser(generateTestEmail());
loginUser($b, $enhancedUser['email'], $enhancedUser['password'], 'Logging in enhanced user to test the first step of the plus signup process.');

// Assert that enhanced users accessing this route with valid email and agreeing to terms and conditions, results in 200 status code.
$dump = testPostAjaxRequest($b, $firstStepRoute, 200, array('email' => $enhancedUser['email'], 'terms_and_conditions' => 1));

// Assert that the response contains first_name, last_name, company and business_phone.
$responseContent = $b->getResponse()->getContent();
$b->test()->ok(!empty($responseContent));
$response = json_decode($responseContent, true);
$b->test()->is($enhancedUser['email'], $response['form_result']['email'], 'First step of plus signup response to enhanced user contains given email.');
$b->test()->ok(isset($response['form_result']['first_name']), 'First step of plus signup response to enhanced user contains first name.');
$b->test()->ok(isset($response['form_result']['last_name']), 'First step of plus signup response to enhanced user contains last name.');
$b->test()->ok(isset($response['form_result']['company_name']), 'First step of plus signup response to enhanced user contains company name.');
$b->test()->ok(isset($response['form_result']['business_phone']), 'First step of plus signup response to enhanced user contains business phone.');

// Assert that enhanced users accessing this route with non-matching email, results in error message 'FORBIDDEN_EMAIL' and 400 status code.
$plusUserCredentials = $b->args('doPlusUserLogin.php');
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => $plusUserCredentials['email'], 'terms_and_conditions' => 1));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORBIDDEN_EMAIL'));

// Assert that enhanced users accessing this route with invalid email, results in error message 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS' and 400 status code.
testPostAjaxRequest($b, $firstStepRoute, 400, array('terms_and_conditions' => 1));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS'));

testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => '', 'terms_and_conditions' => 1));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS'));

testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => array(), 'terms_and_conditions' => 1));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS'));

// Assert that enhanced users accessing this route without agreeing to the terms and conditions, results in error message 'TERMS_ARE_REQUIRED' and 400 status code.
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => $enhancedUser['email']));
plusSignupResponseContainsErrorMessages($b, array('terms_and_conditions' => 'TERMS_ARE_REQUIRED'));

testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => $enhancedUser['email'], 'terms_and_conditions' => 0));
plusSignupResponseContainsErrorMessages($b, array('terms_and_conditions' => 'TERMS_ARE_REQUIRED'));

// Assert empty email and not agreeing to terms and conditions results in 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'TERMS_ARE_REQUIRED' and 400 status code.
testPostAjaxRequest($b, $firstStepRoute, 400, array('email' => "", 'terms_and_conditions' => ''));
plusSignupResponseContainsErrorMessages($b, array('email' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'terms_and_conditions' => 'TERMS_ARE_REQUIRED'));

// Assert that plus users accessing this route results in 403 status code.
require 'loginPlusUser.php';
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->post($firstStepRoute, array())
    ->with('response')->begin()
        ->isStatusCode(403)
    ->end();
