<?php
require_once 'test/bootstrap/functional.php';
$user = $b->args('doEnhancedRegistrationRegister.php');
$result = Common::doCreateOrUpdateAccount(array(
    'email'      => $user['createalogin']['email'], 
    'first_name' => $user['createalogin']['first_name'],
    'last_name'  => $user['createalogin']['last_name'], 
    'password'   => $user['createalogin']['password'],
    'source'     => '/Create-A-Login-Functional-Test',
    'locale'     => 'en_GB',
));

$b->info('login user ' . $user['createalogin']['email'] . ' with password ' . $user['createalogin']['password'])
    ->post('/s/test/login', array('user_email' => $user['createalogin']['email'], 'user_password' => $user['createalogin']['password']))
        ->with('response')->isStatusCode(200);
