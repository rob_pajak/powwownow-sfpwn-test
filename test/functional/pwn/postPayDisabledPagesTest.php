<?php

/**
 * Asserts that, for post-pay customers:
 * - the Auto Top-Up page only contains a message regarding disabled auto top-up,
 * - the Auto Top-Up AJAX action is disabled,
 * - the Auto Top-Up and Purchase Credit menu links are not shown.
 *
 * @author Maarten Jacobs
 */

$autoTopUpUrl = '/s/Auto-Topup';
$autoTopUpAJAXUrl = '/s/Auto-Topup-Ajax';
$purchaseCreditUrl = '/s/Purchase-Credit';

// Create a new plus admin
require_once 'registerPlusUserTest.php';

// Assert that Auto Top-Up is disabled, and the regular message is shown.
$b->get($autoTopUpUrl)
    ->with('response')
        ->checkElement('div.auto-top-up-disabled-content:contains("Auto Top-up is currently disabled on your account.")');

// Assert that Auto Top-Up AJAX action results in a successful response (even though it's not enabled?!).
makePostAJAXRequest($b, $autoTopUpAJAXUrl, array());

// Assert that the Auto Top-Up and Purchase Credit menu links are present.
$b->get($autoTopUpUrl)
    ->with('response')->begin()
        ->checkElement('#top-menu1 > li:nth(1) a[href$="' . $purchaseCreditUrl . '"]')
        ->checkElement('#top-menu1 > li:nth(1) a[href$="' . $autoTopUpUrl . '"]')
        ->checkElement('#top-menu1 > li:nth(2) a[href$="' . $purchaseCreditUrl . '"]')
        ->checkElement('#top-menu1 > li:nth(2) a[href$="' . $autoTopUpUrl . '"]')
    ->end();

// Convert the customer. (see Age of Empires monk)
$bundle5000Ids = retrieveBundleProductGroupId($b);
$accountId = sfContext::getInstance()->getUser()->getAttribute('account_id');
Hermes_Client_Rest::call('Bundle.assignNewBundle', array('account_id' => $accountId, 'product_group_id' => $bundle5000Ids['bundle_id']));
plusCommon::retrievePostPayStatus($accountId, true);

// Assert that the Auto Top-Up page is accessible, but it shows a message regarding post-pay.
$b->get($autoTopUpUrl)
    ->with('response')
        ->checkElement('.grid_24:contains("Auto Top-Up is disabled for your account as you are a Post-Pay customer.")');

// Assert that the Auto Top-Up AJAX action results in an error message explaining that auto top-up is disabled due to post-pay.
testPostAjaxRequest($b, $autoTopUpAJAXUrl, 200, array(), false, 'Auto Top-Up is disabled for your account.');

// Assert that the Auto Top-Up and Purchase Credit menu links are not present.
$b->get($autoTopUpUrl)
    ->with('response')->begin()
        ->checkElement('a[href$="/s/Purchase-Credit"]', false)
        ->checkElement('a[href$="' . $autoTopUpUrl. '"]', false)
    ->end();
