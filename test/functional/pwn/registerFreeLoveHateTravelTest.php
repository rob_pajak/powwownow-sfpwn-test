<?php
/**
 * This is the Test for the Free PIN Registration of the Remote Working Page
 * @author Asfer Tamimi
 */
require_once 'test/bootstrap/functional.php';

// Free PIN Registration Variable Over-rides
$pageIds = array(
    'page'                => '/Love-Hate-Travel',
    'registration_source' => 'GBR-Love-Hate-Travel',
    'registration_type'   => 'lp',
);

// Page Elements Should Exist
$pageElements = array(
    'h1.tab_page_heading',
    'div.subheading',
    'div#left-content-container',
    'div#landingpage-tabs-container'
);

// Setup the Browser for the Test, And check a Successful Response
$b->get($pageIds['page']);
$b->with('response')->isStatusCode(200);

// Test the Initial Page
freePinTestPageElements($b, $pageElements);

// Test the Initial Form Error Tests
$response = freePinTestInitialForm($b, $pageIds);

// Post a Successful Request
$response = freePinTestSuccessForm($b, $pageIds);

