<?php
/**
 * This is the Test for the User Details Page (myPwn/User-Details OR myPwn/User-Details?c=xxxxxx)
 * @author Asfer Tamimi
 */

$firstName            = 'User';
$lastName             = 'Test';
$userDetailsAjaxRoute = '/myPwn/User-Details-Contact-Ajax';
$passwordAjaxRoute    = '/myPwn/User-Details-Password-Ajax';

// Enhanced Login
require_once 'loginEnhancedUser.php';
noAccessTest($b);

// Plus User Login
require_once 'loginPlusUser.php';
noAccessTest($b);

// Plus Admin Login
require_once 'loginPlusAdmin.php';
noAccessTest($b);

// Premium User Login
require_once 'loginPremiumUser.php';
noAccessTest($b);

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/userDetails')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'userDetails')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'User Details')
            ->checkElement('h1.mypwn_sub_page_heading_premium')

            // User Details Form
            ->checkElement('form#frm_user_details_user_details')
            ->checkElement('input#contact_title')
            ->checkElement('input#contact_first_name')
            ->checkElement('input#contact_last_name')
            ->checkElement('input#contact_middle_initials')
            ->checkElement('input#contact_business_phone')
            ->checkElement('input#contact_mobile_phone')
            ->checkElement('select#contact_country')
            ->checkElement('input#contact_company')
            ->checkElement('form#frm_user_details_user_details button','SAVE')

            // Password Form
            ->checkElement('form#frm_user_details_user_password')
            ->checkElement('input#authentication_password')
            ->checkElement('input#authentication_confirm_password')
            ->checkElement('form#frm_user_details_user_password button','SAVE')
        ->end();

// Ajax Testing

// No First Name Error Message Test
$args = array('contact' => array('first_name' => '', 'last_name' => ''));
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_NO_FIRST_NAME');

// No Last Name Error Message Test
$args = array('contact' => array('first_name' => 'Test', 'last_name' => ''));
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_NO_LAST_NAME');

// First Name Too Long Error Message Test
$args = array('contact' => array('first_name' => '12345678901234567890123456789012', 'last_name' => ''));
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_INVALID_CONTACT_FIRST_NAME');

// Last Name Too Long Error Message Test
$args = array('contact' => array('first_name' => 'Test', 'last_name' => '12345678901234567890123456789012'));
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_INVALID_CONTACT_LAST_NAME');

// Title Too Long Error Message Test
$args = array('contact' => array('title' => '12345678901', 'first_name' => 'Test', 'last_name' => 'Test'));
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_INVALID_CONTACT_TITLE');

// Middle Initials Too Long Error Message Test
$args = array('contact' => array('middle_initials' => '12345678901', 'first_name' => 'Test', 'last_name' => 'Test'));
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_INVALID_MIDDLE_INITIALS');

// Business Phone Number Too Long Error Message Test
$args = array(
    'contact' => array(
        'business_phone' => '123456789012345678901',
        'first_name'     => 'Test',
        'last_name'      => 'Test'
    )
);
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_INVALID_PHONE_NUMBER');

// Mobile Phone Number Too Long Error Message Test
$args = array(
    'contact' => array(
        'mobile_phone' => '123456789012345678901',
        'first_name'   => 'Test',
        'last_name'    => 'Test'
    )
);
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'INVALID_MOBILE_NUMBER');

// Invalid Country Chosen Error Message Test
$args = array('contact' => array('country' => 'invalid country', 'first_name' => 'Test', 'last_name' => 'Test'));
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_INVALID_CONTACT_COUNTRY');

// Company Name Too Long Error Test
$args = array(
    'contact' => array(
        'company'    => '123456789012345678901234567890123456789012345678901',
        'first_name' => 'Test',
        'last_name'  => 'Test'
    )
);
testPostAjaxRequest($b, $userDetailsAjaxRoute, 500, $args, false, 'FORM_VALIDATION_NO_COMPANY_NAME');

// Successful User Details Post
$args = array(
    'contact' => array(
        'first_name'      => 'Test',
        'last_name'       => 'Test',
        'title'           => '',
        'middle_initials' => '',
        'business_phone'  => '',
        'mobile_phone'    => '',
        'country'         => 'GBR',
        'company'         => ''
    )
);
testPostAjaxRequest($b, $userDetailsAjaxRoute, 200, $args, false, false);

// No Password Error Message Test
$args = array('authentication' => array('password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $passwordAjaxRoute, 500, $args, false, 'FORM_VALIDATION_NO_PASSWORD');

// No Confirm Password Error Message Test
$args = array('authentication' => array('password' => '123456', 'confirm_password' => ''));
testPostAjaxRequest($b, $passwordAjaxRoute, 500, $args, false, 'FORM_VALIDATION_NO_PASSWORD');

// Short Password Error Message Test
$args = array('authentication' => array('password' => '123', 'confirm_password' => ''));
testPostAjaxRequest($b, $passwordAjaxRoute, 500, $args, false, 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS');

// Short Confirm Password Error Message Test
$args = array('authentication' => array('password' => '123456', 'confirm_password' => '123'));
testPostAjaxRequest($b, $passwordAjaxRoute, 500, $args, false, 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS');

// Long Password Error Message Test
$args = array(
    'authentication' => array(
        'password'         => '123456789012345678901234567890123456789012345678901',
        'confirm_password' => '123456'
    )
);
testPostAjaxRequest($b, $passwordAjaxRoute, 500, $args, false, 'FORM_VALIDATION_PASSWORD_TOO_LONG');

// Long Confirm Password Error Message Test
$args = array(
    'authentication' => array(
        'password'         => '123456789012345678901234567890123456789012345678901',
        'confirm_password' => '123456789012345678901234567890123456789012345678901'
    )
);
testPostAjaxRequest($b, $passwordAjaxRoute, 500, $args, false, 'FORM_VALIDATION_PASSWORD_TOO_LONG');

// Password Mismatch Error Message Test
$args = array('authentication' => array('password' => '123456', 'confirm_password' => '1234561'));
testPostAjaxRequest($b, $passwordAjaxRoute, 500, $args, false, 'FORM_VALIDATION_PASSWORD_MISMATCH');

// Successful User Details Post
$args = array('authentication' => array('password' => 'r1chm0nd', 'confirm_password' => 'r1chm0nd'));
testPostAjaxRequest($b, $passwordAjaxRoute, 200, $args, false, false);

/**
 * Check for 404 User Access Failure
 * @param Object $b
 */
function noAccessTest ($b) {
    $b->get('/myPwn/User-Details')
        ->with('request')
            ->begin()
                ->isParameter('module', 'mypwn')
                ->isParameter('action', 'userDetails')
            ->end()
        ->with('response')
            ->begin()
                ->isStatusCode(200)
                ->checkElement('h1', 'No Access')
                ->checkElement('h2','You do not have permission to access this page!')
            ->end();
}
