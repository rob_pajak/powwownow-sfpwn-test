<?php

/**
 * Assert that Premium users and admins are shown read-only data regarding their company.
 * @todo Full User Tests Required
 */

require_once 'test/bootstrap/functional.php';

// Test as a Premium user.
$premiumUserCredentials = $b->args('doPremiumUserLogin.php');
loginUser($b, $premiumUserCredentials['email'], $premiumUserCredentials['password']);
assertTextsExistsOnPage($b, '/myPwn/Account-Details', array(
    // Check for the company header.
    'My Company',
    // Check for the Mnemonic label.
    'Mnemonic',
));
logoutUser($b);

// Test as a Premium admin.
$premiumAdminCredentials = $b->args('doPremiumAdminLogin.php');
loginUser($b, $premiumAdminCredentials['email'], $premiumAdminCredentials['password']);
assertTextsExistsOnPage($b, '/myPwn/Account-Details', array(
    // Check for the company header.
    'My Company',
    // Check for the Mnemonic label.
    'Mnemonic',
));
logoutUser($b);
