<?php
/**
 * This is the Test for the New Call History Test
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 *
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/callHistory')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'callHistory')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Call History')
            ->checkElement('h1.mypwn_sub_page_heading_powwownow')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/callHistory')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'callHistory')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Call History')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/callHistory')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'callHistory')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Call History')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/callHistory')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'callHistory')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Call History')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/callHistory')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'callHistory')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Call History')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('form#frm-callhistory')
            ->checkElement('select#callhistory_service')
            ->checkElement('select#callhistory_called_number')
            ->checkElement('select#callhistory_country')
            ->checkElement('input#callhistory_pin_type')
            ->checkElement('input#callhistory_from_date')
            ->checkElement('input#callhistory_to_date')
            ->checkElement('input#callhistory_callers_number')
            ->checkElement('select#callhistory_report')
            ->checkElement('input#callhistory_include_unsuccessful_calls')
            ->checkElement('input#callhistory_show_all_account')
            ->checkElement('button#frm-callhistory-submit','Run Report')
            ->checkElement('div.div-callhistory')
            ->checkElement('table#tblCallHistory')
            ->checkElement('table.mypwn')
            ->checkElement('table.premium')
            ->checkElement('button#frm-callhistory-csv','Download Report')
        ->end();


$routeCallhistory = '/s/Call-History-Ajax';

$args = array('callhistory' => array('service' => '273', 'called_number' => '', 'country' => '', 'pin_type' => '', 'from_date' => '', 'to_date' => '2013-03-19', 'callers_number' => '', 'report' => 'detail'));
testPostAjaxRequest($b, $routeCallhistory, 500, $args, false, 'FORM_VALIDATION_INVALID_FROM_DATE');

$args = array('callhistory' => array('service' => '273', 'called_number' => '', 'country' => '', 'pin_type' => '', 'from_date' => '2013-03-12', 'to_date' => '', 'callers_number' => '', 'report' => 'detail'));
testPostAjaxRequest($b, $routeCallhistory, 500, $args, false, 'FORM_VALIDATION_INVALID_TO_DATE');

$args = array('callhistory' => array('service' => '273', 'called_number' => '', 'country' => '', 'pin_type' => '', 'from_date' => '2013-03-12', 'to_date' => '2013-03-19', 'callers_number' => '', 'report' => 'detail'));
testPostAjaxRequest($b, $routeCallhistory, 200, $args, false, false);