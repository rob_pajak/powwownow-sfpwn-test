<?php
/**
 * Makes assertions about the Call Minute Bundles functionality.
 *
 * @author Asfer Tamimi
 */

require_once 'test/bootstrap/functional.php';

// Introduction Test
$b->info('Testing Introduction Text Appears');
$b->get('/Conference-Call/Call-Minute-Bundles');
//    ->with('response')->begin()
//        ->checkElement('div.slider-introduction-content p','Find out how much you could be saving by simply using the sliders to make your selection and calculate.')
//    ->end();

// No Bundle Test - Does not like me pressing the Non Form Button
//$b->get('/Conference-Call/Call-Minute-Bundles')
//    ->with('response')->begin()->isStatusCode(200)
//        ->click('#btn-calculate')
//        ->checkElement('div.close-control')
//        ->click('a.tooltip-close')
//    ->end();

$testArray = array(
    'Landline 1000 Bundle' => array(
        'people' => 5,
        'conferences' => 6,
        'minutes' => 20,
        'bundle_id' => 223
    ),
    'Landline 2500 Bundle' => array(
        'people' => 5,
        'conferences' => 18,
        'minutes' => 20,
        'bundle_id' => 225
    ),
    'Landline 5000 Bundle' => array(
        'people' => 10,
        'conferences' => 20,
        'minutes' => 20,
        'bundle_id' => 227
    ),
    'AYCM Bundle' => array(
        'people' => 5,
        'conferences' => 18,
        'minutes' => 20,
        'bundle_id' => 229
    )
);

foreach ($testArray as $bundleName => $bundleInfo) {
    $b->info('Testing ' . $bundleName);

    $argsStr  = 'people='.$bundleInfo['people'];
    $argsStr .= '&conferences='.$bundleInfo['conferences'];
    $argsStr .= '&minutes='.$bundleInfo['minutes'];
    $b->get('/Bundles-Selector-Tool/store?' . $argsStr);

    $response = json_decode($b->getResponse()->getcontent(),true);
    $bundleId = (isset($response['id'])) ? $response['id'] : false;

    $b->test()->ok($bundleId, false, 'Bundle ID Check Failed. Response: ' . print_r($response,true));
}