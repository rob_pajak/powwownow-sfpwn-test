<?php

// Assert that the purchased Landline bundle can be reassigned on the "Assign Users" page,

require_once 'test/bootstrap/functional.php';

// create new plus account
$registrationData = $b->args('doPlusRegistrationRegister.php', array('email' => 'bundler'));
$registrationResponse = Hermes_Client_Rest::call('doPlusRegistrationRegister', $registrationData);

// login
$b->info("login user {$registrationData['email']} with password {$registrationData['password']}")
    ->post('/s/test/login', array('user_email' => $registrationData['email'], 'user_password' => $registrationData['password']))
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->end();


//Hermes_Client_Rest::call(
//    'Default.doPlusAddContactAndPins',
//    array(
//        'account_id'    => $registrationResponse['account']['id'],
//        'email'         => 'bundleTest.user-notAssigned+'.uniqid().'@test.pl',
//        'first_name'    => 'test',
//        'last_name'     => 'test',
//        'password'      => 'test123',
//        'source'        => 'PHPUNIT',
//        'locale'        => 'en_GB',
//        'service_ref'   => '850',
//
//    )
//);
