<?php

/**
 * Assert that the new product activation flow is similar to a basket flow.
 * Assert that multiple BWMs can be created in a single flow.
 */

// Create a new user (and a new session).
require_once 'registerPlusUserTest.php';
$b->args('doPlusRegistrationRegister.php');

// Add 2 products to the basket:
// - Worldwide Freephone Numbers
// - UK Landline Number
$b->post('/s/Add-To-Basket', array('add-product' => 3))
    ->post('/s/Add-To-Basket', array('add-product' => 6))
    // View the basket page.
    ->get('/s/Basket')
        ->with('response')
            ->isStatusCode(200)
        ->with('response')
            // Assert that Worldwide Freephone Numbers has been added to the Basket.
            ->matches('/Worldwide Freephone Numbers/')
        ->with('response')
            // Assert that UK Landline Number has been added to the basket
            ->matches('/UK Landline Number/');

// Testing adding multiple BWMs.
// -> Create a new user for basket testing.
$b->args('doPlusRegistrationRegister.php');

// 1. Add one BWM.
$bwmOneName = 'BWM Product -1';
configureBWM($b, array(
    'step-3' => array(
        'bwm_label' => $bwmOneName,
    ),
));
// - Assert that the basket contains only one BWM with ID -1.
assertBasketHasOnlyKeys(array(-1));
// - Reload Select-Products; assert that one new BWM row has been added.
assertTextExistsOnPage($b, '/s/Select-Products', $bwmOneName);
// - Reload Basket page; assert that one new BWM row has been added.
assertTextExistsOnPage($b, '/s/Basket', $bwmOneName);

// 2. Add another BWM.
$bwmTwoName = 'BWM Product -2';
configureBWM($b, array(
    'step-3' => array(
        'bwm_label' => $bwmTwoName,
    ),
));
// - Assert that the basket contains only BWMs with ID -1, and -2.
assertBasketHasOnlyKeys(array(-1, -2));
// - Reload Select-Products; assert that two new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Select-Products', array($bwmOneName, $bwmTwoName));
// - Reload Basket page; assert that two new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Basket', array($bwmOneName, $bwmTwoName));

// 3. Add another BWM.
$bwmThreeName = 'BWM Product -2';
configureBWM($b, array(
    'step-3' => array(
        'bwm_label' => $bwmThreeName,
    ),
));
// - Assert that the basket contains only BWMs with ID -1, -2 and -3.
assertBasketHasOnlyKeys(array(-1, -2, -3));
// - Reload Select-Products; assert that three new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Select-Products', array($bwmOneName, $bwmTwoName, $bwmThreeName));
// - Reload Basket page; assert that three new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Basket', array($bwmOneName, $bwmTwoName, $bwmThreeName));

// 4. Remove BWM with ID -2.
removeProductFromBasket($b, -2);
// - Assert that the basket contains only BWMs with ID -1 and -3.
assertBasketHasOnlyKeys(array(-1, -3));
// - Reload Select-Products; assert that two new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Select-Products', array($bwmOneName, $bwmThreeName));
// - Reload Basket page; assert that two new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Basket', array($bwmOneName, $bwmThreeName));

// 5. Try to remove a BWM with ID -5.
removeProductFromBasket($b, -5);
// - Assert that the basket contains only BWMs with ID -1 and -3.
assertBasketHasOnlyKeys(array(-1, -3));
// - Reload Select-Products; assert that two new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Select-Products', array($bwmOneName, $bwmThreeName));
// - Reload Basket page; assert that two new BWM rows has been added.
assertTextsExistsOnPage($b, '/s/Basket', array($bwmOneName, $bwmThreeName));
