<?php

require_once 'test/bootstrap/functional.php';

testLetsGetItDoneArticleElements($b);
testLetsGetItDoneRegistration($b, '/Free-Pin-Registration');

/**
 * @param sfPwnPlusFunctionalTest $sfPwnPlusFunctionalTest
 */
function testLetsGetItDoneArticleElements(sfPwnPlusFunctionalTest $sfPwnPlusFunctionalTest)
{
    $sfPwnPlusFunctionalTest
        ->get('/Lets-Get-It-Done')
            ->with('request')->begin()
        ->end()
        ->with('response')->begin()
            ->isStatusCode(200)
            ->checkElement('div#get-it-done')
            ->checkElement('article.hero-element')
            ->checkElement('article.get-started')
            ->checkElement('article.service-details')
            ->checkElement('article.clients')
            ->checkElement('article.footer')
        ->end();
}

/**
 * @param sfPwnPlusFunctionalTest $sfPwnPlusFunctionalTest
 * @param $initialAjaxRoute
 */
function testLetsGetItDoneRegistration(sfPwnPlusFunctionalTest $sfPwnPlusFunctionalTest, $initialAjaxRoute)
{
    // Check that we are on the right page
    $sfPwnPlusFunctionalTest
        ->get('/Lets-Get-It-Done')
            ->with('request')->begin()
            ->isParameter('module', 'pages')
            ->isParameter('action', 'letsGetItDoneLandingPage')
        ->end()
            ->with('response')->begin()
            ->isStatusCode(200)
        ->end();

    // Existing Email Address
    $args = array('email' => 'mark.wiseman+pl@powwownow.com');
    testPostAjaxRequest(
        $sfPwnPlusFunctionalTest,
        $initialAjaxRoute,
        400,
        $args,
        false,
        'API_RESPONSE_EMAIL_ALREADY_TAKEN_RESPONSIVE_OTHER'
    );

    // New Email Address
    $registrationEmail = 'pwn_test+' . uniqid() . '@powwownow.com';
    testPostAjaxRequest(
        $sfPwnPlusFunctionalTest,
        $initialAjaxRoute,
        200,
        array(
            'email' => $registrationEmail,
            'agree_tick' => 'agreed'
        ),
        false,
        false
    );
}