<?php
/**
 * This is the Test for the God Mode Functionality
 * @author Asfer Tamimi
 */
require_once 'test/bootstrap/functional.php';

// Email parameters:
$remoteAccessParameters = array(
    96204  => 'juha.jantunen+enhanced@powwownow.com', // Free user
    312011 => 'mark.wiseman+user@powwownow.com', // Plus user
    292069 => 'mark.wiseman+pl@powwownow.com', // Plus admin
    100139 => 'juha.jantunen+premium@powwownow.com', // Premium user
    94498  => 'stewart.millard@powwownow.com', // Premium admin
);
$requiredExistingKeys   = array(
    'contact_ref',
    'service_ref',
    'first_name',
    'last_name',
    'account_id',
    'user_type',
    'service_type',
);

// Assert successful ajax response.
$successPassword = 'parad15e';
$failurePassword = 'not-parad1se';

// Ajax Page
$loginGodModeAjax = '/ajax/mypwn/loginGodMode';

foreach ($remoteAccessParameters as $contactRef => $email) {
    unset($_SESSION);
    $_SESSION = array();

    $successArgs = array(
        'loginEmail'            => $email,
        'contact_ref'           => $contactRef,
        'god_mode_password'     => $successPassword,
        'accessorUserReference' => '1585'
    );

    $failArgs = array(
        'loginEmail'        => $email,
        'god_mode_password' => $failurePassword
    );

    testPostAjaxRequest($b, $loginGodModeAjax, 200, $successArgs, false, false);
    testPostAjaxRequest($b, $loginGodModeAjax, 403, $failArgs, false, 'FORM_VALIDATION_WRONG_PASSWORD');
}