<?php
/**
 * This is the Test for the New Contact Us Page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 *
 */
require_once 'test/bootstrap/functional.php';
$routeContactus     = '/Contact-Us-Ajax';
$uniqueEmailAddress = 'test+' . uniqid() . '@powwownow.com';

$b->get('/s/pages/contactUs')
    ->with('request')
        ->begin()
            ->isParameter('module', 'pages')
            ->isParameter('action', 'contactUs')
        ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Contact Us')
            ->checkElement('h1.mypwn_sub_page_heading_powwownow')
            ->checkElement('h3.lightgreen','Powwownow')
            ->checkElement('form#frm-contact-us')
            ->checkElement('a.chatlink')
            ->checkElement('input#contactus_first_name')
            ->checkElement('input#contactus_last_name')
            ->checkElement('input#contactus_company')
            ->checkElement('input#contactus_contact_number')
            ->checkElement('input#contactus_email')
            ->checkElement('input#contactus_confirm_email')
            ->checkElement('textarea#contactus_comments')
            ->checkElement('button#contactus-submit','SEND')
        ->end();

// Contact Us Tests
$args = array('contactus' => array('contact_number' => '', 'first_name' => '', 'last_name' => '', 'email' => '', 'confirm_email' => '', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_NO_FIRST_NAME');

$args = array('contactus' => array('contact_number' => '', 'first_name' => 'asd', 'last_name' => '', 'email' => '', 'confirm_email' => '', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_NO_SURNAME');

$args = array('contactus' => array('contact_number' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => '', 'confirm_email' => '', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_INVALID_PHONE_NUMBER');

$args = array('contactus' => array('contact_number' => '123123123', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => '', 'confirm_email' => '', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_NO_EMAIL');

$args = array('contactus' => array('contact_number' => '123123123', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => 'asd', 'confirm_email' => '', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

$args = array('contactus' => array('contact_number' => '123123123', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddress, 'confirm_email' => '', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_NO_EMAIL');

$args = array('contactus' => array('contact_number' => '123123123', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddress, 'confirm_email' => 'asd', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

$args = array('contactus' => array('contact_number' => '123123123', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddress, 'confirm_email' => $uniqueEmailAddress . 'a', 'comments' => ''));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_NO_COMMENTS');

$args = array('contactus' => array('contact_number' => '123123123', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddress, 'confirm_email' => $uniqueEmailAddress . 'a', 'comments' => 'asd'));
testPostAjaxRequest($b, $routeContactus, 500, $args, false, 'FORM_VALIDATION_EMAIL_MISMATCH');

$args = array('contactus' => array('contact_number' => '123123123', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddress, 'confirm_email' => $uniqueEmailAddress, 'comments' => 'asd'));
testPostAjaxRequest($b, $routeContactus, 200, $args, false, false);
