<?php
/**
 * Asserts that newly created Plus users are automatically assigned to all purchased:
 *  - Feature Products (these are not purchased, but automatically assigned),
 *  - BWMs, and,
 *  - Landline Bundles (i.e. all Bundles except AYCM bundle).
 *
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

// Set up: all feature product group ids, and bundle product group ids.
$featureIds = retrieveAllFeatureProductGroupIds();
$bundleIds = retrieveAllBundleProductGroupIds();

// Set up: new Plus admin with one user.
require_once 'registerPlusUserTest.php';

/*
 * Test with a Plus admin whom has feature products enabled.
 */
$userEmail = createNewPlusUser($b);
$userContactRef = retrieveContactRefByEmail($userEmail);
testUserIsAssignedToProducts($b, $userContactRef, $featureIds);

/*
 * Test with a Plus admin whom has a BWM.
 *
 * @note: Due to the need of having dedicated DNISes, this is not tested by Lime.
 */

/*
 * Test with a Plus admin whom has a Landline Bundle.
 */
$landlineId = $bundleIds['landline'][0];
assignBundleToUser($landlineId);
$userEmail = createNewPlusUser($b);
$userContactRef = retrieveContactRefByEmail($userEmail);
testUserIsAssignedToProducts($b, $userContactRef, array($landlineId));

/*
 * Test with a Plus admin whom has an AYCM Bundle.
 * Create a new Plus admin for this test, because we cannot have 2 bundles simultaneously.
 */
logoutUser($b);
require 'registerPlusUserTest.php';
$aycmId = $bundleIds['individual'][0];
assignBundleToUser($aycmId);
$userEmail = createNewPlusUser($b);
$userContactRef = retrieveContactRefByEmail($userEmail);
testUserIsNotAssignedToProducts($b, $userContactRef, array($aycmId));
