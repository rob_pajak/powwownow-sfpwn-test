<?php
/**
 * This is the Test for the /Its-Your-Call-A and /Its-Your-Call-Welcome
 * @author Asfer Tamimi
 *
 */
require_once 'test/bootstrap/functional.php';

$itsYourCallPage             = '/Its-Your-Call';
$freePinRegistrationAjax     = '/Free-Pin-Registration';
$welcomePackRegistrationAjax = '/Its-Your-Call-Welcome-Ajax';

$b->info('### Initial Its Your Call Page ###');
$b->get($itsYourCallPage)
    ->with('request')
        ->begin()
            ->isParameter('module', 'pages')
            ->isParameter('action', 'itsYourCallA')
        ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('section#its-your-call-get-pin-container')
    ->end();

// Existing Email Address Test
$args = array('email' => 'mark.wiseman+pl@powwownow.com');
testPostAjaxRequest($b, $freePinRegistrationAjax, 400, $args, false, 'API_RESPONSE_EMAIL_ALREADY_TAKEN');

// New Email Address
$args = array(
    'email'               => 'test+Its-Your-Call' . uniqid() . '@powwownow.com',
    'registration_source' => 'Its-Your-Call',
    'registration_type'   => 'iyc',
    'agree_tick'          => 'agreed'
);
$argsFirstForm = $args;
testPostAjaxRequest($b, $freePinRegistrationAjax, 200, $args, false, false);

/*Welcome Pack Page*/
$b->info('### Its Your Call Welcome Pack Request Page ###');
$argsOrig = array(
    'requestWelcomePack' => array(
        'first_name'      => 'FName',
        'last_name'       => 'LName',
        'company'         => 'Company',
        'address'         => 'Address',
        'town'            => 'Town',
        'county'          => 'Surrey',
        'postcode'        => 'WE11EW',
        'country'         => 'GBR',
        'password'        => '123123',
        'password_retype' => '123123',
        'question'        => 'No'
    )
);

// First Name Missing
$args = $argsOrig;
$args['requestWelcomePack']['first_name'] = '';
testPostAjaxRequest($b, $welcomePackRegistrationAjax, 400, $args, false, 'FORM_VALIDATION_NO_FIRST_NAME');

// Last Name Missing
$args = $argsOrig;
$args['requestWelcomePack']['last_name'] = '';
testPostAjaxRequest($b, $welcomePackRegistrationAjax, 400, $args, false, 'FORM_VALIDATION_NO_SURNAME');

// Address Missing
$args = $argsOrig;
$args['requestWelcomePack']['address'] = '';
testPostAjaxRequest($b, $welcomePackRegistrationAjax, 400, $args, false, 'FORM_VALIDATION_NO_ADDRESS');

// Town Missing
$args = $argsOrig;
$args['requestWelcomePack']['town'] = '';
testPostAjaxRequest($b, $welcomePackRegistrationAjax, 400, $args, false, 'FORM_VALIDATION_NO_TOWN');

// Postcode Missing or Incorrect
$args = $argsOrig;
$args['requestWelcomePack']['postcode'] = '';
testPostAjaxRequest($b, $welcomePackRegistrationAjax, 400, $args, false, 'FORM_VALIDATION_INVALID_POSTCODE');

// Successful Form Request
$args = $argsOrig;
testPostAjaxRequest($b, $welcomePackRegistrationAjax, 200, $args, false, false);

/*Third Form*/
$b->info('### Its Your Call Welcome Pack Ordered Page ###');
$b->get('/Its-Your-Call-Welcome-Ordered')
    ->with('request')->begin()
        ->isParameter('module', 'pages')
        ->isParameter('action', 'itsYourCallStepB')
    ->end();
