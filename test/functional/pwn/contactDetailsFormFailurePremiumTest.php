<?php

/**
 * Assert that the Contact Details form allows manipulation of the Premium admin/user contact, and fails if the input is incorrect.
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

// Test as a Premium user.
$premiumUserCredentials = getPremiumUserWithContactRef($b);
loginUser($b, $premiumUserCredentials['email'], $premiumUserCredentials['password']);
testContactDetailsFormFailure(
    $b,
    'Testing the Contact details form failure as a Premium user.',
    'pr_user_f ',
    array(
        'contact_ref' => $premiumUserCredentials['contact_ref']
    )
);
logoutUser($b);

// Test as a Premium admin.
$premiumAdminCredentials = getPremiumAdminWithContactRef($b);
loginUser($b, $premiumAdminCredentials['email'], $premiumAdminCredentials['password']);
testContactDetailsFormFailure(
    $b,
    'Testing the Contact details form failure as a Premium admin.',
    'pr_admin_f ',
    array(
        'contact_ref' => $premiumAdminCredentials['contact_ref']
    )
);
logoutUser($b);
