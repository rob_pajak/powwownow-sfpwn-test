<?php

// Assert that when the basket contains a Bundle, Call Credit cannot be added to basket,

require_once 'test/bootstrap/functional.php';

// create new plus account
$registrationData = $b->args('doPlusRegistrationRegister.php', array('email' => 'bundler'));
$registrationResponse = Hermes_Client_Rest::call('doPlusRegistrationRegister', $registrationData);

// login
$b->info("login user {$registrationData['email']} with password {$registrationData['password']}")
    ->post('/s/test/login', array('user_email' => $registrationData['email'], 'user_password' => $registrationData['password']))
    ->with('response')
        ->begin()
            ->isStatusCode(200)
        ->end();

// test
$b->get('/myPwn/Products')
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('#bundle-row-5000 button[class="button-green bundle-configure "]')
            ->checkElement('#bundle-row-5000 button.button-green.bundle-configure.not-show', false)
        ->end();

// add bundle to the basket
$productsDOM = $b->getResponseDom();
$bundle5000ProductId = $productsDOM->getElementById('add-5000')->getAttribute('value');
$bundleArr = array('add-bundle' => $bundle5000ProductId, 'bundle-tc' => '1');
testPostAjaxRequest($b, '/s/Add-Bundle-To-Basket-Ajax', 200, $bundleArr, false, false);
assertBasketHasOnlyKeys(array($bundle5000ProductId));

// test
$b->get('/myPwn/Products')
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            //->checkElement('#bundle-row-5000 button.button-green.bundle-configure.not-show')
            ->checkElement('.credit-row #credit-disabled-button')
        ->end();

