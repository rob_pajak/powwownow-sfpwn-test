<?php

/**
 * Asserts that the Call History page displays the DNIS type of every call the user has made.
 *
 * This test is disabled for the time being, because retrieving the conference IDs takes too long, so the request to
 * Hermes will time out.
 *
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

$pinRetriever = new PinRetriever();
$nextIds = Hermes_Client_Rest::call('retrieveNextConferenceIds', array());
// @todo: Change app_callGenerator_dev to the configuration for the current environment.
$callGenerator = new ConferenceCallGenerator(sfConfig::get('app_callGenerator_dev'), $nextIds['conference_seq'], $nextIds['server_seq']);

function assertNoDNISTypeColumn(sfPwnPlusFunctionalTest $b)
{
    $b->info('Asserting that the DNIS Type column is not shown for user with no calls.')
        ->get('/myPwn/Call-History')
        ->with('response')->begin()
            // The string that should NOT be in the body is a part of JS array:
            // If the user did have calls there would be an array for the datatable, containing the following values.
            ->checkElement('body', '!/"Shared Cost","GBR","00:01:00"/')
        ->end();
}

function assertDNISTypeColumn(sfPwnPlusFunctionalTest $b)
{
    $b->info('Asserting that the DNIS Type column is shown for user with calls.')
        ->get('/myPwn/Call-History')
        ->with('response')->begin()
            // The string that should be in the body is a part of JS array:
            // If the user did have calls there would be an array for the datatable, containing the following values.
            ->checkElement('body', '/"Shared Cost","GBR","00:01:00"/')
        ->end();
}

// Assert that newly created Enhanced users have no calls, and thus no DNIS type is visible.
registerNewEnhancedUser($b);
assertNoDNISTypeColumn($b);

// Assert that Enhanced users with calls have the DNIS type set.
$contactPins = $pinRetriever->getAccountPinsByContactRef(sfContext::getInstance()->getUser()->getAttribute('contact_ref'));
$firstPin = array_shift($contactPins);
$b->info("Generating a call for pin {$firstPin['pin']}");
$callResult = $callGenerator->generateConferenceCall($firstPin['pin'], 801, time() - 3600, time() + 60 - 3600);
assertDNISTypeColumn($b);

// Assert that newly created Plus users have no calls, and thus no DNIS type is visible.
require_once 'registerPlusUserTest.php';
assertNoDNISTypeColumn($b);

// Assert that Plus users with calls have the DNIS type set.
$contactPins = $pinRetriever->getAccountPinsByContactRef(sfContext::getInstance()->getUser()->getAttribute('contact_ref'));
$firstPin = array_shift($contactPins);
$b->info("Generating a call for pin {$firstPin['pin']}");
$callResult = $callGenerator->generateConferenceCall($firstPin['pin'], 850, time() - 3600, time() + 60 - 3600);
assertDNISTypeColumn($b);
