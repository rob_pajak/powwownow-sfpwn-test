<?php
/**
 * Tests the home page web conferencing page
 *
 * The page uses backbone templates there is not much testing we can do with the form
 *
 * @author Wiseman
 */
require_once 'test/bootstrap/functional.php';

$b->get('/s/homepageG/webConferencing')
    ->with('request')
    ->begin()
        ->isParameter('module', 'homepageG')
        ->isParameter('action', 'webConferencing')
    ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Web Conferencing')
            ->checkElement('h2', 'Web Conferencing Demo')
            ->checkElement('h3', 'PIN Reminder')
            ->checkElement('h4', 'Compare')
            ->checkElement('script#top-panel-login-form-template')
            ->checkElement('ul.site-links')
        ->end();
