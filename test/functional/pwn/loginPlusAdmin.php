<?php
/**
 * This is to login an Plus Admin User
 * @author Asfer Tamimi
 */
require_once 'test/bootstrap/functional.php';

// Obtain Plus Admin Credentials
$credentials = $b->args('doPlusAdminLogin.php');

// Login
$b->info('login user ' . $credentials['email'] . ' with password ' . $credentials['password'])
    ->post('/s/test/login', array('user_email' => $credentials['email'], 'user_password' => $credentials['password']))
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->end();
