<?php
/**
 * This is the Test for the New Login / Create-A-Login Page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 *
 */
require_once 'test/bootstrap/functional.php';
$routeLogin = '/Login-Ajax';
$routeCreateALogin = '/Create-A-Login-Ajax';
$uniqueEmailAddressLogin = 'test+' . uniqid() . '@powwownow.com';

// Login Tests
$args = array('login' => array('redirect' => '', 'email' => '', 'password' => ''));
testPostAjaxRequest($b, $routeLogin, 500, $args, false, 'FORM_VALIDATION_NO_EMAIL');

$args = array('login' => array('redirect' => '', 'email' => 'a', 'password' => ''));
testPostAjaxRequest($b, $routeLogin, 500, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

$args = array('login' => array('redirect' => '', 'email' => 'asfer.tamimi@powwownow.com', 'password' => ''));
testPostAjaxRequest($b, $routeLogin, 500, $args, false, 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS');

$args = array('login' => array('redirect' => '', 'email' => 'asfer.tamimi@powwownow.com', 'password' => '123123'));
testPostAjaxRequest($b, $routeLogin, 500, $args, false, 'FORM_VALIDATION_INVALID_PASSWORD');

$args = array('login' => array('redirect' => '', 'email' => 'asfer.tamimi@powwownow.com', 'password' => 'r1chm0nd'));
testPostAjaxRequest($b, $routeLogin, 200, $args, false, false);

// Create-A-Login Tests
$args = array('createalogin' => array('redirect' => '', 'first_name' => '', 'last_name' => '', 'email' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'FORM_VALIDATION_NO_FIRST_NAME');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => '', 'email' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'FORM_VALIDATION_NO_SURNAME');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'FORM_VALIDATION_NO_EMAIL');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => 'a', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddressLogin, 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddressLogin, 'password' => '123123', 'confirm_password' => ''));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'FORM_VALIDATION_PASSWORD_MISMATCH');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => 'juha.jantunen+enhanced@powwownow.com', 'password' => '123123', 'confirm_password' => '123123'));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'API_RESPONSE_ACCOUNT_ALREADY_CREATED_FOR_REQUESTED_DATA');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => 'juha.jantunen+premium@powwownow.com', 'password' => '123123', 'confirm_password' => '123123'));
testPostAjaxRequest($b, $routeCreateALogin, 500, $args, false, 'API_RESPONSE_EMAIL_TAKEN_AND_NON_STANDARD_PWN');

$args = array('createalogin' => array('redirect' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'email' => $uniqueEmailAddressLogin, 'password' => '123123', 'confirm_password' => '123123'));
testPostAjaxRequest($b, $routeCreateALogin, 200, $args, false, false);
