<?php

/**
 * Assert that the introduction text on the dial-in numbers page is determined by the user type and purchased bundle type.
 *
 * @author Maarten Jacobs
 */

// Create a new plus admin
require 'registerPlusUserTest.php';

// Non-bundle admins should see PAYG related information.
$b->get('/myPwn/Dial-In-Numbers')
    ->with('response')->begin()
        ->checkElement('body', '/Please note that when using a Freephone or Landline number, there needs to be enough available PAYG Call Credit on your account for you and your Participants to join a conference call. The balance of your account is displayed on the top right-hand corner of this page. Need to Top-up?/')
    ->end();
$bundleIds = retrieveBundleProductGroupId($b);

// Non-bundle users should also see PAYG-related information, but it should be advice to contact the admin.
$userEmail = createNewPlusUser($b);
loginUser($b, $userEmail, 'r1chm0nd');
$b->get('/myPwn/Dial-In-Numbers')
    ->with('response')->begin()
        ->checkElement('body', '/Please note that there needs to be enough available credit on your account for you and your Participants to join a conference call using a Freephone or Landline number. The balance of your account is displayed on the top right-hand corner of this page. Contact your Administrator to purchase PAYG Call Credit./')
    ->end();

// Admins and users with a landline bundle assigned should see a reminder.
loginUser($b, $user['email'], $user['password']);
assignBundleToUser($bundleIds['bundle_id']);
$b->get('/myPwn/Dial-In-Numbers')
    ->with('response')->begin()
        ->checkElement('body', "/Don't forget that you have a UK Landline Bundle enabled on your account!/")
    ->end();

loginUser($b, $userEmail, 'r1chm0nd');
$b->get('/myPwn/Dial-In-Numbers')
    ->with('response')->begin()
        ->checkElement('body', "/Don't forget that you have a UK Landline Bundle enabled on your account!/")
    ->end();

// Admins and user with a bundle add-on should see a reminder about their bundle and add-on.
// Create a new plus admin, for the new bundle.
require 'registerPlusUserTest.php';
assignBundleToUser($bundleIds['bundle_upgrade_id']);
$b->get('/myPwn/Dial-In-Numbers')
    ->with('response')->begin()
        ->checkElement('body', "/Don't forget that you have a UK Landline Bundle with an International Landline Add-on enabled on your account!/")
    ->end();

$userEmail = createNewPlusUser($b);
loginUser($b, $userEmail, 'r1chm0nd');
$b->get('/myPwn/Dial-In-Numbers')
    ->with('response')->begin()
        ->checkElement('body', "/Don't forget that you have a UK Landline Bundle with an International Landline Add-on enabled on your account!/")
    ->end();
