<?php
/**
 * Assert that:
 * - The AYCM bundle cannot be re-assigned. All checkboxes in the AYCM bundle are disabled.
 * - The LandLine bundles can be re-assigned.
 *
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';
$bundleIds = retrieveAllBundleProductGroupIds();
$aycmBundleId = $bundleIds['individual'][0];
$landlineBundleId = $bundleIds['landline'][0];

// Assert non-assignment of the AYCM bundle.
require 'registerPlusUserTest.php';
assignBundleToUser($aycmBundleId);
plusCommon::assignAllProductsToAllPinsOfContact(sfContext::getInstance()->getUser()->getAttribute('contact_ref'), array($aycmBundleId));
createNewPlusUser($b);
$b->get('/s/Assign-Products')
    ->with('response')->begin()
        ->checkElement("td.pinproduct-$aycmBundleId input[type=\"checkbox\"][disabled]")
    ->end();

// Assert assignment of LandLine bundles.
require 'registerPlusUserTest.php';
createNewPlusUser($b);
assignBundleToUser($landlineBundleId);
$b->get('/s/Assign-Products')
    ->with('response')->begin()
        // Assert there are checkboxes for the landline bundle product:
        ->checkElement("td.pinproduct-$landlineBundleId input[type=\"checkbox\"]")
        // Assert there are NO disabled checkboxes for the landline bundle product:
        ->checkElement("td.pinproduct-$landlineBundleId input[type=\"checkbox\"][disabled]", false)
    ->end();
