<?php

/**
 * Assertions related to Post-Pay Customers.
 *
 * @author Maarten Jacobs
 */

require_once 'registerPlusUserTest.php';

// Convert the customer.
$bundleIds = retrieveBundleProductGroupId($b);
assignBundleToUser($bundleIds['bundle_id']);

// Assertions for post-pay customers:
$b->get('/myPwn/')->with('response')
        ->checkElement('#minute-allowance', true);
