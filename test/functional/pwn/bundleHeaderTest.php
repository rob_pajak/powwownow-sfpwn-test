<?php
/**
 * Assert that the header is different for the PostPay customers.
 *
 * The PostPay customers should see:
 *  - "Remaining Minutes: [minutes]:[seconds], and,
 *  - if and only if the balance of the user is not 0, it should show the outstanding balance.
 *
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';
$bundleIds = retrieveAllBundleProductGroupIds();
$landlineBundleId = $bundleIds['landline'][0];

// Assert that pre-pay customer do not see the PostPay banner.
require 'registerPlusUserTest.php';
$b->get('/myPwn/Products')
    ->with('response')->begin()
        ->checkElement('.plus-header #minute-allowance', false)
    ->end();


//@Todo Fix this part
//// Assert that the new PostPay customer sees the PostPay banner, but no outstanding balance.
//assignBundleToUser($landlineBundleId);
//$b->get('/myPwn/Products')
//    ->with('response')->begin()
//        ->checkElement('.plus-header #minute-allowance', '/Remaining minutes:/')
//        ->checkElement('.plus-header #outstanding-balance', false)
//    ->end();$b->get('/myPwn/Products')
//    ->with('response')->begin()
//        ->checkElement('.plus-header #minute-allowance', false)
//    ->end();
//
//// Assert that the new PostPay customer sees the PostPay banner, but no outstanding balance.
//assignBundleToUser($landlineBundleId);
//$b->get('/myPwn/Products')
//    ->with('response')->begin()
//        ->checkElement('.plus-header #minute-allowance', '/Remaining minutes:/')
//        ->checkElement('.plus-header #outstanding-balance', false)
//    ->end();
//
//// Assert that the PostPay customer with outstanding balance greater than zero sees the outstanding balance.
//$user = sfContext::getInstance()->getUser();
//Hermes_Client_Rest::call('Default.updatePlusAccountBalance', array('account_id' => $user->getAttribute('account_id'), 'credit_balance_diff' => 20));
//$b->get('/myPwn/Products')
//    ->with('response')->begin()
//        ->checkElement('.plus-header #minute-allowance', '/Remaining minutes:/')
//        ->checkElement('.plus-header #outstanding-balance', '/Outstanding balance:/')
//    ->end();
//
//// Assert that the PostPay customer with outstanding balance less than zero sees the outstanding balance.
//Hermes_Client_Rest::call('Default.updatePlusAccountBalance', array('account_id' => $user->getAttribute('account_id'), 'credit_balance_diff' => -40));
//$b->get('/myPwn/Products')
//    ->with('response')->begin()
//        ->checkElement('.plus-header #minute-allowance', '/Remaining minutes:/')
//        ->checkElement('.plus-header #outstanding-balance', '/Outstanding balance:/')
//    ->end();