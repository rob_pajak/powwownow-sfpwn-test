<?php
/**
 * This is the Test for the New Web Conferencing Page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Since I dont want to Spam the Yuuguu Database for the testing, I am not going to do the Successful test
 */

// Create New Enhanced User + Log them in
require_once 'registerEnhancedUserTest.php';

$routeWebConferencing = '/s/Web-Conferencing-Ajax';

$b->get('/s/mypwn/webConferencing')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'webConferencing')
        ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Web Conferencing')
            ->checkElement('h1.mypwn_sub_page_heading_powwownow')
            ->checkElement('div.webconferencing')
            ->checkElement('div.getstarted')
            ->checkElement('div.downloadicons')
            ->checkElement('div.webConferencingForm')
            ->checkElement('div.formresults')
            ->checkElement('button#downloadpc','Windows PC')
            ->checkElement('button#downloadmac','Apple Macintosh')
        ->end();

$args = array('webConferencing' => array('first_name' => '', 'last_name' => '', 'password' => '', 'filetype' => 'PC'));
testPostAjaxRequest($b, $routeWebConferencing, 500, $args, false, 'FORM_VALIDATION_NO_FIRST_NAME');

$args = array('webConferencing' => array('first_name' => 'asd', 'last_name' => '', 'password' => '', 'filetype' => 'PC'));
testPostAjaxRequest($b, $routeWebConferencing, 500, $args, false, 'FORM_VALIDATION_NO_SURNAME');

$args = array('webConferencing' => array('first_name' => 'asd', 'last_name' => 'asd', 'password' => '', 'filetype' => 'PC'));
testPostAjaxRequest($b, $routeWebConferencing, 500, $args, false, 'FORM_VALIDATION_NO_PASSWORD');

// $args = array('webConferencing' => array('first_name' => 'asd', 'last_name' => 'asd', 'password' => '123123', 'filetype' => 'PC'));
// testPostAjaxRequest($b, $routeWebConferencing, 200, $args, false, false);
