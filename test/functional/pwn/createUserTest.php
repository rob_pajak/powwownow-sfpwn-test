<?php
/**
 * This is the Test for the New Create User Test
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/createUser')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createUser')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/createUser')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createUser')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();


// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/createUser')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createUser')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/createUser')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createUser')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();        

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/createUser')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createUser')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Create User')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('form#form-create-user')
            ->checkElement('input#createUser_email')
            ->checkElement('input#createUser_cost_code')
            ->checkElement('input#createUser_title')
            ->checkElement('input#createUser_first_name')
            ->checkElement('input#createUser_last_name')
            ->checkElement('input#createUser_phone_number')
            ->checkElement('input#createUser_password')
            ->checkElement('input#createUser_confirm_password')
            ->checkElement('input#createUser_notify_user') 
            ->checkElement('button#form-create-user-submit','Create User')
        ->end();

// Ajax Testing
$uniqueEmailAddress = 'test+' . uniqid() . '@powwownow.com';
$routecreateUser    = '/s/Create-User-Ajax';

$args = array('createUser' => array('email' => '', 'cost_code' => '', 'title' => '', 'first_name' => '', 'last_name' => '', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreateUser, 500, $args, false, 'FORM_VALIDATION_NO_EMAIL');

$args = array('createUser' => array('email' => 'asd', 'cost_code' => '', 'title' => '', 'first_name' => '', 'last_name' => '', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreateUser, 500, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

$args = array('createUser' => array('email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => '', 'last_name' => '', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreateUser, 500, $args, false, 'FORM_VALIDATION_NO_FIRST_NAME');

$args = array('createUser' => array('email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => '', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreateUser, 500, $args, false, 'FORM_VALIDATION_NO_SURNAME');

$args = array('createUser' => array('email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreateUser, 500, $args, false, 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS');

$args = array('createUser' => array('email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '123123', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreateUser, 500, $args, false, 'FORM_VALIDATION_PASSWORD_MISMATCH');

$args = array('createUser' => array('email' => 'asfer.tamimi@powwownow.com', 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '123123', 'confirm_password' => '123123'));
testPostAjaxRequest($b, $routecreateUser, 500, $args, false, 'FORM_CREATE_USER_EMAIL_TAKEN');

$args = array('createUser' => array('email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '123123', 'confirm_password' => '123123'));
testPostAjaxRequest($b, $routecreateUser, 200, $args, false, false);