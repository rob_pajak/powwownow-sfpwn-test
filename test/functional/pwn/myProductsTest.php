<?php
/**
 * Tests for the /myPwn/My-Products page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Wiseman
 *
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/myPwn/My-Products')
    ->with('request')
        ->begin()
            ->isParameter('module', 'products')
            ->isParameter('action', 'myProducts')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/myPwn/My-Products')
    ->with('request')
    ->begin()
    ->isParameter('module', 'products')
    ->isParameter('action', 'myProducts')
    ->end()

    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'No Access')
    ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';
$b->get('/myPwn/My-Products')
    ->with('request')
    ->begin()
    ->isParameter('module', 'products')
    ->isParameter('action', 'myProducts')
    ->end()

    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'No Access')
    ->end();



// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/myPwn/My-Products')
    ->with('request')
        ->begin()
        ->isParameter('module', 'products')
        ->isParameter('action', 'myProducts')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'My Products')
            ->checkElement('table#products')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';
$b->get('/myPwn/My-Products')
    ->with('request')
    ->begin()
    ->isParameter('module', 'products')
    ->isParameter('action', 'myProducts')
    ->end()

    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('h1', 'My Products')
    ->checkElement('table#products')
    ->end();
