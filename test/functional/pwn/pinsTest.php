<?php
/**
 * This is the Test for the New PINs
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/pins')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'pins')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'My PINs')
            ->checkElement('h1.mypwn_sub_page_heading_powwownow')
            ->checkElement('form#frm-myPINs')
            ->checkElement('p.single-pin')
            ->checkElement('button.button-orange','SHARE DETAILS')
            ->checkElement('button#call-settings-single','EDIT CALL SETTINGS')
            ->checkElement('button#request-welcome-pack','REQUEST WELCOME PACK')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/pins')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'pins')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'My PINs')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('form#frm-myPINs')
            ->checkElement('p.single-pin')
            ->checkElement('button.button-orange','SHARE DETAILS')
            ->checkElement('button#call-settings-single','EDIT CALL SETTINGS')
            ->checkElement('button#request-welcome-pack','REQUEST WELCOME PACK')
            ->checkElement('button#call-history','VIEW USAGE')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/pins')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'pins')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'My PINs')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('form#frm-myPINs')
            ->checkElement('table#tblPINs')
            ->checkElement('button#call-settings','EDIT CALL SETTINGS')
            ->checkElement('button#request-welcome-pack','REQUEST WELCOME PACK')
            ->checkElement('button#assign-products','ASSIGN PRODUCTS')
            ->checkElement('button#call-history','VIEW USAGE')
            ->checkElement('button#create-pin','CREATE PINs')
            ->checkElement('button#delete-pins','DELETE PINs')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/pins')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'pins')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'My PINs')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('form#frm-myPINs')
            ->checkElement('table#tblPINs')
            ->checkElement('button#call-settings','EDIT CALL SETTINGS')
            ->checkElement('button#request-welcome-pack-single','REQUEST WELCOME PACK')
            ->checkElement('button#call-history','VIEW USAGE')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/pins')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'pins')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'My PINs')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('form#frm-myPINs')
            ->checkElement('table#tblPINs')
            ->checkElement('button#call-settings','EDIT CALL SETTINGS')
            ->checkElement('button#request-welcome-pack','REQUEST WELCOME PACK')
            ->checkElement('button#call-history','VIEW USAGE')
            ->checkElement('button#add-pin','ADD NEW PIN')
            ->checkElement('button#delete-pins','DELETE PINs')
        ->end();