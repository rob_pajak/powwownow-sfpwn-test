<?php
/**
 * This is the Test for the New Schedule A Call page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/scheduleACall')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'scheduleACall')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_powwownow','Schedule A Call')
            ->checkElement('h3#button-email-details','Email details')
            ->checkElement('h3#button-scheduler-tool','Scheduler Tool')
            ->checkElement('h3#button-plugin-for-outlook','Plugin for Outlook')
            ->checkElementWithoutWhitespace('button.button-green','EMAIL DETAILS')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/scheduleACall')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'scheduleACall')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_plus','Schedule A Call')
            ->checkElement('h3#button-email-details','Email details')
            ->checkElement('h3#button-scheduler-tool','Scheduler Tool')
            ->checkElement('h3#button-plugin-for-outlook','Plugin for Outlook')
            ->checkElementWithoutWhitespace('button.button-green','EMAIL DETAILS')
        ->end();


// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/scheduleACall')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'scheduleACall')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_plus','Schedule A Call')
            ->checkElement('h3#button-email-details','Email details')
            ->checkElement('h3#button-scheduler-tool','Scheduler Tool')
            ->checkElement('h3#button-plugin-for-outlook','Plugin for Outlook')
            ->checkElementWithoutWhitespace('button.button-green','EMAIL DETAILS')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/scheduleACall')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'scheduleACall')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_premium','Schedule A Call')
            ->checkElement('h3#button-email-details','Email details')
            ->checkElement('h3#button-scheduler-tool','Scheduler Tool')
            ->checkElement('h3#button-plugin-for-outlook','Plugin for Outlook')
            ->checkElementWithoutWhitespace('button.button-green','EMAIL DETAILS')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/scheduleACall')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'scheduleACall')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_premium','Schedule A Call')
            ->checkElement('h3#button-email-details','Email details')
            ->checkElement('h3#button-scheduler-tool','Scheduler Tool')
            ->checkElement('h3#button-plugin-for-outlook','Plugin for Outlook')
            ->checkElementWithoutWhitespace('button.button-green','EMAIL DETAILS')
        ->end();

