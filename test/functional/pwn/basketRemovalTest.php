<?php

require_once 'registerPlusUserTest.php';

// Necessary setup.
$test = new lime_test(2, new lime_output_color());

// Add an existing product to the basket.
$addParameters = array(
    'topup' => array(
        'topup-amount' => 'input',
        'topup-amount-input' => 20,
        'auto-recharge' => 'off',
    )
);
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->post('/s/Add-Credit-To-Basket-Ajax', $addParameters);

// Test that it's in the basket.
$basket = new Basket(sfContext::getInstance()->getUser());
$addedProducts = $basket->retrieveAddedProducts();
$test->ok(isset($addedProducts[-1]), 'The product has been added to the basket.');

// Remove the product from the basket with the defined AJAX route.
$removeParameters = array(
    'remove-product' => -1,
);
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->post('/s/Remove-From-Basket', $removeParameters);

// Test that the basket no longer contains the item.
$basket = new Basket(sfContext::getInstance()->getUser());
$addedProducts = $basket->retrieveAddedProducts();
$test->ok(empty($addedProducts[-1]), 'The product has been removed from the basket.');
