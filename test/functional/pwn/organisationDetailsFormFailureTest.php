<?php

/**
 * Assert that the Organisation Details form allows manipulation of the Plus admin contact.
 * @author Maarten Jacobs
 */

require_once 'test/bootstrap/functional.php';

// Create and test as a new Plus admin.
require_once 'registerPlusUserTest.php';
testInvalidOrganisationDetails($b, 'Testing the Contact details form as a Plus admin.');
logoutUser($b);
