<?php
/**
 * Assert that when checking out the basket with a customer who has a balance greater than the basket cost,
 * the customer is redirected to the Recurring Payment Agreement page and all required parameters are set.
 *
 * @author Maarten Jacobs
 */

$basketUrl           = '/s/Basket';
$productUrl          = '/myPwn/Products';
$bundleToBasketUrl   = '/s/Add-Bundle-To-Basket-Ajax';
$removeFromBasketUrl = '/s/Remove-From-Basket';
$selectProductsUrl   = '/s/Select-Products-Submit';
$paymentPageUrl      = '/s/Basket-Submit';

// Login as the Plus Admin (Has Loads of Money)
require_once 'loginPlusAdmin.php';

// Check Page Status
$b->get($productUrl)->with('response')->begin()->isStatusCode(200)->end();

// Get the Bundle
$productsDOM = $b->getResponseDom();
$bundle5000ProductId = $productsDOM->getElementById('add-5000')->getAttribute('value');
$bundleArr = array('add-bundle' => $bundle5000ProductId, 'bundle-tc' => '1');

// Store Bundle
testPostAjaxRequest($b, '/s/Add-Bundle-To-Basket-Ajax', 200, $bundleArr, false, false);

assertBasketHasOnlyKeys(array($bundle5000ProductId));
//removeProductFromBasket($b, $bundle5000ProductId);

// Press Continue on the Products Page
// Press Checkout on the Basket Page
// Should go to the Recurring Payment Agreement page
$b->get($selectProductsUrl)->followRedirect();
$b->get($paymentPageUrl)->followRedirect()
    ->with('request')
        ->begin()
            ->isParameter('module', 'account')
            ->isParameter('action', 'recurringPaymentAgreementSetupPage')
        ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('select[name=paymentType]')
        ->end();

$domSelector = new sfDomCssSelector($b->getResponseDOM());
$val = $domSelector->matchAll('h3.blue')->getValues();
$b->test()->is(trim(array_shift($val)), 'Select credit card type:', 'The credit card label matches the expected value.');
