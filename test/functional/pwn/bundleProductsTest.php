<?php

/**
 * Assert the presence of the bundle products and their add-ons on the Products page.
 *
 * @author Maarten Jacobs
 */

require_once 'registerPlusUserTest.php';

$b->get('/myPwn/Products')
    ->with('response')->begin()
        ->isStatusCode(200)
        ->checkElement('tr.bundle.5000 .bundle-name', 'Landline 5000')
        ->checkElement('tr.bundle.5000.form #upgrade-5000')
        ->checkElement('tr.bundle.5000.form button')
        ->checkElement('tr.bundle.2500 .bundle-name', 'Landline 2500')
        ->checkElement('tr.bundle.2500.form #upgrade-2500')
        ->checkElement('tr.bundle.2500.form button')
        ->checkElement('tr.bundle.1000 .bundle-name', 'Landline 1000')
        ->checkElement('tr.bundle.1000.form #upgrade-1000')
        ->checkElement('tr.bundle.1000.form button')
    ->end();
/** @var DOMDocument $productsDOM */
$productsDOM = $b->getResponseDom();
$bundle5000ProductId = $productsDOM->getElementById('add-5000')->getAttribute('value');
$bundle5000UpgradeProductId = $productsDOM->getElementById('upgrade-5000')->getAttribute('value');

$b->get('/s/Add-Bundle-To-Basket-Ajax')
    ->with('response')->begin()
        ->isStatusCode(404)
    ->end();
$b->get('/s/Add-Bundle-To-Basket-Ajax')
    ->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->with('response')->begin()
        ->isStatusCode(404)
    ->end();

testPostAjaxRequest($b, '/s/Add-Bundle-To-Basket-Ajax', 200, array('add-bundle' => $bundle5000ProductId, 'bundle-tc' => '1'), false, false);
assertBasketHasOnlyKeys(array($bundle5000ProductId));
removeProductFromBasket($b, $bundle5000ProductId);

testPostAjaxRequest($b, '/s/Add-Bundle-To-Basket-Ajax', 200, array('add-bundle' => $bundle5000ProductId, 'upgrade-bundle' => $bundle5000UpgradeProductId, 'bundle-tc' => '1'), false, false);
assertBasketHasOnlyKeys(array($bundle5000UpgradeProductId));
