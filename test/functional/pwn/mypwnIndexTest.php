<?php
/**
 * This is the Test for the New myPwn Index Dashboard page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/index')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'index')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('div.home-grid')
            ->checkElement('div.powwownow_envelope')
            ->checkElement('div.powwownow_request_wallet_card')
            ->checkElement('div.powwownow_dial_in_numbers')
            ->checkElement('div.powwownow_letter_clock')
            ->checkElement('div.powwownow_manage_call_settings')
            ->checkElement('div.powwownow_in_conference_controls')
            ->checkElement('div.powwownow_scheduler')
            ->checkElement('div.powwownow_share_my_desktop')
            ->checkElement('div.powwownow_how_it_works')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/index')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'index')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('div.home-grid')
            ->checkElement('div.plus_request_wallet_card')
            ->checkElement('div.plus_dial_in_numbers')
            ->checkElement('div.plus_scheduler')
            ->checkElement('div.plus_purchase_products')
            ->checkElement('div.plus_pound')
            ->checkElement('div.plus_share_my_desktop')
            ->checkElement('div.plus_minute_bundles_new')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/index')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'index')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('div.home-grid')
            ->checkElement('div.premium_envelope')
            ->checkElement('div.premium_request_wallet_card')
            ->checkElement('div.premium_dial_in_numbers')
            ->checkElement('div.premium_letter_clock')
            ->checkElement('div.premium_manage_call_settings')
            ->checkElement('div.premium_in_conference_controls')
            ->checkElement('div.premium_scheduler')
            ->checkElement('div.premium_share_my_desktop')
            ->checkElement('div.premium_time_limited_pins')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/index')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'index')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('div.home-grid')
            ->checkElement('div.premium_manage_sets_of_pins')
            ->checkElement('div.premium_dial_in_numbers')
            ->checkElement('div.premium_in_conference_controls')
            ->checkElement('div.premium_letter_clock')
            ->checkElement('div.premium_access_recordings')
            ->checkElement('div.premium_call_history')
            ->checkElement('div.premium_scheduler')
            ->checkElement('div.premium_share_my_desktop')
            ->checkElement('div.premium_time_limited_pins')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/index')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'index')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('div.home-grid')
            ->checkElement('div.plus_envelope')
            ->checkElement('div.plus_request_wallet_card')
            ->checkElement('div.plus_dial_in_numbers')
            ->checkElement('div.plus_letter_clock')
            ->checkElement('div.plus_manage_call_settings')
            ->checkElement('div.plus_scheduler')
            ->checkElement('div.plus_share_my_desktop')
            ->checkElement('div.plus_how_it_works')
        ->end();

$overlay = array('regNew','regEx','invNew','invEx','regPromo');

foreach ($overlay as $id => $ref) {
    $b->get('/s/mypwn?' . $ref)
        ->with('request')
            ->begin()
                ->isParameter('module', 'mypwn')
                ->isParameter('action', 'index')
            ->end()

        ->with('response')
            ->begin()
                ->isStatusCode(200)
                ->matches("/Welcome to Powwownow Plus/")
                ->checkElement('div.dialin','0844 4 73 73 73')
                ->checkElement('div.pins')
                ->checkElement('span.pin')
                ->checkElement('button#btn-share-details span','SHARE DETAILS')
                ->checkElement('div.pin-usage-description')
                ->checkElement('div.chairpin')
                ->checkElement('div.partpin')
                ->checkElement('h2.plus-blue','So, what\'s next?')
                ->checkElement('div.footer-text')
                ->checkElement('div.do-now-buttons')
            ->end();
}


