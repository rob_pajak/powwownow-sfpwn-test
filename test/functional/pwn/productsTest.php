<?php
require_once 'registerPlusUserTest.php';

$b->get('/myPwn/Products')
    ->with('request')
        ->begin()
            ->isParameter('module', 'products')
            ->isParameter('action', 'index')
        ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Products')
        ->end();

$b->get('/s/Purchase-Credit')->followRedirect()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Products')
            ->checkElement('input[type=radio]#topup_topup-amount_20[checked=checked]')
            ->checkElement('input[type=checkbox]#topup_auto-recharge[checked=checked]')
        ->end();