<?php
/**
 * This is the Test for the New Create Pin Test
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/createPin')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createPin')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/createPin')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createPin')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/createPin')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createPin')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/createPin')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createPin')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'No Access')
            ->checkElement('h2','You do not have permission to access this page!')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/createPin')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'createPin')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Create PIN')
            ->checkElement('h1.mypwn_sub_page_heading_premium')
            ->checkElement('form#form-create-pin')
            ->checkElement('select#createPin_contact_ref')
            ->checkElement('input#createPin_email')
            ->checkElement('input#createPin_cost_code')
            ->checkElement('input#createPin_title')
            ->checkElement('input#createPin_first_name')
            ->checkElement('input#createPin_middle_initials')
            ->checkElement('input#createPin_last_name')
            ->checkElement('input#createPin_phone_number')
            ->checkElement('input#createPin_mobile_phone_number')
            ->checkElement('input#createPin_password')
            ->checkElement('input#createPin_confirm_password')  
            ->checkElement('button','Create PIN')
        ->end();

// Ajax Testing

$existingContactRef   = '94498';
$uniqueEmailAddress   = 'test+' . uniqid() . '@powwownow.com';
$routecreatePin       = '/s/Create-Pin-Ajax';

// Existing User Test

$args = array('createPin' => array('contact_ref' => '1'));
testPostAjaxRequest($b, $routecreatePin, 500, $args, false, 'FORM_VALIDATION_INVALID_CONTACT');

$args = array('createPin' => array('contact_ref' => $existingContactRef));
testPostAjaxRequest($b, $routecreatePin, 200, $args, false, false);

// New User Test

$args = array('createPin' => array('contact_ref' => '','email' => '', 'cost_code' => '', 'title' => '', 'first_name' => '', 'last_name' => '', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreatePin, 500, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

$args = array('createPin' => array('contact_ref' => '','email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => '', 'last_name' => '', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreatePin, 500, $args, false, 'FORM_VALIDATION_FIRST_NAME_EMPTY');

$args = array('createPin' => array('contact_ref' => '','email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => '', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreatePin, 500, $args, false, 'FORM_VALIDATION_LAST_NAME_EMPTY');

$args = array('createPin' => array('contact_ref' => '','email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreatePin, 500, $args, false, 'FORM_VALIDATION_NO_PASSWORD');

$args = array('createPin' => array('contact_ref' => '','email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '123123', 'confirm_password' => ''));
testPostAjaxRequest($b, $routecreatePin, 500, $args, false, 'FORM_VALIDATION_PASSWORD_MISMATCH');

$args = array('createPin' => array('contact_ref' => '','email' => 'asfer.tamimi@powwownow.com', 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '123123', 'confirm_password' => '123123', 'middle_initials' => '', 'mobile_phone_number' => ''));
testPostAjaxRequest($b, $routecreatePin, 500, $args, false, 'FORM_CREATE_USER_EMAIL_TAKEN');

$args = array('createPin' => array('email' => $uniqueEmailAddress, 'cost_code' => '', 'title' => '', 'first_name' => 'asd', 'last_name' => 'asd', 'phone_number' => '', 'password' => '123123', 'confirm_password' => '123123', 'contact_ref' => '', 'middle_initials' => '', 'mobile_phone_number' => ''));
testPostAjaxRequest($b, $routecreatePin, 200, $args, false, false);
