<?php
/**
 * This is the Test for the How Web Conferencing Works Page (/Web-Conference/How-Web-Conferencing-Works)
 * @author Asfer Tamimi
 */
require_once 'test/bootstrap/functional.php';

$b->get('/Web-Conference/How-Web-Conferencing-Works')
    ->with('request')
        ->begin()
            ->isParameter('module', 'pages')
            ->isParameter('action', 'howWebConferencingWorks')
        ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'How Web Conferencing Works')
            ->checkElement('h1.mypwn_sub_page_heading_powwownow')
            ->checkElement('video.pwn-video-helper')
        ->end();
