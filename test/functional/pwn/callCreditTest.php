<?php
/**
 * Asserting Call Credit on the Products Page
 *
 * @author Asfer Tamimi
 */
require_once 'registerPlusUserTest.php';

// Necessary setup.
$test         = new lime_test(null, new lime_output_color());
$productsPage = '/myPwn/Products';
$bundlesAjax  = '/s/Add-Bundle-To-Basket-Ajax';
$creditAjax   = '/s/Add-Credit-To-Basket-Ajax';
$removeAjax   = '/s/Remove-From-Basket';

// Add an existing product to the basket.
$addParameters = array(
    'topup' => array(
        'topup-amount'       => 'input',
        'topup-amount-input' => 20,
        'auto-recharge'      => 'off',
    )
);
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')->post($creditAjax, $addParameters);

// Test that it's in the basket.
$basket        = new Basket(sfContext::getInstance()->getUser());
$addedProducts = $basket->retrieveAddedProducts();
$test->ok(isset($addedProducts[-1]), 'The product has been added to the basket.');

// Check if the Button has been removed from the Products Page
$b->get($productsPage)
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('button#credit-added-button')
    ->end();

// Remove Call Credit from the basket with the defined AJAX route.
$removeParameters = array(
    'remove-product' => -1,
);
$b->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')->post($removeAjax, $removeParameters);

// Test that the basket no longer contains the item.
$basket        = new Basket(sfContext::getInstance()->getUser());
$addedProducts = $basket->retrieveAddedProducts();
$test->ok(empty($addedProducts[-1]), 'The product has been removed from the basket.');

// Add A Bundle
/** @var DOMDocument $productsDOM */
$b->get($productsPage);
$productsDOM                = $b->getResponseDom();
$bundle5000ProductId        = $productsDOM->getElementById('add-5000')->getAttribute('value');
$bundle5000UpgradeProductId = $productsDOM->getElementById('upgrade-5000')->getAttribute('value');

$b->get($bundlesAjax)
    ->with('response')->begin()
    ->isStatusCode(404)
    ->end();
$b->get($bundlesAjax)
    ->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->with('response')->begin()
    ->isStatusCode(404)
    ->end();

testPostAjaxRequest(
    $b,
    $bundlesAjax,
    200,
    array('add-bundle' => $bundle5000ProductId, 'bundle-tc' => '1'),
    false,
    false
);
assertBasketHasOnlyKeys(array($bundle5000ProductId));
removeProductFromBasket($b, $bundle5000ProductId);

testPostAjaxRequest(
    $b,
    $bundlesAjax,
    200,
    array(
        'add-bundle'     => $bundle5000ProductId,
        'upgrade-bundle' => $bundle5000UpgradeProductId,
        'bundle-tc'      => '1'
    ),
    false,
    false
);
assertBasketHasOnlyKeys(array($bundle5000UpgradeProductId));

// Check if the Button has been removed from the Products Page
$b->get($productsPage)
    ->with('response')
    ->begin()
    ->isStatusCode(200)
    ->checkElement('button#credit-disabled-button')
    ->checkElement('tr.bundle.5000 td.product-action button[disabled=disabled]')
    ->checkElement('tr.bundle.2500 td.product-action button[disabled=disabled]')
    ->checkElement('tr.bundle.aycm td.product-action button[disabled=disabled]')
    ->end();