<?php

require_once 'test/bootstrap/functional.php';

$user = $b->args('doPlusRegistrationRegister.php');

Hermes_Client_Rest::call('doPlusRegistrationRegister', $user);

$b->info('login user ' . $user['email'] . ' with password ' . $user['password'])
    ->post('/s/test/login', array('user_email' => $user['email'], 'user_password' => $user['password']))
        ->with('response')
            ->begin()
                ->isStatusCode(200)
            ->end();
