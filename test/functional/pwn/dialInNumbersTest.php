<?php
/**
 * This is the Test for the New Dial-in Numbers Test
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/dialInNumbers')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'dialInNumbers')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Dial-in Numbers and Rates')
            ->checkElement('table#tblcontactdialinnumbers')
            ->checkElement('table.mypwn')
            ->checkElement('button','DOWNLOAD PDF')
            ->checkElement('div.dial_in_numbers_footer')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/dialInNumbers')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'dialInNumbers')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Dial-in Numbers and Rates')
            ->checkElement('table#tblcontactdialinnumbers')
            ->checkElement('table.mypwn')
            ->checkElement('table.plus')
            ->checkElement('div.dial_in_numbers_footer')
        ->end();

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/dialInNumbers')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'dialInNumbers')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Dial-in Numbers and Rates')
            ->checkElement('table#tblcontactdialinnumbers')
            ->checkElement('table.mypwn')
            ->checkElement('table.plus')
            ->checkElement('div.dial_in_numbers_footer')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/dialInNumbers')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'dialInNumbers')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Dial-in Numbers and Rates')
            ->checkElement('table#tblcontactdialinnumbers')
            ->checkElement('table.mypwn')
            ->checkElement('table.premium')
            ->checkElement('div.dial_in_numbers_footer')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/dialInNumbers')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'dialInNumbers')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'Dial-in Numbers and Rates')
            ->checkElement('table#tblcontactdialinnumbers')
            ->checkElement('table.mypwn')
            ->checkElement('table.premium')
            ->checkElement('div.dial_in_numbers_footer')
        ->end();