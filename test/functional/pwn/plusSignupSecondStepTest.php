<?php

/**
 * Assert that the second step of the plus signup process is only accessible via POST, AJAX requests for Enhanced and anonymous user.
 *
 * The behaviour differs for both user types:
 *  - Enhanced users do not pass password and their email must match their current email,
 *  - Anonymous users must pass their password.
 */

require_once 'test/bootstrap/functional.php';
$secondStepRoute = '/s/ajax/plus-signup/second-step';
$emailIt = 1;
$testPassword = 'r1chm0nd';

$enhancedUserBrowser = new sfPwnPlusFunctionalTest(new sfBrowser());

$validEmail = generateTestEmail();
$validAnonymousFormParams = array(
    'email' => $validEmail,
    'terms_and_conditions' => 1,
    'password' => $testPassword,
    'confirm_password' => $testPassword,
    'first_name' => 'Hank',
    'last_name' => 'Scorpio',
    'company_name' => 'Globex',
    'business_phone' => '001 123123',
);
$validEnhancedFormParams = array(
    'email' => '',
    'terms_and_conditions' => 1,
    'first_name' => 'Hank',
    'last_name' => 'Scorpio',
    'company_name' => 'Globex',
    'business_phone' => '001 123123',
);

$invalidEnhancedUser = createNewEnhancedUser(generateTestEmail());
$invalidEnhancedFormParams = array(
    'email' => $invalidEnhancedUser['email'],
    'terms_and_conditions' => 1,
    'first_name' => 'Hank',
    'last_name' => 'Scorpio',
    'company_name' => 'Globex',
    'business_phone' => '001 123123',
);

// Assert GET requests are not allowed, even if they are AJAX.
$b->info('Asserting 404 for GET requests to second step of plus-signup.')
    ->setHttpHeader('X_REQUESTED_WITH', 'XMLHttpRequest')
    ->get($secondStepRoute)
    ->with('response')->begin()
        ->isStatusCode(404)
    ->end();

// Assert valid request by anonymous results in signup.
$b->setHttpHeader('User-Agent', 'console');
testPostAjaxRequest($b, $secondStepRoute, 200, $validAnonymousFormParams, false, false);
logoutUser($b);
loginUser($b, $validEmail, $testPassword, 'Logging in anonymous user after signing up as plus.');
logoutUser($b);

// Assert valid request by enhanced user results in signup
$validEnhancedUser = createNewEnhancedUser(generateTestEmail());
loginUser($enhancedUserBrowser, $validEnhancedUser['email'], $validEnhancedUser['password']);
$formParams = $validEnhancedFormParams;
$formParams['email'] = $validEnhancedUser['email'];
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 200, $formParams, false, false);
logoutUser($enhancedUserBrowser);
loginUser($enhancedUserBrowser, $validEnhancedUser['email'], $validEnhancedUser['password'], 'Logging in enhanced user after upgrading to plus.');
logoutUser($enhancedUserBrowser);
$enhancedUserBrowser = new sfPwnPlusFunctionalTest(new sfBrowser());

// Assert, for both user types, that invalid first name results in error message INVALID_FIRST_NAME and HTTP Status 400.
// For anonymous users.
$b->info('Testing first name validation for anonymous users.');
// For anonymous user, with empty first name.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['first_name'] = '';
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('first_name' => 'INVALID_FIRST_NAME'));
// For anonymous user, with first name with type array.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['first_name'] = array();
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('first_name' => 'INVALID_FIRST_NAME'));
// For enhanced users
$enhancedUserBrowser->info('Testing first name validation for enhanced users.');
// For Enhanced user, with empty first name.
loginUser($enhancedUserBrowser, $invalidEnhancedUser['email'], $invalidEnhancedUser['password'], 'Logging in enhanced user.');
$invalidEnhancedFormParams = $validEnhancedFormParams;
$invalidEnhancedFormParams['email'] = $invalidEnhancedUser['email'];
$invalidEnhancedFormParams['first_name'] = '';
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('first_name' => 'INVALID_FIRST_NAME'));
// For enhanced user, with first name with type array.
$invalidEnhancedFormParams['first_name'] = array();
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('first_name' => 'INVALID_FIRST_NAME'));

// Assert, for both user types, that invalid last name results in error message INVALID_LAST_NAME and HTTP Status 400.
$b->info('Test last name validation for anonymous users.');
// For anonymous user, with empty last name.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['last_name'] = '';
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('last_name' => 'INVALID_LAST_NAME'));
// For anonymous user, with last name with type array.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['last_name'] = array();
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('last_name' => 'INVALID_LAST_NAME'));
// For enhanced users,
$enhancedUserBrowser->info('Test last name validation for enhanced users.');
// For Enhanced user, with empty last name.
$invalidEnhancedFormParams = $validEnhancedFormParams;
$invalidEnhancedFormParams['email'] = $invalidEnhancedUser['email'];
$invalidEnhancedFormParams['last_name'] = '';
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('last_name' => 'INVALID_LAST_NAME'));
// For enhanced user, with last name with type array.
$invalidEnhancedFormParams['last_name'] = array();
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('last_name' => 'INVALID_LAST_NAME'));

// Assert, for both user types, that invalid company name results in error message INVALID_COMPANY_NAME and HTTP Status 400.
$b->info('Test company name validation for anonymous users.');
// For anonymous user, with empty company name.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['company_name'] = '';
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('company_name' => 'INVALID_COMPANY_NAME'));
// For anonymous user, with company name with type array.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['company_name'] = array();
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('company_name' => 'INVALID_COMPANY_NAME'));
// For enhanced users,
$enhancedUserBrowser->info('Test company name validation for enhanced users.');
// For Enhanced user, with empty company name.
$invalidEnhancedFormParams = $validEnhancedFormParams;
$invalidEnhancedFormParams['email'] = $invalidEnhancedUser['email'];
$invalidEnhancedFormParams['company_name'] = '';
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('company_name' => 'INVALID_COMPANY_NAME'));
// For enhanced user, with company name with type array.
$invalidEnhancedFormParams['company_name'] = array();
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('company_name' => 'INVALID_COMPANY_NAME'));

// Assert, for both user types, that invalid business phone results in error message INVALID_BUSINESS_PHONE and HTTP Status 400.
$b->info('Test business phone validation for anonymous users.');
// For anonymous user, with empty business phone.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['business_phone'] = '';
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('business_phone' => 'INVALID_BUSINESS_PHONE'));
// For anonymous user, with business phone with type array.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['business_phone'] = array();
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('business_phone' => 'INVALID_BUSINESS_PHONE'));
// For enhanced users,
$enhancedUserBrowser->info('Test business phone validation for enhanced users.');
// For Enhanced user, with empty business phone.
$invalidEnhancedFormParams = $validEnhancedFormParams;
$invalidEnhancedFormParams['email'] = $invalidEnhancedUser['email'];
$invalidEnhancedFormParams['business_phone'] = '';
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('business_phone' => 'INVALID_BUSINESS_PHONE'));
// For enhanced user, with business phone with type array.
$invalidEnhancedFormParams['business_phone'] = array();
testPostAjaxRequest($enhancedUserBrowser, $secondStepRoute, 400, $invalidEnhancedFormParams);
plusSignupResponseContainsErrorMessages($enhancedUserBrowser, array('business_phone' => 'INVALID_BUSINESS_PHONE'));

// Assert, for anonymous, that invalid terms and conditions results in error message TERMS_ARE_REQUIRED and HTTP Status 400.
$b->info('Test terms and conditions validation for anonymous users.');
// For anonymous user, with unset terms and conditions.
$invalidFormParams = $validAnonymousFormParams;
unset($invalidFormParams['terms_and_conditions']);
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('terms_and_conditions' => 'TERMS_ARE_REQUIRED'));
// For anonymous user, with empty terms and conditions.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['terms_and_conditions'] = 0;
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('terms_and_conditions' => 'TERMS_ARE_REQUIRED'));
// For anonymous user, with terms and conditions with type array.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['terms_and_conditions'] = array();
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('terms_and_conditions' => 'TERMS_ARE_REQUIRED'));

// Assert, for anonymous, that an invalid password results in error message INVALID_PASSWORD and HTTP Status 400.
$b->info('Test password validation for anonymous users.');
// For anonymous user, with empty password.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['password'] = '';
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('password' => 'INVALID_PASSWORD'));
// For anonymous user, with password with type array.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['password'] = array( 'no-no' );
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('password' => 'INVALID_PASSWORD'));

// Assert, for anonymous, that non-matching passwords results in error message PASSWORDS_DO_NOT_MATCH and HTTP Status 400.
$b->info('Test password match validation for anonymous users.');
// For anonymous user, with empty confirm password.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['confirm_password'] = '';
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b,  array('confirm_password' => 'PASSWORDS_DO_NOT_MATCH'));
// For anonymous user, with confirm password with type array.
$invalidFormParams = $validAnonymousFormParams;
$invalidFormParams['confirm_password'] = array();
testPostAjaxRequest($b, $secondStepRoute, 400, $invalidFormParams);
plusSignupResponseContainsErrorMessages($b, array('confirm_password' => 'PASSWORDS_DO_NOT_MATCH'));
