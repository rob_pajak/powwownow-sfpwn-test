<?php

/**
 * Assert the presence of Bundles and their add-ons on the Basket page.
 *
 * @author Maarten Jacobs
 */

$basketUrl = '/s/Basket';
$productUrl = '/myPwn/Products';
$bundleToBasketUrl = '/s/Add-Bundle-To-Basket-Ajax';
$removeFromBasketUrl = '/s/Remove-From-Basket';

// Create a new plus admin
require_once 'registerPlusUserTest.php';

// Assert that the basket page contains no products.
$b->get($basketUrl)
    ->with('response')->begin()
        ->checkElement('p > a[href$="/myPwn/Products"]', 'continue shopping')
    ->end();

// Add a bundle without add-ons to the basket.
$b->get($productUrl);
$productsDOM = $b->getResponseDom();
$bundle5000ProductId = $productsDOM->getElementById('add-5000')->getAttribute('value');
$bundle5000UpgradeProductId = $productsDOM->getElementById('upgrade-5000')->getAttribute('value');
makePostAJAXRequest($b, $bundleToBasketUrl, array('add-bundle' => $bundle5000ProductId, 'bundle-tc' => '1'));

// Assert that there is only one row in the basket and that it's the bundle.
$b->get($basketUrl)
    ->with('response')
        ->checkElement('#basket-products tbody tr', true, array('count' => 1));

// Remove the bundle from the basket.
makePostAJAXRequest($b, $removeFromBasketUrl, array('remove-product' => $bundle5000ProductId));

// Assert that the basket page contains no products.
$b->get($basketUrl)
    ->with('response')
        ->checkElement('#basket-products tbody tr', false);

// Add a bundle with an add-on the basket.
makePostAJAXRequest($b, $bundleToBasketUrl, array('add-bundle' => $bundle5000ProductId, 'upgrade-bundle' => $bundle5000UpgradeProductId, 'bundle-tc' => '1'));

// Assert that there are two rows in the basket and that they're the bundle and its add-on.
$b->get($basketUrl)
    ->with('response')->begin()
        ->checkElement('#basket-products tbody tr', true, array('count' => 2))
        ->checkElement('#basket-products tbody tr.addon-' . $bundle5000ProductId)
    ->end();
