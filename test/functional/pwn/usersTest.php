<?php
/**
 * This is the Test for the Users Page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/users')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'users')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1', 'My Users')
            ->checkElement('h1.mypwn_sub_page_heading_plus')
            ->checkElement('form#frm-users')
            ->checkElement('table#tblUsers')
            ->checkElement('button#call-settings','EDIT CALL SETTINGS')
            ->checkElement('button#request-welcome-pack','REQUEST WELCOME PACK')
            ->checkElement('button#assign-products','ASSIGN PRODUCTS')
            ->checkElement('button#call-history','VIEW USAGE')
            ->checkElement('button#delete-users','DELETE USERS')
            ->checkElement('button#add-user','ADD NEW USER')
        ->end();
