<?php
/**
 * This is the Test for the New Recordings page
 * Full Symfony Route will be used, since it is currently not using the Old Existing Route
 * @author Asfer Tamimi
 * @todo Create Better Tests.
 */

// Enhanced Login
require_once 'loginEnhancedUser.php';

$b->get('/s/mypwn/recordings')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'recordings')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_powwownow','Recordings')
            ->checkElement('table#tblRecordings')
        ->end();

// Plus User Login
require_once 'loginPlusUser.php';

$b->get('/s/mypwn/recordings')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'recordings')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_plus','Recordings')
            ->checkElement('table#tblRecordings')
        ->end();


// Plus Admin Login
require_once 'loginPlusAdmin.php';

$b->get('/s/mypwn/recordings')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'recordings')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_plus','Recordings')
            ->checkElement('form#frm-recordings')
            ->checkElement('input#recordings_from_date')
            ->checkElement('input#recordings_to_date')
            ->checkElement('input#recordings_description')
            ->checkElement('input#recordings_pin')
            ->checkElement('input#recordings_show_all_users')
            ->checkElement('button#frm-recordings-submit')
            ->checkElement('table#tblRecordings')
        ->end();

// Premium User Login
require_once 'loginPremiumUser.php';

$b->get('/s/mypwn/recordings')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'recordings')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_premium','Recordings')
            ->checkElement('table#tblRecordings')
        ->end();

// Premium Admin Login
require_once 'loginPremiumAdmin.php';

$b->get('/s/mypwn/recordings')
    ->with('request')
        ->begin()
            ->isParameter('module', 'mypwn')
            ->isParameter('action', 'recordings')
        ->end()

    ->with('response')
        ->begin()
            ->isStatusCode(200)
            ->checkElement('h1.mypwn_sub_page_heading_premium','Recordings')
            ->checkElement('form#frm-recordings')
            ->checkElement('input#recordings_from_date')
            ->checkElement('input#recordings_to_date')
            ->checkElement('input#recordings_description')
            ->checkElement('input#recordings_pin')
            ->checkElement('input#recordings_show_all_users')
            ->checkElement('button#frm-recordings-submit')
            ->checkElement('table#tblRecordings')
        ->end();

// Add Testing for the Premium Admin, Downloading, Updating their Recordings

// @todo: for all admin types, test successful form submission.

// @todo: for all admin types, test that invalid from_date (date < today - 1 year OR date > maximum date) results in HTTP status 500.

// @todo: for all admin types, test that invalid to_date (date < today - 1 year) results in HTTP status 500.

// @todo: for all admin types, test that invalid description (length > 50) results in HTTP status 500.

// @todo: for all admin types, test that invalid pin (length > 50, subject to change) results in HTTP status 500.

// @todo: for all admin types, test that invalid show_all_users (length > 50, subject to change) results in HTTP status 500.