<?php
/**
 * This is the Test for the Old Homepage Schedule A Call Functionality
 * Full Symfony Route will be used, since there is AB testing on the homepage
 * @author Asfer Tamimi
 *
 */

// Must be done with a New User.
require_once 'test/bootstrap/functional.php';
$routeAjax = '/Outlook-Plugin';
$uniqueEmailAddress = 'test+' . uniqid() . '@powwownow.com';
$b->get('/?forceVar=E')
    ->with('request')
        ->begin()
            ->isParameter('module', 'pages')
            ->isParameter('action', 'indexE')
        ->end()
    ->with('response')
        ->begin()
            ->isStatusCode(200)
        ->end();

// Schedule A Call Tests.
$args = array('email_address' => '', 'job_title' => '', 'company' => '');
testPostAjaxRequest($b, $routeAjax, 500, $args, false, 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS');

$args = array('email_address' => $uniqueEmailAddress, 'job_title' => '', 'company' => '');
testPostAjaxRequest($b, $routeAjax, 200, $args, false, false);
