<?php
/**
 * Class CreateUserTest
 * This is to test the Create User Task
 * @author Asfer Tamimi
 */
class CreateUserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var createUserTask Create User Task
     */
    private $taskObj;

    /**
     * @var Project Configuration
     */
    private $configurationObj = null;

    /**
     * @todo Move the Below setUp Code into the public static function setUpBelowClass, without breaking code
     */
    protected function setUp() {
        if (is_null($this->configurationObj)) {
            $projectDir = realpath(dirname(__FILE__) . '/../../../../');
            require_once $projectDir . '/config/ProjectConfiguration.class.php';
            $this->configurationObj = ProjectConfiguration::hasActive() ? ProjectConfiguration::getActive() : new ProjectConfiguration($projectDir);
            $this->taskObj = new createUserTask($this->configurationObj->getEventDispatcher(), new sfFormatter());
        }

        // Disable All Test Accounts
        $this->taskObj->run(array('userType' => 'disable'),array());
    }

    /**
     * Test the Enhanced User Creation Functionality
     */
    public function testCreateEnhancedUser() {
        // Create Enhanced Users
        $this->taskObj->run(array('userType' => 'enhanced'),array());

        try {
            $result = Hermes_Client_Rest::call('UserManagement.getAllActiveAccountsByEmailFormatting',array(
                'emailFormatting' => 'pwn_test+',
                'userType' => 'enhanced'
            ));

            $this->assertEquals(5,count($result),'Enhanced User Count did not match');
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call Client Exception was called. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Exception was called, Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }
    }

    /**
     * Test the Plus User Creation Functionality
     */
    public function testCreatePlusUser() {
        // Create Enhanced Users
        $this->taskObj->run(array('userType' => 'plusUser'),array());

        try {
            $result = Hermes_Client_Rest::call('UserManagement.getAllActiveAccountsByEmailFormatting',array(
                'emailFormatting' => 'pwn_test+',
                'userType' => 'plusUser'
            ));

            $this->assertEquals(21,count($result),'Plus User Count did not match');
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call Client Exception was called. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Exception was called, Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }
    }

    /**
     * Test the Plus Admin Creation Functionality
     */
    public function testCreatePlusAdmin() {
        // Create Enhanced Users
        $this->taskObj->run(array('userType' => 'plusAdmin'),array());

        try {
            $result = Hermes_Client_Rest::call('UserManagement.getAllActiveAccountsByEmailFormatting',array(
                'emailFormatting' => 'pwn_test+',
                'userType' => 'plusAdmin'
            ));

            $this->assertEquals(21,count($result),'Plus Admin Count did not match');
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call Client Exception was called. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Exception was called, Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }
    }

    /**
     * Test the Premium User Creation Functionality
     */
    public function testCreatePremiumUser() {
        // Create Enhanced Users
        $this->taskObj->run(array('userType' => 'premiumUser'),array());

        try {
            $result = Hermes_Client_Rest::call('UserManagement.getAllActiveAccountsByEmailFormatting',array(
                'emailFormatting' => 'pwn_test+',
                'userType' => 'premiumUser'
            ));

            $this->assertEquals(21,count($result),'Premium User Count did not match');
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call Client Exception was called. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Exception was called, Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }
    }

    /**
     * Test the Premium Admin Creation Functionality
     */
    public function testCreatePremiumAdmin() {
        // Create Enhanced Users
        $this->taskObj->run(array('userType' => 'premiumAdmin'),array());

        try {
            $result = Hermes_Client_Rest::call('UserManagement.getAllActiveAccountsByEmailFormatting',array(
                'emailFormatting' => 'pwn_test+',
                'userType' => 'premiumAdmin'
            ));

            $this->assertEquals(21,count($result),'Premium Admin Count did not match');
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Hermes Call Client Exception was called. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('UserManagement.getAllActiveAccountsByEmailFormatting Exception was called, Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }
    }
}
