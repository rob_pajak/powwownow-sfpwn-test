#!/bin/sh

export PATH=/usr/local/scripts/include:$PATH

. pwnlog.sh
. pwnutils.sh

if [ -z "$LOCKPREFIX" ]
then
    LOCKPREFIX=""
fi
lockFile=$LOCKPREFIX$0

pwnlock $lockFile #check for or create a lock

hg pull
hg update --clean
./sfpwn-autotest.php

pwnunlock $lockFile #check for and remove a lock
