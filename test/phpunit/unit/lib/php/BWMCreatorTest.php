<?php
require_once dirname(__FILE__) . '/../../../../../lib/php/PinRetriever.php';
require_once dirname(__FILE__) . '/../../../../../lib/php/BWMCreator.php';
require_once dirname(__FILE__) . '/../../../lib/MockHermesClientRest.php';
require_once dirname(__FILE__) . '/../../../lib/FaultyMockHermesClientRest.php';

class BWMCreatorTest extends PHPUnit_Framework_TestCase
{
    public function testBWMCreationWithoutPins()
    {
        $bwm = array(
            'pins' => array(),
            0 => array(
                'bwm_label' => 'New BWM',
                'transcript' => 'BWM Transcript',
            ),
            1 => array(
                array(
                    'type' => true,
                    'rate' => true,
                    'id' => true,
                    'country_code' => true,
                )
            ),
        );
        MockHermesClientRest::$returnValue = 1;
        $pinRetriever = $this->getPinRetrieverMock();
        $pinRetriever->expects($this->never())
            ->method('retrievePinsFromContact');
        $creator = new BWMCreator(1, $pinRetriever, 'MockHermesClientRest');
        $result = $creator->createAndAssign($bwm);

        $this->assertEquals('BWM.doAssign', MockHermesClientRest::$calledMethodName);
        $expectedParameters = array(
            'service_ref' => 850,
            'pin_refs' => array(),
            'group_name' => 'New BWM',
            'account_id' => 1,
            'transcript' => 'BWM Transcript',
            'products' => array(
                array(
                    'geo_city_id' => true,
                    'dnis_type' => true,
                    'rate' =>  true,
                    'country_code' => true,
                )
            ),
        );
        $this->assertEquals($expectedParameters, MockHermesClientRest::$calledParameters);
        $expectedResult = array(
            'success' => true,
            'pins_assigned' => array(
                1 => array(),
            ),
            'contacts_assigned' => array(
                1 => array()
            ),
            'products' => array(
                1 => $bwm,
            ),
            'error_messages' => array(),
        );
        $this->assertEquals($expectedResult, $result);
    }

    public function testBWMCreationWithPins()
    {
        $bwm = array(
            'pins' => array(1, 2, 3),
            0 => array(
                'bwm_label' => 'New BWM',
                'transcript' => 'BWM Transcript',
            ),
            1 => array(
                array(
                    'type' => true,
                    'rate' => true,
                    'id' => true,
                    'country_code' => true,
                )
            ),
        );
        MockHermesClientRest::$returnValue = 1;
        $pinRetriever = $this->getPinRetrieverMock();
        $pinRetriever->expects($this->at(0))
            ->method('retrievePinsFromContact')
            ->with(1)
            ->will($this->returnValue(array(1, 2)));
        $pinRetriever->expects($this->at(1))
            ->method('retrievePinsFromContact')
            ->with(2)
            ->will($this->returnValue(array(3)));
        $pinRetriever->expects($this->at(2))
            ->method('retrievePinsFromContact')
            ->with(3)
            ->will($this->returnValue(array(4, 5, 6)));
        $creator = new BWMCreator(1, $pinRetriever, 'MockHermesClientRest');
        $result = $creator->createAndAssign($bwm);

        $this->assertEquals('BWM.doAssign', MockHermesClientRest::$calledMethodName);
        $expectedParameters = array(
            'service_ref' => 850,
            'pin_refs' => array(1, 2, 3, 4, 5, 6),
            'group_name' => 'New BWM',
            'account_id' => 1,
            'transcript' => 'BWM Transcript',
            'products' => array(
                array(
                    'geo_city_id' => true,
                    'dnis_type' => true,
                    'rate' =>  true,
                    'country_code' => true,
                )
            ),
        );
        $this->assertEquals($expectedParameters, MockHermesClientRest::$calledParameters);
        $expectedResult = array(
            'success' => true,
            'pins_assigned' => array(
                1 => array(1, 2, 3, 4, 5, 6),
            ),
            'contacts_assigned' => array(
                1 => array(1, 2, 3),
            ),
            'products' => array(
                1 => $bwm,
            ),
            'error_messages' => array(),
        );
        $this->assertEquals($expectedResult, $result);
    }

    public function testHermesClientExceptionHandling()
    {
        $bwm = array(
            'pins' => array(),
            0 => array(
                'bwm_label' => true,
                'transcript' => true,
            ),
            1 => array(
                array(
                    'type' => true,
                    'rate' => true,
                    'id' => true,
                    'country_code' => true,
                )
            ),
        );

        $creator = new BWMCreator(1, $this->getPinRetrieverMock(), 'FaultyMockHermesClientRest');
        $result = $creator->createAndAssign($bwm);

        $this->assertFalse($result['success']);
        $this->assertEquals(array('Error whilst trying to create new BWM.'), $result['error_messages']);
    }

    /**
     * @param array $bwm
     *
     * @dataProvider invalidBWMProvider
     */
    public function testBWMValidation(array $bwm)
    {
        $creator = new BWMCreator(1, $this->getPinRetrieverMock(), 'MockHermesClientRest');
        $result = $creator->createAndAssign($bwm);

        $this->assertFalse($result['success']);
        $this->assertEquals(array('An attempt to create an invalid BWM was found.'), $result['error_messages']);
    }

    /**
     * @param array $dnises
     *
     * @dataProvider invalidDNISProvider
     */
    public function testDNISValidation(array $dnises)
    {
        $bwm = array(
            'pins' => array(),
            0 => array(
                'bwm_label' => true,
                'transcript' => true,
            ),
            1 => $dnises,
        );
        $creator = new BWMCreator(1, $this->getPinRetrieverMock(), 'MockHermesClientRest');
        $result = $creator->createAndAssign($bwm);

        $this->assertFalse($result['success']);
        $this->assertEquals(array('No valid DNISes were found.'), $result['error_messages']);
    }

    public function invalidDNISProvider()
    {
        $args = array(
            array(
                array(false),
            ),
            array(
                array(
                    'type' => true,
                    'rate' => true,
                    'id' => false,
                    'country_code' => false,
                ),
            ),
            array(
                array(
                    'type' => true,
                    'rate' => false,
                    'id' => true,
                ),
            ),
            array(
                array(
                    'type' => false,
                    'rate' => true,
                    'id' => true,
                ),
            ),
        );

        return $args;
    }

    public function invalidBWMProvider()
    {
        $args = array();

        // Empty BWM.
        $args[] = array(
            array(),
        );

        // Invalid pins.
        $args[] = array(
            array(
                'pins' => null,
            ),
        );
        $args[] = array(
            array(
                'pins' => false,
            ),
        );
        $args[] = array(
            array(
                'pins' => new stdClass(),
            ),
        );

        // Invalid metadata (key 0) and fee (key 1) blocks.
        $args[] = array(
            array(
                'pins' => array(),
                0 => null,
                1 => null,
            ),
        );
        $args[] = array(
            array(
                'pins' => array(),
                0 => array(
                    'bwm_label' => true,
                    'transcript' => true,
                ),
                1 => false,
            ),
        );
        $args[] = array(
            array(
                'pins' => array(),
                0 => false,
                1 => array(),
            ),
        );
        $args[] = array(
            array(
                'pins' => array(),
                0 => array(
                    'bwm_label' => true,
                    'transcript' => false,
                ),
                1 => array(),
            ),
        );
        $args[] = array(
            array(
                'pins' => array(),
                0 => array(
                    'bwm_label' => false,
                    'transcript' => true,
                ),
                1 => array(),
            ),
        );

        return $args;
    }

    private function getPinRetrieverMock()
    {
        return $this->getMockBuilder('PinRetriever')
            ->disableOriginalConstructor()
            ->getMock();
    }
}