<?php

class SourceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Source
     */
    private $sourceObj;

    protected function setUp()
    {
        $this->sourceObj = new Source;
    }

    public function testGetSwitchToPlus()
    {
        // Test switches on the homepage
        $result = $this->sourceObj->get('switch_to_plus', '/', 'A');
        $this->assertEquals('/Plus_Reg_Switch_A', $result);

        $result = $this->sourceObj->get('switch_to_plus', '/', 'B');
        $this->assertEquals('/Plus_Reg_Switch_B', $result);

        // Tests a switch on the homepage with url params
        $result = $this->sourceObj->get('switch_to_plus', '/?foo=bar', 'A');
        $this->assertEquals('/Plus_Reg_Switch_A', $result);

        // Tests a switch on a non-cro page
        $result = $this->sourceObj->get('switch_to_plus', '/Not-A-Cro-Page', 'A');
        $this->assertEquals('/Plus_Reg_Switch', $result);

    }

    public function testGetUnkownEvent()
    {
        $result = $this->sourceObj->get('unknown_event', '/', 'A');
        $this->assertEquals('unknown_event', $result);
    }
}
