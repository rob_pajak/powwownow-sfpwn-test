<?php
require_once dirname(__FILE__) . '/../../../../../lib/php/PinRetriever.php';
require_once dirname(__FILE__) . '/../../../lib/MockHermesClientRest.php';
require_once dirname(__FILE__) . '/../../../lib/FaultyMockHermesClientRest.php';

class PinRetrieverTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        MockHermesClientRest::$callCount = 0;
    }

    public function testSkipsPinsWithoutMasterPin()
    {
        MockHermesClientRest::$returnValue = array(
            array(
                'master_pin' => null,
                'pin_ref' => 1,
            ),
            array(
                'pin_ref' => 2,
            ),
            array(
                'master_pin' => true,
                'pin_ref' => 3,
            ),
        );
        $firstCallPins = array(3);
        $retriever = new PinRetriever('MockHermesClientRest');

        $this->assertEquals($firstCallPins, $retriever->retrievePinsFromContact(1));
        $this->assertEquals('getContactPins', MockHermesClientRest::$calledMethodName);
        $this->assertEquals(array('contact_ref' => 1), MockHermesClientRest::$calledParameters);
    }

    public function testCachesPins()
    {
        MockHermesClientRest::$returnValue = array(
            array(
                'master_pin' => true,
                'pin_ref' => 1,
            ),
            array(
                'master_pin' => true,
                'pin_ref' => 2,
            ),
            array(
                'master_pin' => true,
                'pin_ref' => 3,
            ),
        );
        $firstCallPins = array(1, 2, 3);
        $retriever = new PinRetriever('MockHermesClientRest');

        $this->assertEquals($firstCallPins, $retriever->retrievePinsFromContact(1));
        $this->assertEquals('getContactPins', MockHermesClientRest::$calledMethodName);
        $this->assertEquals(array('contact_ref' => 1), MockHermesClientRest::$calledParameters);

        $this->assertEquals($firstCallPins, $retriever->retrievePinsFromContact(1));
        $this->assertEquals('getContactPins', MockHermesClientRest::$calledMethodName);
        $this->assertEquals(array('contact_ref' => 1), MockHermesClientRest::$calledParameters);
        $this->assertEquals(1, MockHermesClientRest::$callCount);
    }

    public function testResetClearsCache()
    {
        MockHermesClientRest::$returnValue = array(
            array(
                'master_pin' => true,
                'pin_ref' => 1,
            ),
            array(
                'master_pin' => true,
                'pin_ref' => 2,
            ),
            array(
                'master_pin' => true,
                'pin_ref' => 3,
            ),
        );
        $firstCallPins = array(1, 2, 3);
        $retriever = new PinRetriever('MockHermesClientRest');

        $this->assertEquals($firstCallPins, $retriever->retrievePinsFromContact(1));
        $this->assertEquals('getContactPins', MockHermesClientRest::$calledMethodName);
        $this->assertEquals(array('contact_ref' => 1), MockHermesClientRest::$calledParameters);

        MockHermesClientRest::$returnValue = array(
            array(
                'master_pin' => true,
                'pin_ref' => 4,
            ),
            array(
                'master_pin' => true,
                'pin_ref' => 5,
            ),
            array(
                'master_pin' => true,
                'pin_ref' => 6,
            ),
        );
        $secondCallPins = array(4, 5, 6);

        $this->assertEquals($secondCallPins, $retriever->retrievePinsFromContact(1, true));
        $this->assertEquals('getContactPins', MockHermesClientRest::$calledMethodName);
        $this->assertEquals(array('contact_ref' => 1), MockHermesClientRest::$calledParameters);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Control Error
     */
    public function testForwardsExceptions()
    {
        $retriever = new PinRetriever('FaultyMockHermesClientRest');
        $retriever->retrievePinsFromContact(1);
    }
}