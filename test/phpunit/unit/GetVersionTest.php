<?php
/**
 * Class GetVersionTest
 * This is a Dummy Test to check the Hermes Client Rest and also to Log any Errors which may come from it.
 * The Logging file used is the pwn_test,log
 * @author Asfer Tamimi
 */
class GetVersionTest extends PHPUnit_Framework_TestCase
{
    public function testGetVersion() {
        try {
            $result = Hermes_Client_Rest::call('getVersion',array());
            $this->assertEquals('Hermes', $result['api name']);
            $this->assertEquals('Powwownow', $result['vendor']);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getVersion Hermes Call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getVersion Hermes Call Client Exception was called. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('getVersion Exception was called, Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }
    }
}
