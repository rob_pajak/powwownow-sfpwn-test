<?php
class FaultyMockHermesClientRest
{
    public static $controlErrorMessage = 'Control Error';

    public static function call($methodName, $parameters)
    {
        throw new Exception(self::$controlErrorMessage);
    }
}