<?php
class MockHermesClientRest
{
    public static $calledMethodName;
    public static $calledParameters;
    public static $returnValue;

    public static function call($methodName, $parameters)
    {
        self::$calledMethodName = $methodName;
        self::$calledParameters = $parameters;
        return array('product_group_id' => self::$returnValue);
    }
}