<?php
class BasePhpunitTestSuite extends sfBasePhpunitTestSuite implements sfPhpunitContextInitilizerInterface {

    protected function _start() {
        $this->_initFilters();
    }

    protected function _end() {
    }

    protected function _initFilters() {
        $filters = sfConfig::get('sf_phpunit_filter', array());
        foreach ($filters as $filter) {
            if (version_compare(PHPUnit_Runner_Version::id(), '3.5') >= 0) {
                $filter1 = new PHP_CodeCoverage_Filter();
                $filter1->addDirectoryToBlacklist($filter['path']);
            } elseif (version_compare(PHPUnit_Runner_Version::id(), '3.4') >= 0) {
                PHP_CodeCoverage_Filter::getInstance()->addDirectoryToBlacklist($filter['path']);
            } else {
                PHPUnit_Util_Filter::addDirectoryToFilter($filter['path'], $filter['ext']);
            }
        }
    }

    public function getApplication() {
        return 'pwn';
    }
}