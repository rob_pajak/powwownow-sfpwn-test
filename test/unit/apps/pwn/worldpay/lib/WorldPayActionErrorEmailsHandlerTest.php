<?php
require_once sfConfig::get('sf_apps_dir') . '/pwn/modules/worldpay/lib/WorldPayActionHandler.php';

/**
 * Unit tests for the WorldPayActionHandler class.
 * These tests are specific for the ErrorMessage Email Handling.
 * There should be around 15 of them.
 *
 * @author Asfer Tamimi
 */
class WorldPayActionErrorEmailsHandlerTest extends Test_Case_Unit
{
    private $request;
    private $addedProducts;
    private $gatewayDetails;
    private $accountAndContact;
    private $invoiceParameters;
    private $creditCardDetails;
    private $paymentDetails;
    private $logger;
    private $user;
    private $paymentSession;

    const ACCOUNT_ID = 24;

    /**
     * Test Set Up
     */
    public function _setUp()
    {
        parent::_setUp();

        $cartId          = '12345';
        $contactRef      = 42;
        $accountId       = self::ACCOUNT_ID;
        $paymentId       = '456465';
        $transactionTime = time() * 1000;

        $this->request           = $this->mapToRequest(
            array(
                'cartId'         => $cartId,
                'transId'        => '658974',
                'futurePayId'    => '45646',
                'transStatus'    => 'Y',
                'transTime'      => $transactionTime,
                'authAmount'     => 170,
                'authCurrency'   => 'GBP',
                'rawAuthMessage' => 'dGhlIGFuc3dlciBpcyA0Mg==',
                'rawAuthCode'    => 42,
                'M_action'       => 'checkout',
                'address1'       => 'the end of the lane',
                'address2'       => '',
                'address3'       => '',
                'town'           => 'London',
                'region'         => 'Greater London',
                'postcode'       => '',
                'country'        => 'GB',
            )
        );
        $this->addedProducts     = array(
            -1 => array(
                array(
                    'bwm_label' => 'My BWM',
                ),
                array(
                    'BWM_total_cost' => array(
                        'total'         => 100,
                        'connectionFee' => 50,
                        'qty'           => 1,
                        'dnisPerRate'   => 50,
                    ),
                ),
            )
        );
        $this->gatewayDetails    = array(
            'paymentGatewayId' => '45646546',
            'installationID'   => '12345',
        );
        $this->accountAndContact = array(
            'contact' => array('contact_ref' => $contactRef, 'email' => 'test-email@powwownow.com'),
            'account' => array('language_code' => 'en_GB'),
        );
        $this->invoiceParameters = array(
            'locale'          => 'en_GB',
            'invoice_date'    => date("d/m/Y"),
            'billing_period'  => date("M Y"),
            'invoice_no'      => "PP " . date("my"),
            'account_id'      => $accountId,
            'billing_address' => array(
                'organisation' => '',
                'building'     => '',
                'street'       => '',
                'town'         => '',
                'county'       => '',
                'post_code'    => '',
                'country'      => '',
            ),
            'items'           => array(
                array(
                    'order_no'   => 0,
                    'item'       => 'My BWM',
                    'amount_net' => 150,
                    'currency'   => 'GBP',
                )
            ),
            'invoice_summary' => array(
                'subtotal' => array(
                    'amount'   => 150,
                    'currency' => 'GBP',
                ),
                'vat'      => array(
                    'rate'     => '2,000',
                    'amount'   => 20,
                    'currency' => 'GBP',
                ),
                'total'    => array(
                    'amount'   => 170,
                    'currency' => 'GBP',
                )
            )
        );
        $this->creditCardDetails = array(
            'account_id'   => $accountId,
            'organisation' => 'the end of the lane',
            'building'     => '',
            'street'       => '',
            'town'         => 'London',
            'county'       => 'Greater London',
            'postal_code'  => '',
            'country'      => 'GBR',
        );
        $this->paymentDetails    = array(
            'payment_id'             => $paymentId,
            'payment_gateway_id'     => 1,
            'payment_method_id'      => null,
            'account_id'             => $accountId,
            'session_id'             => $cartId,
            'payment_amount'         => 150,
            'product_group_id'       => '',
            'invoice_id'             => '',
            'payment_time'           => date("Y-m-d H:i:s", $transactionTime / 1000),
            'gateway_status_code'    => 42,
            'gateway_status_message' => 'dGhlIGFuc3dlciBpcyA0Mg==',
            'gateway_transaction_id' => '658974',
            'transaction_status'     => 'Transaction Successful',
            'operation_type'         => 1,
            'description'            => 'BWM_PURCHASE',
            'recurring_payment_id'   => '45646',
            'autotopup_amount'       => 150,
        );

        $this->logger         = $this->getStubSfFileLogger();
        $this->user           = $this->getStubSfUser($this->addedProducts);
        $this->paymentSession = $this->createMockPaymentSession(base64_encode(serialize($this->addedProducts)));
    }

    /**
     * Retrieve Billing Address Exception Parameters
     *
     * @return array
     */
    public function retrieveBillingAddressExceptionParameters()
    {
        return array(
            array(
                'exception'     => new Hermes_Client_Request_Timeout_Exception("Request to server has timed out", array(0), 'GET', null, null),
                'error_message' => 'Plus.getPlusBillingAddress Hermes call has timed out. Error message: Request to server has timed out., Error Code: 0',
            ),
            array(
                'exception'     => new Hermes_Client_Exception(array(
                        'errorMessage' => "Some Error Message",
                        'errorCode'    => 42
                    ), array(0), 'GET', null, null),
                'error_message' => 'Plus.getPlusBillingAddress Hermes call is faulty. Error message: Some Error Message, Error Code: 42'
            ),
            array(
                'exception'     => new Exception("Some Error Message", 42),
                'error_message' => 'Plus.getPlusBillingAddress Failed. Error message: Some Error Message, Error Code: 42'
            )
        );
    }

    /**
     * Assert that the errors thrown during Failure of the Billing Address Hermes Call and are sent to the webteam.
     *
     * @dataProvider retrieveBillingAddressExceptionParameters
     */
    public function testErrorEmailIsSentForRetrieveBillingAddressExceptions(Exception $exception, $expectedErrorMessage)
    {
        $callbackHandler = $this->getMockCallbackHandler();

        // Do the Actual Test
        $callbackHandler->expects($this->once())
            ->method('retrieveBillingAddress')
            ->will($this->throwException($exception));
        $callbackHandler->expects($this->once())
            ->method('sendErrorEmail')
            ->with($expectedErrorMessage, 'WorldpayFailure :: Billing Address Error');

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback(
            $this->request,
            $this->logger,
            $callbackHandler,
            $this->gatewayDetails,
            $this->user
        );
    }

    /**
     * Retrieve Create Invoice DB Exception Parameters
     *
     * @return array
     */
    public function retrieveCreateInvoiceDBExceptionParameters()
    {
        return array(
            array(
                'exception'     => new Hermes_Client_Request_Timeout_Exception("Request to server has timed out", array(0), 'GET', null, null),
                'error_message' => 'Plus.createInvoiceDB Hermes call has timed out. Error message: Request to server has timed out., Error Code: 0',
            ),
            array(
                'exception'     => new Hermes_Client_Exception(array(
                        'errorMessage' => "Some Error Message",
                        'errorCode'    => 42
                    ), array(0), 'GET', null, null),
                'error_message' => 'Plus.createInvoiceDB Hermes call is faulty. Error message: Some Error Message, Error Code: 42'
            ),
            array(
                'exception'     => new Exception("Some Error Message", 42),
                'error_message' => 'Plus.createInvoiceDB Failed. Error message: Some Error Message, Error Code: 42'
            )
        );
    }

    /**
     * Assert that the errors thrown during Failure of the Create Invoice DB Hermes Call and are sent to the webteam.
     *
     * @dataProvider retrieveCreateInvoiceDBExceptionParameters
     */
    public function testErrorEmailIsSentForCreateInvoiceDBExceptions(Exception $exception, $expectedErrorMessage)
    {
        $callbackHandler = $this->getMockCallbackHandler();

        // Do the Actual Test
        $callbackHandler->expects($this->once())
            ->method('createInvoiceDB')
            ->will($this->throwException($exception));
        $callbackHandler->expects($this->once())
            ->method('sendErrorEmail')
            ->with($expectedErrorMessage, 'WorldpayFailure :: Create Invoice Error');

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback(
            $this->request,
            $this->logger,
            $callbackHandler,
            $this->gatewayDetails,
            $this->user
        );
    }

    /**
     * Retrieve Plus Credit Card Billing Address Exception Parameters
     *
     * @return array
     */
    public function retrieveUpdatePlusCreditCardAddressExceptionParameters()
    {
        return array(
            array(
                'exception'     => new Hermes_Client_Request_Timeout_Exception("Request to server has timed out", array(0), 'GET', null, null),
                'error_message' => 'Plus.updatePlusCreditCardAddress Hermes call has timed out. Error message: Request to server has timed out., Error Code: 0',
            ),
            array(
                'exception'     => new Hermes_Client_Exception(array(
                        'errorMessage' => "Some Error Message",
                        'errorCode'    => 42
                    ), array(0), 'GET', null, null),
                'error_message' => 'Plus.updatePlusCreditCardAddress Hermes call is faulty. Error message: Some Error Message, Error Code: 42'
            ),
            array(
                'exception'     => new Exception("Some Error Message", 42),
                'error_message' => 'Plus.updatePlusCreditCardAddress Failed. Error message: Some Error Message, Error Code: 42'
            )
        );
    }

    /**
     * Assert that the errors thrown during Failure of the UpdatePlusCreditCardAddress Hermes Call and are sent to the webteam.
     *
     * @dataProvider retrieveUpdatePlusCreditCardAddressExceptionParameters
     */
    public function testErrorEmailIsSentForUpdatePlusCreditCardAddressExceptions(Exception $exception, $expectedErrorMessage)
    {
        $callbackHandler = $this->getMockCallbackHandler();

        // Do the Actual Test
        $callbackHandler->expects($this->once())
            ->method('updatePlusCreditCardAddress')
            ->will($this->throwException($exception));
        $callbackHandler->expects($this->once())
            ->method('sendErrorEmail')
            ->with($expectedErrorMessage, 'WorldpayFailure :: Update Plus Credit Card Address');

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback(
            $this->request,
            $this->logger,
            $callbackHandler,
            $this->gatewayDetails,
            $this->user
        );
    }

    /**
     * Retrieve Process Payment Gateway Response Parameters
     *
     * @return array
     */
    public function retrieveProcessPaymentGatewayResponseExceptionParameters()
    {
        return array(
            array(
                'exception'     => new Hermes_Client_Request_Timeout_Exception("Request to server has timed out", array(0), 'GET', null, null),
                'error_message' => 'Plus.doProcessPaymentGatewayResponse Hermes call has timed out. Error message: Request to server has timed out., Error Code: 0',
            ),
            array(
                'exception'     => new Hermes_Client_Exception(array(
                        'errorMessage' => "Some Error Message",
                        'errorCode'    => 42
                    ), array(0), 'GET', null, null),
                'error_message' => 'Plus.doProcessPaymentGatewayResponse Hermes call is faulty. Error message: Some Error Message, Error Code: 42'
            ),
            array(
                'exception'     => new Exception("Some Error Message", 42),
                'error_message' => 'Plus.doProcessPaymentGatewayResponse Failed. Error message: Some Error Message, Error Code: 42'
            )
        );
    }

    /**
     * Assert that the errors thrown during Failure of the processPaymentGatewayResponse Hermes Call and are sent to the webteam.
     *
     * @dataProvider retrieveProcessPaymentGatewayResponseExceptionParameters
     */
    public function testErrorEmailIsSentForProcessPaymentGatewayResponseExceptions(Exception $exception, $expectedErrorMessage)
    {
        $callbackHandler = $this->getMockCallbackHandler();

        // Do the Actual Test
        $callbackHandler->expects($this->once())
            ->method('processPaymentGatewayResponse')
            ->will($this->throwException($exception));
        $callbackHandler->expects($this->once())
            ->method('sendErrorEmail')
            ->with($expectedErrorMessage, 'WorldpayFailure :: Process Payment');

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback(
            $this->request,
            $this->logger,
            $callbackHandler,
            $this->gatewayDetails,
            $this->user
        );
    }

    /**
     * Map To Request
     *
     * @param array $map
     * @return sfWebRequest (PHPUnit_Framework_MockObject_MockObject)
     */
    private function mapToRequest(array $map)
    {
        $request = $this->getMockBuilder('sfWebRequest')->disableOriginalConstructor()->getMock();
        $at      = 0;
        foreach ($map as $key => $value) {
            $request->expects($this->at($at))->method('getPostParameter')->with($key)->will($this->returnValue($value));
            $at++;
        }
        return $request;
    }

    /**
     * Get the SF File Logger Mock Object
     *
     * @return sfFileLogger (PHPUnit_Framework_MockObject_MockObject)
     */
    private function getStubSfFileLogger()
    {
        return $this->getMockBuilder('sfFileLogger')
            ->disableOriginalConstructor()
            ->setMethods(array('info', 'err'))
            ->getMock();
    }

    /**
     * Get the sfUser Mock Object
     * @param $addedProducts
     * @return myUser (PHPUnit_Framework_MockObject_MockObject)
     */
    private function getStubSfUser($addedProducts)
    {
        $user = $this->getMockBuilder('myUser')->disableOriginalConstructor()->getMock();
        $user->expects($this->any())
            ->method('getAttribute')
            ->with($this->identicalTo('basket-added'), $this->identicalTo(array()))
            ->will($this->returnValue($addedProducts));
        return $user;
    }

    /**
     * Do Initial Payment Mock Session
     *
     * @param string $paymentSessionProducts
     * @return PHPUnit_Framework_MockObject_MockObject $paymentSessionMock
     */
    private function createMockPaymentSession($paymentSessionProducts) {
        $paymentSessionMock = $this->getMockBuilder('PaymentSession')->disableOriginalConstructor()->getMock();
        $paymentSessionMock->expects($this->once())
            ->method('getAccountId')
            ->will($this->returnValue(self::ACCOUNT_ID));
        $paymentSessionMock->expects($this->once())
            ->method('getAddedProducts')
            ->will($this->returnValue($paymentSessionProducts));
        $paymentSessionMock->expects($this->once())
            ->method('getAutotopup')
            ->will($this->returnValue(true));
        $paymentSessionMock->expects($this->once())
            ->method('getAmountPreVAT')
            ->will($this->returnValue(150));
        $paymentSessionMock->expects($this->once())
            ->method('getVATPercentage')
            ->will($this->returnValue(20));

        return $paymentSessionMock;
    }

    /**
     * Return the Mock Worldpay Callback Handler
     *
     * @return WorldPayCallbackHandler (PHPUnit_Framework_MockObject_MockObject)
     */
    private function getMockCallbackHandler()
    {
        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')
            ->setMethods(
                array(
                    'two2three',
                    'retrievePaymentSession',
                    'retrieveAccountAndContactByAccountId',
                    'retrieveBillingAddress',
                    'sendErrorEmail',
                    'createInvoiceDB',
                    'updatePlusCreditCardAddress',
                    'processPaymentGatewayResponse',
                    'getPlusAccount',
                    'sendProductSummaryEmail',
                )
            )
            ->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($this->paymentSession));
        $callbackHandler->expects($this->once())
            ->method('two2three')
            ->will($this->returnValue('GBR'));
        $callbackHandler->expects($this->once())
            ->method('retrieveAccountAndContactByAccountId')
            ->with(self::ACCOUNT_ID)
            ->will($this->returnValue($this->accountAndContact));
        return $callbackHandler;
    }
}