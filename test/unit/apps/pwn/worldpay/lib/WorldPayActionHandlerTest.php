<?php

require_once sfConfig::get('sf_apps_dir') . '/pwn/modules/worldpay/lib/WorldPayActionHandler.php';

/**
 * Unit tests for the WorldPayActionHandler class.
 *
 * @author Maarten Jacobs
 */
class WorldPayActionHandlerTest extends Test_Case_Unit
{
    /**
     * Assert that empty requests result in a new line in the log.
     */
    public function testEmptyRequestResultsInErrorLog()
    {
        $handler = new WorldPayActionHandler();
        $request = $this->getStubRequest();
        $callbackHandler = $this->getStubCallbackHandler();
        $gatewayDetails = array(
            'paymentGatewayId' => '54321',
            'installationID' => '12345',
        );
        $user = $this->getStubUser();

        // The actual test: $logger->err() should be called.
        $loggerMock = $this->getMockBuilder('sfFileLogger')->disableOriginalConstructor()->getMock();
        $loggerMock->expects($this->once())
            ->method('err')
            ->with($this->stringStartsWith('Session ID / CartId Not Set. Payment Unknown / Cancelled. Request Args: '));

        $handler->handleMainCallback($request, $loggerMock, $callbackHandler, $gatewayDetails, $user);
    }

    /**
     * Assert that FuturePay cancellation calls can be handled through the mainCallback as well.
     */
    public function testRecurringPaymentAgreementsAreCancelledThroughMainCallback()
    {
        $futurePayId = '12132312';
        $accountId = 42;
        $handler = new WorldPayActionHandler();
        $gatewayDetails = array(
            // The paymentGatewayId is hardcoded for the moment in the production code, due to a failed transaction.
            'paymentGatewayId' => 1,
            'installationID' => '12345',
        );
        $logger = $this->getStubSfFileLogger();
        $user = $this->getStubUser();

        $callbackHandler = $this->getMock('WorldPayCallbackHandler', array('retrievePaymentSession', 'two2three', 'deactivateAutoTopupByTopupBundleID', 'notifyPostPayAdminForMissingRecurringPaymentAgreement'));
        $callbackHandler->expects($this->once())
            ->method('deactivateAutoTopupByTopupBundleID')
            ->with($this->equalTo($futurePayId), $this->equalTo($gatewayDetails['paymentGatewayId']))
            ->will($this->returnValue(array('account_id' => $accountId)));
        $request = $this->getMockBuilder('sfWebRequest')->disableOriginalConstructor()->getMock();
        $request->expects($this->at(2))->method('getPostParameter')->will($this->returnValue($futurePayId));
        $request->expects($this->at(17))->method('getPostParameter')->will($this->returnValue('Customer Cancelled'));
        $request->expects($this->at(18))->method('getPostParameter')->will($this->returnValue($gatewayDetails['installationID']));
        $request->expects($this->any())->method('getPostParameter')->will($this->returnValue(null));

        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    /**
     * Assert that only the gateway actions "topup" and "checkout" are handled.
     *
     * @dataProvider invalidActions
     */
    public function testOnlyValidGatewayActionsAreHandled($invalidAction)
    {
        $cartId = '4565464564';
        $gatewayDetails = array(
            'paymentGatewayId' => '45646546',
            'installationID' => '12345',
        );
        $user = $this->getStubUser();
        $handler = new WorldPayActionHandler();

        $request = $this->getMockBuilder('sfWebRequest')
            ->disableOriginalConstructor()
            ->getMock();
        $request->expects($this->at(0))
            ->method('getPostParameter')
            ->with('cartId')
            ->will($this->returnValue($cartId));
        $request->expects($this->at(9))
            ->method('getPostParameter')
            ->with('M_action')
            ->will($this->returnValue($invalidAction));
        $request->expects($this->any())->method('getPostParameter')->will($this->returnValue(null));

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')->disableOriginalConstructor()->getMock();
        $paymentSessionMock->expects($this->any())
            ->method('getAccountId')
            ->will($this->returnValue(42));

        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')
            ->setMethods(array('getPlusAccount', 'two2three', 'retrievePaymentSession', 'retrieveAccountAndContactByAccountId'))
            ->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->with($this->equalTo($cartId))
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->any())
            ->method('retrieveAccountAndContactByAccountId')
            ->with(42)
            ->will($this->returnValue(array('contact' => array('contact_ref' => '', 'email' => ''), 'account' => array('language_code' => ''))));

        $logger = $this->getMockBuilder('sfFileLogger')->disableOriginalConstructor()->getMock();
        $logger->expects($this->once())
            ->method('err')
            ->with('Gateway Action Was Not Topup, so was Invalid: ' . $invalidAction);

        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    public function invalidActions()
    {
        return array(
            array('invalid action'),
            array('')
        );
    }

    /**
     * Asserts that successful transactions trigger an invoice to be generated, and the credit card to be updated.
     *
     * @dataProvider commonTransactionParameters
     */
    public function testSuccessfulTransactionTriggersInvoiceAndCreditCardUpdate(array $requestMap, array $addedProducts, array $gatewayDetails, array $accountAndContact)
    {
        $accountId = 24;
        $paymentSessionProducts =  base64_encode(serialize($addedProducts));
        $invoiceParameters = array(
            'locale'           => 'en_GB',
            'invoice_date'     => date("d/m/Y"),
            'billing_period'   => date("M Y"),
            'invoice_no'       => "PP ".date("my"),
            'account_id'       => $accountId,
            'billing_address'  => array(
                'organisation' => '',
                'building'     => '',
                'street'       => '',
                'town'         => '',
                'county'       => '',
                'post_code'    => '',
                'country'      => '',
            ),
            'items'            => array(
                array(
                    'order_no'   => 0,
                    'item'       => 'My BWM',
                    'amount_net' => 150,
                    'currency'   => 'GBP',
                )
            ),
            'invoice_summary'    => array(
                'subtotal' => array(
                    'amount'   => 150,
                    'currency' => 'GBP',
                ),
                'vat' => array(
                    'rate'     => '2,000',
                    'amount'   => 20,
                    'currency' => 'GBP',
                ),
                'total' => array(
                    'amount'   => 170,
                    'currency' => 'GBP',
                )
            )
        );
        $creditCardDetails = array(
            'account_id'   => $accountId,
            'organisation' => 'the end of the lane',
            'building'     => '',
            'street'       => '',
            'town'         => 'London',
            'county'       => 'Greater London',
            'postal_code'  => '',
            'country'      => 'GBR',
        );
        $logger = $this->getStubSfFileLogger();
        $user = $this->getMockBuilder('myUser')->disableOriginalConstructor()->getMock();
        $user->expects($this->any())
            ->method('getAttribute')
            ->with($this->identicalTo('basket-added'), $this->identicalTo(array()))
            ->will($this->returnValue($addedProducts));

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')->disableOriginalConstructor()->getMock();
        $paymentSessionMock->expects($this->once())
            ->method('getAccountId')
            ->will($this->returnValue(24));
        $paymentSessionMock->expects($this->once())
            ->method('getAddedProducts')
            ->will($this->returnValue($paymentSessionProducts ));
        $paymentSessionMock->expects($this->once())
            ->method('getAutotopup')
            ->will($this->returnValue(true));
        $paymentSessionMock->expects($this->once())
            ->method('getAmountPreVAT')
            ->will($this->returnValue(150));
        $paymentSessionMock->expects($this->once())
            ->method('getVATPercentage')
            ->will($this->returnValue(20));

        $callbackHandlerMethods = array(
            'getPlusAccount',
            'two2three',
            'retrievePaymentSession',
            'retrieveAccountAndContactByAccountId',
            'retrieveBillingAddress',
            'createInvoiceDB',
            'sendErrorEmail',
            'updatePlusCreditCardAddress',
            'processPaymentGatewayResponse',
            'sendProductSummaryEmail',
        );
        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')->setMethods($callbackHandlerMethods)->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->once())
            ->method('two2three')
            ->will($this->returnValue('GBR'));
        $callbackHandler->expects($this->once())
            ->method('retrieveAccountAndContactByAccountId')
            ->with($accountId)
            ->will($this->returnValue($accountAndContact));
        $callbackHandler->expects($this->once())
            ->method('retrieveBillingAddress')
            ->will($this->returnValue(array()));
        $callbackHandler->expects($this->once())
            ->method('createInvoiceDB')
            ->with($this->identicalTo($invoiceParameters));
        $callbackHandler->expects($this->once())
            ->method('updatePlusCreditCardAddress')
            ->with($this->identicalTo($creditCardDetails));

        $request = $this->mapToRequest($requestMap);

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    private function mapToRequest(array $map)
    {
        $request = $this->getMockBuilder('sfWebRequest')
            ->disableOriginalConstructor()
            ->getMock();
        $at = 0;
        foreach ($map as $key => $value) {
            $request->expects($this->at($at))->method('getPostParameter')->with($key)->will($this->returnValue($value));
            $at++;
        }
        return $request;
    }

    /**
     * Asserts that successful transactions trigger the update on the transaction details.
     *
     * The transaction details are stored in the database, and displayed to the user.
     *
     * @dataProvider commonTransactionParameters
     */
    public function testSuccessfulTransactionTriggersTransactionUpdate(array $requestMap, array $addedProducts, array $gatewayDetails, array $accountAndContact)
    {
        $paymentId = '456465';
        $cartId = '12345';
        $accountId = 24;
        $paymentSessionProducts =  base64_encode(serialize($addedProducts));
        $paymentArgs = array(
            'payment_id'             => $paymentId,
            'payment_gateway_id'     => 1,
            'payment_method_id'      => null,
            'account_id'             => $accountId,
            'session_id'             => $cartId,
            'payment_amount'         => 150,
            'product_group_id'       => '',
            'invoice_id'             => '',
            'payment_time'           => date("Y-m-d H:i:s", $requestMap['transTime'] / 1000),
            'gateway_status_code'    => 42,
            'gateway_status_message' => 'dGhlIGFuc3dlciBpcyA0Mg==',
            'gateway_transaction_id' => '658974',
            'transaction_status'     => 'Transaction Successful',
            'operation_type'         => 1,
            'description'            => 'BWM_PURCHASE',
            'recurring_payment_id'   => '45646',
            'autotopup_amount'       => 150,
        );
        $logger = $this->getStubSfFileLogger();
        $user = $this->getMockBuilder('myUser')->disableOriginalConstructor()->getMock();
        $user->expects($this->any())
            ->method('getAttribute')
            ->with($this->identicalTo('basket-added'), $this->identicalTo(array()))
            ->will($this->returnValue($addedProducts));

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')->disableOriginalConstructor()->getMock();
        $paymentSessionMock->expects($this->once())
            ->method('getAccountId')
            ->will($this->returnValue(24));
        $paymentSessionMock->expects($this->once())
            ->method('getAddedProducts')
            ->will($this->returnValue($paymentSessionProducts ));
        $paymentSessionMock->expects($this->once())
            ->method('getAutotopup')
            ->will($this->returnValue(true));
        $paymentSessionMock->expects($this->once())
            ->method('getAmountPreVAT')
            ->will($this->returnValue(150));
        $paymentSessionMock->expects($this->once())
            ->method('getVATPercentage')
            ->will($this->returnValue(20));
        $paymentSessionMock->expects($this->once())
            ->method('getPaymentId')
            ->will($this->returnValue($paymentId));

        $callbackHandlerMethods = array(
            'getPlusAccount',
            'two2three',
            'retrievePaymentSession',
            'retrieveAccountAndContactByAccountId',
            'retrieveBillingAddress',
            'createInvoiceDB',
            'updatePlusCreditCardAddress',
            'processPaymentGatewayResponse',
            'sendProductSummaryEmail',
            'sendErrorEmail',
        );
        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')->setMethods($callbackHandlerMethods)->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->once())
            ->method('two2three')
            ->will($this->returnValue('GBR'));
        $callbackHandler->expects($this->once())
            ->method('retrieveAccountAndContactByAccountId')
            ->with($accountId)
            ->will($this->returnValue($accountAndContact));
        $callbackHandler->expects($this->once())
            ->method('processPaymentGatewayResponse')
            ->with($this->identicalTo($paymentArgs));

        $request = $this->mapToRequest($requestMap);

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    /**
     * Asserts that cancelled transactions trigger the update on the transaction details.
     *
     * The transaction details are stored in the database, and displayed to the user.
     * Cancelled transactions also need to be displayed, so they go through a part of the same process of the successful
     * transaction.
     *
     * @dataProvider commonTransactionParameters
     */
    public function testCancelledTransactionTriggersTransactionUpdate(array $requestMap, array $addedProducts, array $gatewayDetails, array $accountAndContact)
    {
        $paymentId = '456465';
        $accountId = 24;
        $paymentSessionProducts =  base64_encode(serialize($addedProducts));
        $requestMap['transStatus'] = 'C';
        $paymentArgs = array(
            'payment_id'             => $paymentId,
            'payment_gateway_id'     => 1,
            'payment_method_id'      => null,
            'account_id'             => $accountId,
            'session_id'             => $requestMap['cartId'],
            'payment_amount'         => 150,
            'product_group_id'       => '',
            'invoice_id'             => '',
            'payment_time'           => date("Y-m-d H:i:s", $requestMap['transTime'] / 1000),
            'gateway_status_code'    => 42,
            'gateway_status_message' => 'dGhlIGFuc3dlciBpcyA0Mg==',
            'gateway_transaction_id' => '658974',
            'transaction_status'     => 'Transaction Cancelled',
            'operation_type'         => 1,
            'description'            => 'BWM_PURCHASE',
            'recurring_payment_id'   => '45646',
            'autotopup_amount'       => 150,
        );
        $logger = $this->getStubSfFileLogger();
        $user = $this->getMockBuilder('myUser')->disableOriginalConstructor()->getMock();
        $user->expects($this->any())
            ->method('getAttribute')
            ->with($this->identicalTo('basket-added'), $this->identicalTo(array()))
            ->will($this->returnValue($addedProducts));

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')->disableOriginalConstructor()->getMock();
        $paymentSessionMock->expects($this->once())
            ->method('getAccountId')
            ->will($this->returnValue(24));
        $paymentSessionMock->expects($this->once())
            ->method('getAddedProducts')
            ->will($this->returnValue($paymentSessionProducts ));
        $paymentSessionMock->expects($this->once())
            ->method('getAutotopup')
            ->will($this->returnValue(true));
        $paymentSessionMock->expects($this->once())
            ->method('getAmountPreVAT')
            ->will($this->returnValue(150));
        $paymentSessionMock->expects($this->once())
            ->method('getVATPercentage')
            ->will($this->returnValue(20));
        $paymentSessionMock->expects($this->once())
            ->method('getPaymentId')
            ->will($this->returnValue($paymentId));

        $callbackHandlerMethods = array(
            'getPlusAccount',
            'two2three',
            'retrievePaymentSession',
            'retrieveAccountAndContactByAccountId',
            'retrieveBillingAddress',
            'createInvoiceDB',
            'updatePlusCreditCardAddress',
            'processPaymentGatewayResponse',
            'sendProductSummaryEmail',
            'sendErrorEmail',
        );
        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')->setMethods($callbackHandlerMethods)->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->once())
            ->method('two2three')
            ->will($this->returnValue('GBR'));
        $callbackHandler->expects($this->once())
            ->method('retrieveAccountAndContactByAccountId')
            ->with($accountId)
            ->will($this->returnValue($accountAndContact));
        $callbackHandler->expects($this->once())
            ->method('processPaymentGatewayResponse')
            ->with($this->identicalTo($paymentArgs));
        $callbackHandler->expects($this->never())->method('retrieveBillingAddress');
        $callbackHandler->expects($this->never())->method('createInvoiceDB');
        $callbackHandler->expects($this->never())->method('updatePlusCreditCardAddress');

        $request = $this->mapToRequest($requestMap);

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    public function commonTransactionParameters()
    {
        $cartId = '12345';
        $contactRef = 42;
        $transactionTime = time() * 1000;
        return array(
            array(
                array(
                    'cartId' => $cartId,
                    'transId' => '658974',
                    'futurePayId' => '45646',
                    'transStatus' => 'Y',
                    'transTime' => $transactionTime,
                    'authAmount' => 170,
                    'authCurrency' => 'GBP',
                    'rawAuthMessage' => 'dGhlIGFuc3dlciBpcyA0Mg==',
                    'rawAuthCode' => 42,
                    'M_action' => 'checkout',
                    'address1' => 'the end of the lane',
                    'address2' => '',
                    'address3' => '',
                    'town' => 'London',
                    'region' => 'Greater London',
                    'postcode' => '',
                    'country' => 'GB',
                ),
                array(
                    -1 => array(
                        array(
                            'bwm_label' => 'My BWM',
                        ),
                        array(
                            'BWM_total_cost' => array(
                                'total' => 100,
                                'connectionFee' => 50,
                                'qty' => 1,
                                'dnisPerRate' => 50,
                            ),
                        ),
                    )
                ),
                array(
                    'paymentGatewayId' => '45646546',
                    'installationID' => '12345',
                ),
                array(
                    'contact' => array('contact_ref' => $contactRef, 'email' => 'test-email@powwownow.com'),
                    'account' => array('language_code' => 'en_GB'),
                ),
            )
        );
    }

    /**
     * Assert that invalid cart IDs and missing sessions result in an error being logged.
     */
    public function testFuturePayWithInvalidSessionResultsInErrorLogged()
    {
        $invalidCartId = 'Not a cart ID; bring a towel!';
        $callbackHandler = $this->getStubCallbackHandler();
        $user = $this->getStubUser();

        $logger = $this->getMockBuilder('sfFileLogger')
            ->disableOriginalConstructor()
            ->getMock();
        $logger->expects($this->once())
            ->method('err')
            ->with($this->stringStartsWith('Session ID / CartId Not Set. Payment Unknown / Cancelled. Request Args: '));

        $handler = new WorldPayActionHandler();
        $handler->handleRecurringPaymentAgreementCallback($invalidCartId, null, $callbackHandler, $logger, $user);
    }

    /**
     * Assert that futurePay agreements can only be setup if the session is for auto-topup and has a futurePay id.
     *
     * @group works
     */
    public function testFuturePayRequiresAutoTopupSessionsAndFuturePayId()
    {
        $user = $this->getStubUser();

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')
            ->disableOriginalConstructor()
            ->getMock();
        $paymentSessionMock->expects($this->at(0))->method('getAutotopup')->will($this->returnValue(true));
        $paymentSessionMock->expects($this->at(1))->method('getAutotopup')->will($this->returnValue(false));

        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')
            ->setMethods(array('retrievePaymentSession'))
            ->disableOriginalConstructor()
            ->getMock();
        $callbackHandler->expects($this->exactly(2))
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));

        $logger = $this->getMockBuilder('sfFileLogger')
            ->disableOriginalConstructor()
            ->getMock();
        $logger->expects($this->exactly(2))
            ->method('err')
            ->with($this->stringStartsWith('Payment session was not marked as auto top-up and/or empty futurePayId. Request Args: '));

        $handler = new WorldPayActionHandler();

        // Empty futurePay ID:
        $handler->handleRecurringPaymentAgreementCallback(42, null, $callbackHandler, $logger, $user);

        // PaymentSession not marked as autoTopUp
        $handler->handleRecurringPaymentAgreementCallback(42, 654, $callbackHandler, $logger, $user);
    }

    /**
     * Assert that futurePay agreements are setup when given a valid session.
     * @group works
     */
    public function testFuturePayWithValidSessionIsSetup()
    {
        $accountId = 42;
        $futurePayId = 255;
        $logger = $this->getStubSfFileLogger();
        $user = $this->getStubUser();

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')
            ->disableOriginalConstructor()
            ->getMock();
        $paymentSessionMock->expects($this->once())->method('getAutotopup')->will($this->returnValue(true));
        $paymentSessionMock->expects($this->once())->method('getAccountId')->will($this->returnValue($accountId));

        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')
            ->setMethods(array('retrievePaymentSession', 'createTopupBundle'))
            ->disableOriginalConstructor()
            ->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->once())
            ->method('createTopupBundle')
            ->with($this->equalTo($accountId), $this->equalTo($futurePayId));

        $handler = new WorldPayActionHandler();
        $handler->handleRecurringPaymentAgreementCallback(22, $futurePayId, $callbackHandler, $logger, $user);
    }

    /**
     * Assert that the basket is not processed if it's empty.
     * @group works
     */
    public function testBasketNotProcessedWhenEmptyOnFuturePay()
    {
        $accountId = 42;
        $futurePayId = 255;
        $logger = $this->getStubSfFileLogger();
        $user = $this->getStubUser();

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')
            ->disableOriginalConstructor()
            ->getMock();
        $paymentSessionMock->expects($this->once())->method('getAutotopup')->will($this->returnValue(true));
        $paymentSessionMock->expects($this->once())->method('getAccountId')->will($this->returnValue($accountId));
        $paymentSessionMock->expects($this->once())->method('hasAddedProducts')->will($this->returnValue(false));
        $paymentSessionMock->expects($this->never())->method('getAddedProducts');

        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')
            ->setMethods(array('retrievePaymentSession', 'createTopupBundle'))
            ->disableOriginalConstructor()
            ->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->once())
            ->method('createTopupBundle')
            ->with($this->equalTo($accountId), $this->equalTo($futurePayId));

        $handler = new WorldPayActionHandler();
        $handler->handleRecurringPaymentAgreementCallback(22, $futurePayId, $callbackHandler, $logger, $user);
    }

    /**
     * Assert that the basket is processed on main callback, if there are added products and the invoice was generated.
     *
     * @dataProvider commonTransactionParameters
     * @group works
     */
    public function testBasketIsProcessedOnMainCallback(array $requestMap, array $addedProducts, array $gatewayDetails, array $accountAndContact)
    {
        $accountId = 24;
        $paymentSessionProducts =  base64_encode(serialize($addedProducts));
        $logger = $this->getStubSfFileLogger();
        $user = $this->getMockBuilder('myUser')->disableOriginalConstructor()->getMock();
        $summaryDetails = array('error' => false, 'error_messages' => array());
        $user->expects($this->any())
            ->method('getAttribute')
            ->with($this->identicalTo('basket-added'), $this->identicalTo(array()))
            ->will($this->returnValue($addedProducts));

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')->disableOriginalConstructor()->getMock();
        $paymentSessionMock->expects($this->any())
            ->method('getAccountId')
            ->will($this->returnValue(24));
        $paymentSessionMock->expects($this->once())
            ->method('getAddedProducts')
            ->will($this->returnValue($paymentSessionProducts ));
        $paymentSessionMock->expects($this->once())
            ->method('getAutotopup')
            ->will($this->returnValue(true));
        $paymentSessionMock->expects($this->exactly(2))
            ->method('getAmountPreVAT')
            ->will($this->returnValue(150));
        $paymentSessionMock->expects($this->once())
            ->method('getVATPercentage')
            ->will($this->returnValue(20));
        $paymentSessionMock->expects($this->any())
            ->method('getCartID')
            ->will($this->returnValue(4242));

        $callbackHandlerMethods = array(
            'getPlusAccount',
            'two2three',
            'retrievePaymentSession',
            'retrieveAccountAndContactByAccountId',
            'retrieveBillingAddress',
            'createInvoiceDB',
            'updatePlusCreditCardAddress',
            'processPaymentGatewayResponse',
            'sendProductSummaryEmail',
            'processBasket',
            'sendBasketProcessErrorMail',
            'storeProcessedPayment',
            'handleIncentivesForPlusPurchase',
            'sendErrorEmail',
        );
        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')->setMethods($callbackHandlerMethods)->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->once())
            ->method('two2three')
            ->will($this->returnValue('GBR'));
        $callbackHandler->expects($this->once())
            ->method('retrieveAccountAndContactByAccountId')
            ->with($accountId)
            ->will($this->returnValue($accountAndContact));
        $callbackHandler->expects($this->once())
            ->method('retrieveBillingAddress')
            ->will($this->returnValue(array()));
        $callbackHandler->expects($this->once())
            ->method('createInvoiceDB')
            ->will($this->returnValue(array('code' => 204)));
        $callbackHandler->expects($this->once())
            ->method('processBasket')
            ->will($this->returnValue($summaryDetails));
        $callbackHandler->expects($this->once())
            ->method('storeProcessedPayment')
            ->with($this->equalTo(4242), $this->equalTo(base64_encode(serialize($summaryDetails))));
        $callbackHandler->expects($this->never())
            ->method('sendBasketProcessErrorMail');
        $callbackHandler->expects($this->once())
            ->method('handleIncentivesForPlusPurchase');

        $request = $this->mapToRequest($requestMap);

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    /**
     * Assert that the errors thrown during basket processing are sent to the webteam.
     *
     * @dataProvider commonTransactionParameters
     */
    public function testBasketProcessorErrorsAreEmailed(array $requestMap, array $addedProducts, array $gatewayDetails, array $accountAndContact)
    {
        $accountId = 24;
        $paymentSessionProducts =  base64_encode(serialize($addedProducts));
        $logger = $this->getStubSfFileLogger();
        $user = $this->getMockBuilder('myUser')->disableOriginalConstructor()->getMock();
        $user->expects($this->any())
            ->method('getAttribute')
            ->with($this->identicalTo('basket-added'), $this->identicalTo(array()))
            ->will($this->returnValue($addedProducts));

        $paymentSessionMock = $this->getMockBuilder('PaymentSession')->disableOriginalConstructor()->getMock();
        $paymentSessionMock->expects($this->once())
            ->method('getAccountId')
            ->will($this->returnValue(24));
        $paymentSessionMock->expects($this->once())
            ->method('getAddedProducts')
            ->will($this->returnValue($paymentSessionProducts ));
        $paymentSessionMock->expects($this->once())
            ->method('getAutotopup')
            ->will($this->returnValue(true));
        $paymentSessionMock->expects($this->once())
            ->method('getAmountPreVAT')
            ->will($this->returnValue(150));
        $paymentSessionMock->expects($this->once())
            ->method('getVATPercentage')
            ->will($this->returnValue(20));

        $callbackHandlerMethods = array(
            'getPlusAccount',
            'two2three',
            'retrievePaymentSession',
            'retrieveAccountAndContactByAccountId',
            'retrieveBillingAddress',
            'createInvoiceDB',
            'updatePlusCreditCardAddress',
            'processPaymentGatewayResponse',
            'sendProductSummaryEmail',
            'processBasket',
            'sendBasketProcessErrorMail',
            'sendErrorEmail',
        );
        $callbackHandler = $this->getMockBuilder('WorldPayCallbackHandler')->setMethods($callbackHandlerMethods)->getMock();
        $callbackHandler->expects($this->once())
            ->method('retrievePaymentSession')
            ->will($this->returnValue($paymentSessionMock));
        $callbackHandler->expects($this->once())
            ->method('two2three')
            ->will($this->returnValue('GBR'));
        $callbackHandler->expects($this->once())
            ->method('retrieveAccountAndContactByAccountId')
            ->with($accountId)
            ->will($this->returnValue($accountAndContact));
        $callbackHandler->expects($this->once())
            ->method('retrieveBillingAddress')
            ->will($this->returnValue(array()));
        $callbackHandler->expects($this->once())
            ->method('createInvoiceDB')
            ->will($this->returnValue(array('code' => 204)));
        $callbackHandler->expects($this->once())
            ->method('processBasket')
            ->will($this->throwException(new Exception("PHP has ceased to exist. Bring a towel next time.", 42)));
        $callbackHandler->expects($this->once())
            ->method('sendBasketProcessErrorMail')
            ->with($this->equalTo(array('Basket Processor Failed. Error message: "PHP has ceased to exist. Bring a towel next time.", Error Code: 42')));

        $request = $this->mapToRequest($requestMap);

        $handler = new WorldPayActionHandler();
        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    /**
     * When a successful top-up call was made and Hermes has dealt with it, WorldPay makes a request to the main callback.
     * These need to be logged as information (which is what they are, as Hermes already handles them).
     *
     * @group failed1
     */
    public function testTopUpInformationalCallsAreLogged()
    {
        // Pay attention: this is important.
        // The response below is an actual request which resulted in an error line in the log, but is actually an
        // informational call. I am using this data, unserialised, to make sure it would work with similar requests.
        $informationalRequest = array (
            'installation' => '245372',
            'msgType' => 'authResult',
            'region' => '',
            'authAmountString' => '&#163;24.00',
            '_SP_charEnc' => 'UTF-8',
            'desc' => 'Payment 2 of FuturePay agreement ID 12111275',
            'tel' => '',
            'address1' => '',
            'countryMatch' => 'S',
            'cartId' => 'db97b6b516bf273b1c82a5ee431c99bb',
            'address2' => '',
            'address3' => '',
            'lang' => 'en',
            'callbackPW' => '',
            'rawAuthCode' => 'A',
            'amountString' => '&#163;24.00',
            'transStatus' => 'Y',
            'authCost' => '24.00',
            'currency' => 'GBP',
            'amount' => '24.00',
            'countryString' => 'United Kingdom',
            'displayAddress' => '8 Frankfurt Road


London',
            'name' => 'Mr R S Ede',
            'testMode' => '0',
            'transTime' => '1373440891472',
            'routeKey' => 'VISA_DEBIT-SSL',
            'ipAddress' => '',
            'fax' => '',
            'rawAuthMessage' => 'cardbe.msg.authorised',
            'instId' => '245372',
            'AVS' => '0111',
            'compName' => 'Via Vox Ltd',
            'futurePayId' => '12111275',
            'authAmount' => '24.00',
            'postcode' => 'SE24 9NY',
            'cardType' => 'Visa Delta',
            'cost' => '24.00',
            'authCurrency' => 'GBP',
            'country' => 'GB',
            'charenc' => 'UTF-8',
            'email' => 'russ.ede@lycamobile.com',
            'address' => '8 Frankfurt Road


London',
            'transId' => '399018724',
            'town' => '',
            'authMode' => 'A',
        );
        // Now I am transforming it, so that the Request mapper can build a Mock from it.
        $informationalRequest = array(
            'cartId' => $informationalRequest['cartId'],
            'transId' => $informationalRequest['transId'],
            'futurePayId' => $informationalRequest['futurePayId'],
            'transStatus' => $informationalRequest['transStatus'],
            'transTime' => $informationalRequest['transTime'],
            'authAmount' => $informationalRequest['authAmount'],
            'authCurrency' => $informationalRequest['authCurrency'],
            'rawAuthMessage' => $informationalRequest['rawAuthMessage'],
            'rawAuthCode' => $informationalRequest['rawAuthCode'],
            'M_action' => null,
            'address1' => $informationalRequest['address1'],
            'address2' => $informationalRequest['address2'],
            'address3' => $informationalRequest['address3'],
            'town' => $informationalRequest['town'],
            'region' => $informationalRequest['region'],
            'postcode' => $informationalRequest['postcode'],
            'country' => $informationalRequest['country'],
            'futurePayStatusChange' => null,
            'installation' => $informationalRequest['installation'],
            'desc' => $informationalRequest['desc'],
        );
        $request = $this->mapToRequest($informationalRequest);

        $futurePayId = $informationalRequest['futurePayId'];
        $handler = new WorldPayActionHandler();
        $gatewayDetails = array(
            // The paymentGatewayId is hardcoded for the moment in the production code, due to a failed transaction.
            'paymentGatewayId' => 1,
            'installationID' => $informationalRequest['installation'],
        );
        $logger = $this->getMockBuilder('sfFileLogger')
            ->disableOriginalConstructor()
            ->setMethods(array('info'))
            ->getMock();

        $logger->expects($this->at(4))
            ->method('info')
            ->with('A Recurring Payment was completed through Hermes. The FuturePay ID is ' . $futurePayId . '.');

        $user = $this->getStubUser();

        $callbackHandler = $this->getMock('WorldPayCallbackHandler', array('retrievePaymentSession', 'two2three'));

        $handler->handleMainCallback($request, $logger, $callbackHandler, $gatewayDetails, $user);
    }

    private function getStubRequest()
    {
        return $this->getMockBuilder('sfWebRequest')
            ->setMethods(array('getPostParameter'))
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function getStubCallbackHandler()
    {
        return $this->getMock('WorldPayCallbackHandler', array('retrievePaymentSession', 'two2three', 'sendErrorEmail'));
    }

    private function getStubUser()
    {
        return $this->getMockBuilder('myUser')->disableOriginalConstructor()->getMock();
    }

    private function getStubSfFileLogger()
    {
        return $this->getMockBuilder('sfFileLogger')
            ->disableOriginalConstructor()
            ->setMethods(array('info', 'err'))
            ->getMock();
    }
}
