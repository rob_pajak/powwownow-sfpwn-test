<?php

require_once sfConfig::get('sf_lib_dir') . '/php/BwmRenewal/BwmFutureRenewalProcess.php';

/**
 * @group bwm-renewal
 */
class BwmFutureRenewalProcessTest extends Test_Case_Unit
{
    const ACCOUNT_ID = 42;
    const ADMIN_EMAIL = 'foobar@powwownow.com';

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $emailSender;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $invoiceHandler;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $renewalStore;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $output;

    protected function _setUp()
    {
        parent::_setUp();

        $this->emailSender = $this->getMock('BwmRenewalEmailSenderInterface');
        $this->invoiceHandler = $this->getMock('BwmRenewalInvoiceHandlerInterface');
        $this->renewalStore = $this->getMock('BwmRenewalStoreInterface');
        $this->output = $this->getMock('CommandOutputInterface');

        $this->renewalStore->expects($this->once())
            ->method('retrieveDueBwmRenewals')
            ->will(
                $this->returnValue(
                    array(
                        array(
                            'account_id' => self::ACCOUNT_ID,
                            'account_email' => self::ADMIN_EMAIL,
                            'account_cyclic_payment' => 60,
                            'dedicated_number_count' => 1
                        )
                    )
                )
            );
    }

    public function testInvoiceIsCreatedForUpcomingBwmRenewals()
    {
        $this->invoiceHandler->expects($this->once())
            ->method('createPendingInvoice')
            ->with(self::ACCOUNT_ID, $this->isInstanceOf('DateTime'), 60, 1);

        $futureRenewalProcess = new BwmFutureRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, true);
        $futureRenewalProcess->handleFutureRenewalNotifications($this->output);
    }

    public function testEmailIsSentForUpcomingBwmRenewals()
    {
        $this->emailSender->expects($this->once())->method('sendPaymentDueEmail')->with(self::ACCOUNT_ID);

        $futureRenewalProcess = new BwmFutureRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, true);
        $futureRenewalProcess->handleFutureRenewalNotifications($this->output);
    }

    public function testEmailIsNotSentOnInvoiceFailure()
    {
        // Trigger an exception in the invoice creation process.
        $this->invoiceHandler->expects($this->once())
            ->method('createPendingInvoice')
            ->will($this->throwException(new Exception('Invoice creation failed.')));

        $this->emailSender->expects($this->never())->method('sendPaymentDueEmail');

        $futureRenewalProcess = new BwmFutureRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, true);
        $futureRenewalProcess->handleFutureRenewalNotifications($this->output);
    }

    public function testInvoiceExceptionIsLogged()
    {
        $invoiceErrorMessage = 'Invoice creation failed.';

        // Trigger an exception in the invoice creation process.
        $this->invoiceHandler->expects($this->once())
            ->method('createPendingInvoice')
            ->will($this->throwException(new Exception('Invoice creation failed.')));

        // Check if the logging occurs.
        $this->output->expects($this->at(2))
            ->method('logError')
            ->with($this->anything(), 'Failed to create pending invoice for the account %d.', self::ACCOUNT_ID);
        $this->output->expects($this->at(3))
            ->method('logError')
            ->with($this->anything(), 'Exception message: %s', $invoiceErrorMessage);

        $futureRenewalProcess = new BwmFutureRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, true);
        $futureRenewalProcess->handleFutureRenewalNotifications($this->output);
    }

    public function testEmailExceptionIsLogged()
    {
        $emailErrorMessage = 'Email service failed miserably.';

        // Trigger error in the email sending process.
        $this->emailSender->expects($this->once())
            ->method('sendPaymentDueEmail')
            ->will($this->throwException(new Exception($emailErrorMessage)));

        // Check if the logging occurs.
        $this->output->expects($this->at(4))
            ->method('logError')
            ->with($this->anything(), 'Failed to notify account %d for upcoming BWM renewal.', self::ACCOUNT_ID);
        $this->output->expects($this->at(5))
            ->method('logError')
            ->with($this->anything(), 'Exception message: %s', $emailErrorMessage);

        $futureRenewalProcess = new BwmFutureRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, true);
        $futureRenewalProcess->handleFutureRenewalNotifications($this->output);
    }

    public function testEmailIsNotSentOutsideProduction()
    {
        $this->emailSender->expects($this->never())->method('sendPaymentDueEmail');

        $futureRenewalProcess = new BwmFutureRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, false);
        $futureRenewalProcess->handleFutureRenewalNotifications($this->output);
    }

    public function testUpcomingBwmRenewalRetrievalFailureIsLogged()
    {
        $errorMessage = 'Invalid due date or DB has crashed mid-way Texas.';

        // Trigger an error in the upcoming BWM retrieval process.
        $this->renewalStore->expects($this->once())
            ->method('retrieveDueBwmRenewals')
            ->will($this->throwException(new Exception($errorMessage)));

        // Assert that the logger will be called to log the event with the error message.
        $this->output->expects($this->at(0))
            ->method('logError')
            ->with($this->anything(), 'Failed to retrieve upcoming BWM renewals.');
        $this->output->expects($this->at(1))
            ->method('logError')
            ->with(
                $this->anything(),
                'Exception message: %s',
                $errorMessage
            );

        $futureRenewalProcess = new BwmFutureRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, false);
        $futureRenewalProcess->handleFutureRenewalNotifications($this->output);
    }
}
