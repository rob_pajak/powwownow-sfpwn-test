<?php

require_once sfConfig::get('sf_lib_dir') . '/php/BwmRenewal/BwmDueRenewalProcess.php';

/**
 * @group bwm-renewal
 */
class BwmDueRenewalProcessTest extends Test_Case_Unit
{
    const PAYG_ACCOUNT_ID = 42;
    const AUTO_TOPUP_ACCOUNT_1 = 43;
    const AUTO_TOPUP_ACCOUNT_2 = 44;
    const PAYG_ACCOUNT_EMAIL = 'foobar1@powwownow.com';
    const AUTO_TOPUP_ACCOUNT_EMAIL_1 = 'foobar2@powwownow.com';
    const AUTO_TOPUP_ACCOUNT_EMAIL_2 = 'foobar3@powwownow.com';
    const TOPUP_BUNDLE_ID_1 = 1;
    const TOPUP_BUNDLE_ID_2 = 2;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $emailSender;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $invoiceHandler;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $renewalStore;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentHandler;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $output;

    protected function _setUp()
    {
        parent::_setUp();

        $this->emailSender = $this->getMock('BwmRenewalEmailSenderInterface');
        $this->invoiceHandler = $this->getMock('BwmRenewalInvoiceHandlerInterface');
        $this->renewalStore = $this->getMock('BwmRenewalStoreInterface');
        $this->paymentHandler = $this->getMock('BwmRenewalPaymentInterface');
        $this->output = $this->getMock('CommandOutputInterface');
    }

    public function testPaygAccountsAreSkipped()
    {
        $this->setDueRenewals(
            array(
                array(
                    'topup_bundle_id' => '',
                    'account_id' => self::PAYG_ACCOUNT_ID,
                    'account_email' => 'foobar@powwownow.com'
                )
            )
        );
        $this->paymentHandler->expects($this->never())->method('tryToTakePaymentForBwmRenewal');

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testPaymentIsTakenForDueRenewals()
    {
        $inProduction = true;
        $testMode = !$inProduction;
        $this->setDefaultDueRenewals();

        // Test if both auto-topup customers are charged.
        $this->paymentHandler->expects($this->at(0))
            ->method('tryToTakePaymentForBwmRenewal')
            ->with(self::AUTO_TOPUP_ACCOUNT_1, 120, self::TOPUP_BUNDLE_ID_1, $testMode);
        $this->paymentHandler->expects($this->at(1))
            ->method('tryToTakePaymentForBwmRenewal')
            ->with(self::AUTO_TOPUP_ACCOUNT_2, 60, self::TOPUP_BUNDLE_ID_2, $testMode);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, $inProduction);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testInvoiceIsMarkedAsPaidOnPaymentSuccess()
    {
        $this->setDefaultDueRenewals();

        // Test if both auto-topup customers are have their invoice marked as paid.
        $this->invoiceHandler->expects($this->at(0))->method('markInvoiceAsPaid')->with(self::AUTO_TOPUP_ACCOUNT_1);
        $this->invoiceHandler->expects($this->at(1))->method('markInvoiceAsPaid')->with(self::AUTO_TOPUP_ACCOUNT_2);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testAccountAdminIsNotifiedOfPaymentSuccess()
    {
        $this->setSingleDueRenewal();

        // Test if both auto-topup customers are notified.
        $this->emailSender->expects($this->at(0))
            ->method('sendInvoiceAvailableEmail')
            ->with(self::AUTO_TOPUP_ACCOUNT_EMAIL_1);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testAccountAdminIsNotifiedOfPaymentFailure()
    {
        $this->setSingleDueRenewal();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->once())
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        // Test if both auto-topup customers are notified.
        $this->emailSender->expects($this->at(0))
            ->method('sendPaymentNotProcessedEmail')
            ->with(self::AUTO_TOPUP_ACCOUNT_EMAIL_1);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testEmailGroupIsNotifiedOfPaymentFailure()
    {
        $this->setSingleDueRenewal();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->once())
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        // Test if both auto-topup customers are notified.
        $this->emailSender->expects($this->at(1))
            ->method('sendPaymentNotProcessedEmailToEmailGroup')
            ->with(BwmDueRenewalProcess::EMAIL_GROUP);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testInvoiceIsMarkedAsHavingFailedPaymentOnPaymentFailure()
    {
        $this->setSingleDueRenewal();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->once())
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        // Test if invoice is marked as having a failed payment.
        $this->invoiceHandler->expects($this->at(0))
            ->method('markBwmRenewalInvoiceAsHavingFailedPayment')
            ->with(self::AUTO_TOPUP_ACCOUNT_EMAIL_1);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testInvoiceIsNotMarkedAsPaidOnPaymentFailure()
    {
        $this->setDefaultDueRenewals();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->exactly(2))
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        // Test that invoice marking is never called.
        $this->invoiceHandler->expects($this->never())->method('markInvoiceAsPaid');

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testPaymentFailureExceptionIsLogged()
    {
        $errorMessage = 'Card declined.';

        $this->setSingleDueRenewal();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->once())
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception($errorMessage)));

        // Test that the payment failure is logged.
        $this->output->expects($this->at(2))
            ->method('logError')
            ->with(
                $this->anything(),
                'Failed to take payment from the account %d. The top-up bundle used has the id %d.',
                self::AUTO_TOPUP_ACCOUNT_1,
                self::TOPUP_BUNDLE_ID_1
            );
        $this->output->expects($this->at(3))
            ->method('logError')
            ->with($this->anything(), 'Exception message: %s', $errorMessage);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testMarkInvoiceFailureExceptionIsLogged()
    {
        $errorMessage = 'No pending BWM renewal invoice found.';

        $this->setSingleDueRenewal();

        // Trigger an error in the invoice-marking process.
        $this->invoiceHandler->expects($this->once())
            ->method('markInvoiceAsPaid')
            ->will($this->throwException(new Exception($errorMessage)));

        // Test that the invoice-marking failure is logged.
        $this->output->expects($this->at(3))
            ->method('logError')
            ->with(
                $this->anything(),
                'Failed to mark BWM renewal invoice as paid, for the account %d.',
                self::AUTO_TOPUP_ACCOUNT_1
            );
        $this->output->expects($this->at(4))
            ->method('logError')
            ->with($this->anything(), 'Exception message: %s', $errorMessage);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testPaymentSuccessEmailExceptionIsLogged()
    {
        $errorMessage = 'Email service failed hard.';

        $this->setSingleDueRenewal();

        // Trigger an error in the email process.
        $this->emailSender->expects($this->once())
            ->method('sendInvoiceAvailableEmail')
            ->will($this->throwException(new Exception($errorMessage)));

        // Test that the email failure is logged.
        $this->output->expects($this->at(4))
            ->method('logError')
            ->with(
                $this->anything(),
                'Failed to notify plus admin of available invoice, for the account %d.',
                self::AUTO_TOPUP_ACCOUNT_1
            );
        $this->output->expects($this->at(5))
            ->method('logError')
            ->with($this->anything(), 'Exception message: %s', $errorMessage);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testPaymentFailureEmailExceptionIsLogged()
    {
        $errorMessage = 'Email service failed hard.';

        $this->setSingleDueRenewal();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->once())
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        // Trigger an error in the email process.
        $this->emailSender->expects($this->once())
            ->method('sendPaymentNotProcessedEmail')
            ->will($this->throwException(new Exception($errorMessage)));

        // Test that the email failure is logged.
        $this->output->expects($this->at(5))
            ->method('logError')
            ->with(
                $this->anything(),
                'Failed to notify account %d about failed payment.',
                self::AUTO_TOPUP_ACCOUNT_1
            );
        $this->output->expects($this->at(6))
            ->method('logError')
            ->with($this->anything(), 'Exception message: %s', $errorMessage);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testEmailGroupNotificationExceptionIsLogged()
    {
        $errorMessage = 'Email service failed hard for email group.';

        $this->setSingleDueRenewal();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->once())
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        // Trigger an error in the email process.
        $this->emailSender->expects($this->once())
            ->method('sendPaymentNotProcessedEmailToEmailGroup')
            ->will($this->throwException(new Exception($errorMessage)));

        // Test that the triggered email is logged.
        $this->output->expects($this->at(6))
            ->method('logError')
            ->with(
                $this->anything(),
                'Failed to notify email group about failed payment for account %d.',
                self::AUTO_TOPUP_ACCOUNT_1
            );
        $this->output->expects($this->at(7))
            ->method('logError')
            ->with($this->anything(), 'Exception message: %s', $errorMessage);

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, true);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testInvoiceAvailableEmailIsNotSentOutsideProduction()
    {
        $this->setDefaultDueRenewals();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->exactly(2))
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        $this->emailSender->expects($this->never())->method('sendInvoiceAvailableEmail');

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, false);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testPaymentFailureEmailIsNotSentOutsideProduction()
    {
        $this->setDefaultDueRenewals();

        // Trigger an error in the payment-taking process.
        $this->paymentHandler->expects($this->exactly(2))
            ->method('tryToTakePaymentForBwmRenewal')
            ->will($this->throwException(new Exception));

        $this->emailSender->expects($this->never())->method('sendPaymentNotProcessedEmail');

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, false);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    public function testDueBwmRenewalsRetrievalFailureIsLogged()
    {
        $errorMessage = 'Invalid due date or DB has crashed mid-way Texas.';

        // Trigger an error in the due BWM retrieval process.
        $this->renewalStore->expects($this->once())
            ->method('retrieveDueBwmRenewals')
            ->will($this->throwException(new Exception($errorMessage)));

        // Assert that the logger will be called to log the event with the error message.
        $this->output->expects($this->at(0))
            ->method('logError')
            ->with($this->anything(), 'Failed to retrieve due BWM renewals.');
        $this->output->expects($this->at(1))
            ->method('logError')
            ->with(
                $this->anything(),
                'Exception message: %s',
                $errorMessage
            );

        $dueRenewalProcess = new BwmDueRenewalProcess($this->emailSender, $this->invoiceHandler, $this->renewalStore, $this->paymentHandler, false);
        $dueRenewalProcess->handleDuePayments($this->output);
    }

    private function setDueRenewals(array $dueRenewals)
    {
        $this->renewalStore->expects($this->once())
            ->method('retrieveDueBwmRenewals')
            ->will($this->returnValue($dueRenewals));
    }

    private function setDefaultDueRenewals()
    {
        $this->setDueRenewals(
            array(
                array(
                    'topup_bundle_id' => '',
                    'account_id' => self::PAYG_ACCOUNT_ID,
                    'account_email' => self::PAYG_ACCOUNT_EMAIL,
                    'account_cyclic_payment' => 60,
                ),
                array(
                    'topup_bundle_id' => self::TOPUP_BUNDLE_ID_1,
                    'account_id' => self::AUTO_TOPUP_ACCOUNT_1,
                    'account_email' => self::AUTO_TOPUP_ACCOUNT_EMAIL_1,
                    'account_cyclic_payment' => 120,
                ),
                array(
                    'topup_bundle_id' => self::TOPUP_BUNDLE_ID_2,
                    'account_id' => self::AUTO_TOPUP_ACCOUNT_2,
                    'account_email' => self::AUTO_TOPUP_ACCOUNT_EMAIL_2,
                    'account_cyclic_payment' => 60,
                ),
            )
        );
    }

    private function setSingleDueRenewal()
    {
        $this->setDueRenewals(
            array(
                array(
                    'topup_bundle_id' => self::TOPUP_BUNDLE_ID_1,
                    'account_id' => self::AUTO_TOPUP_ACCOUNT_1,
                    'account_email' => self::AUTO_TOPUP_ACCOUNT_EMAIL_1,
                    'account_cyclic_payment' => 120,
                ),
            )
        );
    }
}
