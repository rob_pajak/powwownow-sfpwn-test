/*global window, jQuery, dataLayer, ko, _, define, module*/
;(function (window, $, _, ko) {
    "use strict";
    if (typeof window.console === 'undefined') {
        window.console = {
            log: function() {},
            error: function() {},
            warn: function() {}
        };
    }

    var console = window.console;
    /**
     *
     * @constructor
     */
    function RegistrationInterface(trackingEvents, registrationSource, isLegacy) {

        if (_.isObject(trackingEvents)) {
            if (_.has(trackingEvents, 'FormSubmitEvent') && _.has(trackingEvents, 'registrationSuccessEvent')) {
                this.trackingEvents = trackingEvents;
            }
        } else {
            console.warn('Tracking Events, Fallback to Defaults');
            this.trackingEvents = {
                'FormSubmitEvent' : 'Registration/onClick',
                'registrationSuccessEvent': 'Registration/RegistrationSuccess'
            };
        }

        if (_.isEmpty(registrationSource)) {
            console.warn('Registration Source, Fallback to Defaults');
            registrationSource = 'GBR-CX-Header-Registration';
        }

        /**
         * @fixme
         *  Remove when we move all designs to CX
         */
        if (isLegacy === true) {
            this.isLegacy = true;
        } else {
            this.isLegacy = false;
        }

        /**
         * @desc
         *  Registration Model, data required to perform registration.
         * @type {{email: *, agree_tick: string, registration_source: string}}
         */
        this.registrationModel = {
            email: ko.observable(''),
            agree_tick: 'agreed',
            registration_source: registrationSource
        };

        /**
         * @desc
         *  Registration Errors Model, track all registration errors.
         * @type {{errors: *}}
         */
        this.registrationErrors = {
            errors: ko.observable('')
        };

        /**
         * @desc
         *  Controls what elements with registration form
         *  are enabled or visible.
         * @type {{email: *, submit: *, loader: *}}
         */
        this.isEnabled = {
            'email' : ko.observable(true),
            'submit': ko.observable(true),
            'loader': ko.observable(false)
        };
    }

    // Easy access to the prototype
    var proto = RegistrationInterface.prototype;

    /**
     * @desc
     *  form submission action.
     * @type {function(this:RegistrationInterface)|*}
     */
    proto.register = function register() {
        var emailAddress = this.registrationModel.email(),
            error = {};

        if (emailAddress) {
            if (this._validateEmail(emailAddress)) {
                try {
                    dataLayer.push({'event': this.trackingEvents.FormSubmitEvent});
                } catch (e) {
                    console.warn('DataLayer : ' + this.trackingEvents.FormSubmitEvent, e);
                } finally {
                    return this._registerUser();
                }
            } else {
                error = {error_messages:{message: 'CLIENT_INVALID_EMAIL_ADDRESS'}};
            }
        } else {
            error = {error_messages:{message: 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK'}};
        }
        this._renderErrors(error);
    };

    /**
     * @desc
     *  Close error message dialogue.
     * @type {function(this:RegistrationInterface)|*}
     */
    proto.closeErrorWindow = function closeErrorWindow() {
        if (this.registrationErrors.errors().length) {
            this.registrationErrors.errors('');
        }
    };

    /**
     * @desc
     *  alias for closeErrorWindow
     * @fixme
     *  Remove when we move all designs to CX.
     */
    proto.clearErrorWindowOnClick = function clearErrorWindowOnClick() {
        if (this.isLegacy) {
            this.closeErrorWindow();
        }
    };

    /**
     * @desc
     *  Validate a given email address.
     * @param emailAddress
     * @returns {boolean}
     * @private
     */
    proto._validateEmail = function _validateEmail (emailAddress) {
        var filter = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        return filter.test(emailAddress);
    };

    /**
     * @desc
     *  Perform server side registration.
     * @private
     */
    proto._registerUser = function _registerUser () {
        var _this = this;

        $.ajax({
            type: 'POST',
            url: '/United-Free-Pin-Registration',
            dataType: 'json',
            data: ko.toJS(_this.registrationModel),
            beforeSend:function(){
                _this.registrationErrors.errors('');
                _this.isEnabled.email(false);
                _this.isEnabled.submit(false);
                _this.isEnabled.loader(true);
            },
            success:function(response){
                var redirectUrl,
                    pinRefHash;

                if (_.isObject(response)) {
                    redirectUrl = response.redirect_url;
                    pinRefHash = response.pinRefHash;

                    try {
                        console.log('tracking' + _this.trackingEvents.registrationSuccessEvent);
                        dataLayer.push({'event': _this.trackingEvents.registrationSuccessEvent, 'PIN': pinRefHash});
                    } catch (e) {
                        console.warn('DataLayer : ' + _this.trackingEvents.registrationSuccessEvent, e);
                    } finally {
                        return setTimeout(function(){
                            window.location.replace(redirectUrl)
                        }, 1000);
                    }
                }
                _this._renderErrors('default');
            },
            error:function(jqXHR) {
                var errors = null;
                try {
                    errors = $.parseJSON(jqXHR.responseText);
                } catch (e) {
                    console.warn('KO : Setting Response error message to default', e);
                    // Generic Error Message
                    errors = 'technical error';
                } finally {
                    _this._renderErrors(errors);
                    _this.isEnabled.email(true);
                    _this.isEnabled.submit(true);
                }
            },
            complete: function() {
                _this.isEnabled.loader(false);
            }
        });
    };

    /**
     * @desc
     *  Render error onto registration_form template.
     * @param errors
     * @private
     */
    proto._renderErrors = function _renderErrors(errors) {
        var error,
            errorMessage = 'technical error';
        if (!_.isUndefined(errors)) {
            if (_.has(errors, 'error_messages') ) {
                if (_.isArray(errors.error_messages)) {
                    if (_.has(errors.error_messages[0], 'message')) {
                        errorMessage = errors.error_messages[0].message;
                    }
                } else if (_.isObject(errors.error_messages)) {
                    if (_.has(errors.error_messages, 'message')) {
                        errorMessage = errors.error_messages.message;
                    }
                }
            }
        }
        error = this._getErrorResponses(errorMessage);
        this.registrationErrors.errors(error);
    };

    /**
     * @desc
     *  Specific error messages for SEO landing page.
     * @param error
     * @returns {*}
     * @private
     */
    proto._getErrorResponses = function _getNewErrorResponses (error) {
        switch (error) {
            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN_OTHER':
            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN':
                error = 'Uh oh! Did you forget you have an account with us? ' +
                        ' <a href="/Login" target="_blank">Click here to Login</a> ' +
                        ' or <a target="_blank" href="http://www.powwownow.co.uk/Forgotten-Password">Click here for a password reminder</a>.';
                break;
            case 'CLIENT_INVALID_EMAIL_ADDRESS':
            case 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS':
                error = 'Oops! You made a typo, please enter a valid email.';
                break;
            case 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK':
                error = 'Looks like you forgot to type in your email.';
                break;
            default:
                error = 'Sorry, we are currently experiencing technical difficulties. Please try again later.';
        }
        return error;
    };

    // Expose the class either via AMD, CommonJS or the global object
    if (typeof define === 'function' && define.amd) {
        define('RegistrationInterface/RegistrationInterface',[],function () {
            return RegistrationInterface;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = RegistrationInterface;
    }
    else {
        window.RegistrationInterface = RegistrationInterface;
    }

    /**
     * @desc
     *  Custom handler to perform fadeIn/fadeOut effects
     * @type {{init: init, update: update}}
     */
    /* jshint ignore:start */
    ko.bindingHandlers.fadeVisible = {
        init: function(element, valueAccessor) {
            // Initially set the element to be instantly visible/hidden depending on the value
            var value = valueAccessor();
            // Use "unwrapObservable" so we can handle values that may or may not be observable
            $(element).toggle(ko.unwrap(value));
        },
        update: function(element, valueAccessor) {
            // Whenever the value subsequently changes, slowly fade the element in or out
            var value = valueAccessor(),
                isVisible = ko.unwrap(value);

            if (isVisible) {
                try {
                    $(element).velocity('fadeIn');
                } catch (e) {
                    $(element).fadeIn();
                }

            } else {
                try {
                    $(element).velocity('fadeOut', {duration: 1});
                } catch(e) {
                    $(element).fadeOut('fast');
                }

            }
        }
    };

//    ko.bindingHandlers.stopBinding = {
//        init: function() {
//            return { controlsDescendantBindings: true };
//        }
//    };
    /* jshint ignore:end */
})(window, jQuery, _, ko);