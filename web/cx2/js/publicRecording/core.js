/*global ko*/
/*global jQuery*/
/*global $*/
/*global dataLayer*/
/*global _*/

if (typeof console === "undefined") {
    window.console = {
        log: function () {},
        warn: function () {}
    };
}

$(document).ready(function() {

    $('#recording-password-submit').click(function(){
        $('#public-recording-password').submit();
        return false;
    });

    $(".password-field-fake").focus(function() {
        $(this).hide();
        $(".password-field").show();
        $(".password-field").focus();
    });

    $(".password-field").blur(function() {
        if ($(this).val() == "") {
            $(this).hide();
            $(".password-field-fake").show();
        }
    });
    
    $(".defaultText").blur();
    $('.password-field').change()

    $('#public-recording-password').submit(function(){
        var password = $(this).find('.password-field').val();
        var recordingRef = $(this).data('recording_ref');
        var self = $(this);
        $.post('/Recording-password',{password:password,recording_ref:recordingRef},function(response){
            if (response.status == true) {
                self.find('.form-error').hide();
                window.location.reload();
            } else {
                self.find('.form-error').show();
            }
        });
        return false;
    })


    var trackingEvents = {
            'FormSubmitEvent' : 'Generate Pin - GBR-Conference-Call/onClick',
            'registrationSuccessEvent': '/Conference-Call/RegistrationSuccess'
        },
        registrationSource = 'GBR-Public-Recordings',
        isLegacy = true;

    var MainRegistration = new RegistrationInterface(trackingEvents, registrationSource, isLegacy);

    //ko.applyBindings(MainRegistration, document.getElementById('ko.generic.registration.container'));
});