/*global window, jQuery, dataLayer, ko, _, jPlayerPlaylist, CirclePlayer, Quiz */
;(function (window, $, _, ko) {
    "use strict";

    $(function(){

        var Mplaylist = new jPlayerPlaylist({
            jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
        }, [
            {
                title: 'Pow Wow',
                mp3: '/myPwn/music.on.hold/powwowTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/1.jpg'
            },
            {
                title: 'PorkPie Dub',
                mp3: '/myPwn/music.on.hold/porkPieDubTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/2.jpg'
            },
            {
                title: 'Get on the Funk Bus',
                mp3: '/myPwn/music.on.hold/GetOnTheFunkBusTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/3.jpg'
            },
            {
                title: '21st Century Disco',
                mp3: '/myPwn/music.on.hold/CenturyDiscoTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/4.jpg'
            },
            {
                title: 'Global Celebration',
                mp3: '/myPwn/music.on.hold/globalCelebrationTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/5.jpg'
            },
            {
                title: 'Time to Shine',
                mp3: '/myPwn/music.on.hold/timeToShineTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/6.jpg'
            },
            {
                title: 'Eine Kleine Nacht',
                mp3: '/myPwn/music.on.hold/eineKleineNachtTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/7.jpg'
            },
            {
                title: 'Hispoaniola',
                mp3: '/myPwn/music.on.hold/hispoaniolaTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/8.jpg'
            },
            {
                title: 'Calm River',
                mp3: '/myPwn/music.on.hold/calmRiverTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/9.jpg'
            },
            {
                title: 'String Quartlet No.23',
                mp3: '/myPwn/music.on.hold/stringQuartetTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/10.jpg'
            }
        ], {
            swfPath: "/cx2/js/vendor/",
            supplied: "mp3",
            smoothPlayBar: true,
            keyEnabled: false,
            audioFullScreen: false
        });

        $("#jp_container_1").bind($.jPlayer.event.ended, function(event) {
            Mplaylist.pause();
        });

        /**
         *
         * @param questions
         * @param music
         * @constructor
         */
        function QuizViewModel(questions, music) {

            var _this = this;

            _this.music = music;

            _this.questions = questions;

            _this.answers = {
                one: ko.observable(),
                two: ko.observable(),
                three: ko.observable(),
                four: ko.observable(),
                five: ko.observable()
            };

            _this.currentSelected = ko.observableArray([{}]);

            _this.result = ko.observableArray([{}]);

            /**
             *
             * @param questionObject
             * @param currentSelectedId
             * @returns {*}
             * @private
             */
            _this._setCurrentSelectObservable = function _setCurrentSelectObservable(questionObject, currentSelectedId) {
                var currentSelectedObject = _this._findQuestionById(questionObject, currentSelectedId);
                _this.currentSelected(currentSelectedObject);
                return currentSelectedObject;
            };

            /**
             *
             * @param questionObject
             * @param questionId
             * @returns {*}
             * @private
             */
            _this._findQuestionById = function _findQuestionById(questionObject, questionId) {
                return _.findWhere(questionObject, {id: questionId});
            };

            _this.answers.one.subscribe(function(selectId) {
                _this._setCurrentSelectObservable(_this.questions.one, parseInt(selectId));
            });

            _this.answers.two.subscribe(function(selectId) {
                _this._setCurrentSelectObservable(_this.questions.two, parseInt(selectId));
            });

            _this.answers.three.subscribe(function(selectId) {
                _this._setCurrentSelectObservable(_this.questions.three, parseInt(selectId));
            });

            _this.answers.four.subscribe(function(selectId) {
                _this._setCurrentSelectObservable(_this.questions.four, parseInt(selectId));
            });

            _this.answers.five.subscribe(function(selectId) {
                _this._setCurrentSelectObservable(_this.questions.five, parseInt(selectId));
                _this._calculateResults();
            });

            _this.initialize = function initialize() {

                try {

                    _this.slider = $('#quiz-asset').bxSlider({
                        mode: 'horizontal',
                        adaptiveHeight: true,
                        infiniteLoop: false,
                        captions: false,
                        responsive: true,
                        useCSS: true,
                        touchEnabled: false,
                        pager: false,
                        controls: false,
                        onSliderLoad: function() {
                            $('#ri-grid').gridrotator({
                                rows: 3,
                                columns: 3,
                                w1024 : { rows : 3, columns : 3 },
                                w768 : {rows : 3, columns : 3 },
                                w480 : {rows : 3, columns : 3 },
                                w320 : {rows : 3, columns : 3 },
                                w240 : {rows : 3, columns : 3 }
                            });
                        }
                    });


                    if (_.isEmpty(_this.questions)) {
                        throw 'Question Object is empty.';
                    }

                    if (_.isEmpty(_this.music)) {
                        throw 'Music Object is empty.';
                    }

                } catch (e) {
                    console.warn(e);
                    alert(e);
                    $('#quiz-asset').hide();
                }
            };

            _this.startQuiz = function startQuiz() {
                try {
                    dataLayer.push({'event': 'Start Quiz/onClick'});
                } catch (e) {
                    // Do nothing
                }

                _this.nextQuestion();
            };

            /**
             *
             * @param data
             * @param event
             * @returns {boolean}
             */
            _this.endQuiz = function endQuiz(data, event) {
                dataLayer.push({'event': 'End Quiz/onClick'});
                var target;
                try {
                    target = event.target.href;
                    setTimeout(function(){window.location = target;}, 1000);
                } catch (e) {
                    console.log('Target Location Not Found, so could not sleep');
                    console.warn(e);
                    return true;
                }
            };

            _this.nextQuestion = function nextQuestion() {
                _this.slider.goToNextSlide();
                _this.currentSelected('');
            };

            _this.restartQuiz = function restartQuiz() {
                try {
                    _this.slider.goToSlide(0);
                } catch (e) {
                    // Do Nothing
                }
            };

            _this._calculateResults = function calculateResults() {

                var scores = [],
                    finalScore,
                    isShuffle,
                    result = 11,
                    musicResult;

                var q1 = _this._findQuestionById(_this.questions.one, _this.answers.one());
                var q2 = _this._findQuestionById(_this.questions.two, _this.answers.two());
                var q3 = _this._findQuestionById(_this.questions.three, _this.answers.three());
                var q4 = _this._findQuestionById(_this.questions.four, _this.answers.four());
                var q5 = _this._findQuestionById(_this.questions.five, _this.answers.five());

                try {
                    scores.push(q1.musicId, q2.musicId, q3.musicId, q4.musicId, q5.musicId);
                    finalScore = _.countBy(scores);
                    isShuffle = _.size(finalScore);
                } catch (e) {
                    isShuffle = 5;
                } finally {

                    if (isShuffle !== 5) {
                        result = _.invert(finalScore)[_.max(finalScore)];
                        result = parseInt(result);
                    } else {
                        // Last min change, shuffle no longer an option
                        result = 1;
                    }

                    musicResult = _.findWhere(_this.music, {id: result});
                    _this.result(musicResult);

                    var resultTrack = new CirclePlayer('#mobile_quiz_result_player', musicResult.trackKey,
                        { cssSelectorAncestor: '#mobile_quiz_result_player_container'}
                    );
                }
            };
        }


        ko.applyBindings(new QuizViewModel(Quiz.questions, Quiz.music), document.getElementById('quiz.asset.section.container'));
    });




})(window, jQuery, _, ko);