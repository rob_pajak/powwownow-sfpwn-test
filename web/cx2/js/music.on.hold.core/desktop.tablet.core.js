/*global window, jQuery, dataLayer, ko, _, Tracks, Quiz, CirclePlayer*/
;(function (window, $, _, ko) {
    "use strict";

    $(window).load(function() {

        var powwowTrack = new CirclePlayer('#music_catalogue_track_1', Tracks.powwowTrack,
            { cssSelectorAncestor: '#music_catalogue_track_container_1'}
        );

        $("#music_catalogue_track_1").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });


        var porkPieDubTrack = new CirclePlayer('#music_catalogue_track_2',Tracks.porkPieDubTrack,
            { cssSelectorAncestor: '#music_catalogue_track_container_2'}
        );

        $("#music_catalogue_track_2").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });


        var GetOnTheFunkBusTrack = new CirclePlayer('#music_catalogue_track_3', Tracks.GetOnTheFunkBusTrack,
            { cssSelectorAncestor: '#music_catalogue_track_container_3'}
        );

        $("#music_catalogue_track_3").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });


        var CenturyDiscoTrack = new CirclePlayer('#music_catalogue_track_4', Tracks.CenturyDiscoTrack,
            { cssSelectorAncestor: '#music_catalogue_track_container_4'}
        );

        $("#music_catalogue_track_4").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });

        var globalCelebrationTrack = new CirclePlayer('#music_catalogue_track_5', Tracks.globalCelebrationTrack,
            { cssSelectorAncestor: '#music_catalogue_track_container_5'}
        );

        $("#music_catalogue_track_5").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });


        var timeToShineTrack = new CirclePlayer('#music_catalogue_track_6', Tracks.timeToShineTrack,
            { cssSelectorAncestor: "#music_catalogue_track_container_6"}
        );

        $("#music_catalogue_track_6").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });

        var eineKleineNachtTrack = new CirclePlayer('#music_catalogue_track_7', Tracks.eineKleineNachtTrack,
            { cssSelectorAncestor: '#music_catalogue_track_container_7'}
        );

        $("#music_catalogue_track_7").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });

        var hispoaniolaTrack = new CirclePlayer('#music_catalogue_track_8', Tracks.hispoaniolaTrack,
            { cssSelectorAncestor: '#music_catalogue_track_container_8'}
        );

        $("#music_catalogue_track_8").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });

        var calmRiverTrack = new CirclePlayer('#music_catalogue_track_9', Tracks.calmRiverTrack,
            { cssSelectorAncestor: "#music_catalogue_track_container_9"}
        );

        $("#music_catalogue_track_9").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });

        var stringQuartetTrack = new CirclePlayer('#music_catalogue_track_10', Tracks.stringQuartetTrack,
            { cssSelectorAncestor: "#music_catalogue_track_container_10"}
        );

        $("#music_catalogue_track_10").bind($.jPlayer.event.pause, function() {
            $(this).jPlayer("stop");
        });

        /**
         * @desc
         *  Event for handling and Storing favourite track
         *  into cookie.
         */
        $('.js-favourite').on('click', function(event) {
            event.preventDefault();
            var newFavourite = $(this).data('track-id') || false,
                newFavouriteName = $(this).data('track-name') || false;

            try {
                if(newFavourite) {
                    var oldFavourite = $.cookie('currentFavourite');
                    $.cookie('currentFavourite', newFavourite);

                    // Update View
                    $('.js-favourite').find('span').removeClass('user-favourite').html('&#9734;');
                    $(this).find('span').addClass('user-favourite').html('&#9733;');

                    // Tracking
                    dataLayer.push({'event': 'Music-on-Hold-Favourite/onClick', 'TrackName': newFavouriteName});
                    if (oldFavourite !== null) {
                        var trackName;
                        try {
                            trackName = $('.album-art-favourite-layer[data-track-id=' + oldFavourite + ']').data('track-name');
                        } catch (e) {
                            console.log('Track Name not found for track-id: ' + oldFavourite);
                            console.warn(e);
                        }
                        dataLayer.push({
                            'event': 'Music-on-Hold-Unfavourite/onClick',
                            'TrackName': trackName
                        });
                    }
                }
            } catch (e) {
                console.log('Failed to Save Favourite', e);
            }
        });


        /**
         * @param questions {*}
         * @param music {*}
         * @constructor
         */
        function QuizViewModel(questions, music) {

            /**
             *
             * @type {QuizViewModel}
             * @private
             */
            var _this = this;

            /**
             * @desc
             *  Object contain on hold music
             *  [
             *       {
             *           id: int,
             *           title: string,
             *           description: string
             *       },
             *   ]
             * @type {*}
             */
            _this.music = music || {};

            /**
             * @desc
             *  Object of arrays contain Questions to display.
             *      {
             *          #: [
             *              {id: int,
             *              answer: string,
             *              description: string,
             *              cssSpriteClass: string, *optional
             *              musicId: int}
             *            ]
             *      }
             * @type {*|{}}
             */
            _this.questions = questions || {};

            /**
             * @desc
             *  Observable Object which tracks all answers.
             * @type {{one: *, two: *, three: *, four: *, five: *}}
             */
            _this.answers = {
                one: ko.observable(),
                two: ko.observable(),
                three: ko.observable(),
                four: ko.observable(),
                five: ko.observable()
            };

            /**
             * @desc
             *  Observable for presenting final result.
             * @type {*}
             */
            _this.result = ko.observableArray([{}]);

            /**
             * @desc
             *  Observable needed only for presentation.
             * @type {*}
             */
            _this.questionDescription = ko.observable('aaaa as das dasdas dassdasdasda asdasd as dassdasdas ');

            /**
             * @desc
             *  Called directly after post render. Any init code
             *  should be placed in here.
             *
             */
            _this.initialize = function initialize() {
                try {
                    _this.slider = $('#quiz-asset').bxSlider({
                        mode: 'horizontal',
                        adaptiveHeight: false,
                        infiniteLoop: false,
                        captions: false,
                        responsive: true,
                        useCSS: true,
                        touchEnabled: false,
                        pager: false,
                        controls: false
                    });

                    $('#ri-grid').gridrotator({
                        rows: 3,
                        columns: 3,
                        w1024 : { rows : 3, columns : 3 },
                        w768 : {rows : 3, columns : 3 },
                        w480 : {rows : 3, columns : 3 },
                        w320 : {rows : 3, columns : 3 },
                        w240 : {rows : 5, columns : 3 }
                    });

                    if (_.isEmpty(_this.questions)) {
                        throw 'Question Object is empty.';
                    }

                    if (_.isEmpty(_this.music)) {
                        throw 'Music Object is empty.';
                    }

                } catch (e) {
                    console.warn(e);
                    $('#quiz-asset').hide();
                }
            };

            /**
             * @desc
             *  Hide Description and move to next slide.
             * @private
             */
            _this._hideDescriptionAndMoveToNextSlide = function _hideDescriptionAndMoveToNextSlide() {
                _this.hideDescription();
                _this.slider.goToNextSlide();
            };

            /**
             * @desc
             *  alias for _hideDescriptionAndMoveToNextSlide();
             */
            _this.startQuiz = function startQuiz() {
                dataLayer.push({'event': 'Start Quiz/onClick'});
                _this._hideDescriptionAndMoveToNextSlide();
            };

            /**
             *
             * @param data
             * @param event
             * @returns {boolean}
             */
            _this.endQuiz = function endQuiz(data, event) {
                try {
                    dataLayer.push({'event': 'End Quiz/onClick'});
                } catch (e) {
                    // Do Nothing
                }

                try {
                    setTimeout(function(){window.location = event.target.href;}, 1000);
                } catch (e) {
                    console.warn(e);
                }
            };

            _this.goToSlide = function goToSlide(slideNumber) {
                try {
                    _this.slider.goToSlide(slideNumber);
                } catch (e) {
                    // Dont do anything
                }
            };

            /**
             * @desc
             *  Wrapper function. Populate observable with description to
             *  render in template.
             * @param description
             */
            _this.showDescription = function showDescription(description) {
                _this.questionDescription(description);
            };

            /**
             * @desc
             *  Wrapper function. Remove value from observable.
             */
            _this.hideDescription = function hideDescription() {
                _this.questionDescription('');
            };

            /**
             * @desc
             *  Wrapper function, populate question one observable.
             */
            _this.questionOne = function questionOne() {
                _this.answers.one(this);
                _this._hideDescriptionAndMoveToNextSlide();
            };

            /**
             * @desc
             *  Wrapper function, populate question two observable.
             */
            _this.questionTwo = function questionTwo() {
                _this.answers.two(this);
                _this._hideDescriptionAndMoveToNextSlide();
            };

            /**
             * @desc
             *  Wrapper function, populate question three observable.
             */
            _this.questionThree = function questionThree() {
                _this.answers.three(this);
                _this._hideDescriptionAndMoveToNextSlide();
            };

            /**
             * @desc
             *  Wrapper function, populate question four observable.
             */
            _this.questionFour = function questionFour() {
                _this.answers.four(this);
                _this._hideDescriptionAndMoveToNextSlide();
            };

            /**
             * @desc
             *  Wrapper function, populate question five observable
             *  and calculate results.
             */
            _this.questionFive = function questionFive() {
                _this.answers.five(this);
                _this._calculateResults();
                _this._hideDescriptionAndMoveToNextSlide();
            };

            /**
             * @desc
             *  Calculate the final result.
             */
            _this._calculateResults = function calculateResults() {

                var scores = [],
                    finalScore,
                    isShuffle,
                    result = 11,
                    musicResult;

                try {
                    scores.push(
                        _this.answers.one().musicId,
                        _this.answers.two().musicId,
                        _this.answers.three().musicId,
                        _this.answers.four().musicId,
                        _this.answers.five().musicId
                    );

                    finalScore = _.countBy(scores);
                    isShuffle = _.size(finalScore);
                } catch (e) {
                    // Fallback to shuffle
                    isShuffle = 5;
                } finally {

                    if (isShuffle !== 5) {
                        result = _.invert(finalScore)[_.max(finalScore)];
                        result = parseInt(result);
                    } else {
                        // Last min change, shuffle no longer an option
                        result = 1;
                    }

                    musicResult = _.findWhere(_this.music, {id: result});
                    _this.result(musicResult);
                }


                try {
                    var resultsPlayer = new CirclePlayer('#quiz_music_player', musicResult.trackKey,
                        { cssSelectorAncestor: '#quiz-music-player_container'}
                    );
                } catch (e) {
                    // Do Nothing
                }
            };
        }

        ko.applyBindings(new QuizViewModel(Quiz.questions, Quiz.music), document.getElementById('quiz.asset.section.container'));
    });

})(window, jQuery, _, ko);