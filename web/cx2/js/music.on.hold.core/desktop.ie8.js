/*global window, jQuery, dataLayer, ko, _*/
;(function (window, $, _, ko) {
    "use strict";

    $(function(){
        var Mplaylist = new jPlayerPlaylist({
            jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
        }, [
            {
                title: 'Pow Wow',
                mp3: '/myPwn/music.on.hold/powwowTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/1.jpg'
            },
            {
                title: 'PorkPie Dub',
                mp3: '/myPwn/music.on.hold/porkPieDubTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/2.jpg'
            },
            {
                title: 'Get on the Funk Bus',
                mp3: '/myPwn/music.on.hold/GetOnTheFunkBusTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/3.jpg'
            },
            {
                title: '21st Century Disco',
                mp3: '/myPwn/music.on.hold/CenturyDiscoTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/4.jpg'
            },
            {
                title: 'Global Celebration',
                mp3: '/myPwn/music.on.hold/globalCelebrationTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/5.jpg'
            },
            {
                title: 'Time to Shine',
                mp3: '/myPwn/music.on.hold/timeToShineTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/6.jpg'
            },
            {
                title: 'Eine Kleine Nacht',
                mp3: '/myPwn/music.on.hold/eineKleineNachtTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/7.jpg'
            },
            {
                title: 'Hispoaniola',
                mp3: '/myPwn/music.on.hold/hispoaniolaTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/8.jpg'
            },
            {
                title: 'Calm River',
                mp3: '/myPwn/music.on.hold/calmRiverTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/9.jpg'
            },
            {
                title: 'String Quartlet No.23',
                mp3: '/myPwn/music.on.hold/stringQuartetTrack.mp3',
                poster: '/cx2/img/music.on.hold/album/mobile/10.jpg'
            }
        ], {
            swfPath: "/cx2/js/vendor/",
            supplied: "mp3",
            smoothPlayBar: true,
            keyEnabled: false,
            audioFullScreen: false
        });

        $("#jp_container_1").bind($.jPlayer.event.ended, function(event) {
            Mplaylist.pause()
        });
    });

})(window, jQuery, _, ko);