/*global jQuery*/
(function($) {
    'use strict';
    $(function() {
        $('button').on('click', function() {
            var destination = '';

            try {
                destination = $(this).data('destination');
                window.open(destination,'_blank');
            } catch (e) {
            }
        });
    });
})(jQuery);
