/**
 * @desc
 *  Handle iMeet page
 * @author Jack Adams
 */
(function($) {
    'use strict';

    $(function () {
        var $yt_modal = $('#youtube_modal'),
            $yt_iframe = $yt_modal.children('iframe');

        $('#youtube_modal_close').on('click', function(ev) {
            $yt_modal.fadeOut(function() {
                $yt_iframe.attr('src', 'about:blank');
            });
        });

        $('#imeet-video').on('click', function(ev) {
            $yt_iframe.attr('src', $yt_iframe.data('src'));
            $yt_modal.fadeIn(400);
        });

    });

})(jQuery);

