// This is temporary JS until we build the global registration responsive.
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}

;(function (window, $, ko) {

        if (getQueryVariable('register') == 'true') {

            var element = $('#cx_registration'),
                elementState = element.data('registration-element-state'),
                self = this;

            if (elementState === 'closed') {
                try {
                    element.velocity('slideDown', {
                        duration: 400,
                        complete: function () {
                            if ($('.ie8').length) {
                                $(self).css('visibility', 'hidden');
                            } else {
                                $(self).velocity({
                                    opacity: 0,
                                    complete: function () {
                                        $(self).css('visibility', 'hidden');
                                    }
                                });
                            }
                            element.data('registration-element-state', 'open');
                        }
                    });
                } catch (e) {
                    console.log('Velocity not loaded falling back to Jquery');
                    element.slideToggle();
                }
            }
        };
})(window, jQuery, ko);