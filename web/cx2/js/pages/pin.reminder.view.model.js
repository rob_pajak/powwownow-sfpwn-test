/*global Class, BaseViewModel, ko, _*/

var PinReminderViewModel = Class.create();

PinReminderViewModel.prototype = Object.extend( new BaseViewModel(), {
    /**
     * @desc
     *  Model representing our form
     * @type {{email: *, mobileNumber: *}}
     */
    model: {
        email: ko.observable(''),
        mobileNumber: ko.observable('')
    },
    /**
     * @desc
     *  ko observable object controls our template.
     * @type {{errors: {message: *}, success: {message: *, displaySuccess: *}, isEnabled: {submit: *, loader: *}}}
     */
    ui: {
        errors : {
            'message' : ko.observable('')
        },
        success: {
            'message' : ko.observable(''),
            'displaySuccess' : ko.observable(false)
        },
        isEnabled : {
            'submit': ko.observable(true),
            'loader': ko.observable(false)
        }
    },

    /**
     * @desc
     *  initialize function required by default.
     */
    initialize: function() {},

    /**
     * @desc
     *  Validation for our form.
     * @returns {*}
     */
    validation: function validation () {
        var email = this.model.email(),
            mobileNumber = this.model.mobileNumber();

        if (_.isEmpty(email)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK'};
        }
        if (!this.validators.email(email)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS'};
        }
        if (!_.isEmpty(mobileNumber)) {
            if (!this.validators.mobile(mobileNumber)) {
                return {error: 'CLIENT_INVALID_MOBILE_NUMBER'};
            }
        }
        return true;
    },

    /**
     * @desc
     *  Do Form Submission
     */
    submit: function submit () {
        var $ = window.jQuery,
            _this = this;
        var isFormValid = this.validation();
        if (isFormValid === true) {
            $.ajax({
                type: 'POST',
                url: '/Pin-Reminder-Ajax',
                dataType: 'json',
                data: ko.toJS(_this.model),
                beforeSend:function(){
                    _this.ui.errors.message('');
                    _this.ui.isEnabled.loader(true);
                },
                success:function(){
                    return _this.ui.success.displaySuccess(true);
                },
                error:function(jqXHR) {
                    var errors = null;
                    try {
                        errors = $.parseJSON(jqXHR.responseText);
                    } catch (e) {
                        console.error('KO : Setting Response error message to default', e);
                        // Generic Error Message
                        errors = {error: 'TECHNICAL_ERROR'};
                    } finally {
                        _this.ui.isEnabled.submit(true);
                        _this.ui.errors.message(_this.getErrorMessage(errors));
                    }
                },
                complete: function() {
                    _this.ui.isEnabled.loader(false);
                }
            });
        } else {
            _this.ui.errors.message(_this.getErrorMessage(isFormValid));
        }
    }
});

var PinReminderViewModel = new PinReminderViewModel();

ko.applyBindings(PinReminderViewModel, document.getElementById('js.ko.pin.reminder.container'));
