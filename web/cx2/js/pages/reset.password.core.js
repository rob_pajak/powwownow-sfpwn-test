/*global document, ko, $, _, module, define*/
;(function (ko, $) {
    "use strict";

    try {
        $('.logo, .reset-password-container').velocity('fadeIn', {duration: 1000});
    } catch (e) {
        try {
            $('.reset-password-container').fadeIn(1000);
        } catch (err) {
            $('.reset-password-container').css('display', 'block');
        }
    }

    function ResetPasswordViewModel() {
        var _this = this;

        /**
         * @desc
         *  Ko observable of our form.
         * @type {{resetpassword: {password: *, confirm_password: *, contact: *, token: *}}}
         */
        _this.model = {
            'resetpassword' : {
                'password' : ko.observable(''),
                'confirm_password' : ko.observable(''),
                'contact' : ko.observable(''),
                'token' : ko.observable('')
            }
        };

        /**
         * @desc
         *  Ko observable for controlling template logic
         * @type {{errors: {message: *}, success: {message: *, displaySuccess: *}, isEnabled: {submit: *, loader: *}}}
         */
        _this.ui = {
            errors : {
                'message' : ko.observable('')
            },
            success: {
                'message' : ko.observable(''),
                'displaySuccess' : ko.observable(false)
            },
            isEnabled : {
                'submit': ko.observable(true),
                'loader': ko.observable(false)
            }
        };
    }

    /**
     * @desc
     *  Easy access to proto.
     * @type {Object|Function|ResetPasswordViewModel}
     */
    var proto = ResetPasswordViewModel.prototype;

    /**
     * @desc
     *  Do basic form validation.
     * @returns {*} | Boolean
     * @private
     */
    proto._doFormValidation = function _doFormValidation() {
        var _this = this,
            password = _this.model.resetpassword.password(),
            confirmPassword = _this.model.resetpassword.confirm_password();

        if (_.isEmpty(password)) {
            return {error: 'CLIENT_INVALID_PASSWORD'};
        }

        if (password.length < 6) {
            return {error: 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS'};
        }

        if (_.isEmpty(confirmPassword)) {
            return {error: 'CLIENT_INVALID_CONFIRM_PASSWORD_BLANK'};
        }

        if (password !== confirmPassword) {
            return {error: 'CLIENT_INVALID_PASSWORD_MISMATCH'};
        }
        return true;
    };

    /**
     * @desc
     *  Called immediately after template has been rendered, setup
     *  vars needed for form submission.
     * @param token
     * @param contactRef
     */
    proto.initialise = function initialise(token, contactRef) {
        var _this = this;
        _this._setToken(token);
        _this._setContactRef(contactRef);
    };

    /**
     * @desc
     *  Set form token
     * @param token
     * @private
     */
    proto._setToken = function _setToken(token) {
        var _this = this;
        _this.model.resetpassword.token(token);
    };

    /**
     * @desc
     *  Set form contact ref
     * @param contactRef
     * @private
     */
    proto._setContactRef = function _setContactRef(contactRef) {
        var _this = this;
        _this.model.resetpassword.contact(contactRef);
    };

    /**
     * @desc
     *  Password reset
     */
    proto.doResetPassword = function doLogin() {
        var _this = this,
            formValidationStatus;

        formValidationStatus = _this._doFormValidation();

        if (formValidationStatus === true) {
            $.ajax({
                type: 'POST',
                url: '/Reset-Password-Ajax',
                dataType: 'json',
                data: ko.toJS(_this.model),
                beforeSend:function(){
                    _this.ui.errors.message('');
                    _this.ui.isEnabled.loader(true);
                },
                success:function(){
                    _this.ui.success.displaySuccess(true);
                },
                error:function(jqXHR) {
                    var errors = null;
                    try {
                        errors = $.parseJSON(jqXHR.responseText);
                    } catch (e) {
                        console.warn('KO : Setting Response error message to default', e);
                        // Generic Error Message
                        errors = {error: 'TECHNICAL_ERROR'};
                    } finally {
                        _this.ui.isEnabled.submit(true);
                        _this._renderErrors(errors);
                    }
                },
                complete: function() {
                    _this.ui.isEnabled.loader(false);
                }
            });
        } else {
            _this._renderErrors(formValidationStatus);
            return false;
        }
    };

    /**
     * @desc
     *  Render error onto registration_form template.
     * @param errors
     * @private
     */
    proto._renderErrors = function _renderErrors(errors) {

        var _this = this,
            error,
            errorMessage = 'TECHNICAL_ERROR';

        if (!_.isUndefined(errors)) {
            if(_.isObject(errors)) {
                if (_.has(errors, 'error')) {
                    errorMessage = errors.error;
                }
            }
        }

        error = _this._getErrorResponses(errorMessage);
        _this.ui.errors.message(error);
    };

    /**
     * @desc
     *  Specific error messages for Login Page
     * @param error
     * @returns {*}
     * @private
     */
    proto._getErrorResponses = function _getNewErrorResponses (error) {
        switch (error) {
            case 'CLIENT_INVALID_PASSWORD':
                error = 'Looks like you forgot to choose a password.';
                break;
            case 'CLIENT_INVALID_CONFIRM_PASSWORD_BLANK':
                error = 'Uh oh! Please confirm your password to continue.';
                break;
            case 'CLIENT_INVALID_PASSWORD_MISMATCH':
            case 'FORM_VALIDATION_PASSWORD_MISMATCH':
                error = 'Oops! Your passwords don’t match, please try again.';
                break;
            case 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS':
                error = 'Sorry! Your new password needs to be at least 6 characters long. ';
                break;
            case 'FORM_VALIDATION_FORGOT_PASSWORD_EXPIRED_TOKEN':
                error = 'You’re too late, To protect your account from nasty hackers, reset password links only work for 24 hours.';
                break;
            default:
                error = 'Sorry, we are currently experiencing technical difficulties. Please try again later.';
        }
        return error;
    };


    // Expose the class either via AMD, CommonJS or the global object
    if (typeof define === 'function' && define.amd) {
        define('ResetPasswordViewModel/ResetPasswordViewModel',[],function () {
            return ResetPasswordViewModel;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = ResetPasswordViewModel;
    }
    else {
        window.ResetPasswordViewModel = ResetPasswordViewModel;
    }

    var resetPasswordViewModel = new ResetPasswordViewModel();

    ko.applyBindings(resetPasswordViewModel, document.getElementById('js.ko.reset.password.container'));

})(ko, window.jQuery);
