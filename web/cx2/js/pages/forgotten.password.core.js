/*global document, ko, $, define, module, _*/
;(function (ko, $) {
    "use strict";

    try {
        $('.logo, .forgotten-password-container, .new-customer-container, .pin-reminder-container').velocity('fadeIn', {duration: 1000});
    } catch (e) {
        try {
            $('.forgotten-password-container, .new-customer-container, .pin-reminder-container').fadeIn(1000);
        } catch (err) {
            $('.forgotten-password-container, .new-customer-container, .pin-reminder-container').css('display', 'block');
        }
    }

    /**
     *
     * @constructor
     */
    function ForgottenPasswordViewModel() {
        var _this = this;

        /**
         * @desc
         *  Ko observable represents our form
         * @type {{email: *}}
         */
        _this.model = {
            email : ko.observable('')
        };

        /**
         * @desc
         *  ko observable object controlls our template.
         * @type {{errors: {message: *}, success: {message: *, displaySuccess: *}, isEnabled: {submit: *, loader: *}}}
         */
        _this.ui = {
            errors : {
                'message' : ko.observable('')
            },
            success: {
                'message' : ko.observable(''),
                'displaySuccess' : ko.observable(false)
            },
            isEnabled : {
                'submit': ko.observable(true),
                'loader': ko.observable(false)
            }
        };
    }

    /**
     * @desc
     *  Easy access to proto.
     * @type {Object|Function|ForgottenPasswordViewModel}
     */
    var proto = ForgottenPasswordViewModel.prototype;

    /**
     * @desc
     *  Perform basic validation
     * @returns {*} | Boolean
     * @private
     */
    proto._doFormValidation = function _doFormValidation() {
        var _this = this,
            emailAddress = _this.model.email();

        if (!_this._validateEmail(emailAddress)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS'};
        }
        if (_.isEmpty(emailAddress)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK'};
        }
        return true;
    };

    /**
     * @desc
     *  Validate a given email address.
     * @param emailAddress
     * @returns {boolean}
     * @private
     */
    proto._validateEmail = function _validateEmail (emailAddress) {
        var filter = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        return filter.test(emailAddress);
    };

    /**
     * @desc
     *  Do login
     */
    proto.doLogin = function doLogin() {
        var _this = this,
            formValidationStatus;

        formValidationStatus = _this._doFormValidation();

        if (formValidationStatus === true) {
            $.ajax({
                type: 'POST',
                url: '/Forgotten-Password-Ajax',
                dataType: 'json',
                data: ko.toJS(_this.model),
                beforeSend:function(){
                    _this.ui.errors.message('');
                    _this.ui.isEnabled.loader(true);
                },
                success:function(){
                    _this.ui.success.displaySuccess(true);
                },
                error:function(jqXHR) {
                    var errors = null;
                    try {
                        errors = $.parseJSON(jqXHR.responseText);
                    } catch (e) {
                        console.warn('KO : Setting Response error message to default', e);
                        // Generic Error Message
                        errors = {error: 'TECHNICAL_ERROR'};
                    } finally {
                        _this.ui.isEnabled.submit(true);
                        _this._renderErrors(errors);
                    }
                },
                complete: function() {
                    _this.ui.isEnabled.loader(false);
                }
            });
        } else {
            _this._renderErrors(formValidationStatus);
        }
    };

    /**
     * @desc
     *  Render error onto registration_form template.
     * @param errors
     * @private
     */
    proto._renderErrors = function _renderErrors(errors) {

        var _this = this,
            error,
            errorMessage = 'TECHNICAL_ERROR';

        if (!_.isUndefined(errors)) {
            if(_.isObject(errors)) {
                if (_.has(errors, 'error')) {
                    errorMessage = errors.error;
                }
            }
        }
        error = _this._getErrorResponses(errorMessage);
        _this.ui.errors.message(error);
    };

    /**
     * @desc
     *  Specific error messages for Login Page
     * @param error
     * @returns {*}
     * @private
     */
    proto._getErrorResponses = function _getNewErrorResponses (error) {
        
        switch (error) {
            case 'CLIENT_INVALID_EMAIL_ADDRESS':
            case 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS':
                error = 'Oops! You made a typo, please enter a valid email.';
                break;
            case 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK':
                error = 'Looks like you forgot to type in your email.';
                break;
            case 'FORGOTTEN_PASSWORD_UNKNOWN_ERROR':
            case 'API_RESPONSE_NO_ACCOUNT_FOR_EMAIL':
                error = 'Oops!, we could not find that email address.';
                break;
            default:
                error = 'Sorry, we are currently experiencing technical difficulties. Please try again later.';
        }
        return error;
    };


    // Expose the class either via AMD, CommonJS or the global object
    if (typeof define === 'function' && define.amd) {
        define('ForgottenPasswordViewModel/ForgottenPasswordViewModel', [], function () {
            return ForgottenPasswordViewModel;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = ForgottenPasswordViewModel;
    }
    else {
        window.ForgottenPasswordViewModel = ForgottenPasswordViewModel;
    }

    var forgottenPasswordViewModel = new ForgottenPasswordViewModel();

    ko.applyBindings(forgottenPasswordViewModel, document.getElementById('js.ko.forgotten.password.container'));


})(ko, window.jQuery);