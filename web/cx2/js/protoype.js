var Pwn = Pwn || { Utilities: {} };

Pwn.Utilities.getFormPostUrl = function getFormPostUrl($form) {
    return $form.attr('action');
};

Pwn.Utilities.renderErrorsToUiLegacy = function renderErrorsToUiLegacy(errorResponse) {
    var inputField, errorMessageToDisplay;

    if (_.isArray(errorResponse.error_messages)) {
        inputField = errorResponse.error_messages[0].field_name;
        errorMessageToDisplay = window[errorResponse.error_messages[0].message];
    } else if (_.isObject(errorResponse.error_messages)) {
        inputField = errorResponse.error_messages.field_name;
        errorMessageToDisplay = window[errorResponse.error_messages.message];
    } else {
        alert('We are currently experiencing a Technical problem.');
        return false;
    }
    // selector is deprecated in Jquery 1.9
    $(this.forms.$form1.selector + ' input[name="' + inputField + '"]')
        .after('<div class="error_message">' + errorMessageToDisplay + '</div>');
};