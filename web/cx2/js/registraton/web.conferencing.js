/*global RegistrationInterface, document, ko*/
;(function (ko) {
    "use strict";
    var trackingEvents = {
            'FormSubmitEvent' : 'Generate Pin - Web-Conferencing/onClick',
            'registrationSuccessEvent': '/WebConferencing/RegistrationSuccess'
        },
        registrationSource = 'GBR-Web-Conferencing',
        isLegacy = true;

    var MainRegistration = new RegistrationInterface(trackingEvents, registrationSource, isLegacy);

    ko.applyBindings(MainRegistration, document.getElementById('ko.webConference.registration.container'));
})(ko);
