/*global RegistrationInterface, document, ko*/
;(function (ko) {
    "use strict";
    var trackingEvents = {
            'FormSubmitEvent' : 'Generate Pin - GBR-Teleconference/onClick',
            'registrationSuccessEvent': '/Teleconference/RegistrationSuccess'
        },
        registrationSource = 'GBR-Teleconference',
        isLegacy = true;

    var MainRegistration = new RegistrationInterface(trackingEvents, registrationSource, isLegacy);

    ko.applyBindings(MainRegistration, document.getElementById('ko.generic.registration.container'));
})(ko);
