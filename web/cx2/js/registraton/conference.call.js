/*global RegistrationInterface, document, ko*/
;(function (ko) {
    "use strict";
    var trackingEvents = {
            'FormSubmitEvent' : 'Generate Pin - GBR-Conference-Call/onClick',
            'registrationSuccessEvent': '/Conference-Call/RegistrationSuccess'
        },
        registrationSource = 'GBR-Conference-Call',
        isLegacy = true;

    var MainRegistration = new RegistrationInterface(trackingEvents, registrationSource, isLegacy);

    ko.applyBindings(MainRegistration, document.getElementById('ko.generic.registration.container'));
})(ko);
