/*global RegistrationInterface, document, ko*/
;(function (ko) {
    "use strict";
    var trackingEvents = {
            'FormSubmitEvent' : 'Generate Pin - Homepage-Top/onClick',
            'registrationSuccessEvent': '/Homepage-E-Top/RegistrationSuccess'
        },
        registrationSource = 'GBR-Homepage-Top',
        isLegacy = true;

    var MainRegistration = new RegistrationInterface(trackingEvents, registrationSource, isLegacy);

    ko.applyBindings(MainRegistration, document.getElementById('ko.homepageppcshortterm.registration.container'));
})(ko);
