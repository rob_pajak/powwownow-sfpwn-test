/*global ko*/
/*global jQuery*/
/*global $*/
/*global dataLayer*/
/*global _*/

(function($) {
    'use strict';
    $(function () {
        // Quotes
        var quotes = $(".customer_reviews");
        var quoteIndex = -1;

        function showNextQuote() {
            ++quoteIndex;
            quotes.eq(quoteIndex % quotes.length)
                .fadeIn(3000)
                .delay(1000)
                .fadeOut(3000, showNextQuote);
        }
        showNextQuote();

        $('.register_now_anchor').on('click', function() {

            var all = document.getElementsByTagName("*");
            for (var i=0, max=all.length; i < max; i++) {
                all[i].style.zIndex = 0;
            }

            try {
                $.each( $('video'), function(i, v ) {
                    v.pause();
                });
            } catch(e) {
            }

            darkenUI();

            $('body').on('click', function(event){
                if ($(event.target).is('.mask')) {
                    lightenUI();
                    $(this).unbind();
                }
            });

            $(document).keyup(function(event) {
                if (event.keyCode == 27) {
                    lightenUI();
                    $(this).unbind();
                }
            });

            function darkenUI() {
                if (!$('.ie8').length) {
                    $('body').append('<div class="mask"></div>');
                    $('main').css({
                        'position': 'relative',
                        'z-index': 2
                    });
                }
            }

            function lightenUI() {
                $('body').find('.mask').removeClass('mask');
                $('.top-menu-container-grid').css({'z-index': 701});
            }
        });

        $('#register_now_dl').on('click', function() {
            try {
                dataLayer.push({'event': 'RegisterNow - FreeConferenceCallService-Bottom/onClick'});
            } catch (e) {
                console.warn('DataLayer : RegisterNow - FreeConferenceCallService-Bottom/onClick', e);
            }
        });

        $('.video_player_popout').on('click', 'img', function() {
            try {
                dataLayer.push({'event': 'Video - Getting Started/onClick'});
            } catch (e) {
                console.warn('DataLayer : Video - Getting Started/onClick', e);
            }
        });

        // Anchor tag smooth scrolling
        $('a[href*=#]:not([href=#])').click(function (event) {
            event.preventDefault();
            if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });

    var trackingEvents = {
            'FormSubmitEvent' : 'Generate Pin - GBR-Free-Conference-Call-Service/onClick',
            'registrationSuccessEvent': '/Free-Conference-Call-Service/RegistrationSuccess'
        },
        redirectUrl = '/myPwn/welcome-1',
        registrationSource = 'GBR-Free-Conference-Call-Service-Top';

    var MainRegistration = new RegistrationInterface(trackingEvents, redirectUrl, registrationSource);

    ko.applyBindings(MainRegistration, document.getElementById('ko.main.registration.container'));

})(jQuery);

