/*
*    Tb.          Tb.
*    :$$b.        $$$b.
*    :$$$$b.      :$$$$b.
*    :$$$$$$b     :$$$$$$b
*    $$$$$$$b     $$$$$$$b
*    $$$$$$$$b    :$$$$$$$b
*    :$$$$$$$$b---^$$$$$$$$b
*    :$$$$$$$$$b        ""^Tb
*    $$$$$$$$$$b    __...__`.
*    $$$$$$$$$$$b.g$$$$$$$$$pb
*    $$$$$$$$$$$$$$$$$$$$$$$$$b
*    $$$$$$$$$$$$$$$$$$$$$$$$$$b
*    :$$$$$$$$$$$$$$$$$$$$$$$$$$;
*    :$$$$$$$$$$$$$^T$$$$$$$$$$P;
*    :$$$$$$$$$$$$$b  "^T$$$$P' :
*    :$$$$$$$$$$$$$$b._.g$$$$$p.db
*    :$$$$$$$$$$$$$$$$$$$$$$$$$$$$;
*    :$$$$$$$$"""^^T$$$$$$$$$$$$P^;
*    :$$$$$$$$       ""^^T$$$P^'  ;
*    :$$$$$$$$    .'       `"     ;
*    $$$$$$$$;   /                :
*    $$$$$$$$;           .----,   :
*    $$$$$$$$;         ,"          ;
*    $$$$$$$$$p.                   |
*    :$$$$$$$$$$$$p.                :
*    :$$$$$$$$$$$$$$$p.            .'
*    :$$$$$$$$$$$$$$$$$$p...___..-"
*    $$$$$$$$$$$$$$$$$$$$$$$$$;
*    .db.          bug  $$$$$$$$$$$$$$$$$$$$$$$$$$
*    d$$$$bp.            $$$$$$$$$$$$$$$$$$$$$$$$$$;
*    d$$$$$$$$$$pp..__..gg$$$$$$$$$$$$$$$$$$$$$$$$$$$
*    d$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$p._            .gp.
*    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$p._.ggp._.d$$$$b
*/

/*global document, ko, $, define, module, _, dataLayer*/
;(function (ko, $) {
    "use strict";

    try {
        $('.logo, .login-container, .new-customer-container').velocity('fadeIn', {duration: 1000});
    } catch (e) {
        try {
            $('.login-container, .new-customer-container').fadeIn(1000);
        } catch (err) {
            $('.login-container, .new-customer-container').css('display', 'block');
        }
    }

    /**
     * @desc
     *  Login Form View Model.
     * @constructor
     */
    function LoginViewModel() {

        /**
         * @desc
         *  View Model represents form values.
         * @type {{login: {email: *, password: *, remember: *}}}
         */
        this.model = {
            'login' : {
                'email' : ko.observable(''),
                'password' : ko.observable(''),
                'remember' : ko.observable(true)
            }
        };

        /**
         * @desc
         *  Ko observable controls template logic
         * @type {{errors: {message: *, resetlink: *}, isEnabled: {email: *, submit: *, loader: *}}}
         */
        this.ui = {
            errors : {
                'message' : ko.observable('')
            },
            isEnabled : {
                'submit': ko.observable(true),
                'loader': ko.observable(false)
            },
            redirect: {
                'url' : ko.observable('/myPwn/')
            },
            reset: {
                'email': ko.observable(false),              // flag whether or not to append email address
                'url':   ko.observable('/Forgotten-Password')
            }
        };

        /**
         * @desc
         *  Computed variables - must be defined after the other objects, order is important. computed items cannot use other computed variables in this object, a new
         *  object would have to be created.
         * @type {*}
         */
        this.computed = {
            resetlink: ko.computed(function() {
                if(this.ui.reset.email()) {
                    return this.ui.reset.url() + '?email=' + encodeURIComponent(this._getEmailAddress());
                }
                return this.ui.reset.url();
            }, this)
        };
    }

    /**
     * @desc
     *  Easy access to proto.
     * @type {Object}
     */
    var proto = LoginViewModel.prototype;

    /**
     * @desc
     *  Set RedirectURL after success login.
     * @param redirectUrl
     */
    proto.setRedirect = function setRedirect(redirectUrl) {
        if (!_.isEmpty(redirectUrl)) {
            return this.ui.redirect.url(redirectUrl);
        }
    };

    /**
     * @desc
     *  Set Reset Password after page render
     * @param resetUrl
     */
    proto.setResetLink = function setResetLink(resetUrl) {
        this.ui.reset.url(resetUrl);
    };

    /**
     * @desc
     *  Performs basic validation.
     * @returns {*} | Boolean
     * @private
     */
    proto._doFormValidation = function _doFormValidation() {
        var emailAddress = this._getEmailAddress(),
            password = this._getPassword();
        if (!this._validateEmail(emailAddress)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS'};
        }
        if (_.isEmpty(emailAddress)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK'};
        }
        if (_.isEmpty(password)) {
            return {error: 'CLIENT_INVALID_PASSWORD'};
        }
        return true;
    };

    /**
     * @desc
     *  This function fixes the following bug related to browser auto completes:
     *  https://github.com/knockout/knockout/issues/648
     *  Check if a password has been entered in our model, if not we check using
     *  traditional if the field has been updated.
     *  This issue is related to the fact that some browser when doing auto complete do
     *  not trigger the change event.
     * @returns {*}
     * @private
     */
    proto._getPassword = function _getPassword() {
        var password, jqPassword;
        password = this.model.login.password();
        jqPassword = $('#js-password').val();
        if (_.isEmpty(password)) {
            if (!_.isEmpty(jqPassword)) {
                this.model.login.password(jqPassword);
            }
        }
        return this.model.login.password();
    };

    /**
     * @desc
     *  See _getPassword.
     * @returns {*}
     * @private
     */
    proto._getEmailAddress = function _getEmailAddress() {
        var emailAddress, jqEmailAddress;
        emailAddress = this.model.login.email();
        jqEmailAddress = $('#js-email').val();
        if (_.isEmpty(emailAddress)) {
            if (!_.isEmpty(jqEmailAddress)) {
                this.model.login.email(jqEmailAddress);
            }
        }
        return this.model.login.email();
    };
    
    /**
     * @desc
     *  Validate a given email address.
     * @param emailAddress
     * @returns {boolean}
     * @private
     */
    proto._validateEmail = function _validateEmail (emailAddress) {
        var filter = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        return filter.test(emailAddress);
    };

    /**
     * @desc
     *  Login user.
     */
    proto.doLogin = function doLogin() {
        var _this = this,formValidationStatus;

        formValidationStatus = _this._doFormValidation();

        if (formValidationStatus === true) {
            $.ajax({
                type: 'POST',
                url: '/Login-Ajax',
                dataType: 'json',
                data: ko.toJS(_this.model),
                beforeSend:function(){
                    _this.ui.errors.message('');
                    _this.ui.isEnabled.loader(true);
                    _this.ui.isEnabled.submit(false);
                },
                success:function(){
                    try {
                        dataLayer.push({'event': '/Login/Success'});
                    } catch (e) {
                        // do nothing
                        console.warn('dataLayer event "/Login/Success" Failed');
                    }

                    return setTimeout(function(){
                        window.location.replace(_this.ui.redirect.url())
                    }, 1000);
                },
                error:function(jqXHR) {
                    var errors = null;
                    try {
                        errors = $.parseJSON(jqXHR.responseText);
                    } catch (e) {
                        console.warn('KO : Setting Response error message to default', e);
                        // Generic Error Message
                        errors = {error: 'TECHNICAL_ERROR'};
                    } finally {
                        _this.ui.isEnabled.submit(true);
                        _this._renderErrors(errors);
                    }
                },
                complete: function() {
                    _this.ui.isEnabled.loader(false);
                    _this.ui.isEnabled.submit(true);
                }
            });
        } else {
            _this._renderErrors(formValidationStatus);
        }
    };

    /**
     * @desc
     *  Render error onto registration_form template.
     * @param errors
     * @private
     */
    proto._renderErrors = function _renderErrors(errors) {
        var error,
            errorMessage = 'TECHNICAL_ERROR';

        if (!_.isUndefined(errors)) {
            if(_.isObject(errors)) {
                if (_.has(errors, 'error')) {
                    errorMessage = errors.error;
                }
            }
        }

        this._updateResetLink(errors.error); // Handle error types
        error = this._getErrorResponses(errorMessage); // Get the new message

        this.ui.errors.message(error);
    };

    /**
     * @desc
     *  Checks whether the user's email should be added to the reset password link
     * @param errortype - The error type
     * @private
     * @author Jack Adams
     */
    proto._updateResetLink = function _updateResetLink(errortype) {
        this.ui.reset.email(false); // Clear email from forgotten password link

        if(errortype == 'FORM_VALIDATION_LOGIN_ERROR' && this._validateEmail(this._getEmailAddress())) {
            this.ui.reset.email(true);
        }
    };

    /**
     * @desc
     *  Specific error messages for Login Page
     * @param error
     * @returns {*}
     * @private
     */
    proto._getErrorResponses = function _getNewErrorResponses (error) {
        switch (error) {
            case 'CLIENT_INVALID_EMAIL_ADDRESS':
                error = 'Oops! You made a typo, please enter a valid email.';
                break;
            case 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK':
                error = 'Looks like you forgot to type in your email.';
                break;
            case 'CLIENT_INVALID_PASSWORD':
                error = 'Oops!, please enter a password.';
                break;
            case 'FORM_VALIDATION_INVALID_PASSWORD':
            case 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS':
            case 'FORM_VALIDATION_LOGIN_ERROR':
                error = 'Your email and password are not recognised. Please amend and try again. ' +
                        'If you are a registered User but have forgotten\nyour password please ' +
                        'use the <a href="' + this.computed.resetlink() + '">Password Reminder</a>.';
                break;
            default:
                error = 'Sorry, we are currently experiencing technical difficulties. Please try again later.';
        }
        return error;
    };

    // Expose the class either via AMD, CommonJS or the global object
    if (typeof define === 'function' && define.amd) {
        define('LoginViewModel/LoginViewModel', [], function () {
            return LoginViewModel;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = LoginViewModel;
    }
    else {
        window.LoginViewModel = LoginViewModel;
    }

    ko.applyBindings(new LoginViewModel(), document.getElementById('js.login.container'));

})(ko, window.jQuery);