/*global window, jQuery, dataLayer*/
;(function (window, $, _, ko) {

    var trackingEvents = {
            'FormSubmitEvent' : 'Generate Pin – Mobile/onClick',
            'registrationSuccessEvent': ' /Mobile/RegistrationSuccess'
        },
        registrationSource = 'GBR-Mobile',
        isLegacy = false;

    var MainRegistration = new RegistrationInterface(trackingEvents, registrationSource, isLegacy);

    ko.applyBindings(MainRegistration, document.getElementById('ko.indexE.registration.container'));

    var quotes = $(".customer_reviews"),
        quoteIndex = -1;
    // Quotes
    function showNextQuote() {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(3000)
            .delay(1000)
            .fadeOut(3000, showNextQuote);
    }

    showNextQuote();

    $('#accordion_1').on('click', function () {
        if ($(this).is(':checked')) {
            try {
                dataLayer.push({'event': 'Conference Call Cost/onClick'});
            } catch (e) {
                console.warn('DataLayer : Conference Call Cost/onClick', e);
            }
        }
    });

    $('#accordion_2').on('click', function () {
        if ($(this).is(':checked')) {
            try {
                dataLayer.push({'event': 'International Dial-in Numbers/onClick'});
            } catch (e) {
                console.warn('DataLayer : International Dial-in Numbers/onClick', e);
            }
        }
    });

})(window, jQuery, _, ko);