/*global jQuery*/
/*global $*/
/*global dataLayer*/

$(window).load(function() {
    $('.mail_to').on('click', function() {
        try {
            dataLayer.push({'event': 'Mobile Welcome - Share Details/onClick'});
        } catch (e) {
            console.warn('DataLayer : Mobile Welcome - Share Details/onClick', e);
        }
    });
});