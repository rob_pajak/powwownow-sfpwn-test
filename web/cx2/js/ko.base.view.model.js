/**
 * This Base Class should not be used directly. Instead it should be extended.
 * Any Changes made to this Class should be reviewed by a Senior Developer.
 */
/*global jQuery, Class, _*/

// Do not remove
jQuery.noConflict();

var BaseViewModel = Class.create();

BaseViewModel.prototype = {
    // Required
    initialize: function() {},

    /**
     * @desc
     *  Global Validation helpers
     */
    validators: {
        email: function (emailAddress) {
            var filter = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            return filter.test(emailAddress);
        },
        mobile: function (mobileNumber) {
            var pattern = /^\s*\(?(020[7,8]{1}\)?[ ]?[1-9]{1}[0-9{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{3})\s*$/;
            return pattern.test(mobileNumber);

        }
    },

    /**
     * @desc
     * basic email validation.
     * @param emailAddress
     * @returns {*}
     */
    isValidEmail: function (emailAddress) {
        if (!this.validators.email(emailAddress)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS'};
        }
        if (_.isEmpty(emailAddress)) {
            return {error: 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK'};
        }
        return true;
    },

    /**
     * @desc
     *  Get PWN error message.
     * @param errors
     * @returns {string}
     */
    getErrorMessage: function (errors) {
        var errorMessage = 'TECHNICAL_FAULT',
            error = 'Sorry, we are currently experiencing technical difficulties. Please try again later.';

        if (!_.isUndefined(errors)) {
            if(_.isObject(errors)) {
                if (_.has(errors, 'error')) {
                    errorMessage = errors.error;
                }
            }
        }
        switch (errorMessage) {
            case 'CLIENT_INVALID_EMAIL_ADDRESS':
            case 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS':
                error = 'Oops! You made a typo, please enter a valid email.';
                break;
            case 'CLIENT_INVALID_EMAIL_ADDRESS_BLANK':
                error = 'Looks like you forgot to type in your email.';
                break;
            case 'FORGOTTEN_PASSWORD_UNKNOWN_ERROR':
            case 'API_RESPONSE_NO_ACCOUNT_FOR_EMAIL':
                error = 'Oops!, we could not find that email address.';
                break;
            case 'API_RESPONSE_CONTACT_IS_NOT_STANDARD_POWWOWNOW_USER':
                error = 'In order to see your PIN(s) please <a href="/Login">Login to myPwn</a>.If you forgot your password please click here.';
                break;
            case 'CLIENT_INVALID_MOBILE_NUMBER':
                error = 'Please enter a valid UK Mobile number e.g 0xxxxxxxxxx';
                break;
            default:
                error = 'Sorry, we are currently experiencing technical difficulties. Please try again later.';
        }
        return error;
    }
};