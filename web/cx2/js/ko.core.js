/*jshint ignore:start */
;(function (ko) {
    if (typeof ko.bindingHandlers.fadeVisible !== 'object') {
        /**
         * @desc
         *  Custom handler to perform fadeIn/fadeOut effects
         * @type {{init: init, update: update}}
         */
        ko.bindingHandlers.fadeVisible = {
            init: function(element, valueAccessor) {
                // Initially set the element to be instantly visible/hidden depending on the value
                var value = valueAccessor();
                // Use "unwrapObservable" so we can handle values that may or may not be observable
                $(element).toggle(ko.unwrap(value));
            },
            update: function(element, valueAccessor) {
                // Whenever the value subsequently changes, slowly fade the element in or out
                var value = valueAccessor(),
                    isVisible = ko.unwrap(value);

                if (isVisible) {
                    try {
                        $(element).velocity('fadeIn');
                    } catch (e) {
                        $(element).fadeIn();
                    }

                } else {
                    try {
                        $(element).velocity('fadeOut', {duration: 1});
                    } catch(e) {
                        $(element).fadeOut('fast');
                    }

                }
            }
        };
    }

    if (typeof ko.bindingHandlers.initBind !== 'object') {
        /**
         * @desc
         *  Custom binding which allows values to be initialised from any element property and bound to any KO binding (e.g. textInput, value etc)
         *  HTML binding like: data-bind="initBind: { property: model.email, binding: 'textInput', attr: 'value' }"
         *  Where 'property' is the observable, 'binding' is the type of bind and 'attr' is where to find the initial value
         * @type {{init: init}}
         * @author Jack Adams
         */
        ko.bindingHandlers.initBind = {
            init: function(element, valueAccessor) {
                var params = valueAccessor(),
                    value = element[params.attr],
                    bindingArgs = {};
                bindingArgs[params.binding] = params.property;
                params.property(value); // Set value
                ko.applyBindingsToNode(element, bindingArgs); // Add binding
            }
        };
    }
})(ko);