/*global jQuery*/
(function($) {
    'use strict';
    $(function() {
        $('.video_player_popout').on('click', 'img', function() {
            var video_source = '',
                autoplay = false;

            try {
                video_source = $(this).data('video-id');
                $(video_source).fadeIn('slow');
            } catch (e) {
            }

            try {
                autoplay = $(this).data('auto-play');
                if (autoplay) {
                    $(video_source + ' video')[0].play();
                }
            } catch (e) {
            }
        });

        $('.pop_out_modal_video_player').on('click', '.close', function(event) {
            event.preventDefault();
            try {
                $.each( $('video'), function(i, v ) {
                    v.pause();
                });
                $(this).parent().parent().fadeOut('slow');
            } catch(e) {
            }
        });
    });
})(jQuery);