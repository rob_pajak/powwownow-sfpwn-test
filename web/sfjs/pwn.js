/**
*   Global pwnApp object
*   version: 0.3 (07-December-2012)
*   by VK
*   @requires jQuery >= 1.7.1
*
*/
// This file while was breaking up on the Response Web Design Layout. We don't know why, but taking out the function ()
// Fixed the problem with this file

var pwnApp  = {}; //global pwn object

pwnApp.utils = {}, //normally for cross-browser solutions or better implementations
pwnApp.common = {}, // common functionality for various pages across the website
pwnApp.homepage = {}, // homepage specific functionality
pwnApp.landingpage = {}, // Landing page functionality [Added to stop js bug occurring later on the file]
pwnApp.contactus = {}, // Contact us page
pwnApp.unsubscribe = {},
pwnApp.deletePin = {},
pwnApp.login = {}, // Login page functionality
pwnApp.outlookPlugin = {}, // Outlook Plugin Form
pwnApp.outlookPlugin.formInit = {},
pwnApp.homepageE = {}, // Homepage E Functionality
pwnApp.webConferencing = {}, // Web Conferencing
pwnApp.forgottenPassword = {}, // Forgotten Password
pwnApp.resetPassword = {}, //Reset Password
pwnApp.tellAFriend = {}; // Tell A Friend

// cross-browser console.log support (if it doesn't exist - just fake it)
pwnApp.utils.consoleSupport = function () {
    if (typeof console === "undefined") {
        window.console = {};
        window.console.log = function () { return; };
    }
};

/**
 * @param formElement
 * @param defaultText
 * @deprecated
 */
pwnApp.utils.defaultTextField = function (formElement, defaultText) {
    console.warn('pwnApp.utils.defaultTextField is now deprecated.');
};

pwnApp.utils.scrollUp = function() {
    $('body').on('click', 'a.scroll-up', function () {
        $('html, body').animate({ scrollTop: 0 }, 600);
        return false;
    });
};

// Returns the String into Camel Case
pwnApp.utils.camelCase = function(str) {
    return str.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
};

pwnApp.utils.init = function () {

    //list of methods to launch when the app loads
    pwnApp.utils.consoleSupport(); //support console in legacy browsers

    // ajax loader image
    pwnApp.utils.loaderImage = '<img src="/sfimages/ajaxLoaders/form-ajax-loader.gif" alt="Loading" class="loader-image" />';

};

pwnApp.utils.isValidEmail = function (email) {
    if (email === undefined) {
        return false;
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email.match(re);
}

// global init for every page
pwnApp.init = function () {
    // login form init
    pwnApp.utils.init();
};

pwnApp.homepage.init = function () {
    //pwnApp.homepage.pwnBasicRegistration();
    pwnApp.homepage.scrollToRegistration();
    pwnApp.common.initVideoPopups();
    pwnApp.common.pinReminder();
    pwnApp.common.promoRotator();
    pwnApp.common.socialMediaFooter();
};

pwnApp.landingpage.init = function () {};

/**
 * @deprecated
 */
pwnApp.homepage.pwnBasicRegistration = function () {
    console.log('pwnApp.homepage.pwnBasicRegistration is deprecated');
    return;
};

// full registration screen with FName and LName
pwnApp.homepage.pwnFullRegistration = function () {

    $('#form-create-log-in').initMypwnForm({
        cssPath : '/sfcss/',
        cssStyle: 'pwnForms.css',
        messagePos: 'off',
        requiredAsterisk: 'on',
        success: function (responseText) {
            $('#next_step').html(responseText.html);
            pwnApp.homepage.pwnWelcomePack();
        }
    });

    pwnApp.homepage.videoCarouselInit();

};

// video carousel init
pwnApp.homepage.videoCarouselInit = function () {

    $.getScript("/shared/jQuery/jquery.jcarousel.min.js", function() {
        $('#videos').jcarousel( { visible: 3, scroll: 3 } );
    });

    $.getScript("/shared/jQuery/jquery.prettyPhoto.js", function() {
        $('#videos a').prettyPhoto({
            flash_markup:
                '<object id="flowplayer" width="{width}" height="{height}" data="{path}" type="application/x-shockwave-flash">' +
                    '<param name="movie" value="{path}" />' +
                    '<param name="allowfullscreen" value="true" />' +
                    '<param value="opaque" name="wmode" />' +
                '</object>'
        });
    });

    $.getScript("/shared/jQuery/jquery.polaroid.js", function() {
        $('#videos').find('.polaroid').polaroid({
            /*cufon: true,*/
            zoom: 1.6,
            preAnimate: function() {
                $('a', this).prettyPhoto();
            }
        });
    });

    // add some styles
    $('<link rel="stylesheet" type="text/css" href="/sfcss/vendor/jQuery/jquery.prettyPhoto.css">').appendTo('head');
    $('<link rel="stylesheet" type="text/css" href="/sfcss/vendor/jQuery/jquery.jcarousel.css">').appendTo('head');
};

// Wallet Card Request method
pwnApp.homepage.pwnWelcomePack = function () {

    $('#mypins-walletcard-form').initMypwnForm({
        cssPath : '/sfcss/',
        cssStyle: 'pwnForms.css',
        messagePos: 'off',
        requiredAsterisk: 'on',
        success: function (responseText) {
            $('#next_step').html(responseText.html);
        }
    });

};

pwnApp.homepage.scrollToRegistration = function () {
    $('body').on('click', 'a.scroll-to-reg', function () {
        $('html, body').animate({ scrollTop: 150 }, 600);
        return false;
    });
};

/**
 *   This method initialises all videos marked as data-photo="prettyPhoto" on the page
 *   to load in popups. It also loads needed plugin if it hasn't been loaded before.
 */
pwnApp.common.initVideoPopups = function () {

    if (typeof $.fn.prettyPhoto !== "function") {

        $.getScript("/shared/jQuery/jquery.prettyPhoto.js", function() {
            // add some styles
            $('head').append('<link rel="stylesheet" href="/sfcss/vendor/jQuery/jquery.prettyPhoto.css" type="text/css" />');
            $("a[data-photo^='prettyPhoto']").prettyPhoto({
                flash_markup:
                    '<object id="flowplayer" width="{width}" height="{height}" data="{path}" type="application/x-shockwave-flash">' +
                        '<param name="movie" value="{path}" />' +
                        '<param name="allowfullscreen" value="true" />' +
                        '<param value="opaque" name="wmode" />' +
                    '</object>'
            });
        });
    } else {
        $("a[data-photo^='prettyPhoto']").prettyPhoto({
            flash_markup:
                '<object id="flowplayer" width="{width}" height="{height}" data="{path}" type="application/x-shockwave-flash">' +
                    '<param name="movie" value="{path}" />' +
                    '<param name="allowfullscreen" value="true" />' +
                    '<param value="opaque" name="wmode" />' +
                '</object>'
        });
    }
};

pwnApp.common.pinReminder = function (responsive) {
    console.log('pwnApp.common.pinReminder is now deprecated, do not use.');
};

pwnApp.common.promoRotator = function () {

    var liToShow = 0, // Tracks which list item should be shown
        promotionalFadeSlideSpeed = 5000, // Sets the duration each slide is shown for
        promotionalTimeout,
        slideUrl,
        container = $('#rotating-promotional-container'), // DOM element for promo rotator container
        promoList = $('ul#rotating-promotional-list'); // DOM element for promo list

    function fadeNext () {

        //load first image of the slide
        if (liToShow === 0) {
            slideUrl = promoList.find('li:eq(' + liToShow + ')').attr('title');
            promoList.find('li:eq(' + liToShow + ')').attr('title', '');
            if (slideUrl !== '') {
                promoList.find('li:eq(' + liToShow + ')').css({'backgroundImage': 'url(' + slideUrl +')'});
            }
        }

        // next slide
        liToShow += 1;

        // Hide the visibile list item(s)
        promoList.children('li:visible').hide();

        // See if we have reached the last slide, if so we need to show the first
        // one. Note we do not call setTimeout(); as we don't want animation to continue
        // after all slides have been shown
        if (liToShow >= promoList.children().length) {
            // Show first slide and not show any more slides
            fadeShowSlide(0, true);
        } else {
            // Show next slide and call this function again to show next slide
            promotionalTimeout = setTimeout(fadeNext, promotionalFadeSlideSpeed);
            fadeShowSlide(liToShow);
        }
    }


    function fadeShowSlide(slideNum, cancelLooping) {

        var promoLinks = $('ul#rotating-promotional-links');

        // Show specified left nav
        promoLinks.find('li').removeClass('filled');
        promoLinks.find('li:eq(' + slideNum + ')').addClass('filled');

        // Show specified slide
        promoList.find('li:visible').hide();
        slideUrl = promoList.find('li:eq(' + slideNum + ')').attr('title');
        promoList.find('li:eq(' + slideNum + ')').attr('title', '');

        if (slideUrl !== '') {
            promoList.find('li:eq(' + slideNum + ')').css({'backgroundImage': 'url(' + slideUrl +')'});
        }

        promoList.find('li:eq(' + slideNum + ')').show();

        // If cancelLooping was set to true, then clear the timeout which shows the next slide
        if (cancelLooping !== undefined && cancelLooping) {
            clearTimeout(promotionalTimeout);
        }

    }

    // Add time delay to call function to show the next slide
    promotionalTimeout = setTimeout(fadeNext, promotionalFadeSlideSpeed);

    // Set up cancelling of fade on hover of the promotionals area
    container.mouseenter(function() {
        clearTimeout(promotionalTimeout);
    });

};

pwnApp.common.socialMediaFooter = function () {
    $.getJSON('/ajax/social-media-footer.php', function(data) {
        $.each(data, function(key, listItems) {
            $('#socialMediaFooter_' + key).html(listItems);
        });
    });

    function socialListFade () {
        var nextLi, // index for the next <li> to show
            delay = 9000; // time delay between items

        // Loop round all ul's with the list-fading class
        $('ul.list-fading').each(function(index) {
            // If there is only one <li> then just show it with no affects
            if ($(this).children('li').length === 1) {
                return true;
            }

            // See if it is the first time this function has been run by seeing
            // if all li's are visible
            if ($(this).children('li:visible').length === $(this).children('li').length && $(this).children('li').length !== 1) {
                // First time function is run so hide all li's
                $(this).children('li').hide();
                // Show the first li
                $(this).children('li:first-child').fadeIn('slow');
            } else {
                // It is not the first time function has run, so we need to fade out the currently
                // visible li and then fade in the next, unless we have reached the end
                // of the list, then we need to show the first li again

                // Get the index of the next <li> to show
                nextLi = $(this).children('li:visible').index() + 2; // Add 2 as index() starts from 0
                                                                     // :nth-child() starts from 1
                // If we have reached the end of the list, the start from the first item
                if (nextLi > $(this).children().length) {
                    nextLi = 1;
                }

                // Do the fading in and out
                $(this).children('li:visible').fadeOut('slow');
                $(this).children('li:nth-child(' + (nextLi) + ')').delay(900).fadeIn('slow');
            }
        });
        // Add time delay to run this function again to show the next list item
        setTimeout(socialListFade, delay); // To change the time delay between items you
                                        // only need to change here
    }


    // Starts the fading of the articles
    socialListFade();

};

/**
 * This Method is for the Form Init on the MyPINs Page
 */
pwnApp.common.myPINsInit = function () {
    // Create PWN Form
    $('#form-pins-table').initMypwnForm({
        successMessagePos: 'off',
        debug: 'off',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        beforeSubmit: function() {
            // Delete PINs Validation
            if ($('input[name="r[]"]:checked').length === 0) {
                alert(FORM_VALIDATION_NO_PIN);
                return false;
            }
            return confirm(FORM_CONFIRM_PIN_DELETION);
        },
        success: function(responseText) {
            var responseArr = $.parseJSON(responseText);

            if ((responseArr['error_messages'] instanceof Array) && responseArr['error_messages'].length > 0) {
                return showInputErrorsBoxes(responseArr['error_messages'], 'form-edit-contact', true);
            }

            if (typeof responseArr['deleted'] !== 'undefined') {

                // Show alert box depending on pin delete success and failures
                if (((responseArr['deleted']['success'] instanceof Array) && responseArr['deleted']['success'].length > 0) && ((responseArr['deleted']['failed'] instanceof Array) && responseArr['deleted']['failed'].length > 0)) {
                    alert(FORM_RESPONSE_PINS_DELETED_SUCCESS_AND_FAILURE);
                } else if (((responseArr['deleted']['success'] instanceof Array) && responseArr['deleted']['success'].length > 0) && ((responseArr['deleted']['failed'] instanceof Array) && responseArr['deleted']['failed'].length === 0)) {
                    alert(FORM_RESPONSE_PINS_DELETED_SUCCESS);
                } else if (((responseArr['deleted']['success'] instanceof Array) && responseArr['deleted']['success'].length === 0) && ((responseArr['deleted']['failed'] instanceof Array) && responseArr['deleted']['failed'].length > 0)) {
                    alert(FORM_RESPONSE_PINS_DELETED_FAILURE);
                }

                // If pins have been deleted, then reload page
                if (responseArr['deleted']['success'].length > 0) {
                    window.location.reload();
                }

            } else {
                alert(responseArr['html']);
                window.location.reload();
            }

            return true;
        },
        error: function(responseText) {
            alert(FORM_COMMUNICATION_ERROR);
        }
    });

};

/**
 * This Method is for the DataTable on the MyPINs Page
 */
pwnApp.common.myPINsDataTable = function (data, columns, sortcol, sortorder) {
    // DataTable Setup
    yepnope({
        test : jQuery.fn.dataTable,
        nope : [
            '/shared/jQuery/jquery.dataTables.min.js',
            '/sfcss/vendor/jQuery/datatable.css'
        ],
        complete: function () {
            oTable = $('#tblPINs').dataTable({
                "aaData": data,
                "aoColumns": columns,
                "oLanguage" : {
                    "sProcessing": DATA_TABLES_sProcessing,
                    "sLengthMenu": DATA_TABLES_sLengthMenu,
                    "sZeroRecords": DATA_TABLES_sZeroRecords,
                    "sEmptyTable": DATA_TABLES_sEmptyTable,
                    "sLoadingRecords": DATA_TABLES_sLoadingRecords,
                    "sInfo": DATA_TABLES_sInfo,
                    "sInfoEmpty": DATA_TABLES_sInfoEmpty,
                    "sInfoFiltered": DATA_TABLES_sInfoFiltered,
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sSearch": DATA_TABLES_sSearch,
                    "sUrl": "",
                    "fnInfoCallback": null,
                    "oPaginate": {
                        "sFirst":    DATA_TABLES_sFirst,
                        "sPrevious": DATA_TABLES_sPrevious,
                        "sNext":     DATA_TABLES_sNext,
                        "sLast":     DATA_TABLES_sLast
                    }
                },
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "iDisplayLength": 25
            });

            oTable.fnSort([[sortcol,sortorder]]);

            $('#tblPINs th.sorting').append('<div></div>').css('text-decoration', 'underline').css('cursor', 'pointer');
        }
    });

};

/**
 * This is a Common Datatable Method.
 *
 * @requires YepNope
 *
 */
pwnApp.common.dataTable = function (tableID, data, columns, includes, sortCol, sortOrder, onHover, floatLeft, bottomPadding, autoWidth, tableEmpty, displayLength,fnDrawCallback) {
    // Table ID
    if (null === tableID || null === data || null === columns) {
        return false;
    }

    // Includes
    if (null === includes) {
        includes = [
            '/shared/jQuery/jquery.dataTables.min.js',
            '/sfjs/language/en.js',
            '/sfcss/vendor/jQuery/datatable.css'
        ];
    }

    // Sort Col and Sort Order
    if (null === sortCol || null === sortOrder) {
        sortcol = 0;
        sortOrder = 'asc';
    }

    // onHover
    if (null === onHover) {
        onHover = false;
    }

    // floatLeft
    if (null === floatLeft) {
        floatLeft = false;
    }

    // bottom Padding
    if (null === bottomPadding) {
        bottomPadding = false;
    }

    // Auto Width
    if (null === autoWidth) {
        autoWidth = true;
    }

    // Sort
    var aaSorting = [];
    if ('false' !== sortOrder) {
        aaSorting = [[sortCol,sortOrder]];
    }

    // Display Length
    if (null === displayLength) {
        displayLength = 25;
    }

    // DataTable Setup
    yepnope({
        test : jQuery.fn.dataTable,
        nope : includes,
        complete: function () {
            oTable = $(tableID).dataTable({
                "aaData": data,
                "aoColumns": columns,
                "oLanguage" : {
                    "sProcessing": DATA_TABLES_sProcessing,
                    "sLengthMenu": DATA_TABLES_sLengthMenu,
                    "sZeroRecords": DATA_TABLES_sZeroRecords,
                    "sEmptyTable": tableEmpty,
                    "sLoadingRecords": DATA_TABLES_sLoadingRecords,
                    "sInfo": DATA_TABLES_sInfo,
                    "sInfoEmpty": DATA_TABLES_sInfoEmpty,
                    "sInfoFiltered": DATA_TABLES_sInfoFiltered,
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sSearch": DATA_TABLES_sSearch,
                    "sUrl": "",
                    "fnInfoCallback": null,
                    "oPaginate": {
                        "sFirst":    DATA_TABLES_sFirst,
                        "sPrevious": DATA_TABLES_sPrevious,
                        "sNext":     DATA_TABLES_sNext,
                        "sLast":     DATA_TABLES_sLast
                    }
                },
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "iDisplayLength": displayLength,
                "aaSorting": aaSorting,
                "fnDrawCallback": function(){
                    $( document ).trigger('draw.datatables')
                }
            });

            // Set the Table Headers to have onHover effect
            if (onHover) {
                $(tableID + ' th.sorting').hover(
                    function(){$(this).css("text-decoration", "underline");$(this).css("cursor", "pointer");},
                    function(){$(this).css("text-decoration", "none");$(this).css("cursor", "default");}
                );
            }

            // Move Table to the Left. Some browsers do a funky shift the right.
            if (floatLeft) {
                $(tableID).addClass("floatleft");
            }

            // Use Clearfix on the Table Wrapper
            if (bottomPadding) {
                $(tableID + '_wrapper').addClass("clearfix");
                $(tableID + '_wrapper').addClass("padding-bottom-20");
            }

            // Auto Width - By Default the Table has an Auto Width
            if (!autoWidth) {
                $(tableID + ' thead th').css('width','');
            }

        }
    });
};

// Contact Us Form
pwnApp.contactus.init = function (formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var responseText = $.parseJSON(responseArr),
                message = '';

            if ("success_message" in responseArr) {
                message = responseArr.success_message;
            } else if ("success_message" in responseText) {
                message = responseText.success_message;
            }

            // GTM Tracking
            if (formId === '#frm-call-back') {
                dataLayer.push({'event': '/ImInterested/Success'});
            } else if (formId === '#frm-contact-us') {
                dataLayer.push({'event': '/ContactUs/Success'});
            }

            $('div.contactus-form').html(message);
        }
    });
};

// Unsubscribe Page
pwnApp.unsubscribe.init = function (formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var responseText = $.parseJSON(responseArr);
            $('.unsubscribe-response').css('display', 'block');
            $(".unsubscribe-form-container").hide();
            $('#confirmBlurb').hide();
        },
        error: function (responseArr) {
            var message = utils.obtainMessage(responseArr, 'error');
            utils.outputMessage(formId, message, 'error', false, false, false);
        }
    });
};

// Delete Pin Page
pwnApp.deletePin.init = function (formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var responseText = $.parseJSON(responseArr);
            $('.delete-pin-response').show();
            $(".delete-pin-form-container").hide();
        },
        error: function (responseArr) {
            var message = utils.obtainMessage(responseArr, 'error');
            utils.outputMessage(formId, message, 'error', false, false, false);
        }
    });
};

//Reset Password Page
pwnApp.resetPassword.init = function (formId) {
    console.log('pwnApp.resetPassword.init is now deprecated.');
};

/**
 * @deprecated
 */
pwnApp.login.tabsInit = function () {
    console.log('pwnApp.login.tabsInit is now deprecated.');
    return;
};

/**
 * @deprecated
 */
pwnApp.login.loginPageLoginTab = function () {
    console.log('pwnApp.login.loginPageLoginTab is now deprecated.');
    return;
};

/**
 * @deprecated
 */
pwnApp.login.loginPageCreateALoginTab = function () {
    console.log('pwnApp.login.loginPageCreateALoginTab is now deprecated.');
};

/**
 * @deprecated
 */
pwnApp.login.loginPageTabs_ShowContentForTab = function (tabNumber) {
    console.log('pwnApp.login.loginPageTabs_ShowContentForTab is now deprecated.');
    return;
};

/**
 * @deprecated
 */
pwnApp.login.formInit = function (loginFormID, createALoginFormID) {
    console.log('pwnApp.login.formInit is now deprecated.');
    return;
};

// Outlook Plugin - Schedule a Call Form
pwnApp.outlookPlugin.init = function () {};

// Outlook Plugin - Schedule a Call Form
pwnApp.outlookPlugin.formInit = function (formID, responsive) {
    $(formID).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            // Hide Form + Run GA Tracking + Open Download
            $('form'+formID).hide();
            dataLayer.push({
                'event': '/RightMenuOutlookPlugin/RegistrationSuccess',
                'gcp': window.google_tag_params,
                'PIN': responseArr.pin
            });

            $(formID).next('.outlook-plugin-download-links').css({
                'display': 'block',
                'visibility': 'visible'
            });
        },
        error: function (responseArr) {
            var message = myPwnApp.utils.obtainMessage(responseArr,'error');

            if (responsive) {
                if ($().parsley !== undefined) {
                    utils.outputMessage('#frm-outlook-plugin',message,'error',false,false,true);
                    return true;
                }
            } else {
                utils.outputMessage('#frm-outlook-plugin',message,'error',false,false,false);
            }
            return true;
        }
    });
};

// Homepage E Ajax Full Registration
pwnApp.homepageE.FullRegistration = function () {
    $('#form-create-log-in').initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        beforeSubmit: function () {

            var errors = [];

            if ($('#fullRegistration_first_name').val() == '') {
                errors.push({'message': FORM_VALIDATION_NO_FIRST_NAME, 'field_name' : 'fullRegistration[first_name]'});
            } else if ($('#fullRegistration_last_name').val() == '') {
                errors.push({'message': FORM_VALIDATION_NO_SURNAME, 'field_name' : 'fullRegistration[last_name]'});
            } else if ($('#fullRegistration_password').val() == '') {
                errors.push({'message': FORM_VALIDATION_NO_PASSWORD, 'field_name' : 'fullRegistration[password]'});
            } else if ($('#fullRegistration_password').val().length < 6) {
                errors.push({'message': FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS, 'field_name' : 'fullRegistration[password]'});
            } else if ($('#fullRegistration_password').val() !== $('#fullRegistration_confirm_password').val()) {
                errors.push({'message': FORM_VALIDATION_PASSWORD_MISMATCH, 'field_name' : 'fullRegistration[confirm_password]'});
            }

            if (errors.length == 0) {
                utils.showAjaxLoader('/sfimages/ajaxLoaders/ajax-loader-transparent.gif', $('#create-account-btn'), 'ajax_loader_transparent', 25, 25, true);
                return true;
            } else {
                return showInputErrorsBoxes(errors, 'form-create-log-in');
            }
        },

        success: function (responseArr) {
            // Show New Content
            utils.removeAjaxLoader('ajax_loader_transparent');

            if((typeof responseArr['html'] == 'undefined') || (typeof responseArr['html']['wpHTML'] == 'undefined')) {
                var errors = [];
                errors.push({'message':  FORM_TECHNICAL_ISSUE, 'field_name' : 'fullRegistration[confirm_password]'});
                return showInputErrorsBoxes(errors, 'form-create-log-in');
            }

            $('.reg-step-2').html(responseArr['html']['wpHTML']);
            $('.reg-step-2').show();

            if(typeof responseArr['html']['loginContainer'] != 'undefined') {
                $('#loginContainer').replaceWith(responseArr['html']['loginContainer']);
            }

            // Google Tag Manager
            dataLayer.push({'event': '/Homepage-E/FullRegistrationSuccess'});

        },
        error: function (responseArr) {

            utils.removeAjaxLoader('ajax_loader_transparent');

            if(typeof responseArr['error_messages'] == 'undefined') {
                var errors = [];
                errors.push({'message':  FORM_TECHNICAL_ISSUE, 'field_name' : 'fullRegistration[confirm_password]'});
                return showInputErrorsBoxes(errors, 'form-create-log-in');
            }

            var message = myPwnApp.utils.obtainMessage(responseArr,'error');
            if ("field_name" in message) {
                return showInputErrorsBoxes(message, 'form-create-log-in');
            } else {
                return showInputErrorsBoxes(message[0], 'form-create-log-in');
            }
        }
    });
};

// Homepage E Ajax Request Welcome Pack Registration
pwnApp.homepageE.RequestWelcomePackRegistration = function () {
    $('#index-welcomepack-form').initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        beforeSubmit: function () {
            var errors = [];

            if ($('#welcomePack_building').val() == '') {
                errors.push({'message': FORM_VALIDATION_NO_ADDRESS, 'field_name'  : 'welcomePack[building]'});
            } else if ($('#welcomePack_town').val() == '') {
                errors.push({'message': FORM_VALIDATION_NO_TOWN, 'field_name'  : 'welcomePack[town]'});
            } else if ($('#welcomePack_postal_code').val() == '') {
                errors.push({'message': FORM_VALIDATION_INVALID_POSTCODE, 'field_name'  : 'welcomePack[postal_code]'});
            }

            if (errors.length == 0) {
                return true;
            } else {
                return showInputErrorsBoxes(errors, 'index-welcomepack-form');
            }
        },
        success: function (responseArr) {
            // Show New Content
            $('.reg-step-2').html(responseArr['html']);
            $('.reg-step-2').show();
            $('.green-rounded-box').css('height','410px');

            // Track Event
            dataLayer.push({'event': '/Homepage-E/WelcomePackSuccess', 'gcp': window.google_tag_params});

            // Modify OnClick Event
            var onClick = $('#top-outlook-plugin-link').attr('onclick');
            $('#top-outlook-plugin-link').attr('onclick', onClick + ' rightLandingPageMenuShowTooltip("top-outlook-plugin"); return false;');
        },
        error: function (responseArr) {
            console.log('Form Error');
            var message = myPwnApp.utils.obtainMessage(responseArr,'error');
            if ("field_name" in message) {
                return showInputErrorsBoxes(message, 'form-create-log-in');
            } else {
                return showInputErrorsBoxes(message[0], 'form-create-log-in');
            }
        }
    });
};

// Homepage E Existing User Error Message - Enhanced User
pwnApp.homepageE.existingUserPinReminder = function (inputEmail) {
    if (inputEmail == null) {
        var email = $('input#register_email').val();
    } else {
        var email = inputEmail;
    }
    rightLandingPageMenuShowTooltip('tooltip-pin-reminder');
    $('#pin_reminder_email_address').val(email);
    $('#email-error-box').hide();
}

// Homepage E Existing User Error Message - Other Users
pwnApp.homepageE.existingUserLogin = function () {
    console.warn('pwnApp.homepageE.existingUserLogin is now deprecated.');
}

// Forgotten Password Form
pwnApp.forgottenPassword.init = function (formId) {
    console.warn('pwnApp.forgottenPassword.init is now deprecated.');
};

// Tell A Friend Form
pwnApp.tellAFriend.init = function (formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        beforeSubmit: function () {
            if (!$(formId + ' #tellAFriend_your_name').val()) {
                var message = {};
                message.message = 'FORM_VALIDATION_NO_NAME';
                message.field_name = 'tellAFriend[your_name]';
                utils.outputMessage(formId, message, 'error', false, false, false);
                return false;
            } else if (!utils.isValidEmail($(formId + ' #tellAFriend_your_email').val())) {
                var message = {};
                message.message = 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS';
                message.field_name = 'tellAFriend[your_email]';
                utils.outputMessage(formId, message, 'error', false, false, false);
                return false;
            } else if (!$(formId + ' #tellAFriend_friends_name').val()) {
                var message = {};
                message.message = 'FORM_VALIDATION_NO_FRIENDS_NAME';
                message.field_name = 'tellAFriend[friends_name]';
                utils.outputMessage(formId, message, 'error', false, false, false);
                return false;
            } else if (!utils.isValidEmail($(formId + ' #tellAFriend_friends_email').val())) {
                var message = {};
                message.message = 'FORM_VALIDATION_NO_FRIENDS_EMAIL';
                message.field_name = 'tellAFriend[friends_email]';
                utils.outputMessage(formId, message, 'error', false, false, false);
                return false;
            }
            return true;
        },
        success: function () {
            $('div.tell-a-friend-success-message').show();
            $(formId).hide();
        }
    });
};

// launch the app
pwnApp.init();
