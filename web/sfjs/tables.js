/**
 * Create a DataTable, using a Fixed Column
 * @param selector
 */
function createFixedColumnTable(selector) {
    // dynamically calculates fixed column width with email length, for less than 30 chars it's 300px
    // for more 30 chars it's email length times 10px, for more than 50 chars it's 500px
    var fixedWidth = 300,
        emailMaxLength = 0,
        leftBodyWrapperElement = $('.DTFC_LeftBodyWrapper');

    $('.pinpair').each(function () {
        var splited = $(this).html().split('<br>');
        if (splited.length > 1) emailMaxLength = Math.max(emailMaxLength, splited[1].length);
    });
    if (emailMaxLength > 30) fixedWidth = emailMaxLength * 10;
    if (emailMaxLength > 50) fixedWidth = 500;

    var oTable = $(selector).dataTable({
        "sScrollX": "100%",
        "bScrollCollapse": true,
        "bFilter": false,
        "bSort": false,
        "bPaginate": false
    });

    new FixedColumns(oTable, {
        "sLeftWidth": 'fixed',
        "iLeftWidth": fixedWidth
    });

    leftBodyWrapperElement.css('border-right', 'solid 1px #aaaaaa');
    $('.DTFC_LeftBodyWrapper').find('table tbody tr').css('height', '37px');
}
