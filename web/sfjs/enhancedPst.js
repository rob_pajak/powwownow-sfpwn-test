$(document).ready(function() {
    // Initial Action of the Plus Link
    $('#dialog-plus-link').click(function() {
        resetForms();
    });

    // Reset Forms
    function resetForms () {
        // Close All Popups
        $('#plusFormWithPassword1, #plusRedirectionExisting1').hide();

        // Reset Form Values
        $('#plusFormWithPassword1').find('#plus_signup_first_name, #plus_signup_last_name, #plus_signup_company_name, #plus_signup_business_phone, #plus_signup_confirm_password').removeAttr('value');
        $('#plusFormWithPassword1').find('#plus_signup_first_name').attr('value',$('#plusFormWithPassword1').find('#plus_signup_fname').attr('value'));
        $('#plusFormWithPassword1').find('#plus_signup_last_name').attr('value',$('#plusFormWithPassword1').find('#plus_signup_lname').attr('value'));

        // Show Initial Popup
        $('#plusFormWithPassword1').show();

        // Set the Background Colour
        $('#dialog-plus').css('background','#7EA2B4');

        popupModalFunctions();
        popupInitPlusWithPasswordForm();
    }

    // Generic Popup Modal Functions
    function popupModalFunctions() {
        // Input Focus and Blur
        $('#dialog-plus').find('input').blur(function(){
            $(this).parent('span').removeClass('mypwn-input-container-focused').addClass('mypwn-input-container');
        }).focus(function(){
                $(this).parent('span').removeClass('mypwn-input-container').addClass('mypwn-input-container-focused');
            });

        // On Click Event
        $('.pst-tooltip-close').click(function(){
            $('#dialog-plus').dialog('close');
        });
    }

    // Load the Plus With Password Form
    function popupInitPlusWithPasswordForm () {
        $('#form-plus-registration-with-password1').initMypwnForm({
            debug: 'on',
            requiredAsterisk: 'on',
            messagePos: 'off',
            success: function (responseText) {
                var template = responseText.template;

                switch (template) {
                    case 'plus_redirection_existing':
                        $('#plusFormWithPassword1').hide();
                        $('#plusRedirectionExisting').show();
                        $('#dialog-plus').css('min-height','50px');

                        dataLayer.push({'event': 'PlusDashboardSwitch/RegistrationSuccess'});

                        // Redirect to Basket Page
                        $.post('/s/Basket', {notandc : 'yes'}, 'json')
                            .done(function() {
                                $(location).attr('href', '/s/Basket');
                            })
                            .fail(function(response) {
                                var responseMessage = $.parseJSON(response.responseText);
                                if (responseMessage.alert && responseMessage.error) {
                                    alert(responseMessage.error);
                                }
                            });
                        break;
                    case 'plus_no_switch1':
                        $('#plusFormWithPassword1').hide();
                        $('#plusNoSwitch1').show();
                        break;
                }
            },
            error: function (responseText) {
                $('.nice-error-box').hide();
                $('.plus-registration-error').remove();
                var message = myPwnApp.utils.obtainMessage(responseText,'error');
                outputMessage('#form-plus-registration-with-password1',message,'error',false,false);
            }
        });
    }

    // Output the Message from Either the Success and Error Methods
    function outputMessage (formId,responseText,msgType,redirectOnSuccess,reload) {
        // Check if the Message is a Global
        if (window[responseText.message] !== undefined) {
            responseText.message = window[responseText.message];
        }

        // Show Message
        if ('alert' == responseText.field_name) {
            $(formId).find('.message').remove();
            switch (msgType) {
                case 'error':
                    $(formId).append('<div class="error message grid_24">'+responseText.message+'</div>');
                    $(formId).find('.message').delay(5000).fadeOut('slow');

                    if (false !== reload) {
                        window.setTimeout(function (){ window.location.reload(); }, 5500);
                    }
                    break;
                case 'success':
                    $(formId).append('<div class="success message grid_24">'+responseText.message+'</div>');
                    $(formId).find('.message').delay(5000).fadeOut('slow');

                    if (false !== redirectOnSuccess && window[redirectOnSuccess] !== undefined) {
                        window.setTimeout(function (){ window.location = window[redirectOnSuccess]; }, 5500);
                    } else if (false !== redirectOnSuccess && typeof redirectOnSuccess !== 'undefined') {
                        window.setTimeout(function (){ window.location = redirectOnSuccess; }, 5500);
                    } else if (false !== reload) {
                        window.setTimeout(function (){ window.location.reload(); }, 5500);
                    }

                    break;
                default:
                    // Do Something
                    break;
            }
        } else {
            switch (msgType) {
                case 'error':
                    $(formId).find('input[name="'+responseText.field_name+'"]').parent().after('<div class="plus-registration-error">'+responseText.message+'</div>');
                    $(formId).find('.plus-registration-error').delay(5000).fadeOut('slow');
                    break;
            }
        }
    };

    // Plus Admin Post Request
    function postPlusAdminToBasket () {
        $.post('/s/Basket', {}, 'json')
            .done(function() {
                $(location).attr('href', '/s/Basket');
            })
            .fail(function(response) {
                var responseMessage = $.parseJSON(response.responseText);
                if (responseMessage.alert && responseMessage.error) {
                    alert(responseMessage.error);
                }

                if(responseMessage.modal == 'accountHasBundle') {
                    $('#accountHasBundle-modal').dialog( "open" );

                    $('#accountHasBundle-modal button').click(function() {
                        $('#accountHasBundle-modal').dialog("close");
                        $('#dialog-plus').dialog('close');
                    });
                }
            });
    }
});