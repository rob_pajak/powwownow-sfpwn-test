/** Contains English messages used on the sites. **/
/** Both the Teamsite and Symfony Messages have been Re-organised **/

var
    /** Locale and Internationalised **/
    //LANGUAGE = 'en',
    COUNTRY_CODE = 'GB',
    //LANGUAGE_COUNTRY = 'en_GB',

    /** URLs for Tabs **/
    HOMEPAGE_CANONICAL_URL = 'http://www.powwownow.co.uk',
    HOMEPAGE_PLUS_TAB_REGISTRATION = '/Registration-For-Plus',
    WEB_CONFERENCING_CANONICAL_URL = 'http://www.powwownow.co.uk/Web-Conferencing',
    VIDEO_CONFERENCING_CANONICAL_URL = 'http://www.powwownow.co.uk/Video-Conferencing',
    WEB_CONFERENCING_DEMO_TAB = '/Web-Conferencing/Demo',
    WEB_CONFERENCING_GET_STARTED_TAB = '/Web-Conferencing/Get-Started',
    LOGIN_PAGE_CANONICAL_URL = 'http://www.powwownow.co.uk/Login',
    LOGIN_PAGE_CREATE_A_LOGIN_TAB = '/Create-A-Login',

    /** Headings and Titles for tabs **/
    WEB_CONFERENCING_SHOWTIME_TITLE = 'Web conference and screenshare services at Powwownow',
    WEB_CONFERENCING_SHOWTIME_H1 = 'Web Conferencing',
    WEB_CONFERENCING_DEMO_TITLE = 'Free UK Web Conferencing and Web Conference Service - Demo - Powwownow',
    WEB_CONFERENCING_DEMO_H1 = 'Web Conferencing',
    WEB_CONFERENCING_GET_STARTED_TITLE = 'Free UK Web Conferencing and Web Conference Service - Get Started - Powwownow',
    WEB_CONFERENCING_GET_STARTED_H1 = 'Web Conferencing',

    /** Page URLs **/
    URL_FORGOTTEN_PASSWORD = '/Forgotten-Password',
    URL_IN_CONFERENCE_CONTROLS = '/In-Conference-Controls',
    URL_INTERNATIONAL_DIAL_IN_NUMBERS = '/International-Dial-In-Numbers',
    URL_PIN_REMINDER = '/PIN-Reminder',
    URL_MYPWN_LOGIN = '/ajax/mypwn/login',
    URL_MYPWN_START_PAGE = '/myPwn/',
    URL_MYPWN_PLUS_CONFIRMATION_PAGE = '/myPwn/New-Registration',
    URL_MYPWN_PLUS_INVITE_ACCEPT_CONFIRMATION_PAGE = '/myPwn/?invEx',
    URL_MYPWN_PLUS_MYPIN_PRODUCTS_PAGE = '/s/Assign-Products',
    URL_MOBILE_APP = '/Mobile-App',
    URL_MYPWN_PINS = '/myPwn/Pins',
    URL_MYPWN_USERS = '/myPwn/Users',
    URL_MYPWN_CALL_SETTINGS = '/myPwn/Call-Settings',
    URL_MYPWN_REQUEST_WALLET_CARD = '/myPwn/Request-Welcome-Pack',
    //URL_PLUS_UPGRADE_PAGE = '../account/UpgradeToPlus',
    //URL_PLUS_HOMEPAGE_PAGE = '/',
    URL_PLUS_CREATE_A_LOGIN_TAB = '../login/login',
    URL_PLUS_CREATE_A_LOGIN_HASH = '#tab-2',

    /** Form Validation - General **/
    //FORM_SESSION_EXPIRED = 'For security, your session has expired. Please log back in to carry on using myPowwownow.',
    //ERROR_MESSAGE_CSRF = 'CSRF attack detected. Please Reload the Page',
    FORM_COMMUNICATION_ERROR = 'Sorry, we are currently experiencing technical difficulties. Please try again later',
    FORM_NO_PERMISSION_ERROR = 'You do not have any permission to access the page',
    FORM_VALIDATION_ERRORS_FOUND = "Errors found on this form",
    FORM_VALIDATION_SUCCESS_MESSAGE = "Form has been successfully submitted",
    FORM_UNKNOWN_ERROR = "Unknown Error Occured",
    FORM_VALIDATION_FIELD_EMPTY = "Empty field",
    FORM_VALIDATION_INVALID_PATTERN = "Invalid field value",
    FORM_VALIDATION_INVALID_NUMBER = "Not a valid number",
    FORM_VALIDATION_OVER_MAX = "Field value must be less than {maxValue}",
    FORM_VALIDATION_UNDER_MIN = "Field value must be bigger than {minValue}",
    FORM_VALIDATION_LENGTH_UNDER_MIN = "Field length must be more than {minLength} characters",
    FORM_VALIDATION_LENGTH_OVER_MAX = "Field length must be less than {maxLength} characters",
    FORM_VALIDATION_LENGTH_BETWEEN = "Field length must be between {minLength} and {maxLength} characters",
    FORM_VALIDATION_MARKS_UNDER_MIN = "Field value must be between {minValue} and {maxValue}",
    FORM_VALIDATION_MARKS_OVER_MAX = "Field value must be between {minValue} and {maxValue}",

    /** Form Validation - Email **/
    FORM_VALIDATION_INVALID_EMAIL_ADDRESS = 'Please enter a valid email address',
    FORM_VALIDATION_NO_EMAIL = 'Please enter your email address',
    FORM_VALIDATION_EMAIL_MISMATCH = 'Please make sure you retyped your email address correctly',
    FORM_VALIDATION_EMAIL_NOT_FOUND = 'Your email address could not be found in our system. Please make sure you type the correct address',
    FORM_EMAIL_CHANGED_WARNING = 'Please be aware that from now on you will have to use your new email as your login ID',
    EMAIL_NOT_ENHANCED = 'We could not find an account associated with this email address.',
    FORM_VALIDATION_EMAIL_EMPTY = 'Please enter your email address',
    FORM_VALIDATION_EMAIL_NOT_FOUND_PLUS = 'We could not find an account associated with this email address. <br/><a href="' + HOMEPAGE_PLUS_TAB_REGISTRATION + '">To register for Plus please fill in the form in this page.</a>',
    FORBIDDEN_EMAIL = 'Please enter a valid Email. This Email cannot be switched to PLUS.',
    FORM_VALIDATION_NO_FRIENDS_EMAIL = 'Please enter a valid email address for your friend',
    API_RESPONSE_EMAIL_ALREADY_TAKEN = 'You already have an account with us.<br/>If you have forgotten your PIN<br/>please use the <a href="javascript:pwnApp.homepageE.existingUserPinReminder();">PIN Reminder</a> form',
    API_RESPONSE_EMAIL_ALREADY_TAKEN_OTHER = 'You already have an account with us.<br/>If you have forgotten your PIN<br/>please use the <a href="/Login">login</a> form to see your PINs',
    API_RESPONSE_EMAIL_ALREADY_TAKEN_RESPONSIVE_OTHER = 'You already have an account with us. Please Login <a href="http://www.powwownow.co.uk/Login">Here</a>',
    API_RESPONSE_EMAIL_ALREADY_TAKEN_RESPONSIVE = 'You already have an account with us. If you have forgotten your PIN. Please use the <a href="http://www.powwownow.co.uk/Pin-Reminder">PIN Reminder</a> form',
    API_RESPONSE_ACCOUNT_ALREADY_CREATED_FOR_REQUESTED_DATA = 'An account for this e-mail has already been created. If you have forgotten your password <a href="/Forgotten-Password">click here</a>.',
    API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE = 'You already have an account with us, but your account is inactive.<br/>Please contact us for assistance on 0203 398 0398.',
    API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE1 = 'You already have an account with us, but your account is inactive. Please contact us for assistance on 0203 398 0398.',
    API_RESPONSE_EMAIL_TAKEN_AND_NON_STANDARD_PWN = 'It appears you are already a Premium customer or have signed up with one of our partners.<br><br>If you require further assistance then please contact us on 0203 398 0398.',
    API_RESPONSE_EMAIL_TAKEN_AND_NON_STANDARD_PWN_RESPONSIVE = 'It appears you are already a Premium customer or have signed up with one of our partners. If you require further assistance then please contact us on 0203 398 0398.',
    API_RESPONSE_EMAIL_TAKEN_AND_IS_PLUS = 'It appears you are already a Plus customer or have signed up with one of our partners<br/><br/>If you require further assistance then please contact us on 0203 398 0398',
    API_RESPONSE_NO_ACCOUNT_FOR_EMAIL = 'We could not find an account associated with that email address',
    FORM_VALIDATION_FRIEND1_EMAIL_EMPTY = "Please enter email of your Friend",
    FORM_VALIDATION_FRIEND2_EMAIL_EMPTY = "Please enter email of your Friend",
    FORM_VALIDATION_FRIEND3_EMAIL_EMPTY = "Please enter email of your Friend",

    /** Form Validation - Password **/
    FORM_VALIDATION_INVALID_PASSWORD = '<p style="width:240px; display: block; white-space: normal;">Your email and password are not recognised. Please amend and try again<br /><br />If you are a registered User but have forgotten your password please use <a href="/Forgotten-Password">Password Reminder</a><br /><br />If you are a registered User but haven' + "'" + 't created a login please <a href="/Create-A-Login">create one here</a></p>',
    FORM_VALIDATION_NO_PASSWORD = 'Please enter a password',
    FORM_VALIDATION_PASSWORD_EMPTY = 'Please enter a password',
    FORM_VALIDATION_PASSWORD_MISMATCH = 'Please make sure you retyped your password correctly',
    FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS = 'Please make sure password is at least 6 characters long',
    FORM_VALIDATION_INVALID_PASSWORD_SPACES = 'Please make sure password does not start or end in a space',
    FORM_VALIDATION_FORGOT_PASSWORD_INVALID_TOKEN = 'We were unable to validate your email address. Please fill out the forgot password form',
    FORM_VALIDATION_FORGOT_PASSWORD_EXPIRED_TOKEN = 'The time to reset your password has expired. Please fill out the forgotten password form again.',
    FORM_VALIDATION_NEW_PASSWORD_EMPTY = 'Please enter your new password',
    FORM_VALIDATION_CONFIRM_PASSWORD_EMPTY = 'Please make sure you retyped your password correctly',
    INVALID_EMAIL_PASSWORD_CO = 'Your password is incorrect',
    FORM_VALIDATION_WRONG_PASSWORD = 'Your password is incorrect',
    API_RESPONSE_PASSWORD_NOT_SET = 'You have not yet created a password to access myPowwownow. Please <a href="/Create-A-Login">create a login</a> first',
    FORM_VALIDATION_USER_DETAILS_PASSWORD_SUCCESS = 'Your Password has been successfully updated',
    FORGOTTEN_PASSWORD_UNKNOWN_ERROR = 'An Unknown Error Occurred trying to email your forgotten password.',
    PASSWORD_NOT_CHANGED = 'Your Password could not be changed. Please Try again later, or contact Customer Services.',
    FORM_VALIDATION_PASSWORD_TOO_LONG = 'Your Password is too long. Please reduce it to less than 50 characters.',

    /** Form Validation - Names **/
    FORM_VALIDATION_NO_NAME = 'Please enter your name',
    FORM_VALIDATION_NO_FIRST_NAME = 'Please enter your first name',
    FORM_VALIDATION_NO_SURNAME = 'Please enter your last name',
    FORM_VALIDATION_FIRST_NAME_EMPTY = 'Please enter first name',
    FORM_VALIDATION_LAST_NAME_EMPTY = 'Please enter last name',
    FORM_VALIDATION_NO_FRIENDS_NAME = 'Please enter your friend\'s name',
    FORM_VALIDATION_FRIEND1_FIRST_NAME_EMPTY = "Please enter first name of your Friend",
    FORM_VALIDATION_FRIEND2_FIRST_NAME_EMPTY = "Please enter first name of your Friend",
    FORM_VALIDATION_FRIEND3_FIRST_NAME_EMPTY = "Please enter first name of your Friend",
    FORM_VALIDATION_FRIEND1_LAST_NAME_EMPTY = "Please enter last name of your Friend",
    FORM_VALIDATION_FRIEND2_LAST_NAME_EMPTY = "Please enter last name of your Friend",
    FORM_VALIDATION_FRIEND3_LAST_NAME_EMPTY = "Please enter last name of your Friend",
    FORM_VALIDATION_CONTACT_NAME_EMPTY = "Enter contact name",
    FORM_VALIDATION_INVALID_CONTACT_FIRST_NAME = 'Please give a valid first name.',
    FORM_VALIDATION_INVALID_CONTACT_LAST_NAME = 'Please give a valid last name.',

    /** Form Validation - PIN and Contact **/
    FORM_VALIDATION_INVALID_CONTACT = 'Please choose a valid existing User, otherwise create a new User',
    FORM_VALIDATION_INVALID_CONTACT_REF = 'An Invalid Contact Reference has been selected. Please try again.',
    PIN_AND_CONTACT_DO_NOT_MATCH = 'The email address and specified PIN do not match',
    PIN_ALREADY_INACTIVE = 'Your PIN is already inactive',
    PIN_ALREADY_TAKEN = 'That PIN has already been taken',
    CANNOT_DEACTIVATE_ACCOUNT_USING_PARTICIPANT_PIN = 'A PIN cannot be deleted using the Participant PIN. Please enter the Chairperson PIN.',
    FORM_VALIDATION_INVALID_SIX_DIGIT_PIN = 'Please enter a valid 6 digit PIN',
    FORM_VALIDATION_INVALID_PIN = 'This PIN appears to be invalid',
    FORM_VALIDATION_NO_PIN = 'Please select a PIN',
    FORM_VALIDATION_NO_PINS_SELECTED = 'Please select a PIN',
    FORM_VALIDATION_NO_PERMISSION_TO_EDIT_PIN = 'You do not have permission to edit that PIN',
    FORM_CONFIRM_PIN_DELETION = 'Are you sure you wish to delete the selected PINs?',
    FORM_RESPONSE_PINS_DELETED_SUCCESS = 'The selected PINs have been deleted',
    FORM_RESPONSE_PINS_DELETED_FAILURE = 'An error occurred deleting the PINs',
    FORM_RESPONSE_PINS_DELETED_SUCCESS_AND_FAILURE = 'Not all PINs could be deleted',
    FORM_RESPONSE_PINS_DELETED_FAILURE_ADMIN_PIN = 'As the account administrator you are required to have a valid set of PINs',
    FORM_RESPONSE_PINS_UPDATE_SUCCESS = 'The selected PINs have been deleted',
    FORM_RESPONSE_PINS_UPDATE_FAILURE = 'An error occurred updating the PINs',
    FORM_RESPONSE_PINS_UPDATE_SUCCESS_AND_FAILURE = 'There was a problem updating some of your PINs.',
    FORM_RESPONSE_PINS_PRODUCTS_UPDATE_SUCCESS = 'Your account has been successfully updated',
    API_RESPONSE_SYSTEM_ERROR_PIN_ACCOUNT_PRODUCTS = 'A system error occurred trying to retieve you PIN / Account Product Information.',
    API_RESPONSE_SYSTEM_ERROR_PIN_ACCOUNT_PRODUCTS_UPDATE = 'A system error occurred trying to update you PIN / Account Product Information.',
    API_RESPONSE_CONTACT_IS_NOT_STANDARD_POWWOWNOW_USER = 'In order to see your PIN(s) please Login to <a href="/Login">myPwn</a>.<br/>If you forgot your password please <a href="/Forgotten-Password">click here</a>.',
    API_RESPONSE_CONTACT_IS_NOT_STANDARD_POWWOWNOW_USER1 = 'In order to see your PIN(s) please Login to <a href="/Login">myPwn</a>. If you forgot your password please <a href="/Forgotten-Password">click here</a>.',
    FORM_CREATE_PIN_ERROR = 'An error occurred creating the PIN',
    FORM_UPDATE_CONTACT_ERROR = 'An error occurred updating the contact',
    FORM_VALIDATION_USER_DETAILS_CONTACT_SUCCESS = 'Your User Details have been successfully updated',
    FORM_GODMODE_NONMATCHING_CONTACT = 'The given contact does not match the given email.',

    /** Form Validation - Terms And Conditions **/
    FORM_VALIDATION_T_AND_C = 'Please read our terms and conditions and mark the checkbox if you agree',
    FORM_VALIDATION_T_AND_C_EMPTY = 'Please read our terms and conditions and mark the checkbox if you agree',

    /** Form Validation - Login **/
    FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_ADMIN = "Your account has been suspended.<br />Please contact Customer Service for further information",
    FORM_VALIDATION_LOGIN_FAILED_SUSPENDED_ACCOUNT_USER = "Your account has been suspended.<br />Please contact your administrator for further information",
    FORM_VALIDATION_LOGIN_ERROR = "An error occurred trying to log you in",
    FORM_VALIDATION_EMAIL_LOGIN_ERROR = 'Could not Login. Please check if the user has any Active PINs under Master Service 900<br/>(belonging to Services: Plus-850, Free-801, or any Powwownow Premium).',
    FORM_RESPONSE_LOG_IN_SUCCESSFUL = 'You have successfully logged in',
    FORM_RESPONSE_LOG_IN_UNSUCCESSFUL = 'Your log in was unsuccessful, please try again',
    FORM_RESPONSE_LOG_IN_UNABLE = 'We are currently unable to log you in to your account',
    FORM_RESPONSE_LOGOUT_UNSUCCESSFUL = 'Your logout was unsuccessful',
    REGISTRATION_ERROR = "Powwownow has encountered a problem and can not complete the request. Please try again. We are sorry for the inconvenience",

    /** Form Validation - Date **/
    FORM_VALIDATION_INVALID_TO_DATE = 'Please enter the To Date in the correct Format: yyyy-mm-dd',
    FORM_VALIDATION_INVALID_TO_DATE_DAY = 'Please enter a valid Day in the To Date field',
    FORM_VALIDATION_INVALID_TO_DATE_MONTH = 'Please enter a valid Month in the To Date field',
    FORM_VALIDATION_INVALID_TO_DATE_YEAR = 'Please enter a valid Year in the To Date field',
    FORM_VALIDATION_INVALID_FROM_DATE = 'Please enter the From Date in the correct Format: yyyy-mm-dd',
    FORM_VALIDATION_INVALID_FROM_DATE_DAY = 'Please enter a valid Day in the From Date field',
    FORM_VALIDATION_INVALID_FROM_DATE_MONTH = 'Please enter a valid Month in the From Date field',
    FORM_VALIDATION_INVALID_FROM_DATE_YEAR = 'Please enter a valid Year in the From Date field',
    FORM_VALIDATION_INVALID_DATE_RANGE = 'Please enter a valid date range. The From Date must be older than the To Date.',
    FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_DATE = 'Please enter the Expiry Date',
    FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_DAY = 'Please enter a valid Day in the Publish Expire field',
    FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_MONTH = 'Please enter a valid Month in the Publish Expire field',
    FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_YEAR = 'Please enter a valid Year in the Publish Expire field',
    FORM_VALIDATION_INVALID_PUBLISH_EXPIRE_OLD = 'Please enter a valid Date in the future for the Publish Expire field',
    DATE_TOO_OLD = 'Please select a date range of at most 100 days.',

    /** Form Validation - Other **/
    FORM_VALIDATION_NO_COMPANY_NAME = 'Please enter a company name',
    FORM_VALIDATION_COMPANY_NAME_EMPTY = 'Please enter a company name',
    FORM_VALIDATION_INVALID_PHONE_NUMBER = 'Please enter a valid phone number',
    FORM_VALIDATION_NO_MOBILE_NUMBER = 'Please enter your mobile number',
    INVALID_MOBILE_NUMBER = 'Please enter a valid mobile number',
    FORM_VALIDATION_HOW_HEARD_OF_PWN_NOT_SELECTED = 'Please tell us how you heard about Powwownow',
    FORM_VALIDATION_NO_COMMENTS = 'Please enter a comment',
    FORM_VALIDATION_NO_OPERATING_SYSTEM = 'Please choose your operating system',
    FORM_VALIDATION_NO_ADDRESS = 'Please enter your street name',
    FORM_VALIDATION_NO_TOWN = 'Please enter your town',
    FORM_VALIDATION_INVALID_POSTCODE = 'Please enter a valid postcode',
    FORM_VALIDATION_NO_LANGUAGE = 'Please select a voice prompt language',
    FORM_VALIDATION_BUSINESS_PHONE_EMPTY = 'Please enter your business phone number',
    FORM_VALIDATION_NO_COUNTRY = 'Please select a country.',
    FORM_VALIDATION_NO_QUESTION = 'Please select an answer to the question',
    API_RESPONSE_COST_CALCULATOR_FAILED = 'An error occurred calculating the call cost',
    API_RESPONSE_WEB_CONFERENCE_STATUS_DENIED = 'A system error occurred, which means you cannot download Powwownow Web right now. If you need to connect urgently please call 0203 398 0398',
    API_RESPONSE_WEB_CONFERENCE_ERROR = 'A system error has occurred, which means you cannot download Powwownow Web right now. If you need to connect urgently please call 0203 398 0398',
    FORM_VALIDATION_INVALID_PROMOCODE = 'Your promocode is incorrect',
    FORM_VALIDATION_BILLING_ORGANISATION_EMPTY = "Please enter your organisation name",
    FORM_VALIDATION_BUILDING_EMPTY = FORM_VALIDATION_NO_ADDRESS,
    FORM_VALIDATION_TOWN_EMPTY = FORM_VALIDATION_NO_TOWN,
    FORM_VALIDATION_POSTAL_CODE_EMPTY = "Please enter your postcode",
    FORM_VALIDATION_POSTAL_CODE_INVALID_PATTERN = FORM_VALIDATION_INVALID_POSTCODE,
    FORM_VALIDATION_COMMENT_EMPTY = "Comment field is empty",
    FORM_VALIDATION_COMPANY_EMPTY = "Enter company name",
    FORM_VALIDATION_SCRIPT_ACCEPT_EMPTY = "Please mark the checkbox if you agree",
    FORM_VALIDATION_INVALID_CONTACT_TITLE = 'Please give a valid title.',
    FORM_VALIDATION_INVALID_CONTACT_COUNTRY = 'Please select one of the provided countries.',
    FORM_VALIDATION_INVALID_MIDDLE_INITIALS = 'Please give valid middle initials.',
    FORM_VALIDATION_MAXLENGTH_ORGANISATION = 'Please give a shorter organisation name.',
    FORM_VALIDATION_MAXLENGTH_ACCOUNT_NAME = 'Please give a shorter account name.',
    FORM_VALIDATION_MAXLENGTH_VAT_NUMBER = 'Please give a shorter VAT Number.',
    FORM_VALIDATION_BUNDLE_ID_NOT_SET = 'No Bundle ID was given.',
    FORM_VALIDATION_INVALID_COST_CODE = 'Please enter a cost code with fewer than 50 characters.',
    FORM_VALIDATION_CONTACT_NUMBER_EMPTY = "Enter contact number",
    TELL_A_FRIEND_FAILED_TO_SEND = 'Your Email failed to be sent due to an Unknown Error.',

    /** Form Validation - Wallet Card Request **/
    WALLET_CARD_REQUEST_SUCCESS = 'Thank you for your request. Your welcome pack is on its way!',
    WALLET_CARD_REQUEST_FAILURE = 'An error occurred ordering your welcome pack',
    WALLET_CARD_REQUEST_EXCEEDED_MAX = 'Your welcome pack request was unsuccessful as it has been requested 3 times already. Please Contact Us if you would like more welcome packs',
    WALLET_CARD_REQUEST_NO_PERMISSION = 'You do not have permission to request welcome packs for that User',
    WALLET_CARD_REQUEST_SUCCESS_SINGLE = 'Thank you for your request. Your welcome pack is on its way for the PIN pair of %%PINPAIR%%.',
    WALLET_CARD_REQUEST_FAILURE_SINGLE = 'An error occurred ordering the welcome pack for the PIN pairs of %%PINPAIR%%',
    WALLET_CARD_REQUEST_EXCEEDED_MAX_SINGLE = 'Your welcome pack request was unsuccessful for the PIN pairs of %%PINPAIR%%, as it has been requested 3 times already. Please Contact Us if you would like more welcome packs for this PIN pair.',
    WALLET_CARD_REQUEST_NO_PERMISSION_SINGLE = 'You do not have permission to request welcome packs for the PIN pair of %%PINPAIR%%.',
    WALLET_CARD_PINS_TABLE_ONLY_ONE_PIN = 'Please select only one PIN, as only one welcome pack can be selected at a time',
    WALLET_CARD_REQUEST_ONE_PIN = 'Please select only one PIN',

    /** Form Validation - Create User **/
    FORM_CREATE_USER_ERROR = 'An error occurred creating the User',
    FORM_CREATE_USER_EMAIL_TAKEN = 'That email address is already in use',
    FORM_CREATE_USER_EMAIL_TAKEN_INVITE = 'That email address is already in use.<br/><a href="#" id="plus_invite">Invite this User to Plus</a>',
    FORM_CREATE_USER_EMAIL_TAKEN_BY_INACTIVE_ACCOUNT = 'That email address is already in use but by an inactive account. Please contact Customer Services if you would like the email reinstated.',
    FORM_CREATE_USER_EMAIL_TAKEN_OTHER = 'That email address is already in use and cannot be joined automatically. Please contact Customer Services.',
    FORM_CREATE_USER_INVITATION_SUCCESS = 'User has been succesfully invited by email. After accepting the invitation they will be joined to your account.',

    /** Form Validation - Update Account **/
    FORM_VALIDATION_NO_ACCOUNT_NAME = 'Please enter an Account Name',
    FORM_VALIDATION_NO_ORGANISATION = 'Please enter an Organisation',
    FORM_VALIDATION_CHANGE_EMAIL_ERROR = 'Your email cannot be changed via myPowwownow. Please contact Customer Services if you would like to change your email',

    /** Form Validation - Recording **/
    FORM_RESPONSE_RECORDINGS_NOT_FOUND = 'The recording was not found.',
    FORM_RESPONSE_RECORDINGS_INVALID_VALUE = 'The recording was not saved successfully.',

    /** Form Validation - Call Settings **/
    MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT = 'Please select if a Chairperson should be present',
    MYPWN_CALLSETTINGS_CONFIRM = "This will update all the selected PINs Call Settings to the details shown on the page. Reverting back to the old settings is not available. Are you sure you want to go ahead?",
    MYPWN_CALLSETTINGS_SUCCESS = "<strong>Your call settings have been updated</strong>",
    FORM_VALIDATION_NO_MUSIC_ON_HOLD = 'Please select an on-hold Music.',

    /** Form Validation - Plus Registration Upgrade Messages **/
    PLUS_UPGRADE_SUCCESS = "Your Account has been successfully switched to Plus.",
    PLUS_UPDATE_ADDRESS_SUCCESS = "Your addresses have been successfully updated",

    /** Form Validation - Plus Top-up **/
    FORM_VALIDATION_CARDTYPES_EMPTY = "Please choose your payment card type",
    FORM_VALIDATION_AMOUNT_INVALID = "Please enter a valid top-up amount.",
    FORM_VALIDATION_AMOUNT_TOO_SMALL_CREDIT_BIG = "You must enter the same amount as your purchase, plus any additional call credit you require.",
    FORM_VALIDATION_AMOUNT_TOO_SMALL_CREDIT_SMALL = "You must enter the same amount as your purchase, plus £5 or more for call credit.",
    FORM_VALIDATION_AMOUNT_TOO_BIG = "Please enter a top-up amount below 4166.",
    FORM_VALIDATION_AMOUNT_TOO_SMALL = "Please enter a top-up amount above 20.",

    /** Default Text on Form Fields **/
    DEFAULT_TEXT_EMAIL_ADDRESS_HOME_PAGE_REGISTER = 'Please enter your email address here',
    DEFAULT_TEXT_EMAIL_ADDRESS = 'Please enter your email address',
    DEFAULT_TEXT_MOBILE = 'Please enter your mobile number',
    INFO_MESSAGE_CLI = 'Caller Line Identification (CLI) is the number that you are ringing from. Some phones can also be configured to withhold the number.',

    /** Page Redirects After Login Array **/
    MYPWN_LOGIN_REDIRECTS = [
        '/myPwn/Schedule-A-Call',
        '/myPwn/Web-Conferencing',
        '/myPwn/Request-Welcome-Pack',
        '/myPwn/Dial-In-Numbers',
        '/myPwn/Call-Settings',
        '/myPwn/Account-Details',
        '/myPwn/Pins',
        '/myPwn/Recordings',
        '/myPwn/In-Conference-Controls',
        '/myPwn/Call-History',
        '/myPwn/Get-Plus',
        '/s/Assign-Products',
        '/myPwn/Products',
        '/s/Auto-Topup',
        '/s/Purchase-Credit'
    ],

    /** jQuery Date Picker **/
    JQUERY_DATE_PICKER_DAY_NAMES_MIN = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    JQUERY_DATE_PICKER_MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

    /** jQuery DataTables **/
    DATA_TABLES_sProcessing = "Processing...",                                  //This is shown when the Table is being formed.
    DATA_TABLES_sLengthMenu = "Show _MENU_ entries",                            //This is the Drop down box label. _MENU_ is the dropdown box.
    DATA_TABLES_sZeroRecords = "No matching records found",                      //This is shown when there are no records found.
    DATA_TABLES_sEmptyTable = "No data available in table",                     //This is shown when the table is empty
    DATA_TABLES_sLoadingRecords = "Loading...",                                     //This is shown when the Table is Loading.
    DATA_TABLES_sInfo = "Showing _START_ to _END_ of _TOTAL_ entries",    //This is shown below the Table on the left side. _START_ , _END , and _TOTAL_ are values of the current table records being shown.
    DATA_TABLES_sInfoEmpty = "Showing 0 to 0 of 0 entries",                    //This is for any empty results table.
    DATA_TABLES_sInfoFiltered = "(filtered from _MAX_ total entries)",            //This is shown when the results are Filtered. _MAX_ are the maximum records in the table.
    DATA_TABLES_sSearch = "Search:",                                        //This is the Search Label shown next to the Search box
    DATA_TABLES_sFirst = "First",                                          //This is shown below the table, and is the First Button Label.
    DATA_TABLES_sPrevious = "Previous",                                       //This is shown below the table, and is the Previous Button Label.
    DATA_TABLES_sNext = "Next",                                           //This is shown below the table, and is the Next Button Label.
    DATA_TABLES_sLast = "Last",                                           //This is shown below the table, and is the Last Button Label.

    /** myPwn Recordings Update Messages **/
    MYPWN_RECORDINGS_DESCRIPTION = "Description",
    MYPWN_RECORDINGS_PUBLISH_ONLINE = "Publish online",
    MYPWN_RECORDINGS_PUBLISH_UNTIL = "Publish until",
    MYPWN_RECORDINGS_REQUIRE_PASSWORD = "Require Password",
    MYPWN_RECORDINGS_PASSWORD = "Password",
    MYPWN_RECORDINGS_OK = "OK",
    MYPWN_RECORDINGS_SAVE = "Save",
    MYPWN_RECORDINGS_CANCEL = "Cancel",
    MYPWN_RECORDINGS_LINK_TEXT = "Copy the link below and paste it into an email to share with others",

    /** Dial-In-Numbers Popup on myPwn POWWOWNOW User Homepage */
    MYPWN_DIALINNUMBER_WORLDWIDE = "Worldwide &",
    MYPWN_DIALINNUMBER_SKYPE = "SkypeOut",
    MYPWN_AUTO_TOPUP_DISABLE_CONFIRM = "Are you sure you wish to disable auto top-up on your account?",
    MYPWN_AUTO_TOPUP_DISABLE_SUCCESS = "Auto top-up is disabled"

;