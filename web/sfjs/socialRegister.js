var onSelectFreeService = function(){
    $('#service').val('Free');
    $('#service-free-checkbox').attr('checked',true);
    $('#service-plus-checkbox').attr('checked',false);
};
var onSelectPlusService = function(){
    $('#service').val('Plus');
    $('#service-plus-checkbox').attr('checked',true);
    $('#service-free-checkbox').attr('checked',false);
};

$(document).ready(function(){
    $('#service-free').click(onSelectFreeService);
    $('#service-plus').click(onSelectPlusService);

    $('#form-plus-signin').ajaxForm({
        url: '/login/SocialPwnAuthAndLinkAjax',
        beforeSubmit: formLogInSocial_Validate,
        success: function (responseText) {
            var service = $('#service').val();
            if ('Free' === service) {
                dataLayer.push({'event': 'OAuth - Social Connect Success - Free/onClick'});
            } else if ('Plus' === service) {
                dataLayer.push({'event': 'OAuth - Social Connect Success - Plus Switch/onClick'});
            }

            $('#social-login-register-result').html(responseText);
        },
        error: function (responseText) {
            if (responseText.responseText != '') {
                eval('errors = ' + responseText.responseText);
                showInputErrorsBoxes(errors, 'form-plus-signin');
            }
            return false;
        }
    });
});

var formLogInSocial_Validate = function(){
    dataLayer.push({'event': 'OAuth - Social Connect/onClick'});

    var errors = [];
    if (!isValidEmail($('form#form-plus-signin input[name$="email"]').val())) {
        errors.push({'message': FORM_VALIDATION_INVALID_EMAIL_ADDRESS, 'field_name': 'email'});
        return showInputErrorsBoxes(errors, 'form-plus-signin');
    }

    if (($('form#form-plus-signin input[name$="password"]').val() == '')) {
        errors.push({'message': FORM_VALIDATION_PASSWORD_EMPTY, 'field_name': 'password'});
        return showInputErrorsBoxes(errors, 'form-plus-signin');
    }

    return showInputErrorsBoxes(errors, 'form-plus-signin');

};

$(function () {
    $('#form-plus-signup').initMypwnForm({
        debug: 'on',
        requiredAsterisk: 'on',
        messagePos: 'off',
        beforeSubmit: function() {
            var errors = [],
                password = $('#plus_signup_password').val(),
                confirm_password = $('#plus_signup_confirm_password').val();

            if (password !== confirm_password) {
                errors.push({'message': FORM_VALIDATION_PASSWORD_MISMATCH, 'field_name' : 'confirm_password'});
                $.showInputErrors(errors, '','', '#form-plus-signup');
                return false;
            }

            if ($('#service').val() == '') {
                dataLayer.push({'event': 'OAuth - Social Register/onClick'});
                showChooseServicePopUp();
                return false;
            }
            return true;
        },
        success: function (responseText) {
            // Don't Think Below Code is getting Executed. The Redirection Partial gets Triggered, and redirects.
            var service = $('#service').val();
            if (service == 'Free') {
                dataLayer.push({'event': 'OAuth - Free Registration Success/onClick', 'PIN': responseText.pin});
            } else if (service == 'Plus') {
                dataLayer.push({'event': 'OAuth - Plus Registration Success/onClick'});
            }
            $('#social-login-register-result').html(responseText);
        },
        error: function (responseText) {
            var errors = [];
            // Only FORM_COMMUNICATION_ERROR can be thrown
            // there are no others errors that can occur, so we
            // don't need to do anything fancy here.

            errors.push({'message': FORM_COMMUNICATION_ERROR, 'field_name' : 'alert'});
            $.showInputErrors(errors, '','', '#form-plus-signup');
        }
    });
});

var showChooseServicePopUp = function(){
    $('#choose-service').dialog({
        modal: true,
        width: 600,
        height: 420,
        closeOnEscape: false,
        close: function( event, ui ) {
            $('#service').val('');
        }
    });
    $('#service-free-checkbox').blur();
};
