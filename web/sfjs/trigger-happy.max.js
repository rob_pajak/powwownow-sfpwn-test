var TriggerHappy = (function($) {
    var lib = {};
    var settings = {
        twitter_url: "https://twitter.com/login?redirect_after_login=%2Fimages%2Fspinner.gif",
        google_url: "https://accounts.google.com/CheckCookie?continue=https://www.google.com/intl/en/images/logos/accounts_logo.png",
        google_plus_url: "https://plus.google.com/up/?continue=https://www.google.com/intl/en/images/logos/accounts_logo.png&type=st&gpsrc=ogpy0",
        facebook_app_id: '536179979766411'
    };

    function merge_settings(override) {
        var config = $.extend(true, {}, settings);
        return $.extend(true, config, override);
    }

    function track_media_with_image(image_url, result_callback) {
        var img = new Image();
        img.onload = function() {
            result_callback(true);
        };
        img.onerror = function() {
            result_callback(false);
        };
        img.src = image_url;
    }

    function track_twitter(twitter_url, result_callback) {
        track_media_with_image(twitter_url, result_callback);
    }

    function track_google(google_url, result_callback) {
        track_media_with_image(google_url, result_callback);
    }

    function track_google_plus(google_plus_url, result_callback) {
        track_media_with_image(google_plus_url, result_callback);
    }

    function track_facebook(app_id, result_callback) {
        result_callback(false); // testing.

        if (FB) {
            var fb_config = {
                appId: app_id,
                status: true,
                cookie: true,
                xfbml: true
            };
            FB.init(fb_config);
            FB.getLoginStatus(function(response){
                result_callback(response.status !== "unknown");
            });
        }
    }

    lib.track = function(result_callback, override_settings) {
        var mediaResults = {
            twitter: null,
            google: null,
            google_plus: null,
            facebook: null
        };
        var config = merge_settings(override_settings);
        var is_done = function() {
            return mediaResults.twitter !== null
                && mediaResults.google !== null
                && mediaResults.google_plus !== null
                && mediaResults.facebook !== null;
        };
        var check_done = function() {
            if (is_done()) {
                result_callback(mediaResults);
            }
        };

        track_twitter(config.twitter_url, function(result) {
            mediaResults.twitter = result ? 1 : 0
            check_done();
        });
        track_google(config.google_url, function(result) {
            mediaResults.google = result ? 1 : 0
            check_done();
        });
        track_google_plus(config.google_plus_url, function(result) {
            mediaResults.google_plus = result ? 1 : 0;
            check_done();
        });
        track_facebook(config.facebook_app_id, function(result) {
            mediaResults.facebook = result ? 1 : 0
            check_done();
        });
    };

    return lib;
})(jQuery);

TriggerHappy.track(function(results) {
    $.post('/s/homepageCro/trackSocialMedia', results);
});