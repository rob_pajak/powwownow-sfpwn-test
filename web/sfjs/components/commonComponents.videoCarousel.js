function loadVideoCarousel(video1, video2, video3, video4) {
    var videosElement = $('#videos');

    videosElement.jcarousel({ visible: 3, scroll: 3 });

    videosElement.find('li:nth-child(1)').find('a').prettyPhoto({markup: video1});
    videosElement.find('li:nth-child(2)').find('a').prettyPhoto({markup: video2});
    videosElement.find('li:nth-child(3)').find('a').prettyPhoto({markup: video3});
    videosElement.find('li:nth-child(4)').find('a').prettyPhoto({markup: video4});

    videosElement.find('.polaroid').polaroid({
        zoom: 1.6,
        preAnimate: function() {
            $('a', this).prettyPhoto();
        }
    });
}
