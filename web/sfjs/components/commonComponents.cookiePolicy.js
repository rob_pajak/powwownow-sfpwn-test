(function($) {
    $('#cookie-policy').on('click','.close', function(event){
        event.preventDefault();
        try {
            $.cookie("cookie_policy_accepted", true, { expires: 1500, path:'/' });
        } catch (e) {
            console.warn(e);
        } finally {
            $('#cookie-policy').addClass('hidden hide');
        }
    });
})(window.jQuery);
