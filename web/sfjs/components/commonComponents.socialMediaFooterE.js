/**
 * Initialise the Social Media Tool tip
 * @param socialMediaToolTips
 */
function socialMediaFooterInitialise(socialMediaToolTips) {
    var toolTipClassName,
        baseId;

    $.each(socialMediaToolTips, function(index, value) {
        toolTipClassName = '.' + value;

        $(toolTipClassName + '-link').click(function() {
            baseId = $(this).attr('class').replace('-link', '');
            socialMediaFooterShowTooltip(baseId, socialMediaToolTips);
            return false;
        });

        $(toolTipClassName + '-close').click(function() {
            baseId = $(this).attr('class').replace('-close floatright sprite tooltip-close', '');
            $('.' + baseId).hide('fast');
            return false;
        });
    });
}

/**
 * Show or Hide the Tool Tips
 * @param toolTipId
 * @param socialMediaToolTips
 */
function socialMediaFooterShowTooltip(toolTipId, socialMediaToolTips) {
    var toolTipElement = $('.' + toolTipId),
        toolTipClassName;

    if (toolTipElement.is(':hidden')) {
        $.each(socialMediaToolTips, function(index, value) {
            toolTipClassName = $('.' + value);
            if (toolTipClassName.is(':visible')) {
                toolTipClassName.hide('fast');
            }
        });
    }
    toolTipElement.toggle('fast');
}

(function($) {
    'use strict';
    $(function() {
        $.getJSON('/ajax/social-media-footer.php', function(data) {
            $.each(data, function(key, listItems) {
                $('#socialMediaFooter_' + key).html(listItems);
            });
        });
    });
})(jQuery);