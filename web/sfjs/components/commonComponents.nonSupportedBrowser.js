(function($) {
    $('#non-supported-browser-container .close').on('click', function (event) {
        event.preventDefault();
        try {
            $.cookie("non_supported_browser_closed", true, { expires: 1500, path: '/' });
        } catch (e) {
            console.warn(e);
        } finally {
            $('#non-supported-browser-container').remove();
        }
    });
})(window.jQuery);
