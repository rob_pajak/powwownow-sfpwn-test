/**
 * Initialises the right menu tooltips and similar tooltips (such as those on the homepage)
 *
 * To create a new tooltip:
 *
 * 1. Add the id of the actual tooltip div to the toolTips array.
 * 2. The link to open the tooltip must be the same as the tooltip id but
 *    with -link at the end.
 * 3. The close button (the cross on the actual tooltip) must be the name of
 *    the tooltip followed by -close
 * 4. Optionally, if the tooltip needs to be reset when it is re-opened (such as to
 *    hide a form submit) then add to the switch statement in
 *    rightLandingPageMenuResetContent();
 *
 *
 * If you just need to open a right menu tooltip, use the
 * rightLandingPageMenuInitialise(); function
 *
 */



var toolTips = [
    'tooltip-international-dial-in-numbers',
    'tooltip-in-conference-controls',
    'tooltip-pin-reminder',
    'tooltip-schedule-call',
    'tooltip-mobile-app',
    'home-outlook-plugin',
    'top-outlook-plugin',
    'bottom-outlook-link',
    'tooltip-schedule-outlook'
];

/**
 * Sets up the right menu tooltip (also know as 'informational links'), does the following:
 *  * Does some crazy hack to solve z index problems in IE
 *  * Sets up the tooltips
 *  * Checks if a specified url needs to open up a specific tab
 */
function rightLandingPageMenuInitialise() {
    var rightIndexZIndex = 1000,
        baseId,
        trackingEvent,
        pageString = location.pathname,
        headElement = $('head'),
        headElementHtml = '<link href="' + HOMEPAGE_CANONICAL_URL + '" rel="canonical" />';

    // Fixes a z-index issues in IE7 an IE8.
//    if ((jQuery.browser.version == '6.0') || (jQuery.browser.version == '7.0')) {
//        $('#right-menu-container li').each(function (index) {
//            $(this).css('z-index', rightIndexZIndex);
//            rightIndexZIndex = rightIndexZIndex - 10;
//        });
//    }

    // Loop round each tooltip in our tooltip array to apply the onclick event
    // to the tooltip link and tooltip close buttons
    $.each(toolTips, function(index, value) {
        $('#' + value + '-link').on('click', function() {
            baseId = $(this).attr('id').replace('-link', '');
            trackingEvent = $(this).data('tracking');

            if ($('div#' + baseId).is(':hidden')) {
                dataLayer.push({'event': trackingEvent});
            }

            rightLandingPageMenuShowTooltip(baseId);
            return false;
        });

        // Attach the click handler to the tooltip close arrow
        $('#' + value + '-close').on('click', function () {
            baseId = $(this).attr('id').replace('-close', '');
            $('#' + baseId).hide('fast');
            return false;
        });
    });

    /**
     * Certain urls need to open certain tabs or tooltips on page load, this goes all that
     */

    pageString = pageString.replace(/\/$/, '');

    // Perform redirect
    switch (pageString) {
        case URL_IN_CONFERENCE_CONTROLS:
            rightLandingPageMenuShowTooltip('tooltip-in-conference-controls');
            headElement.append(headElementHtml);
            break;
        case URL_INTERNATIONAL_DIAL_IN_NUMBERS:
            rightLandingPageMenuShowTooltip('tooltip-international-dial-in-numbers');
            headElement.append(headElementHtml);
            break;
        case URL_MOBILE_APP:
            rightLandingPageMenuShowTooltip('tooltip-mobile-app');
            headElement.append(headElementHtml);
            break;
    }
}

/**
 * Opens the specified tooltip, and closes the other tooltips on the right nav
 *
 * This is called by rightLandingPageMenuInitialise(); but can also be called manually
 * by passing the id of the tooltip to open:
 * e.g. rightLandingPageMenuShowTooltip(tooltip-international-dial-in-numbers);
 *
 */
function rightLandingPageMenuShowTooltip(toolTipId) {
    var toolTipElement = $('#' + toolTipId),
        toolTipSelected;

    $.each(toolTips, function(index, value) {
        toolTipSelected = $('#' + value);

        if (toolTipId !== value && toolTipSelected.is(':visible')) {
            toolTipSelected.hide('fast');
        }
    });

    toolTipElement.toggle('fast');
}

/*global Modernizr MobileDetect $*/
(function (window, Modernizr) {
    'use strict';
    var md = new MobileDetect(navigator.userAgent),
        grade = md.mobileGrade();
    Modernizr.addTest({
        mobile: !!md.mobile(),
        phone: !!md.phone(),
        tablet: !!md.tablet(),
        mobilegradea: grade === 'A'
    });
    window.mobileDetect = md;
})(window, Modernizr);

$(function() {
    if ($('#right-menu-container').length) {
        $('html.mobile .tap-to-dial').each(function() {
            $(this).html('<a href="tel:'+$(this).text().replace(' ', '')+'">'+ $(this).text() +'</a>');
        });
    }
});