/*global window, jQuery, $, ko, RegistrationInterface*/

/**
 * @deprecated
 */
function mainNavigation() {
    'use strict';
    console.warn('mainNavigation deprecated');
}

/**
 * @deprecated
 */
function regionSelectorTooltip () {
    'use strict';
    console.warn('regionSelectorTooltip deprecated');
}

if (typeof window.ko === 'undefined') {
    window.ko = [];
}

$(function() {
    'use strict';
    try {
        $('input').placeholder();
    } catch (e) {
        console.log(e);
    }

    $('#top-menu1 > li, #top-menu2 > li').mouseenter(function () {
        var $element = $('div', this);
        try {
            if ($element.length > 0) {
                $element.velocity('fadeIn', {duration: 200});
            }
        } catch (e) {
            console.log('Velocity not loaded falling back to Jquery');
            $element.show();
        }
    }).mouseleave(function () {
        var $element = $('div', this);
        try {
            if ($element.length > 0) {
                $element.velocity('fadeOut', {duration: 200});
            }
        } catch (e) {
            $(this).children('div').hide();
        }
    });
});

;(function (window, $, ko) {
    if (typeof window._ === 'function' && typeof window.ko === 'object') {

        $('#cx_sign_up_button').on('click', function(event) {
            event.preventDefault();

            var element = $('#cx_registration'),
                elementState = element.data('registration-element-state'),
                self = this;

            if (elementState === 'closed') {
                try {
                    element.velocity('slideDown', {
                        duration: 400,
                        complete: function () {
                            if ($('.ie8').length) {
                                $(self).css('visibility', 'hidden');
                            } else {
                                $(self).velocity({
                                    opacity: 0,
                                    complete: function () {
                                        $(self).css('visibility', 'hidden');
                                    }
                                });
                            }
                            element.data('registration-element-state', 'open');
                        }
                    });
                } catch (e) {
                    console.log('Velocity not loaded falling back to Jquery');
                    element.slideToggle();
                }
            }
        });

        $('#close_cx_registration').on('click', function() {
            var element = $('#cx_registration'),
                elementState = element.data('registration-element-state');

            if (elementState === 'open') {
                try {
                    element.velocity('slideUp', {
                        duration: 400,
                        complete: function () {
                            if ($('.ie8').length) {
                                $('#cx_sign_up_button').css('visibility', 'visible');
                            } else {
                                $('#cx_sign_up_button').velocity({
                                    opacity: 1,
                                    complete: function() {
                                        $('#cx_sign_up_button').css('visibility', 'visible');
                                    }
                                });
                            }
                            element.data('registration-element-state', 'closed');
                        }
                    });
                } catch (e) {
                    console.log('Velocity not loaded falling back to Jquery');
                    element.slideToggle();
                }
            }
        });
    } else {
        $('#cx_sign_up_button').css('visibility', 'hidden');
    }

    // Region Selector Event Handler
    $("ul.widget_region_selector li").on('click', function(){
        var elementState = $(this).data('state'),
            element = $('ul:first', this),
            animation = 'transition.slideDownBigIn',
            state = 'closed';

        if (elementState === 'closed') {
            animation = 'transition.slideDownBigIn';
            state = 'open';
        } else if (elementState === 'open') {
            animation = 'transition.slideUpBigOut';
            state = 'closed';
        }
        try {
            element.velocity(animation, {
                duration: 400,
                complete: function () {
                    $(this).parent().data('state', state);
                }
            });
        } catch (e) {
            console.log('Velocity not loaded falling back to Jquery');
            element.slideToggle();
        }
    });

    $('body').on('click', function() {
        var element = $('ul.widget_region_selector li'),
            state = element.data('state');
        if (state === 'open') {
            try {
                $('ul.widget_region_selector li ul').velocity('transition.slideUpBigOut',
                    {
                        duration: 400,
                        complete: function () {
                            $(this).parent().data('state', 'closed');
                        }
                    });
            } catch (e) {
                console.log('Velocity not loaded');
            }
        }
    });

    if (typeof window.RegistrationInterface === 'function' && document.getElementById('ko.header.registration.container')) {
        var trackingEvents = {
                'FormSubmitEvent' : 'Generate Pin - Homepage-Dropdown/onClick',
                'registrationSuccessEvent': '/Homepage-E-Dropdown/RegistrationSuccess'
            },
            registrationSource = 'GBR-CX-Header-Registration';
        var HeaderRegistration = new RegistrationInterface(trackingEvents, registrationSource);
        ko.applyBindings(HeaderRegistration, document.getElementById('ko.header.registration.container'));
    }
})(window, jQuery, ko);