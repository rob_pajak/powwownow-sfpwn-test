/**
 * SetUp Video Fallbacks
 * @param autoPlay
 * @param autoBuffering
 * @param url
 * @param playerId
 * @param errorMessage
 */
function setUpVideo(autoPlay, autoBuffering, url, playerId, errorMessage) {
    if (Modernizr.video) {
        console.log('Has HTML5 Video');
    } else if (swfobject.hasFlashPlayerVersion("1")) {
        console.log('Has No HTML5 Video, but has Flash, Show FlowPlayer');
        $f(playerId, "/shared/flowplayer/flowplayer-3.2.7.swf", {
            clip:{
                autoPlay: autoPlay,
                autoBuffering: autoBuffering,
                url: url
            },
            canvas: {
                backgroundColor: '#ffffff',
                backgroundGradient: 'none'
            }
        });
    } else {
        console.log('Has No HTML5 Video and No Flash, Show Error Message');
        $('#' + playerId).html(errorMessage);
    }
}
