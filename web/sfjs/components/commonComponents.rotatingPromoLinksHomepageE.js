(function($) {
    'use strict';
    $(function() {
        promotionalFadeInitialise();
    });
    $('#rotating-promotional-list').find('.item-2').on('click', function(){
        window.location.href = "/s/Powwownow-Engage-Service";
    });
    $('#rotating-promotional-list').find('.item-3').on('click', function(){
        window.location.href = "/blog/opinions/powwownow-included-prestigious-tech-track-100/";
    });
    $('#rotating-promotional-list').find('.item-4').on('click', function(){
        window.location.href = "/Conference-Call/How-Conference-Calling-Works";
    });
    $('#rotating-promotional-list').find('.item-5').on('click', function(){
        window.location.href = "/Conference-Call/Costs";
    });
    $('#rotating-promotional-list').find('.item-6').on('click', function(){
        window.location.href = "/Conference-Call/International-Number-Rates";
    });
    $('#rotating-promotional-list').find('.item-7-iphone').on('click', function(){
        window.location.href = "http://app.powwownow.com";
    });
    $('#rotating-promotional-list').find('.item-7-android').on('click', function(){
        window.location.href = "https://market.android.com/details?id=com.grppl.android.shell.CMBpwn&amp;feature=search_result";
    });
})(jQuery);