var pst = (function() {
    var pst = {};
    pst.standardCost = 0;
    pst.userSelectedBundleId = 0;
    pst.authenticated = 0;
    pst.bundlePackages = {};

    // --- Private methods below.

    function getDialogPosition()
    {
        var $sliders = $('#sliders');

        var $headerSource;
        var $introductionHeader = $('#slider-introduction');
        var $resultHeader = $('#slider-result .slider-bundle-header > p');
        if ($resultHeader.is(':visible')) {
            $headerSource = $resultHeader;
        } else if ($introductionHeader.is(':visible')) {
            $headerSource = $introductionHeader;
        } else {
            $headerSource = $('#slider-no_bundle .slider-bundle-header > p')
        }

        var offset = $headerSource.offset();
        return [ offset.left, $sliders.offset().top - $(window).scrollTop() ];
    }

    function getDialogWidth()
    {
        return $('#slider-results').width();
    }

    function calculateBundleSavings(totalMinutes, bundleCost, bundleMinutes, rate)
    {
        var totalCost = totalMinutes * 0.043;
        if (bundleMinutes >= totalMinutes) {
            return totalCost - bundleCost;
        } else {
            // [TotalCost] - [BundleCost] - ([TotalMins] - [BundleMinutes]) * [standardCost]
            // OR [TotalCost] - [BundleCost] - ([TotalMins] - [BundleMinutes]) * [BundleRate]
            return totalCost - bundleCost - (totalMinutes - bundleMinutes) * rate;
        }
    }

    function showBundleWithSavings(id, savings)
    {
        $(id).show()
            .find('.money_savings').text(savings.toFixed(2));
    }

    function initRangeSlider($slider, initVal, min, max, step)
    {
        // Create Slider
        $slider.slider({
            value: initVal,
            min: min,
            max: max,
            step: step,
            slide: function (event,ui) {
                $slider.find('a').text(ui.value);
            }
        });

        // Create the Initial Slider Value
        $slider.find('a').text(initVal);
    }

    function initSliderWithLabelExceptions($slider, initVal, min, max, step, labelExceptions)
    {
        // Fill up the labelExceptions so it becomes a label holder instead.
        for (var i = min; i <= max; i += step) {
            if (!labelExceptions.hasOwnProperty(i)) {
                labelExceptions[i] = i;
            }
        }
        var labels = labelExceptions;

        // Create Slider
        $slider.slider({
            value: initVal,
            min: min,
            max: max,
            step: step,
            slide: function (event,ui) {
                $slider.find('a').text(labels[ui.value]);
            }
        });

        // Create the Initial Slider Value
        $slider.find('a').text(initVal);
    }

    function retrieveValueFromSlider(text)
    {
        switch (text) {
            case '10+':
                return 12;
            case'60+':
                return 90;
            default:
                return parseInt(text);
        }
    }

    function tryToAddBundleFromSessionToBasket()
    {
        $.post('/s/Basket', {}, 'json')
            .done(function() {
                $(location).attr('href', '/s/Basket');
            })
            .fail(function(response) {
                var responseMessage = $.parseJSON(response.responseText);
                if (responseMessage.alert && responseMessage.error) {
                    alert(responseMessage.error);
                }

                if(responseMessage.modal == 'bundleInTheBasket') {
                   $('#bundleInTheBasket-modal').dialog( "open" );
                }
                if(responseMessage.modal == 'accountHasBundle') {
                    $('#accountHasBundle-modal').dialog( "open" );
                }
            });
    }

    function showResultsHeaderDependingOnNumberOfBundles()
    {
        var bundlesVisible = $('.bundle_teaser:visible').length;
        if (bundlesVisible) {
            var $multipleBundlesHeader = $('#multiple-bundle-results-header');
            var $singleBundleHeader = $('#single-bundle-result-header');
            if (bundlesVisible === 1) {
                $singleBundleHeader.show();
                $multipleBundlesHeader.hide();
            } else {
                $singleBundleHeader.hide();
                $multipleBundlesHeader.show();
            }
        }
    }

    function addLastBundleClass()
    {
        $('.last-bundle').removeClass('last-bundle');
        $('.bundle_teaser:visible').last().addClass('last-bundle');
    }

    function numberWithCommas(x)
    {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    // --- Public module methods below.

    // Initial Slider Configuration
    pst.initSlider = (function (pageName,auth) {
        // Conference Slider
        initRangeSlider($('#conferenceSlider'), 1, 1, 20, 1);

        // People Slider
        initSliderWithLabelExceptions($('#peopleSlider'), 2, 2, 11, 1, { 11: "10+" });

        // Minutes Slider
        initSliderWithLabelExceptions($('#minuteSlider'), 5, 5, 65, 5, { 65: "60+" });

        // Calculate Button
        $('#btn-calculate').click(function() {
            pst.run_product_selector(pageName);

            // Dashboard Specific
            if ('dashboard' === pageName || 'dashboard_enhanced' === pageName) {
                var $sliders = $('#sliders'),
                    $slidersResult = $('#slider-results').show(),
                    $body = $('body'),
                    animationSpeed = 1500,
                    start = -$slidersResult.outerWidth();

                showResultsHeaderDependingOnNumberOfBundles();
                addLastBundleClass();

                if (!$sliders.data('moving-left') && !$sliders.data('moved-left')) {
                    $('.home-grid').css('opacity', 0);
                    // In the calculation below assumes a padding-left of 20 px from the slider-results.
                    // That's why it's set to 660.
                    var endRight = 660 - $slidersResult.width();
                    endRight += 'px';

                    // Animate the sliders to the left.
                    $sliders
                        .data('moving-left', true)
                        .animate({right: '640px'}, animationSpeed, function () {
                            $('.home-grid')
                                .hide()
                                .css('opacity','');
                            $sliders
                                .css('right', '')
                                .data('moved-left', true)
                                .data('moving-left', false);
                        });

                    // Simultaneously move the slider results.
                    $slidersResult
                        .css({position: 'absolute', right: start + 'px'})
                        .animate({right: endRight}, animationSpeed, function () {
                            $slidersResult.css({position: '', right: ''});
                            // Overflow the Container to Remove Bottom Scroll Bar
                            $body.css('overflow-x','visible');
                        });

                    // Hide the Overflow
                    $body.css('overflow-x','hidden');
                }
            }
        });

        // Buy Now Button
        $('.btn-buy_now').click(function() {
            var bundleID = $(this).data('id');

            $.post('/Bundles-Selector-Tool/update', {bundle_id: bundleID, id: pst.userSelectedBundleId})
                .always(function() {
                    dataLayer.push({'event': 'ProductSelectorTool/BuyNow'});

                    if (pageName == 'dashboard') {
                        tryToAddBundleFromSessionToBasket();
                    } else if (pageName == 'landingPage') {
                        if (pst.authenticated) {
                            tryToAddBundleFromSessionToBasket();
                        } else {
                            // Not Logged In.
                            $(location).attr('href','/s/Basket');
                        }
                    } else if (pageName == 'callMinuteBundles') {
                        $('#dialog-plus-link').click();
                    } else if (pageName == 'dashboard_enhanced') {
                        $('#dialog-plus-link').click();
                    }
                });
        });

        // Close Button used in the
        $('.close-control').click(function(){
            if (('dashboard' === pageName || 'dashboard_enhanced' === pageName) && $('.home-grid').not(':visible')) {
                // Should move (animate) the tool to the right and show the grid again

                // Hide
                $('#slider-result').hide();
                $('#slider-no_bundle').hide();

                // Move (Animate) right
                $('#sliders').data('moving-left', true).animate({
                    right: '-640px'
                }, 2000, function() {
                    $('.home-grid').show();
                    $('#sliders').css('right','')
                        .data('moved-left', false)
                        .data('moving-left', false);
                });

            } else if ('landingPage' === pageName) {
                $('#slider-result').hide();
                $('#slider-no_bundle').hide();
                $('#slider-introduction').show();
            } else if ('callMinuteBundles' === pageName) {
                $('#slider-result').hide();
                $('#slider-no_bundle').hide();
                $('#slider-introduction').show();
            }
        });

        // Dashboard Specific
        if ('dashboard' === pageName || 'dashboard_enhanced' === pageName) {
            $('#slider-results').hide();
            $('#sliders').css('margin-bottom','17px');
        }

        // Landing Page Specific
        if ('landingPage' === pageName || 'callMinuteBundles' === pageName) {
            $('.slider-introduction-content').outerHeight($('#sliders').outerHeight());
            $('.slider-introduction-content-view').css('margin-top','100px');
        }

        // Is the User Authenticated
        pst.authenticated = auth;

        // Store the Bundle Packages into an Object
        pst.storeBundleInformation();
    });

    // Run the Product Selector Process
    pst.run_product_selector = (function (pageName) {
        // Hide all Slider Divs
        $('#slider-introduction').hide();
        var $noBundles = $('#slider-no_bundle').hide();
        var $bundleResult = $('#slider-result').hide();

        // Setup which Bundles to be Shown - Return if there are going to be Results to be shown true | false
        if (pst.calculate()) {
            $bundleResult.show({
                // Dirty hack to make sure the last visible teaser has the class.
                duration: 0,
                complete: function() {
                    addLastBundleClass();
                    showResultsHeaderDependingOnNumberOfBundles();
                }
            });
        } else {
            $noBundles.show();
            var $outerHeight = $('#sliders').outerHeight()-$noBundles.outerHeight();

            // Dashboard Specific
            if ('dashboard' === pageName || 'dashboard_enhanced' === pageName) {
                if ($('.slider-bundle-header').outerHeight() == 0) {
                    $outerHeight -= 66;
                }
            }

            $('.slider-no-bundle-content').outerHeight($outerHeight);
        }

        // Store the Calculation
        pst.store_calculation();
    });

    // Calculate Bundles
    pst.calculate = (function () {
        // Store Calculation Figures
        var conference    = retrieveValueFromSlider($('#conferenceSlider').text()),
            people        = retrieveValueFromSlider($('#peopleSlider').text()),
            minute        = retrieveValueFromSlider($('#minuteSlider').text()),
            standard_cost = pst.getStandardCost(),
            calculatedBundles  = pst.bundlePackages,
            minutes_diff,
            rate;

        // Minutes used right now
        var minutes_total = conference * people * minute;
        // Total cost of the conferences right now
        var cost_total    = minutes_total * standard_cost / 100;

        // Display Calculated Figures
        $('.standard_cost').text(standard_cost);
        $('.minutes').text(numberWithCommas(minutes_total));
        $('.cost').text(cost_total.toFixed(2));

        // Check Total Minutes.
        if (minutes_total < 580) {
            return false;
        }

        for (var bundleKey in calculatedBundles) {
            if (!calculatedBundles.hasOwnProperty(bundleKey)) {
                continue;
            }

            var bundle = calculatedBundles[bundleKey];

            // Hide All .bundle_teaser Bundles
            $('#' + bundle.id).hide();

            // Savings and Time Calculations
            minutes_diff = bundle.min - minutes_total;

            if (bundle.id == '5000') {
                rate = bundle['rate'];
            } else {
                rate = standard_cost / 100;
            }
            bundle.savings = calculateBundleSavings(minutes_total, bundle.cost, bundle.min, rate);
            bundle.time = Math.floor(minutes_diff / 60);

            // Check if the Current Bundle has the required minutes to accommodate the Users Minutes
            if (minutes_diff < 0) {
                bundle['time'] = 0;
            }

            // Set the Time Gained Field
            if (bundle['time'] > 0) {
                $('#'+bundle['id']+' .time_gained').text(bundle['time']);
                $('#'+bundle['id']+' .more_minutes').show();
            } else {
                $('#'+bundle['id']+' .more_minutes').hide();
            }
        }

        // Bundles Logic - AYCM
        if (calculatedBundles.hasOwnProperty("5000") && calculatedBundles.hasOwnProperty("AYCM") && calculatedBundles['5000']['savings'] <= 72.5 && calculatedBundles.AYCM.savings > 0) {
            showBundleWithSavings('#AYCM', calculatedBundles.AYCM.savings);
            pst.showBundles(calculatedBundles, 1);
            return true;
        }

        // Bundles Logic - Remaining Bundles - Show Two Bundles
        return pst.showBundles(calculatedBundles, 2);
    });

    // Show the Bundles for the User
    // @todo Refactor this method to use less codes, and less repetitive lines
    pst.showBundles = (function (bundles_calc,bundles_to_show) {
        var bundles_shown = false;
        var bundle1000Savings = bundles_calc['1000'].savings;
        var bundle2500Savings = bundles_calc['2500'].savings;
        var bundle5000Savings = bundles_calc['5000'].savings;

        if (bundle1000Savings > 0 && bundle1000Savings > bundle2500Savings && bundle1000Savings > bundle5000Savings) {
            showBundleWithSavings('#1000', bundle1000Savings);
            bundles_shown = true;
        }

        if (bundle2500Savings > 0 && bundle2500Savings > bundle1000Savings && bundle2500Savings > bundle5000Savings) {
            showBundleWithSavings('#2500', bundle2500Savings);
            bundles_shown = true;
        }

        if (bundle5000Savings > 0 && bundle5000Savings > bundle1000Savings && bundle5000Savings > bundle2500Savings) {
            showBundleWithSavings('#5000', bundle5000Savings);
            bundles_shown = true;
        }

        if (bundles_to_show == 2) {
            if (bundle2500Savings > 0 && bundle1000Savings > bundle2500Savings && bundle1000Savings > bundle5000Savings && bundle2500Savings > bundle5000Savings) {
                showBundleWithSavings('#2500', bundle2500Savings);
                bundles_shown = true;
            }

            if (bundle5000Savings > 0 && bundle1000Savings > bundle2500Savings && bundle1000Savings > bundle5000Savings && bundle5000Savings > bundle2500Savings) {
                showBundleWithSavings('#5000', bundle5000Savings);
                bundles_shown = true;
            }

            if (bundle1000Savings > 0 && bundle2500Savings > bundle1000Savings && bundle2500Savings > bundle5000Savings && bundle1000Savings > bundle5000Savings) {
                showBundleWithSavings('#1000', bundle1000Savings);
                bundles_shown = true;
            }

            if (bundle5000Savings > 0 && bundle2500Savings > bundle1000Savings && bundle2500Savings > bundle5000Savings && bundle5000Savings > bundle1000Savings) {
                showBundleWithSavings('#5000', bundle5000Savings);
                bundles_shown = true;
            }

            if (bundle1000Savings > 0 && bundle5000Savings > bundle1000Savings && bundle5000Savings > bundle2500Savings && bundle1000Savings > bundle2500Savings) {
                showBundleWithSavings('#1000', bundle1000Savings);
                bundles_shown = true;
            }

            if (bundle2500Savings > 0 && bundle5000Savings > bundle1000Savings && bundle5000Savings > bundle2500Savings && bundle2500Savings > bundle1000Savings) {
                showBundleWithSavings('#2500', bundle2500Savings);
                bundles_shown = true;
            }
        }

        return bundles_shown;
    });

    // Store Calculation
    pst.store_calculation = (function () {
        $.ajax({
            url: '/Bundles-Selector-Tool/store',
            data: {
                'people': $('#peopleSlider').text(),
                'conferences': $('#conferenceSlider').text(),
                'minutes': $('#minuteSlider').text()
            }}).done(function(data) {
                data = $.parseJSON(data);
                pst.userSelectedBundleId = data.id;
                dataLayer.push({'event': 'ProductSelectorTool/Calculate'});
            });
    });

    // Get Standard Cost
    pst.getStandardCost = (function () {
        return pst.standardCost;
    });

    // Set Standard Cost
    pst.setStandardCost = (function (standard_cost) {
        pst.standardCost = standard_cost;
    });

    // Initialise Modal Window
    pst.initModalWindow = (function (windowId,linkTo) {
        var modalId = '#'+windowId+'-modal';
        var closeId = '.'+windowId+'-close-btn';

        // Create Dialog
        $(modalId).dialog({
            autoOpen: false,
            minHeight: 380,
            width: 700,
            modal: true,
            resizable: false
        });

        // Connect Dialog to Class
        $('.'+linkTo).click(function() {
            $(modalId).dialog("open")
                .dialog('option', 'position', getDialogPosition())
                .dialog('option', 'width', getDialogWidth());
            return false;
        });

        // Close Button
        $(closeId).click(function() {
            $(modalId).dialog("close");
        });
    });

    // Store Bundle Information
    pst.storeBundleInformation = (function () {
        var bundle_id;

        // Bundle Information is stored on Divs using bundle_teaser
        $(".bundle_teaser").each(function () {
            var $teaser = $(this);
            bundle_id = $teaser.attr('id');
            pst.bundlePackages[bundle_id] = {
                'id'   : bundle_id,
                'cost' : $teaser.data('cost'),
                'min'  : $teaser.data('minutes'),
                'rate' : $teaser.data('rate'),
                'type' : $teaser.data('type')
            };
        });
    });

    pst.initDynamicTAC = (function () {
        $('.assumptionsOpener').click( function() {
            if($('#slider-result').is(':visible')) {
                $('#assumptions-modal > ul > li').each( function() {
                    var tac = $(this);
                    tac.show();
                    if ($('#bundle-suggestions div#' +  tac.attr('class')).is(':hidden')) {
                        tac.hide();
                    }
                });
            }
        });
    });

    pst.initShakeSliders = (function() {
        if (location.href.indexOf('#shake') != -1) {
            $(document).ready(function() {
                setTimeout(function() { $('#sliders').effect('shake', 50); }, 500);
            });
        }
    });

    return pst;
})();
