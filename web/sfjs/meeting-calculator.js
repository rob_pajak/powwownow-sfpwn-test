$(function() {
  /* Custom checkbox */
  $('.meeting-calculator-form ul').find('label').click(function(){
      if( $(this).find('input').is(':checked') ) {
          $(this).removeClass('active');
          $(this).find('input').removeAttr('checked');
          getCostOfMeeting();
      } else {
          $(this).addClass('active');
          $(this).find('input').attr('checked', true);
          getCostOfMeeting();
      }
  });

  // Calculate savings
  $('.calculate-savings-btn').click(
     function (event) {
      event.preventDefault();
      dataLayer.push({'event': 'Calculate Savings - Meeting-Cost-Calculator/onClick'});

      if(Number($(".annual-wage-input").val()) > 0) {
        $(".meeting-calculator .error").css("display", "none");
        $(".total-saved").text(Math.round(getMeetingSaving()));
        $(".meeting-calculator-first-layout").fadeOut(400, function () {
          $(".meeting-calculator-second-layout").css("position", "relative");
        });
      }
       else {
          $(".meeting-calculator .error").css("display", "block");
       }
       
  });

  /* Generate Pin Button */ 
  $('.generate-pin-btn').click(function (e) {
        e.preventDefault();
        dataLayer.push({'event': 'Generate Pin - Meeting-Cost-Calculator/onClick'});
        window.setTimeout(function (){ window.location = $('.generate-pin-btn').attr('href') }, 1000);
  });

  // Error trigger
  $(".annual-wage-input").on(
    "input", function() {
      if(Number($(".annual-wage-input").val()) > 0) {
        $(".meeting-calculator .error").css("display", "none");
      }
      else {
          $(".meeting-calculator .error").css("display", "block");
       }
  }); 

  // Create another meeting (Reset form)
  $('.calculate-another-meeting').click(
   function (event) {
     event.preventDefault();
     reset();
     $(".meeting-calculator-first-layout").fadeIn(400, function () {
        $(".meeting-calculator-second-layout").css("position", "absolute");
     });
  });

  /* Change annual wage value */
  $('.annual-wage-container').on('keydown', '.annual-wage-input', function(event) {
      // Allow: backspace, delete, tab, escape, enter
      if ( $.inArray(event.keyCode,[46,8,9,27,13]) !== -1 ||
           // Allow: Ctrl+A
          (event.keyCode == 65 && event.ctrlKey === true) ||
           // Allow: home, end, left, right
          (event.keyCode >= 35 && event.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      else {
          // Ensure that it is a number and stop the keypress
          if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
              event.preventDefault();
          } else {
            // Max length - 6 numbers
            if($(this).val().length >= 6) {
              event.preventDefault();
            }
          }
      }
  });
  $('.annual-wage-container').on('keyup', '.annual-wage-input', getCostOfMeeting);

  /* Add / Remove colleague */
  $('.annual-wage-container').on('click', '.add-colleague', function() {
    addColleague($(this).parent());
    return false;
  });
  $('.annual-wage-container').on('click', '.remove-colleague', function() {
    removeColleague($(this).parent());
    return false;
  });


/* Sliders */
  $("#hourMeetingSlider").slider({
    orientation: 'horizontal',
    range: "min",
    min:0,
    max: 8,
    value: 0,
    step : 0.5,
    slide: refreshMeetingHours,
    change: refreshMeetingHours
  });
  $("#hourTravelSlider").slider({
    orientation: 'horizontal',
    range: "min",
    min:0,
    max: 480,
    value: 0,
    step : 10,
    slide: refreshTravelHours,
    change: refreshTravelHours
  });

  $("#costTravelSlider").slider({
    orientation: 'horizontal',
    range: "min",
    min:0,
    max: 1000,
    value: 0,
    step : 5,
    slide: refreshTravelCost,
    change: refreshTravelCost
  });
  $("#cateringCostSlider").slider({
    orientation: 'horizontal',
    range: "min",
    min:0,
    max: 1000,
    value: 0,
    step : 5,
    slide: refreshCateringCost,
    change: refreshCateringCost
  });


  $("#hourMeetingSlider").slider("value", 0);
  $("#hourTravelSlider").slider("value", 0);
  $("#costTravelSlider").slider("value", 0);
  $("#cateringCostSlider").slider("value", 0);
});

function is_touch_device() {
  try {
    document.createEvent("TouchEvent");
    return true;
  } catch ( e ) {
    return false;
  }
}

function refreshMeetingHours() {
  var meetingHours = $("#hourMeetingSlider").slider("value");
  $('.meeting-hours').text(meetingHours);
  switch (meetingHours) {
    case 1:
      $('.meeting-hour-plural').text('');
      break;
    default:
      $('.meeting-hour-plural').text('s');
      break;
  }
  getCostOfMeeting();
}
function refreshTravelHours() {
  var travelHours = $("#hourTravelSlider").slider("value");
  $('.travel-hours').text(travelHours);
//  switch (travelHours) {
//    case 1:
//      $('.travel-hour-plural').text('');
//      break;
//    default:
//      $('.travel-hour-plural').text('s');
//      break;
//  }
  getCostOfMeeting();
}
function refreshTravelCost() {
  $('.travel-cost').text($("#costTravelSlider").slider("value"));
  getCostOfMeeting();
}
function refreshCateringCost() {
  $('.catering-cost').text($("#cateringCostSlider").slider("value"));
  getCostOfMeeting();
}



/* Get meeting cost */
function getCostOfMeeting() {
  var costOfMeeting = 0;

  // Salary per hour
  var salaryPerHour = getSalaryPerHour();

  // Hour travel
  var hourTravel = $('.travel-hours').text();
  if(hourTravel == '') {
    hourTravel = 0;
  } else {
    hourTravel = parseFloat(hourTravel);
  }

  // Hour meeting
  var hourMeeting = $('.meeting-hours').text();
  if(hourMeeting == '') {
    hourMeeting = 0;
  } else {
    hourMeeting = parseFloat(hourMeeting);
  }

  // Travel cost
  var travelCost = $('.travel-cost').text();
  if(travelCost == '') {
    travelCost = 0;
  } else {
    travelCost = parseInt(travelCost);
  }

  // Catering cost
  var cateringCost = $('.catering-cost').text();
  if(cateringCost == '') {
    cateringCost = 0;
  } else {
    cateringCost = parseInt(cateringCost);
  }

  // Extras
  var extras = 0;
  $('.meeting-calculator-form ul input:checked').each(function() {
    extras += $(this).data('cost');
  });
  costOfMeeting = salaryPerHour * ((hourTravel/60) + hourMeeting) + travelCost + cateringCost + extras;
  //console.log(costOfMeeting);
  $(".progress-bar").css("height", (costOfMeeting/getMax(costOfMeeting)*100) + "%");
  $(".total-cost").text(Math.round(costOfMeeting));
  return costOfMeeting;
}

/* Get MAX value for left progress bar */
function getMax(costOfMeeting) {
  // TODO: get max value for left bar
  if(costOfMeeting < 500) return 500;
  if(costOfMeeting >= 500 && costOfMeeting < 1000)  return 1000;
  if(costOfMeeting >= 1000 && costOfMeeting < 2000)  return 2000;
  if(costOfMeeting >= 2000 && costOfMeeting < 5000)  return 5000;
  if(costOfMeeting >= 5000 && costOfMeeting < 10000)  return 10000;
}

/* Get meeting saving */
function getMeetingSaving() {
  var costOfMeeting = $(".total-cost:first").text();
  if(costOfMeeting == '') {
    costOfMeeting = 0;
  } else {
    costOfMeeting = parseInt(costOfMeeting);
  }
  var hourTravel = $('.travel-hours').text();
  if(hourTravel == '') {
    hourTravel = 0;
  } else {
    hourTravel = parseFloat(hourTravel);
  }
  var hourMeeting = $('.meeting-hours').text();
  if(hourMeeting == '') {
    hourMeeting = 0;
  } else {
    hourMeeting = parseFloat(hourMeeting);
  }

  // Travel cost
  var travelCost = $('.travel-cost').text();
  if(travelCost == '') {
    travelCost = 0;
  } else {
    travelCost = parseInt(travelCost);
  }
  // Catering cost
  var cateringCost = $('.catering-cost').text();
  if(cateringCost == '') {
    cateringCost = 0;
  } else {
    cateringCost = parseInt(cateringCost);
  }
  if(hourTravel == 0 && travelCost == 0 && cateringCost == 0 && $('.meeting-calculator-form ul input:checked').length == 0) {
    return 0;
  } else {
    return costOfMeeting - (((hourTravel/60) + hourMeeting) * 2.58); // 0.043 * 60
  }
}

/* Get Salary per hour */
function getSalaryPerHour() {
  // Annual Salary
  var sumAnnualSalary = 0;
  var colleagues = $('.annual-wage-container input');
  for(var i = 0; i < colleagues.length; i++) {
    var value = colleagues.eq(i).val();
    if(value == '') {
      value = 0;
    }
    sumAnnualSalary += parseInt(value);
  }
  return sumAnnualSalary / 1920; // 48 * 5 * 8
}

/* Reset form */
function reset() {
  $("#hourMeetingSlider").slider("value", 0);
  $("#hourTravelSlider").slider("value", 0);
  $("#costTravelSlider").slider("value", 0);
  $("#cateringCostSlider").slider("value", 0);
  $('.annual-wage-container .remove-colleague').each(function() {
    $(this).parent().remove();
  });
  $('.annual-wage-container input').val('');
  $('.meeting-calculator-form ul').find('label').each(function() {
    if( $(this).find('input').is(':checked') ) {
      $(this).removeClass('active');
      $(this).find('input').removeAttr('checked');
    }
  });
  getCostOfMeeting();
}

/* Add colleague */
function addColleague(element) {
  if(element.find('input').val() == '' || parseInt(element.find('input').val()) == 0) {
    $(".meeting-calculator .error").css("display", "block");
    return;
  }
  var newLine = $(element).clone();
  newLine.find('input').val('');
  newLine.appendTo('.annual-wage-container');
  $(element).find('button').removeClass('add-colleague').addClass('remove-colleague').text('Remove colleague');
}

/* Remove colleague */
function removeColleague(element) {
  element.remove();
  $('.annual-wage-container button:last').text('Add colleague').removeClass('remove-colleague').addClass('add-colleague');
  getCostOfMeeting();
}
