/**
 * Will be used to store the Shared / Unified / Generic Free PIN Basic Registrations
 * @requires utils.js
 * @requires $.cookie
 * @requires functions.js
 * @requires Google Tag Manager
 * @author Asfer Tamimi
 */

// Registration
var registration = {};

/**
 * Basic Registration
 * @param formId
 * @param email
 * @param source
 * @param legacy
 * @param responsive
 */
registration.basic = function (formId, email, source, legacy, responsive) {
    var pageName = $(location).pathname,
        pinContainer = '.pin-container';

    if (responsive == undefined) {
        responsive = false;
    }

    // Set the Default Email Placeholder Text. Only used in NON Response Pages
    if (!responsive) {
        utils.defaultTextField($('#' + formId + ' #' + email), DEFAULT_TEXT_EMAIL_ADDRESS_HOME_PAGE_REGISTER);
    }

    // Legacy Cookie Code, if the PIN Container Exists
    // There is a Session Replacement in the Component. Checks Session before Cookie
    if (legacy && $(pinContainer).length != 0) {
        if ($.cookie('registered_pin') != undefined && $(".pin-container").html() == '******') {
            $(pinContainer).addClass('pin-container-response');
            $(pinContainer).html($.cookie('registered_pin'));
        }
    }

    // Setup the Form
    $('#' + formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        responsive: (responsive) ? 'on' : 'off', // Used to disable the default CSS used in the form
        beforeSubmit: function () {
            return registration.basic.onSubmission(formId, email, source, responsive);
        },
        success: function (responseArr) {

            utils.removeAjaxLoader('ajax_loader_a');
            utils.removeAjaxLoader('ajax_loader_b');

            if (
                (typeof responseArr['html'] == 'undefined') ||
                    (typeof responseArr['html']['homepageHTML'] == 'undefined') ||
                    (typeof responseArr['html']['landingpageHTML'] == 'undefined') ||
                    (typeof responseArr['html']['iycHTML'] == 'undefined') ||
                    (typeof responseArr['success_message'] == 'undefined') ||
                    (typeof responseArr['error_messages'] == 'undefined') ||
                    (typeof responseArr['javascript'] == 'undefined') ||
                    (typeof responseArr['pin'] == 'undefined') ||
                    (typeof responseArr['pinRefHash'] == 'undefined')
                ) {
                var error = [];
                error.push({'message': FORM_COMMUNICATION_ERROR, 'field_name': 'email'});
                return utils.legacyInputErrorBoxes(error, formId, false, false);
            }

            registration.basic.onSuccess(formId, pageName, email, source, legacy, responseArr);
            return true;
        },
        error: function (responseArr) {
            registration.basic.onError(formId, responseArr, source, responsive);
        }
    });
};

/**
 * Basic Registration - Form Submission - Client Validation
 * @param formId
 * @param email
 * @param source
 * @param responsive
 * @returns {boolean}
 */
registration.basic.onSubmission = function (formId, email, source, responsive) {

    // Email Validation
    if (!utils.isValidEmail($('#' + formId + ' #' + email).val())) {
        // Check Source for Pre Submission Error Tasks
        switch (source) {
            case 'HomepageE':
                utils.removeAjaxLoader('ajax_loader_a');
                break;
            case 'GBR-Its-Your-Call':
                // Do Nothing
                break;
            default:
                utils.removeAjaxLoader(null);
                break;
        }

        if (responsive == undefined || !responsive) {
            var error = [];
            error.push({'message': FORM_VALIDATION_INVALID_EMAIL_ADDRESS, 'field_name': 'email'});
            return utils.legacyInputErrorBoxes(error, formId, false, false);
        } else {
            formId = '#' + formId;

            // Check if Parsley JS exists
            if ($().parsley !== undefined) {
                $(formId).parsley({
                    errorClass: 'error',
                    animate: true,
                    animateDuration: 200,
                    errors: {
                        errorsWrapper: '<small>',
                        errorElem: '<p>',
                        classHandler: function (elem, isRadioOrCheckbox) {
                            return $(elem).parent();
                        }
                    }
                });

                var validate = $(formId).parsley('validate');
                if (!validate) return validate;
            }
        }
    }

    // Check Source for Pre Submission Tasks
    switch (source) {
        case 'HomepageE':
            // Load the Ajax Loader
            utils.showAjaxLoader('/shared/assets/img/misc/ajax-loader.gif', $('#register_email-a'), 'ajax_loader_a', 35, 5, true);
            break;
        case 'GBR-Its-Your-Call':
            // Do Nothing
            break;
        default:
            // Load the Ajax Loader
            utils.showAjaxLoader('/sfimages/ajaxLoaders/ajax-loader-h.gif', $('.pin-container'), null, 35, 5, false);
            break;
    }

    return true;
};

/**
 * Basic Registration - Form Submission - Server onError Validation
 * @param formId
 * @param responseArr
 * @param source
 * @param responsive
 */
registration.basic.onError = function (formId, responseArr, source, responsive) {
    var message;

    // Check Source for Post Submission Error Tasks
    switch (source) {
        case 'HomepageE':
            utils.removeAjaxLoader('ajax_loader_a');
            break;
        case 'GBR-Its-Your-Call':
            // Do Nothing
            break;
        default:
            utils.removeAjaxLoader(null);
            break;
    }

    if (responsive == undefined || !responsive) {
        message = utils.obtainMessage(responseArr, 'error');
        utils.outputMessage('#' + formId, message, 'error', false, false, false);
    } else {
        // Check if Parsley JS exists
        if ($().parsley !== undefined) {
            message = utils.obtainMessage(responseArr, 'error');
            utils.outputMessage('#' + formId, message, 'error', false, false, true);
            return true;
        }
    }
};

/**
 * Basic Registration - Form Submission - Server onSuccess Validation
 * @param formId
 * @param pageName
 * @param email
 * @param source
 * @param legacy
 * @param responseArr
 */
registration.basic.onSuccess = function (formId, pageName, email, source, legacy, responseArr) {

    // Success Event Tracking
    if (dataLayer !== undefined) {
        switch (source) {
            case 'HomepageE':
                break;
            case 'Its-Your-Call':
                dataLayer.push({
                    'event': '/Its-Your-Call/RegistrationSuccess',
                    'PIN': responseArr['pinRefHash']
                });
                break;
            default:
                dataLayer.push({
                    'event': '/' + source.replace('GBR-', '') + '/RegistrationSuccess',
                    'gcp': window.google_tag_params,
                    'PIN': responseArr['pinRefHash']
                });
        }
    }

    // Success Differences
    switch (source) {
        case 'HomepageE':
            registration.basic.homepageESuccess(formId, pageName, email, source, legacy, responseArr);
            break;
        case 'Its-Your-Call':
            registration.basic.itsYourCallSuccess(formId, pageName, email, source, legacy, responseArr);
            break;
        case 'GBR-Audio-Conferencing':
        case 'GBR-Better-Conferencing':
        case 'GBR-Conference-Call':
        case 'GBR-Conference-Call-Providers':
        case 'GBR-Conference-Call-Services':
        case 'GBR-Free-Conference-Call':
        case 'GBR-International-Conference-Call':
        case 'GBR-Int-Conference-Call':
        case 'GBR-Love-Hate-Travel':
        case 'GBR-Remote-Working':
        case 'GBR-Teleconference':
        case 'GBR-Teleconferencing-Services':
        case 'GBR-Telephone-Conferencing':
        case 'GBR-Voice-Conferencing':
            registration.basic.landingPageSuccess(formId, pageName, email, source, legacy, responseArr);
            break;
        default:
            console.log('formId: ' + formId);
            console.log('email: ' + email);
            console.log('source: ' + source);
            console.log('legacy: ' + legacy);
            console.log('pageName: ' + pageName);
            console.log(responseArr);
    }
};

/**
 * Basic Registration - Homepage E Success
 * @param formId
 * @param pageName
 * @param email
 * @param source
 * @param legacy
 * @param responseArr
 */
registration.basic.homepageESuccess = function (formId, pageName, email, source, legacy, responseArr) {
    var pin = responseArr['pin'],
        pinRefHash = responseArr['pinRefHash'],
        vDialIn,
        vScreenShare,
        vInternational,
        prm,
        date,
        registeredEmail;

    if (responseArr["redirect_url"].trim() != '') {
        dataLayer.push({
            'event': '/Homepage-E-Top/RegistrationSuccess',
            'PIN': pinRefHash,
            'gcp': window.google_tag_params
        });
        setTimeout(function () {
            window.location.assign(responseArr["redirect_url"].trim());
        }, 1000);
    } else {
        utils.removeAjaxLoader(null);

        // Show Ajax Response
        $('#tab-pwn-pin-registered')
            .html(responseArr['html']['homepageHTML'])
            .show();

        $('#videos-for-signup').show();

        // Hide Current DIVs
        $('#tabs-grid').hide();
        $('.index-costs-compare').hide();
        $('#index-choose-plan').hide();
        $('#right-content-container').hide();
        $('#index-information').hide();

        // Email
        registeredEmail = $('#register_email').val();

        // Outlook plugin parameters
        vDialIn = "0844 4 73 73 73";
        vScreenShare = "http://powwownow.yuuguu.com/?pin=" + pin;
        vInternational = "http://www.powwownow.co.uk/Conference-Call/International-Number-Rates";

        prm = "CSPPrmtr0=" +
            pin + "|CSPPrmtr1=" +
            vDialIn + "|CSPPrmtr2=" +
            vScreenShare + "|CSPPrmtr3=" +
            vInternational + "|";
        StoreInfo('Parameters', prm, 30);

        // Google Tag Manager
        if ('HomepageE' === source) {
            dataLayer.push({
                'event': '/Homepage-E-Top/RegistrationSuccess',
                'PIN': pinRefHash,
                'gcp': window.google_tag_params
            });
        }

        $('#share-details').click(function () {
            setTimeout(function () {
                dataLayer.push({'event': '/Homepage-E/FullRegistrationSharedDetailsClick'});
            }, 1000);
        });

        $('#create_login_email').val(registeredEmail);

        // Set a cookie value with current email address => this cookie will expire in next 15 minutes,
        // (used for create login form)
        date = new Date();
        date.setTime(date.getTime() + (15 * 60 * 1000));
        $.cookie("prepopulatedemail", registeredEmail, { expires: date });

        // Set a cookie to remember the pin until the browser gets closed
        $.cookie("registered_pin", pin);

        // Setup JCarousel & LightBox for videos
        videos_Init();
    }
};

/**
 * Basic Registration - Landing Pages
 * @param formId
 * @param pageName
 * @param email
 * @param source
 * @param legacy
 * @param responseArr
 */
registration.basic.landingPageSuccess = function (formId, pageName, email, source, legacy, responseArr) {
    utils.removeAjaxLoader(null);

    var pinHtml = responseArr['html']['landingpageHTML'],
        rightContentContainer = '#right-content-container',
        tabPinRegistered = '#tab-pwn-pin-registered';

    // Apply HTML Changes to the Main Pages

    // Video Content
    // Should take the whole width of the page
    $(rightContentContainer).html($('#videoCarouselContainer').parent().html());
    $(rightContentContainer).attr('id', 'lower-content-container');
    $('#lower-content-container').attr('class', 'grid_24 omega');
    $('#left-content-container').remove();

    // resizing the main box to fit the content
    $('#right-menu-container').css('margin-bottom', '80px');
    $('#landingpage-tabs-container').animate({height: '370px'}, 1500);

    // PIN Content
    $(tabPinRegistered).html(pinHtml);
    $('#tab-pwn').hide();
    $(tabPinRegistered).show();

    // Setup the Video Carousel
    var videosContainer = $('#videos');
    if (videosContainer.is(':visible')) {
        videosContainer.jcarousel({ visible: 3, scroll: 3 });
//        videosContainer.find('.polaroid').polaroid({zoom: 1.6});
    }
};

/**
 * Basic Registration - Its-Your-Call
 * @param formId
 * @param pageName
 * @param email
 * @param source
 * @param legacy
 * @param responseArr
 */
registration.basic.itsYourCallSuccess = function (formId, pageName, email, source, legacy, responseArr) {
    setTimeout(function () {
        $('#response').html(responseArr['html']['iycHTML']);
    }, 1000);
};
