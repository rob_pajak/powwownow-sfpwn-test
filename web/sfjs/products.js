$(document).ready(function () {
    $(".product-accordion").accordion({ autoHeight: false });
    $('#purchse-credit-cancel-button').click(function(){
        $('#credit-purchase-row').hide();
        $('#credit-select-button').show();
    });
    $('#credit-select-button').click(function(){
        $('#credit-purchase-row').show('slow');
        $('#credit-select-button').hide();
    });
    $('.bwm-name-form').each(function(i,obj){
        var formId = $(obj).attr("id");
        myPwnApp.bwm.changeName.initAjaxForm(formId);
    });
    $('#topup_topup-amount_input').keyup(function () {
        if (isNaN($(this).val())) {
            $('.topup-amount-fill').html('Invalid Number');
            return;
        } else if ($(this).val() > 4166) {
            $('.topup-amount-fill').html('Maximum &pound;4166');
            return;
        }

        tmp = Math.round(($(this).val() * 1.2) *100)/100; // 20% VAT and 2 DP Rounding
        tmp = tmp.toString();
        if (tmp.indexOf('.') > -1) {
            var diff = tmp.length - tmp.indexOf('.') - 1;
            if (diff === 0) {
                tmp = tmp + '00';
            } else if (diff === 1) {
                tmp = tmp + '0';
            }
        } else {
            tmp = tmp + '.00';
        }
        $('.topup-amount-fill').html('+20% VAT (£' + tmp + ')');
    });
    $('#topup_topup-amount_xxx').focus(function () {
        $('#topup_topup-amount_input').focus();
    });

    $('#credit-form').initMypwnForm({
        messagePos: 'off',
        requiredAsterisk: 'off',
        success: function (json) {
            if (json.data) {
                $('#credit-purchase-amount-warning').hide();
                $('#credit-purchase-row').hide();
                $('#credit-select-button').hide();
                $('#credit-added-button').show();
                $('#updateAccount').show();
                CommonBasket.increaseBasketcount(parseFloat(json.data.total));
                CommonBasket.addProductToBasketTooltip(
                    json.data.id,
                    json.data.total,
                    json.data.label
                );
                CommonBasket.showBasketTooltip();
                var hrefSplit = location.href.split('#');
                if (hrefSplit.length > 1) {
                    location.href = hrefSplit[0] + '#bskt-prds';
                } else {
                    location.href += '#bskt-prds';
                }
            }
            if (json['incompatible']) {
                alert(json.incompatible);
                $('#purchse-credit-cancel-button').click();
                $('#credit-select-button').hide();
            }
        }
    });

});


