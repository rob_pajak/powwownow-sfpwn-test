/**
 * Ths file will be used to store Utility Functions which are used throughout site
 */

var utils;
utils = {};

/**
 * Checks to see if an Email is valid
 * @param email
 * @returns bool
 */
utils.isValidEmail = function (email) {
    if (email === undefined) {
        return false;
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email.match(re);
}

/**
 * Sets a Text Field to show some text, until the field has focus.
 * Originally Taken from form-functions.js
 * @param formElement
 * @param defaultText
 * @deprecated
 */
utils.defaultTextField = function (formElement, defaultText) {
    console.warn('utils.defaultTextField is now deprecated');
};

/**
 * Shows form error boxes for each field in the fieldArr
 * Originally taken from form-functions.js
 *
 * fieldsArr is a multidimensional arr containing the field to validate and the
 * error message, or specify 'alert' as the field_name for an alert box.
 *
 * e.g
 * {
 *  {'message':'Please enter a valid email', 'field_name': 'email',
 *  {'message':'Please check the box', 'field_name': 'agree_terms_and_conditions'}
 *  {'message':'An error occurred', 'field_name': 'alert'}
 * }
 *
 * The error box and arrow are created with ids of field-name-error-box and
 * field-name-error-pointer so their layout can be fine-tuned in CSS
 *
 * To separate out language from functionality, the message can be passed as a JS constant.
 * If the constant has been specified in quotes, (it probably will if it came from PHP)
 * pass quotedConstants as true to eval() them.
 *
 * @param fieldsArr       Form fields and error messages
 * @param formId          The id of the form being validated
 * @param quotedConstants True if the messages are values of javascript constants,
 *                                  but have been quoted
 * @param showMultiple    If true will show multiple error boxes, otherwise
 *                                  only the first error will be shown
 *
 * @return bool                     True if no error boxes were needed to be shown,
 *                                  otherwise true
 */
utils.legacyInputErrorBoxes = function (fieldsArr, formId, quotedConstants, showMultiple) {

    // Set Default Values
    if (showMultiple === undefined) {
        showMultiple = false;
    }

    if (quotedConstants === undefined) {
        quotedConstants = false;
    }

    // Remove the # of the ID, if it is given
    formId = formId.replace('#', '');

    // Hide all existing error boxes
    $('.nice-error-box').remove();

    // If there is nothing to validate then validation has passed
    if (fieldsArr.length == 0) {
        return true;
    }

    // Loop round every field to show checkbox for
    $.each(fieldsArr, function (fieldIndex, fieldValues) {

        // If the constant is quoted then get the constant value
        if (quotedConstants) {
            fieldValues['message'] = eval(fieldValues['message']);
        }

        // If the field name is specified as alert, then show an alert box
        if (fieldValues['field_name'] == 'alert') {
            alert(fieldValues['message']);
        } else {
            // A field name has been specified, so show the red tooltip error

            // Get the input (or select etc) element
            fieldHandle = $("#" + formId + " [name=\"" + fieldValues['field_name'] + "\"]");

            // Show error message,
            fieldHandle.parent().after('<div class="nice-error-box" id="' + fieldValues['field_name'] + '-error-box"><div class="nice-error-box-before"><img src="/sfimages/nice-error-pointer.gif" alt="" /></div>' + fieldValues['message'] + '<span class="error-box-close" onclick="$(this).parent(\'div\').hide();">x</span></div>');

            // Remove the error when the field the error message for becomes focused,
            fieldHandle.focusin(function () {
                $('#' + fieldValues['field_name'] + '-error-box').remove();
            });
        }

        // Quit looping if we are not showing multiple messages
        if (!showMultiple) {
            return false;
        }
    });

    return false;
};

/**
 * Output the Message from Either the Success and Error Methods
 * @param formId
 * @param responseText
 * @param msgType
 * @param redirectOnSuccess
 * @param reload
 * @param responsive
 */
utils.outputMessage = function (formId, responseText, msgType, redirectOnSuccess, reload, responsive) {
    // Check if the ResponseText is not Undefined
    if (undefined == responseText.field_name && responseText[0].field_name !== undefined) {
        responseText = responseText[0];
    }

    // Check if the Message is a Global
    if (window[responseText.message] !== undefined) {
        responseText.message = window[responseText.message];
    }

    // Show Message
    if ('alert' == responseText.field_name) {
        $(formId).find('.message').remove();
        switch (msgType) {
            case 'error':
                $(formId).append('<div class="error message grid_24">' + responseText.message + '</div>');
                $(formId).find('.message').delay(5000).fadeOut('slow');

                if (false !== reload) {
                    window.setTimeout(function () {
                        window.location.reload();
                    }, 5500);
                }
                break;
            case 'success':
                $(formId).append('<div class="success message grid_24">' + responseText.message + '</div>');
                $(formId).find('.message').delay(5000).fadeOut('slow');

                if (false !== redirectOnSuccess && window[redirectOnSuccess] !== undefined) {
                    window.setTimeout(function () {
                        window.location = window[redirectOnSuccess];
                    }, 5500);
                } else if (false !== redirectOnSuccess && typeof redirectOnSuccess !== 'undefined') {
                    window.setTimeout(function () {
                        window.location = redirectOnSuccess;
                    }, 5500);
                } else if (false !== reload) {
                    window.setTimeout(function () {
                        window.location.reload();
                    }, 5500);
                }

                break;
            default:
                alert(responseText.message);
                break;
        }
    } else {
        // Check Responsive - Used for Parsley Output
        if (undefined !== responsive && responsive) {
            // Remove any Existing Parsley Errors and Also PWN Form Errors
            $('.pwn-custom-error').remove();
            $('.h5form-error').remove();

            $(formId + ' input[name="' + responseText.field_name + '"]').after(
                '<small class="pwn-custom-error parsley-error-list">' +
                    '<p class="custom-error-message">' +
                    responseText.message +
                    '</p>' +
                    '</small>'
            ).parent().addClass('error');
        } else if (undefined === responsive || !responsive) {
            $(formId + ' input[name="' + responseText.field_name + '"]').after(
                '<div class="h5form-error nice-error-box">' +
                    responseText.message +
                    '</div>'
            );
        }
    }
};

/**
 * Obtain the Correct Message
 * @param responseArr
 * @param msgType
 * @returns {string}
 */
utils.obtainMessage = function (responseArr, msgType) {
    var responseTxt = null,
        responseText = null,
        message = '';

    // Newer versions of Jquery Log Errors to the console on the case if the response is not a JSON.
    try {
        responseTxt = $.parseJSON(responseArr);
    } catch (err) {

    }

    try {
        responseText = $.parseJSON(responseArr.responseText)
    } catch (err) {

    }

    // Most Common Success
    if ("success" == msgType && null !== responseArr && typeof responseArr === "object" && "success_message" in responseArr) {
        message = responseArr.success_message;
    } else if ("success" == msgType && null !== responseTxt && typeof responseTxt === "object" && "success_message" in responseTxt) {
        message = responseTxt.success_message;
    } else if ("success" == msgType && null !== responseArr && responseArr === "object" && "responseText" in responseArr && "success_message" in responseArr.responseText) {
        message = responseArr.responseText.success_message;
    } else if ("success" == msgType && null !== responseText && typeof responseText === "object" && "success_message" in responseText) {
        message = responseText.success_message;
    }

    // Most Common Errors
    if ("error" == msgType && null !== responseArr && typeof responseArr === "object" && "error_messages" in responseArr) {
        message = responseArr.error_messages;
    } else if ("error" == msgType && null !== responseTxt && typeof responseTxt === "object" && "error_messages" in responseTxt) {
        message = responseTxt.error_messages;
    } else if ("error" == msgType && null !== responseArr && responseArr === "object" && "responseText" in responseArr && "error_messages" in responseArr.responseText) {
        message = responseArr.responseText.error_messages;
    } else if ("error" == msgType && null !== responseText && typeof responseText === "object" && "error_messages" in responseText) {
        message = responseText.error_messages;
    }

    return message;
};

/**
 * Show the Ajax Loader Image after a Specific Element.
 *
 * @param img - This is the Image src of the loader image
 * @param appearAfter - The Element the Image should appear after
 * @param imgId - The Image Container ID
 * @param positionTop - The Top Position in the Container
 * @param positionLeft - The Left Position in the Container
 * @param imageOnly - Show the Image Only?
 */
utils.showAjaxLoader = function (img, appearAfter, imgId, positionTop, positionLeft, imageOnly) {
    // Check the Image ID
    if (imgId == null) {
        imgId = 'ajax_loader';
    }

    // Create Element
    if (imageOnly) {
        var ele = '<img id="' + imgId + '" src=' + img + ' alt="Ajax Loader"/>';
    } else {
        var ele = '<div id="' + imgId + '" style="top: ' + positionTop + 'px; left: ' + positionLeft + 'px;"><img src=' + img + ' alt="Ajax Loader"/></div>';
    }


    // Display the Image
    $(ele).insertAfter(appearAfter);

    return true;
};

/**
 * Removes the Ajax Loader Image, which was added by the utils.showAjaxLoader method
 * @param imgId - The Image Container ID
 */
utils.removeAjaxLoader = function (imgId) {
    // Check the Image ID
    if (imgId == null) {
        imgId = 'ajax_loader';
    }

    // Remove the Container
    $('#' + imgId).remove();
};

/**
 * Button Functionality
 * @param formId
 */
utils.buttonInitialise = function (formId) {
    $(formId + ' button').click(function () {
        var buttonId = $(this).attr('id');
        var buildUrl = false;
        var pinCount = $(formId).find('input[name="r[]"]:checked').length;
        var target = '_self';
        switch (buttonId) {
            case 'see-demo':
                buildUrl = '/Show-Time/Demo/';
                target = '_blank';
                break;
            case 'user-guide':
                buildUrl = '/sfpdf/en/Powwownow-Web-Conferencing-User-Guide.pdf';
                target = '_blank';
                break;
            case 'call-settings':
                if (pinCount === 0) {
                    alert(FORM_VALIDATION_NO_PIN);
                    return false;
                }
                buildUrl = URL_MYPWN_CALL_SETTINGS;
                break;
            case 'call-settings-single':
                buildUrl = URL_MYPWN_CALL_SETTINGS;
                break;
            case 'request-welcome-pack':
                buildUrl = URL_MYPWN_REQUEST_WALLET_CARD;
                break;
            case 'request-welcome-pack-single':
                if (pinCount === 0) {
                    alert(FORM_VALIDATION_NO_PIN);
                    return false;
                }
                buildUrl = URL_MYPWN_REQUEST_WALLET_CARD;
                break;
            case 'assign-products':
                buildUrl = URL_MYPWN_PLUS_MYPIN_PRODUCTS_PAGE;
                break;
            case 'call-history':
                buildUrl = MYPWN_LOGIN_REDIRECTS[9];
                break;
            case 'delete-users':
            case 'delete-pins':
                if (pinCount === 0) {
                    alert(FORM_VALIDATION_NO_PIN);
                    return false;
                } else {
                    return confirm(FORM_CONFIRM_PIN_DELETION);
                }
                break;
            case 'add-pin':
                buildUrl = '/myPwn/Create-Pin';
                break;
            case 'add-user':
                buildUrl = '/myPwn/Create-User';
                break;
            case 'create-pin':
                return true;
            case 'share-details':
                return true;
            default:
                return false;
        }

        // Check the buildUrl
        if (false !== buildUrl && pinCount > 0) {
            // Build the PIN Refs into the URL
            buildUrl += '?';
            $(formId).find('input[name="r[]"]:checked').each(function (i) {
                buildUrl += 'r[]=' + $(this).val() + '&';
            });

            // Remove the Additional & in the URL
            if (buildUrl.charAt(buildUrl.length - 1) === '&') {
                buildUrl = buildUrl.substr(0, buildUrl.length - 1);
            }

            if (target == '_self') {
                window.location = buildUrl;
            } else if (target == '_blank') {
                window.open(buildUrl, target);
            }
        } else if (false !== buildUrl && pinCount == 0) {
            if (target == '_self') {
                window.location = buildUrl;
            } else if (target == '_blank') {
                window.open(buildUrl, target);
            }
        }
        return false;
    });
};
