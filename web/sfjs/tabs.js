/**
 * Show the Content and Hide Appropriate Tabs for Web and Video Conferencing Pages
 * @param tabNumber
 */
function showContentForWebAndVideoConferencingTags(tabNumber) {
    var tab1Element = $('#tab-1'),
        tab2Element = $('#tab-2'),
        tab3Element = $('#tab-3'),
        tab4Element = $('#tab-4'),
        getStartedElement = $('#tab-web-conferencing-get-started'),
        demoElement = $('#tab-web-conferencing-demo'),
        downloadElement = $('#tab-web-conferencing-download'),
        showtimeElement = $('#tab-web-conferencing-showtime');

    switch(tabNumber) {
        case 1:
            tabs_deactivate(2);
            tabs_deactivate(3);
            tabs_activate(1);

            tab1Element.addClass('tab-1-green');
            tab1Element.removeClass('tab-1-grey');
            tab2Element.removeClass('tab-2-green');
            tab2Element.addClass('tab-2-grey');
            tab3Element.removeClass('tab-3-green');
            tab3Element.addClass('tab-3-grey');

            getStartedElement.hide();
            demoElement.hide();
            downloadElement.hide();
            showtimeElement.show();
            break;
        case 2:
            tabs_deactivate(1);
            tabs_deactivate(3);
            tabs_activate(2);

            tab2Element.addClass('tab-2-green');
            tab2Element.removeClass('tab-2-grey');
            tab1Element.removeClass('tab-1-green');
            tab1Element.addClass('tab-1-grey');
            tab3Element.removeClass('tab-3-green');
            tab3Element.addClass('tab-3-grey');

            getStartedElement.hide();
            showtimeElement.hide();
            downloadElement.hide();
            demoElement.show();
            break;
        case 3:
            tabs_deactivate(1);
            tabs_deactivate(2);
            tabs_activate(3);

            tab3Element.addClass('tab-3-green');
            tab3Element.removeClass('tab-3-grey');
            tab1Element.removeClass('tab-1-green');
            tab1Element.addClass('tab-1-grey');
            tab2Element.removeClass('tab-2-green');
            tab2Element.addClass('tab-2-grey');

            demoElement.hide();
            showtimeElement.hide();
            downloadElement.hide();
            getStartedElement.show();
            break;
        case 4:
            tab4Element.removeClass('tab-3-grey');
            tab4Element.addClass('tab-4-green');

            demoElement.hide();
            showtimeElement.hide();
            getStartedElement.hide();
            downloadElement.show();
            break;
    }
}


var tab_active_position,
    tab_active_position_jump,
    tab_passive_position,
    tab_passive_position_jump;

/**
 * IE fix for positioning tabs animation
 * @param top - Optional
 */
function tabs_setUpTabPositions(top) {
	tab_active_position = top || 71;
	tab_active_position_jump  = tab_active_position - 3;
	tab_passive_position      = tab_active_position + 37;
	tab_passive_position_jump = tab_passive_position - 3;

	if ($.browser && ($.browser.version == '6.0' || $.browser.version == '7.0')) {
		tab_active_position       = (top + 6) || 77;
		tab_active_position_jump  = tab_active_position - 3;
		tab_passive_position      = tab_active_position + 33;
		tab_passive_position_jump = tab_passive_position - 3;
	}

	if ($.browser && $.browser.version == '8.0') {
		tab_active_position       = (top + 1) || 74;
		tab_active_position_jump  = tab_active_position - 3;
		tab_passive_position      = tab_active_position + 36;
		tab_passive_position_jump = tab_passive_position - 3;
	}
}

/**
 * Tab onHover Wiggle
 * @param tabNum
 */
function tabs_setUpHoverWiggle(tabNum) {
    var tabIdElement = $('#tab-' + tabNum);

    tabIdElement
        .mouseenter(function () {
            tabIdElement.not('.active').animate({top: tab_passive_position_jump + 'px'}, 300);
        })
        .mouseleave(function() {
            tabIdElement.not('.active').animate({top: tab_passive_position + 'px'}, 300, function() {
			$('#tab-' + tabNum + ' .tab-shadow').show();
		});
	});
}

/**
 * Returns the specified tab to the un-active position
 * @param tabNum
 */
function tabs_deactivate(tabNum) {
    var tabIdElement = $('#tab-' + tabNum);
    tabIdElement
        .stop()
        .removeClass('active')
        .not('.active').animate({top: tab_passive_position + 'px'}, 300, function () {
            $('#tab-' + tabNum + ' .tab-shadow').show();
        });
}

/**
 * Puts a tab in the active position
 * @param tabNum
 */
function tabs_activate(tabNum) {
	var tabIdElement = $('#tab-' + tabNum);
    tabIdElement.addClass('active');
	$('#tab-' + tabNum + ' .tab-shadow').hide();
    tabIdElement.animate({top: tab_active_position_jump + 'px'}, 300, function() {
        tabIdElement.animate({top: tab_active_position + 'px'});
        tabIdElement.removeClass('passive');
	});
}

/**
 * Add canonical link element for SEO purposes; check for existence first to avoid duplicates
 * @param link
 */
function tabs_addCanonicalLink(link) {
    if ($('head link[rel="canonical"]').length == 0) {
        $('head').append('<link href="' + link + '" rel="canonical" />');
    }
}