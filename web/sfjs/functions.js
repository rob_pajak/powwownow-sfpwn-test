/************************** JS Needed on Every Page*********************/
$(document).ready(function () {
    // Since Conditional Comments are removed in IE10, this will add it back in.
    if ($.browser.msie && $.browser.version == 10) {
        $("html").addClass("ie10");
    }

    // Store the Width and the Height
    var width = (screen.width) ? screen.width : '';
    var height = (screen.height) ? screen.height : '';

    // Store the Date and Time
    var date = new Date();
    date.setTime(date.getTime() + (15 * 60 * 1000));

    // Check For Windows Off Standard DPI Screen Resolution
    if (typeof(screen.deviceXDPI) == 'number') {
        width *= screen.deviceXDPI / screen.logicalXDPI;
        height *= screen.deviceYDPI / screen.logicalYDPI;
    }

    $.cookie('screenWidth', width, '{expires: date}');
    $.cookie('screenHeight', height, '{expires: date}');
});


/************************** JS Functions ******************************/

function showTimeInActionToolTips_Init() {
    $('.tooltip-showtime-in-action-close').click(function() {
        $('#tooltip-showtime-in-action').toggle('fast');
        return false;
    });
    $('.tooltip-showtime-in-action-link').click(function() {
        $('#tooltip-showtime-in-action').toggle('fast');
        return false;
    });
}

/*My PWN Tooltips Array*/
var myPWNtoolTips = Array('tooltip-header-all-pins', 'tooltip-header-all-tels');

/**
 * Opens the myPWN Header Popup Window for either the All Pins or All Tels Window
 */
function myPwnHeader_init() {
    
    //Go through the Tooltips in the Array to apply the onClick Events. -link and -close buttons are applied
    for (toolTipsIndex in myPWNtoolTips) {

        //-link Function
        $('.' + myPWNtoolTips[toolTipsIndex] + '-link').click(function() {

             //Get the Class of the original Tooltip, by removing the -link and additional classes
            baseId = $(this).attr('class').replace('-link', '');

            //Open the Tooltip
            myPwnHeader_ShowTooltip(baseId);
            
            return false;
        });

        //-close Function
        $('.' + myPWNtoolTips[toolTipsIndex] + '-close').click(function() {
            
            //Get the Class of the original Tooltip, by removing the -close and additional classes
            baseId = $(this).attr('class').replace('-close floatright sprite tooltip-close', '');

            //Hide and Reset the Tooltip
            $('.' + baseId).hide('fast');
            
            return false;
        });
    }
    
    //Update Popup Description for the Skype and Worldwide Class
    $('.worldwide').html(MYPWN_DIALINNUMBER_WORLDWIDE);
    $('.skype').html(MYPWN_DIALINNUMBER_SKYPE);
}

/**
 * Opens the specified tooltip, and closes the other tooltips on the right nav
 *
 * This is called by myPwnHeader_init(); but can also be called manually
 * by passing the id of the tooltip to open:
 * e.g. myPwnHeader_ShowTooltip(tooltip-international-dial-in-numbers);
 *
 */
function myPwnHeader_ShowTooltip(toolTipId) {
    // If we are opening an element then hide any other open tooltip
    if ($('.' + toolTipId).is(':hidden')) {
        for (toolTiptoCloseIndex in myPWNtoolTips) {
            if ($('.' + myPWNtoolTips[toolTiptoCloseIndex]).is(':visible')) {
                $('.' + myPWNtoolTips[toolTiptoCloseIndex]).hide('fast');
            }
        }
    }
    $('.' + toolTipId).toggle('fast');    
}

/**
 * This loads the social media footer content from the cached JSON based HTML file
 * into the corresponding ul.
 *
 * To know which ul to load the data into, it calculates the ID tag by getting the
 * JSON key, then prepends socialMediaFooter_
 */
function socialMediaFooter_Init() {
    $.getJSON('/ajax/social-media-footer.php', function(data) {
        $.each(data, function(key, listItems) {
            $('#socialMediaFooter_' + key).html(listItems);
        });
    });
}

/**
 * Sets up the fading of the promotional boxes. (The big images
 * on the lower right half of the home page, also appears on some other pages)
 *
 * They loop round once then stop on the first slide
 * If the slide is hovered then animation stops
 * There is also a navigation for these elements see function
 * for this promotionalFadeShowSlide()
 */
function promotionalFadeInitialise() {

    // Tracks which list item should be shown
    liToShow = 0;
    
    // Sets the duration each slide is shown for, only needs to be changed
    // here
    promotionalFadeSlideSpeed = 5000;

    // Add time delay to call function to show the next slide
    promotionalTimeout = setTimeout('promotionalFadeNext()', promotionalFadeSlideSpeed);

    // Set up cancelling of fade on hover of the promotionals area
    $('#rotating-promotional-container').mouseenter(function() {
        clearTimeout(promotionalTimeout);
    });

}

/**
 * Figures out what slide is the next one to show, then does the
 * function call do show it
 *
 * Stops on the first slide after one iteration
 */
function promotionalFadeNext() {

    //load first image of the slide
    if (liToShow == 0) {
        var slideUrl = $('ul#rotating-promotional-list li:eq(' + liToShow + ')').attr('title');
        $('ul#rotating-promotional-list li:eq(' + liToShow + ')').attr('title', '');
        if (slideUrl != '') {
            $('ul#rotating-promotional-list li:eq(' + liToShow + ')').css({'backgroundImage': 'url(' + slideUrl +')'});
        }    
    }

    liToShow = liToShow + 1;

    // Hide the visibile list item(s)
    $("ul#rotating-promotional-list").children('li:visible').hide();

    // See if we have reached the last slide, if so we need to show the first
    // one. Note we do not call setTimeout(); as we don't want animation to continue
    // after all slides have been shown
    if (liToShow >= ($("ul#rotating-promotional-list").children().length)) {
        // Show first slide and not show any more slides
        promotionalFadeShowSlide(0, true);
    } else {
        // Show next slide and call this function again to show next slide
        promotionalTimeout = setTimeout('promotionalFadeNext()', promotionalFadeSlideSpeed);
        promotionalFadeShowSlide(liToShow);
    }
}

/**
 * Show the specified slide and highlight the corresponding left nav
 *
 * @param slideNum      int         Slide number to show, 1 being the first slide
 * @param cancelLooping null|bool   If true, will stop the rotator automatically moving
 *                                  to the next slide
 */
function promotionalFadeShowSlide(slideNum, cancelLooping) {

    // Show specified left nav
    $('ul#rotating-promotional-links li').removeClass('filled');
    $('ul#rotating-promotional-links li:eq(' + slideNum + ')').addClass('filled');

    // Show specified slide
    $('ul#rotating-promotional-list li:visible').hide();
    var slideUrl = $('ul#rotating-promotional-list li:eq(' + slideNum + ')').attr('title');
    $('ul#rotating-promotional-list li:eq(' + slideNum + ')').attr('title', '');
    if (slideUrl != '') {
        $('ul#rotating-promotional-list li:eq(' + slideNum + ')').css({'backgroundImage': 'url(' + slideUrl +')'});
    }
    $('ul#rotating-promotional-list li:eq(' + slideNum + ')').show();

    // If cancelLooping was set to true, then clear the timeout which shows the next slide
    if (cancelLooping !== undefined && cancelLooping) {
        clearTimeout(promotionalTimeout);
    }

}

/**
 * TOGGLE LIST
 *
 * The FAQ page has a show/hide component which has a links at the top,
 * which when clicked show the corresponding content.
 * 
 * To set this up, put the main content which needs to be toggled in
 * <ul> list items, ensuring that all content which should be hidden on page load
 * is hidden via css. Also give that <ul> a nice id, like faq_show_hide_list.
 *
 * Then to show the content for a particular li, pass its index and the 
 * id of the list, it will show the required li and hide the rest:
 *
 * Eg:
 *   <a href="#" onclick="toggleListItems_ShowListItem(1, 'faq_show_hide_list');">link 1</a>
 *
 * Will show the content of the first li in a ul with id of 'faq_show_hide_list'
 */
function toggleList_ShowListItem(itemNumber, listId) {
    $('#' + listId).children('li:visible').hide();
    $('#' + listId).children('li:nth-child(' + (itemNumber) + ')').fadeIn('fast');
}

function hide_Submenu2(obj) {
    console.warn('hide_Submenu2 is now deprecated');
}

/**
 * Sets up the drop down menus for the horizontal navigation bar near the
 * top of the page
 */
function dropDownMenu_Init() {
    console.warn('dropDownMenu_Init is now deprecated');
}

function videos_Init() {

    $('#videos').jcarousel( { visible: 3, scroll: 3 } );
    
    $('#videos a').prettyPhoto({
        flash_markup:
            '<object id="flowplayer" width="{width}" height="{height}" data="{path}" type="application/x-shockwave-flash">' +
                '<param name="movie" value="{path}" />' +
                '<param name="allowfullscreen" value="true" />' +
                '<param value="opaque" name="wmode" />' +
            '</object>'
    });
        
    $('#videos .polaroid').polaroid({
        /*cufon: true,*/
        zoom: 1.6,
        preAnimate: function() {
            $('a', this).prettyPhoto();
        }
    });
}

function chapterSelect_Init() {
    var $chapterSelectArea = $('#video-chapters, #video-chapter-select p, #video-chapter-select .arrow');
    var $arrow = $('#video-chapter-select .arrow');
    
    // Set up event handlers for extending & retracting the chapter select bar
    $chapterSelectArea
        .hover( function() {    // Mouseenter
            // Change arrow to lighter colour
            var height = $arrow.height();
            $arrow.css('background-position', '0 -'+height+'px');
            
            // Clear timeout for retracting chapter select bar, if set
            if ( $('#video-chapter-select').data('retractTimeout') != 'undefined' ) {
                window.clearTimeout( $('#video-chapter-select').data('retractTimeout') );
            }
        }, function() {            // Mouseleave
            // Return arrow to default colour
            $arrow.css('background-position', '');
            
            // Retract chapter select bar if mouse is outside of it for 500ms
            $('#video-chapter-select').data(
                'retractTimeout',
                window.setTimeout( function() { retractVideoChapterSelect() }, 500 ) 
            );
        }).click( function() {
            toggleVideoChapterSelect();
        });
        
    
    // Set up flowplayer    
    var videoUrl = $('#flowplayer').attr('href');
    $f('flowplayer', {
        src: '/shared/flowplayer/flowplayer-3.2.5.swf',
        wmode: 'opaque'
    },  {
        clip: {
            autoPlay: false,
            autoBuffering: true,
            baseUrl: videoUrl
        },
        
        plugins: {
            controls: {
                url: '/shared/flowplayer/flowplayer.controls.swf'
            }
        }
    });


    // To prevent Firefox automatic drag & drop
    if ($.browser.mozilla) {
        $('#flowplayer').mousedown( function( event ) {
            event.preventDefault();
        });
    }
        
    // Set up click handlers for chapters (time index in seconds is in href of link, e.g. href="#30")
    $('#video-chapters a').click( function( event ) {
        event.preventDefault();
        $f().play().seek( $(this).attr('href').substring(1) );
    });
}

function extendVideoChapterSelect() {
    $('#video-chapter-select ul').removeClass('hidden');
    $('#video-chapters').animate({
        'width': '412'
    }, 'fast', function() {
        $('#video-chapter-select').data('extended', true);
    });
}

function retractVideoChapterSelect() {
    $('#video-chapters').animate({
        'width': '23'
    }, 'fast', function() {
        $('#video-chapter-select ul').addClass('hidden');
        $('#video-chapter-select').data('extended', false);
    });
}

function toggleVideoChapterSelect() {
    if ( $('#video-chapter-select').data('extended') ) {
        retractVideoChapterSelect();
    } else {
        extendVideoChapterSelect();
    }
}

/**
 * @deprecated
 * @param mainText
 * @param titleText
 */
function dialogBox(mainText, titleText) {
    console.warn('dialogBox is now deprecated');
}

/**
 * Initialises the displaying of the paginated news
 */
function newsPagination_init() {
    newsPagination_LoadPaginatedPage(0, 5);
}

/**
 * Loads up the list of pagianted news articles for the specified page based on the
 * quantity per page, and places them in an div with id of news_paginator_placeholder:
 * Note the page number in this function start from 0, but obviously we wouldn't show
 * that to a user
 * newsPagination_LoadPaginatedPage(0, 10);
 * 
 * This also updates the value of all elements with a class of items_per_page to
 * be that of articlesPerPage
 
 * Specify -1 as the quantity per page to view all articles, eg:
 * newsPagination_LoadPaginatedPage(0, -1);
 *
 * @param int pageNumber       The page paginated page number to display, starting from 0
 * @param int articlesPerPage  Number of articles to show on each page, -1 for all articles
 */
function newsPagination_LoadPaginatedPage(pageNumber, articlesPerPage) {

    // Change the value of the items_per_page select box to be that of pageNumber
    $("select[name=items_per_page]").val(articlesPerPage);
    
    // Perform request to get news articles
    $.get('/News/ajax/paginator.php', {'pageNumber': pageNumber, 'articlesPerPage': articlesPerPage}, function(responseText) {
        reponseArr = eval('(' + responseText + ')');
        $('#pager').html(reponseArr['navigation']);
        $('#news-list').html(reponseArr['articles']);
    });
}

/**
 * Executes Bing tracking only if it is required on the current page
 * 
 * The tracking is only required on some pages, but they 
 * share components and ajax files, so this function only executes the
 * tracking if it is in the BingTrackingRequired array
 * 
 * @return bool If tracking was executed
 */
function Bing_tracker_ExecuteConditionally() {

    // Array of pages tracking is required on
    var trackingRequired = {
        'GB': [
            '/',
            '/Audio-Conferencing',
            '/Better-Conferencing',
            '/Conference-Call',
            '/Conference-Calling',
            '/Conference-Call-Providers',
            '/Conference-Call-Services',
            '/Free-Conference-Call',
            '/International-Conference-Call',
            '/Iphone-Conference-App',
            '/Phone-Conferencing',
            '/Teleconference',
            '/Teleconferencing',
            '/Teleconferencing-Services',
            '/Telephone-Conferencing',
            '/Uk-Conference-Calls',
            '/Voice-Conferencing'
        ],
        'DE': [
            '/Audio-Conference',
            '/Kostenfrei-Telefonieren',
            '/Telefonkonferenz',
            '/Telefonkonferenz/Festnetz',
            '/Telefonkonferenz/Handy',
            '/Telefonkonferenz/Internationale',
            '/Telefonkonferenz/Organisieren',
            '/Telefonkonferenz/was-ist',
            '/Telefonkonferenz/wie-man',
            '/Telefonkonferenzen',
            '/Telephone-Conference',
            '/Web-Meeting',
            '/Konferenzschaltung',
            '/Audiokonferenz',
            '/Conference-Calls',
            '/Webkonferenz'
        ]
    };

    // Gets the current page name without the trailing slash
    var currentPage = window.location.pathname.replace(/\/+$/,'');

    // If we are on one of the BingTrackingRequired, then execute tracking
    if (jQuery.inArray(currentPage, trackingRequired[COUNTRY_CODE]) !== -1) {
        try {
            Bing_tracker(93224);
            return true;
        } catch (err) {
            return false;
        }
    }

    return false;
}


/**
 * Bing MS AdCenter Tracker Function
 * This function is used to bypass async ajax tracking, since Bing does not provide async tracking
 * @param int actionId - This is the Bing Unique Tracking Number. It Can be found within the script they provide
 */
function Bing_tracker(actionId) {
    var domainId = "48000";
    var uniqueId = "938d06d1-25fd-442b-a1fe-01530e5aa63a";

    var bing1 = document.createElement("script");
    bing1.type = "text/javascript";
    bing1.text = " if (!window.mstag) mstag = {loadTag : function(){},time : (new Date()).getTime()};";
 
    var bing2 = document.createElement("script");
    bing2.id = "mstag_tops";
    bing2.type = "text/javascript";
    bing2.src = "//flex.atdmt.com/mstag/site/"+uniqueId+"/mstag.js";

    var bing3 = document.createElement("script");
    bing3.type = "text/javascript";
    bing3.text = ' mstag.loadTag("analytics", {dedup:"1",domainId:"'+domainId+'",type:"1",actionid:"'+actionId+'"})';

    var bingnsiframe = document.createElement("iframe");
    bingnsiframe.src = "//flex.atdmt.com/mstag/tag/"+uniqueId+"/analytics.html?dedup=1&domainId="+domainId+"&type=1&actionid="+actionId;
    bingnsiframe.frameborder = "0";
    bingnsiframe.scrolling = "no";
    bingnsiframe.width = "1";
    bingnsiframe.height = "1";
    bingnsiframe.style.visibility = "hidden";
    bingnsiframe.style.display = "none";

    var bingns = document.createElement("noscript");
    bingns.appendChild(bingnsiframe);

    document.body.appendChild(bing1);
    document.body.appendChild(bing2);
    document.body.appendChild(bing3);
    document.body.appendChild(bingns);
}

/**
 * Mediacom Tracker Function
 * @param string url - This is the Mediacom Unique Tracking Number. It Can be found within the script they provide
 */
function login_mediacom_tracking(url) {
    var turn_client_track_id = "";
    var url_init = "http://r.turn.com/server/beacon_call.js?";

    var ele = document.createElement("script");
    ele.type = "text/javascript";
    ele.src = url_init + url;

    var eleImg = document.createElement("img");
    eleImg.border = "0";
    eleImg.src = url_init + url + "&cid=";

    try {
        $('document').append(ele);
    } catch(e) {

    }

    try {
        $('document').append('<noscript>').html(eleImg);
    } catch(e) {

    }    
}

/**
 * @deprecated
 * @param firstName
 */
function logUserIn(firstName) {
    console.warn('logUserIn in now deprecated');
}

/**
 * @deprecated
 */
function logUserOut() {
    console.warn('logUserOut is now deprecated');
}

function SetCookie(name, value) {
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;

    //Generate the cookie string in "name=value" format
    var curCookie = name + "=" + value +
      ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
      ((path == null) ? "" : ("; path=" + path)) +
      ((domain == null) ? "" : ("; domain=" + domain)) +
      ((secure == true) ? "; secure" : "");

    // Saves Name Value pair in memory or text file 
    document.cookie = curCookie;
}

function GnrtExprDt(duration) {
    // duration = number of days cookie lives; expire immediately = -1; 
    var today = new Date();
    var expr = new Date();
    expr.setTime(today.getTime() + duration*24*60*60*1000);
    return  expr.toGMTString();
}

function StoreInfo(CkNm,val,ExpVlu) {
    var expd = new Date(GnrtExprDt(ExpVlu));
    SetCookie(CkNm, val, expd);
}

/**
 * Initial Plus Registration - Via Symfony
 */
function form_Plus_Registration_Init() {
   $('#frm-plus-registration').ajaxForm({
      success:      form_Plus_Registration_Response,
      error:        formCommunicationError
   });
}

/**
 * Initial Plus Registration - Via Symfony
 */
function form_Plus_Registration_Response(responseText) {
   responseArr = eval('(' + responseText + ')');

   if ((responseArr['error_messages'] instanceof Array) && responseArr['error_messages'].length > 0) {
      return showInputErrorsBoxes(responseArr['error_messages'], 'frm-plus-registration', true);
   }
   
   $('#frm-plus-registration-response-placeholder').html(eval('(' + responseArr['html'] + ')'));
   return true;
}

/**
 * Print Button to print the Full Datatable of the given table
 */
function data_table_print_full (tableName) {
    $('.print-full-data-table').click(function() {
        var oTable = $(tableName).dataTable();
        var oSettings = oTable.fnSettings();
        var iInitLength = oSettings._iDisplayLength;
        oTable.fnLengthChange( -1 );
        window.print();
        oTable.fnLengthChange( iInitLength );
        return false;
    });
}

// generic tool-tip
$(function () {
    $('.pwn-tooltip').hover(
        function () {
            if ($('#pwn-tooltip').length === 0) {
                var title = $(this).attr('title');
                $(this).attr('title', '');
                $(this).append('<div id="pwn-tooltip"><span></span>' + title + '</div>');
            }
        }, 
        function () {
            var title = $('#pwn-tooltip').html().replace('<span></span>', '');
            $(this).attr('title', title);
            $('#pwn-tooltip').remove();
        }
    );
});

/*pwn-modal Function for the Product Pages*/
$(function () {
    $('body').delegate(".pwn-modal", "click", function () {
        $.get($(this).attr('href'), function (response) {
            if ($('#pwn-modal-window').length > 0) {
                $('#pwn-modal-window').remove();
            }
            $('body').append('<div id="pwn-modal-window" class="ui-corner-all" style="display: none"></div>');

            $('#pwn-modal-window').html(response);
            $('#pwn-modal-window').append('<a class="floatright sprite tooltip-close" href="#" style="text-decoration: none"><span class="sprite-cross-white png"></span></a>');

            $('#pwn-modal-window').find(".tooltip-close").click(function () {
                $("#pwn-modal-window").dialog('close');
                return false;
            });

            $('#pwn-modal-window').dialog({
                modal: true,
                dialogClass: 'no-title',
                closeOnEscape: true,
                width: 940,
                minHeight: 630
            }).scrollTop(0);
        });
        return false;
    });
});

/*Whoson Chat Window*/
$(document).ready(function () {
    $('.chatlink').click(function () {
        openChatWindow();
        return false;
    });
});

function openChatWindow() {
    var window_width = 484,
        window_height = 361,
        page_url,
        page_title = 'LiveChat'; // Must be 1 Word. Fails on IE Family when more than 1 Word is used.
    var center_left = (screen.width / 2) - (window_width / 2),
        center_top = (screen.height / 2) - (window_height / 2);

    // Temporary Fix. IE7 does not like the https certificate.
    if ($.browser.msie && parseInt($.browser.version, 10) === 7) {
        page_url = 'http://hosted3.whoson.com/chat/chatstart.aspx?domain=www.powwownow.co.uk&theme=Powwownow';
    } else {
        page_url = 'https://hosted3.whoson.com/chat/chatstart.aspx?domain=www.powwownow.co.uk&theme=Powwownow';
    }

    window.open(page_url, page_title, 'alwaysRaised=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + window_width + ', height=' + window_height + ', top=' + center_top + ', left=' + center_left);
    return false;
}
