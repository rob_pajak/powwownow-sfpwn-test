/**
 * This is used to store Tool Tip Related Methods
 */
var tooltips = {};

/**
 * Create the Tooltip Functionality
 *
 * @param toolTipInfo - Multidimensional Object Describing the Tooltip
 *    toolTipInfo.xxx.id    - The Unique Tooltip ID [Required]
 *    toolTipInfo.xxx.link  - The Unique Tooltip Link, to Show the Tooltip [Default: id-link]
 *    toolTipInfo.xxx.close - The Unique Tooltip Link, to Close the Tooltip [Default: id-close]
 *    toolTipInfo.xxx.onClick - Show on 'onClick' ? [Default: true]
 *    toolTipInfo.xxx.onHover - Show on 'onHover' ? [Default: false]
 */
tooltips.initTooltip = function (toolTipInfo) {
    if (toolTipInfo instanceof Object) {
        var x;
        for (x in toolTipInfo) {
            // Check Required Fields
            if (!("id" in toolTipInfo[x])) {
                continue;
            }

            // Additional Fields Check
            var id = toolTipInfo[x]['id'],
                link = ("link" in toolTipInfo[x]) ? toolTipInfo[x]['link'] : toolTipInfo[x]['id']+'-link',
                close = ("close" in toolTipInfo[x]) ? toolTipInfo[x]['close'] : toolTipInfo[x]['id']+'-close',
                onClick = ("onClick" in toolTipInfo[x]) ? toolTipInfo[x]['onClick'] : true,
                onHover = ("onHover" in toolTipInfo[x]) ? toolTipInfo[x]['onHover'] : false;

            // Setup Functionality
            if (onClick === true) {
                $('#'+link).click(function(){
                    $('#'+id).show('fast');
                });
            }

            if (onHover === true) {
                $('#'+link).hover(function(){
                    $('#'+id).show('fast');
                },function(){
                    $('#'+id).hide('fast');
                });
            }

            $('#'+close).click(function(){
                $('#'+id).hide('fast');
            });
        }
    }
};
