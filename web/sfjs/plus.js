/**
 * Plus Product Pages
 */
$(function () {
    // Enable Product Behaviour
    $('table#products input[type=checkbox]').click(function () {
        if ($(this).attr('checked') !== undefined) {
            $(this).parent('td').parent('tr').removeClass('product-disabled');
            $(this).parent('td').parent('tr').addClass('product-enabled');
        } else {
            $(this).parent('td').parent('tr').addClass('product-enabled');
            $(this).parent('td').parent('tr').addClass('product-disabled');
        }
    });

    // Enable Pin Product Behaviour
    $('table#pinproducts .checkall').click(function () {
        var checkVal = $(this).val() + '[]';
        if ($(this).attr('checked') !== undefined) {
            $('input[name="' + checkVal + '"]:enabled').attr("checked", true);
        } else {
            $('input[name="' + checkVal + '"]:enabled').attr("checked", false);
        }
    });

    //Enable Pin Product Submit Behaviour
    $('.pinproductssubmit').click(function () {
        $('.message.success').remove();
    });

    $(document).on('click', 'div.product-name', function () {
        $('.tooltip').hide(); // Hide All Tooltips
        $(this).parent('td,div.bwm-prod-wrapper').find('.tooltip').show(); // show current one
    });

    $(document).on('click', '.tooltip-close', function () {
        $(this).parent('div.tooltip').hide();
        return false;
    });
});

/**
 * Plus Switch
 */
$(function () {
    /**
     * Start the Plus Switch Process
     * @param plusElement
     */
    function initPlusSwitch(plusElement) {
        var switch2PlusElement = $('#switch2plus'),
            data = {};

        // Check if Element Exists
        if (switch2PlusElement.length === 0) {
            $('body').append('<div id="switch2plus" class="ui-corner-all"></div>');
            switch2PlusElement = $('#switch2plus');
        }

        // Promo Email
        if (window.location.pathname.indexOf("plus-switch") >= 0) {
            var emailName = window.location.pathname.split('/');
            if (3 === emailName.length) {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                emailName[2] = decodeURIComponent(emailName[2]); // The URI from the email will be decoded
                if (regex.test(emailName[2])) {
                    data.promoemail = emailName[2];
                }
            }
        }

        // Tracking Name
        data.trackingName = plusElement.attr('data-tracking-id');

        // Initial Plus Sign-up Form
        $.ajax({
            url: '/s/Plus-Signup',
            type: 'POST',
            data: data,
            success: function (responseText) {
                switch2PlusElement.html(responseText.html);
            },
            error: function () {
                switch2PlusElement.html(window[FORM_COMMUNICATION_ERROR]);
            }
        });

        switch2PlusElement.dialog({
            modal: true,
            dialogClass: 'no-title',
            open: function (event, ui) {
                $("body").css("overflow", "hidden")
            },
            beforeClose: function (event, ui) {
                $("body").css("overflow", "auto");
            },
            closeOnEscape: true,
            width: 650,
            minHeight: 200,
            closeText: 'hide'
        });

        switch2PlusElement.parent().css({
            'top': '200px',
            'z-index': 9999
        });

        var scrollTop = switch2PlusElement.offset().top - 100;
        $('html, body').animate({
            scrollTop: scrollTop
        }, 200);
    }

    /**
     * Trigger with the 'get-plus' Class to obtain the Plus Popup
     */
    $('body').on('click', '.get-plus', function () {
        var plusElement = $(this);

        // If Data Variable Exists, Redirect instead of showing popup
        if (typeof plusElement.data('redirect') !== 'undefined') {
            window.location = plusElement.data('redirect');
        } else {
            // Load Plus
            if (typeof jQuery.ui === 'undefined') {
                $.getScript('/shared/jQuery/jquery-ui-1.8.7.custom.min.js', function () {
                    $("<link/>", {
                        rel: "stylesheet",
                        type: "text/css",
                        href: "/sfcss/vendor/jQuery/jquery-ui-1.8.7.custom.css"
                    }).appendTo("head");
                    initPlusSwitch(plusElement);
                });
            } else {
                initPlusSwitch(plusElement);
            }
        }
    });
});
