/**
 * Trigger with the 'goto-registration-free' Class to Animate, Shake and Select the Register Email Box
 */
$('body').on('click', '.goto-registration-free', function (e) {
    e.preventDefault();
    $('html,body').animate({scrollTop: $(".how-it-works-box").offset().top}, function () {
        if ($(this).is('.ie8')) {
            $('form.ko_registration_form input[type=email]').select();
        } else {
            $(".register_box").effect("shake", {times: 1}, 300, function () {
                $('form.ko_registration_form input[type=email]').select();
            });
        }
    });
});
