/**
 * Web Conferencing Tabs Initialisation
 * @param formId
 */
function webConferencingTabs_Init(formId) {

    // Tab Positions
    tabs_setUpTabPositions(71);

    // Tab Wiggle
    tabs_setUpHoverWiggle('1');
    tabs_setUpHoverWiggle('2');
    tabs_setUpHoverWiggle('3');

    // Tab URL Check, for Second and Third Tabs
    switch (window.location.pathname) {
        case WEB_CONFERENCING_DEMO_TAB :
            webConferenceDemoTab();
            break;
        case WEB_CONFERENCING_GET_STARTED_TAB :
            webConferenceGetStartedTab();
            break;
    }

    // Tab Clicks

    // Showtime TAB
    $('#tab-1').click(webConferenceShowtimeTab);

    // Demo TAB
    $('#tab-2').click(webConferenceDemoTab);

    // Get started TAB
    $('#tab-3').click(webConferenceGetStartedTab);

    // Sets up showtime in action tooltip
    $('.tooltip-showtime-in-action-close').click(function () {
        $('#tooltip-showtime-in-action').toggle('fast');
        return false;
    });
    $('.tooltip-showtime-in-action-link').click(function () {
        $('#tooltip-showtime-in-action').toggle('fast');
        return false;
    });
}

/**
 * Web Conferencing Form Initialisation
 * @param formId
 */
function webConferencingFormInit(formId) {
    console.warn('webConferencingFormInit is now deprecated.');
}

/**
 * Web-Conferencing Showtime Tab
 */
function webConferenceShowtimeTab() {
    showContentForWebAndVideoConferencingTags(1);
}

/**
 * Web-Conferencing Demo Tab
 */
function webConferenceDemoTab() {
    showContentForWebAndVideoConferencingTags(2);
    tabs_addCanonicalLink(WEB_CONFERENCING_CANONICAL_URL);
}

/**
 * Web-Conferencing Get-Started Tab
 */
function webConferenceGetStartedTab() {
    showContentForWebAndVideoConferencingTags(3);
    tabs_addCanonicalLink(WEB_CONFERENCING_CANONICAL_URL);
}
