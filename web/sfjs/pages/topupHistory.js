function loadPlusTopUpHistoryForm() {
    $('#frm-plus-topup-history').initMypwnForm({
        successMessagePos: 'above',
        debug: 'on',
        html5Validation: 'off',
        requiredAsteriks: 'on',
        beforeSubmit: function () {
            var sd = $('#plustopuphistory_topup_start_date').val().split("-");
            var ed = $('#plustopuphistory_topup_end_date').val().split("-");

            var startDate = new Date(sd[2] + '-' + sd[1] + '-' + sd[0]);
            var endDate = new Date(ed[2] + '-' + ed[1] + '-' + ed[0]);

            if (startDate > endDate) {
                var errors = [];
                errors.push({'message': FORM_VALIDATION_INVALID_DATE_RANGE, 'field_name': 'plustopuphistory[topup_start_date]'});
                showInputErrorsBoxes(errors, 'frm-plus-topup-history');
                return false;
            }

            return true;
        },
        success: function (responseText) {
            oTableScreen.fnClearTable();
            oTableScreen.fnAddData(responseText.tableData);
            oTableScreen.fnSort([
                [0, 'desc']
            ]);

            oTablePrint.fnClearTable();
            oTablePrint.fnAddData(responseText.tableData);
            oTablePrint.fnSort([
                [0, 'desc']
            ]);
        }
    });
}

function loadPlusTopUpHistoryTable(aaData) {
    // Set the Languages
    var oLanguage = {
        "sProcessing": DATA_TABLES_sProcessing,
        "sLengthMenu": DATA_TABLES_sLengthMenu,
        "sZeroRecords": DATA_TABLES_sZeroRecords,
        "sEmptyTable": DATA_TABLES_sEmptyTable,
        "sLoadingRecords": DATA_TABLES_sLoadingRecords,
        "sInfo": DATA_TABLES_sInfo,
        "sInfoEmpty": DATA_TABLES_sInfoEmpty,
        "sInfoFiltered": DATA_TABLES_sInfoFiltered,
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sSearch": DATA_TABLES_sSearch,
        "sUrl": "",
        "fnInfoCallback": null,
        "oPaginate": {
            "sFirst": DATA_TABLES_sFirst,
            "sPrevious": DATA_TABLES_sPrevious,
            "sNext": DATA_TABLES_sNext,
            "sLast": DATA_TABLES_sLast
        }
    };

    // Set the Column Headings
    var aoColumns = [

        { "sTitle": "Date" },
        { "sTitle": "Order Number" },
        { "sTitle": "Transaction Type" },
        { "sTitle": "Amount" },
        { "sTitle": "Status" },
        { "sTitle": "Actions" }

    ];

    oTableScreen = $('#topup_history_screen').dataTable({
        "aaData": aaData,
        "aoColumns": aoColumns,
        "oLanguage": oLanguage,
        "sPaginationType": "full_numbers"
    });

    oTableScreen.fnSort([
        [0, 'desc']
    ]);

    oTablePrint = $('#topup_history_print').dataTable({
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "aaData": aaData,
        "aoColumns": aoColumns,
        "oLanguage": oLanguage
    });

    oTablePrint.fnSort([
        [0, 'desc']
    ]);

    $('#topup_history th')
        .append('<div></div>')
        .css('text-decoration', 'underline')
        .css('cursor', 'pointer');
}

function viewTopupDetails(transactionId) {
    var transactionElement = $("#transaction");
    var topupInfoElement = $('#topup_info');
    transactionElement.attr('title', 'Powwownow Plus: Transaction Details');

    $.ajax({
        url: 'account/topupDetail/transaction/' + transactionId,
        success: function (data) {
            var transaction = $.parseJSON(data);
            $('#transaction_title').html('Order No.' + transactionId);
            $('#table_purchased_by').html(transaction.organisation);
            $('#table_transaction_date').html(transaction.payment_time);
            $('#table_transaction_status').html(transaction.transaction_status);
            $('#table_topup_method').html(transaction.transaction_type === 'Manual Topup' ? 'Product Purchase' : transaction.transaction_type);
            $('#table_topup_amount').html('£' + transaction.payment_amount);
            $('#table_topup_vat_rate').html('VAT ' + (transaction.vat_percent * 100) + '%');
            $('#table_topup_vat_amount').html('£' + transaction.vat_value);
            $('#table_topup_total').html('£' + transaction.payment_amount_post);
            $('#invoice-link').attr('href', '/s/Show-Invoice' + '/' + transactionId).attr('target', '_blank');
            $('#table_invoice_date').text(transaction.payment_period);

            if (transaction.transaction_type === 'Manual Topup') {
                topupInfoElement.addClass('manual-invoice').removeClass('auto-invoice');
            } else {
                topupInfoElement.addClass('auto-invoice').removeClass('manual-invoice');
            }

            var buttons = { "Close": function () {
                $(this).dialog("close");
            } };

            if ((transaction.invoice_id != null) && (transaction.transaction_status == 'Transaction Successful')) {
                buttons["Download Invoice"] = function () {
                    window.open('/s/Show-Invoice' + '/' + transactionId)
                };
            }

            transactionElement.dialog({
                modal: true,
                width: 650
            });

            transactionElement.dialog("option", "buttons", buttons);
        },
        error: function () {
            $("#transaction").html(FORM_COMMUNICATION_ERROR);
        }
    });
}

