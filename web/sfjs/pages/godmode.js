/*global $, _*/

/**
 * @constructor
 */
function godLogin () {
    this.forms = {
        $form1 : $('#form-god-mode'),
        $form2 : $('#form-god-mode-hidden')
    };

    this.required = {
        $password: $('#god_mode_password')
    };

    this.selectors = {
        $country : $("#country_name"),
        $password : $('#god_mode_password')
    };

    this.initialize();
}

// Easy access to prototype
var proto = godLogin.prototype;

/**
 *
 */
proto.initialize = function initialize () {
    var _this = this;
    this.forms.$form1.on('submit', function(event) {
        event.preventDefault();
        _this._doFormSubmission();
    });
};

/**
 *
 * @type {Pwn.Utilities.getFormPostUrl}
 * @private
 */
proto._getFormUrl = Pwn.Utilities.getFormPostUrl

/**
 *
 * @param errorResponse
 * @returns {boolean}
 * @private
 */
proto._renderErrorsToUi = Pwn.Utilities.renderErrorsToUiLegacy

/**
 *
 * @private
 */
proto._doFormSubmission = function _doFormSubmission () {
    var _this = this,
        formPostUrl = _this._getFormUrl($('#form-god-mode')),
        formData = _this.forms.$form1.serialize();
    $.ajax({
        type: 'POST',
        url: formPostUrl,
        dataType: 'json',
        data: formData,
        beforeSend:function(){
            if (!_this.required.$password.val()) {
                alert("Please Enter a password.");
                return false;
            }
        },
        success:function(response){
            var selectedCountry;
            try {
                selectedCountry = _this.selectors.$country.find("option:selected").val();
            } catch (e) {
                selectedCountry = 'GBR';
            }
            if (selectedCountry === 'GBR') {
                window.location = response.redirect;
            } else {
                var password = _this.forms.$form1.find('#god_mode_password').val();
                _this.forms.$form2.attr('action', response.redirect + 'login/dogodmodelogin');
                _this.forms.$form2.find('#god_mode_password').val(password);
                _this.forms.$form2.submit();
            }
        },
        error:function(jqXHR) {
            var errorResponse;
            try {
                errorResponse = $.parseJSON(jqXHR.responseText);
            } catch(e) {
                errorResponse = {};
            } finally {
                _this._renderErrorsToUi(errorResponse);
            }
        },
        complete: function() {}
    });
};

(function($) {
    'use strict';
    $(function() {

        // List Setup
        if ($.fn.msDropdown) {
            $("#country_name").msDropdown();
        }
        var God = new godLogin();
    });
})(window.jQuery);