// Create User Form Functionality
function createUserFormInitialise(formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var message = utils.obtainMessage(responseArr,'success'),
                createUserSuccess = $('#createUser-success');
            $('button#form-create-user-submit').attr('disabled','disabled');
            createUserSuccess.html(message.message);
            createUserSuccess.dialog({
                height: 140,
                modal: true,
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        if (window[URL_MYPWN_USERS] !== undefined) {
                            window.location.replace(window[URL_MYPWN_USERS]);
                        } else if (typeof URL_MYPWN_USERS !== 'undefined') {
                            window.location.replace(URL_MYPWN_USERS);
                        }
                    }
                }
            });
        },
        error: function (responseArr) {
            $('#plus_invite').click(function () {
                $.ajax({
                    type: "POST",
                    url: '/s/invite/send',
                    data: {email: $('#createUser_email').val()},
                    success: function(){
                        alert(FORM_CREATE_USER_INVITATION_SUCCESS);
                        $('.nice-error-box').hide();
                        $('#form-create-user').resetForm();
                    }
                });
            });

            // Display error alert message.
            // Not sure why PwnForm is not displaying it.
            if (responseArr.responseText && responseArr.responseText.constructor === String) {
                var response = $.parseJSON(responseArr.responseText);
                if ($.isArray(response.error_messages) && response.error_messages.length && response.error_messages[0].field_name === 'alert') {
                    alert(response.error_messages[0].message);
                }
            }
        }
    });
}
