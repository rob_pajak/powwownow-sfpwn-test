// PINs Form Functionality
function pinsFormInitialise(formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var message = utils.obtainMessage(responseArr, 'success');
            utils.outputMessage('#frm-myPINs', message, 'success', false, true, false);
            // $('#pins-success').html(message.message);
            // $('#pins-success').dialog({
            //     height: 140,
            //     modal: true,
            //     buttons: {
            //         Ok: function() {
            //             $(this).dialog("close");
            //             window.location.reload();
            //         }
            //     }
            // });
        },
        error: function (responseArr) {
            var responseText = $.parseJSON(responseArr.responseText);
            if ("error_messages" in responseText) {
                if ("field_name" in responseText.error_messages) {
                    utils.outputMessage('#frm-myPINs', responseText.error_messages, 'error', false, true, false);
                } else if ("field_name" in responseText.error_messages[0]) {
                    utils.outputMessage('#frm-myPINs', responseText.error_messages[0], 'error', false, true, false);
                }
            }
        }
    });
}

// PINs Toggle Checkboxes
function pinsToggleCheckbox(toggleState) {
    $('#frm-myPINs').find('input[name="r[]"]').each(function () {
        $(this).prop('checked', toggleState);
    });
}
