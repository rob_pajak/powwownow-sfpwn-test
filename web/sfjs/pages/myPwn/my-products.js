$(document).ready(function () {

    $(".product-accordion").accordion({ autoHeight: false });

    $('.bwm-name-form').each(function(i,obj){
        var formId = $(obj).attr("id");
        myPwnApp.bwm.changeName();
        myPwnApp.bwm.changeName.initAjaxForm(formId);
    });

    $('.my-products #products .bundle-information').click(function() {
        var $trigger = $(this);
        if ($trigger.data('dialog-id')) {
            $($trigger.data('dialog-id')).dialog('open');
        } else {
            var $dialogContents = $trigger.parents('.product-desc').find('.bundle-product-dialog');
            $dialogContents.dialog({
                minWidth: 750,
                modal: true,
                closeOnEscape: true,
                width: 940,
                minHeight: 630
            });
            $trigger.data('dialog-id', '#' + $dialogContents.attr('id'));
        }
    });
    $('.bundle-product-dialog .close-control a').click(function(event) {
        event.preventDefault();
        $(this).parents('.bundle-product-dialog').dialog('close');
    });

});
