$(document).ready(function () {
    var $bundleDialog;

    $('.bundle-modal').click(function(event) {
        event.preventDefault();

        if ($bundleDialog) {
            $bundleDialog.dialog('open');
        } else {
            var $trigger = $(this);
            $bundleDialog = $trigger.parents('.stand-out-banner').find('.bundle-product-dialog');
            $bundleDialog.dialog({
                minWidth: 750,
                modal: true,
                closeOnEscape: true,
                width: 940,
                minHeight: 630,
                resizable: false
            });
        }
    });
    $('.bundle-product-dialog .close-control a').click(function(event) {
        event.preventDefault();
        $bundleDialog.dialog('close');
    });
});
