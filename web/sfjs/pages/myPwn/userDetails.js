// User Details Form Functionality
function userDetailsFormInitialise(userDetailsUserFormId, userDetailsPasswordFormId) {
    $(userDetailsUserFormId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'off',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        beforeSubmit: function () {
            var error = [],
                fNameField = $('#contact_first_name'),
                lNameField = $('#contact_last_name');

            if (fNameField.val().length < 1) {
                error.push({'message': "FORM_VALIDATION_NO_FIRST_NAME", 'field_name': 'contact[first_name]'});
            } else if (lNameField.val().length < 1) {
                error.push({'message': "FORM_VALIDATION_NO_SURNAME", 'field_name': 'contact[last_name]'});
            }

            if (error.length > 0) {
                $.showInputErrors(error, '', '', '#frm_user_details_user_details');
                return false;
            }

            return true;
        },
        success: function (responseArr) {
            utils.outputMessage('#frm_user_details_user_details', $.parseJSON(responseArr), 'success', false, false, false);
        },
        error: function (responseArr) {
            var message = utils.obtainMessage(responseArr, 'error');
            utils.outputMessage('#frm_user_details_user_details', message, 'error', false, false, false);
        }
    });

    $(userDetailsPasswordFormId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'off',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        beforeSubmit: function () {
            var error = [],
                passField = $('#authentication_password'),
                confPassField = $('#authentication_confirm_password');

            if (passField.val().length == 0) {
                error.push({'message': FORM_VALIDATION_NO_PASSWORD, 'field_name': 'authentication[password]'});
            } else if (passField.val().length < 6) {
                error.push({'message': FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS, 'field_name': 'authentication[password]'});
            } else if (passField.val().length > 50) {
                error.push({'message': FORM_VALIDATION_PASSWORD_TOO_LONG, 'field_name': 'authentication[password]'});
            } else if (confPassField.val().length == 0) {
                error.push({'message': FORM_VALIDATION_NO_PASSWORD, 'field_name': 'authentication[confirm_password]'});
            } else if (confPassField.val().length < 6) {
                error.push({'message': FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS, 'field_name': 'authentication[confirm_password]'});
            } else if (confPassField.val().length > 50) {
                error.push({'message': FORM_VALIDATION_PASSWORD_TOO_LONG, 'field_name': 'authentication[confirm_password]'});
            } else if (passField.val() != confPassField.val()) {
                error.push({'message': FORM_VALIDATION_PASSWORD_MISMATCH, 'field_name': 'authentication[confirm_password]'});
            }

            if (error.length > 0) {
                $.showInputErrors(error, '', '', '#frm_user_details_user_password');
                return false;
            }

            return true;
        },
        success: function (responseArr) {
            utils.outputMessage('#frm_user_details_user_password', $.parseJSON(responseArr), 'success', false, false, false);
        },
        error: function (responseArr) {
            var message = myPwnApp.utils.obtainMessage(responseArr, 'error');
            utils.outputMessage('#frm_user_details_user_password', message, 'error', false, false, false);
        }
    });

    $('#email-change-info').click(function () {
        $('.frm_user_details_user_details_results_placeholder1').html('<div class="message warning">' + FORM_VALIDATION_CHANGE_EMAIL_ERROR + '</div>');
        $('.message').show().delay(10000).fadeOut('slow');
    });
}
