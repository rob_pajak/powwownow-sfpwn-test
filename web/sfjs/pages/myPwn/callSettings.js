/**
 * Call Settings Javascript Functions. Used on the /myPwn/Call-Settings page.
 */

/**
 * Call Settings Form Initialisation
 * @param formId - The Form ID
 * @param showConfirm - Do you want to show confirmation box, before submission?
 */
function callSettingsFormInit(formId, showConfirm) {
    var options = {
            successMessagePos: 'off',
            debug: 'on',
            html5Validation: 'off',
            requiredAsterisk: 'off',
            success: function (responseArr) {
                var message = myPwnApp.utils.obtainMessage(responseArr, 'success');
                myPwnApp.utils.outputMessage('#frm-call-settings', message, 'success', false, true);
            },
            error: function (responseArr) {
                var message = myPwnApp.utils.obtainMessage(responseArr, 'error');
                myPwnApp.utils.outputMessage('#frm-call-settings', message, 'error', false, false);
            }
        },
        entryExitAnnouncementResponse = $('.entry_exit_announcement_response'),
        entryExitAnnouncementTypeResponse = $('.entry_exit_announcement_type_response'),
        chairmanPresentInfoResponse = $('.chairman_present_info_response'),
        chairmanOnlyResponse = $('.chairman_only_response');

    if (showConfirm) {
        options.beforeSubmit = function () {
            $('div.info-message').parent().hide();
            return confirm(MYPWN_CALLSETTINGS_CONFIRM);
        };
    }

    $(formId).initMypwnForm(options);

    entryExitAnnouncementResponse.hide();
    entryExitAnnouncementTypeResponse.hide();
    chairmanPresentInfoResponse.hide();
    chairmanOnlyResponse.hide();

    $('#entry_exit_announcement-info').click(function () {
        entryExitAnnouncementResponse.hide();
        entryExitAnnouncementTypeResponse.hide();
        chairmanPresentInfoResponse.hide();
        chairmanOnlyResponse.hide();
        entryExitAnnouncementResponse.show().delay(20000).fadeOut('slow');
    });
    $('#entry_exit_announcement_type-info').click(function () {
        entryExitAnnouncementResponse.hide();
        entryExitAnnouncementTypeResponse.hide();
        chairmanPresentInfoResponse.hide();
        chairmanOnlyResponse.hide();
        entryExitAnnouncementTypeResponse.show().delay(10000).fadeOut('slow');
    });
    $('#chairman_present-info').click(function () {
        entryExitAnnouncementResponse.hide();
        entryExitAnnouncementTypeResponse.hide();
        chairmanPresentInfoResponse.hide();
        chairmanOnlyResponse.hide();
        chairmanPresentInfoResponse.show().delay(10000).fadeOut('slow');
    });
    $('#chairman_only-info').click(function () {
        entryExitAnnouncementResponse.hide();
        entryExitAnnouncementTypeResponse.hide();
        chairmanPresentInfoResponse.hide();
        chairmanOnlyResponse.hide();
        chairmanOnlyResponse.show().delay(10000).fadeOut('slow');
    });
}

/**
 * Call Settings - Music on Hold HTML5 Audio Player
 * @param id - Music on hold id.mp3 file name
 * @param autoPlay - Do you want to auto play
 */
function callSettingsSetupMusicOnHold(id, autoPlay) {
    var $element = $('div#player_span');
    switch (id) {
        case '':
        case '2160': // Mute file
            $element.html('');
            break;
        case '-1': // Shuffle
            $element.html('* A different song will play each time you make a call.');
            break;
        default:
            if (!$('.lt-ie9').length) {
                $element.html('<audio style="width:207px;" preload="none" src="/myPwn/moh/'+ id +'.mp3" type="audio/mp3" controls="controls"></audio>');
                $('audio').mediaelementplayer();
            } else {
                $element.html('');
            }
    }
}
