var oTableScreen = '',
    oTablePrint = '';

// TopUp History Form Initialisation
function topUpHistoryFormInitialise(formId) {
    if (!Modernizr.touch || !Modernizr.inputtypes.date) {
        $('input[type=date]').datepicker({
            dateFormat: 'yy-mm-dd',
            minDate:    $(this).data('min'),
            maxDate:    $(this).data('max')
        });
    }

    $(formId).initMypwnForm({
        successMessagePos: 'above',
        debug: 'on',
        html5Validation: 'off',
        requiredAsteriks: 'on',
        beforeSubmit: function () {
            // Due to the Stupid Differences between Browsers, the datePicker is funky between browsers
            var startDateArr = $('#plustopuphistory_topup_start_date').val().split("-"),
                endDateArr = $('#plustopuphistory_topup_end_date').val().split("-"),
                startDate,
                endDate;

            if (startDateArr[0].length === 4) {
                startDate = new Date(startDateArr[0] + '-' + startDateArr[1] + '-' + startDateArr[2]);
            } else {
                startDate = new Date(startDateArr[2] + '-' + startDateArr[1] + '-' + startDateArr[1]);
            }

            if (endDateArr[0].length === 4) {
                endDate = new Date(endDateArr[0] + '-' + endDateArr[1] + '-' + endDateArr[2]);
            } else {
                endDate = new Date(endDateArr[2] + '-' + endDateArr[1] + '-' + endDateArr[0]);
            }

            if (startDate > endDate) {
                var errors = [];
                errors.push({
                    'message': FORM_VALIDATION_INVALID_DATE_RANGE,
                    'field_name': 'plustopuphistory[topup_start_date]'
                });
                showInputErrorsBoxes(errors, 'frm-plus-topup-history');
                return false;
            }
            return true;
        },
        success: function (responseText) {
            oTableScreen.fnClearTable();
            oTableScreen.fnAddData(responseText.tableData);
            oTableScreen.fnSort([
                [0, 'desc']
            ]);

            oTablePrint.fnClearTable();
            oTablePrint.fnAddData(responseText.tableData);
            oTablePrint.fnSort([
                [0, 'desc']
            ]);
        }
    });
}

function topUpHistoryDataTableInitialise(tableData) {
    // Set the Languages
    var oLanguage = {
        "sProcessing": DATA_TABLES_sProcessing,
        "sLengthMenu": DATA_TABLES_sLengthMenu,
        "sZeroRecords": DATA_TABLES_sZeroRecords,
        "sEmptyTable": DATA_TABLES_sEmptyTable,
        "sLoadingRecords": DATA_TABLES_sLoadingRecords,
        "sInfo": DATA_TABLES_sInfo,
        "sInfoEmpty": DATA_TABLES_sInfoEmpty,
        "sInfoFiltered": DATA_TABLES_sInfoFiltered,
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sSearch": DATA_TABLES_sSearch,
        "sUrl": "",
        "fnInfoCallback": null,
        "oPaginate": {
        "sFirst": DATA_TABLES_sFirst,
        "sPrevious": DATA_TABLES_sPrevious,
        "sNext": DATA_TABLES_sNext,
        "sLast": DATA_TABLES_sLast
        }
    };

    // Set the Column Headings
    var aoColumns = [
        { "sTitle": "Date" },
        { "sTitle": "Order Number" },
        { "sTitle": "Transaction Type" },
        { "sTitle": "Amount" },
        { "sTitle": "Status" },
        { "sTitle": "Actions" }
    ];

    // Data to be Displayed
    var aaData;
    aaData = tableData;

    oTableScreen = $('#topup_history_screen').dataTable({
        "aaData": aaData,
        "aoColumns": aoColumns,
        "oLanguage": oLanguage,
        "sPaginationType": "full_numbers"
    });

    oTableScreen.fnSort([
        [0, 'desc']
    ]);

    oTablePrint = $('#topup_history_print').dataTable({
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "aaData": aaData,
        "aoColumns": aoColumns,
        "oLanguage": oLanguage
    });

    oTablePrint.fnSort([
        [0, 'desc']
    ]);

    $('#topup_history').find('th')
        .append('<div></div>')
        .css('text-decoration', 'underline')
        .css('cursor', 'pointer');
}

function topUpHistoryViewTopUpDetails(transactionId, topUpDetailsUrl, accountInvoiceUrl) {
    var transactionElement = $("#transaction"),
        topUpInfoElement = $('#topup_info');

    transactionElement.attr('title', 'Powwownow Plus: Transaction Details');

    $.ajax({
        type: "POST",
        url: topUpDetailsUrl + '/' + transactionId,
        success: function (data) {
            var transaction = $.parseJSON(data);
            $('#transaction_title').html('Order No.' + transactionId);
            $('#table_purchased_by').html(transaction.organisation);
            $('#table_transaction_date').html(transaction.payment_time);
            $('#table_transaction_status').html(transaction.transaction_status);
            $('#table_topup_method').html(transaction.transaction_type === 'Manual Topup' ? 'Product Purchase' : transaction.transaction_type);
            $('#table_topup_amount').html('£' + transaction.payment_amount);
            $('#table_topup_vat_rate').html('VAT ' + (transaction.vat_percent * 100) + '%');
            $('#table_topup_vat_amount').html('£' + transaction.vat_value);
            $('#table_topup_total').html('£' + transaction.payment_amount_post);
            $('#invoice-link').attr('href', accountInvoiceUrl + transactionId).attr('target', '_blank');
            $('#table_invoice_date').text(transaction.payment_period);

            if (transaction.transaction_type === 'Manual Topup') {
                topUpInfoElement.addClass('manual-invoice').removeClass('auto-invoice');
            } else {
                topUpInfoElement.addClass('auto-invoice').removeClass('manual-invoice');
            }

            var transactionElementButtons = {};
            transactionElementButtons["Close"] = function () {
                $(this).dialog("close");
            };
            if ((transaction.invoice_id != null) && (transaction.transaction_status == 'Transaction Successful')) {
                transactionElementButtons["Download Invoice"] = function () {
                    window.open(accountInvoiceUrl + '/' + transactionId)
                };
            }

            transactionElement.dialog({
                modal: true,
                width: 650
            });
            transactionElement.dialog("option", "buttons", transactionElementButtons);
        },
        error: function () {
            $("#transaction").html(FORM_COMMUNICATION_ERROR);
        }
    });
}
