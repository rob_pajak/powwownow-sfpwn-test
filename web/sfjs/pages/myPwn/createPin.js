/**
 *
 * @constructor
 */
function CreatePin() {
    this.formElement = '#form-create-pin';
    this.$newUserForm = $('#new_user_options');
    this.required = {
        $email : $('#createPin_email'),
        $firstName : $('#createPin_first_name'),
        $lastName: $('#createPin_last_name'),
        $password: $('#createPin_password'),
        $confirmPassword: $('#createPin_confirm_password'),
        $costCode : $('#createPin_cost_code')
    }

    this.bindFormEvent();
}

/**
 *
 * @type {Object}
 */
var proto = CreatePin.prototype;

/**
 *
 * @returns {*|jQuery}
 */
proto.getFormPostUrl = function getFormPostUrl () {
    return $(this.formElement).attr('action');
}

/**
 *
 * @param emailAddress
 * @returns {boolean}
 * @private
 */
proto._validateEmail = function _validateEmail (emailAddress) {
    var filter = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    return filter.test(emailAddress);
};

/**
 *
 * @returns {*}
 */
proto._doFormValidation = function doFormValidation () {

    if (this.$newUserForm.is(":visible")) {

        if (!this._validateEmail(this.required.$email.val())) {
            return {error_messages: {field_name: 'createPin[email]', message: 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS'}};
        }

        if (!this.required.$firstName.val()) {
            return {error_messages: {field_name: 'createPin[first_name]', message: 'FORM_VALIDATION_FIRST_NAME_EMPTY'}};
        }

        if (!this.required.$lastName.val()) {
            return {error_messages: {field_name: 'createPin[last_name]', message: 'FORM_VALIDATION_LAST_NAME_EMPTY'}};
        }

        if (!this.required.$password.val()) {
            return {error_messages: {field_name: 'createPin[password]', message: 'FORM_VALIDATION_NO_PASSWORD'}};
        }

        if (!this.required.$confirmPassword.val()) {
            return {error_messages: {field_name: 'createPin[confirm_password]', message: 'FORM_VALIDATION_PASSWORD_MISMATCH'}};
        }
    }
    return true;
}

/**
 *
 */
proto.bindFormEvent = function bindFormEvent() {

    var _this = this;

    // Form Submission Event
    $(this.formElement).submit(function(event){
        event.preventDefault();
        var validateResponse = _this._doFormValidation();
        _this._setCostCodeValue();
        if (validateResponse === true ) {
            _this._doRequest();
        } else {
            // FORM VALIDATION FAILED
            // RENDER ERRORS TO UI
            _this._renderErrorsToUi(validateResponse);

        }
    });

    $(this.formElement).on('click','.close', function(event){
        $(this).parent().remove();
    });

}

/**
 * @desc
 *
 * @private
 */
proto._setCostCodeValue = function _setCostCodeValue () {
    if (!this.required.$costCode.val()) {
        this.required.$costCode.val(this.required.$firstName.val() + ' ' + this.required.$lastName.val());
    }
};

/**
 *
 * @param errorResponse
 * @private
 */
proto._renderErrorsToUi = function _renderErrorsToUi(errorResponse) {

    var inputField, errorMessageToDisplay;

        if (_.isArray(errorResponse.error_messages)) {
            inputField = errorResponse.error_messages[0].field_name;
            errorMessageToDisplay = window[errorResponse.error_messages[0].message];
        } else if (_.isObject(errorResponse.error_messages)) {
            inputField = errorResponse.error_messages.field_name;
            errorMessageToDisplay = window[errorResponse.error_messages.message];
        } else {
            alert('We are currently experiencing a Technical problem.');
            return false;
        }

    $(this.formElement + ' input[name="' + inputField + '"]')
        .after('<div class="error_message">' + errorMessageToDisplay + '<a class="close" title="Close">&#215;</a></div>');
};

/**
 *
 * @private
 */
proto._doRequest = function _doRequest() {
    var _this = this;
    $.ajax({
        type: 'POST',
        url: _this.getFormPostUrl(),
        dataType: 'json',
        data: $(this.formElement).serialize(),
        beforeSend:function(){
            $('.error_message').remove();
        },
        success:function(response){
            var successMessage = '';
            try {
                successMessage = response.success_message.message;
            } catch (e) {
                successMessage = 'Your PIN has been created.'
            }

            $(_this.formElement).after(
                '<div class="success_message"><p>' + successMessage + '</p></div>');

            window.setTimeout( function(){
                window.location.replace('/myPwn/Pins');
            }, 100 );

        },
        error:function(jqXHR) {
            var errorResponse = {};

            try {
                errorResponse = $.parseJSON(jqXHR.responseText);
            } catch(e) {
                errorResponse = {};
            } finally {
                _this._renderErrorsToUi(errorResponse);
            }
        },
        complete: function() {

        }
    });
};




(function($) {

    new CreatePin();

})(window.jQuery);



// Toggle Main Form show/hide of Create Pin Page
function createPinFormToggle(element) {

    if ($(element).val() === '') {
        $('#new_user_options').show();
        $('.form-action p').show();
    } else {
        $('#new_user_options').hide();
        $('.form-action p').hide();
    }
}
