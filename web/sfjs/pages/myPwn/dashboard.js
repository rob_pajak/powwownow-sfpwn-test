// Index Dashboard Functionality
function dashboardInitialise() {
    $(".grid_hover").hover(function () {
        $(this).attr('href', $(this).prev('a').attr('href'));
        $(this).stop().animate({"opacity": "0.7"}, "fast");
    }, function () {
        $(this).attr('href', $(this).prev('a').attr('href'));
        $(this).stop().animate({"opacity": "0"}, "fast");
    });
}