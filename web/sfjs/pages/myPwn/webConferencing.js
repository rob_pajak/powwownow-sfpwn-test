// Web Conferencing Form Functionality
function webConferencingFormInitialise(formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var formResults = $('.formresults');
            if ("success_message" in responseArr) {
                $('.getstarted').hide();
                $('.downloadicons').hide();
                $('.webConferencingForm').hide();
                formResults.html(responseArr.success_message.message);
                formResults.show();
            }
        },
        error: function (responseArr) {
            var message = utils.obtainMessage(responseArr, 'error');
            utils.outputMessage('#frm-webconferencing', message, 'error', false, false, false);
        }
    });

    // Update the Filetype Hidden Field
    $('.loadWebConferencingForm').click(function () {
        $('#frm-webconferencing').find('input#webConferencing_filetype').val($(this).val());
        $('div.webConferencingForm').show();
        return false;
    });
}
