function requestWelcomePackFormInitialise(formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var responseText = $.parseJSON(responseArr);
            if ("success_message" in responseArr) {
                utils.outputMessage('#frm-request-welcome-pack', responseArr.success_message, 'success', URL_MYPWN_PINS, false, false);
            } else if ("success_message" in responseText) {
                utils.outputMessage('#frm-request-welcome-pack', responseText.success_message, 'success', URL_MYPWN_PINS, false, false);
            }
        },
        error: function (responseArr) {
            var responseText = $.parseJSON(responseArr.responseText);
            if ("error_messages" in responseText) {
                if ("field_name" in responseText.error_messages) {
                    utils.outputMessage('#frm-request-welcome-pack', responseText.error_messages, 'error', false, false, false);
                } else if ("field_name" in responseText.error_messages[0]) {
                    utils.outputMessage('#frm-request-welcome-pack', responseText.error_messages[0], 'error', false, false, false);
                }
            }
        }
    });
}

function requestWelcomePackUpdatePreview(chair_pin, part_pin, first_name, last_name) {
    // Update Names
    $('#walletCardPreview_line_name').html(first_name + ' ' + last_name);

    // Update Chair PIN
    if (chair_pin !== undefined) {
        $('#walletCardPreview_line_chairPin').show();
        $('#walletCardPreview_chairPin').html(chair_pin);
    } else {
        $('#walletCardPreview_line_chairPin').hide();
    }

    // Update Participant PIN
    if (part_pin !== undefined) {
        $('#walletCardPreview_participantPin').html(part_pin);
        $('#walletCardPreview_line_participantPin').show();
    } else {
        $('#walletCardPreview_line_participantPin').hide();
    }
}
