// Call History Form Functionality
function callHistoryFormInitialise(formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var responseText = $.parseJSON(responseArr);
            if ("success" in responseArr) {
                $('.div-callhistory').html(responseArr.success);
            } else if ("success" in responseText) {
                $('.div-callhistory').html(responseText.success);
            }
        },
        error: function (responseArr) {
            var responseText = $.parseJSON(responseArr.responseText);
            if ("error_messages" in responseText) {
                if ("field_name" in responseText.error_messages) {
                    myPwnApp.utils.outputMessage('frm-callhistory',responseText.error_messages,'error',false,false);
                } else if ("field_name" in responseText.error_messages[0]) {
                    myPwnApp.utils.outputMessage('frm-callhistory',responseText.error_messages[0],'error',false,false);
                }
            }
        }
    });

    // Download CSV Button
    $('.downloadcsv').click(function (){
        window.open('/s/Call-History-Ajax?' + $(formId).serialize() + '&frm-callhistory-csv=1', 'Download File');
        return false;
    });
}

// Call History Date Functionality
function callHistoryDateInitialise(formId) {
    var today = new Date();
    var dateOld = new Date();
    dateOld.setYear(dateOld.getYear()-1);

    // From Date
    $(formId).find('#callhistory_from_date').datepicker({
        dateFormat:'yy-mm-dd',
        defaultDate:dateOld,
        minDate:dateOld,
        maxDate:today,
        showButtonPanel:true
    });

    // To Date
    $(formId).find('#callhistory_to_date').datepicker({
        dateFormat:'yy-mm-dd',
        defaultDate:dateOld,
        minDate:dateOld,
        maxDate:today,
        showButtonPanel:true
    });
}
