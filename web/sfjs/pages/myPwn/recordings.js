// Recordings Form Functionality
function recordingsFormInitialise(formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            var responseText = $.parseJSON(responseArr),
                data         = [],
                tableID      = '#tblRecordings';
            var oTable       = $(tableID).dataTable();

            if (null !== responseText &&  "data" in responseText) {
                data = responseText.data;
            } else if (null !== responseArr && "data" in responseArr) {
                data = responseArr.data;
            }

            // Reset the DataTable
            oTable.fnClearTable();
            oTable.fnAddData(data);
        },
        error: function (responseArr) {
            var responseText = $.parseJSON(responseArr.responseText);
            if ("error_messages" in responseText) {
                if ("field_name" in responseText.error_messages) {
                    utils.outputMessage('#frm-users',responseText.error_messages,'error',false,false,false);
                } else if ("field_name" in responseText.error_messages[0]) {
                    utils.outputMessage('#frm-users',responseText.error_messages[0],'error',false,false,false);
                }
            }
        }
    });
}

// Recordings DatePicker Functionality
function recordingsDateInitialise(formId) {
    var today = new Date();
    var dateOld = new Date();
    dateOld.setYear(dateOld.getYear()-1);

    // From Date
    $(formId).find('#recordings_from_date').datepicker({
        dateFormat:'yy-mm-dd',
        defaultDate:dateOld,
        minDate:dateOld,
        maxDate:today,
        showButtonPanel:true
    });

    // To Date
    $(formId).find('#recordings_to_date').datepicker({
        dateFormat:'yy-mm-dd',
        defaultDate:dateOld,
        minDate:dateOld,
        maxDate:today,
        showButtonPanel:true
    });
}

// Recordings Audio Init Functionality
function recordingsAudioInitialise() {
    yepnope({
        test : jQuery.fn.AudioPlayer,
        nope : ['/shared/SWFObject/audio-player.min.js'],
        complete: function () {
            AudioPlayer.setup("/sfswf/audioPlayer.swf", {
                width: 200,
                animation: 'no',
                noinfo: 'yes'
            });
        }
    });
}

// Recordings Run Audio Player Function
function recordingsAudioPlayer(recording_ref) {
    $('p#audioplayer_' + recording_ref).html(
        '<script type="text/javascript">AudioPlayer.embed("audioplayer_' + recording_ref + '", {soundFile: \'/s/Download-Recording?recording_ref=' + recording_ref + '&action=listen&autostart=yes\'});</script>'
    );
}

// Recordings Run Audio Player Function
function recordingsPublicAudioPlayer(recording_ref) {
    $('p#audioplayer_' + recording_ref).html(
        '<script type="text/javascript">AudioPlayer.embed("audioplayer_' + recording_ref + '", {soundFile: \'/Recording-Download?recording_ref=' + recording_ref + '&action=listen&autostart=yes\'});</script>'
    );
}

// Share Recordings Functionality
function recordingsShareRecordings(recording_ref, recording_publish, recording_expire, recording_password, hidden_field_id) {
    var tableID    = '#tblRecordings';
    var rowID      = $('#' + hidden_field_id).parent().closest('tr').index() + 1;
    var tblID      = rowID - 1;
    var settingsID = $('.rec-settings').closest('tr').index();

    // Check if you have clicked the same Share Button again
    $('.rec-settings').remove();
    if (settingsID === rowID) {
        return;
    }

    // Add New Settings Row, and Show it
    var settingsRow = '<tr class="rec-settings" id="rec-current-' + hidden_field_id + '"><td colspan="6">';
    settingsRow    += '<div class="grid_12">';
    settingsRow    += '<form method="post" action="/s/Recordings-Update-Ajax" id="recform_1" name="recform_1" enctype="application/x-www-form-urlencoded">';
    settingsRow    += '<input type="hidden" value="' + recording_ref + '" name="recording_ref"/>';
    settingsRow    += '<input type="hidden" value="' + hidden_field_id + '" name="hidden_field_id"/>';
    settingsRow    += '<div class="field" id="f_rec_ref"><span>Recording ref:</span><span class="mypwn-input-container">' + recording_ref + '</span></div>';
    settingsRow    += '<div class="field" id="f_rec_desc"><span>' + MYPWN_RECORDINGS_DESCRIPTION + ':</span><span class="mypwn-input-container"><input type="text" value="" id="rec_desc" name="rec_desc" class="mypwn-input font-small input-large"/></span></div>';
    settingsRow    += '<div class="field" id="f_rec_online"><span>' + MYPWN_RECORDINGS_PUBLISH_ONLINE + ':</span><input type="checkbox" name="publish_set" id="publish_set" /></div>';
    settingsRow    += '<div class="field" id="f_rec_expire"><span>' + MYPWN_RECORDINGS_PUBLISH_UNTIL + ':</span><span class="mypwn-input-container"><input value="" name="publish_expire" id="publish_expire" class="mypwn-input font-small input-large"/></span></div>';
    settingsRow    += '<div class="field" id="f_rec_password_set"><span>' + MYPWN_RECORDINGS_REQUIRE_PASSWORD + ':</span><input type="checkbox" name="password_set" id="password_set"></div>';
    settingsRow    += '<div class="field" id="f_rec_password"><span>' + MYPWN_RECORDINGS_PASSWORD + ':</span><span class="mypwn-input-container"><input type="password" value="" id="rec_password" name="rec_password" class="mypwn-input font-small input-large" /></span></div>';
    settingsRow    += '<div class="field"><button id="btn-rec-cancel" class="button-orange" type="button"><span>' + MYPWN_RECORDINGS_CANCEL + '</span></button><button id="btn-rec-save" class="button-green" type="submit"><span>' + MYPWN_RECORDINGS_SAVE + '</span></button></div>';
    settingsRow    += '</form>';
    settingsRow    += '</div>';
    settingsRow    += '<div class="grid_12">';
    settingsRow    += '<div id="f_rec_link">' + MYPWN_RECORDINGS_LINK_TEXT + ':&nbsp;<br/><span class="mypwn-input-container">http://www.powwownow.co.uk/Recordings/recording/id/' + recording_ref + '</span></div>';
    settingsRow    += '</div>';
    settingsRow    += '</div>';
    settingsRow    += '</td></tr>';

    $(tableID + ' tbody tr').eq(tblID).after(settingsRow);
    $('#rec-current-' + hidden_field_id).show();

    // Initialise the New Update Form
    recordingsUpdateFormInitialise('#recform_1');

    // Form Setup
    $('#rec_desc').val($(tableID + ' tbody tr:eq(' + tblID + ') td.desc').text());

    // Hide Additional Fields
    $('#f_rec_expire').hide();
    $('#f_rec_password_set').hide();
    $('#f_rec_password').hide();
    if (recording_publish != '1') $('#f_rec_link').hide();

    // Has the Recording been Published
    if (recording_publish == '1') {
        $("#publish_set").attr('checked', true);
        $('#publish_expire').val(recording_expire);
        $('#f_rec_expire').show();
        $('#f_rec_password_set').show();
    }

    // Has the Recording have a Password and has been Published
    if (recording_password == '1' && recording_publish == '1') {
        $('#password_set').attr('checked', true);
        $('#f_rec_password').show();
    }

    // show options field when user decides to publish a recording
    $('#publish_set').click(function(){
        $('#f_rec_expire').toggle();
        $('#f_rec_password_set').toggle();
        if ($('#password_set').attr('checked')) { $('#f_rec_password').toggle(); }
    });

    // Show password field when user requires it
    $('#password_set').click(function(){
        $('#f_rec_password').toggle();
    });

    var today         = new Date();
    var aftertomorrow = new Date(today.getTime() + (48 * 60 * 60 * 1000));

    $("#publish_expire").datepicker({
        dateFormat:'dd/mm/yy',
        defaultDate:aftertomorrow,
        minDate:aftertomorrow,
        showButtonPanel:true
    });

    $('#btn-rec-cancel').click(function(){
        $('.rec-settings').remove();
    });
}

// Update Recordings Form Functionality
function recordingsUpdateFormInitialise(formId) {
    $(formId).initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseArr) {
            // var responseText    = $.parseJSON(responseArr);
            var responseText    = responseArr;
            var publish_set     = responseText.publish_set,
                password_set    = responseText.password_set,
                description     = responseText.description,
                recording_ref   = responseText.recording_ref,
                publish_expire  = responseText.publish_expire,
                hidden_field_id = responseText.hidden_field_id;
            var tableID         = '#tblRecordings';
            var rowID           = $('#' + hidden_field_id).parent().closest('tr').index() + 1;
            var tblID           = rowID - 1;

            // Replace Description with the New Description
            $(tableID + ' tbody tr:eq(' + tblID + ') td.desc').html(description);

            // Replace Share Link with Updated Information
            $(tableID + ' tbody tr:eq(' + tblID + ') td a.rec_link').attr('href','javascript:recordingsShareRecordings(\'' + recording_ref + '\',' + publish_set + ',\'' + publish_expire + '\',' + password_set + ',\'' + hidden_field_id + '\');');

            // Publish Settings, decides on showing the link or not
            if (1 === publish_set || true === publish_set) {
                $('#f_rec_link').fadeIn('slow');
                $('#f_rec_link input').select();
            } else {
                $('#f_rec_link').fadeOut('slow');
            }

            // Hide Most of the Form apart from OK Button
            $('#f_rec_ref').hide();
            $('#f_rec_desc').hide();
            $('#f_rec_online').hide();
            $('#f_rec_expire').hide();
            $('#f_rec_password_set').hide();
            $('#f_rec_password').hide();
            $('#btn-rec-cancel span').html(MYPWN_RECORDINGS_OK);
            $('#btn-rec-cancel').css('margin-bottom','40px');
            $('#btn-rec-save').hide();
            return true;
        },
        error: function (responseArr) {
            var responseText = $.parseJSON(responseArr.responseText);
            if ("error_messages" in responseText) {
                if ("field_name" in responseText.error_messages) {
                    utils.outputMessage('#frm-users',responseText.error_messages,'error',false,false,false);
                } else if ("field_name" in responseText.error_messages[0]) {
                    utils.outputMessage('#frm-users',responseText.error_messages[0],'error',false,false,false);
                }
            }
        }
    });
}
