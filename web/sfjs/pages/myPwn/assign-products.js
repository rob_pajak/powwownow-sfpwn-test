$(document).ready(function () {
    var $bundleDialog = $('#relevant-bundle-feature-disabled-dialog');
    var $assignPinProduct = $('#form-update-my-pin-products');
    var $notifyUsersSuccess = $('#dialog-modal-notify-users-success');
    var $notifyUsersFailure = $('#dialog-modal-notify-users-failure');
    var $notifyUsersGenericError = $('#dialog-modal-notify-users-backend-error');
    var $notifyUsersForm = $('#notify-users-of-product-assignment');
    var $notifySubmodal = $('#submodal-notify-users');
    var $confirmDialog = $("#dialog-modal-confirm-update");
    var commonDialogConfig = {
        resizable: false,
        modal: true,
        autoOpen: false,
        buttons: {
            Ok: function () {
                $(this).dialog('close');
            }
        }
    };

    // Configure the message dialogs.
    $notifyUsersSuccess.dialog({
        resizable: false,
        modal: true,
        autoOpen: false,
        buttons: {},
        open: function () {
            var $dialog = $(this);
            // Close the dialog after 2 seconds.
            setTimeout(function () {
                $dialog.dialog('close');
            }, 2000);
        }
    });
    $notifyUsersFailure.dialog(commonDialogConfig);
    $notifyUsersGenericError.dialog(commonDialogConfig);

    // Listen to the change event for notify-user checkboxes, to enable or disable form submission.
    // These are dynamically generated via JS (see below), so the event is live.
    $notifySubmodal.on('change', '.notifiable-user', function () {
        var $notifyButton = $('#notify-user-submit');
        if ($('.notifiable-user:checked').length === 0) {
            disableButton($notifyButton);
        } else {
            enableButton($notifyButton);
        }
    });

    /**
     * Searches the AJAX response for notifiable contacts and, if any, allows the admin to choose the contacts to notify.
     *
     * @param {Object} response
     * @author Maarten Jacobs
     */
    function renderNotifiableContacts(response) {
        var notifiableContacts = response.notifiable_contacts;
        if (notifiableContacts) {
            // Render the notifiable-contacts into list items.
            var userId;
            var renderedContacts = $.map(notifiableContacts, function (contact, contactRef) {
                userId = 'notifiable-user-' + contactRef;
                return '<li class="notifiable-user-item">'
                    + '<label for="' + userId + '">' + contact.contact_name + '</label>'
                    + '<input id="' + userId + '" class="notifiable-user" type="checkbox" name="notify[' + contactRef + ']" value="' + contact.products.join(',') + '" />'
                    + '</li>';
            });
            if (renderedContacts.length) {
                return renderedContacts.join('');
            }
        }
        return false;
    }

    function extendModalForUserNotification(extend) {
        var modalProperties = {};
        if (extend) {
            modalProperties.buttons = [
                {
                    html: "<span>Notify</span>",
                    id: 'notify-user-submit',
                    click: notifyUsersOfProductAssignment
                },
                {
                    html: "<span>Cancel</span>",
                    id: 'notify-user-cancel',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ];
            modalProperties.width = 500;
            modalProperties.height = 'auto';
            modalProperties.open = function () {
                disableButton($('#notify-user-submit'));
                enableButton($('#notify-user-cancel'));
                $('#notify-user-submit').addClass('button-green');
            };
        }
        return modalProperties;
    }

    function notifyUsersOfProductAssignment() {
        // Disable any submit and input fields.
        disableButton($('#notify-user-submit'));
        disableButton($('#notify-user-cancel'));
        $('.notifiable-user').attr('disabled', 'disabled');

        // Make an AJAX post request to send the email.
        $.post($notifyUsersForm.attr('action'), formNotifyUserData($notifyUsersForm.find('.notifiable-user:checked')), 'json')
            .done(function () {
                $confirmDialog.dialog('close');
                $notifyUsersSuccess.dialog('open');
            })
            .fail(function (response) {
                $confirmDialog.dialog('close');

                var parsedResponse = $.parseJSON(response.responseText);
                if (parsedResponse.invalid_request) {
                    // The user has altered some values.
                    // Close the dialog and ignore; it has been logged.
                } else if (parsedResponse.error_occurred) {
                    $notifyUsersGenericError.dialog('open');
                } else if (parsedResponse.failed_sends && parsedResponse.failed_sends.length) {
                    // Notify the current user we were unable to notify a number of users.
                    var messageContent = $.map(parsedResponse.failed_sends, function (value) {
                        return '<li>' + value + '</li>';
                    });
                    $notifyUsersFailure.find('#failed-notifications').empty().append(messageContent.join(''));
                    $notifyUsersFailure.dialog('open');
                }
            });
    }

    function disableButton($button) {
        $button.addClass('button-disabled').attr('disabled', 'disabled');
    }

    function enableButton($button) {
        $button.removeClass('button-disabled').removeAttr('disabled');
    }

    function formNotifyUserData($checkedUserData) {
        var userData = {};
        var $nextUser;
        $checkedUserData.each(function () {
            $nextUser = $(this);
            userData[$nextUser.attr('name')] = $nextUser.attr('value').split(',');
        });
        return userData;
    }

    /*
     * When a LandLine Bundle is disabled AND the relevant LandLine features are still active, we need to let the
     * user know.
     *
     * The bundle-row class is only added to LandLine Bundles.
     * The relevant LandLine features are determined the Symfony action, and are marked with the relevant-bundle-feature
     * class.
     *
     * @author Maarten Jacobs
     */
    $('.pinproduct.bundle-row :checkbox').change(function () {
        var $alteredCell = $(this);
        if (!$alteredCell.is(':checked')) {
            var $relevantFeatures = $alteredCell.parents('tr').find('.relevant-bundle-feature :checked:enabled');
            if ($relevantFeatures.length) {
                $bundleDialog.dialog('open');
            }
        }
    });

    // Configure the dialog in PWN style.
    if ($('.relevant-bundle-feature').length) {
        $bundleDialog.show().dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            buttons: [
                {
                    text: "Ok",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    }

    function showConfirmUpdateModal(topup, modalProperties) {
        if (topup) {
            $('#dialog-modal-topup').removeClass('not-show');
        } else {
            $('#dialog-modal-topup').addClass('not-show');
        }
        var dialogProperties = {
            width: 360,
            height: 160,
            modal: true,
            resizable: false,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        };
        if (modalProperties) {
            dialogProperties = $.extend(dialogProperties, modalProperties);
        }
        return $confirmDialog.dialog(dialogProperties);
    }

    $assignPinProduct.initMypwnForm({
        successMessagePos: 'off',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'off',
        success: function (responseText) {
            // The response text should be JSON and contain a flag to indicate insufficient balance.
            var response = $.parseJSON(responseText);
            var insufficientBalance = response && typeof response.insufficient_credit !== 'undefined' && response.insufficient_credit;
            var message = '';
            var notifiableContactsContent = renderNotifiableContacts(response);
            var dialogExtension = extendModalForUserNotification(notifiableContactsContent !== false);

            // Remove the existing insufficient balance warning balance, if any.
            if (insufficientBalance) {
                // Notify the user that the products require credit.
                showConfirmUpdateModal(true, dialogExtension);
            } else {
                // show an update message.
                showConfirmUpdateModal(false, dialogExtension);
            }

            if (notifiableContactsContent) {
                $notifySubmodal.removeClass('not-show');
                $('#notifiable-contacts').empty().append(notifiableContactsContent);
            } else {
                $notifySubmodal.addClass('not-show');
            }

            $('#form-response-placeholder').hide().html(message).fadeIn('slow');
        }
    });

    $(".product-accordion").accordion({ autoHeight: false });

    $('#adminupdate').hide();

    /**
     * Create Single Fixed Column Table
     */
    createFixedColumnTable('#pinproducts');

    /**
     * Header Checkbox to select ALL Functionality
     */
    $('input.checkall').on('click', function() {
        var checkboxVal = $(this).val() + '[]';
        if ($(this).is(':checked')) {
            $('input[name="' + checkboxVal + '"]:enabled').attr("checked", true);
        } else {
            $('input[name="' + checkboxVal + '"]:enabled').attr("checked", false);
        }
        showUpdateButton();
    });

    /**
     * Individual Checkbox
     */
    $('td.pinproduct').find('input[type=checkbox]').on('click', function () {
        if (!$(this).is(':disabled')) {
            showUpdateButton();
        }
    });

    /**
     * Show the Update Button
     */
    function showUpdateButton() {
        $('#admincontinue').hide();
        $('#adminupdate').show();
    }

    /**
     * Header Tooltip Helper
     */
    $('span.productheader').click(function () {
        var tooltip = $('div.tooltip.product' + $(this).data('product')),
            offset  = $(this).offset();

        if (!tooltip.is(':visible')) {
            $('.tooltip-container').find('.tooltip').hide();
            offset.top += $(this).closest('table').height();
            tooltip.show()
                .css('position', 'absolute')
                .offset(offset)
                .focus();
            $('.dataTables_scrollBody').scroll(function () {
                $('div.tooltip').hide();
            });
            $('tooltip-close').on('click', function () {
                $('div.tooltip').hide();
            });
        } else {
            $('.tooltip-container').find('.tooltip').hide();
        }
    });
});