/**
 * Video Conferencing Tabs Initialisation
 */
function videoConferencingTabs_Init() {

    // Tab Positions
    tabs_setUpTabPositions(71);

    // Tab Wiggle
    tabs_setUpHoverWiggle('1');
    tabs_setUpHoverWiggle('2');
    tabs_setUpHoverWiggle('3');

    // Tab URL Check, for Second and Third Tabs
    switch (window.location.pathname) {
        case '/Video-Conferencing/Demo':
            videoConferenceDemoTab();
            break;
        case '/Video-Conferencing/Get-Started':
            videoConferenceGetStartedTab();
            break;
    }

    // Tab Clicks

    // Showtime TAB
    $('#tab-1').click(videoConferenceShowtimeTab);

    // Demo TAB
    $('#tab-2').click(videoConferenceDemoTab);

    // Get started TAB
    $('#tab-3').click(videoConferenceGetStartedTab);

    // Sets up showtime in action tooltip
    $('.tooltip-showtime-in-action-close').click(function() {
        $('#tooltip-showtime-in-action').toggle('fast');
        return false;
    });
    $('.tooltip-showtime-in-action-link').click(function() {
        $('#tooltip-showtime-in-action').toggle('fast');
        return false;
    });
}

/**
 * Video-Conferencing Showtime Tab
 */
function videoConferenceShowtimeTab() {
    showContentForWebAndVideoConferencingTags(1);
}

/**
 * Video-Conferencing Demo Tab
 */
function videoConferenceDemoTab() {
    showContentForWebAndVideoConferencingTags(2);
    tabs_addCanonicalLink(VIDEO_CONFERENCING_CANONICAL_URL);
}

/**
 * Video-Conferencing Get-Started Tab
 */
function videoConferenceGetStartedTab() {
    showContentForWebAndVideoConferencingTags(3);
    tabs_addCanonicalLink(VIDEO_CONFERENCING_CANONICAL_URL);
}
