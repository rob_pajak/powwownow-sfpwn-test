var Recommendations = {
    
    initialize: function () {

        Recommendations.initPwnForm();
        Recommendations.dynamicRequired();

    },

    dynamicRequired: function () {
        var el = $('.recommend-form-row input');
        
        el.focusin(function () {
            $(this).closest('.recommend-form-row').css('background-color','#8AB23E');

        });

        el.focusout(function () {
            $(this).closest('.recommend-form-row').css('background','none');


        });

        el.keyup(function () {

            var notEmpty = false;
            var context = $(this).closest('.recommend-form-row');
            context.find('input').each(function () {
                if ($(this).attr('value') !== '') notEmpty = true;
                
            });

            if (notEmpty) {
                Recommendations.makeRowRequired(context);
            } else {
                Recommendations.makeRowNotRequired(context);
            }
        });
    },

    makeRowRequired: function (el) {
        el.find('input').each(function () {
            if (($(this).attr('required') !== 'required') || (!$(this).hasClass('required'))) {
                $(this).addClass('required');
                var label = $(this).parent().parent().children('label');
                label.children('sup').remove();
                label.append('<sup class="asterisk">*</sup>');
            }
        });

    },

    makeRowNotRequired: function (el) {
        el.find('input').each(function () {
            if (!$(this).hasClass('persist-required')) {
                $(this).removeClass('required');
                var label = $(this).parent().parent().children('label');
                label.children('sup').remove();
            }
        });

    },

    initPwnForm: function () {

        var el = $('#frm-recommendations');
        
        el.initMypwnForm({
            debug: 'on',
            html5Validation: 'off',
            requiredAsterisk: 'on',
            successMessagePos: 'off',
            success: function() {
                $( "#thankYou" ).dialog({
                    width: 570,
                    height: 260,
                    modal: true,
                    closeOnEscape: false
                });
            }
        });

        //remove IE double asterisks
        $('sup+sup').remove();

    }

};