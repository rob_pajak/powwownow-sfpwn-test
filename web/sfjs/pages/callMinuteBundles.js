$(document).ready(function() {
    // Initial Action of the Plus Link
    $('#dialog-plus-link').click(function() {
        resetForms();
        showStartForm();
    });

    // Reset Forms
    function resetForms () {
        // Close All Popups
        $('#plusPasswordForm, #plusFormWithPassword, #plusFormWithoutPassword, #plusNoSwitchPowwownow, #plusNoSwitchPlusUser, #plusNoSwitchPlusAdmin, #plusNoSwitchPremiumUser, #plusNoSwitchPremiumAdmin, #plusRedirectionNew, #plusRedirectionExisting, #plusNoSwitch1, #plusExistingCustomerPowwownow, #plusExistingCustomerPlusUser, #plusExistingCustomerPlusAdmin, #plusExistingCustomerPremiumUser, #plusExistingCustomerPremiumAdmin').hide();

        // Reset Form Values
        $('#plusEmailForm').find('#plus_register_email, #form-existing-customer_email, #form-existing-customer_password').removeAttr('value');
        $('#plusFormWithPassword').find('#plus_register_email, #plus_signup_first_name, #plus_signup_last_name, #plus_signup_company_name, #plus_signup_business_phone, #plus_signup_password, #plus_signup_confirm_password').removeAttr('value');
        $('#plusFormWithoutPassword').find('#plus_register_email, #plus_register_password, #plus_signup_first_name, #plus_signup_last_name, #plus_signup_company_name, #plus_signup_business_phone').removeAttr('value')
        $('#plusNoSwitchPowwownow, #plusNoSwitchPlusUser, #plusNoSwitchPlusAdmin, #plusNoSwitchPremiumUser, #plusNoSwitchPremiumAdmin, #plusExistingCustomerPowwownow, #plusExistingCustomerPlusUser, #plusExistingCustomerPlusAdmin, #plusExistingCustomerPremiumUser, #plusExistingCustomerPremiumAdmin').find('.toggle-firstname').html('Hi ');

        // Set the Background Colour
        $('#dialog-plus').css('background','#7EA2B4');

        // Show Initial Popup
        $('#plusEmailForm').show();

        popupModalFunctions();
        popupInitEmailForm();
    }

    // New Function to Handle Authenticated Users
    function showStartForm() {
        var template = '';
        $.ajax({
            url: '/Call-Minute-Bundles-Initial-Form',
            type: 'POST',
            data: {},
            success: function(responseText) {
                var template = responseText.template;
                var first_name = responseText.first_name;
                var email = responseText.email;

                switch (template) {
                    case 'plusExistingCustomerPowwownow':
                    case 'plusExistingCustomerPlusUser':
                    case 'plusRedirectionAuth':
                    case 'plusExistingCustomerPremiumUserAuth':
                    case 'plusExistingCustomerPremiumAdminAuth':
                        $('#plusEmailForm').hide();
                        $('#'+template).find('.toggle-firstname').append(first_name);
                        $('#'+template).find('#email').val(email);
                        popupInitLinkPowwownowForm(email);

                        if (template == 'plusRedirectionAuth') {
                            $('#dialog-plus').css('min-height','50px');
                            postPlusAdminToBasket();
                        } else if (template == 'plusExistingCustomerPremiumUserAuth') {
                            $('#dialog-plus').css('min-height','50px');
                        } else if (template == 'plusExistingCustomerPremiumAdminAuth') {
                            $('#dialog-plus').css('min-height','50px');
                        }

                        $('#'+template).show();
                        break;
                    case 'plusEmailForm':
                    default:
                        logOut();
                        break;
                }
            },
            error: function (responseText) {
                var message = myPwnApp.utils.obtainMessage(responseText,'error');
                outputMessage('#form-existing-customer',message,'error',false,false);
            }
        });
    }

    // Logout the User
    function logOut () {
        $.ajax({
            url: '/ajax/mypwn/logout',
            method: 'get'
        });
    }

    // Generic Popup Modal Functions
    function popupModalFunctions() {
        // Input Focus and Blur
        $('#dialog-plus').find('input').blur(function(){
            $(this).parent('span').removeClass('mypwn-input-container-focused').addClass('mypwn-input-container');
        }).focus(function(){
            $(this).parent('span').removeClass('mypwn-input-container').addClass('mypwn-input-container-focused');
        });

        // On Click Event
        $('.pst-tooltip-close').click(function(){
            $('#dialog-plus').dialog('close');
        });
    }

    // Load the Initial Forms
    function popupInitEmailForm() {
        dataLayer.push({'event': 'PlusAttemptEmailForm/onLoad', 'page': 'Call-Minute-Bundles'});

        $('#form-plus-switch-step1').initMypwnForm({
            debug: 'on',
            messagePos: 'off',
            success: function (responseText) {
                var template = responseText.template;
                var first_name = responseText.first_name;
                var email = responseText.email;

                switch (template) {
                    case 'plus_no_switch':
                    case 'plus_no_switch1':
                    case 'plusNoSwitchPowwownow':
                    case 'plusNoSwitchPlusUser':
                    case 'plusNoSwitchPlusAdmin':
                    case 'plusNoSwitchPremiumUser':
                    case 'plusNoSwitchPremiumAdmin':
                        $('#plusEmailForm').hide();
                        $('#'+template).find('.toggle-firstname').append(first_name);
                        $('#'+template).find('#email').val(email);
                        popupInitLinkPlusAdminForm(email);

                        $('#'+template).show();
                        $('#dialog-plus').css('min-height','50px');
                        break;
                    case 'plus_form_with_password':
                        $('#plusEmailForm').hide();
                        $('#plusFormWithPassword').find('#plus_register_email').val(email);
                        $('#plusFormWithPassword').show();

                        if ($.browser.msie && $.browser.version <= 8.0) {
                            $("#dialog-plus").dialog({width: 500});
                        } else {
                            $("#dialog-plus").dialog({width: 460});
                        }

                        popupModalFunctions();
                        popupInitPlusWithPasswordForm();
                        break;
                    case 'plus_password_form':
                        $('#plusEmailForm').hide();
                        $('#plusPasswordForm').find('#plus_register_email').val(email);
                        $('#plusPasswordForm').show();
                        $('#dialog-plus').css('min-height','150px');
                        popupModalFunctions();
                        popupInitPasswordForm();
                        break;
                }
            },
            error: function (responseText) {
                $('.nice-error-box').hide();
                $('.plus-registration-error').remove();

                var message = myPwnApp.utils.obtainMessage(responseText,'error');
                outputMessage('#form-plus-switch-step1',message,'error',false,false);
            }
        });

        // Existing Password Form
        $('#form-existing-customer').initMypwnForm({
            debug: 'off',
            messagePos: 'off',
            success: function (responseText) {
                var template = responseText.template;
                var email    = responseText.email;
                var first_name = responseText.first_name;

                switch (template) {
                    case 'plusExistingCustomerPowwownow':
                    case 'plusExistingCustomerPlusUser':
                    case 'plusExistingCustomerPlusAdmin':
                    case 'plusExistingCustomerPremiumUser':
                    case 'plusExistingCustomerPremiumAdmin':
                        $('#plusEmailForm').hide();
                        $('#'+template).find('.toggle-firstname').append(first_name);

                        if (template == 'plusExistingCustomerPowwownow') {
                            $('#'+template).find('#email').val(email);
                            popupInitLinkPowwownowForm(email);
                        } else if (template == 'plusExistingCustomerPlusAdmin') {
                            postPlusAdminToBasket();
                        }

                        $('#'+template).show();
                        $('#dialog-plus').css('min-height','50px');
                        break;
                }
            },
            error: function (responseText) {
                $('.nice-error-box').hide();
                $('.plus-registration-error').remove();

                var message = myPwnApp.utils.obtainMessage(responseText,'error');
                outputMessage('#form-existing-customer',message,'error',false,false);
            }
        });
    }

    // Load the Password Form
    function popupInitPasswordForm () {
        dataLayer.push({'event': 'PlusAttemptPasswordForm/onLoad', 'page': 'Call-Minute-Bundles'});

        $('#form-plus-switch-auth').initMypwnForm({
            debug: 'on',
            messagePos: 'off',
            success: function (responseText) {
                var template = responseText.template;
                var email = responseText.email;
                var password = responseText.password;

                if (template == 'plus_form_without_password') {
                    $('#plusPasswordForm').hide();
                    $('#plusFormWithoutPassword').find('#plus_register_email').val(email);
                    $('#plusFormWithoutPassword').find('#plus_register_password, #plus_register_confirm_password').val(password);
                    $('#plusFormWithoutPassword').show();

                    if ($.browser.msie && $.browser.version <= 8.0) {
                        $("#dialog-plus").dialog({width: 500});
                    } else {
                        $("#dialog-plus").dialog({width: 460});
                    }

                    popupModalFunctions();
                    popupInitPlusWithoutPasswordForm();
                }
            },
            error: function (responseText) {
                $('.nice-error-box').hide();
                $('.plus-registration-error').remove();

                var message = myPwnApp.utils.obtainMessage(responseText,'error');
                outputMessage('#form-plus-switch-auth',message,'error',false,false);
            }
        });
    }

    // Load the Link back from the PlusA Template
    function popupInitLinkPlusAdminForm (email) {
        $('.first-screen-plusadmin').click(function() {
            $.ajax({
                url: '/Plus-Signup-New',
                type: 'POST',
                data: {'init' : true, 'email' : email, 't_and_c' : true},
                success: function(responseText) {
                    resetForms();
                    logOut();
                    $('#form-existing-customer').find('#form-existing-customer_email').val(responseText.email);
                    $('#form-existing-customer').find('#form-existing-customer_password').parent('span').removeClass('mypwn-input-container').addClass('mypwn-input-container-focused');
                }
            });
        });
    }

    // Load the Link back from the Powwownow Template
    function popupInitLinkPowwownowForm (email) {
        $('.first-screen-powwownow').click(function() {
            $.ajax({
                url: '/Plus-Signup-New',
                type: 'POST',
                data: {'init' : true, 'email' : email, 't_and_c' : true},
                success: function(responseText) {
                    resetForms();
                    logOut();
                    $('#form-plus-switch-step1').find('#plus_register_email').val(responseText.email);
                    $('#form-plus-switch-step1').find('#plus_register_email').parent('span').removeClass('mypwn-input-container').addClass('mypwn-input-container-focused');
                    $('#form-existing-customer').find('#form-existing-customer_email, #form-existing-customer_password').removeAttr('value');
                }
            });
        });
    }

    // Load the Plus With Password Form
    function popupInitPlusWithPasswordForm () {
        dataLayer.push({'event': 'PlusAttemptFinalForm/onLoad', 'page': 'Call-Minute-Bundles'});

        $('#form-plus-registration-with-password').initMypwnForm({
            debug: 'on',
            requiredAsterisk: 'on',
            messagePos: 'off',
            success: function (responseText) {
                var template = responseText.template;

                switch (template) {
                    case 'plus_redirection_new':
                        $('#plusFormWithPassword').hide();
                        $('#plusRedirectionNew').show();
                        $('#dialog-plus').css('min-height','50px');

                        dataLayer.push({'event': 'PlusCallMinuteBundlesNew/RegistrationSuccess'});

                        // Redirect to Basket Page
                        $.post('/s/Basket', {}, 'json')
                            .done(function() {
                                $(location).attr('href', '/s/Basket');
                            })
                            .fail(function(response) {
                                var responseMessage = $.parseJSON(response.responseText);
                                if (responseMessage.alert && responseMessage.error) {
                                    alert(responseMessage.error);
                                }
                            });
                        break;
                    case 'plus_redirection_existing':
                        $('#plusFormWithPassword').hide();
                        $('#plusRedirectionExisting').show();
                        $('#dialog-plus').css('min-height','50px');

                        dataLayer.push({'event': 'PlusCallMinuteBundlesSwitch/RegistrationSuccess'});

                        // Redirect to Basket Page
                        $.post('/s/Basket', {}, 'json')
                            .done(function() {
                                $(location).attr('href', '/s/Basket');
                            })
                            .fail(function(response) {
                                var responseMessage = $.parseJSON(response.responseText);
                                if (responseMessage.alert && responseMessage.error) {
                                    alert(responseMessage.error);
                                }
                            });
                        break;
                    case 'plus_no_switch1':
                        $('#plusFormWithPassword').hide();
                        $('#plusNoSwitch1').show();
                        break;
                }
            },
            error: function (responseText) {
                $('.nice-error-box').hide();
                $('.plus-registration-error').remove();

                var message = myPwnApp.utils.obtainMessage(responseText,'error');
                outputMessage('#form-plus-registration-with-password',message,'error',false,false);
            }
        });
    }

    // Load the Plus With Password Form
    function popupInitPlusWithoutPasswordForm () {
        dataLayer.push({'event': 'PlusAttemptFinalForm/onLoad', 'page': 'Call-Minute-Bundles'});

        $('#form-plus-registration-without-password').initMypwnForm({
            debug: 'on',
            requiredAsterisk: 'on',
            messagePos: 'off',
            success: function (responseText) {
                var template = responseText.template;

                switch (template) {
                    case 'plus_redirection_new':
                        $('#plusFormWithoutPassword').hide();
                        $('#plusRedirectionNew').show();
                        $('#dialog-plus').css('min-height','50px');

                        dataLayer.push({'event': 'PlusCallMinuteBundlesNew/RegistrationSuccess'});

                        // Redirect to Basket Page
                        $.post('/s/Basket', {}, 'json')
                            .done(function() {
                                $(location).attr('href', '/s/Basket');
                            })
                            .fail(function(response) {
                                var responseMessage = $.parseJSON(response.responseText);
                                if (responseMessage.alert && responseMessage.error) {
                                    alert(responseMessage.error);
                                }
                            });
                        break;
                    case 'plus_redirection_existing':
                        $('#plusFormWithoutPassword').hide();
                        $('#plusRedirectionExisting').show();
                        $('#dialog-plus').css('min-height','50px');

                        dataLayer.push({'event': 'PlusCallMinuteBundlesSwitch/RegistrationSuccess'});

                        // Redirect to Basket Page
                        $.post('/s/Basket', {}, 'json')
                            .done(function() {
                                $(location).attr('href', '/s/Basket');
                            })
                            .fail(function(response) {
                                var responseMessage = $.parseJSON(response.responseText);
                                if (responseMessage.alert && responseMessage.error) {
                                    alert(responseMessage.error);
                                }
                            });
                        break;
                    case 'plus_no_switch1':
                        $('#plusFormWithoutPassword').hide();
                        $('#plusNoSwitch1').show();
                        break;
                }
            },
            error: function (responseText) {
                $('.nice-error-box').hide();
                $('.plus-registration-error').remove();

                var message = myPwnApp.utils.obtainMessage(responseText,'error');
                outputMessage('#form-plus-registration-without-password',message,'error',false,false);
            }
        });
    }

    // Output the Message from Either the Success and Error Methods
    function outputMessage (formId,responseText,msgType,redirectOnSuccess,reload) {
        // Check if the Message is a Global
        if (window[responseText.message] !== undefined) {
            responseText.message = window[responseText.message];
        }

        // Show Message
        if ('alert' == responseText.field_name) {
            $(formId).find('.message').remove();
            switch (msgType) {
                case 'error':
                    $(formId).append('<div class="error message grid_24">'+responseText.message+'</div>');
                    $(formId).find('.message').delay(5000).fadeOut('slow');

                    if (false !== reload) {
                        window.setTimeout(function (){ window.location.reload(); }, 5500);
                    }
                    break;
                case 'success':
                    $(formId).append('<div class="success message grid_24">'+responseText.message+'</div>');
                    $(formId).find('.message').delay(5000).fadeOut('slow');

                    if (false !== redirectOnSuccess && window[redirectOnSuccess] !== undefined) {
                        window.setTimeout(function (){ window.location = window[redirectOnSuccess]; }, 5500);
                    } else if (false !== redirectOnSuccess && typeof redirectOnSuccess !== 'undefined') {
                        window.setTimeout(function (){ window.location = redirectOnSuccess; }, 5500);
                    } else if (false !== reload) {
                        window.setTimeout(function (){ window.location.reload(); }, 5500);
                    }

                    break;
                default:
                    // Do Something
                    break;
            }
        } else {
            switch (msgType) {
                case 'error':
                    $(formId).find('input[name="'+responseText.field_name+'"]').parent().after('<div class="plus-registration-error">'+responseText.message+'</div>');
                    $(formId).find('.plus-registration-error').delay(5000).fadeOut('slow');
                    break;
            }
        }
    };

    // Plus Admin Post Request
    function postPlusAdminToBasket () {
        $.post('/s/Basket', {}, 'json')
            .done(function() {
                $(location).attr('href', '/s/Basket');
            })
            .fail(function(response) {
                var responseMessage = $.parseJSON(response.responseText);
                if (responseMessage.alert && responseMessage.error) {
                    alert(responseMessage.error);
                }

                if(responseMessage.modal == 'accountHasBundle') {
                    $('#accountHasBundle-modal').dialog( "open" );

                    $('#accountHasBundle-modal button').click(function() {
                        $('#accountHasBundle-modal').dialog("close");
                        $('#dialog-plus').dialog('close');
                    });
                }
            });
    }
});