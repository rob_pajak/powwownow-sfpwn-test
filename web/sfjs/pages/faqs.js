$(document).ready(function () {
    var faqsElement = $('div#faqs');
    faqsElement.accordion({
        active: false,
        collapsible: true,
        autoHeight: false
    });

    if (typeof window.location.hash != 'undefined' && window.location.hash != '') {
        if (window.location.hash == '#Browsers') {
            faqsElement.accordion('option', 'active', 35);
            $('html,body').animate({
                scrollTop: $("div.link-browsers").offset().top
            }, 'slow');
        }
    }
});
