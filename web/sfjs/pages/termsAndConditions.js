$(document).ready(function () {
    // Check the Window Location for Other Tabs
    var tabHash = window.location.hash; //'#1' or ''

    // Class Name for the Terms and Conditions Item Container
    var className = 'terms-conditions-container';

    // No Hash being used, so Load First Item
    if (tabHash == '') {
        $('.' + className).find('.item-1').show();
    } else {
        tabHash = tabHash.replace('#', '');
        var tabLength = tabHash.length,
            tabInt = parseInt(tabHash),
            tabIntLength = tabInt.toString().length;

        // Hash was a Number Only.
        if (tabLength == tabIntLength && !isNaN(tabHash)) {
            $('.' + className).find('.item-' + tabHash).show();
        }

        // Has was not a Number (Has Letters Mixed in, or is whole Letters
        if (tabLength != tabIntLength) {
            var linkTarget = $('a[name="' + tabHash + '"]');
            var localTabName = linkTarget.closest('div.terms-conditions-container-item').attr('class');
            localTabName = localTabName.replace('terms-conditions-container-item ', '');

            $('.' + className).find('.' + localTabName).show();
            $(document.body).animate({
                'scrollTop': linkTarget.offset().top
            }, 1000);
        }
    }

    // on Click Functionality
    $('a.terms-and-conditions-contents').click(function () {
        var linkId = $(this).attr('data-id');
        var allTabs = $('.' + className);
        allTabs.find('.item-1, .item-2, .item-3, .item-4, .item-5, .item-6, .item-7').hide();
        allTabs.find('.item-' + linkId).show();
    });
});
