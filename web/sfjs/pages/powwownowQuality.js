var powwownowQuality = {
    
    initialize: function () {

        powwownowQuality.toggleTable();

    },

    toggleTable: function (el) {

        // toggle td.showstats by default
        if (el === undefined) {
            el = $('td.showstats');
        }

        el.click(function () {
            var table = $(this).parents('table'),
                tmp = $(this).html();

            table.find('tr.hiddenstats').toggle(function() {
                if ("Show more..." == tmp) {
                    table.find('td.showstats').html('Hide above...');
                } else {
                    table.find('td.showstats').html('Show more...');
                }
            });
        });

    }

};