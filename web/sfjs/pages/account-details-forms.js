$(document).ready(function () {
    var options = {
        successMessagePos: 'above',
        debug: 'on',
        html5Validation: 'off',
        requiredAsterisk: 'on',
        error: function(responseArr) {
            $('div.error.message').remove();
        }
    };
    var $accountDetailsForm = $('#frm_account_details_my_details');
    var $passwordForm = $('#frm_account_details_my_password');
    var $plusAccountForm = $('#form_plus_account_details');

    $accountDetailsForm.initMypwnForm(options);
    $passwordForm.initMypwnForm(options);
    if ($plusAccountForm.length) {
        $plusAccountForm.initMypwnForm(options);
    }

    $('#email-change-info').click(function() {
        $('.frmaccountdetails-results-placeholder1').html('<div class="message warning">'+ FORM_VALIDATION_CHANGE_EMAIL_ERROR +'</div>');
        $('.message').show().delay(10000).fadeOut('slow');
    });
});
