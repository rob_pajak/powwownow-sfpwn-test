(function($) {
    'use strict';
    $(function() {
        $('#how-web-conferencing-works-download').on('click', function() {
            var redirect;
            try {
                redirect = $(this).attr('data-link');
            } catch (e) {
                console.info('#how-web-conferencing-works-download could not get redirect link defaulting');
                redirect = '/myPwn/Web-Conferencing';
            }

            try {
                dataLayer.push({'event': 'How-Web-Conferencing-Works - Download - Click/onClick'});
            }catch (e) {
                console.info('dataLayer event How-Web-Conferencing-Works - Download - Click/onClick not pushed');
            } finally {
                setTimeout(function(){
                    window.location.href = redirect;
                }, 1000)
            }
        });
    });
})(jQuery);