/**
 * Initialise the Testimonials Carousel and the Videos
 */
function testimonialsInitialisation() {
    var carousel = $('.jcarousel');
    carousel
        .jcarousel({
            wrap: 'both',
            animation: 'slow'
        })
        .jcarouselAutoscroll({
            interval: 5000
        });

    testimonialsCarouselControls('.jcarousel-control-prev', '-=1');
    testimonialsCarouselControls('.jcarousel-control-next', '+=1');
    testimonialsCarouselCallbacks(carousel);
}

/**
 * Setup the Carousel Controls
 * @param control
 * @param target
 */
function testimonialsCarouselControls(control, target) {
    $(control)
        .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function () {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            target: target
        });
}

/**
 * Set up the Multiple Callbacks and Events for the Video and jCarousel Functionality
 * @param carousel
 */
function testimonialsCarouselCallbacks(carousel) {
    if (Modernizr.video) {
        $('video').each(function () {
            $(this)
                .on('play', function() {
                    if (!this.paused) {
                        carousel.jcarouselAutoscroll('stop');
                    }
                })
                .on('ended, pause', function () {
                    if (this.paused) {
                        carousel.jcarouselAutoscroll('start');
                    }
                });
        });
    } else if (swfobject.hasFlashPlayerVersion("1")) {
        $f("*").each(function() {
            this
                .onStart(function () {
                    carousel.jcarouselAutoscroll('stop');
                })
                .onPause(function () {
                    carousel.jcarouselAutoscroll('start');
                })
                .onFinish(function () {
                    carousel.jcarouselAutoscroll('start');
                });
        });
    }

    carousel
        .on('jcarousel:visiblein', 'li', function(event, carousel) {
            testimonialsDisableAllVideos();
        })
        .on('jcarousel:visibleout', 'li', function(event, carousel) {
            testimonialsDisableAllVideos();
        });
}

/**
 * Disable All Videos on the Testimonials Page.
 * Checks to See if HTML5 is available, then disables the HTML5 Videos,
 * otherwise checks to see if Flash Player is enabled, then disables the FlowPlayer Videos.
 *
 * The Try Catch around this.currentTime is to block any errors being shown, even though the code is valid
 */
function testimonialsDisableAllVideos() {
    if (Modernizr.video) {
        $('video').each(function () {
            this.pause();
            try {
                this.currentTime = 0;
            } catch (err) {

            }
        });
    } else if (swfobject.hasFlashPlayerVersion("1")) {
        $f("*").each(function() {
            if (this.isLoaded()) {
                this.unload();
                this.stop();
            }
        });
    }
}
