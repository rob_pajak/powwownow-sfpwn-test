/**
 * Extend the Select Products page to prevent page redirection for basket actions.
 */
$(document).ready(function() {
//    "use strict";

    // Flag to denote we are making an AJAX request at the moment.
    var requestInProgress = false;

    function handleBundleFormIdInHash() {
        var hash = window.location.hash;
        var $element = $(hash);
        if ($element.length && $element.is('tr.bundle.form')) {
            $element.prev().find('.bundle-configure').click();
        }
    }

    // Removes disabled classes and indicators from a button,
    // and adds enabled classes and indicators.
    function enableButton($button) {
        if (!$button.length) {
            return false;
        }

        $button.removeAttr('disabled');

        // Switch from green to grey as a visual indicator.
        $button.removeClass('button-disabled');
        $button.addClass('button-green');

        return true;
    }

    // Removes enabled classes and indicators from a button,
    // and adds disabled classes and indicators.
    function disableButton($button) {
        if (!$button.length) {
            return false;
        }

        $button.attr('disabled', 'disabled');

        // Switch from grey to green as a visual indicator.
        $button.removeClass('button-green');
        $button.addClass('button-disabled');

        return true;
    }

    // Given a selector of all affected forms, change the form submission to
    // an AJAX (POST) request.
    // Automatically disables (and leaves disabled) the submit buttons to prevent multiple submissions.
    function handleBasketManipulationForms(forms) {
        $(document).on('submit', forms, function(evt) {
            var $currentForm = $(this);
            var $formSubmit = $currentForm.find('button[type="submit"]');
            var isAddForm = $currentForm.is('.product-addition-action');
            // Changes /s/Add-To-Basket to /s/Add-To-Basket-AJAX (suffixing -AJAX = AJAX request).
            var formAJAXUrl = $currentForm.attr('action') + '-AJAX';

            // Prevent form submission
            evt.preventDefault();

            // Disable the submit button for further submissions.
            $formSubmit.attr('disabled', 'disabled');

            // Make an AJAX request to the relevant URL to alter the session.
            $.post(formAJAXUrl, $currentForm.serialize())
                // On success, leave the button disabled.
                .done(function() {
                    // If the button is for adding to the basket, we change the label to 'Pending'
                    // to indicate a pending addition.
                    if (isAddForm) {
                        $formSubmit.find('span').text('Added to Basket');
                        disableButton($formSubmit);
                        CommonBasket.increaseBasketcount();
                        // Display the continue button, as we've added new items to the basket.
                        $('#updateAccount').show();

                        // Show the remove from basket button.
                        var $removeFromBasketForm = $formSubmit.parents('form').parent().find('.unadded-product');
                        var $removeButton = $removeFromBasketForm.find('button');
                        $removeFromBasketForm.removeClass('unadded-product');
                        enableButton($removeButton);
                        $removeButton.removeClass('button-green');
                    }
                })
                // On failure, re-enable the button.
                .fail(function() {
                    $formSubmit.attr('disabled', '');
                });
        });
    }

    // Replaces a remove-from-account form with an add-to-basket form.
    function replaceRemoveForm($removeForm) {
        var $formContainer;
        var productId;

        // Opt-out if passed an empty element.
        if (!$removeForm.length) {
            return false;
        }

        // Retrieve the product id from the form.
        productId = $removeForm.find('input[name="remove-product"]').val();

        // Store a reference to the container of the remove form.
        $formContainer = $removeForm.parent();

        // Remove the form completely.
        $removeForm.remove();

        // Retrieve the markup for the form from the server.
        var formRequest = $.get('/s/Add-To-Basket-Form/' + productId, {}, 'html');

        // Add the markup to the page on success.
        formRequest.done(function(form) {
            if (form.length) {
                $formContainer.append(form);
            }
        });

        return true;
    }

    function createStandardDialog(id, title, dialogText) {
        // Create element.
        var confirmMarkup = '<div id="' + id + '" title="' + title + '">';
        confirmMarkup += '<p>' + dialogText + '</p>';
        confirmMarkup += '</div>';
        var $dialog = $(confirmMarkup);
        $('body').append($dialog);

        return $dialog;
    }

    function showStandardAlertDialog(title, message) {
        var $alert = retrieveAlertElement();
        $alert.attr('title', title);
        $alert.find('> p').text(message);
        $alert.dialog({
            resizable: false,
            height:140,
            modal: true,
            buttons: {
                'Ok': function() {
                    $alert.dialog('close');
                }
            }
        });
        return $alert;
    }

    var retrieveConfirmElement = function(productTitle) {
        var $confirm = $('#confirm-removal');

        // Assert that the element exists.
        if (!$confirm.length) {
            $confirm = createStandardDialog(
                'confirm-removal',
                'Remove product from account?',
                ''
            );
        }

        // Change the function to always return the confirm dialog element.
        retrieveConfirmElement = function(productTitle) {
            // Set the title of the product in the confirm window.
            $confirm.find('productTitle').text(productTitle);

            return $confirm;
        };
        return $confirm;
    };

    var retrieveProcessingElement = function() {
        var $dialog = $('#processing-removal');

        // Assert that the element exists.
        if (!$dialog.length) {
            $dialog = createStandardDialog(
                'processing-removal',
                'Removing product from account',
                'Removing the product from your account now...'
            );
        }

        // Change the function to always return the dialog element.
        retrieveProcessingElement = function() {
            return $dialog;
        };
        return $dialog;
    };

    var retrieveAlertElement = function() {
        var $dialog = $('#alert');

        // Assert that the element exists.
        if (!$dialog.length) {
            $dialog = createStandardDialog(
                'alert',
                '',
                ''
            );
        }

        // Change the function to always return the dialog element.
        retrieveAlertElement = function() {
            return $dialog;
        };
        return $dialog;
    };

    // Confirms the removal of a product by prompting for confirmation.
    function confirmProductRemoval($forms) {
        // Prevent form submission after asking for confirmation,
        // and redirect to action which will take care of the removal.
        $forms.submit(function() {
            var $currentForm = $(this);
            // Retrieve the title of the product to remove.
            var productName = $currentForm.parents('tr').find('.product-name').text();
            // Retrieve the confirm element
            var $confirm = retrieveConfirmElement(productName);
            // Retrieve it's text element.
            var $confirmText = $confirm.find('> p').html('Are you sure you want to remove the product <span class="productTitle">' + productName  + '</span>?');

            $confirm.dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    Yes: function() {
                        // Retrieve it's buttons.
                        var $confirmButtons = $confirm.parent().find('.ui-dialog-buttonpane button');
                        // Create a new element to show the processing element.
                        var $confirmProcessing = $('<div><p>Removing product from account…</p><img alt="processing" title="processing" src="/sfimages/ajaxLoaders/ajax-loader-bar-transparent-bg.gif" /></div>');

                        // Disable the buttons.
                        $confirmButtons.attr('disabled', 'disabled').addClass('button-disabled');

                        // Alter the text to notify the user we are dealing with the request.
                        $confirmProcessing.insertAfter($confirmText);
                        $confirmText.hide();

                        var cleanupProcessing = function() {
                            $confirmText.show();
                            $confirmProcessing.remove();
                            $confirmButtons.removeAttr('disabled').removeClass('button-disabled');
                            $confirm.dialog('close');
                        };

                        // If confirmed, redirect to the page which will handle removal.
                        $.post('/s/Remove-Product-AJAX', $currentForm.serialize())
                            .done(function() {
                                cleanupProcessing();

                                showStandardAlertDialog('Removed product', 'We have removed the product from your account as you requested.');

                                // Replace the remove form with an Add-Form
                                replaceRemoveForm($currentForm);
                            })
                            .fail(function() {
                                cleanupProcessing();

                                showStandardAlertDialog('Unable to remove the product', 'We were unable to remove the product from your account.');
                            });
                    },
                    Cancel: function() {
                        $confirm.dialog('close');
                    }
                }
            });

            // Deactivate form submission.
            return false;
        });
    }

    // Handles form for removing products from the basket.
    function handleBasketRemovalForms($removeFromBasketForms) {
        // Opt-out if we
        if (!$removeFromBasketForms.length) {
            return false;
        }

        // Disable submitting the form.
        $removeFromBasketForms.submit(function() {
            return false;
        });

        // On clicking the button, trigger an AJAX POST request to remove the product from the basket.
        $removeFromBasketForms.on('click', 'button', function(event) {
            event.preventDefault();

            // Skip handling this event, if a request is already in progress.
            if (requestInProgress) {
                return true;
            } else {
                requestInProgress = true;
            }
            var $button = $(this);
            var $form = $button.parents('form');
            var removeFromBasketUrl = $form.attr('action');

            var totalPrice = parseFloat($form.parents('tr').data('cost'));

            // Perform a simple validation of the product id.
            var productId = parseInt($button.parents('form').find('input[name="remove-product"]').val(), 10);
            if (productId === 0) {
                return false;
            }

            $.post(removeFromBasketUrl, $form.serialize(), 'json')
                .done(function() {
                    var $addButton;

                    // Disable and hide the remove button and its parent form.
                    disableButton($button);
                    $form.addClass('unadded-product');

                    // Decrease the basket count
                    CommonBasket.decreaseBasketCount(totalPrice);

                    // Remove the item from the tooltip.
                    CommonBasket.removeProductFromBasketTooltip(productId);

                    // Revert the add-form to its previous enabled state.
                    var $formCell = $form.parent();
                    var $addForm = $formCell.find('.product-addition-action');
                    if ($addForm.length) {
                        $addButton = $addForm.find('button');
                        enableButton($addButton);
                        $addButton.find('span').text('Add to Basket');

                        // In case of Basket BWMs, simply remove the entire row.
                        if (productId < 0) {
                            $formCell.parents('tr').remove();
                        }
                    } else {
                        // Handle the edge case of the BWM row.
                        // Re-enable the button.
                        $addButton = $formCell.find('> button');
                        $addButton.attr('class', 'button-green bwm-configure');
                        $addButton.removeAttr('disabled');
                        $addButton.find('span').text('Select');
                        // Reset the row class.
                        $formCell.parents('tr').removeClass('bwm-row-configured').addClass('bwm-row');
                    }
                })
                .fail(function() {
                    // Notify the user that we failed to remove the product from the basket.
                    showStandardAlertDialog('Failure to remove from basket', 'Unfortunately we were unable to remove the product from your basket.');
                })
                .always(function() {
                    // Undo the flag we set before we fired this AJAX request.
                    requestInProgress = false;
                });

            return true;
        });

        return true;
    }

    /**
     * Handles the event on which a new BWM is added to the products table.
     */
    function handleNewlyAddedBasketBWMs() {
        $('.bwm-row').on('pwn.new-bwm-row', function(event, $newRow) {
            var $removeFromBasketForms = $('.product-basket-removal-action', $newRow);
            handleBasketRemovalForms($removeFromBasketForms);
        });
    }

    function handleCustomTopUpAmount() {
        var $radioField = $('[name="topup[topup-amount]"][value="input"]');
        $('[name="topup[topup-amount-input]"]').keyup(function () {
            $radioField.attr('checked', 'checked');
        });
    }

    function handleHidingPopupsOnProductConfiguration() {
        $('button.bwm-configure, button.credit-configure').click(function () {
            $('.tooltip-close:visible').click();
        });
    }

    function handleBundleForms($forms) {
        var openFormButtonClass = '.bundle-configure';

        function hideForm($form) {
            return $form.parents('tr').slideUp(500);
        }

        function showConfigureButton($form) {
            hideForm($form).prev().find(openFormButtonClass).show();
        }

        $(openFormButtonClass).click(function() {
            $(this).parents('tr').next().slideDown(500)
                .find('.addon-bundle-form-row.addon-bundle-field:hidden').show().end()
                .end().end().hide();
        });
        $forms.each(function () {
            var $form = $(this);
            var $formParent = $form.parents('tr').prev();

            $form.find('button.cancel').click(function () {
                showConfigureButton($form);
            });

            $form.initMypwnForm({
                messagePos: 'below',
                success: function(response) {
                    $formParent.parents('table').find(openFormButtonClass).hide()
                        .end().find('.bundle-configure-disabled').show()
                        .end().end().find('button.bundle-configured').show()
                        .end().find('.bundle-configure-disabled').hide();

                    if ($.isArray(response['removed_products'])) {
                        var $removeItem;
                        for (var i = 0, list = response.removed_products, max = list.length; i < max; i++) {
                            $removeItem = CommonBasket.retrieveProductById(list[i]);
                            CommonBasket.decreaseBasketCount($removeItem.data('cost'));
                            CommonBasket.removeProductFromBasketTooltip(list[i]);
                        }
                        alert("We have removed the PAYG call credit you have selected, as that product is not compatible with a Bundle.");
                    }

                    $('#credit-disabled-button').show();
                    $('#credit-added-button, #credit-select-button').hide();
                    hideForm($form);

                    CommonBasket.increaseBasketcount(response.cost);
                    CommonBasket.addBundleToBasketTooltip(
                        response.id,
                        response.cost,
                        response.monthly_cost,
                        response.label
                    );
                    CommonBasket.showBasketTooltip();

                    $('#updateAccount').show();
                    $('#header-purchase-credit').attr('disabled', 'disabled')
                        .removeClass('button-orange')
                        .addClass('button-disabled');
                },
                error: function(response) {
                    if (response.responseText && response.responseText.constructor === String) {
                        var responseObj = $.parseJSON(response.responseText);
                        if ($.isPlainObject(responseObj) && responseObj.reload_form === true) {
                            window.location.hash = $form.parents('tr').attr('id');
                            window.location.reload(true);
                        }
                        if (responseObj.error_messages && $.isArray(responseObj.error_messages)) {
                            checkForAndHandleTCError($form, responseObj);
                        }
                    }
                }
            });
        });
    }

    function checkForAndHandleTCError($form, responseObj) {
        var firstError = responseObj.error_messages.shift();
        if (firstError.field_name && firstError.field_name === 'bundle-tc') {
            $form.find('> .error.message').remove();
            $form.find('[name="bundle-tc"]').one('change', handleTCChangeAfterError);
        }
    }

    function handleTCChangeAfterError() {
        var $errorTC = $('#bundle-tc-error-box');
        if ($(this).is(':checked') && $errorTC.length) {
            $errorTC.remove();
        }
    }

    function handleBundleProductDialog() {
        $('.product-bundle-dialog-link').click(function() {
            var $trigger = $(this);
            if ($trigger.data('dialog-id')) {
                $($trigger.data('dialog-id')).dialog('open');
            } else {
                var $dialogContents = $trigger.parents('.product-desc').find('.bundle-product-dialog');
                $dialogContents.dialog({
                    minWidth: 750,
                    modal: true,
                    closeOnEscape: true,
                    width: 940,
                    minHeight: 630
                });
                $trigger.data('dialog-id', '#' + $dialogContents.attr('id'));
            }
        });
        $('.bundle-product-dialog .close-control a').click(function(event) {
            event.preventDefault();
            $(this).parents('.bundle-product-dialog').dialog('close');
        });
    }

    function handleTooltips() {
        $('.product-name-cell, .product-name.credit, tr.bundle .product-name, tr.bundle .bundle-description-line').click(function () {
            if ($(this).parent().find('.tooltip').is(':visible')) {
                $(this).parent().find('.tooltip').hide();
            } else {
                $(this).parent().parent().find('.tooltip').hide();
                $(this).parent().find('.tooltip').show();
            }
        });
    }

    function handleWeeklyCallsAndBundleDescriptionTooltips() {
        $('.product-name-cell, .product-name.credit, tr.bundle .product-name, tr.bundle .bundle-description-line').hover(function () {
            $('.tooltip').hide();

            if ($(this).is('.product-name-cell')) {
                $(this).find('.product-name').click();
            } else {
                $(this).click();
            }
        }, function () {
            $('.tooltip').hide();
        });
    }

    function handleAddOnDenial() {
        $('.addon-bundle-denial span').click(function() {
            $(this).parents('.addon-bundle-field').slideUp(500);
        });
    }

    // Handle Add-Product actions:
    handleBasketManipulationForms('.product-addition-action');

    // Handle product-removal actions:
    var $removeForms = $('.product-removal-action');
    confirmProductRemoval($removeForms);

    // Handle product-basket removal actions.
    var $removeFromBasketForms = $('.product-basket-removal-action');
    handleBasketRemovalForms($removeFromBasketForms);

    // Handle new BWM rows added by the inline form.
    handleNewlyAddedBasketBWMs();

    handleCustomTopUpAmount();
    handleHidingPopupsOnProductConfiguration();
    handleBundleForms($('tr.bundle.form form'));

    handleTooltips();
    handleBundleProductDialog();
    handleWeeklyCallsAndBundleDescriptionTooltips();
    handleAddOnDenial();

    handleBundleFormIdInHash();
});