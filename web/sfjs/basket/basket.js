$(document).ready(function() {

    // @todo: most of this is a direct copy from select_products.js; find a better way to share JS code.

    /**
     * Check if the basket is empty.
     *
     * @return {Boolean}
     */
    function isBasketEmpty() {
        return $('.added-products table tbody > tr').length === 0;
    }

    /**
     * Remove all parts of the basket table and form, and show the empty-basket message.
     */
    function handleEmptyBasket() {
        $('.basket-content').remove();
        $('.empty-basket-message').addClass('show');
    }

    function removeBasketProduct(totalPrice, productId, $form) {
        CommonBasket.decreaseBasketCount(totalPrice);
        CommonBasket.removeProductFromBasketTooltip(productId);
        CommonBasket.updateBasketTotalValue();

        // Remove the row and any form it might have attached from the basket.
        var $row = $form.parents('tr');
        if ($row.next().is('.editable-credit-form-row')) {
            $row.next().remove();
        }
        $row.slideUp('fast', function() {
            $row.remove();

            // Handle an empty basket, if any.
            if (isBasketEmpty()) {
                handleEmptyBasket();
            }
        });


        // Remove the remove-button to prevent multiple calls.
        $form.remove();
    }

    function retrieveTotalPriceFromForm($form) {
        return parseFloat($form.parents('tr').data('cost').substr(1));
    }

    // Handles the removal of added basket products.
    function handleRemoveFromBasketForms($removeFromBasketForms) {
        if (!$removeFromBasketForms.length) {
            return false;
        }

        // Prevent submitting the form.
        $removeFromBasketForms.submit(function() {
            return false;
        });

        // When clicking on the button, remove the item from the basket.
        var requestInProgress = false;
        $removeFromBasketForms.find('button').click(function(event) {
            event.preventDefault();

            // Skip handling this event, if a request is already in progress.
            if (requestInProgress) {
                return true;
            } else {
                requestInProgress = true;
            }

            var $button = $(this);
            var $form = $button.parents('form');
            var productId = parseInt($form.find('input[name="remove-product"]').val(), 10);
            // Search for the related row (in case the product is a bundle with an add-on)
            var $relatedRow = $('.addon-' + productId + ', .bundle-actions-row');
            var removeFromBasketUrl = $form.attr('action');
            var totalPrice = retrieveTotalPriceFromForm($form);

            // Perform a simple validation of the product id.
            if (productId === 0) {
                return false;
            }

            $.post(removeFromBasketUrl, $form.serialize(), 'json')
                .done(function(response) {
                    removeBasketProduct(totalPrice, productId, $form);
                    if ($relatedRow.length) {
                        $relatedRow.remove();
                    }

                    var $totalCost = $('.total-cost-value');
                    var currentTotal = $totalCost.data('basket-total');
                    if (!currentTotal.toFixed) {
                        currentTotal = parseFloat(currentTotal);
                    }
                    currentTotal -= totalPrice;
                    $totalCost.data('basket-total', currentTotal).text(currentTotal.toFixed(2));

                    if (response !== undefined && response && $.isArray(response.removed_ids)) {
                        for (var list = response.removed_ids, i = 0, max = list.length; i < max; i++) {
                            var $subForm = $(':hidden[value="' + list[i] + '"]').parents('form');
                            var subPrice = retrieveTotalPriceFromForm($subForm);
                            removeBasketProduct(subPrice, list[i], $subForm);
                        }
                        alert(response.message);
                    }
                })
                .fail(function() {
                    // Notify the user that we failed to remove the product from the basket.
                    // @todo: how to notify the user?
                })
                .always(function() {
                    // Undo the flag we set before we fired this AJAX request.
                    requestInProgress = false;
                });

            return true;
        });

        return true;
    }

    function retrieveTopUpAmountValue($topUpAmount, $customTopUpAmount) {
        var selectedValue = $topUpAmount.filter(':checked').val();
        if (selectedValue === 'input') {
            selectedValue = $customTopUpAmount.val();
        }
        return selectedValue;
    }

    function handleEditableCreditForm() {
        var $form = $('#editable-credit-form');
        var oneOffChargeValue = null;
        var $topUpAmount = $('[name="topup[topup-amount]"]');
        var $customTopUpAmount = $('[name="topup[topup-amount-input]"]');
        var $oneOffCharge = $form.parents('tr').prev().find('.one-off.charge');
        var $totalCharge = $form.parents('tr').prev().find('.total.charge');
        var productId = $form.find('[name=product_id]').val();

        $form.initMypwnForm({
            messagePos: 'below',
            successMessagePos: 'below',
            debug: 'off',
            html5Validation: 'off',
            requiredAsterisk: 'off',
            beforeSubmit: function() {
                if ($topUpAmount.length && $customTopUpAmount.length) {
                    oneOffChargeValue = retrieveTopUpAmountValue($topUpAmount, $customTopUpAmount);
                }
                return true;
            },
            success: function(response) {
                if (oneOffChargeValue !== null) {
                    CommonBasket.updateProductPrice(productId, oneOffChargeValue);
                    CommonBasket.updateBasketTotalValue();

                    oneOffChargeValue = '£' + oneOffChargeValue;
                    $oneOffCharge.text(oneOffChargeValue);
                    $totalCharge.text(oneOffChargeValue);
                }
            },
            error: function(response) {}
        });
    }

    function handleCustomTopUpAmount() {
        var $field = $('[name="topup[topup-amount-input]"]');
        var $radioField = $('[name="topup[topup-amount]"][value="input"]');
        var $amountWithVat = $('.topup-amount-with-vat');
        $field.keyup(function() {
            $radioField.attr('checked', 'checked');
            var value = parseInt($field.val(), 10) * 1.2;
            if (isNaN(value)) {
                value = '--';
            } else {
                value = value.toFixed(2);
            }
            $amountWithVat.text(value);
        });
    }

    function removeBundleActions() {
        var $bundleActionsParent = $('.bundle-actions-row');
        var $bundleActions = $bundleActionsParent.find('.bundle-actions');
        if ($bundleActions.length) {
            $bundleActionsParent.remove();
        }
    }

    function handleUpgradeBundleForm() {
        var $upgradeBundleForm = $('#upgrade-bundle');
        if ($upgradeBundleForm.length) {
            $upgradeBundleForm.initMypwnForm({
                messagePos: 'none',
                debug: 'off',
                html5Validation: 'off',
                requiredAsterisk: 'off',
                success: function() {
                    $upgradeBundleForm.remove();
                    window.location.reload();
                }
            });
        }
    }

    function handleAgreeToTAndC() {
        var $agreeToTAndC = $('#bundle-tc');
        if ($agreeToTAndC.length) {
            $agreeToTAndC.change(function() {
                if ($agreeToTAndC.is(':checked')) {
                    $.post($agreeToTAndC.data('url'))
                        .done(function(responseArr) {
                            dataLayer.push({'event': 'ProductSelectorTool/Basket'});
                            $agreeToTAndC.parents('.bundle-tc-action').remove();
                            removeBundleActions();
                        });
                }
            });
        }
    }

    function handleAddOnDenial() {
        $('.addon-bundle-denial span').click(function() {
            $(this).parents('.addon-bundle-field').slideUp(500);
            if (!$('#bundle-tc, .pin-pair-selection').is(':visible')) {
                removeBundleActions();
            }
        });
    }

    function validateBasketSubmit() {
        $('#basket-checkout').submit(function(e) {
            var $bundleTC = $('#bundle-tc');
            if ($bundleTC.length && !$bundleTC.is(':checked') && $bundleTC.is(':visible')) {
                e.preventDefault();
                alert('Please check the box to confirm you have read and accept the Terms and Conditions.');
            }
        });
    }

    function handlePinPairSelection() {
        var $pinPairContainer = $('.pin-pair-selection-fields');
        var $pinPair = $pinPairContainer.find('#user');
        if ($pinPair.length) {
            $pinPair.change(function() {
                $.post($pinPairContainer.data('url'), { user: $pinPair.val() });
            });
        }
    }

    handleRemoveFromBasketForms($('.product-basket-removal-action'));
    handleEditableCreditForm();
    handleCustomTopUpAmount();
    handleUpgradeBundleForm();
    handleAgreeToTAndC();
    handleAddOnDenial();
    validateBasketSubmit();
    handlePinPairSelection();
});
