$(document).ready(function() {
    var $container = $('.basket-page');
    if ($container.length && $container.data('bundle-notification')) {
        $('#accountHasBundle-modal').dialog('open');
    }
});