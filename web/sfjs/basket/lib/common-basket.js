/**
 * Common functions to use for the basket and relevant pages, such as select-products.
 *
 * Must be included before all other dependent scripts, because of selected DOM elements.
 */

var CommonBasket = {};

$(document).ready(function() {
    var $plusHeader = $('.plus-header');
    var $basketCount = $('#basket-items');
    var $basketTooltip = $('.basket-contents');
    var $basketTooltipProductPrototype = $basketTooltip.find('.product.prototype');
    var tooltipProductIdPrefix = 'tooltip-product-';
    var basketShowClass = 'new-purchased';
    var $basketTotalValue = $('.total-cost span.total-cost-value');

    function getCurrentcost() {
        return parseFloat($plusHeader.data('cost'));
    }

    function setCurrentCost(cost) {
        $plusHeader.data('cost', cost);
    }

    function addTooltipHideAfterPurchase() {
        setTimeout(hideTooltipIfShown, 10000);
        $basketTooltip.one('mouseover', hideTooltipIfShown);
    }

    function hideTooltipIfShown() {
        if ($basketTooltip.hasClass(basketShowClass)) {
            $basketTooltip.removeClass(basketShowClass);
        }
    }

    function updateBasketToolTipTotal() {
        $basketTooltip.find('tfoot .price').text('£' + CommonBasket.getCurrentBasketCost().toFixed(2));
    }

    /**
     * @param {Number|String} val
     * @returns {Number}
     */
    function ensureFloat(val) {
        if (val.toFixed === undefined) {
            return parseFloat(val);
        }
        return val;
    }

    function addProductPrototypeToBasketTooltip(id, prototypeManipulator) {
        var $tooltipRow = $basketTooltipProductPrototype.clone(true);
        $tooltipRow.removeClass('prototype').attr('id', tooltipProductIdPrefix + id);
        prototypeManipulator($tooltipRow);
        $basketTooltip.find('.products').prepend($tooltipRow);
        $basketTooltip.addClass('has-products')
            .find('tfoot .price').text('£' + CommonBasket.getCurrentBasketCost().toFixed(2));
        return $tooltipRow;
    }

    function addProductToBasketTooltip(id, priceStr, labelStr) {
        return addProductPrototypeToBasketTooltip(id, function ($prototype) {
            $prototype.find('.price').text(priceStr).end()
                .find('.label').text(labelStr).end();
        });
    }

    function addBundleToBasketTooltip(id, priceStr, labelStr) {
        return addProductPrototypeToBasketTooltip(id, function ($prototype) {
            $prototype.find('.price').text(priceStr).end()
                .find('.label').html(labelStr).end();
        });
    }

    CommonBasket.getCurrentBasketCost = function() {
        return getCurrentcost();
    };

    CommonBasket.setBasketTitle = function(productCount) {
        if (productCount === 0) {
            $basketLink.attr('title', 'Your basket is empty');
        } else {
            $basketLink.attr('title', 'Your basket value is £' + getCurrentcost().toFixed(2));
        }
    };

    CommonBasket.decreaseBasketCount = function(cost) {
        var currentCount = parseInt($basketCount.text(), 10);
        if (currentCount > 0) {
            currentCount--;
            $basketCount.text(currentCount);

            // update the basket title.
            if (cost) {
                setCurrentCost(getCurrentcost() - cost);
            }
        }
    };

    CommonBasket.increaseBasketcount = function(cost) {
        var currentCount = parseInt($basketCount.text(), 10);
        currentCount++;
        $basketCount.text(currentCount);

        // update the basket title.
        if (cost) {
            cost = ensureFloat(cost);
            var n = (getCurrentcost() + cost).toFixed(2);
            setCurrentCost(n);
        }
    };

    CommonBasket.addProductToBasketTooltip = function(id, cost, productName) {
        var costStr = ensureFloat(cost);
        var $row = addProductToBasketTooltip(id, '£' + costStr.toFixed(2), productName);
        $row.data('cost', cost);
    };

    CommonBasket.addBundleToBasketTooltip = function(id, cost, monthlyCost, productName) {
        cost = ensureFloat(cost);
        monthlyCost = ensureFloat(monthlyCost);
        var productNameHTML = '<span>' + productName + ' (£' + monthlyCost.toFixed(2) + ')</span><br /><em>(current month pro-rata charge)</em>';
        var $row = addBundleToBasketTooltip(id, '£' + cost.toFixed(2), productNameHTML);
        $row.data('cost', cost);
    };

    CommonBasket.retrieveProductById = function(id) {
        return $basketTooltip.find('#' + tooltipProductIdPrefix + id);
    };

    CommonBasket.removeProductFromBasketTooltip = function(id) {
        CommonBasket.retrieveProductById(id).remove();
        $basketTooltip.find('tfoot .price').text('£' + CommonBasket.getCurrentBasketCost().toFixed(2));
        if (!$basketTooltip.find('.product:not(.prototype)').length) {
            $basketTooltip.removeClass('has-products');
        }
    };

    CommonBasket.updateProductName = function(id, name) {
        $basketTooltip.find('#' + tooltipProductIdPrefix + id + ' .label').text(name);
    };

    CommonBasket.updateProductPrice = function(id, price) {
        var $price = $basketTooltip.find('#' + tooltipProductIdPrefix + id + ' .price');
        var currentPrice = parseFloat($.trim($price.text()).substr(1));
        var newTotal = parseFloat(CommonBasket.getCurrentBasketCost()) - currentPrice + parseFloat(price);
        setCurrentCost(newTotal);
        updateBasketToolTipTotal();
        $price.text('£' + price);
    };

    CommonBasket.showBasketTooltip = function() {
        $basketTooltip.addClass(basketShowClass);
        addTooltipHideAfterPurchase();
    };

    CommonBasket.updateBasketTotalValue = function() {
        if ($basketTotalValue.length) {
            $basketTotalValue.text(getCurrentcost().toFixed(2));
        }
    };
});
