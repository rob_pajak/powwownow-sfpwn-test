var showSocialDialog = function(){
    $("#social-dialog-message").dialog({
        modal: true,
        width: 500,
        height: 345,
        dialogClass: 'social-dialog',
        closeOnEscape: false,
        resizable: false,
        close: function( event, ui) {
            $.cookie('social_media_connect', 'N', {expires: 30, path: '/'});
        }
    });
};

var hideSocialDialog = function(){
    $("#social-dialog-message").dialog( "close" );
};

var tryToShowSocialDialog = function () {
    if ($.cookie('social_media_connect') != 'E' && $.cookie('social_media_connect') != 'N') {
        if (Math.floor((Math.random() * 100) + 1) % 10 == 0) {
            $.cookie('social_media_connect', 'Y', { expires: 30, path: '/' });
            dataLayer.push({'event': 'OAuth - Social Dialog Open/onClick'});
            showSocialDialog();
        } else {
            $.cookie('social_media_connect', 'E', { expires: 30, path: '/' });
        }
    }
};