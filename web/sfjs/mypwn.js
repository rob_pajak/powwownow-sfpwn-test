 /**
 *   Global myPwnApp object
 *   version: 0.4 (19-December-2012)
 *   by VK
 *   @requires jQuery >= 1.7.1
 *
 */

var myPwnApp  = {}; //global mypwn object

$(function() {

    /**
     * Prepares a DNIS type to be used as a class name.
     *
     * @param dnisType
     * @return string
     */
    function dnisTypeToClassName(dnisType) {
        return dnisType.toLowerCase().replace(/[\-_ ]+/, '_');
    }

    /**
     * Groups selected DNIS numbers per dnis type.
     *
     * @param row
     * @param bwmCharges
     * @return object|Boolean
     */
    function groupSelectedNumbers(row, bwmCharges) {
        if (!$.isArray(row)) {
            return false;
        }

        var i, max = row.length;
        var dnis, dnisType;
        var grouped = {};

        // Group the selected numbers into buckets.
        for (i = 0; i < max; i++) {
            dnis = row[i];
            if ($.isArray(dnis) && dnis.length >= 5) {
                dnisType = dnis[3];
                if (!grouped.hasOwnProperty(dnisType)) {
                    grouped[dnisType] = [];
                }
                grouped[dnisType].push(dnis);
            }
        }

        // Group the selected numbers into simpler data structures of "type", "dnisTypeClass", "count" and "totalFee":
        var result = {};
        for (dnisType in grouped) {
            if (grouped.hasOwnProperty(dnisType)) {
                result[dnisType] = {
                    type: dnisType,
                    dnisTypeClass: dnisTypeToClassName(dnisType),
                    count: grouped[dnisType].length,
                    totalFee: bwmCharges.dnisPerRate * grouped[dnisType].length
                };
            }
        }

        return result;
    }

    /**
     * Alters a table to display the grouped prices.
     *
     * @param $table
     *   The context for all jQuery calls.
     * @param groupedNumbers
     *   An object which contains all grouped data.
     * @param appendCallback
     *   Function to append new row the table.
     */
    function displayGroupedPrices($table, groupedNumbers, appendCallback) {
        var dnisType;
        var dnis;
        var $existingDnisRow;
        var dnisClasses = [];
        var classSelector;

        for (dnisType in groupedNumbers) {
            if (groupedNumbers.hasOwnProperty(dnisType)) {
                dnis = groupedNumbers[dnisType];
                classSelector = '.' + dnis.dnisTypeClass;
                dnisClasses.push(classSelector);
                $existingDnisRow = $(classSelector, $table);

                // If there isn't, add in a new row after the connection fee row and populate it.
                if (!$existingDnisRow.length) {
                    appendCallback($('<tr class="selected-dnis ' + dnis.dnisTypeClass + '"><td></td><td class="bwm-numbers-price"></td></tr>'), $table);
                    $existingDnisRow = $(classSelector, $table);
                }

                // Replace the content of the row.
                $existingDnisRow.find('td:nth(0)').html(dnisType + ' &times; ' + dnis.count);
                $existingDnisRow.find('td:nth(1)').html(myPwnApp.options.currencySign + dnis.totalFee);
            }
        }

        // Delete all non-affected rows.
        if (dnisClasses.length) {
            $('.selected-dnis:not(' + dnisClasses.join(', ') + ')', $table).remove();
        }
    }

    // pre-load prices
    function calculatePrices(row, bwmCharges) {
        // Group the selected numbers based on type.
        var groupedNumbers = groupSelectedNumbers(row, bwmCharges);
        var $selectedRates = $('#selectedRates');
        var $connectionFeeRow = $('tr.connection-fee');

        // If we have numbers, alter the rows of the numbers table accordingly.
        if (groupedNumbers) {
            var $oneOffPriceRow = $('td.bwm-one-off-price');

            // Show the connection fee and total fee as normal.
            $oneOffPriceRow.html(myPwnApp.options.currencySign + bwmCharges.connectionFee);
            $('td.bwm-total-price').html(myPwnApp.options.currencySign + (row.length * bwmCharges.dnisPerRate + bwmCharges.connectionFee));

            displayGroupedPrices($oneOffPriceRow.parents('table'), groupedNumbers, function($newElement, $table) {
                $newElement.insertAfter($connectionFeeRow);
            });
        }
    }

    myPwnApp.utils = {}; //normally for cross-browser solutions or better implementations
    myPwnApp.common = {}; // common functionality for various pages across the website
    myPwnApp.products = {}; // products specific functionality
    myPwnApp.bwm = {}; // branded welcome message specific functionality
    myPwnApp.bwm.init = {}; // branded welcome message specific functionality
    myPwnApp.dialinnumbers = {}; // Dial-in Numbers Functionality
    myPwnApp.requestWelcomePack = {}; // Request Welcome Pack Functionality
    myPwnApp.callSettings = {}; // Request Welcome Pack Functionality
    myPwnApp.createPin = {}; // Create Pin Functionality
    myPwnApp.createUser = {}; // Create User Functionality
    myPwnApp.pins = {}; // PIN Functionality
    myPwnApp.users = {}; // Users Functionality
    myPwnApp.index = {}; // Homepage Dashboard Functionality
    myPwnApp.recordings = {}; // Recordings Functionality
    myPwnApp.callhistory = {}; // Call History Functionality
    myPwnApp.webConferencing = {}; // Web Conferencing Functionality
    myPwnApp.userDetails = {}; // User Details Functionality
    myPwnApp.options = {}; //global options

    // init global options for myPwn
    myPwnApp.options.init = function () {
        myPwnApp.options.currencySign = '&pound;';
    };

    // cross-browser console.log support (if it doesn't exist - just fake it)
    myPwnApp.utils.consoleSupport = function () {
        if (typeof console === "undefined") {
            window.console = {};
            window.console.log = function () { return; };
        }
    };

    myPwnApp.utils.scrollUp = function() {
        $('body').on('click', 'a.scroll-up', function () {
            $('html, body').animate({ scrollTop: 0 }, 600);
            return false;
        });
    };

    // function to highlight elements
    myPwnApp.utils.highlightEffect = function() {

        $.fn.highlight = function (o) {

            o = o || {}; // options object
            o.options = o.options || {}; // sub options

            return this.queue(function() {

                // Create element
                var el = $(this),
                    props = ['backgroundImage','backgroundColor','opacity'],
                    mode = o.options.mode || 'show', // Set Mode
                    color = o.options.color || "#ffff99", // Default highlight color
                    oldColor = el.css("backgroundColor"),
                    childColor = el.children().css("backgroundColor"),
                    duration = o.duration || 300;

                // Adjust
                //$.effects.save(el, props); el.show(); // Save & Show
                el.css({backgroundImage: 'none', backgroundColor: color}); // Shift
                el.children().css({backgroundImage: 'none', backgroundColor: color}); // Shift child as well

                // Animation
                var animation = {backgroundColor: oldColor };
                if (mode == "hide") animation['opacity'] = 0;

                // Animate
                el.animate(animation, { queue: false, duration: duration, complete: function() {
                    if(mode == "hide") el.hide();
                    el.css({backgroundColor: oldColor}); // restore
                    el.children().css({backgroundColor: childColor}); // restore child as well

                    if (mode == "show" && $.browser.msie) this.style.removeAttribute('filter');
                    if(o.callback) o.callback.apply(this, arguments);
                    el.dequeue();
                }});

            });
        };

    };

    // Output the Message from Either the Success and Error Methods
    myPwnApp.utils.outputMessage = function (formId,responseText,msgType,redirectOnSuccess,reload) {
        // Check if the Message is a Global
        if (window[responseText.message] !== undefined) {
            responseText.message = window[responseText.message];
        }

        // Show Message
        if ('alert' == responseText.field_name) {
            $(formId).find('.message').remove();
            switch (msgType) {
                case 'error':
                    $(formId).append('<div class="error message grid_24">'+responseText.message+'</div>');
                    $(formId).find('.message').delay(5000).fadeOut('slow');

                    if (false !== reload) {
                        window.setTimeout(function (){ window.location.reload(); }, 5500);
                    }
                    break;
                case 'success':
                    $(formId).append('<div class="success message grid_24">'+responseText.message+'</div>');
                    $(formId).find('.message').delay(5000).fadeOut('slow');

                    if (false !== redirectOnSuccess && window[redirectOnSuccess] !== undefined) {
                        window.setTimeout(function (){ window.location = window[redirectOnSuccess]; }, 5500);
                    } else if (false !== redirectOnSuccess && typeof redirectOnSuccess !== 'undefined') {
                        window.setTimeout(function (){ window.location = redirectOnSuccess; }, 5500);
                    } else if (false !== reload) {
                        window.setTimeout(function (){ window.location.reload(); }, 5500);
                    }

                    break;
                default:
                    // Do Something
                    break;
            }
        } else {
            // Do Something
        }
    };

    // Obtain the Correct Message
    myPwnApp.utils.obtainMessage = function (responseArr,msgType) {
        var responseTxt  = $.parseJSON(responseArr),
            responseText = $.parseJSON(responseArr.responseText),
            message      = '';

        // Most Common Success
        if ("success" == msgType && null !== responseArr && typeof responseArr === "object" && "success_message" in responseArr) {
            message = responseArr.success_message;
        } else if ("success" == msgType && null !== responseTxt && typeof responseTxt === "object" && "success_message" in responseTxt) {
            message = responseTxt.success_message;
        } else if ("success" == msgType && null !== responseArr && responseArr === "object" && "responseText" in responseArr && "success_message" in responseArr.responseText) {
            message = responseArr.responseText.success_message;
        } else if ("success" == msgType && null !== responseText && typeof responseText === "object" && "success_message" in responseText) {
            message = responseText.success_message;
        }

        // Most Common Errors
        if ("error" == msgType && null !== responseArr && typeof responseArr === "object" && "error_messages" in responseArr) {
            message = responseArr.error_messages;
        } else if ("error" == msgType && null !== responseTxt && typeof responseTxt === "object" && "error_messages" in responseTxt) {
            message = responseTxt.error_messages;
        } else if ("error" == msgType && null !== responseArr && responseArr === "object" && "responseText" in responseArr && "error_messages" in responseArr.responseText) {
            message = responseArr.responseText.error_messages;
        } else if ("error" == msgType && null !== responseText && typeof responseText === "object" && "error_messages" in responseText) {
            message = responseText.error_messages;
        }

        return message;
    };

    myPwnApp.utils.init = function () {

        //list of methods to launch when the app loads

        myPwnApp.utils.consoleSupport(); //support console in legacy browsers

        // ajax loader image
        myPwnApp.utils.loaderImage = '<img src="/sfimages/ajaxLoaders/form-ajax-loader.gif" alt="Loading" class="loader-image" />';

        myPwnApp.utils.highlightEffect(); // highlight effect functionality

    };


    // global init for every page
    myPwnApp.init = function () {

        // login form init

        myPwnApp.utils.init();

        // init global options
        myPwnApp.options.init();

    };

    // branded welcome message functionality
    myPwnApp.bwm.init = function () {

        // array that holds selected numbers for BWM
        var row = [];

        myPwnApp.bwm.startEditing(row); // BWM edit button functionality and eventListener
    };

    myPwnApp.bwm.startEditing = function (row) {
        $('body').on('click', 'button.bwm-configure', function () {
            $('.tooltip').hide();
            var table = $('#products'),
                td = $(this).parent('td'),
                tr = td.parent('tr');

            $(this).hide(); // hide edit button
            $('#updateAccount').hide(); // hide update button until the flow is finished

            // get form partial
            $.getJSON('/s/products/BwmScriptConfigAjax').done(function(data) {
                var formHtml = data.html, // html partial for the form
                    formId = 'configure_bwm_script',
                    bwmScriptWelcomeMessage,
                    bwmScriptInstructionsMessage,
                    $bwmForm,
                    $bwmFormInputs,
                    $scriptWelcomeMessage,
                    $scriptInstructionsMessage;
                // add row to place configuration form there in formHtml variable
                tr.after('<tr class="bwm-row-container"><td colspan="4"><div class="bwm-form-container clearfix"><div class="hr-spotted-top png content-seperator"><!--blank--></div>'+ formHtml +'</td></tr>');
                $('.bwm-form-container').hide().slideDown('fast'); // hide newly created row and then animate down;

                // init form win pwnForm
                $bwmForm = $('#' + formId);
                $bwmFormInputs = $bwmForm.find('input');
                $scriptWelcomeMessage = $('#bwm-script1 blockquote');
                $scriptInstructionsMessage = $('#bwm-script2 blockquote');
                bwmScriptWelcomeMessage = $scriptWelcomeMessage.html() || '';
                bwmScriptInstructionsMessage = $scriptInstructionsMessage.html() || '';

                $bwmForm.initMypwnForm({
                    messagePos: 'off',
                    requiredAsterisk: 'off',
                    success: function (responseText) {
                        if (!responseText.bwmNumbersJson) {
                            responseText = $.parseJSON(responseText);
                        }

                        var el = $('.bwm-form-container');
                        //load the next step (add dedicated numbers)
                        el.html(responseText.html);

                        // load numbers form functionality and pass Json with numers and rates
                        myPwnApp.bwm.addNumbers(responseText.bwmNumbersJson, responseText.bwmCharges);
                    }
                });

                // Default RequiredAsterisk action puts the required note before the submit button.
                // However before the submit button is a Cancel Button, so place the line at the start of form-action.
                // $bwmForm.find('.required-note').prependTo('.form-action');
                $('.form-action').prepend('<p class="required-note" style="font-size: 10px">* Required field</p>');

                $bwmFormInputs.each(function () {
                    var val = $(this).val() || '________',
                        name = $(this).attr('name');

                    // Replace all placeholders (e.g. {contact_name} in the script with input values
                    bwmScriptWelcomeMessage = bwmScriptWelcomeMessage.replace(new RegExp('({' + name + '})', 'g'), '<span class="bwm-script-placeholder bwm-script-' + name + '">' + val + '</span>');
                    bwmScriptInstructionsMessage = bwmScriptInstructionsMessage.replace(new RegExp('({' + name + '})', 'g'), '<span class="bwm-script-placeholder bwm-script-' + name + '">' + val + '</span>');
                });

                // real-time update the script
                $bwmFormInputs.keyup(function() {
                    var val = $(this).val() || '________';

                    // change script placeholders
                    $('.bwm-script-' + $(this).attr('name')).html(val);
                });

                $scriptWelcomeMessage.html(bwmScriptWelcomeMessage);
                $scriptInstructionsMessage.html(bwmScriptInstructionsMessage);
            });
        });

        // cross in the right top corner of bwm
        // Cancel BWM flow
        $('body').on('click', 'button.bwm-cancel', function () {

            row = []; // clear Numbers array on cancel
            $('#updateAccount').show(); // show update account button


            // product label is set back to default
            $('.product-name.bwm').html('Branded Welcome Message');

            // slideUp and remove configuration container
            $('#products').find('.bwm-form-container').slideUp('fast', function () {
                $(this).parents('.bwm-row-container').remove();
            }); // slide up config container and remove it from DOM

            $('#products').find('.bwm-row button.bwm-configure').show(); // show edit button again

            return false;
            // @todo: API call to remove BWM from session
        });

        myPwnApp.bwm.changeName();

    };

    myPwnApp.bwm.changeName = function() {

        // Click behaviour
        myPwnApp.bwm.changeName.onClick = function (show,hide) {
            $('body').on('click', show, function () {
                $('.tooltip').hide();
                myPwnApp.bwm.changeName.showForm($(this));
            });
            $('body').on('click', hide, function () {
                $('.tooltip').hide();
                //we will pass the pencil related to this specific pencil
                var hereShow = $(this).parent().parent().parent().find(show);
                myPwnApp.bwm.changeName.showLable(hereShow);
            });
        };
        myPwnApp.bwm.changeName.showLable = function(here) {
            here.parent().find(".bwm-prod-wrapper .bwm-name").show();
            here.parent().find(".bwm-prod-wrapper .bwm-name-form").hide();
            here.parent().find(".product-name").css("border-bottom","1px dashed");
            here.parent().find(".bwm-prod-wrapper .bwm-name-form .error.message,.h5form-error.nice-error-box,.success.message").remove();
            here.show();
            here.parent().find(".bwm-name-form .bwm-name-input").val(here.parent().find('div.product-name').text());
        };
        myPwnApp.bwm.changeName.showForm = function(here) {
            here.parent().find(".bwm-prod-wrapper .bwm-name").hide();
            here.parent().find(".bwm-prod-wrapper .bwm-name-form").show();
            here.parent().find(".product-name").css("border-bottom","1px none");
            here.parent().find(".bwm-prod-wrapper .bwm-name-form .error.message,.h5form-error.nice-error-box,.success.message").remove();
            here.hide();
        };

        myPwnApp.bwm.changeName.initAjaxForm = function(formId) {
            $('#'+formId).initMypwnForm({
                messagePos: 'off',
                success: function (responseText) {
                    var data = $.parseJSON(responseText);
                    var $pencil = $(".product"+data.gr_id).parent().parent().find("img.bwm-pencil").eq(0);
                    var $closeTick = $pencil.parent().find('.red-cross');
                    $(".product"+data.gr_id).html(data.name);
                    var productId = $pencil.parent().find('input[name="session_id"]').val();
                    CommonBasket.updateProductName(productId, data.name);
                    $pencil.parent().find(".bwm-name-form .bwm-name-input").val(data.name);
                    myPwnApp.bwm.changeName.showLable($pencil,$closeTick);
                },
                error: function (responseText) {
                    // if field_name key in JSON is 'alert', show error message in alert box
                    var resp = $.parseJSON(responseText.responseText);
                    if (resp.error_messages[0].field_name === 'alert') {
                        alert(window[resp.error_messages[0].message]);
                    }
                }
            });
        };

        myPwnApp.bwm.changeName.onClick('img.bwm-pencil','.red-cross');
    };

    myPwnApp.bwm.addNumbers = function (bwmNumbersJson, bwmCharges) {

        var oTable = '',
            row = [],
        // Set the Languages
            oLanguage = {
                "sProcessing": DATA_TABLES_sProcessing,
                "sLengthMenu": DATA_TABLES_sLengthMenu,
                "sZeroRecords": DATA_TABLES_sZeroRecords,
                "sEmptyTable": DATA_TABLES_sEmptyTable,
                "sLoadingRecords": DATA_TABLES_sLoadingRecords,
                "sInfo": DATA_TABLES_sInfo,
                "sInfoEmpty": DATA_TABLES_sInfoEmpty,
                "sInfoFiltered": DATA_TABLES_sInfoFiltered,
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sSearch": DATA_TABLES_sSearch,
                "sUrl": "",
                "fnInfoCallback": null,
                "oPaginate": {
                    "sFirst":    '&lt;&lt;',
                    "sPrevious": '&lt;',
                    "sNext":     '&gt;',
                    "sLast":     '&gt;&gt;'
                }
            },

        // Set the Column Headings
            aoColumns = [
                {
                    "bSearchable": false,
                    "bVisible":    false
                },
                {
                    "sTitle": "Country",
                    "sWidth": "30%",
                    "bSortable": true,
                    "bSearchable": true
                },
                {
                    "sTitle": "City",
                    "sWidth": "30%",
                    "bSortable": true,
                    "bSearchable": true
                },
                {
                    "sTitle": "Type",
                    "sWidth": "30%",
                    "bSortable": true,
                    "bSearchable": true
                },
                {
                    "sTitle": "Rate per min",
                    "sWidth": "10%",
                    "bSortable": true,
                    "bSearchable": false
                }
            ];

        var that = this;
        var resourcesLoadedCallback = function () {

            var $addNumbers = $('#addNumbers');
            var $bwmCharges = $('#bwmCharges');

            // display submit button when numbers are selected
            function toggleSubmitButtonAndPrices() {

                // calculate prices first
                calculatePrices(row, bwmCharges);

                // then toggle submit button and prices container
                if (row.length === 0) {
                    $addNumbers.hide();
                    $bwmCharges.hide();
                } else {
                    $addNumbers.show();
                    $bwmCharges.show();
                }
            }

            toggleSubmitButtonAndPrices();

            window.oTableScreen = $('#bwmRates').dataTable({
                "aaData": bwmNumbersJson, // load bwm Numbers JSON in DataTable
                "aoColumns": aoColumns,
                "oLanguage" : oLanguage,
                "sPaginationType": "full_numbers",
                "bFilter": true,
                "bInfo": false,
                "bLengthChange": false,
                "iDisplayLength": 8,
                "fnDrawCallback":function(){

                }
            });

            //Use Clearfix on the Table Wrapper
            $('#bwmRates' + '_wrapper').addClass("clearfix");

            window.oTableScreen.fnSort([[0,'asc']]);

            $('#bwmRates th').append('<div></div>').css('text-decoration', 'underline').css('cursor', 'pointer');
            $('#bwmRates_filter input[type=text]').attr('placeholder','Enter country name');


            // highlight the row in Numbers table
            $('.bwm-form-container').on('mouseenter', 'table#bwmRates tr', function (event) {
                if (!$(this).children('td').hasClass('dataTables_empty')) {
                    $(this).addClass('highlight-row');
                }
            });

            // remove highlight class in Numbers table row
            $('.bwm-form-container').on('mouseleave', 'table#bwmRates tr', function (event) {

                // if there are numbers in the table, highlight on hover
                if (!$(this).children('td').hasClass('dataTables_empty')) {
                    $(this).removeClass('highlight-row');
                }
            });

            // On Numbers table click we add a row to Selected Numbers table
            $('.bwm-form-container').on('click', 'table#bwmRates tbody tr', function (event) {
                var i; // increment for the loop
                var current;
                var max;


                // if there are numbers in the table, select number on click
                if (!$(this).children('td').hasClass('dataTables_empty')) {

                    // add all data into row array (including hidden ids and stuff)
                    row.push(oTableScreen.fnGetData(this));
                    toggleSubmitButtonAndPrices();

                    // remove "No number selected" label
                    if ($('#selectedRates tbody tr:first-child').is('.bwm-no-number')) {
                        $('#selectedRates tbody tr.bwm-no-number').remove();
                    }

                    // add row with all values into Selected Numbers table
                    $('#selectedRates tbody').append('<tr id="' + row.length + '"></tr>');

                    for (i = 1, current = row[row.length -1], max = current.length - 1; i < max; i += 1) {
                        $('#selectedRates tbody tr:last-child').append('<td>' + ( current[i]  || "" )+ '</td>');
                    }

                    // highlight the row to attract user's attention to it
                    $('#selectedRates tbody tr:last-child').highlight();
                }
            });

            $('.bwm-form-container').on('mouseenter', 'table#selectedRates tr', function (event) {
                $(this).addClass('remove-row');

                // if it is a row with Selected Number, show delete button
                if (!$(this).hasClass('bwm-no-number')) {
                    var val = $(this).find('td:last-child').html();
                    $(this).find('td:last-child').css('position','relative').html('<div class="floatleft">' + val + '</div><div class="delete floatright">&times;</div>');
                }
            });

            $('.bwm-form-container').on('mouseleave', 'table#selectedRates tr', function (event) {
                $(this).removeClass('remove-row');
                $(this).find('.delete').remove();
            });

            // remove Selected Number from the table
            $('.bwm-form-container').on('click', '#selectedRates .delete', function (event) {
                var tr = $(this).parent('td').parent('tr'),
                    id = $(tr).attr('id'), // id of the row
                    num = 0; // increment

                $(tr).slideUp('fast', function () {

                    $(this).remove(); // remove from the table
                    row.splice(id-1,1); // remove element from array
                    toggleSubmitButtonAndPrices();

                    // redraw ids on each tr after deletion
                    $('#selectedRates tbody tr').each(function () {
                        num += 1;
                        $(this).attr('id', num);
                    });


                    if ($('#selectedRates tbody tr').length === 0) {
                        $('#selectedRates tbody')
                            .append('<tr class="bwm-no-number"><td colspan="4" class="aligncenter"><em>No number selected</em></td></tr>');

                        $('#selectedRates tbody tr:last-child')
                            .hide()
                            .fadeIn()
                            .highlight();
                    }
                });

            });

            // submit numbers in and proceed to the Summary step
            $('.bwm-form-container').on('click','button#addNumbers', function () {
                var url = '/s/products/BwmSelectedNumbersAjax',
                    request = $.ajax({
                        type: 'POST',
                        url: url,
                        data: 'json=' + JSON.stringify(row),
                        dataType: 'json'
                    });

                request.done(function(jqXHR, textStatus) {

                    // load Summary form into BWM container
                    $('.bwm-form-container').html(jqXHR.html);

                    // load final bwm step
                    myPwnApp.bwm.summary(row, bwmCharges);
                });

                request.fail(function(jqXHR, textStatus) {
                    var msg = $.parseJSON(jqXHR.responseText);

                    // alert evaluated error message to the user
                    alert(window[msg.error_messages[0].message]);
                });

                return false;
            });
        };

        resourcesLoadedCallback();

        $('body').on('click','a.filter', function () {
            oTableScreen.fnFilter( 'Landline' );
            alert('filtered');
            return false;
        });
    };

    // final Summary page for BWM
    myPwnApp.bwm.summary = function(row, bwmCharges) {

        var $form = $('#bwm_summary_form'),
            $productName = $('.product-name.bwm'),
            $formContainer = $('.bwm-form-container'),
            $rowContainer = $('.bwm-row-container'),
            totalPrice = $('.bwm-total-price').text();

        if (totalPrice.length > 0) {
            totalPrice = parseFloat(totalPrice.substr(1));
        } else {
            totalPrice = 0.00;
        }

        $form.initMypwnForm({
            messagePos: 'below',
            requiredAsterisk: 'off',
            success: function (bwmResult) {
                var bwmResultParsed = $.parseJSON(bwmResult);
                var productId = bwmResultParsed['product_id'];

                //all done, slide up the container!

                // Increase the basket count.
                CommonBasket.increaseBasketcount(totalPrice);

                // show button for new BWM
                $('.bwm-configure').show();

                $formContainer.slideUp('fast', function () {
                    $rowContainer.remove();
                    $('#updateAccount').show();
                }); // slide up configuration row and remove it;

                // Update the numbers summary in the tooltip of the product.
                var $dnisTable = $('#configured-bwm-prices');
                var groupedNumbers = groupSelectedNumbers(row, bwmCharges);
                $dnisTable.find('tr.default-dnis-price').remove();
                displayGroupedPrices($dnisTable, groupedNumbers, function($newElement, $table) {
                    $table.append($newElement);
                });

                // Update the basket tooltip.
                CommonBasket.addProductToBasketTooltip(
                    productId,
                    totalPrice,
                    $('#bwm_label').val()
                );
                if (bwmResultParsed.credit) {
                    CommonBasket.increaseBasketcount(bwmResultParsed.credit.total);
                    CommonBasket.addProductToBasketTooltip(
                        bwmResultParsed.credit.id,
                        bwmResultParsed.credit.total,
                        bwmResultParsed.credit.label
                    );
                    $('#credit-select-button').hide();
                    $('#credit-added-button').show();
                }
                CommonBasket.showBasketTooltip();

                // Sets up the accordian for the newly added BWM row
                $(".bwm-prod-wrapper .product-accordion").accordion({ autoHeight: false });
            }
        });

        // Default RequiredAsterisk action puts the required note before the submit button.
        // However before the submit button is a Cancel Button, so place the line at the start of form-action.
        // $bwmForm.find('.required-note').prependTo('.form-action');
        $('.form-action').prepend('<p class="required-note" style="font-size: 10px">* Required field</p>');

        // prepopulate product label and description
        $('.product-name.bwm').html($form.find('#bwm_label').val());

    };

    // Dial-in Numbers DataTable Functionality
    myPwnApp.dialinnumbers.init = function(tableName,data) {
        var oTableScreen = '';

        // Set the Languages
        var oLanguage = {
            "sProcessing": DATA_TABLES_sProcessing,
            "sLengthMenu": DATA_TABLES_sLengthMenu,
            "sZeroRecords": DATA_TABLES_sZeroRecords,
            "sEmptyTable": DATA_TABLES_sEmptyTable,
            "sLoadingRecords": DATA_TABLES_sLoadingRecords,
            "sInfo": DATA_TABLES_sInfo,
            "sInfoEmpty": DATA_TABLES_sInfoEmpty,
            "sInfoFiltered": DATA_TABLES_sInfoFiltered,
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sSearch": DATA_TABLES_sSearch,
            "sUrl": "",
            "fnInfoCallback": null,
            "oPaginate": {
                "sFirst":    DATA_TABLES_sFirst,
                "sPrevious": DATA_TABLES_sPrevious,
                "sNext":     DATA_TABLES_sNext,
                "sLast":     DATA_TABLES_sLast
            }
        };

        // Set the Column Headings
        var aoColumns = [
            { "sTitle": "Country (Language)" },
            { "sTitle": "In-Country Number" },
            { "sTitle": "International Number" },
            { "sTitle": "Cost per min" },
            { "sTitle": "Type", "sWidth": "108px" }
        ];

        // Set the Table to be shown on the screen
        oTableScreen = $(tableName).dataTable({
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "iDisplayLength": 50,
            "sPaginationType": "full_numbers",
            "oLanguage" : oLanguage,
            "aoColumns": aoColumns,
            "aaData": data
        });

        // Set the Default Sort
        oTableScreen.fnSort([[0,'asc']]);

        // Set the Table Headers to have onHover effect
        $(tableName + ' th').hover(
            function(){$(this).css("text-decoration", "underline");$(this).css("cursor", "pointer");},
            function(){$(this).css("text-decoration", "none");$(this).css("cursor", "default");}
        );

        // Move Table to the Left. Some browsers do a funky shift the right.
        $(tableName).addClass("floatleft");

        // Use Clearfix on the Table Wrapper
        $(tableName + '_wrapper').addClass("clearfix");
        $(tableName + '_wrapper').addClass("padding-bottom-20");

        // Set the Table to be Printable - Comes from Old JS File
        data_table_print_full(tableName);
    };

    // Users Form Functionality
    myPwnApp.users.init = function(formId) {
        $(formId).initMypwnForm({
            successMessagePos: 'off',
            debug: 'on',
            html5Validation: 'off',
            requiredAsterisk: 'off',
            success: function (responseArr) {
                var responseText = $.parseJSON(responseArr);
                if (null !== responseText &&  "success_message" in responseText) {
                    myPwnApp.utils.outputMessage('#frm-users',responseText.success_message,'success',false,true);
                } else if (null !== responseArr && "success_message" in responseArr) {
                    myPwnApp.utils.outputMessage('#frm-users',responseArr.success_message,'success',false,true);
                }
            },
            error: function (responseArr) {
                var responseText = $.parseJSON(responseArr.responseText);
                if ("error_messages" in responseText) {
                    if ("field_name" in responseText.error_messages) {
                        myPwnApp.utils.outputMessage('#frm-users',responseText.error_messages,'error',false,true);
                    } else if ("field_name" in responseText.error_messages[0]) {
                        myPwnApp.utils.outputMessage('#frm-users',responseText.error_messages[0],'error',false,true);
                    }
                }
            }
        });
    };

    // Users Toggle Checkboxes
    myPwnApp.users.toggleCheckbox = function (toggleState) {
        $('#frm-users').find('input[name="r[]"]').each(function() {
            this.checked = toggleState;
        });
    };

    // launch the app
    myPwnApp.init();
});