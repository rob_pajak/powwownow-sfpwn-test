/*global $*/
/*global jQuery*/
/*global enquire*/
/*global window*/
/*global dataLayer*/
/*global yepnope*/
/*global Modernizr*/
/*global FORM_COMMUNICATION_ERROR*/

yepnope({
    test : Modernizr.geolocation,
    yep : [
        '/letsGetItDone/gridpak/js/jquery.js',
        '/shared/ext/enquire.js',
        '/letsGetItDone/gridpak/js/foundation/foundation.js',
        '/letsGetItDone/gridpak/js/foundation/foundation.interchange.js'
    ],
    complete : function(){

        $(function(){
            try {
                enquire.register("screen and (min-width: 0px) and (max-width: 599px)", {
                    match : function() {
                        $('.social').insertBefore($('.about'));
                    },
                    unmatch : function() {
                        $('.about').insertBefore($('.social'));
                    },
                    deferSetup : true
                });
            } catch (e) {
                console.info('enquire error', e);
            }

            try {
                $(document).foundation();
            } catch (e) {
                console.info('foundation error', e);
            }
        });
    }
});

(function($) {
    'use strict';
    $(function() {

        function init(module) {
            $(function() {
                if (module.init) {
                    module.init();
                }
            });
            return module;
        }

        init(function() {
            var $formElement  = $('#register-top'),
                $formButton   = $('#submit_button'),
                $errorElement = $('#error'),
                $loading      = $('.loading');
            return {
                init: function () {
                    var self = this;
                    $formElement.on('submit', function(event) {
                        event.preventDefault();
                        if (self.basicEmailValidation($formElement) === true) {
                            self._registerUser();
                            try {
                                dataLayer.push({'event': 'Generate Pin - Lets-Get-It-Done/onClick'});
                            } catch (e) {
                                console.info('dataLayer not pushed', e);
                            }
                        }
                    });
                },
                _registerUser: function () {
                    $.ajax({
                        type: 'POST',
                        url: '/United-Free-Pin-Registration',
                        data: $formElement.serialize(),
                        beforeSend:function(){
                            $errorElement.html('');
                            $formButton.attr('disabled', 'disabled');
                            $loading.removeClass('hide');
                        },
                        success:function(response){
                            try {
                                dataLayer.push({
                                    'event': '/Lets-Get-It-Done/RegistrationSuccess',
                                    'PIN': response.pinRefHash
                                });
                            } catch (e) {
                                console.info('dataLayer not pushed', e);
                            }
                            setInterval(
                                function() {
                                    window.location.replace(response.redirect_url);
                                }, 1000
                            );
                        },
                        error:function(jqXHR){
                            var errorObject, errorMessage;
                            try {
                                errorObject = $.parseJSON(jqXHR.responseText);
                                if (errorObject.hasOwnProperty('error_messages')) {
                                    errorMessage = errorObject.error_messages;
                                    if (errorMessage instanceof Array) {
                                        return $errorElement.html(window[errorMessage[0].message]);
                                    }
                                    return $errorElement.html(window[errorMessage.message]);
                                }
                                throw window[FORM_COMMUNICATION_ERROR];
                            } catch (e) {
                                alert(window[FORM_COMMUNICATION_ERROR]);
                                return;
                            }
                        },
                        complete: function() {
                            $formButton.removeAttr('disabled');
                            $loading.addClass('hide');
                        }
                    });
                },
                basicEmailValidation : function (formElements) {
                    var emailObject = formElements.find('#lets-get-it-done-registration-email'),
                        emailRegex = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))){2,6}$/i,
                        email;
                    try {
                        email = emailObject.val();
                    } catch (e) {
                        //do nothing
                    }
                    $errorElement.html('');
                    if (email === '') {
                        $errorElement.html('Please enter a email address');
                        return false;
                    } else if (!emailRegex.test(email)) {
                        $errorElement.html('Invalid email address');
                        return false;
                    }
                    return true;
                }
            };
        }());
    });
})(jQuery);
