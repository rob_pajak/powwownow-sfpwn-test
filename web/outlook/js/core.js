(function(){
    "use strict";
    /*global $:false */
    /*global Pwn:false */
    /*global timezoneJS:false */
    /*global console:false */

    var Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} };

    Pwn.helpers.clock = {
        /**
         * Set up required path information for timezone.js
         */
        timezoneJS: {
            filePath: '/shared/ext/tz',
            jsonFilePath: '/shared/ext/tz/tz.json'
        },
        /**
         * DOM Elements
         */
        elements: {
            form: '#timezone-form',
            dropDown: '#timezone-form-select',
            startTime: '#start-time',
            endTime: '#end-time',
            timezoneAbb: '#timezone-abbreviation'
        },
        initialize: function() {
            // We REQUIRE timezone.js
            if (typeof window.timezoneJS !== 'undefined') {
                timezoneJS.timezone.zoneFileBasePath = this.timezoneJS.filePath;
                timezoneJS.timezone.init();
                this.render().detectGeo();
            } else {
                $(this.elements.form).hide();
            }
        },
        /**
         * Add 0 in front of numbers less than 10.
         *
         * @param hour
         * @param minute
         * @param separator
         * @returns string
         */
        formatTime: function(hour, minute, separator) {
            if (hour < 10) {
                hour = '0' + hour;
            }
            if (minute < 10) {
                minute = '0' + minute;
            }
            return hour + separator + minute;
        },
        /**
         * Render Dropdown list with countries.
         */
        render : function() {
            var _this = this;

            /**
             * lets bind an event to our dropdown el
             * that will update our "view"
             */
            var bindEvent = function() {
                $(_this.elements.dropDown).on("change", function(){
                    _this.syncUI($(this).val());
                });
            };

            /**
             * Start building drop down list make a request to json file
             * which holds the correct abbreviations for all the world
             * times.
             *
             * This list will only be built if PHP has not done so already.
             */
            var build = function() {
                var  $el = $(_this.elements.dropDown);
                if ($el['0'].options.length < 1) {
                    $.getJSON(_this.timezoneJS.jsonFilePath).done(function(json){
                        $.each(json, function(i, item) {
                            $.each(item.zones, function(i, item){
                                if (item.name === 'London') {
                                    $el.append($('<option>').attr({value:item.value, selected:'selected'}).text(item.name));
                                } else if (item.name) {
                                    $el.append($('<option>').attr('value',item.value).text(item.name));
                                }
                            });
                        });
                    }).fail(function(jqxhr, textStatus, error){
                        $(this.elements.form).hide();
                    });
                }
                bindEvent();
            };
            build();
            // allow chaining
            return this;
        },
        /**
         * Update View with timezoned! time.
         * @param timezone
         */
        syncUI: function(timezone) {

            var $startTimeEl    = $(this.elements.startTime),
                $endTimeEl      = $(this.elements.endTime),
                $timezoneAbb    = $(this.elements.timezoneAbb),
                unixStartTime   = $startTimeEl.attr('data-epoch-time'),
                unixEndTime     = $endTimeEl.attr('data-epoch-time'),
                stDate          = new timezoneJS.Date(unixStartTime*1000, timezone),
                endDate         = new timezoneJS.Date(unixEndTime*1000, timezone);

            $startTimeEl.html(this.formatTime(stDate.hours, stDate.minutes,':'));
            $endTimeEl.html(this.formatTime(endDate.hours, endDate.minutes,':'));
            $timezoneAbb.html(stDate.getTimezoneAbbreviation());
        },
        /**
         * Detect if browser supports geolocation. Since we load our fallback first
         * we dont need to worry about a fallback here.
         * NOTE:
         *  http://stackoverflow.com/questions/5947637/function-fail-never-called-if-user-declines-to-share-geolocation-in-firefox
         * Firefox has a known bug about invoking the error handler when user rejects permissions for geolocation.
         */
        detectGeo: function() {

            if (navigator && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(Pwn.helpers.clock.setGeo, Pwn.helpers.clock.geoError);
            }
        },
        /**
         * @param position
         */
        setGeo: function(position) {

            $.getJSON('https://maps.googleapis.com/maps/api/timezone/json',
            {   location: position.coords.latitude +','+ position.coords.longitude,
                timestamp: '1331161200',
                sensor: 'true'
            }).done(function(json){
                if (json.status === 'OK') {
                    Pwn.helpers.clock.syncUI(json.timeZoneId);
                }
            }).fail(function(jqxhr, textStatus, error){
                // do nothing
            });
        },
        /**
         * Error handler if a user rejects permissions for geolocation.
         * @param err
         */
        geoError: function(err) {
            if (err.code === 1) {
                console.log('The user denied the request for location information.');
            } else if (err.code === 2) {
                console.log('Your location information is unavailable.');
            } else if (err.code === 3) {
                console.log('The request to get your location timed out.');
            } else {
                console.log('An unknown error occurred while requesting your location.');
            }
        }
    };
    Pwn.helpers.clock.initialize();
})();
