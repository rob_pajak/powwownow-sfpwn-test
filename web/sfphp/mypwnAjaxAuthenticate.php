<?php
require_once 'RestRequest.php';
require_once 'Client.php';
require_once 'Rest.php';

$email    = (isset($_REQUEST['email']))    ? $_REQUEST['email'] : '';
$password = (isset($_REQUEST['password'])) ? $_REQUEST['password'] : '';

/**
 * Authenticate the User
 * 
 * @todo Error Handling
 */
try {
    $ret = json_decode(Hermes_Client_Rest::callJson('authenticateContact', array('email' => $email, 'password' => $password)));

    $_SESSION['authenticated']=true;
    $_SESSION['service_user']=isset($ret->service_user) ? $ret->service_user : '';
    $_SESSION['contact_ref']=isset($ret->contact_ref) ? $ret->contact_ref : '';
    $_SESSION['service_ref']=isset($ret->service_ref) ? $ret->service_ref : '' ;
    $_SESSION['first_name']=isset($ret->first_name) ? $ret->first_name : '';
    $_SESSION['last_name']=isset($ret->last_name) ? $ret->last_name : '';
    $_SESSION['email']=$_REQUEST['email'];
    session_regenerate_id();
    header('HTTP/1.1 200 OK');
    echo json_encode('true');
    
} catch (Hermes_Client_Exception $e) {
    $httpCode = $e->getHTTPStatusCode();
    $httpStatus = $e->getHTTPStatus() ? $e->getHTTPStatus() : 'Internal Server Error';
    $_SESSION['authenticated']=false;
    unset($_SESSION['service_user'], $_SESSION['contact_ref'], $_SESSION['service_ref'], $_SESSION['first_name'], $_SESSION['last_name'], $_SESSION['email']);
    session_regenerate_id();
    header("HTTP/1.1 $httpCode $httpStatus");
    echo json_encode(false);
} catch (Exception $e) {
    echo json_encode(false);
}
