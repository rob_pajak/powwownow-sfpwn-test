(function($){

    $('#modal-threeeasysteps-btn').on('click', function(){
        try {
            var $videos = $('video');
            $.each( $videos, function( i, v ) {
                v.play();
            });
        } catch(e) {
            // Don't do anything.
        }
    });

    // Stop Playing any videos on page
    $(".reveal-modal").on('click','.close-reveal-modal',function(e){
        try {
            var $videos = $('video');
            $.each( $videos, function( i, v ) {
                v.pause();
            });
        } catch(e) {
            // Don't do anything.
        }
    });

    $('body').bind('keyup.reveal', function (event) {
        if (event.which === 27) { // 27 is the keycode for the Escape key
            try {
                var $videos = $('video');
                $.each( $videos, function( i, v ) {
                    v.pause();
                });
            } catch(e) {
                // Don't do anything.
            }
        }
    });
})(jQuery);