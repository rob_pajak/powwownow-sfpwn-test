(function(){

    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} };

    Pwn.Models.WelcomePackRequest =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });

    Pwn.Collections.WelcomePackRequest = Pwn.Collections.Requests.extend({
        model: Pwn.Models.WelcomePackRequest
    });

    Pwn.Views.WelcomePackRequest = Pwn.Views.RequestHandler.extend({
        el : '#its-your-call-request-welcome-pack-container',
        template: Pwn.helpers.template('#its-your-call-request-welcome-pack-template'),
        formElement : '#its-your-call-request-welcome-pack-form',
        showMultiple: false,
        events: {
            'submit #its-your-call-request-welcome-pack-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render: function() {
            try {
                this.$el.html(this.template());
            } catch (e) {
                Pwn.helpers.log(e);
            }
            return this;
        },
        responseSuccess: function(model) {
            var status,
                error;

            if (_.has(model.attributes, 'status')) {
                status = model.get('status');
                if (status === 'error') {
                    if (_.has(model.attributes, 'error_messages')) {
                        error = model.get('error_messages');
                        this.onError(error);
                    }
                } else if (status === 'success') {
                    try {
                        if (!_.isUndefined(dataLayer)) {
                            var pageArray = window.location.pathname.split('/');
                            var pageName = pageArray[pageArray.length-1];

                            if ('Its-Your-Call-Welcome' === pageName) {
                                dataLayer.push({'event': '/Its-Your-Call/WelcomePackSuccess'});
                            } else if ('Lets-Get-It-Done-Welcome' === pageName) {
                                dataLayer.push({'event': '/Lets-Get-It-Done/WelcomePackSuccess'});
                            } else {
                                Pwn.helpers.log(window.location);
                                Pwn.helpers.log('Error Occurred in Tracking the Successful Wallet Card Request Registration');
                            }
                        }
                    } catch(e) {
                        Pwn.helpers.log('Error Occurred in Tracking the Successful Wallet Card Request Registration');
                    }
                    var redirect = $('#attrs').attr('data-redirect');
                    setTimeout(window.location.replace(redirect), 1000);
                } else {
                    this.collection.technicalError();
                }
            } else {
                this.collection.technicalError();
            }
        }
    });


    var WelcomePackRequestCollection = new Pwn.Collections.WelcomePackRequest({
        model: Pwn.Collections.WelcomePackRequest
    });

    var WelcomePackRequest = new Pwn.Views.WelcomePackRequest({
        collection : WelcomePackRequestCollection
    });

})();
