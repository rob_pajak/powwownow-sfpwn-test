
$.fn.navDropDown = function(options) {
    // Apply class=hasChildren on those items with children
    this.each(function() {
        $(this).find('li').each(function() {
            if($(this).find("ul").length > 0) {
                $(this).addClass("hasChildren");                       
                $(this).find('> a').wrapInner("<span id='submenu-items'></span>");
            }
        });    
    });
    
    // Apply class=hover on all list items
    $(this).find("li").hover(function() {
        $(this).addClass('hover');
    }, function() {
        $(this).removeClass('hover');
    });
    
    $('.sub-menu').on('click',function(event){
        var that = this;
        if (event.target.id === 'submenu-items') {
            $(this).addClass('active');
            $(this).find('ul').slideToggle(400, function(){
                var status = ($(this).is(':hidden'));
                if (status){
                    $(that).removeClass('active');
                }
            });
        } 
    });
};



$.fn.topMenu = function(options) {
    
    $(this).on('click','li a', function(event){
        event.preventDefault();

        $('#top-panel').slideToggle(400,function() {
            var status = ($(this).is(':hidden'));
            if (!status){
                $('#top-panel-menu').removeClass('active');
            }
        });
    });
    
    $(this).hover(
        function(){ 
            $('#top-panel-menu').addClass('active')
        },
        function(){ 
            $('#top-panel-menu').removeClass('active')
        }
    );
};

$.fn.scrollToFeaturePanel = function() {
    
    var links = $(this).attr('href');
    
    if ( typeof links === 'undefined' || links === '') {
        $(this).on('click', function(event){
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('#feature-panel').position().top
            },function(){
                $('#feature-panel .feature').effect("shake",{times:1},300);  
            });
        
//                        $('#top-panel').slideUp(400,function(){
//                $('html, body').animate({
//                    scrollTop: $('#feature-panel').position().top
//                },function(){
//                    $('#feature-panel .feature').effect("shake",{times:1},300);  
//                });
//            });
        
        });
    }
};

/* On DOM Ready */
$(function() {
    
    $('nav, #international').navDropDown();
    
    $('#top-panel-login-menu-btn').topMenu();
    
    if ($('#ca-container').length){
        $('#ca-container').contentcarousel();
    }
    
    $('#generate-pin-and-account').scrollToFeaturePanel();
    
    /* PLACEHOLDER FOR FORMS ------------- */
	/* Remove this and jquery.placeholder.min.js if you don't need :) */
    $('input, textarea').placeholder();

	/* ALERT BOXES ------------ */
	$(".alert-box").delegate("a.close", "click", function(event) {
        event.preventDefault();
        $(this).closest(".alert-box").fadeOut(function(event){
            $(this).remove();
        });
	});
    
    /* ALERT BOXES ------------ */
    $(".reveal-modal").on('click','.close-reveal-modal',function(e){
        try {
            var $videos = $('video');
            $.each( $videos, function( i, v ) {
                v.pause();
            });
        } catch(e) {
            //dont do anything init!
        }
    });
    
    
    $('body').bind('keyup.reveal', function (event) {
        if (event.which === 27) { // 27 is the keycode for the Escape key
            try {
                var $videos = $('video');
                $.each( $videos, function( i, v ) {
                    v.pause();
                });
            } catch(e) {
                //dont do anything init!
            }
        }
    });
    

}); 