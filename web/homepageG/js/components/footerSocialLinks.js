

var fade = ({
    init :function() {
        
        $('ul.list-fading').each(function() {

            // If there is only one <li> then just show it with no affects
            if ($(this).children('li').length === 1) {
                return true;
            }
        
            // See if it is the first time this function has been run by seeing
            // if all li's are visible
            if ($(this).children('li:visible').length === $(this).children('li').length && $(this).children('li').length !== 1) {
                // First time function is run so hide all li's
                $(this).children('li').hide();
                // Show the first li
                $(this).children('li:first-child').fadeIn('slow');
            } else {
                // It is not the first time function has run, so we need to fade out the currently
                // visible li and then fade in the next, unless we have reached the end
                // of the list, then we need to show the first li again

                // Get the index of the next <li> to show
                nextLi = $(this).children('li:visible').index() + 2; // Add 2 as index() starts from 0
                                                                     // :nth-child() starts from 1
                // If we have reached the end of the list, the start from the first item
                if (nextLi > $(this).children().length) {
                    nextLi = 1;
                }
                // Do the fading in and out
                $(this).children('li:visible').fadeOut('slow');
                $(this).children('li:nth-child(' + (nextLi) + ')').delay(900).fadeIn('slow');
            }
        });
    }
});

$(function() {
    
    var $request = $.ajax({
        url: '/ajax/social-media-footer.php',
        dataType: 'json',
        async: true,
        cache: true,
        template: $("#footer-social-links-template").html(),
        beforeSend: function(){
            $('#footer-social-links').addClass('loading');
        }
    }).fail(function(){
        
        $data = {
            facebook: '',
            twitter: '',
            news: '',
            blog: ''
        }
        
    }).done(function(data){
        $data = {
            facebook: data.facebook,
            twitter: data.twitter,
            news: data.pwn_news,
            blog: data.pwn_blog
        }
        
    }).always(function(){
        $('#footer-social-links').removeClass('loading');
        $("#footer-social-links").html(_.template(this.template, $data));
        setInterval('fade.init()', 9000);
    });
}); 

