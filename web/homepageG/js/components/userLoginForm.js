/**************************************/
/* Top Panel Login Form ------------- */
/**************************************/

(function(){
    
    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} };
    
    Pwn.Models.LoginForm =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });
    
    Pwn.Collections.LoginForm = Pwn.Collections.Requests.extend({
        model: Pwn.Models.LoginForm
    });
    
    
    Pwn.Views.LoginForm = Pwn.Views.RequestHandler.extend({
        el : '#login-form-container',
        template: Pwn.helpers.template('#top-panel-login-form-template'),
        formElement : '#top-panel-login-form',
        events: {
            'submit #top-panel-login-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation', 'responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render : function() {

            var aModel = this.collection.at(this.collection.length - 1).toJSON(),
                viewData = {};

            if (aModel.status === 'fail') {

                viewData ={

                    message :  '<p>Your email and password are not recognised. Please amend and try again.<br />' +
                        'If you are a registered User but have forgotten your password please use our <a href="/Forgotten-Password" class="more">Password Reminder</a>. <br />' +
                        'If you are a registered User but havent created a login please create <a href="/Create-A-Login" class="more"> one here</a>.\n\
                        </p>'

                };
            } else if (aModel.status === 'success') {
                window.location.replace("/myPwn");
            }

            try {
                this.$el.html(this.template(viewData));
            } catch (e) {
                Pwn.helpers.log(e);
            }

            return this;
        },
        responseSuccess: function(model) {
            if (_.has(model.attributes,'status') ){
                this.render();
            } else {
                this.collection.technicalError();
            }
        }
    });
    
    var LoginFormCollection = new Pwn.Collections.LoginForm({
        model: Pwn.Models.LoginForm
    });
    var LoginForm = new Pwn.Views.LoginForm({
        collection : LoginFormCollection
    });
    
    
})();

