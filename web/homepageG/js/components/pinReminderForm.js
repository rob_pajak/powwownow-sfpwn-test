(function(){
    
    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} }; 
    
    Pwn.Models.PinReminder =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });
    
    Pwn.Collections.PinReminder = Pwn.Collections.Requests.extend({
        model: Pwn.Models.PinReminder
    });
    
    Pwn.Views.PinReminder = Pwn.Views.RequestHandler.extend({
        el : '#pin-reminder-form-container',
        template: Pwn.helpers.template('#pin-reminder-form-template'),
        formElement : '#pin-reminder-form',
        events: {
            'submit #pin-reminder-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.collection.bind('add',this.responseSuccess);
            this.render();
        },
        render: function() {

            var aModel = this.collection.at(this.collection.length - 1).toJSON(),
                viewData = {};

            if (aModel.status === 'success') {
                alert("Your PIN reminder is on its way.");
            }

            try {
                this.$el.html(this.template(viewData));    
            } catch (e) {
                Pwn.helpers.log(e)
            }

            return this;
        },
        responseSuccess: function(model) {
            var status,
                error;
            if (_.has(model.attributes, 'status')) {
                status = model.get('status');
                if (status === 'error') {
                    if (_.has(model.attributes, 'error_messages')) {
                        error = model.get('error_messages');
                        switch(error[0].message) {
                            case 'API_RESPONSE_CONTACT_IS_NOT_STANDARD_POWWOWNOW_USER1':
                                $('#modal-PleaseLogin').reveal();
                                break;
                            default:
                                this.onError(error);
                                break;
                        }
                    }
                } else if (status === 'success') {
                    this.render();
                } else {
                    this.collection.technicalError();
                }
            }
        }
    });
    
    var PinReminderCollection = new Pwn.Collections.PinReminder({
        model: Pwn.Models.PinReminder
    });
    var PinReminder = new Pwn.Views.PinReminder({
        collection : PinReminderCollection
    });

})();