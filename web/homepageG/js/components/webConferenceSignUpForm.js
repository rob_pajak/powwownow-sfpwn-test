(function(){

    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} };

    Pwn.Models.WebConferenceSignUpForm =  Backbone.Model.extend({
        defaults: {
            status : 'initialized',
            time : new Date()
        }
    });

    Pwn.Collections.WebConferenceSignUpForm = Pwn.Collections.Requests.extend({
        model: Pwn.Models.WebConferenceSignUpForm
    });

    Pwn.Views.WebConferenceSignUpForm = Pwn.Views.RequestHandler.extend({
        el : '#web-conferencing-container',
        template: Pwn.helpers.template('#web-conferencing-template'),
        formElement : '#web-conferencing-form',
        events: {
            'submit #web-conferencing-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render: function() {

            var aModel = this.collection.at(this.collection.length - 1).toJSON(),
                viewData = {};
            if (aModel.status === 'success') {
                viewData = aModel;
            }
            try {
                this.$el.html(this.template(viewData));
            } catch (e) {
                Pwn.helpers.log(e);
            }
            return this;
        },
        responseSuccess: function(model) {
            var error,
                status;

            if (_.has(model.attributes,'error_messages')) {
                error = model.get('error_messages');
                if (_.has(error[0],'message')) {
                    error = model.get('error_messages');
                    switch(error[0].message) {
                        case 'API_RESPONSE_EMAIL_ALREADY_TAKEN':
                        case 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE':
                            $('#modal-getmypinemailtaken').reveal();
                            break;
                        default:
                            this.onError(error);
                            break;
                    }
                }
            }  else if (_.has(model.attributes,'status') ){
                status = model.get('status');
                if ( status === 'success') {
                    this.render();
                } else {
                    this.collection.technicalError();
                }
            } else {
                this.collection.technicalError();
            }
        }
    });

    var WebConferenceSignUpFormCollection = new  Pwn.Collections.WebConferenceSignUpForm({
        model: Pwn.Models.WebConferenceSignUpForm
    });
    var WebConferenceSignUpForm = new Pwn.Views.WebConferenceSignUpForm({
        collection : WebConferenceSignUpFormCollection
    });

})();


