    stLight.options({
        publisher: "ur-ee0574d5-102a-fba0-f0b6-f5ec309e696", 
        doNotHash: false, 
        doNotCopy: false, 
        hashAddressBar: false,
        popup: false
    });
    
    stWidget.addEntry({
        "service" : "facebook",
        "element" : document.getElementById('st_facebook'),
        "title"   : "<?php echo $facebookMessage?>",
        "type"    : "hcount",
        "text"    : "Share"
    });
    
    
    
    stWidget.addEntry({
        "service" : "twitter",
        "element" : document.getElementById('st_twitter'),
        "title"   : "<?php echo $twitterMessage?>",
        "type"    : "hcount",
        "text"    : "Tweet"
    });
    
    
    stWidget.addEntry({
        "service" : "googleplus",
        "element" : document.getElementById('st_googleplus'),
        "title"   : "<?php echo $googleMessage?>",
        "type"    : "hcount",
        "text"    : "Share"
    });
    
    stWidget.addEntry({
        "service" : "linkedin",
        "element" : document.getElementById('st_linkedin'),
        "title"   : "<?php echo $linkedinMessage?>",
        "type"    : "hcount",
        "text"    : "Share"
    });