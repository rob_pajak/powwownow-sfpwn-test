(function(){
    
    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} }; 
    
    Pwn.Models.GetMyPin =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });
    
    Pwn.Collections.GetMyPin = Pwn.Collections.Requests.extend({
        model: Pwn.Models.GetMyPin
    });

    Pwn.Views.GetMyPin = Pwn.Views.RequestHandler.extend({
        el : '#get-my-pin-feature-top-container',
        template: Pwn.helpers.template('#get-my-pin-feature-top-template'),
        formElement : '#get-my-pin-feature-top',
        events: {
            'submit #get-my-pin-feature-top' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render: function() {
            try {
                this.$el.html(this.template());
            } catch (e) {
                Pwn.helpers.log(e);
            }
            return this;
        },
        responseSuccess: function(model) {
            var status,
                error;

            if (_.has(model.attributes, 'status')) {
                status = model.get('status');
                if (status === 'error') {
                    if (_.has(model.attributes, 'error_messages')) {
                        error = model.get('error_messages');
                        switch(error[0].message) {
                            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN':
                            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE':
                                $('#modal-getmypinemailtaken').reveal();
                                break;
                            default:
                                this.onError(error);
                            break;
                        }
                    }
                } else if (status === 'success') {
                    if ( status === 'success') {
                        _gaq.push(['_trackEvent', 'registration', 'basic', 'G-top']);
                        window.location.replace("/testG/Next-Steps");
                    }
                } else {
                    this.collection.technicalError();
                }
            } else {
                this.collection.technicalError();
            }
        }
    });
    
    Pwn.Views.GetMyPinBottom = Pwn.Views.RequestHandler.extend({
        el : '#get-my-pin-small-bottom-container',
        template: Pwn.helpers.template('#get-my-pin-small-bottom-template'),
        formElement : '#get-my-pin-small-bottom',
        events: {
            'submit #get-my-pin-small-bottom' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.collection.bind('add',this.responseSuccess);
            this.render();
        },
        render: function() {
            try {
                this.$el.html(this.template());
            }catch (e) {
                Pwn.helpers.log(e);
            }
            return this;
        },
        responseSuccess: function(model) {
            var status,
                error;

            if (_.has(model.attributes, 'status')) {
                status = model.get('status');
                if (status === 'error') {
                    if (_.has(model.attributes, 'error_messages')) {
                        error = model.get('error_messages');
                        switch(error[0].message) {
                            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN':
                            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE':
                                $('#modal-getmypinemailtaken').reveal();
                                break;
                            default:
                                this.onError(error);
                            break;
                        }
                    }
                } else if (status === 'success') {
                    if ( status === 'success') {
                        _gaq.push(['_trackEvent', 'registration', 'basic', 'G-bottom']);
                        window.location.replace("/testG/Next-Steps");
                    } else {
                        this.collection.technicalError();
                    }
                } else {
                    this.collection.technicalError();
                }
            }
        }
    });
    
    
    var GetMyPinCollection = new Pwn.Collections.GetMyPin({
        model: Pwn.Collections.GetMyPin
    });
    
    var GetMyPinTop = new Pwn.Views.GetMyPin({
        collection : GetMyPinCollection
    });
    
    
    var GetMyPinCollectionBottom = new Pwn.Collections.GetMyPin({
        model: Pwn.Collections.GetMyPin
    });
    
    var GetMyPinBottom = new Pwn.Views.GetMyPinBottom({
        collection : GetMyPinCollectionBottom
    });
    
})();
