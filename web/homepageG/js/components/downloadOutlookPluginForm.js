/**************************************/
/* Top Panel Login Form ------------- */
/**************************************/

(function(){

    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} };

    Pwn.Models.DownloadOutlookPlugin =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });

    Pwn.Collections.DownloadOutlookPlugin = Pwn.Collections.Requests.extend({
        model: Pwn.Models.DownloadOutlookPlugin
    });

    Pwn.Views.DownloadOutlookPlugin = Pwn.Views.RequestHandler.extend({
        showMultiple: false,
        el : '#download-outlook-plugin-container',
        template: Pwn.helpers.template('#download-outlook-plugin-form-template'),
        formElement : '#download-outlook-plugin-form',
        events: {
            'submit #download-outlook-plugin-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation', 'responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render : function() {
            var viewData;

            try {
                this.$el.html(this.template(viewData));
            } catch (e) {
                Pwn.helpers.log(e);
            }

            return this;
        },
        responseSuccess: function(model) {
            var errors, status, data;

            // User Successfully Added
            if (_.has(model.attributes,'success')) {
                // Hide Form + Run GA Tracking + Open Download
                _gaq.push(['_trackEvent', 'Link', 'Outlook Plugin', 'G-schedule']);
                window.location.replace("/sfdownload/PowwownowSetup.exe");
            } else if (_.has(model.attributes, 'error_messages')) {
                errors = model.get('error_messages');
                this.onError(errors);
            }
            else {
                this.collection.technicalError();
            }
        }
    });

    var DownloadOutlookPluginCollection = new Pwn.Collections.DownloadOutlookPlugin({
        model: Pwn.Models.DownloadOutlookPlugin
    });
    var DownloadOutlookPlugin = new Pwn.Views.DownloadOutlookPlugin({
        collection : DownloadOutlookPluginCollection
    });

})();