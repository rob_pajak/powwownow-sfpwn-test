(function(){
    
    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} };
    
    Pwn.Models.WelcomePack =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });
    
    Pwn.Collections.WelcomePack = Pwn.Collections.Requests.extend({
        model: Pwn.Models.WelcomePack
    });
    
    
    Pwn.Views.WelcomePack = Pwn.Views.RequestHandler.extend({
        el : '#request-welcome-pack-form-container',
        template: Pwn.helpers.template('#request-welcome-pack-form-template'),
        formElement : '#request-welcome-pack-form',
        events: {
            'submit #request-welcome-pack-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation', 'responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render : function() {
            
            this.listenTo(this.collection,'updateView', function(model){
                if (model.get('status') === 'success') {
                    _gaq.push(['_trackEvent', 'welcome-pack', 'G']);
                    try {
                        this.$el.html(this.template({'welcomePackOrdered' : true}));
                    } catch (e) {
                        Pwn.helpers.log(e);
                    }
                }
            });
            try {
                this.$el.html(this.template({'welcomePackOrdered' : false}));
            } catch (e) {
                Pwn.helpers.log(e);
            }
            
            return this;
        },
        responseSuccess: function(model) {
            var error, status;
            if (_.has(model.attributes,'error_messages')) {
                error = model.get('error_messages');
                if (_.has(error[0],'message')) {
                    this.onError(error);
                }
            } else if (_.has(model.attributes,'status') ){
                status = model.get('status');
                if ( status === 'success') {
                    this.collection.trigger("updateView", model);
                } else {
                    this.collection.technicalError();
                }
            } else {
                this.collection.technicalError();
            }
        }
        
    });
    
    
    var WelcomePackCollection = new Pwn.Collections.WelcomePack({
        model: Pwn.Models.WelcomePack
    });
    var WelcomePack = new Pwn.Views.WelcomePack({
        collection : WelcomePackCollection
    });
    
    
})();