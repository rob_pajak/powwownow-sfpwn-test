(function(){
    
    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} };
    
    Pwn.Models.UpdatePassword =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });
    
    Pwn.Collections.UpdatePassword = Pwn.Collections.Requests.extend({
        model: Pwn.Models.UpdatePassword
    });
    
    Pwn.Views.UpdatePassword = Pwn.Views.RequestHandler.extend({
        el : '#password-reset-form-container',
        template: Pwn.helpers.template('#password-reset-form-template'),
        formElement : '#password-reset-form',
        events: {
            'submit #password-reset-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render: function() {

            var aModel = this.collection.at(this.collection.length - 1).toJSON(),
                viewData = {};

            if (aModel.status === 'success') {
                viewData = aModel;
            }

            try {
                this.$el.html(this.template(viewData));
            } catch (e) {
                Pwn.helpers.log(e);
            }

            return this;
        },
        responseSuccess: function(model) {
            var error,
                status;

            if (_.has(model.attributes,'error_messages')) {
                error = model.get('error_messages');                
                if (_.has(error[0],'message')) {
                    this.onError(error);
                }
            } else if (_.has(model.attributes,'status') ){
                status = model.get('status');
                if ( status === 'success') {
                    this.render();
                } else {
                    this.collection.technicalError();
                }
            } else {
                this.collection.technicalError();
            } 
        }
        
    });

    var UpdatePasswordCollection = new  Pwn.Collections.UpdatePassword({
        model: Pwn.Models.UpdatePassword
    });
    var UpdatePassword = new Pwn.Views.UpdatePassword({
        collection : UpdatePasswordCollection
    });

})();


