(function(){
    // If you dont work here and your reading this, WHAT IS WRONG WITH YOU?
    // aint you got a girl?
    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} }; 
    
    Pwn.Models.RegisterPlus = Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });
    
    Pwn.Collections.RegisterPlus = Pwn.Collections.Requests.extend({
        model : Pwn.Models.RegisterPlus
    });
    
    Pwn.Views.RegisterPlus = Pwn.Views.RequestHandler.extend({
        el : '#plus-register-container',
        template: Pwn.helpers.template("#plus-register-template"),
        formElement : '#plus-register-form',
        events: {
            'submit #plus-register-form' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.listenTo(this.collection,'add', this.responseSuccess);
            this.render();
        },
        render: function() {
            var viewData = {};
            try {
                this.$el.html(this.template(viewData));    
            } catch (e) {
                Pwn.helpers.log(e);
            }
            return this;
        },
        responseSuccess: function(model) {
            var status, errors, formResults,showFormErrors;
            if (_.has(model.attributes, 'status')) {
                status = model.get('status');
                if (status === 'error') {
                    if (_.has(model.attributes, 'error_messages')) {
                        errors = model.get('error_messages');
                        showFormErrors = true;
                        _.each(errors,function(error){
                            if (error.message === 'PLUS_PREM_ACCOUNT') {
                                $('#modal-alreadyAPlusCustomers').reveal();
                                showFormErrors = false;
                            }
                        }); 
                        if (showFormErrors) {
                            this.onError(errors);
                        }
                    }
                    
                } else if (status === 'success') {
                    //init step 2.
                    if ( _.has(model.attributes, 'form_result')) {
                        formResults  = model.get('form_result');
                    } else {
                        formResults = {};
                    }
                    RegisterPlusStepTwoCollection.add(formResults);
                    RegisterPlusStepTwo.render(formResults); 
                    $('#modal-plusSwitchRegistrationStepTwo').reveal();
                }
            }
        }
        
    });
    
    /*******************************************************************
    *  Step Two
    *******************************************************************/
   
   Pwn.Views.RegisterPlusStepTwo = Pwn.Views.RequestHandler.extend({
        el : '#plus-switch-registration-container',
        template: Pwn.helpers.template('#plus-switch-registration-template'),
        formElement : '#plus-switch-registration',

        events: {
            'submit #plus-switch-registration' : 'submitForm',
            'submit #plus-switch-registration-login' : 'submitFormLogin'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation', 'responseSuccess');
            this.listenTo(this.collection, 'add',this.responseSuccess);
            
        },
        render: function(viewData) {
            
            // fucking anit-pattern
            var accountType      = $('#accountType').attr('data-account-type'),
                accountEmail     = $('#accountEmail').attr('data-email'),
                accountFirstname = $('#accountFirstname').attr('data-firstname'),
                accountLastname  = $('#accountLastname').attr('data-lastname');
            
            if (! _.has(viewData, 'account_status')) {
                
                if (! _.isNull(accountType) && !_.isUndefined(accountType)){ 
                    viewData.account_status = accountType;
                }
                
                if (! _.isNull(accountEmail) && !_.isUndefined(accountEmail)){   
                    viewData.email = accountEmail;
                }
            
                if (! _.isNull(accountFirstname) && !_.isUndefined(accountFirstname)){   
                    viewData.first_name = accountFirstname;
                    if (accountFirstname === '') {
                        viewData.first_name;
                    }
                }
            
                if (! _.isNull(accountLastname) && !_.isUndefined(accountLastname)){   
                    viewData.last_name = accountLastname;
                    if (accountLastname === '') {
                        viewData.last_name;
                    }
                }
            }
            // You might be thinking WTF, so am i, surely error messages shouldnt give 
            // a success status! This is view data, just to keep the view on the correct
            // form. So calm down take a deep breath! :)
            if ( _.has(viewData, 'error_messages') ) {
                viewData.status = 'success';
            }
            
            if (_.has(viewData, 'registration_status') ) {
                if (viewData.registration_status === 'success') {
                    viewData.account_status = 'complete';
                    // @todo: shouldn't this be /myPwn/Existing-Registrion?
                    window.location.replace("/myPwn/New-Registration");
                }
            }
            
            try {
                this.$el.html(this.template(viewData));    
            } catch (e) {
                Pwn.helpers.log(e);
            }
            
            return this;
            
        },
        submitFormLogin : function(event) {
            event.preventDefault();
            if (this.validation({formElement:'#plus-switch-registration-login'}) === true) {
                this.collection.fetch({
                    data: $('#plus-switch-registration-login').serialize(),
                    type:'POST',
                    add: true, // add to exisiting,
                    url: $('#plus-switch-registration-login').attr('action'),
                    success: function(){
                        Pwn.helpers.log('Pwn.Collections.Requests: success');
                    }
                });   
            }
        },
        responseSuccess: function(model) {
            var accountStatus, registrationStatus, error, status;
            if (_.has(model.attributes, 'error_messages')) {
                error = model.get('error_messages');
                if (_.has(error[0],'message')) {
                    this.onError(error);
                }
            } else if (_.has(model.attributes,'status')) {
                status = model.get('status');
                if(status === 'fail') {
                    this.onError({"error_messages":[{"message":"INVALID_EMAIL_PASSWORD_CO","field_name":"alert"}]});
                } else {
                    accountStatus = model.get('account_status');
                    registrationStatus = model.get('registration_status');
            
                    if (! _.isUndefined(accountStatus)) {
                        model.set('status','updated');
                    }

                    if (! _.isUndefined(registrationStatus)) {
                        model.set('status','complete');
                    }

                    this.render(model.toJSON());
                }
            }else {
                model.set('status','pending');   
            }
        }
   });
    
    
    if (_.isUndefined(RegisterPlus)) {
        var RegisterPlusCollection = new Pwn.Collections.RegisterPlus({
            model: Pwn.Models.RegisterPlus
        });
        var RegisterPlus = new Pwn.Views.RegisterPlus({
                collection : RegisterPlusCollection
        });
    }
    
    
    if (_.isUndefined(RegisterPlusStepTwo)) {
        var RegisterPlusStepTwoCollection = new Pwn.Collections.RegisterPlus({
            model: Pwn.Models.RegisterPlus
        });
        var RegisterPlusStepTwo = new Pwn.Views.RegisterPlusStepTwo({
            collection : RegisterPlusStepTwoCollection
        });
    }
    
    

})();
