(function(){
    window.Pwn = window.Pwn || { Models : {},Collections : {},Views:{}, helpers:{} }; 
    /** This is JSHinted - if you modify make sure it passes.**/
    Pwn.helpers.WhosOn = (function ($, screen, window) {
        'use strict';
        /*global jQuery, screen, window*/
        var window_width, window_height, center_left, center_top, page_url, page_title;
        window_width  = 484;
        window_height = 361;
        center_left   = (screen.width / 2) - (window_width / 2);
        center_top    = (screen.height / 2) - (window_height / 2);
        page_url      = 'https://hosted3.whoson.com/chat/chatstart.aspx?domain=www.powwownow.co.uk&theme=Powwownow';
        page_title    = 'LiveChat'; // Must be 1 Word. Fails on IE Family when more than 1 Word is used.
        return {
            init: function (element) {
                var that = this;
                $(element).on('click', function (event) {
                    event.preventDefault();
                    that.openWindow();
                });
                return this;
            },
            openWindow: function () {
                return window.open(page_url, page_title, 'alwaysRaised=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + window_width + ', height='  + window_height + ', top=' + center_top + ', left=' + center_left);
            }
        };
    }(jQuery, screen, window));

    Pwn.helpers.WhosOn.init('.chatlink');
    
})();


