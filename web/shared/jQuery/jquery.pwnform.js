/*!
* H5Form plugin by Vitaly Kondratiev (aka jquery.pwnform) (Last Update by Vitaly was on version 2.0.46 on 10-December-2012)
* version: 2.048 (24-July-2013)
*
* @requires jQuery >= 1.7.1
* @requires yepnope.js (loaded by autoloader from js path: jsPath)
* @requires jquery.form.js (loaded by autoloader from js path: jsPath)
* @requires language.js file (defaults to en.js) (loaded by autoloader from language path: langPath)
*
* Usage for H5Form
*
* basic init with default options
* $('#myForm').initMypwnForm();
*
*
* custom options can be specified as an object
* for example:
*
* var options = {
*     beforeSubmit: customValidation,
*     success: myExtraSuccessCallback,
*     error: myExtraErrorCallback,
*     messagePos: 'below'
* }
* $('#myForm').initMypwnForm(options);
*
*
* AVAILABLE OPTIONS:
* see options $.extend object in the code for available options and defaults
*
*
*      Validation and error messages:
*
*      REQUIRED FIELD
*
*      Mark-up:
*      <input type="text" id="first_name" name="first_name" class="required" /> or
*      <input type="text" id="first_name" name="first_name" required="required" /> as per HTML5 mark-up
*
*
*      Error messages:
*
*      If the field is empty, the script will first look for a defined custom error message variable
*      which is:
*      FORM_VALIDATION_(field name)_EMPTY
*      for example: FORM_VALIDATION_FIRST_NAME_EMPTY
*
*      if the custom error message is not defined, it will use a default message:
*      FORM_VALIDATION_FIELD_EMPTY
*
*      DATE FIELD AND DATE PICKER
*      <input type="date" name="start_date" required="required" />
*
*      you can specify maximum and minimum dates as following:
*      <input type="date" min="31-01-2012" max="01-06-2012" name="date_selector />
*
* passed JSLint on 28/09/2012 (except for regexp)
*/

/*global window, document, jQuery */

// get the script base path
var h5FormPath = {};

h5FormPath.scripts = document.getElementsByTagName('script');
h5FormPath.path = h5FormPath.scripts[h5FormPath.scripts.length - 1].src.split('?')[0];
h5FormPath.dir = h5FormPath.path.split('/').slice(0, -1).join('/') + '/';
h5FormPath.loaded = false; // variable defines if all required scripts are loaded

(function ($) {
    "use strict";

    // init the form
    $.fn.initMypwnForm = function (options) {
        console.warn('initMypwnForm to be deprecated, do not use.');
        // form object and constants
        var baseUrl,
            h5Form = {}, //main object for h5Form plugin
            index,
            paths,
            scripts,
            that = this; // to access this variable inside the inner functions

        // library version
        h5Form.version = '2.046';

        //powwownow specific flag
        h5Form.powwownow = true;

        // set defaults and merge with user options
        options = $.extend({
            jsPath  : '/shared/',       // path to js libraries {relative or absolute path}
            cssPath : '/sfcss/',              // path to css styling {relative or absolute path}
            imgPath : '/sfimages/',              // path to images (e.g. ajax-loader) {relative or absolute path}
            langPath: '/sfjs/language/',      // path to language js files {relative or absolute path}
            formPlugin: 'jQuery/jquery-form-3.02.js',  //jquery.form plugin {filename.js} http://jquery.malsup.com/form/
            language: 'en.js',              // en.js by default {filename.js}
            cssStyle: 'pwnForms.css',       // Pwn form styling {filename.css}
            showMultipleErrors: false,      // show multiple errors (if any) at once or one by one {true|false}
            debug   : 'off',                // debugging option is the console {on|off}
            requiredAsterisk: 'off',        // asterisks next to required field's labels {on|off}
            messagePos: 'above',            // error and success message position {above|below|off}
            uiSupport: 'off',               // jQueryUI support (extra classes for styling)
            responsive: 'off'               // Is the Form Responsive, then disable the Default CSS
        }, options || {});

        // fetch host from the URL
        h5Form.getHost = function (url) {
            if ($.browser.msie && parseInt($.browser.version, 10) === 7) {
                // function doesn't work for IE7 for time being
                return false;
            }

            if ($.browser.msie && parseInt($.browser.version, 10) !== 7) {
                return (url.match(/:\/\/(.[^/]+)/)[1]).replace('www.', '');
            }
        };


        // check if path is absolute or relative
        h5Form.testPath = function (s) {
            return s.charAt(0) !== "#" &&
                s.charAt(0) !== "/" &&
                (
                    s.indexOf("//") === -1 ||
                    s.indexOf("//") > s.indexOf("#") ||
                    s.indexOf("//") > s.indexOf("?")
                );
        };

        if (h5Form.powwownow === true) {
            //suppress debug on PWN production server
            if (h5Form.getHost(h5FormPath.dir) === 'powwownow.co.uk') {
                options.debug = 'off';
            }

            if (h5Form.getHost(h5FormPath.dir) === 'dev.powwownow.co.uk') {
                options.debug = 'on';
            }
        }

        //manipulate with paths to make relative paths work
        paths = ['cssPath', 'imgPath', 'jsPath', 'langPath'];
        $.each(paths, function (key, value) {
            if ((h5Form.testPath(options[value]) === true) && (options[value] !== '')) {
                options[value] = h5FormPath.dir + options[value];
            }
        });



        /* h5Form Methods */

        // get value from the passed options, return false if there's no option set
        h5Form.getOptionValue = function (option) {
            if ((options !== undefined) && (options[option] !== undefined)) {
                return options[option];
            }

            if ((options === undefined) || (options[option] === undefined)) {
                return false;
            }
        };

        // prepare the form
        h5Form.init = function () {
            var validNodes = ['input', 'select', 'textarea']; //DOM elements to validate

            h5Form.toConsole('form init started (' + h5Form.version + ') - ' + $(that).attr('id'));

            //Switches on/off HTML5 Validation
            if (h5Form.getOptionValue('html5Validation') !== 'on') {

                if ((($.browser.msie === true) && (parseInt($.browser.version, 10) > 8)) || ($.browser.msie === undefined)) {
                    $(that).attr('novalidate', 'novalidate');
                }

            }

            // Check if the Form is Responsive
            if (h5Form.getOptionValue('responsive') == 'off') {
                // Add pwnform class
                $(that).addClass('pwnform');

                // Add h5form class to every form
                $(that).addClass('h5form').addClass('clearfix').addClass(h5Form.getOptionValue('cssClass'));
            }

            // required asterisk only for preceding label
            if (h5Form.getOptionValue('requiredAsterisk') === 'on') {
                $(that).find('[required="required"]').prev('label').append('<sup class="asterisk">*</sup>');
                $(that).find('.required').prev('label').append('<sup class="asterisk">*</sup>');

                // wrapped input case for asterisks
                $(that).find('[required=required]').parent().prev('label').append('<sup class="asterisk">*</sup>');
                $(that).find('.required').parent().prev('label').append('<sup class="asterisk">*</sup>');

                //IE8 workaround for wrapped input case. prev selector refused to work
                if ($.browser.msie && parseInt($.browser.version, 10) === 8) {
                    $(that).find('[required=required]').parent().parent().find('label').append('<sup class="asterisk">*</sup>');
                    $(that).find('.required').parent().parent().find('label').append('<sup class="asterisk">*</sup>');
                    $(that).find('sup+sup').remove();
                }

                //Add a note if required fields exist
                if (($(that).find('[required=required]').length > 0) || ($(that).find('.required').length > 0)) {

                    // Avoid IE7 as it's causing problems here
                    // Added 2nd Part of Check, since RequiredAstrisk Action was being ignored in All NON-IE Browsers.
                    if (($.browser.msie && parseInt($.browser.version, 10) !== 7) || (!$.browser.msie)) {
                        $(that).find('[type=submit]').before('<p class="required-note" style="font-size: 10px">* &mdash; required field</p>');
                    }

                }

            }

            // @todo: focused and unfocused container options where skipped

            //Intialise the Custom Functions
            //h5Form.beforeSubmitCustom = function() {};
            h5Form.onSuccessCustom = function () {};
            h5Form.onErrorCustom = function () {};

            if (h5Form.getOptionValue('beforeSubmit')) {
                h5Form.beforeSubmitCustom = options.beforeSubmit;
            }

            if (h5Form.getOptionValue('success')) {
                h5Form.onSuccessCustom  = options.success;
            }

            if (h5Form.getOptionValue('error')) {
                h5Form.onErrorCustom  = options.error;
            }

            var genericOptions = {
                beforeSubmit: function () {
                    if (!h5Form.beforeSubmit()) {
                        return false;
                    }

                    if (h5Form.beforeSubmitCustom !== undefined && !h5Form.beforeSubmitCustom()) {
                        h5Form.toggleFormInProcess(false);
                        return false;
                    }

                },
                success: function (responseText) { h5Form.onSuccess(responseText); h5Form.onSuccessCustom(responseText); },
                error:   function (responseText) { h5Form.onError(responseText); h5Form.onErrorCustom(responseText); }
            };


            if (window.jQuery().ajaxForm) {

                $(that).ajaxForm(genericOptions);
                h5Form.toConsole('form successfully initialised');
            } else { h5Form.toConsole('form is NOT initialised', 'error'); }

            // remove error on field focus
            $(validNodes).each(function () {

                $(this).focus(function () {
                    $(this).next('.h5form-error').remove();
                });
            });


            // remove error on field focus for IE8
            if ($.browser.msie) {

                $('.mypwn-input-container input').focus(function () {
                    $(this).parent().find('.h5form-error').remove();
                });

            }


            h5Form.initPolyfills();


            // basic jQuery UI theme support
            if (options.uiSupport === 'on') {
                $(that).find('[type=button]').addClass('ui-button');
                $(that).find('button').each(function () {
                    $(this).addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only');
                    $(this).html('<span class="ui-button-text">' + $(this).html() + '</span>');
                    $(this).hover(function () {
                        $(this).removeClass('ui-state-default');
                        $(this).addClass('ui-state-hover');
                    }, function () {
                        if ($(this).is('.ui-state-hover')) {
                            $(this).removeClass('ui-state-hover');
                            $(this).addClass('ui-state-default');
                        }
                    });

                    $(this).blur(function () {
                        $(this).removeClass('ui-state-hover');
                        $(this).addClass('ui-state-default');
                    });
                });
            }

        }; // end init

        // load required css and js files
        h5Form.loadRequired = function () {

            // jquery.form is required to process ajax forms
            var requiredJsCss = [options.jsPath + options.formPlugin];

            // Define languages, en - by default
            requiredJsCss.push(options.langPath + h5Form.getOptionValue('language'));

            // Define styling if any
            if (h5Form.getOptionValue('cssStyle')) {
                requiredJsCss.push(options.cssPath + h5Form.getOptionValue('cssStyle'));
            }

            // load everything from requiredJsCss
            window.yepnope({
                load: requiredJsCss,
                complete: function () {
                    h5Form.toConsole('Required JS and CSS are loaded: ' + requiredJsCss);
                }
            });

            //init the form
            // event driven init breaks multiple forms on the page
            // so back to async init

            setTimeout(h5Form.init, 200);


        };

        // load yepnope if not loaded
        if (typeof window.yepnope !== 'function') {
            $.getScript(options.jsPath + 'yepnope/yepnope.1.5.4-min.js', function (data, textStatus, jqxhr) {
                // when yepnope is loaded, load required scripts
                h5Form.loadRequired();
            }); // end of getScript yepnope.js
        } else {
            if (h5FormPath.loaded === false) {
                h5Form.loadRequired();
            }
        }


        //Before form submit
        h5Form.beforeSubmit = function () {
            var countToValidate, elemsToValidate, errors, fieldOptions, j, message, node, validNodes;

            validNodes = { 'input': '', 'select': '', 'textarea': '' }; //DOM elements to validate
            errors =  []; // errors array

            h5Form.toConsole('generic beforeSubmit function started');

            //Remove all previous messages
            $(that).find('.message').remove();
            $(that).find('.h5form-error').remove();

            h5Form.toggleFormInProcess(true);

            //Go through all the Input Fields within the Form
            elemsToValidate = $(that).find('*').nextAll();
            countToValidate = elemsToValidate.length;
            j = 0;

            $(that).find('*').each(function () {

                node =  $(this).get(0).nodeName.toLowerCase();

                var fieldOptions = {};

                if (validNodes.hasOwnProperty(node)) {
                    /**
                     * EMPTY REQUIRED FIELD
                     * HTML Markup: <input type="text" name="first_name" class="required" /> or HTML5 markup <input type="text" name="first_name" required="required" />
                     * Error format: variable defaults to FORM_VALIDATION_(FIELD_NAME)_EMPTY; if it's not set it uses var FORM_VALIDATION_FIELD_EMPTY
                     */
                    //Normal markup
                    fieldOptions['rule' + (h5Form.countProperties(fieldOptions) + 1)] = { field: this, type: node, attribute: 'class', value: 'required', error: 'EMPTY' };

                    //HTML5 markup
                    fieldOptions['rule' + (h5Form.countProperties(fieldOptions) + 1)] = { field: this, type: node, attribute: 'required', value: 'required', error: 'EMPTY' };

                    /**Email Check**/
                    //Normal
                    fieldOptions['rule' + (h5Form.countProperties(fieldOptions) + 1)] = { field: this, type: node, attribute: 'class', value: 'email', error: 'INVALID_EMAIL_ADDRESS' };
                    //HTML5
                    fieldOptions['rule' + (h5Form.countProperties(fieldOptions) + 1)] = { field: this, type: node, attribute: 'type', value: 'email', error: 'INVALID_EMAIL_ADDRESS' };

                    /**
                    * PATTERN
                    * HTML5 markup <input type="text" name="first_name" required="required" pattern="[^0-9][A-Za-z]{2,20}" />
                    * Error format:
                    * Suggested password pattern: ^.{6,}$  - more than 6 characters
                    * Alpha/numeric with spaces and special chars
                    * Different patterns here: http://html5pattern.com
                    */
                    //HTML5 only
                    fieldOptions['rule' + (h5Form.countProperties(fieldOptions) + 1)] = { field: this, type: node, attribute: 'pattern', value: '*', error: 'INVALID_PATTERN' };

                    /**
                    * NUMBER
                    * HTML5 markup <input type="number" name="participants" max="10" min="1" required="required" value="1" />
                    * only numeric values accepted, optional min and max parameters for the values
                    * Error format:
                    * not a valid number - defaults to FORM_VALIDATION_(FIELD_NAME)_INVALID_NUMBER; if it's not set it uses var FORM_VALIDATION_INVALID_NUMBER
                    * if number is greater than max - defaults to FORM_VALIDATION_(FIELD_NAME)_OVER_MAX; if it's not set it uses var FORM_VALIDATION_OVER_MAX
                    * if number is less than min - defaults to FORM_VALIDATION_(FIELD_NAME)_UNDER_MIN; if it's not set it uses var FORM_VALIDATION_UNDER_MIN
                    *
                    * all error messages are parsed:
                    * {minValue} and {maxValue} in the error messages are substituted with actual values
                    * for example:
                    *
                    * FORM_VALIDATION_OVER_MAX = "Field value must be between {minValue} and {maxValue}";
                    *
                    * will be evaluated as:
                    * Field value must be between 1 and 10
                    */
                    fieldOptions['rule' + (h5Form.countProperties(fieldOptions) + 1)] = { field: this, type: node, attribute: 'type', value: 'number', error: 'INVALID_NUMBER' };

                }

                //check for against validation rules
                $.each(fieldOptions, function (key, value) {
                    h5Form.validationRule(fieldOptions[key], errors);
                });

            }); // end each element loop

            //Check if there are any Errors
            if (errors.length > 0) {

                h5Form.toConsole('Validation errors (' + (errors.length) + ') found on client side', 'warn');
                h5Form.toConsole(errors);

                message = window.FORM_VALIDATION_ERRORS_FOUND;

                h5Form.toggleFormInProcess(false);

                h5Form.showMessage(message, 'error');
                h5Form.showInputErrors(errors, '', options.showMultipleErrors);
                return false;
            }

            if (errors.length === 0) {
                return true;
            }

        }; //End Before Submit


        //successfull form submission
        h5Form.onSuccess = function (responseText) {
            var message, responseArr;

            h5Form.toConsole('generic onSuccess function started');
            h5Form.toggleFormInProcess(false);


            /**
             * If the http header specifies application/json as the content type, then
             * the content is automatically parsed to JSON for us, otherwise do it manually
            */

            if ((responseText[0] === '{') && (responseText[0] === '[')) {

                if (typeof responseText === 'string') {
                    responseArr = $.parseJSON(responseText);
                } else {
                    responseArr = responseText;
                }

            }
            h5Form.toConsole(responseArr);



            //Check the Response Array
            if (responseArr !== undefined) {

                //Check if there are any AJAX SERVER_SIDED Error Messages Which were called back from the PHP File
                if ((responseArr.error_messages instanceof Array) && responseArr.error_messages.length > 0) {
                    h5Form.toConsole(responseArr.error_messages);
                    h5Form.showInputErrors(responseArr.error_messages, '', true);
                    h5Form.toConsole('Error found on server side', 'warn');

                    message = window.FORM_VALIDATION_ERRORS_FOUND;
                    h5Form.showMessage(message, 'error');
                    h5Form.toConsole('Error message shown');

                    return false;
                }

                //Remove error messages
                $(that).find('.h5form-error').remove();

                //Check if the Response Message Is Set
                if ((responseArr.success_message !== undefined) && (responseArr.success_message !== '')) {
                    h5Form.showMessage(responseArr.success_message, 'success');
                    h5Form.toConsole('Success Message shown');
                }
            } else {
                h5Form.showMessage(window.FORM_VALIDATION_SUCCESS_MESSAGE, 'success');
                h5Form.toConsole('Success Message shown');
            }

        }; //End On Success Form Submission


        //Form Generic Error Function
        h5Form.onError = function (responseText) {
            var responseArr, message;

            h5Form.toConsole('generic onError function started');

            h5Form.toggleFormInProcess(false);

            if (typeof responseText === 'string') {
                responseArr = $.parseJSON(responseText);
                h5Form.toConsole('responseText is string');
            } else {
                if (responseText.error_messages !== undefined) {
                    responseArr = responseText;
                    h5Form.toConsole('responseText is object');
                } else {
                    responseArr = null;
                    h5Form.toConsole('responseText.responseText is string');
                }
            }

            if (responseArr === null) {
                responseArr = $.parseJSON(responseText.responseText);
                h5Form.toConsole('responseText.responseText is parsed as JSON');
            }



            h5Form.toConsole(responseArr);

            // convert to array
            if (($.isArray(responseArr.error_messages) === false) && (responseArr.error_messages instanceof Object)) {
                responseArr.error_messages = $.makeArray(responseArr.error_messages);
            }

            //Check the Response Object
            if (responseArr !== undefined) {

                //Check if there are any AJAX SERVER_SIDED Error Messages Which were called back from the PHP File
                if ((responseArr.error_messages instanceof Array) && (responseArr.error_messages.length > 0)) {

                    if (responseArr.error_messages[0].field_name !== undefined) {
                        if (responseArr.error_messages[0].field_name !== 'alert') {
                            h5Form.toConsole(responseArr.error_messages);
                            h5Form.showInputErrors(responseArr.error_messages, '', true);
                            h5Form.toConsole('Error found on server side', 'warn');
                            message = window.FORM_VALIDATION_ERRORS_FOUND;
                        } else {
                            message = responseArr.error_messages[0].message;
                        }
                    } else { message = responseArr.error_messages[0].message; }

                    h5Form.showMessage(message, 'error');
                    h5Form.toConsole('Error message shown');
                    return false;

                }

                if ((responseArr.error_messages === undefined) || (responseArr.error_messages.length === 0)) {

                    message = window.FORM_UNKNOWN_ERROR;
                    h5Form.showMessage(message, 'error');
                    h5Form.toConsole('unknown server side error (1)', 'error');
                    return false;
                }
            }

            if (responseArr === undefined) {

                message = window.FORM_UNKNOWN_ERROR;
                h5Form.showMessage(message, 'error');
                h5Form.toConsole('unknown server side error (2)', 'error');
                return false;

            }

        }; //End Form Generic Error Function

        // console log if debug is on and console exists
        h5Form.toConsole = function (message, type) {
            if (type === undefined) { type = 'log'; }

            if ((options !== undefined) && (options.debug !== undefined) && (!$.browser.msie)) {
                if (window.console) {
                    if ((options.debug === 'on') && (type === 'log')) { window.console.log('H5Form: ' + message); }
                    if ((options.debug === 'on') && (type === 'warn')) { window.console.warn('H5Form: ' + message); }
                    if ((options.debug === 'on') && (type === 'error')) { window.console.error('H5Form: ' + message); }
                }
            }
        };



        // toggle ajax loader and submit button on and off [ status: true/false ]
        h5Form.toggleFormInProcess = function (status) {

            if ($.browser.msie && parseInt($.browser.version, 10) === 7) {

                // function doesn't work for IE7 for time being
                h5Form.toConsole('Ajax Loader Bypassing.');

            } else {

                if (status === false) {

                    $('.ajax-loader').remove();
                    h5Form.toConsole('ajax loader is hidden');

                    $(that).find('[type=submit]').attr("disabled", false);
                    h5Form.toConsole('submit button is enabled');

                }

                if (status === true) {

                    if (($(that).find('.form-action').css('text-align') === 'right') || ($(that).find('.form-action [type=submit]').css('float') === 'right')) {
                        $(that).find('[type=submit]:first-child').before('<span class="ajax-loader"><img src="' + options.imgPath + 'ajaxLoaders/form-ajax-loader.gif" alt="loading" /></span>');
                        h5Form.toConsole('ajax loader is visible (before the submit button)');
                    } else {
                        $(that).find('[type=submit]:first-child').after('<span class="ajax-loader"><img src="' + options.imgPath + 'ajaxLoaders/form-ajax-loader.gif" alt="loading" /></span>');
                        h5Form.toConsole('ajax loader is visible (after the submit button)');
                    }

                    //Disable Submit Button, to prevent clicking the button Multiple times
                    $(that).find('[type=submit]').attr("disabled", true);
                    h5Form.toConsole('submit button is disabled');

                }

            }

        };

        // count properties in the object
        h5Form.countProperties = function (obj) {
            var count = 0;

            $.each(obj, function (key, value) {
                if (obj.hasOwnProperty(key)) {
                    count += 1;
                }
            });

            return count;
        };


        // validation parameters
        h5Form.validationRule = function (fieldOptions, errors) {
            var attributeKey, check, email, emailReg, errorMsg, errorMsgVars, field, fieldMin, fieldMax, maxValue, minValue, pattern, patternLength, val;

            attributeKey = $(fieldOptions.field).attr(fieldOptions.attribute);

            // if field is visible - proceed with validation
            if ($(fieldOptions.field).is(':visible')) {

                // edge case for patterns
                if ((fieldOptions.attribute === 'pattern')) { attributeKey = fieldOptions.value; }

                if ((attributeKey === fieldOptions.value) || ($(fieldOptions.field).hasClass(fieldOptions.value))) {

                    if (fieldOptions.error === 'EMPTY') {

                        // checkbox case
                        if ($(fieldOptions.field).is('input') === true) {
                            if ($(fieldOptions.field).attr('type') === 'checkbox') {
                                if ($(fieldOptions.field).is(':checked') === false) {
                                    check = false;
                                }
                            }
                        }

                        if (($(fieldOptions.field).val().trim() === '') || (check === false)) {
                            errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                            if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_FIELD_' + fieldOptions.error; }
                            errors.push({'message': window[errorMsg], 'field_name'  : $(fieldOptions.field).attr('name')});
                        }

                    }

                    if (fieldOptions.error === 'INVALID_EMAIL_ADDRESS') {
                        email = $(fieldOptions.field).val();

                        if (email !== '') {
                            /*http://stackoverflow.com/questions/46155/validate-email-address-in-javascript*/
                            emailReg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                            if (!emailReg.test(email)) {
                                errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                                if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_' + fieldOptions.error; }
                                errors.push({'message': window[errorMsg], 'field_name'  : $(fieldOptions.field).attr('name')});
                            }
                        }
                    }

                    if (fieldOptions.error === 'INVALID_PATTERN') {

                        patternLength = h5Form.getLengthConstrains($(fieldOptions.field).attr('pattern'));

                        // define Max and Min length from regexp
                        if (patternLength !== undefined) {
                            if (patternLength[0] !== '') { fieldMin = patternLength[0]; }

                            if (patternLength[1] !== '') { fieldMax = patternLength[1]; }
                        }

                        field = $(fieldOptions.field).val();
                        pattern = new RegExp($(fieldOptions.field).attr('pattern'));

                        if (field.length > 0) {
                            // Added First part of the Check. This is to check if the Attribute exists in the HTML element.
                            if (($(fieldOptions.field).attr('pattern') !== undefined) && (!pattern.test(field))) {

                                if (patternLength !== undefined) {

                                    if ((fieldMin !== undefined) && (fieldMax === undefined) && (field.length < fieldMin)) {
                                        errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                                        if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_LENGTH_UNDER_MIN'; }
                                        errorMsgVars = { minLength: fieldMin };
                                        errors.push({'message': h5Form.evalErrorMessage(errorMsg, errorMsgVars), 'field_name': $(fieldOptions.field).attr('name')});
                                    }

                                    else if ((fieldMin === undefined) && (fieldMax !== undefined) && (field.length > fieldMax)) {
                                        errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                                        if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_LENGTH_OVER_MAX'; }
                                        errorMsgVars = { maxLength: fieldMax };
                                        errors.push({'message': h5Form.evalErrorMessage(errorMsg, errorMsgVars), 'field_name': $(fieldOptions.field).attr('name')});
                                    }

                                    else if ((fieldMin !== undefined) && (fieldMax !== undefined) && (field.length < fieldMin) && (field.length > fieldMax)) {
                                        errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                                        if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_LENGTH_BETWEEN'; }
                                        errorMsgVars = { minLength: fieldMin, maxLength: fieldMax };
                                        errors.push({'message': h5Form.evalErrorMessage(errorMsg, errorMsgVars), 'field_name': $(fieldOptions.field).attr('name')});
                                    }

                                    else {
                                        errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                                        if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_' + fieldOptions.error; }
                                        errors.push({'message': window[errorMsg], 'field_name'  : $(fieldOptions.field).attr('name')});
                                    }
                                }

                                if (patternLength === undefined) {
                                    errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                                    if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_' + fieldOptions.error; }
                                    errors.push({'message': window[errorMsg], 'field_name'  : $(fieldOptions.field).attr('name')});
                                }
                            }
                        }

                    }

                    if (fieldOptions.error === 'INVALID_NUMBER') {
                        val = $(fieldOptions.field).val();

                        if ((isNaN(val) === true) && (val !== undefined)) {

                            errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_' + fieldOptions.error;
                            if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_' + fieldOptions.error; }
                            errors.push({'message': window[errorMsg], 'field_name'  : $(fieldOptions.field).attr('name')});

                        }

                        if ((val !== '') && (isNaN(val) === false)) {
                            maxValue = $(fieldOptions.field).attr('max');
                            minValue = $(fieldOptions.field).attr('min');

                            if ((minValue !== undefined) && (val < minValue)) {
                                errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_UNDER_MIN';
                                if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_UNDER_MIN'; }

                                errorMsgVars = { minValue: minValue, maxValue: maxValue};
                                errors.push({'message': h5Form.evalErrorMessage(errorMsg, errorMsgVars), 'field_name': $(fieldOptions.field).attr('name')});
                            }

                            if ((maxValue !== undefined) && (val > maxValue)) {
                                errorMsg = 'FORM_VALIDATION_' + h5Form.getFormInputName($(fieldOptions.field).attr('name')).toUpperCase() + '_OVER_MAX';
                                if (typeof window[errorMsg] === 'undefined') { errorMsg = 'FORM_VALIDATION_OVER_MAX'; }

                                errorMsgVars = { minValue: minValue, maxValue: maxValue};
                                errors.push({'message': h5Form.evalErrorMessage(errorMsg, errorMsgVars), 'field_name': $(fieldOptions.field).attr('name')});
                            }
                        }

                    }

                    return errors;
                }

            } // if field is visible

        }; // End validation rule

        // input name is taken as plain attribute or inside the square brackets
        h5Form.getFormInputName = function (formName) {
            //Check if the formName has [] Square Brackets in it
            if ((formName.indexOf("[") >= 0) && (formName.indexOf("]") >= 0)) {
                return formName.substring(formName.indexOf("[") + 1, formName.indexOf("]"));
            }

            if ((formName.indexOf("[") < 0) && (formName.indexOf("]") < 0)) {
                return formName;
            }
        };

        // show message on the form, status is passed as a css class
        h5Form.showMessage = function (message, status, formSelector, pos) {
            var position, position_legacy, icon;

            //trim message
            if (typeof message === 'string') {
                message = message.trim();
            }

            position = h5Form.getOptionValue('messagePos');

            //legacy code support
            position_legacy = h5Form.getOptionValue('successMessagePos');

            if (position_legacy !== false) {
                position = position_legacy;
            }

            if (formSelector === undefined) {
                formSelector = that;
            } else {
                if (pos !== undefined) {
                    position = pos; //take extra parameter for message position in play
                }
            }


            //JQuery UI support
            if (options.uiSupport === 'on') {
                if (status === 'error') {
                    status = 'error ui-state-error';
                    icon = 'error-icon ui-icon ui-icon-alert ui-corner-all';
                }

                if (status === 'success') {
                    status = 'success ui-state-highlight';
                    icon = 'success-icon ui-icon ui-icon-check ui-corner-all';
                }
            }


            // evaluate the message if it hasn't been done before

            if (window[message] !== undefined) {
                message = window[message];
            } else {
                h5Form.toConsole('message didn\'t evaluate. Displayed as is: ' + message);
            }

            if (position === 'off') {
                return;
            }

            if (position === 'below') {
                $(formSelector).append('<div class="' + status + ' message"><span class="' + icon + '" style="float: left; margin-right: .3em;"></span>' + message + '</div>');
            }

            if (position === 'above') {
                $(formSelector).prepend('<div class="' + status + ' message"><span class="' + icon + '" style="float: left; margin-right: .3em;"></span>' + message + '</div>');
            }

            if (status === 'success') { $(formSelector).find('.message').delay(10000).fadeOut('slow'); }
        };

        // show errors opposite input fields
        h5Form.showInputErrors = function (fieldsArr, quotedConstants, showMultiple, formSelector) {
            var fieldIndex, fieldHandle, prevHandle, quotedMsg;

            if (formSelector === undefined) {
                formSelector = that;
            }

            if (showMultiple === undefined) {
                showMultiple = false;
            }

            if (quotedConstants === undefined) {
                quotedConstants = false;
            }

            // Hide all existing error boxes
            $('.h5form-error').remove();

            // If there is nothing to validate then validation has passed
            if (fieldsArr.length === 0) {
                return true;
            }

            fieldIndex = fieldsArr.length;


            for (fieldIndex in fieldsArr) {
                if (fieldsArr.hasOwnProperty(fieldIndex)) {
                    // If the constant is quoted then get the constant value
                    if (quotedConstants) {
                        quotedMsg = fieldsArr[fieldIndex].message;
                        fieldsArr[fieldIndex].message = window.quotedMsg;
                    }

                    // If the field name is specified as alert, then show an alert box
                    if (fieldsArr[fieldIndex].field_name === 'alert') {
                        window.alert(fieldsArr[fieldIndex].message);
                    } else {

                        // store previous field
                        prevHandle = fieldHandle;

                        fieldHandle = $(formSelector).find("[name=\"" + fieldsArr[fieldIndex].field_name + "\"]");

                        //evaluate message if not yet evaluated
                        if (window[fieldsArr[fieldIndex].message] !== undefined) {
                            fieldsArr[fieldIndex].message = window[fieldsArr[fieldIndex].message];
                        }


                        // to prevent multiple error containers in 1 field
                        if ($(fieldHandle).attr('name') !== $(prevHandle).attr('name')) {
                            fieldHandle.after('<div class="h5form-error nice-error-box" id="' + h5Form.getFormInputName(fieldsArr[fieldIndex].field_name) + '-error-box">' + fieldsArr[fieldIndex].message + '</div>');
                        } else {
                            $('#' + h5Form.getFormInputName(fieldsArr[fieldIndex].field_name) + '-error-box').append('<br/>' + fieldsArr[fieldIndex].message);
                        }

                        // position error message
                        //var rightOffset = '-'+$("#"+ getFormInputName(fieldsArr[fieldIndex]['field_name']) + "-error-box").width()+'px';
                        //$("#"+ getFormInputName(fieldsArr[fieldIndex]['field_name']) + "-error-box").css('right',rightOffset);

                        // Remove the error when the field the error message for becomes focused,
                        // fieldHandle.focusin(function() {
                        // console.log(fieldsArr[fieldIndex]['field_name']);
                        //     $('#' + fieldsArr[fieldIndex]['field_name'] + '-error-box').remove();
                        // });
                    }

                    // Quit looping and show only 1 message if we are not showing multiple messages
                    if (!showMultiple) {
                        return false;
                    }
                }
            }

            return false;
        }; // end of ShowInputErrors


        // lets us access show errors/messages methods publicly through jQuery namespace
        $.showInputErrors = h5Form.showInputErrors;
        $.showMessage = h5Form.showMessage;

        /**
        *  function evaluates the error message and subsitutes variables in it
        *  errorMsg - name of the error variable
        *  obj - object with keys/values to replace in error message
        *  e.g. to substitute {maxValue} in the error message
        *  the following obj should be passed obj = { maxValue : 5 }
        */

        h5Form.evalErrorMessage = function (errorMsg, obj) {
            var errorMsgEval = window[errorMsg];

            $.each(obj, function (key, value) {
                if (obj.hasOwnProperty(key)) {
                    if (errorMsgEval !== undefined) {
                        errorMsgEval = errorMsgEval.replace('{' + key + '}', obj[key]);
                    }

                    // warn if variable doesn't exist
                    if ((errorMsgEval === undefined) && (errorMsg !== undefined)) {
                        h5Form.toConsole(errorMsg + ' variable is not defined', 'error');
                    }
                }
            });

            return errorMsgEval;
        };

        // get field length constrains from the regexp
        h5Form.getLengthConstrains = function (str) {

            var arr, results = [], re = /{([^}]+)}/g, text;

            if ((str !== undefined) && (str.charAt(str.length - 2) === '}')) {
                while (text = re.exec(str)) {
                    results.push(text[1]);
                }

                if (results[0] !== undefined) {
                    arr = results[0].split(',');
                }
            }

            return arr;
        };

        h5Form.objectLength = function (obj) {
            var length = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    length += 1;
                }
            }
            return length;
        };

        h5Form.initPolyfills = function () {

            /* Date Picker Fallback */
            if ($("input[type='date']").length > 0) {

                //if datepicker is not loaded
                if (typeof $.datepicker === "undefined") {

                    window.yepnope({
                        test : window.Modernizr.inputtypes && window.Modernizr.inputtypes.date,
                        nope : [
                            options.jsPath + 'jQuery/jquery-ui-1.8.7.custom.min.js',
                            options.jsPath + 'jQuery/ui.datepicker.js',
                            options.cssPath + 'vendor/jQuery/jquery-ui-1.8.7.custom.css',
                            options.cssPath + 'vendor/jQuery/ui.datepicker.css',
                            options.cssPath + 'vendor/jQuery/ui.theme.css'
                        ],
                        complete: function () {
                            if ($.datepicker) {
                                $("input[type='date']").datepicker({ dateFormat: "dd-mm-yy" });

                                $('input[type=date]').each(function () {
                                    if ($(this).attr('max') !== undefined) {
                                        $(this).datepicker("option", "maxDate", $(this).attr('max'));
                                    }

                                    if ($(this).attr('min') !== undefined) {
                                        $(this).datepicker("option", "minDate", $(this).attr('min'));
                                    }
                                });
                            }
                        }
                    });
                } else {
                    $("input[type='date']").datepicker({ dateFormat: "dd-mm-yy" });

                    $('input[type=date]').each(function () {
                        if ($(this).attr('max') !== undefined) {
                            $(this).datepicker("option", "maxDate", $(this).attr('max'));
                        }

                        if ($(this).attr('min') !== undefined) {
                            $(this).datepicker("option", "minDate", $(this).attr('min'));
                        }
                    });
                }
            }
        };
    }; //end initH5Form

}(jQuery));

//*** The functionality below will be moved to separate files soon ***//
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, ''); 
    };
}
