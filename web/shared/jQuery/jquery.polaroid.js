(function( $ ) {
	$.fn.polaroid = function( options ) {
		var settings = $.extend({
			cufon : false,
			animate : {
				duration : 200,
				easing : 'linear',
				callback : function() { }
			},
			zoom : 1.3,
			delay : 200,
			preAnimate : function() { }
		}, options || {} );
			
		$images = this.find('img');
		
		$images.bind( 'mouseenter.polaroid', function() {
			// Set timeout to call function after specified delay
			// Attach timeout id to the container of the image hovered over using jQuery's .data() method
			$(this).closest('.polaroid').data( 'timeout', window.setTimeout( function ($originalImage, settings) {
				// Timeouts are normally called in the 'global' window context, but we want to give this function a reference to the current 'this' (the image hovered over)
				// So we write an anonymous function that executes immediately and we pass in whatever we want ( in our case $(this) and settings )
				// All this function does is return the actual timeout callback, which forms a closure with the parent function and thus has access to its arguments
				return function() {
					// Make a copy of the original container
					$originalContainer = $originalImage.closest('.polaroid');
					$overlaidContainer = $originalContainer.clone();
					
					// Position absolutely exactly over the top of the existing container
					$overlaidContainer
						.attr('id', 'polaroidOverlay')
						.css({
							'position': 'absolute',
							'z-index': 999,
							'top': $originalContainer.offset().top,
							'left': $originalContainer.offset().left,
							'width': $originalContainer.width()
						}).find('p').removeClass('hidden');
					
					// Hack for cufon - extract original heading text from cufontext elements and paste into overlay container, so cufon can be re-applied
					// This is because of problems in IE
					if ( settings.cufon ) {
						$overlaidContainer.find('h3').html( function() {
							var ret = '';
							$originalContainer.find('cufontext').each( function() {
								ret += $(this).html();
							});
							return ret;
						});
					}
											
					// Insert into DOM
					$('body').prepend( $overlaidContainer );
					
					settings.preAnimate.call( $overlaidContainer );
					
					$overlaidImage = $overlaidContainer.find('img');
					
					/*
					var customZoom = $originalImage.attr('zoom');
					alert('customZoom: '+customZoom);
					var zoom = customZoom ? ( customZoom : settings.zoom );
					*/
					var zoom = settings.zoom;
										
					var heightChange = ($overlaidImage.height() * zoom) - $overlaidImage.height();
					var widthChange = ($overlaidImage.width() * zoom) - $overlaidImage.width();
					
					// Expand image & container
					var containerAnimateParams = {
						'top' : '-=' + heightChange/2,
						'left' : '-=' + widthChange/2,
						'width' : '+=' + widthChange
					};
					
					var imageAnimateParams = jQuery.extend({}, containerAnimateParams);		// shallow copy
					imageAnimateParams['height'] = '+=' + heightChange;
											
					$overlaidContainer.animate( containerAnimateParams, settings.animate.duration, settings.animate.easing, settings.animate.callback );
					$overlaidImage.animate( imageAnimateParams, settings.animate.duration, settings.animate.easing, settings.animate.callback );
				}
			}( $(this), settings ), settings.delay ));
		}).bind( 'mouseleave.polaroid', function() {
			if ( $(this).closest('.polaroid').data('timeout') != null ) {
				window.clearTimeout( $(this).closest('.polaroid').data('timeout') );
			};
		});

		$(document).on('mouseleave.polaroid', '#polaroidOverlay', function() {
			$(this).remove();
		});
		
		// For method chaining
		return this;
	};
}( jQuery ));
