/**
 * This is used to store Jquery Dialog Related Methods
 */
var dialogs = {};

/**
 * Create JQuery Dialog Functionality
 *
 * @param dialogInfo - Multidimensional Object Describing the Dialog
 *    dialogInfo.xxx.id    - The Unique Dialog ID [Required]
 *    dialogInfo.xxx.link  - The Unique Dialog Link, to Show the Dialog [Default: id-link]
 *    dialogInfo.xxx.close - The Unique Dialog Link, to Close the Dialog [Default: id-close]
 *    dialogInfo.xxx.onClick - Show on 'onClick' ? [Default: true]
 *    dialogInfo.xxx.onHover - Show on 'onHover' ? [Default: false]
 */
dialogs.initDialog = function (dialogInfo) {
    if (dialogInfo instanceof Object) {
        var x;
        for (x in dialogInfo) {
            // Check Required Fields
            if (!("id" in dialogInfo[x])) {
                continue;
            }

            // Additional Fields Check
            var id = dialogInfo[x]['id'],
                link = ("link" in dialogInfo[x]) ? dialogInfo[x]['link'] : dialogInfo[x]['id']+'-link',
                close = ("close" in dialogInfo[x]) ? dialogInfo[x]['close'] : dialogInfo[x]['id']+'-close',
                parentId = ("parentId" in dialogInfo[x]) ? dialogInfo[x]['parentId'] : dialogInfo[x]['id']+'-parent',
                onClick = ("onClick" in dialogInfo[x]) ? dialogInfo[x]['onClick'] : true,
                onHover = ("onHover" in dialogInfo[x]) ? dialogInfo[x]['onHover'] : false;

            // Dialog Options Check
            var dialogCloseOnEscape = ('closeOnEscape' in dialogInfo[x]) ? dialogInfo[x]['closeOnEscape'] : true,
                dialogHeight = ('height' in dialogInfo[x]) ? dialogInfo[x]['height'] : 'auto',
                dialogMaxHeight = ('maxHeight' in dialogInfo[x]) ? dialogInfo[x]['maxHeight'] : false,
                dialogMaxWidth = ('maxWidth' in dialogInfo[x]) ? dialogInfo[x]['maxWidth'] : false,
                dialogMinHeight = ('minHeight' in dialogInfo[x]) ? dialogInfo[x]['minHeight'] : 150,
                dialogMinWidth = ('minWidth' in dialogInfo[x]) ? dialogInfo[x]['minWidth'] : 150,
                dialogModal = ('modal' in dialogInfo[x]) ? dialogInfo[x]['modal'] : true,
                dialogPosition = ('position' in dialogInfo[x]) ? dialogInfo[x]['position'] : { my: "center", at: "center", of: window },
                dialogTitle = ('title' in dialogInfo[x]) ? dialogInfo[x]['title'] : null,
                dialogWidth = ('width' in dialogInfo[x]) ? dialogInfo[x]['width'] : 300,
                dialogResizeable = ('resizable' in dialogInfo[x]) ? dialogInfo[x]['resizable'] : false;

            // Default They should be Hidden
            $('#'+id).hide();

            // Set Data Tags on ID
            $('#'+id).attr({
                'data-dialogCloseOnEscape': dialogCloseOnEscape,
                'data-dialogHeight': dialogHeight,
                'data-dialogMaxHeight': dialogMaxHeight,
                'data-dialogMaxWidth': dialogMaxWidth,
                'data-dialogMinHeight': dialogMinHeight,
                'data-dialogMinWidth': dialogMinWidth,
                'data-dialogModal': dialogModal,
                'data-dialogPosition': dialogPosition.my+','+dialogPosition.at, // This is not complete
                'data-dialogTitle': dialogTitle,
                'data-dialogWidth': dialogWidth,
                'data-parentId': parentId,
                'data-dialogResizeable': dialogResizeable
            });

            // onClick Event
            if (onClick === true) {
                $('#'+link).click(function(){
                    var id = $(this).attr('id').replace('-link','');
                    $('#'+id).dialog({
                        closeOnEscape: $('#'+id).attr('data-dialogCloseOnEscape'),
                        height: $('#'+id).attr('data-dialogHeight'),
                        maxHeight: $('#'+id).attr('data-dialogMaxHeight'),
                        maxWidth: $('#'+id).attr('data-dialogMaxWidth'),
                        minHeight: $('#'+id).attr('data-dialogMinHeight'),
                        minWidth: $('#'+id).attr('data-dialogMinWidth'),
                        modal: $('#'+id).attr('data-dialogModal'),
                        position: { my: "center", at: "center", of: window }, // This is not complete
                        title: $('#'+id).attr('data-dialogTitle'),
                        width: $('#'+id).attr('data-dialogWidth'),
                        resizable: $('#'+id).attr('data-dialogResizeable')
                    });
                    $('#'+id).parent().attr('id',$('#'+id).attr('data-parentId'));
                });
            }

            // onHover Event
            if (onHover === true) {
                $('#'+link).hover(function(){
                    var id = $(this).attr('id').replace('-link','');
                    $('#'+id).dialog({
                        closeOnEscape: $('#'+id).attr('data-dialogCloseOnEscape'),
                        height: $('#'+id).attr('data-dialogHeight'),
                        maxHeight: $('#'+id).attr('data-dialogMaxHeight'),
                        maxWidth: $('#'+id).attr('data-dialogMaxWidth'),
                        minHeight: $('#'+id).attr('data-dialogMinHeight'),
                        minWidth: $('#'+id).attr('data-dialogMinWidth'),
                        modal: $('#'+id).attr('data-dialogModal'),
                        position: { my: "center", at: "center", of: window }, // This is not complete
                        title: $('#'+id).attr('data-dialogTitle'),
                        width: $('#'+id).attr('data-dialogWidth'),
                        resizable: $('#'+id).attr('data-dialogResizeable')
                    });
                    $('#'+id).parent().attr('id',$('#'+id).attr('data-parentId'));
                },function(){
                    var id = $(this).attr('id').replace('-close','');
                    $('#'+id).dialog('close');
                });
            }

            // Close Link - Randomly Working.
            $('#'+close).click(function(){
                var id = $(this).attr('id').replace('-close','');
                $('#'+id).dialog('close');
            });
        }
    }
};
