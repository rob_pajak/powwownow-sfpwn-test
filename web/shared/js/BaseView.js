(function(){

    // Please report any bugs.

    /**
     * BaseView,js
     * @author Dav
     * @type {*|{Models: {}, Collections: {}, Views: {}, helpers: {}}}
     */
    window.Pwn = window.Pwn || { Models : {},Collections : {},Views:{}, helpers:{}, functions:{} };

    /**
     *
     * @param id
     * @returns {*}
     */
    Pwn.helpers.template = function(id) {
        if(  $(document.body).find($(id)).length) {
            return _.template($(id).html());
        }
        Pwn.helpers.log('Pwn.helpers.template Failed loading template ', id);
    }

    /***
     * Just another way to debug to the console, without breaking in browsers that
     * don't support the console.
     * @param message
     * @param object
     * @param type
     *  log, warn, error
     */
    Pwn.helpers.log = function(message, object, type) {
        var hosts;
        
        hosts = ['sflocalhost.powwownow.co.uk', 'dev.powwownow.co.uk', 'staging.powwownow.co.uk', 'sqi.powwownow.co.uk'];
        
        if (_.contains(hosts, window.location.host)) {

            if (_.isUndefined(message)) {
                message = '--';
            }
            if (_.isUndefined(type)) {
                type = 'log';
            }
            if (_.isUndefined(object)) {
                object = {};
            }
            if (window.console){

                if (type === 'log') { 
                    window.console.log(message, object); 
                }
                if (type === 'warn') { 
                    window.console.warn(message, object); 
                }
                if (type === 'error') { 
                    window.console.error(message, object); 
                }
            }
        }
    }

    /**
     * ErrorHandler
     * Handles all errors returned by API calls and displays
     * them correctly on a form. This is a rewrite of pwnplus.js error handling.
     * @type {*}
     */
    Pwn.Views.ErrorHandler = Backbone.View.extend({
        quotedConstants : true,
        showMultiple : true,
        className: 'pwn-custom-error',
        validation : function (options) {
            if (!jQuery().parsley) {
                Pwn.helpers.log('$parsley plugin not loaded');
                return true;
            }
            var defaults = {
                formElement : this.formElement
            };

            $.extend(defaults, options)
            
            $(defaults.formElement).parsley({
                errorClass: 'error',
                animate: true,
                animateDuration: 200,
                errors: {
                    errorsWrapper: '<small>',
                    errorElem: '<p>',
                    classHandler: function ( elem, isRadioOrCheckbox ) {
                        return $(elem).parent();
                    }
                },
                listeners: {
                    onFieldError: function ( elem, constraints, ParsleyField ) {
                        // Remove any zombie error messages
                        $('.pwn-custom-error').remove();
                    },
                    onFieldSuccess: function ( elem, constraints, ParsleyField ) {
                        // Remove any zombie error messages
                        $('.pwn-custom-error').remove();
                    }
                }
            });

            return $(defaults.formElement).parsley('validate');
        },
        onError : function (responseText) {
            var responseArr, count;
            Pwn.helpers.log('Pwn.Views.ErrorHandler/onError : responseArr initialized', responseText);
            if ( _.isString(responseText) ) {
                responseArr = jQuery.parseJSON(responseText);
            } else {
                responseArr = responseText;
            }
            if ( _.isNull(responseArr) ) {
                responseArr = $.parseJSON(responseText.responseText);
            }
            //Check the Response Array
            if (! _.isUndefined(responseArr)) {
                count = _.size(responseArr);
                if (count > 1 && this.showMultiple === true) {
                    Pwn.helpers.log('Pwn.Views.ErrorHandler/onError : Starting showMultipleInputErrors', responseArr);
                    this.showMultipleInputErrors(responseArr);
                } else {
                    if (_.has(responseArr, 'error_messages')) {
                        Pwn.helpers.log('Pwn.Views.ErrorHandler/onError : responseArr', responseArr);
                        this.showInputErrors(responseArr.error_messages[0]);
                    } else if (_.isObject(responseArr, 'message')) {
                        Pwn.helpers.log('Pwn.Views.ErrorHandler/onError : message', responseArr);
                        this.showInputErrors(responseArr);
                    } else {
                        Pwn.helpers.log('Pwn.Views.ErrorHandler/onError : responseArr not found', responseArr);
                    }
                }
            } else {
                Pwn.helpers.log('Pwn.Views.ErrorHandler/onError : responseArr isUndefined', responseArr);
            }

        },
        getFormInputName : function (formName) {
            //Check if the formName has [] Square Brackets in it
            if ((formName.indexOf("[") >= 0) && (formName.indexOf("]") >= 0)) {
                return formName.substring(formName.indexOf("[") + 1, formName.indexOf("]"));
            }
            if ((formName.indexOf("[") < 0) && (formName.indexOf("]") < 0)) {
                return formName;
            }
        },
        showInputErrors : function (fieldsArr) { 

            var that, fieldName, fieldMessage, fieldsArr;
            that = this;
            // Hide all existing error boxes
            $('small').remove();
            // If there is nothing to validate then validation has passed
            if (_.isEmpty(fieldsArr)) {
                return true;
            }

            if (_.isArray(fieldsArr)) {
                fieldsArr = _.flatten(fieldsArr);
                if (_.has(fieldsArr, 'message') && _.has(fieldsArr,'field_name')) {
                    fieldName = fieldsArr[0].field_name;
                    fieldMessage = fieldsArr[0].message;
                } else if (_.has(fieldsArr[0], 'message') && _.has(fieldsArr[0],'field_name')) {
                    fieldName = fieldsArr[0].field_name;
                    fieldMessage = fieldsArr[0].message;
                } else {
                    Pwn.helpers.log('showInputErrors', 'error');
                    return false;
                }

            } else if (_.isObject(fieldsArr)) {
                if (_.has(fieldsArr, 'message') && _.has(fieldsArr,'field_name')) {
                    fieldName = fieldsArr.field_name;
                    fieldMessage = fieldsArr.message;
                } else {
                    Pwn.helpers.log('showInputErrors', 'error');
                    return false;
                }
            }

            if (_.isUndefined(fieldMessage)) {
                return false;
            }
            if (_.isUndefined(fieldName)) {
                return false;
            }
            
            that.outputError(fieldName, fieldMessage);

            return false;
        },
        showMultipleInputErrors : function(fieldsArr) {

            var fieldsArr, fieldName, fieldMessage, that;
            that = this;
            fieldsArr = _.flatten(fieldsArr);

            _.each(fieldsArr, function(k, v){
                if (_.has(k, 'message') && _.has(k,'field_name')) {

                    fieldName = k.field_name;
                    fieldMessage = k.message;

                    if (_.isUndefined(fieldMessage)) {
                        Pwn.helpers.log('Pwn.Views.ErrorHandler/showMultipleInputErrors : fieldMessage isUndefined', fieldMessage);
                        return false;
                    }
                    if (_.isUndefined(fieldName)) {
                        Pwn.helpers.log('Pwn.Views.ErrorHandler/showMultipleInputErrors : fieldName isUndefined', fieldMessage);
                        return false;
                    }
                    that.outputError(fieldName, fieldMessage);

                } else {
                    Pwn.helpers.log('Pwn.Views.ErrorHandler/showMultipleInputErrors : message & field_name missing', fieldMessage);
                    return false;
                }
            });
        },
        outputError: function (fieldName, fieldMessage) {

            var prevHandle, fieldHandle;
            if (_.isUndefined(this.quotedConstants)) {
                this.quotedConstants = false;
            }

            if (this.quotedConstants) {
                fieldMessage = _.result(window, fieldMessage);
            }

            if (fieldName === 'alert') {
                window.alert(fieldMessage);
            } else if (fieldName !== 'alert') {
                // store previous field
                prevHandle = fieldHandle;
                fieldHandle = $(this.formElement).find("[name=\"" + fieldName + "\"]");
                // to prevent multiple error containers in 1 field
                if ($(fieldHandle).attr('name') !== $(prevHandle).attr('name')) {
                    fieldHandle.after('<small class="'+this.className+'"><p>' + fieldMessage + '</p></small>').parent().addClass('error');
                } else {
                    Pwn.helpers.log("@todo");
                } 
            }
        }
    });

    /**
     * Requests Collection
     * Handles all requests to API and add response to collection.
     * @type {*}
     */
    Pwn.Collections.Requests = Backbone.Collection.extend({
        url: '',
        formElement: '',
        parse: function(response, options) {
            this.add(response)
        },
        initialize: function() {
            Pwn.helpers.log('Pwn.Collections.Requests.initialize : initialized');
            this.on('error',this.responseError);
        },
        responseError: function(model, error) {
            var model = new this.model();
            // We are Push the errors into the model
            // So that each script must deal with errors
            // just in case we want a better way to handle them.
            if (error.status === 400 || error.status === 500) {
                if (_.has(error,'responseText' )) {
                    model.set({'status': 'error'});
                    model.set($.parseJSON(error.responseText))
                    this.push(model);
                } else {
                    this.technicalError();
                }
            }else {
                this.technicalError();
            }
        },
        submitForm: function(formElement) {
            var that = this;
            this.formElement = formElement;
            this.url = $(formElement).attr('action');
            this.fetch({
                data: $(formElement).serialize(),
                type:'POST',
                add: true, // add to exisiting,
                beforeSend: function() {
                    that.showThrobber().disableForm();
                },
                success: function(){
                    Pwn.helpers.log('Pwn.Collections.Requests: success');
                    that.hideThrobber().enableForm();
                },
                error: function() {
                    Pwn.helpers.log('Pwn.Collections.Requests: error');
                    that.hideThrobber().enableForm();
                }
            }).then(
                function(){
                    that.hideThrobber().enableForm();
                },
                function() {
                    that.hideThrobber().enableForm();
                }
            );
        },
        showThrobber: function() {
            var $form = $(this.formElement),
                $throbber = $form.find('.throbber');
            
            if (_.isObject($throbber)) {
                if ($throbber.length > 0) {
                    $throbber.removeClass('hide');
                }
            }
            return this;
        },
        hideThrobber: function() {
            var $form = $(this.formElement),
                $throbber = $form.find('.throbber');
            
            if (_.isObject($throbber)) {
                if ($throbber.length > 0) {
                    $throbber.addClass('hide');
                }
            }
            return this;
        },
        disableForm: function() {
            var $form = $(this.formElement),
                $button = $form.find(':submit');
            $button.attr('disabled', 'disabled');
            return this;
            
        },
        enableForm: function() {
            var $form = $(this.formElement),
                $button = $form.find(':submit');
            $button.removeAttr('disabled');
            return this;
        },
        technicalError: function() {
            alert('Powwownow has encountered a problem and can not complete the request. Please try again. We are sorry for the inconvenience');
            return this;
        }
    });

    /**
     *
     * @type {*}
     */
   Pwn.Views.RequestHandler = Pwn.Views.ErrorHandler.extend({
       submitForm: function(event) {
           event.preventDefault();

           if (this.validation() === true) {
               this.collection.submitForm(this.formElement);
           }
       }
   });

})();
