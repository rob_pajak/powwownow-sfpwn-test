<?php
// this check prevents access to debug front controllers that are deployed by accident to production servers.
// feel free to remove this, extend it or make something more sophisticated.
/*
if (!in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1')))
{
  die('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');

$configuration = ProjectConfiguration::getApplicationConfiguration('pwn', 'dev', true);
sfContext::createInstance($configuration)->dispatch();
*/

// this check prevents access to debug front controllers that are deployed by accident to production servers.
// feel free to remove this, extend it or make something more sophisticated.

require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');

// The PWNSafeIp validator contains the same regex, but the framework hasn't been bootstrapped here so we cant use it
$pattern = '#^(127\.0\.0\.|10\.170\.[56789]\.)[0-9]{1,3}$|^85\.90\.37\.253$|^5\.63\.1[6-9]|2[0-3]\..{1,3}$#';

if (!preg_match($pattern, @$_SERVER['REMOTE_ADDR']))
{
  $configuration = ProjectConfiguration::getApplicationConfiguration('pwn', 'prod', false);
  sfContext::createInstance($configuration)->getController()->forward('errorModule', 'error404');
}
else {
  $configuration = ProjectConfiguration::getApplicationConfiguration('pwn', 'dev', true);
  sfContext::createInstance($configuration)->dispatch();
}
