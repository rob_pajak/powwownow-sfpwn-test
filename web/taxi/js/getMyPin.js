(function(){
    
    window.Pwn = window.Pwn || { Models : {}, Collections : {}, Views:{}, helpers:{} }; 
    
    Pwn.Models.GetMyPin =  Backbone.Model.extend({
        defaults: {
            status : 'initialized'
        }
    });
    
    Pwn.Collections.GetMyPin = Pwn.Collections.Requests.extend({
        model: Pwn.Models.GetMyPin
    });

    Pwn.Views.GetMyPin = Pwn.Views.RequestHandler.extend({
        el : '#get-my-pin-mobile-container',
        template: Pwn.helpers.template('#get-my-pin-mobile-template'),
        formElement : '#get-my-pin-mobile',
        events: {
            'submit #get-my-pin-mobile' : 'submitForm'
        },
        initialize: function() {
            _.bindAll(this, 'submitForm','render','validation','responseSuccess');
            this.listenTo(this.collection, 'add', this.responseSuccess);
            this.render();
        },
        render: function() {
            try {
                this.$el.html(this.template());
            } catch (e) {
                Pwn.helpers.log(e);
            }
            return this;
        },
        responseSuccess: function(model) {
            var status,
                error;
            if (_.has(model.attributes, 'status')) {
                status = model.get('status');
                if (status === 'error') {
                    if (_.has(model.attributes, 'error_messages')) {
                        error = model.get('error_messages');
                        switch(error[0].message) {
                            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN':
                            case 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE':
                                $('#modal-email-taken').foundation('reveal','open');
                                break;
                            default:
                                this.onError(error);
                            break;
                        }
                    }
                } else if (status === 'success') {
                    if ( status === 'success') {
                        // I Added this in, since There was no tracking on this page. Ask Simon
                        // dataLayer.push({'event': '/Taxi/RegistrationSuccess'});
                        window.location.replace("/s/taxi/yourPin");
                    }
                } else {
                    this.collection.technicalError();
                }
            } else {
                this.collection.technicalError();
            }
        }
    });

    var GetMyPinCollection = new Pwn.Collections.GetMyPin({
        model: Pwn.Collections.GetMyPin
    });
    
    var GetMyPinTop = new Pwn.Views.GetMyPin({
        collection : GetMyPinCollection
    });

    
})();
