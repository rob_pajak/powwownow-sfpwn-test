<?php

/**
 * Collects the PWN logic for valid passwords.
 *
 * Valid passwords must comply with the following rules:
 *  - The obvious: must not be empty and must be a string,
 *  - Must have a minimum length of 6,
 *  - Must not start or end with a space, and,
 *  - Must comply with a maximum length if any is given (I pity the fool whom does not check the database column!).
 *
 * @author Maarten Jacobs
 */
class PWNValidatorPass extends sfValidatorBase
{

    /**
     * Configures the current validator.
     *
     * Available options:
     *
     *  * max_length: The maximum length of the string
     *
     * Available error codes:
     *
     *  * max_length
     *
     * @param array $options   An array of options
     * @param array $messages  An array of error messages
     *
     * @see sfValidatorBase
     */
    protected function configure($options = array(), $messages = array())
    {
        // Add a message for the string type validation.
        $this->addMessage('invalid_type', 'FORM_VALIDATION_NEW_PASSWORD_EMPTY');

        // Add the option max_length, as used by sfValidatorString.
        // @todo: message for an overly long password.
        $this->addMessage('max_length', 'FORM_VALIDATION_CONFIRM_PASSWORD_EMPTY');
        $this->addOption('max_length');

        // Add a message for the minimum length validation.
        $this->addMessage('min_length', 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS');
        $this->addOption('min_length');
    }

    /**
     * Cleans and validates the value.
     *
     * @param mixed $value
     * @return mixed
     *
     * @see sfValidatorBase
     */
    public function clean($value)
    {
        return $this->doClean($value);
    }

    /**
     * Cleans and validates the value.
     *
     * @param mixed $value
     * @return mixed
     *
     * @throws sfValidatorError
     * @see sfValidatorBase
     */
    protected function doClean($value)
    {
        // Check if the given value is a string.
        if (!is_string($value)) {
            throw new sfValidatorError($this, 'invalid_type');
        }

        $clean = (string) $value;
        $length = function_exists('mb_strlen') ? mb_strlen($clean, $this->getCharset()) : strlen($clean);

        // From sfValidatorString.
        if ($this->hasOption('max_length') && $length > $this->getOption('max_length')) {
            throw new sfValidatorError($this, 'max_length', array('value' => $value, 'max_length' => $this->getOption('max_length')));
        }

        if ($length < 6) {
            throw new sfValidatorError($this, 'min_length', array('value' => $value));
        }

        return $value;
    }

}
