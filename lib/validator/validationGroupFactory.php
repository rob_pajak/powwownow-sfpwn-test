<?php
/**
 * Returns standard sets of SF validator classes to allow consistent validation and error messages in our application
 *
 * @author Mark Wiseman
 *
 */
class validationGroupFactory {

    /*First Name*/
    public function firstName() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required'   => 'FORM_VALIDATION_NO_FIRST_NAME', 'min_length' => 'FORM_VALIDATION_NO_FIRST_NAME')
        );
    }

    /*Last Name*/
    public function lastName($required = true) {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => $required),
            array('required'   => 'FORM_VALIDATION_NO_SURNAME', 'min_length' => 'FORM_VALIDATION_NO_SURNAME')
        );
    }

    /*Your Name*/
    public function yourName() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required'   => 'FORM_VALIDATION_NO_NAME', 'min_length' => 'FORM_VALIDATION_NO_NAME')
        );
    }

    /*Friends Name*/
    public function friendsName() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required'   => 'FORM_VALIDATION_NO_FRIENDS_NAME', 'min_length' => 'FORM_VALIDATION_NO_FRIENDS_NAME')
        );
    }

    /*Email Address*/
    public function email() {
        return new sfValidatorEmail(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required' => 'FORM_VALIDATION_NO_EMAIL', 'invalid' => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS', 'min_length' => 'FORM_VALIDATION_NO_EMAIL')
        );
    }

    /*Friends Email Address*/
    public function friendsEmail() {
        return new sfValidatorEmail(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required' => 'FORM_VALIDATION_NO_EMAIL', 'invalid' => 'FORM_VALIDATION_NO_FRIENDS_EMAIL', 'min_length' => 'FORM_VALIDATION_NO_EMAIL')
        );
    }

    /*Address*/
    public function address() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required'   => 'FORM_VALIDATION_NO_ADDRESS', 'min_length' => 'FORM_VALIDATION_NO_ADDRESS')
        );
    }

    /*Town*/
    public function town() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true, 'max_length' => 50),
            array('required'   => 'FORM_VALIDATION_NO_TOWN', 'min_length' => 'FORM_VALIDATION_NO_TOWN')
        );
    }

    /*Post Code*/
    public function postcode() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true, 'max_length' => 20),
            array('required'   => 'FORM_VALIDATION_INVALID_POSTCODE', 'min_length' => 'FORM_VALIDATION_INVALID_POSTCODE')
        );
    }

    /*Company*/
    public function company() {
        return new sfValidatorString(array('required' => false, 'max_length' => 50));
    }

    /*County*/
    public function county() {
        return new sfValidatorString(array('required' => false, 'max_length' => 50));
    }

    /*Comments*/
    public function comments($required = true) {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => $required),
            array('required'   => 'FORM_VALIDATION_NO_COMMENTS', 'min_length' => 'FORM_VALIDATION_NO_COMMENTS')
        );
    }   

    /*Country*/
    public function country($required = true) {
        if ($required) {
            return new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_COUNTRY', 'min_length' => 'FORM_VALIDATION_NO_COUNTRY')
            );
        } else {
            return new sfValidatorString(array('required' => false, 'min_length' => 1, 'trim' => true));
        }
    }

    /*From Date*/
    public function fromDate() {
        return new sfValidatorString(
            array('min_length' => 10, 'trim' => true, 'required' => true, 'max_length' => 10),
            array('required'   => 'FORM_VALIDATION_INVALID_FROM_DATE', 'min_length' => 'FORM_VALIDATION_INVALID_FROM_DATE', 'max_length' => 'FORM_VALIDATION_INVALID_FROM_DATE')
        );
    }

    /*To Date*/
    public function toDate() {
        return new sfValidatorString(
            array('min_length' => 10, 'trim' => true, 'required' => true, 'max_length' => 10),
            array('required'   => 'FORM_VALIDATION_INVALID_TO_DATE', 'min_length' => 'FORM_VALIDATION_INVALID_TO_DATE', 'max_length' => 'FORM_VALIDATION_INVALID_TO_DATE')
        );
    }

    /*Question*/
    public function question($required = true) {
        if ($required) {
            return new sfValidatorString(
                array('min_length' => 1, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_QUESTION', 'min_length' => 'FORM_VALIDATION_NO_QUESTION')
            );
        } else {
            return new sfValidatorString(array('required' => false, 'min_length' => 1, 'trim' => true));
        }
    }

    /**
     * Not required, but if specified allows only plus sign, spaces and numbers with min length of 5
     * @return sfValidatorRegex
     */
    public function mobileNumber() {
        return new sfValidatorRegex(
            array('pattern' => '/^[+ 0-9]{5,}$/', 'required' => false),
            array('invalid' => 'INVALID_MOBILE_NUMBER')
        );
    }

    public function phoneNumber($required = false) {
        return new sfValidatorRegex(
            array('pattern' => '/^[+ 0-9]{5,}$/', 'required' => $required),
            array('required' => 'FORM_VALIDATION_INVALID_PHONE_NUMBER', 'invalid' => 'FORM_VALIDATION_INVALID_PHONE_NUMBER')
        );
    }

    public function phoneNumberNotNull() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required'   => 'FORM_VALIDATION_INVALID_PHONE_NUMBER', 'min_length' => 'FORM_VALIDATION_INVALID_PHONE_NUMBER')
        );
    }

    /**
     * @see PWNValidatorPass
     * @return PWNValidatorPass
     */
    public function password() {
        return new PWNValidatorPass(array('required' => false, 'max_length' => 50, 'trim' => true));
    }

    /** Password New **/
    public function passwordNew($required = true) {
        if ($required) {
            return new PWNValidatorPass(
                array('min_length' => 6, 'trim' => true, 'required' => true),
                array('required'   => 'FORM_VALIDATION_NO_PASSWORD', 'min_length' => 'FORM_VALIDATION_NO_PASSWORD')
            );
        } else {
            return new PWNValidatorPass(array('required' => false, 'max_length' => 50, 'trim' => true));
        }
    }

    /**
     * Validates $passwordFieldName matches $confirmFieldName
     *
     * You probably want to call it as as a post validator in a form->configure()
     * @code $this->validatorSchema->setPostValidator($validationFactory->confirmPassword());
     *
     * @param string $passwordFieldName
     * @param string $confirmFieldName
     * @return sfValidatorSchemaCompare
     */
    public function confirmPassword($passwordFieldName = 'password', $confirmFieldName = 'confirm_password') {
        return new sfValidatorSchemaCompare($confirmFieldName, sfValidatorSchemaCompare::EQUAL, $passwordFieldName, array(), array('invalid' => 'FORM_VALIDATION_PASSWORD_MISMATCH'));
    }

    /**
     * Validates $emailFieldName matches $confirmFieldName
     *
     * You probably want to call it as as a post validator in a form->configure()
     * @code $this->validatorSchema->setPostValidator($validationFactory->confirmEmail());
     *
     * @param string $emailFieldName
     * @param string $confirmFieldName
     * @return sfValidatorSchemaCompare
     */
    public function confirmEmail($emailFieldName = 'email', $confirmFieldName = 'confirm_email') {
        return new sfValidatorSchemaCompare($confirmFieldName, sfValidatorSchemaCompare::EQUAL, $emailFieldName, array(), array('invalid' => 'FORM_VALIDATION_EMAIL_MISMATCH'));
    }    

    /*PIN Ref*/
    public function pinRef($required = true) {
        if ($required) {
            return new sfValidatorString(
                array('required' => true),
                array('required' => 'FORM_VALIDATION_NO_PIN')
            );
        } else {
            return new sfValidatorString(array('required' => false));
        }
    }

    /** Generic Non Required Form Field **/
    public function genericField() {
        return new sfValidatorString(array('required' => false, 'max_length' => 50, 'trim' => true));
    }

    /** Generic Non Required Form Field **/
    public function genericFieldNoMax() {
        return new sfValidatorString(array('required' => false, 'trim' => true));
    }

    /*voice_prompt_language*/
    public function voicePromptLanguage() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required'   => 'FORM_VALIDATION_NO_LANGUAGE', 'min_length' => 'FORM_VALIDATION_NO_LANGUAGE')
        );
    }

    /*entry_exit_announcements*/
    public function entryExitAnnouncements() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true),
            array('required'   => 'FORM_VALIDATION_NO_ENTRY_EXIT_MUSIC', 'min_length' => 'FORM_VALIDATION_NO_ENTRY_EXIT_MUSIC')
        );
    }

    /*announcement_type*/
    public function announcementType() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true, 'max_length' => 50),
            array('required'   => 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE', 'min_length' => 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE')
        );
    }

    /*chairman_present*/
    public function chairmanPresent() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true, 'max_length' => 20),
            array('required'   => 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT', 'min_length' => 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT')
        );
    }

    /*play_participant_count*/
    public function playParticipantCount() {
        return new sfValidatorString(
            array('min_length' => 1, 'trim' => true, 'required' => true, 'max_length' => 50),
            array('required'   => 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT', 'min_length' => 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT')
        );
    }

    /**
     * Common Non Required Form Field
     *
     * Symfony requires all fields to have some validation, this is a 'catch all' validator to use if you have nothing
     * better to use
     */
    public function commonField($trim = true, $max_length = 50, $required = false, $uneditable = false) {
        $output = array('trim' => $trim, 'required' => $required);
        if ($max_length) $output['max_length'] = $max_length;
        if ($uneditable) $output['uneditable'] = true;
        return new sfValidatorString($output);
    }

}
