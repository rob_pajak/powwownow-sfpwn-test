<?php
/**
 * Validates if the specified IP is considered to be on our whitelist (ie local, internal, on VPN etc)
 *
 * pwn_dev.php contains the same regex, but we can't use the framework there as it is yet to be bootstrapped
 *
 * @author Wiseman
 */
class PWNSafeIp extends sfValidatorRegex
{
    const REGEX_IP_WHITELIST = '#^(127\.0\.0\.|10\.170\.[56789]\.|10\.160\.[56789]\.)[0-9]{1,3}$|^85\.90\.37\.253$|^5\.63\.1[6-9]|2[0-3]\..{1,3}$#';

    /**
     * @see sfValidatorRegex
     */
    protected function configure($options = array(), $messages = array())
    {
        parent::configure($options, $messages);
        $this->setOption('pattern', self::REGEX_IP_WHITELIST);
    }
}