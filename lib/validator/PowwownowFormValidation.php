<?php

/**
 * Class PowwownowFormValidation
 *
 * Form Validation Class
 */
class PowwownowFormValidation
{
    /***** SPECIFIC FIELDS *****/


    public function getValidationForPinRef($choices)
    {
        return new sfValidatorChoice(
            array('choices' => $choices, 'required' => true, 'multiple' => true),
            array('required' => 'FORM_VALIDATION_NO_PIN', 'invalid' => 'FORM_VALIDATION_NO_PIN')
        );
    }

    public function getValidationForVoicePromptLanguage($choices)
    {
        return new sfValidatorChoice(
            array('choices' => $choices, 'required' => true),
            array('required' => 'FORM_VALIDATION_NO_LANGUAGE', 'invalid' => 'FORM_VALIDATION_NO_LANGUAGE')
        );
    }

    public function getValidationForOnHoldMusic($choices)
    {
        return new sfValidatorChoice(
            array('choices' => $choices, 'required' => false),
            array()
        );
    }

    public function getValidationForEntryExitAnnouncements($choices)
    {
        return new sfValidatorChoice(
            array('choices' => $choices, 'required' => true),
            array(
                'required' => 'FORM_VALIDATION_NO_ENTRY_EXIT_MUSIC',
                'invalid' => 'FORM_VALIDATION_NO_ENTRY_EXIT_MUSIC'
            )
        );
    }

    public function getValidationForAnnouncementType($choices)
    {
        return new sfValidatorChoice(
            array('choices' => $choices, 'required' => true),
            array(
                'required' => 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE',
                'invalid' => 'FORM_VALIDATION_NO_ENTRY_EXIT_TYPE'
            )
        );
    }

    public function getValidationForChairmanPresent($choices)
    {
        return new sfValidatorChoice(
            array('choices' => $choices, 'required' => true),
            array(
                'required' => 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT',
                'invalid' => 'MYPWN_CALLSETTINGS_CHAIRMAN_PRESENT'
            )
        );
    }

    public function getValidationForPlayParticipantCount($choices)
    {
        return new sfValidatorChoice(
            array('choices' => $choices, 'required' => true),
            array(
                'required' => 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT',
                'invalid' => 'FORM_VALIDATION_NO_PLAY_PARTICIPANT_COUNT'
            )
        );
    }


    /***** GENERIC FIELDS *****/


    public function getValidationGenericNotRequired()
    {
        return new sfValidatorString(array('required' => false, 'trim' => true));
    }
}
