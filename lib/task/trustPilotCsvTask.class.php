<?php

/**
 * TrustPilotCsv symfony task generates links from data provided in CSV file.
 * It takes path to CSV file to be processed and saves data to other CSV file.
 * 
 * INPUT CSV's example content:
 * 1,peterholten@gmail.com,Annie,1234
 * 2,clivemiller@meetingsphere.com,Clive Miller,95
 * 3,Hayden.Reinders@maitlandgroup.co.za,Hayden Reinders,96
 * 5,Paulbuddin@waitrose.com,Paul Buddin,97
 * 
 * OUTPUT CSV's example content:
 * Annie,peterholten@gmail.com,http://www.trustpilot.com/evaluate/....
 * "Clive Miller",clivemiller@meetingsphere.com,http://www.trustpilot.com/evaluate/....
 * "Hayden Reinders",Hayden.Reinders@maitlandgroup.co.za,http://www.trustpilot.com/evaluate/....
 * "Paul Buddin",Paulbuddin@waitrose.com,http://www.trustpilot.com/evaluate/....
 * "David Keefe",david.keefe2@virginmedia.com,http://www.trustpilot.com/evaluate/....

 * 
 * @package    powwownow
 * @subpackage task
 * @author     Michal Macierzynski, Kamil Skowron
 * @version    1.0
 */
class trustPilotCsvTask extends sfBaseTask
{

    /**
     * Default value for suburl link parameter
     * 
     * @var string
     */
    CONST DEFAULT_SUBURL = 'www.powwownow.co.uk'; 

    /**
     * Human readable descriptions of retrieved data
     * 
     * @var int
     */
    CONST DATA_ROW_ID   = 0; // ignored at moment of writing this code
    CONST DATA_EMAIL    = 1;
    CONST DATA_NAME     = 2;
    CONST DATA_ORDER_ID = 3;
    
    /**
     * Hold the number of columns that will be retrived from CSV file
     * 
     * @var int
     */
    CONST DATA_NUMBER_OF_COLUMNS = 4;
    
    /**
     * String used to validate email
     * 
     * @var string
     */
    CONST EMAIL_VALIDATION_REGEX
        = "/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*+[a-z]{2}/is";
    
    /**
     * Method configures task before executing it.
     * 
     * @return void
     * 
     * @author Kamil Skowron <kamil.skowron@powwownow.com>
     */
    protected function configure()
    {
        $inputOption = new sfCommandOption(
            'input',
            null,
            sfCommandOption::PARAMETER_REQUIRED,
            'Input csv file path'
        );
        
        $outputOption = new sfCommandOption(
            'output',
            null,
            sfCommandOption::PARAMETER_OPTIONAL,
            'Output csv file path'
        );
        
        $suburlOption = new sfCommandOption(
            'suburl',
            null,
            sfCommandOption::PARAMETER_OPTIONAL,
            'Suburl will be used inside link'
        );
        
        $this->addOptions(array($inputOption, $outputOption, $suburlOption));

        $this->namespace        = '';
        $this->name             = 'trustPilotCsv';
        $this->briefDescription = '';

        $description = array(
            'The trustPilotCsv task generates unique links from csv file.',
            'Call it with:',
            'symfony trustPilotCsv --input="/path/input.csv" [output|suburl]',
            'Additional informations:',
            'Optional --output defaults to $fileName_out.csv',
            'Optional --suburl defaults to "www.demoshop.com"',            
        );
        
        $this->detailedDescription = implode("\n", $description);
    }

    /**
     * Method executed when command line tool is called
     * 
     * @param array $arguments Array of non-named arguments
     * 
     * @param array $options   Associative array of named options
     * 
     * @throws Exception Thrown when:
     * - input file path is not readable
     * - data inside CSV file is invalid
     * 
     * @author Kamil Skowron <kamil.skowron@powwownow.com>
     */
    protected function execute($arguments = array(), $options = array())
    {
        $secretKey = 'i52wm98kx73cy46ot29bs83ja67fr45e';
        
        if (!is_readable($options['input'])) {
            throw new Exception('Invalid input file provided');
        }
        
        $inputFile = $options['input'];
        $csvData   = $this->processCsvFile($inputFile);
        
        $suburl = $options['suburl'];
        if($suburl === null) {
            $suburl = self::DEFAULT_SUBURL;
        }
        
        $outputFile = $options['output'];
        if($outputFile === null) {
            $outputFile = str_replace('.csv', '_out.csv', $inputFile);
        }        
        
        $url = "http://www.trustpilot.com/evaluate/%s?a=%d&b=%s&c=%s&e=%s";
        
        foreach ($csvData as $customer) {
            
            if (count($customer) !== self::DATA_NUMBER_OF_COLUMNS) {
                throw new Exception(
                    sprintf(
                        'Invalid input data row count is not %d',
                        self::DATA_NUMBER_OF_COLUMNS
                    )
                );
            }
            
            $orderId = $customer[self::DATA_ORDER_ID];
            $email   = $customer[self::DATA_EMAIL];
            $name    = $customer[self::DATA_NAME];

            $outputData[] = array(
                $name,
                $email,
                sprintf(
                    $url,
                    $suburl,
                    $orderId,
                    base64_encode($email),
                    urlencode($name),
                    sha1($secretKey . $email . $orderId)
                )
            );
        }

        $fp = fopen($outputFile, 'w');
        foreach ($outputData as $fields) {
            fputcsv($fp, $fields);
        }
        
        fclose($fp);
    }
    
    /**
     * Protected method process' input csv file and validates it
     * 
     * @param string $fileName File path to input csv file
     * 
     * @return array $data_to_proceed Array of data to proceed
     * 
     * @author Michal Macierzynski <michal.macierzynski@powwownow.com>
     * 
     * @amended Kamil Skowron <kamil.skowron@powwownow.com>
     */
    protected function processCsvFile($fileName)
    {
        $dataToProceed = array();
        $regex         = self::EMAIL_VALIDATION_REGEX;
        $row           = 0;

        if (($handle = fopen($fileName, "r")) !== false) {
            
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {

                $row++;                
              
                if ($data[self::DATA_EMAIL] != ''
                    && $data[self::DATA_NAME] != ''
                    && is_numeric($data[self::DATA_ORDER_ID])                        
                    && $data[self::DATA_ORDER_ID] > 0
                    && preg_match($regex, $data[self::DATA_EMAIL])
                ) {
                    
                    $dataToProceed[] = array(
                        $data[self::DATA_ROW_ID],
                        $data[self::DATA_EMAIL],
                        $data[self::DATA_NAME],
                        $data[self::DATA_ORDER_ID]
                    );
                    
                } else {
                    
                    throw new Exception(
                        sprintf('Invalid data format in line: %d', $row)
                    );
                }
    	    }
            
    	    fclose($handle);
            
        } else{
            throw new Exception(
                sprintf('Error opening file: %s', $fileName)
            );            
        }
        
        return $dataToProceed;       
    }
}
