#!/bin/bash
#
# $1 pretend:                               yes/no,             !dev always yes!, no only for live
# $2 environment                            prod/dev/local
# $3 no of day in in the past from today    3 three days ago
# $4 pounds                                 5 pounds
# $5 pounds                                 5 pounds
#
# usage
#   dev
#       lib/task/send2ndNotificationAfterBalanceDrop.sh yes marcin 6 5 5
#   live
#       lib/task/send2ndNotificationAfterBalanceDrop.sh no prod 3 5 5

export PATH=/usr/local/scripts/include:$PATH
. pwnlog.sh
. pwnutils.sh

pwnlock $0

cd /var/www/www.powwownow.co.uk
php symfony pluspayg:sendBalanceDropEmail --pretend=$1 --env=$2 --day_in_the_past=$3 --min_balance=$4 --after_drop=$5

pwnunlock $0