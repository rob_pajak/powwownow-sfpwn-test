#!/bin/bash
#
# $1 environment                            prod/dev/local
#
# usage
#   dev
#       lib/task/PostPayGenerateBundleAllowances.sh local
#   live
#       lib/task/PostPayGenerateBundleAllowances.sh prod

export PATH=/usr/local/scripts/include:$PATH
. pwnlog.sh
. pwnutils.sh

# By using the first day of the current month instead of the current date, we ensure that the month variable will
# always be next month.
MONTH=`php -r "echo date('m', strtotime(date('Y-m-01') . ' +1 month'));"`
YEAR=`php -r "echo date('Y', strtotime(date('Y-m-01') . ' +1 month'));"`

#cd /var/www/www.powwownow.co.uk
cd /var/www/sfPwn
pwnlock $0

symfony postpay:GenerateBundleAllowances --year=$YEAR --month=$MONTH --env=$1

pwnunlock $0
