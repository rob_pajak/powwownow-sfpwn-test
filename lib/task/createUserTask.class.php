<?php
/**
 * CreateUserTask generates multiple user accounts for any of the 5 user types:
 * 1 - Enhanced (symfony createUser enhanced)
 * 2 - Plus User (symfony createUser plusUser)
 * 3 - Plus Admin (symfony createUser plusAdmin)
 * 4 - Premium User (symfony createUser premiumUser)
 * 5 - Premium Admin (symfony createUser premiumAdmin)
 *
 * If no specific settings are passed to the class, then the script will automatically run all the cases for the
 * specific user type
 *
 * The specific settings are listed below:
 *
 * createUser userType      - Where userType can be from the above list
 *
 * --country                - Contact - Country [iso_3_code]
 * --yuuguu_enabled         - Contact - Yuuguu Enabled? [true,false]
 * --voice_enabled          - PIN - Voice Enabled? [true,false]
 * --multiple_pins          - PIN - Do you want Multiple PINs? [true,false]
 *
 * --bwm_uk_geo_enabled     - PLUS - BWM - Do you want BWM UK GEO Enabled? [true,false]
 * --bwm_uk_ff_enabled      - PLUS - BWM - Do you want BWM UK FF Enabled? [true,false]
 * --uk_geo_enabled         - PLUS - Do you want UK GEO Enabled? [true,false]
 * --uk_ff_enabled          - PLUS - Do you want UK FF Enabled? [true,false]
 * --ww_geo_enabled         - PLUS - Do you want WW GEO Enabled? [true,false]
 * --ww_ff_enabled          - PLUS - Do you want WW FF Enabled? [true,false]
 * --bwm_label              - PLUS - Branded Welcome Message Label [string]
 *
 * --sc_dedicated_enabled   - PREMIUM - Do you want SC Dedicated Enabled [true,false]
 * --ff_dedicated_enabled   - PREMIUM - Do you want FF Dedicated Enabled [true,false]
 * --geo_dedicated_enabled  - PREMIUM - Do you want GEO Dedicated Enabled [true,false]
 * --sc_master_enabled      - PREMIUM - Do you want SC Master Enabled [true,false]
 * --ff_master_enabled      - PREMIUM - Do you want FF Master Enabled [true,false]
 * --geo_master_enabled     - PREMIUM - Do you want GEO Master Enabled [true,false]
 *
 * --application            - The Application (default: pwn)
 * --env                    - The Environment (default: dev)
 * --culture                - The Culture (default: en)
 * --locale                 - The Locale (default: en_GB)
 * --plus_service_ref       - The Plus Service Ref (default: 850)
 *
 * --title                  - Contact - Title (default: Mr)
 * --first_name             - Contact - First Name (default: FirstName)
 * --last_name              - Contact - Last Name (default: LastName)
 * --password               - Contact - Password (default: 123123)
 * --source                 - Contact - Source (default: Create-User-Task)
 * --phone_number           - Contact - Phone Number / Mobile Number (default: 123123123)
 * --organisation           - Contact - Organisation (default: PWN)
 * --location               - Contact - Location (default: UK)
 *
 */
class createUserTask extends sfBaseTask {

    /**
     * This will Store the Options to be used within the Task
     * @var array
     */
    protected $pwn_options = array();

    /**
     * This will store the User Object
     * @var myUser
     */
    protected $user;

    /**
     * Task Configuration
     */
    protected function configure() {
        // Generic Variables
        $this->namespace        = '';
        $this->name             = 'createUser';
        $this->briefDescription = 'This reminds me of Sims. You create your own user, and play around with him.';

        // Add Arguments
        $this->addArguments(array(
            new sfCommandArgument('userType', sfCommandArgument::REQUIRED, 'User Type'),
        ));

        // Add Options
        $this->addOptions(array(
            new sfCommandOption('country', null, sfCommandOption::PARAMETER_OPTIONAL, 'Country'),
            new sfCommandOption('yuuguu_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'Yuuguu Enabled'),
            new sfCommandOption('voice_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'Voice Enabled'),
            new sfCommandOption('multiple_pins', null, sfCommandOption::PARAMETER_OPTIONAL, 'Multiple PINs / PIN Pairs'),

            new sfCommandOption('bwm_uk_geo_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'Branded Welcome Message GEO Enabled'),
            new sfCommandOption('bwm_uk_ff_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'Branded Welcome Message FF Enabled'),
            new sfCommandOption('uk_geo_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'Non BWM UK GEO Enabled'),
            new sfCommandOption('uk_ff_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'NON BWM UK FF Enabled'),
            new sfCommandOption('ww_geo_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'NON BWM WW GEO Enabled'),
            new sfCommandOption('ww_ff_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'NON BWM WW FF Enabled'),
            new sfCommandOption('bwm_label', null, sfCommandOption::PARAMETER_OPTIONAL, 'Branded Welcome Message', 'Branded Welcome Message'),

            new sfCommandOption('sc_dedicated_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'Shared Cost Dedicated Enabled'),
            new sfCommandOption('ff_dedicated_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'FF Dedicated Enabled'),
            new sfCommandOption('geo_dedicated_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'GEO Dedicated Enabled'),
            new sfCommandOption('sc_master_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'Shared Cost Master Enabled'),
            new sfCommandOption('ff_master_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'FF Master Enabled'),
            new sfCommandOption('geo_master_enabled', null, sfCommandOption::PARAMETER_OPTIONAL, 'GEO Master Enabled'),

            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The Application', 'pwn'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('culture', null, sfCommandOption::PARAMETER_REQUIRED, 'The culture', 'en'),
            new sfCommandOption('locale', null, sfCommandOption::PARAMETER_OPTIONAL, 'Locale', 'en_GB'),
            new sfCommandOption('plus_service_ref', null, sfCommandOption::PARAMETER_OPTIONAL, 'Plus Service Ref', 850),

            new sfCommandOption('title', null, sfCommandOption::PARAMETER_OPTIONAL, 'The Title', 'Mr'),
            new sfCommandOption('first_name', null, sfCommandOption::PARAMETER_OPTIONAL, 'First Name', 'FirstName'),
            new sfCommandOption('last_name', null, sfCommandOption::PARAMETER_OPTIONAL, 'Last Name', 'LastName'),
            new sfCommandOption('password', null, sfCommandOption::PARAMETER_OPTIONAL, 'Password', '123123'),
            new sfCommandOption('source', null, sfCommandOption::PARAMETER_OPTIONAL, 'Source', 'Create-User-Task'),
            new sfCommandOption('phone_number', null, sfCommandOption::PARAMETER_OPTIONAL, 'Phone (Mobile, Business) Number', '123123123'),
            new sfCommandOption('organisation', null, sfCommandOption::PARAMETER_OPTIONAL, 'The Company', 'PWN'),
            new sfCommandOption('location', null, sfCommandOption::PARAMETER_OPTIONAL, 'Location', 'UK'),
        ));
    }

    /**
     * This is the Actual Method Which is ran first
     * @param array $arguments
     * @param array $options
     * @return bool false
     */
    protected function execute($arguments = array(), $options = array()) {
        // Setup the sfContext Instance
        sfContext::createInstance(ProjectConfiguration::getApplicationConfiguration($options['application'], $options['env'], false));
        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        $_SERVER['REMOTE_ADDR'] = '';
        $output = array();
        $options['userType'] = $arguments['userType'];
        $this->pwn_options = $options;

        // Setup Globals
        $this->reBuildArguments();
        $this->user = sfContext::getInstance()->getUser();

        $this->logSection('OptionsCheck', 'Options:' . serialize($this->pwn_options));

        // Check the User Type
        switch ($this->pwn_options['userType']) {
            case 'enhanced':
                $output = $this->createUserEnhanced();
                break;
            case 'plusUser':
                $output = $this->createUserPlusUser();
                break;
            case 'plusAdmin':
                $output = $this->createUserPlusAdmin();
                break;
            case 'premiumUser':
                $output = $this->createUserPremiumUser();
                break;
            case 'premiumAdmin':
                $output = $this->createUserPremiumAdmin();
                break;
            case 'disable':
                $output = $this->disableTestAccounts();
                break;
            default:
                $this->logSection('OptionsCheck', 'Not a Correct User Type was selected: ' . $options['userType']);
                break;
        }

        return false;
    }

    /**
     * Create a Enhanced User
     * @return array
     */
    private function createUserEnhanced() {
        $output = array();
        foreach ($this->pwn_options['defaultAccountTestPlan'] as $counter => $testData) {
            $testData['email'] = $this->pwn_options['emailFormatting'] . substr(uniqid(),6) . '@powwownow.com';
            $this->logSection('email', $testData['email']);

            $result = Common::doCreateOrUpdateAccount(array(
                'email'      => $testData['email'],
                'first_name' => $this->pwn_options['first_name'],
                'last_name'  => $this->pwn_options['last_name'],
                'password'   => $this->pwn_options['password'],
                'source'     => $this->pwn_options['source'],
                'locale'     => $this->pwn_options['locale'],
                'ip'         => '',
                'browser'    => '',
                'os'         => ''
            ));

            // Check if there was an Error from the above Hermes Call
            if (isset($result['error'])) {
                $output[$counter] = $result['error'];
                $this->logSection('Account-Creation-Error', print_r($result['error'],true));
            } elseif ($result['statusCode'] == 204) {
                $updates = $this->updateContactAndPin(array(
                    'contact_ref'    => $result['contact']['contact_ref'],
                    'country_code'   => $testData['country'],
                    'hasYuuguu'      => $testData['yuuguu_enabled'],
                ),array(
                    'pin_ref'                   => $result['pin']['pin_ref'],
                    'voice_conference_enabled'  => $testData['voice_enabled'],
                ),array());

                // Status Check
                if ($updates) {
                    $testData['contact_ref'] = $result['contact']['contact_ref'];
                    $output[$counter] = $testData;
                    $this->logSection('Account-Creation-Success', 'Enhanced Account was created Successfully');
                } else {
                    $output[$counter] = 'Pin || Contact Updates Failed';
                    $this->logSection('ERROR', 'Enhanced Account was created Unsuccessfully');
                }
            }
        }

        return $output;
    }

    /**
     * Create a Plus User
     * @return array
     */
    private function createUserPlusUser() {
        return $this->createUserPlusAdmin();
    }

    /**
     * Create a Plus Admin
     * @return array
     */
    private function createUserPlusAdmin() {
        $output = array();
        $dnisReferences = $this->getDNISReferences();
        $connectionFee = $dnisReferences['connectionFee'];
        $dnisPerRate   = $dnisReferences['dnisPerRate'];
        foreach ($this->pwn_options['defaultAccountTestPlan'] as $counter => $testData) {
            $pin_chairman = array();
            $pin_participant = array();
            $addedProducts = array();
            $testData['email']   = $this->pwn_options['emailFormatting'] . substr(uniqid(),6) . '@powwownow.com';
            if ('plusUser' == $this->pwn_options['userType']) {
                $testData['email'] = str_replace('plusUser','plusAdmin',$testData['email']);
                $testData['emailPU'] = $this->pwn_options['emailFormatting'] . substr(uniqid(),6) . '@powwownow.com';
            }
            $this->logSection('email', $testData['email']);
            if (isset($testData['emailPU'])) $this->logSection('emailPU', $testData['emailPU']);

            // Plus Admin Registration
            try {
                $result = Hermes_Client_Rest::call('doPlusRegistrationRegister', array(
                    'email'        => $testData['email'],
                    'title'        => $this->pwn_options['title'],
                    'first_name'   => $this->pwn_options['first_name'],
                    'last_name'    => $this->pwn_options['last_name'],
                    'phone'        => $this->pwn_options['phone_number'],
                    'mobile_phone' => $this->pwn_options['phone_number'],
                    'organisation' => $this->pwn_options['organisation'],
                    'location'     => $this->pwn_options['location'],
                    'password'     => $this->pwn_options['password'],
                    'service_ref'  => $this->pwn_options['plus_service_ref'],
                    'source'       => $this->pwn_options['source'],
                    'locale'       => $this->pwn_options['locale'],
                ));
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->logSection('EXCEPTION', 'Create Plus Registration Failed');
                $output[$counter] = 'Create Plus Registration Failed';
                sfContext::getInstance()->getLogger()->err('Default.doPlusRegistrationRegister Hermes call has timed out.');
                continue;
            } catch (Hermes_Client_Exception $e) {
                $this->logSection('EXCEPTION', 'Create Plus Registration Failed');
                $output[$counter] = 'Create Plus Registration Failed';
                sfContext::getInstance()->getLogger()->err('Default.doPlusRegistrationRegister Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            } catch (Exception $e) {
                $this->logSection('EXCEPTION', 'Create Plus Registration Failed');
                $output[$counter] = 'Create Plus Registration Failed';
                sfContext::getInstance()->getLogger()->err('Default.doPlusRegistrationRegister Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            }

            // Account Was Not Successful
            if ($result['code'] != 204) {
                $this->logSection('FAILED', 'Plus Account Creation Failed');
                continue;
            }

            $this->logSection('CREATE-PLUS-ADMIN', 'Plus Account Creation Success');
            $account_id        = $result['account']['id'];
            $contactRef        = $result['contact']['contact_ref'];
            $pin_chairman[]    = $result['pins']['chairman']['pin_ref'];
            $pin_participant[] = $result['pins']['participant']['pin_ref'];
            $contactRefPU      = false;

            $testData['contact_ref'] = $result['contact']['contact_ref'];
            $testData['account_id']  = $result['account']['id'];

            $this->user->setAttribute('contact_ref', $contactRef);
            $this->user->setAttribute('account_id', $account_id);
            $updatesPlusUser = array('status' => true);

            // Create Plus User
            if ('plusUser' == $this->pwn_options['userType']) {
                // Add Contact and PINs to Plus Account
                $resultCreateUser = PinHelper::doPlusAddContactAndPins(array(
                    'title'            => $this->pwn_options['title'],
                    'first_name'       => $this->pwn_options['first_name'],
                    'last_name'        => $this->pwn_options['last_name'],
                    'email'            => $testData['emailPU'],
                    'phone'            => $this->pwn_options['phone_number'],
                    'password'         => $this->pwn_options['password'],
                    'locale'           => $this->pwn_options['locale'],
                    'source'           => $this->pwn_options['source'],
                    'service_ref'      => $this->pwn_options['plus_service_ref'],
                    'account_id'       => $account_id,
                ),false,$contactRef);

                if (empty($resultCreateUser)) {
                    $output[$counter] = 'Create User Failed';
                    $this->logSection('FAILED', 'Plus User Creation Failed');
                    continue;
                }

                $contactRefPU = $resultCreateUser['result']['contact']['contact_ref'];
                $testData['contact_ref_plusUser'] = $contactRefPU;
                $testData['UserPINRefs'] = array(
                    'chairman'    => $resultCreateUser['result']['pins']['chairman']['pin_ref'],
                    'participant' => $resultCreateUser['result']['pins']['participant']['pin_ref']
                );

                // Plus User
                $updatesPlusUser = $this->updateContactAndPin(array(
                    'contact_ref'    => $resultCreateUser['result']['contact']['contact_ref'],
                    'country_code'   => $testData['country'],
                    'hasYuuguu'      => $testData['yuuguu_enabled'],
                ),array(),array(
                    array(
                        'pin_ref'                   => $resultCreateUser['result']['pins']['chairman']['pin_ref'],
                        'voice_conference_enabled'  => $testData['voice_enabled'],
                    ),
                    array(
                        'pin_ref'                   => $resultCreateUser['result']['pins']['participant']['pin_ref'],
                        'voice_conference_enabled'  => $testData['voice_enabled'],
                    )
                ));
                $this->logSection('CREATE-PLUS-USER', 'Created Successfully');
            }

            // Plus Admin
            $updates = $this->updateContactAndPin(array(
                'contact_ref'    => $result['contact']['contact_ref'],
                'country_code'   => $testData['country'],
                'hasYuuguu'      => $testData['yuuguu_enabled'],
            ),array(),array(
                array(
                    'pin_ref'                   => $result['pins']['chairman']['pin_ref'],
                    'voice_conference_enabled'  => $testData['voice_enabled'],
                ),
                array(
                    'pin_ref'                   => $result['pins']['participant']['pin_ref'],
                    'voice_conference_enabled'  => $testData['voice_enabled'],
                )
            ));

            // Status Check
            if (!$updates['status'] || !$updatesPlusUser['status']) {
                $output[$counter] = 'Pin || Contact Updates Failed';
                $this->logSection('FAILED', 'Multiple PIN Option Failed');
                continue;
            }

            // Obtain Multiple PINs
            if ($testData['multiple_pins']) {
                $this->logSection('PINCreation', 'Multiple PIN Option is Enabled');
                // Create another PIN Pair
                $resultTmp = PinHelper::doPlusAddPins(array(
                    'service_ref' => $this->pwn_options['plus_service_ref'],
                    'contact_ref' => $contactRef,
                    'account_id'  => $account_id,
                ));
                if (!empty($resultTmp['error'])) {
                    $this->logSection('FAILED', 'Multiple PIN Creation Failed. Error: ' . $resultTmp['error']);
                    continue;
                } else {
                    $this->logSection('PINCreation', 'Multiple PIN Creation Success');
                    $pin_chairman[]    = $resultTmp['result']['pins']['chairman']['pin_ref'];
                    $pin_participant[] = $resultTmp['result']['pins']['participant']['pin_ref'];
                }
            }

            // Add a Branded Welcome Message
            if ($testData['bwm_uk_geo_enabled'] || $testData['bwm_uk_ff_enabled']) {
                $this->logSection('BWM', 'Branded Welcome Message Option is Enabled');

                // Set-up Some variables
                $dialingNo        = array();
                $groupedDialingNo = array();
                $productAttr      = array();
                $addedProducts    = array();
                $initialParams    = array(
                    'company'        => $this->pwn_options['organisation'],
                    'contact_name'   => $this->pwn_options['first_name'] . ' ' . $this->pwn_options['last_name'],
                    'contact_number' => $this->pwn_options['phone_number'],
                    'script_accept'  => 1,
                    'bwm_label'      => $this->pwn_options['bwm_label']
                );
                $initialParams['welcome_message'] = get_partial('products/bwmScriptWelcomeMessage', array(
                    'company'        => $this->pwn_options['organisation'],
                    'contact_name'   => $this->pwn_options['first_name'] . ' ' . $this->pwn_options['last_name'],
                    'contact_number' => $this->pwn_options['phone_number'],
                ));
                $initialParams['instructions_message'] = get_partial('products/bwmScriptInstructionsMessage', array(
                    'company'        => $this->pwn_options['organisation'],
                    'contact_name'   => $this->pwn_options['first_name'] . ' ' . $this->pwn_options['last_name'],
                    'contact_number' => $this->pwn_options['phone_number'],
                ));
                $initialParams['transcript'] = $initialParams['welcome_message'] . ' ' . $initialParams['instructions_message'];

                // Get Products with Dial In Numbers and Rates
                try {
                    $bwmProductRates = Hermes_Client_Rest::call('BWM.getProductDialInNumbersWithRates', array(
                        'dedicated' => 1,
                        'allocated' => 0,
                        'service_ref' => 850,
                        'product_type' => 'VOICE',
                        'grouped' => 1
                    ));
                    $this->logSection('ProductCreation', 'Hermes Products Count: ' . count($bwmProductRates['products']));
                } catch (Hermes_Client_Request_Timeout_Exception $e) {
                    $this->logSection('EXCEPTION', 'Get Products with Dial In Numbers and Rates Failed');
                    $output[$counter] = 'Get Products with Dial In Numbers and Rates Failed';
                    sfContext::getInstance()->getLogger()->err('BWM.getProductDialInNumbersWithRates Hermes call has timed out.');
                    continue;
                } catch (Hermes_Client_Exception $e) {
                    $this->logSection('EXCEPTION', 'Get Products with Dial In Numbers and Rates Failed');
                    $output[$counter] = 'Get Products with Dial In Numbers and Rates Failed';
                    sfContext::getInstance()->getLogger()->err('BWM.getProductDialInNumbersWithRates Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                    continue;
                } catch (Exception $e) {
                    $this->logSection('EXCEPTION', 'Get Products with Dial In Numbers and Rates Failed');
                    $output[$counter] = 'Get Products with Dial In Numbers and Rates Failed';
                    sfContext::getInstance()->getLogger()->err('BWM.getProductDialInNumbersWithRates Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                    continue;
                }

                foreach ($bwmProductRates['products'] as $prd) {
                    // Geographic || Freefone is Enabled
                    if (
                        ($testData['bwm_uk_geo_enabled'] && $prd['dnis_type'] == 'Geographic' && $prd['grouped'] == 'GBR22531Geographic') ||
                        ($testData['bwm_uk_ff_enabled'] && $prd['dnis_type'] == 'Freefone' && $prd['grouped'] == 'GBRFreefone')
                    ) {
                        $this->logSection('BWM', $prd['dnis_type'] . ' is Enabled.');
                        $dnisType = $this->switchDnisTypesForDisplay($prd['dnis_type']);
                        $dnisData = array(
                            'id'           => $prd['geo_city_id'],
                            'country'      => $prd['country'],
                            'city'         => $prd['city'],
                            'type'         => $dnisType,
                            'rate'         => $prd['rate'],
                            'country_code' => $prd['country_code'],
                        );

                        if (!isset($groupedDialingNo[$dnisType])) {
                            $groupedDialingNo[$dnisType] = array();
                        }

                        $groupedDialingNo[$dnisType][] = $dnisData;
                        $dialingNo[] = $dnisData;
                    }
                }

                $qty = count($dialingNo);
                $this->logSection('PINCreation', 'Quantity: ' . $qty);
                $total = $qty * $dnisPerRate['rate_per_dnis'];

                $dialingNo['BWM_total_cost'] = array(
                    'total' => $total,
                    'qty' => $qty,
                    'connectionFee' => $connectionFee['connection_fee'],
                    'dnisPerRate' => $dnisPerRate['rate_per_dnis']
                );

                // New DNISES
                $switchedDnises = $this->switchDNISTypesToHermesDNISType($dialingNo);
                $switchedDnises['BWM_total_cost'] = $dialingNo['BWM_total_cost'];

                // New Products
                $productAttr[] = $initialParams;
                $productAttr[] = $switchedDnises;

                // Start Basket
                $basket = new Basket($this->user);

                // Add New Product IDs
                $productId = $basket->addNonExistingProduct($productAttr);
                $addedProducts = $basket->retrieveAddedProducts();
                if ('plusUser' == $this->pwn_options['userType']) {
                    $addedProducts[-1]['pins'] = array($contactRefPU);
                } else {
                    $addedProducts[-1]['pins'] = array();
                }
                $basket->storeAddedProducts($addedProducts);

                // Final Apply Actual Changes
                $basketProcessor = new BasketProcess($this->user, 1, false);
                $basketProcessor->setAddedProducts($addedProducts);
                $basketProcessor->clearBasketOnCompletion = true;

                // New Code to handle the BWMCreator and PIN Retriever Classes
                $pinRetriever = new PinRetriever();
                $bwmCreator = new BWMCreator($this->user, $pinRetriever);
                $bundleCreator = new BundleCreator($this->user, $pinRetriever);

                // Try to apply changes.
                try {
                    $updateDetails = $basketProcessor->applyChanges($bwmCreator, $bundleCreator, $pinRetriever);
                    if ($updateDetails['error']) {
                        $this->logSection('FAILED', 'Failed to add Product. Error: ' . serialize($updateDetails['error_messages']));
                    } else {
                        $this->logSection('BWM', 'BWM Successfully Created');
                    }
                } catch (Exception $e) {
                    $this->logSection('FAILED', 'Failed to add Product. Exception was called. Error Message: ' . $e->getMessage());
                }

            }

            // Add a NON Branded Welcome Message
            if ($testData['uk_ff_enabled'] || $testData['uk_geo_enabled'] || $testData['ww_ff_enabled'] || $testData['ww_geo_enabled']) {
                $this->logSection('NON-BWM', 'Non Branded Welcome Message Option is Enabled');

                // Products Array
                $productsArr = array(
                    3 => 'ww_ff_enabled',
                    4 => 'ww_geo_enabled',
                    5 => 'uk_ff_enabled',
                    6 => 'uk_geo_enabled'
                );

                // Initialise the Products Pins -> Contact Ref Arrays
                $productPinsArr = array();

                // New Basket
                $basket = new Basket($this->user);

                // Check each NON-BWM Product which is Enabled, Then add then to the basket
                foreach ($productsArr as $pid => $pname) {
                    if (isset($testData[$pname]) && $testData[$pname] === true) {
                        $this->logSection('NON-BWM', 'Product ' . $pid . ' is Selected');
                        $basket->addProduct($pid);
                        $productPinsArr['p' . $pid][] = $contactRef;
                        if ($this->pwn_options['userType'] == 'plusUser') {
                            $productPinsArr['p' . $pid][] = $contactRefPU;
                        }
                    }
                }

                // Return the Added Products from the Basket
                $selectedItems = $basket->retrieveAddedProducts();

                // Update the Selected Items, by adding the Pins
                foreach ($selectedItems as $selectedItemKey => &$selectedItem) {
                    if (!empty($productPinsArr['p' . $selectedItemKey])) {
                        if (!is_array($selectedItem)) {
                            $selectedItem = array();
                        }
                        $selectedItem['pins'] = $productPinsArr['p' . $selectedItemKey];
                    } else {
                        $selectedItem['pins'] = array();
                    }
                }

                // Disable the used references, just in case.
                unset($selectedItem);

                // Save the altered products in the basket.
                $basket->storeAddedProducts($selectedItems);
                $addedProducts = $basket->retrieveAddedProducts();

                // Final Apply Actual Changes
                $basketProcessor = new BasketProcess($this->user, 1, false);
                $basketProcessor->setAddedProducts($addedProducts);
                $basketProcessor->clearBasketOnCompletion = true;

                // New Code to handle the BWMCreator and PIN Retriever Classes
                $pinRetriever = new PinRetriever();
                $bwmCreator = new BWMCreator($this->user, $pinRetriever);
                $bundleCreator = new BundleCreator($this->user, $pinRetriever);

                // Try to apply changes.
                try {
                    $updateDetails = $basketProcessor->applyChanges($bwmCreator, $bundleCreator, $pinRetriever);
                    if ($updateDetails['error']) {
                        $this->logSection('FAILED', 'Failed to add Product. Error:' . serialize($updateDetails['error_messages']));
                    } else {
                        $this->logSection('NON-BWM', 'NON-BWM Successfully Created');
                    }
                } catch (Exception $e) {
                    $this->logSection('FAILED', 'Failed to add Product. Exception was called. Error Message: ' . $e->getMessage());
                }

            }

            $testData['PINRefs'] = array('chairman' => $pin_chairman, 'participant' => $pin_participant);
            $output[$counter] = $testData;
        }
        return $output;
    }

    /**
     * Create a Premium Admin
     * @return array
     */
    private function createUserPremiumAdmin() {
        $output = array();
        foreach ($this->pwn_options['defaultAccountTestPlan'] as $counter => $testData) {
            $testData['email'] = $this->pwn_options['emailFormatting'] . substr(uniqid(),6) . '@powwownow.com';
            if ('premiumUser' == $this->pwn_options['userType']) {
                $testData['email'] = str_replace('premiumUser','premiumAdmin',$testData['email']);
                $testData['emailPU'] = $this->pwn_options['emailFormatting'] . substr(uniqid(),6) . '@powwownow.com';
            }
            $this->logSection('email', $testData['email']);
            if (isset($testData['emailPU'])) $this->logSection('emailPU', $testData['emailPU']);

            $testData['customerName'] = uniqid('customername_');
            $testData['serviceName'] = uniqid('servicename_');

            // Create Customer
            try {
                $addCustomer = Hermes_Client_Rest::call('Default.addCustomer', array(
                    'customer_name' => $testData['customerName'],
                    'customer_email' => $testData['email'],
                    'customer_telephone' => $this->pwn_options['phone_number'],
                    'country' => $testData['country']
                ));
                $testData['customer_ref'] = $addCustomer['customer']['customer_ref'];
                $this->logSection('CreateCustomer','Create Customer Success');
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->logSection('EXCEPTION','Create Customer Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.addCustomer Hermes call has timed out.');
                continue;
            } catch (Hermes_Client_Exception $e) {
                $this->logSection('EXCEPTION','Create Customer Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.addCustomer Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            } catch (Exception $e) {
                $this->logSection('EXCEPTION','Create Customer Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.addCustomer Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            }

            // Create Service
            try {
                $serviceData = Hermes_Client_Rest::call('Default.createService', array(
                    'service_name' => $testData['serviceName'],
                    'enable_freefone' => $testData['ff_dedicated_enabled'],
                    'enable_geographic' => $testData['geo_dedicated_enabled'],
                    'enable_shared_cost' => $testData['sc_dedicated_enabled'],
                    'enable_freefone_master' => $testData['ff_master_enabled'],
                    'enable_geographic_master' => $testData['geo_master_enabled'],
                    'enable_shared_cost_master' => $testData['sc_master_enabled']
                ));
                $testData['service_ref'] = $serviceData['service']['service_ref'];
                $this->logSection('CreateService','Create Service Success');
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->logSection('EXCEPTION','Create Service Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createService Hermes call has timed out.');
                continue;
            } catch (Hermes_Client_Exception $e) {
                $this->logSection('EXCEPTION','Create Service Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createService Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            } catch (Exception $e) {
                $this->logSection('EXCEPTION','Create Service Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createService Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            }

            // Create Customer and Service Link
            try {
                $linkUpdate = Hermes_Client_Rest::call('Default.linkServiceToCustomer',  array(
                    'customer_ref' => $addCustomer['customer']['customer_ref'],
                    'service_ref'  => $serviceData['service']['service_ref'],
                ));
                $this->logSection('CreateCustomerService','Create CustomerService Success');
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->logSection('EXCEPTION','Create Customer and Service Link Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.linkServiceToCustomer Hermes call has timed out.');
                continue;
            } catch (Hermes_Client_Exception $e) {
                $this->logSection('EXCEPTION','Create Customer and Service Link Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.linkServiceToCustomer Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            } catch (Exception $e) {
                $this->logSection('EXCEPTION','Create Customer and Service Link Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.linkServiceToCustomer Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            }

            // Create Admin Contact
            try {
                $adminContact = Hermes_Client_Rest::call('Default.createContact',array(
                    'email'                  => $testData['email'],
                    'first_name'             => $this->pwn_options['first_name'],
                    'last_name'              => $this->pwn_options['last_name'],
                    'password'               => $this->pwn_options['password'],
                    'source'                 => $this->pwn_options['source'],
                    'locale'                 => $this->pwn_options['locale'],
                    'title'                  => $this->pwn_options['title'],
                    'phone'                  => $this->pwn_options['phone_number'],
                    'mobile_phone'           => $this->pwn_options['phone_number'],
                    'organisation'           => $this->pwn_options['organisation'],
                    'location'               => $this->pwn_options['location'],
                    'fax_number'             => $this->pwn_options['phone_number'],
                    'country_code'           => $testData['country'],
                ));
                $updateContact = $this->updateContactAndPin(array(
                    'contact_ref' => $adminContact['contact']['contact_ref'],
                    'hasYuuguu' => $testData['yuuguu_enabled']
                ),array(),array());
                $this->logSection('CreateAdminContact','Create AdminContact Success');
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->logSection('EXCEPTION','Create Admin Contact Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createContact Hermes call has timed out.');
                continue;
            } catch (Hermes_Client_Exception $e) {
                $this->logSection('EXCEPTION','Create Admin Contact Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createContact Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            } catch (Exception $e) {
                $this->logSection('EXCEPTION','Create Admin Contact Failed: ' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createContact Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            }

            // Create Contact Links
            try {
                $adminContactLink   = Hermes_Client_Rest::call('Default.createContactLink',array(
                    'contact_ref'           => $adminContact['contact']['contact_ref'],
                    'table_to_link'         => 'customer',
                    'table_to_link_ref'     => $addCustomer['customer']['customer_ref'],
                    'contact_type'          => 'Admin',
                ));
                $billingContactLink = Hermes_Client_Rest::call('Default.createContactLink',array(
                    'contact_ref'           => $adminContact['contact']['contact_ref'],
                    'table_to_link'         => 'customer',
                    'table_to_link_ref'     => $addCustomer['customer']['customer_ref'],
                    'contact_type'          => 'billing',
                ));
                $this->logSection('CreateContactLinks','Create ContactLinks Success');
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->logSection('EXCEPTION','Create Contact Links Failed:' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createContactLink Hermes call has timed out.');
                continue;
            } catch (Hermes_Client_Exception $e) {
                $this->logSection('EXCEPTION','Create Contact Links Failed:' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createContactLink Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            } catch (Exception $e) {
                $this->logSection('EXCEPTION','Create Contact Links Failed:' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createContactLink Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            }

            // Create Address
            try {
                $address = Hermes_Client_Rest::call('Default.createAddress', array(
                    'ref'          =>      $addCustomer['customer']['customer_ref'],
                    'table'        =>      'customer',
                    'address_type' =>      'Company',
                    'organisation' =>      $this->pwn_options['organisation'],
                    'street'       =>      'Some Street',
                    'town'         =>      'Some Town',
                    'major_town'   =>      '',
                    'county'       =>      'Some County',
                    'postal_code'  =>      'aa11aa',
                    'country'      =>      'UK',
                    'state_ref'    =>      '0',
                    'description'  =>      'Description',
                ));
                $this->logSection('CreateAddress','Create Address Success');
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                $this->logSection('EXCEPTION','Create Address Failed:' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createAddress Hermes call has timed out.');
                continue;
            } catch (Hermes_Client_Exception $e) {
                $this->logSection('EXCEPTION','Create Address Failed:' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createAddress Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            } catch (Exception $e) {
                $this->logSection('EXCEPTION','Create Address Failed:' . $e->getMessage());
                sfContext::getInstance()->getLogger()->err('Default.createAddress Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                continue;
            }

            // Create Premium PIN Pair || Multiple PIN Pairs
            $multiplePinCounter =($testData['multiple_pins']) ? 2 : 1;
            for ($x = 1; $x <= $multiplePinCounter; $x++) {
                try {
                    $pins = Hermes_Client_Rest::call('Default.createPremiumPinPair',array(
                        'locale'              => $this->pwn_options['locale'],
                        'service_ref'         => $serviceData['service']['service_ref'],
                        'customer_ref'        => $addCustomer['customer']['customer_ref'],
                        'contact_ref'         => $adminContact['contact']['contact_ref'],
                        'username'            => $this->pwn_options['first_name'] . ' ' . $this->pwn_options['last_name'],
                        'master_service_ref'  => 900,
                    ));
                    $this->logSection('CreatePINPair','Create Premium PIN Pair Success');
                } catch (Hermes_Client_Request_Timeout_Exception $e) {
                    $this->logSection('EXCEPTION','Create PIN Pair Failed: ' . $e->getMessage());
                    sfContext::getInstance()->getLogger()->err('Default.createPremiumPinPair Hermes call has timed out.');
                    continue;
                } catch (Hermes_Client_Exception $e) {
                    $this->logSection('EXCEPTION','Create PIN Pair Failed: ' . $e->getMessage());
                    sfContext::getInstance()->getLogger()->err('Default.createPremiumPinPair Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                    continue;
                } catch (Exception $e) {
                    $this->logSection('EXCEPTION','Create PIN Pair Failed: ' . $e->getMessage());
                    sfContext::getInstance()->getLogger()->err('Default.createPremiumPinPair Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                    continue;
                }

                // Create Contact to Pin Links
                try {
                    $contactPinLink = Hermes_Client_Rest::call('Default.createContactToPinLink', array(
                        'contact_ref'   => $adminContact['contact']['contact_ref'],
                        'pin_ref'       => $pins['chairman_pin']['pin_ref'],
                        'contact_type'  => 'Chairman'
                    ));
                    $contactPinLinkD = Hermes_Client_Rest::call('Default.createContactToPinLink', array(
                        'contact_ref'   => $adminContact['contact']['contact_ref'],
                        'pin_ref'       => $pins['participant_pin']['pin_ref'],
                        'contact_type'  => 'Delegate'
                    ));
                    $this->logSection('CreateContactPINLinks','Create Contact PIN Links Success');
                } catch (Hermes_Client_Request_Timeout_Exception $e) {
                    $this->logSection('EXCEPTION','Create Contact to PIN Links Failed: ' . $e->getMessage());
                    sfContext::getInstance()->getLogger()->err('Default.createContactToPinLink Hermes call has timed out.');
                    continue;
                } catch (Hermes_Client_Exception $e) {
                    $this->logSection('EXCEPTION','Create Contact to PIN Links Failed: ' . $e->getMessage());
                    sfContext::getInstance()->getLogger()->err('Default.createContactToPinLink Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                    continue;
                } catch (Exception $e) {
                    $this->logSection('EXCEPTION','Create Contact to PIN Links Failed: ' . $e->getMessage());
                    sfContext::getInstance()->getLogger()->err('Default.createContactToPinLink Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
                    continue;
                }

                $testData['PINRefs'][] = array(
                    'chairman'    => $pins['chairman_pin']['pin_ref'],
                    'participant' => $pins['participant_pin']['pin_ref']
                );

                // Update Premium Admin Contact / PINs
                $updatesPremiumAdmin = $this->updateContactAndPin(array(
                    'contact_ref'    => $adminContact['contact']['contact_ref'],
                    'country_code'   => $testData['country'],
                    'hasYuuguu'      => $testData['yuuguu_enabled'],
                ),array(),array(
                    array(
                        'pin_ref'                   => $pins['chairman_pin']['pin_ref'],
                        'voice_conference_enabled'  => $testData['voice_enabled'],
                    ),
                    array(
                        'pin_ref'                   => $pins['participant_pin']['pin_ref'],
                        'voice_conference_enabled'  => $testData['voice_enabled'],
                    )
                ));

                if (!$updatesPremiumAdmin) {
                   $this->logSection('UPDATE-PREMIUM-ADMIN', 'Failed');
                   continue;
                }
            }

            // Create Premium User
            if ($this->pwn_options['userType'] == 'premiumUser') {
                $this->logSection('CREATE-PREMIUM-USER', 'Create Premium User Selection is Enabled');

                // Add Contact and PINs to Premium Account
                $resultCreateUser = PinHelper::doPremiumAddContactAndPins(array(
                    'customer_ref'     => $testData['customer_ref'],
                    'service_ref'      => $testData['service_ref'],
                    'first_name'       => $this->pwn_options['first_name'],
                    'last_name'        => $this->pwn_options['last_name'],
                    'email'            => $testData['emailPU'],
                    'phone'            => $this->pwn_options['phone_number'],
                    'password'         => $this->pwn_options['password'],
                    'locale'           => $this->pwn_options['locale'],
                    'source'           => $this->pwn_options['source'],
                ));

                if (empty($resultCreateUser) || !empty($resultCreateUser['error'])) {
                    $output[$counter] = 'Create User Failed';
                    $this->logSection('FAILED', 'User Creation Failed. Error: ' . (isset($resultCreateUser['error'])) ? $resultCreateUser['error'] : 0);
                    continue;
                } else {
                    $this->logSection('CREATE-PREMIUM-USER', 'Successfully Created');
                    $testData['contact_ref_premiumUser'] = $resultCreateUser['result']['contact']['contact_ref'];

                    // Update Premium User Contact / PINs
                    $updatesPremiumUser = $this->updateContactAndPin(array(
                        'contact_ref'    => $resultCreateUser['result']['contact']['contact_ref'],
                        'country_code'   => $testData['country'],
                        'hasYuuguu'      => $testData['yuuguu_enabled'],
                    ),array(),array(
                        array(
                            'pin_ref'                   => $resultCreateUser['result']['pins']['chairman']['pin_ref'],
                            'voice_conference_enabled'  => $testData['voice_enabled'],
                        ),
                        array(
                            'pin_ref'                   => $resultCreateUser['result']['pins']['participant']['pin_ref'],
                            'voice_conference_enabled'  => $testData['voice_enabled'],
                        )
                    ));

                    if ($updatesPremiumUser) {
                        $testData['UserPINRefs'][] = array(
                            'chairman'    => $resultCreateUser['result']['pins']['chairman']['pin_ref'],
                            'participant' => $resultCreateUser['result']['pins']['participant']['pin_ref']
                        );
                    } else {
                        $this->logSection('FAILED', 'Failed');
                        continue;
                    }
                }

                if ($testData['multiple_pins']) {
                    $resultCreateUserMultiplePins = PinHelper::doPremiumAddPins(array(
                        'contact_ref'  => $resultCreateUser['result']['contact']['contact_ref'],
                        'customer_ref' => $testData['customer_ref'],
                        'service_ref'  => $testData['service_ref'],
                    ));

                    if (!empty($resultCreateUserMultiplePins['error'])) {
                        $this->logSection('FAILED', 'Premium Used Multiple PINS Failed');
                        continue;
                    } else {
                        $testData['UserPINRefs'][] = array(
                            'chairman'    => $resultCreateUserMultiplePins['result']['pins']['chairman']['pin_ref'],
                            'participant' => $resultCreateUserMultiplePins['result']['pins']['participant']['pin_ref']
                        );
                    }
                }
            }

            $testData['contact_ref'] = $adminContact['contact']['contact_ref'];
            $output[$counter] = $testData;
        }

        return $output;
    }

    /**
     * Create a Premium User
     * @return array
     */
    private function createUserPremiumUser() {
        return $this->createUserPremiumAdmin();
    }

    /**
     * Takes the Initial Arguments and Returns All Arguments which are NOT null and NOT the task name
     * In addition it adds a few more arguments used in the tests
     * @author Asfer Tamimi
     */
    private function reBuildArguments() {
        unset($this->pwn_options['help']);
        unset($this->pwn_options['quiet']);
        unset($this->pwn_options['trace']);
        unset($this->pwn_options['version']);
        unset($this->pwn_options['color']);
        foreach ($this->pwn_options as $k => $v) {
            if (is_null($v)) {
                unset($this->pwn_options[$k]);
            }
        }
        $this->pwn_options['emailFormatting']  = 'pwn_test+' . $this->pwn_options['userType'] . '_' . date('ymd') . '_';
//        $this->pwn_options['emailFormatting']  = 'asfer.tamimi+pwn_test_' . $this->pwn_options['userType'] . '_' . date('ymd') . '_';

        // Test Check
        if (count($this->pwn_options) == 16) {
            $this->pwn_options['defaultAccountTestPlan'] = $this->getTestPlan($this->pwn_options['userType']);
        } else {
            $this->pwn_options['defaultAccountTestPlan'] = $this->getUserTests($this->pwn_options);
        }
    }

    /**
     * Takes the User Type and Returns the Default Test Plan for that User Type.
     * @param string $userType
     * @return array
     * @author Asfer Tamimi
     */
    private function getTestPlan($userType) {
        $testPlan = array(
            'enhanced' => array(
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'No'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'Yes'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'No'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes'),
            ),
            'plusAdmin' => array(
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'No', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'No', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => false),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => false),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => false),
            ),
            'plusUser' => array(
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'No', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'No', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => true, 'bwm_uk_ff_enabled' => true, 'uk_ff_enabled' => true, 'uk_geo_enabled' => true, 'ww_ff_enabled' => true, 'ww_geo_enabled' => true, 'createUser' => true),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => true, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => true, 'ww_ff_enabled' => false, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => true, 'ww_geo_enabled' => false, 'createUser' => true),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'bwm_uk_geo_enabled' => false, 'bwm_uk_ff_enabled' => false, 'uk_ff_enabled' => false, 'uk_geo_enabled' => false, 'ww_ff_enabled' => false, 'ww_geo_enabled' => true, 'createUser' => true),
            ),
            'premiumAdmin' => array(
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'No', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'No', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'No', 'multiple_pins' => false, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'NO'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'YES'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
            ),
            'premiumUser' => array(
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'No', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'No', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => false, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'GBR', 'locale' => 'en_gb',  'yuuguu_enabled' => 'N', 'voice_enabled' => 'No', 'multiple_pins' => false, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'YES'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'YES', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'YES', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'YES', 'geo_master_enabled' => 'NO'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'YES', 'sc_master_enabled' => 'NO', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'YES'),
                array('country' => 'FRA', 'locale' => 'en_fr',  'yuuguu_enabled' => 'Y', 'voice_enabled' => 'Yes', 'multiple_pins' => true, 'sc_dedicated_enabled' => 'NO', 'ff_dedicated_enabled' => 'NO', 'geo_dedicated_enabled' => 'NO', 'sc_master_enabled' => 'YES', 'ff_master_enabled' => 'NO', 'geo_master_enabled' => 'NO'),
            ),
        );

        if (isset($testPlan[$userType])) {
            return $testPlan[$userType];
        } else {
            return array();
        }
    }

    /**
     * Returns the User Defined Tests.
     * @param string $arguments
     * @return array
     * @author Asfer Tamimi
     */
    private function getUserTests($arguments) {
        switch ($arguments['userType']) {
            case 'enhanced':
                return array(array(
                    'country'        => (isset($arguments['country']) ? $arguments['country'] : 'GBR'),
                    'yuuguu_enabled' => (isset($arguments['yuuguu_enabled']) ? $arguments['yuuguu_enabled'] : true),
                    'voice_enabled'  => (isset($arguments['voice_enabled']) ? $arguments['voice_enabled'] : true),
                ));
            case 'plusUser':
                return array(array(
                    'country'            => (isset($arguments['country']) ? $arguments['country'] : 'GBR'),
                    'yuuguu_enabled'     => (isset($arguments['yuuguu_enabled']) ? $arguments['yuuguu_enabled'] : true),
                    'voice_enabled'      => (isset($arguments['voice_enabled']) ? $arguments['voice_enabled'] : true),
                    'multiple_pins'      => (isset($arguments['multiple_pins']) ? $arguments['multiple_pins'] : true),
                    'bwm_uk_geo_enabled' => (isset($arguments['bwm_uk_geo_enabled']) ? $arguments['bwm_uk_geo_enabled'] : true),
                    'bwm_uk_ff_enabled'  => (isset($arguments['bwm_uk_ff_enabled']) ? $arguments['bwm_uk_ff_enabled'] : true),
                    'uk_ff_enabled'      => (isset($arguments['uk_ff_enabled']) ? $arguments['uk_ff_enabled'] : true),
                    'uk_geo_enabled'     => (isset($arguments['uk_geo_enabled']) ? $arguments['uk_geo_enabled'] : true),
                    'ww_ff_enabled'      => (isset($arguments['ww_ff_enabled']) ? $arguments['ww_ff_enabled'] : true),
                    'ww_geo_enabled'     => (isset($arguments['ww_geo_enabled']) ? $arguments['ww_geo_enabled'] : true),
                    'createUser'         => (isset($arguments['createUser']) ? $arguments['createUser'] : true),
                ));
            case 'plusAdmin':
                return array(array(
                    'country'            => (isset($arguments['country']) ? $arguments['country'] : 'GBR'),
                    'yuuguu_enabled'     => (isset($arguments['yuuguu_enabled']) ? $arguments['yuuguu_enabled'] : true),
                    'voice_enabled'      => (isset($arguments['voice_enabled']) ? $arguments['voice_enabled'] : true),
                    'multiple_pins'      => (isset($arguments['multiple_pins']) ? $arguments['multiple_pins'] : true),
                    'bwm_uk_geo_enabled' => (isset($arguments['bwm_uk_geo_enabled']) ? $arguments['bwm_uk_geo_enabled'] : true),
                    'bwm_uk_ff_enabled'  => (isset($arguments['bwm_uk_ff_enabled']) ? $arguments['bwm_uk_ff_enabled'] : true),
                    'uk_ff_enabled'      => (isset($arguments['uk_ff_enabled']) ? $arguments['uk_ff_enabled'] : true),
                    'uk_geo_enabled'     => (isset($arguments['uk_geo_enabled']) ? $arguments['uk_geo_enabled'] : true),
                    'ww_ff_enabled'      => (isset($arguments['ww_ff_enabled']) ? $arguments['ww_ff_enabled'] : true),
                    'ww_geo_enabled'     => (isset($arguments['ww_geo_enabled']) ? $arguments['ww_geo_enabled'] : true),
                    'createUser'         => (isset($arguments['createUser']) ? $arguments['createUser'] : false),
                ));
            case 'premiumUser':
                return array(array(
                    'country'               => (isset($arguments['country']) ? $arguments['country'] : 'GBR'),
                    'yuuguu_enabled'        => (isset($arguments['yuuguu_enabled']) ? $arguments['yuuguu_enabled'] : true),
                    'voice_enabled'         => (isset($arguments['voice_enabled']) ? $arguments['voice_enabled'] : true),
                    'multiple_pins'         => (isset($arguments['multiple_pins']) ? $arguments['multiple_pins'] : true),
                    'sc_dedicated_enabled'  => (isset($arguments['sc_dedicated_enabled']) ? $arguments['sc_dedicated_enabled'] : true),
                    'ff_dedicated_enabled'  => (isset($arguments['ff_dedicated_enabled']) ? $arguments['ff_dedicated_enabled'] : true),
                    'geo_dedicated_enabled' => (isset($arguments['geo_dedicated_enabled']) ? $arguments['geo_dedicated_enabled'] : true),
                    'sc_master_enabled'     => (isset($arguments['sc_master_enabled']) ? $arguments['sc_master_enabled'] : true),
                    'ff_master_enabled'     => (isset($arguments['ff_master_enabled']) ? $arguments['ff_master_enabled'] : true),
                    'geo_master_enabled'    => (isset($arguments['geo_master_enabled']) ? $arguments['geo_master_enabled'] : true),
                ));
            case 'premiumAdmin':
                return array(array());
            default:
                return array(array());
        }
    }

    /**
     * Update Contact and PIN Tables
     * @param array $contact
     * @param array $pin
     * @param array $pinPair
     * @return array
     */
    private function updateContactAndPin($contact,$pin,$pinPair) {
        // Status
        $status = true;

        // Output
        $output = array();

        // Contact Update
        if (!empty($contact)) {
            $result1 = Common::updateContact($contact);
            $status = (isset($result1['statusCode']) && $result1['statusCode'] != 202) ? false : $status;
            $output['result1'] = $result1;
        }

        // Pin Update
        if (!empty($pin)) {
            $result2 = PinHelper::updatePin($pin,$contact['contact_ref']);
            $status = ($result2['statusCode'] != 202) ? false : $status;
            $output['result2'] = $result2;
        } elseif (!empty($pinPair)) {
            $result2 = PinHelper::updatePin($pinPair[0], $contact['contact_ref']);
            $result3 = PinHelper::updatePin($pinPair[1], $contact['contact_ref']);
            $status = ($result2['statusCode'] != 202) ? false : $status;
            $status = ($result2['statusCode'] != 202) ? false : $status;
            $output['result2'] = $result2;
            $output['result3'] = $result3;
        }

        return array(
            'status' => $status,
            'output' => $output
        );
    }

    /**
     * Disable All Accounts which have been created.
     * @return array $result | bool false
     */
    private function disableTestAccounts() {
        try {
            $result = Hermes_Client_Rest::call('UserManagement.disableAllAccountsByEmailFormatting', array(
                'emailFormatting' => 'pwn_test+'
            ));
            $this->logSection('DisabledOutput', print_r($result,true));
            return $result;
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('Default.disableAllAccountsByEmailFormatting Hermes call has timed out.' . $e->getMessage());
            return false;
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('Default.disableAllAccountsByEmailFormatting Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
            return false;
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Default.disableAllAccountsByEmailFormatting Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
            return false;
        }
    }

    /**
     * Return the Rates and Connection Fees
     */
    private function getDNISReferences() {
        $connectionFee = 1;
        $dnisPerRate = 1;

        try {
            $connectionFee = Hermes_Client_Rest::call('BWM.getAccountConnectionFee',array());
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->logSection('PINCreation', 'Connection Fee Failed');
            sfContext::getInstance()->getLogger()->err('BWM.getAccountConnectionFee Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            $this->logSection('PINCreation', 'Connection Fee Failed');
            sfContext::getInstance()->getLogger()->err('BWM.getAccountConnectionFee Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            $this->logSection('PINCreation', 'Connection Fee Failed');
            sfContext::getInstance()->getLogger()->err('BWM.getAccountConnectionFee Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }

        try {
            $dnisPerRate = Hermes_Client_Rest::call('BWM.getRatePerDnis',array());
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->logSection('PINCreation', 'DNIS Rates Failed');
            sfContext::getInstance()->getLogger()->err('BWM.getRatePerDnis Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            $this->logSection('PINCreation', 'DNIS Rates Failed');
            sfContext::getInstance()->getLogger()->err('BWM.getRatePerDnis Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            $this->logSection('PINCreation', 'DNIS Rates Failed');
            sfContext::getInstance()->getLogger()->err('BWM.getRatePerDnis Exception Occurred: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }

        return (array('connectionFee' => $connectionFee, 'dnisPerRate' => $dnisPerRate));
    }

    /**
     * Iterates through selected DNISes and switches displayable types to Hermes understandable DNIS type.
     *
     * Warning: if no DNIS type is found, the dnis is not passed to the result array.
     *
     * @param array $dnises
     * @return array
     */
    private function switchDNISTypesToHermesDNISType(array $dnises) {
        $result = array();

        foreach ($dnises as $dnis) {
            if (!empty($dnis['type'])) {
                $dnisType = $this->switchDNISTypeToHermesDNISType($dnis['type']);
                if ($dnisType) {
                    $dnis['type'] = $dnisType;
                    $result[] = $dnis;
                }
            }
        }

        return $result;
    }

    /**
     * Switches a displayable DNIS type to a Hermes understandable DNIS type.
     * If the type is unknown, returns false.
     *
     * @param string $dnisType
     * @return bool|string
     */
    private function switchDNISTypeToHermesDNISType($dnisType) {
        if ($dnisType === 'Landline') {
            return 'Geographic';
        } else if ($dnisType === 'Freephone') {
            return 'Freefone';
        } else if ($dnisType === 'Shared Cost') {
            return $dnisType;
        }
        return false;
    }

    /**
     * Changes the DNIS types to a displayable string.
     * i.e. 'Freefone' => 'Freephone'
     *
     * @param string $dnisType
     * @return string
     */
    private function switchDnisTypesForDisplay($dnisType) {
        if ($dnisType === 'Geographic') {
            $dnisType = 'Landline';
        } else if ($dnisType === 'Freefone') {
            $dnisType = 'Freephone';
        }
        return $dnisType;
    }
}
