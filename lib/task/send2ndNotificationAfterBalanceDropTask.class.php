<?php

class send2ndNotificationAfterBalanceDropTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

      $appOptions = array(
          new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name','pwn'),
          new sfCommandOption('env', null, sfCommandOption::PARAMETER_OPTIONAL, 'The environment', 'dev'),
          new sfCommandOption('connection', null, sfCommandOption::PARAMETER_OPTIONAL, 'The connection name', 'doctrine'),
          new sfCommandOption('pretend', null, sfCommandOption::PARAMETER_OPTIONAL, 'Pretend to send emails', 'yes'),

          new sfCommandOption('day_in_the_past', null, sfCommandOption::PARAMETER_OPTIONAL, 'Pretend to send emails', 3),
          new sfCommandOption('min_balance', null, sfCommandOption::PARAMETER_OPTIONAL, 'Pretend to send emails', 5),
          new sfCommandOption('after_drop', null, sfCommandOption::PARAMETER_OPTIONAL, 'Pretend to send emails', 5),

      );
      $this->addOptions(array_merge($appOptions, array()));

    $this->namespace        = 'pluspayg';
    $this->name             = 'sendBalanceDropEmail';
    $this->briefDescription = 'Send notification to all plus payg admins which account credit balance dropped below 5 pounds 3 days ago.';
    $this->detailedDescription = <<<EOF
The [send2ndNotificationAfterBalanceDrop|INFO] task does things.
Call it with:

  [php symfony send2ndNotificationAfterBalanceDrop|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
      $hermesUrl = sfConfig::get('app_hermesenvurl', '');
      if (empty($hermesUrl))
          throw new Exception('Hermes Url is missing');

      Hermes_Client_Rest::init($hermesUrl);

      $result = Hermes_Client_Rest::call(
          'PlusPAYG.do2ndNotificationAboutFallenBalance',
          array(
              'pretend' => $options['pretend'],
              'day_in_the_past' => $options['day_in_the_past'],
              'min_balance' => $options['min_balance'],
              'after_drop' => $options['after_drop']
          )
      );


      print_r($result['emails']);

  }
}
