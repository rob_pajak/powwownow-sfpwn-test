<?php

/*
 * CLASS: AutotopupList
*
* Gets all accounts requiring topup and instantiates a topup object for each.
* THen iterates through the array $this->retryCount number of times. Successful
* topups are logged and removed from the array. Failed topup remain in array for subsequent attempts.
* During the final iteration failed topups are notified to customer services and account admin by email *
*/

class AutoTopup_CronTask extends sfBaseTask
{

    private $list=array();
    private $topupRetryCount=2;
    private $defaultThreshold='5.00';
    private $successLogFile='log/AutoTopupSuccess.log';
    private $successLog;
    private $failureLogFile='log/AutoTopupFail.log';
    private $failureLog;
    private $appLogFile='log/AutoTopup.log';
    private $appLog;
    private $failureNotifyFrom='no-reply@powwownow.com';
    private $failureNotifySubject = 'PWN plus_failed transaction';
    private $failureNotifyCSTo = 'rob.macgregor@powwownow.com';
    private $failureNotifyFinanceTo = 'rob.macgregor@powwownow.com';


    protected function configure()
    {
        $this->addOptions(array(
                new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name','pwn'),
                new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
                new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
        ));

        $this->namespace        = 'pwn';
        $this->name             = 'AutoTopup_Cron';
        $this->briefDescription = '';
        $this->detailedDescription = '';
        
        if(isset($_SERVER['ENV']) && strtoupper($_SERVER['ENV']) == "PRODUCTION"  ){
            
            $this->failureNotifyCSTo='customer.services@powwownow.com';
            
            $this->failureNotifyFinanceTo='carrier.accounting@via-vox.net';
            
        }
    }

    protected function execute($arguments = array(), $options = array())
    {

        $this->failureLog= new sfFileLogger(new sfEventDispatcher(), array('file'=> $this->failureLogFile));

        $this->failureLog->setLogLevel('Info');

        $this->successLog= new sfFileLogger(new sfEventDispatcher(), array('file'=> $this->successLogFile));

        $this->successLog->setLogLevel('Info');

        $this->appLog= new sfFileLogger(new sfEventDispatcher(), array('file'=> $this->appLogFile));

        $this->appLog->setLogLevel('Info');

        try{

            $this->list = Hermes_Client_Rest::call('Plus.getTopupDueList', array( 'threshold' => $this->defaultThreshold ));

            $this->appLog->info('account found below threshold: '. count($this->list));

        }catch(Exception $e){

            $this->appLog->info('hermes call to Plus.getTopupDueList failed on line '. __line__. "\n\n" . $e->getMessage() );

        }

        foreach($this->list as $k=>$v){

            $this->list[$k]= new AutoTopup($v);

        }
        	
        for($i=1;$i <= $this->topupRetryCount;$i++){

            if($this->_getListCount()){
                	
                $this->_executeTopups($i);
                	
            }

        }
        	
    }


    /*
     *  PRIVATE: _executeTopups
    *
    * 	iterates through the array of Autotops calls execute method for each and logs the result.
    *  removes from $this->list array if successful. executes notification code on final iteration if failure
    *
    *
    */

    private function _executeTopups($i){

        foreach($this->list as $k => $topup){

            try{

                $attemptTopup=$topup->executeTopup();

            }catch(Exception $e){

                $this->appLog->info('Top up exception occured  on line '. __line__. 'for account id ' . $topup->getAccountId().': '. "\n\n" . $e->getMessage()."\n\n");

                continue;
            }

            //$attemptTopup=false;

            if( $attemptTopup === TRUE ){

                $message="AutoTopup for accountID: ".$topup->getAccountId()." succeeded. \r\n";
                	
                $this->successLog->info($message);
                	
                unset($this->list[$k]);

            }else{

                if($i == $this->topupRetryCount){

                    $message="AutoTopup for accountID: ".$topup->getAccountId()." failed ".$this->topupRetryCount ." time".($this->topupRetryCount > 1 ?"s":"")."\r\n";

                    $this->failureLog->info($message);

                    try{
                        $this->_notify('CS',$topup);

                        $this->_notify('FINANCE',$topup);
                    }
                    catch(Exception $e){

                        $this->appLog->info('Failed to send notification on line '. __line__.': '."\n\n".$e->getMessage() ."\n\n");

                    }

                    try{
                        $this->_notify('ACC_ADMIN',$topup);
                    }
                    catch(Exception $e){

                        $this->appLog->info('Failed to notify ACC_ADMIN on line '. __line__.': '."\n\n".$e->getMessage() ."\n\n");

                    }

                }

            }

        }


    }

    /*
     * PRIVATE: _notify
    *
    * @param str recipient
    * @param obj Topup
    *
    * sends failure notification email to $recipient
    *
    * Uses PHP mail to send to CS and Hermes/Dotmailer to send to plus admin
    *
    * @author Bob (11/06/12)
    *
    */


    private function _notify($recipient, $subject){

        $message= "<p>The following autotopup attempt has failed twice. No further attempt to top up the credit on this account will take place today.</p>
        <ul>
        <li>Account ID: ".$subject->getAccountId()."</li>
        <li>Account Name: ".$subject->getOrganisation()."</li>
        <li>Topup Amount: ".$subject->getAmount()."</li>
        </ul>
        <p>Status: ".$subject->getTopupStatus()."</p>";

        $headers  = 'MIME-Version: 1.0' . "\r\n".
                'Content-type: text/html; charset=iso-8859-1' . "\r\n".
                'From: '.$this->failureNotifyFrom."\r\n";
        	
        	
        switch( $recipient ){

            case 'CS':
                	
                $to=$this->failureNotifyCSTo;

            case 'FINANCE':
                	
                $to=$this->failureNotifyFinanceTo;

                mail(	$to,
                        $this->failureNotifySubject,
                        $message,
                        $headers
                );

                $this->appLog->info('failure notification send to '.$recipient . '...');


                break;

            case 'ACC_ADMIN':

                try{


                    //Hermes_Client_Rest::call('sendAutoTopupFailureNotification',array('acount_id' => $subject->getAccountId()));

                    echo  $this->appLog->info('failure notification send to account admin...');


                }catch(Exception $e){

                    throw $e;
                }

                break;

            default:

                throw new Exception('failure notification not sent as recipient not recognised');

                break;

        }


    }


    /*
     *  getters & setters
    *
    */


    private function _getListCount(){

        return count($this->list);

    }

    public function getSuccessList(){

        return $this->successList;

    }


}
