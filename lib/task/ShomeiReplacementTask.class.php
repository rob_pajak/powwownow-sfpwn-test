<?php

/**
 * Class ShomeiReplacementTask
 *
 * To Run this task:
 *
 * symfony ShomeiReplacement --input="/home/asfer/SprintWork/DEV-4147_PinRefResultSample.csv"
 */
class ShomeiReplacementTask extends sfBaseTask
{
    /**
     * @var sfUser $user
     */
    protected $user;

    /**
     * @var array $taskOptions
     */
    private $taskOptions;

    /**
     * @var string $logPath
     */
    private $logPath;

    /**
     * @var sfLogger
     */
    private $shomeiScriptLogger;

    /**
     * @var Shomei
     */
    private $shomeiObj;

    /**
     * Task Configuration
     */
    protected function configure()
    {
        $this->namespace           = '';
        $this->name                = 'ShomeiReplacement';
        $this->briefDescription    = '';
        $this->detailedDescription = '';

        // Add Task Options
        $this->addOptions(
            array(
                new sfCommandOption('input', null, sfCommandOption::PARAMETER_REQUIRED, 'Input CSV File Path'),
            )
        );
    }

    /**
     * @param array $arguments
     * @param array $options
     * @return bool false
     */
    protected function execute($arguments = array(), $options = array())
    {
        sfContext::createInstance(
            sfApplicationConfiguration::getApplicationConfiguration('pwn', 'prod', true)
        );

        $this->user               = sfContext::getInstance()->getUser();
        $this->taskOptions        = array_merge($options, $arguments);
        $this->logPath            = sfConfig::get('sf_log_dir') . '/shomeiScript.log';
        $this->shomeiScriptLogger = new sfFileLogger(new sfEventDispatcher(), array('file' => $this->logPath));
        $this->shomeiScriptLogger->setLogLevel(7);

        Hermes_Client_Rest::init('hermes.local/rest.php');
        $this->shomeiScriptLogger->info('Hermes URL: hermes.local/rest.php has been set');

        $this->shomeiObj          = new Shomei();
        $this->processData();
        return false;
    }

    /**
     * Process the Data by first Inputting, Processing and then Outputting the Data
     * @throws Exception
     */
    private function processData()
    {
        // Open the File and Output the File Contents in a string
        $this->shomeiScriptLogger->info('Script Start Time: ' . date('Y-m-d H:i:s'));
        $handle  = fopen($this->taskOptions['input'], "r");
        $counter = 1;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $this->shomeiScriptLogger->info('Counter: ' . $counter . ', Input Line Loaded: ' . trim($line));
                $lineExploded     = explode(',', $line);
                $pinRef           = trim($lineExploded[1]);
                $creationDateTime = trim($lineExploded[2]);
                $countryCode      = trim($lineExploded[4]);
                $shomeiResult     = $this->shomeiObj->registerFreePin(
                    $pinRef,
                    $pinRef,
                    $creationDateTime,
                    $countryCode
                );
                $shomeiResultLog  = array(
                    'api_errors'            => $shomeiResult['apiResult']['responsiveError'],
                    'hermes_status_code'    => $shomeiResult['hermesResult']['statusCode'],
                    'hermes_status_message' => $shomeiResult['hermesResult']['statusMessage']
                );
                $this->shomeiScriptLogger->info('Shomei Registration Result: ' . print_r($shomeiResultLog, true));
                $counter++;
            }

            $output = 'All Good - Check Logs';
        } else {
            $this->shomeiScriptLogger->err('Input File Failed to be read');
            $output = 'Input File Failed to be read';
        }
        fclose($handle);

        $this->shomeiScriptLogger->info('Script End Time: ' . date('Y-m-d H:i:s'));
        echo $output . "\n";
    }
}
