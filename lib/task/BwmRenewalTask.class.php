<?php

require_once sfConfig::get('sf_lib_dir') . '/php/BwmRenewal/CommandOutputInterface.php';

/**
 * Handles the BWM renewal process:
 *  - Checks if there are BWM customers that need to be charged in exactly 7 days,
 *  - Sends appropriate emails to customers with and without auto-topup,
 *  - Tries to charge customers on the renewal date.
 *
 * Usage:
 *  symfony pwn:bwm-renewal
 *
 * @author Maarten
 */
class BwmRenewalTask extends sfBaseTask implements CommandOutputInterface
{
    /**
     * @var string
     */
    protected $name = 'bwm-renewal';

    /**
     * @var string
     */
    protected $namespace = 'pwn';

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    protected function execute($arguments = array(), $options = array())
    {
        // Prepare the hermes client for the
        $hermesUrl = sfConfig::get('app_hermesenvurl', '');
        if (empty($hermesUrl)) {
            throw new Exception('Hermes Url is missing');
        }
        Hermes_Client_Rest::init($hermesUrl);

        $emailSender = new HermesBwmRenewalEmailSender();
        $invoiceHandler = new HermesBwmRenewalInvoiceHandler();
        $bwmRenewalStore = new HermesBwmRenewalStore();
        $paymentHandler = new HermesBwmRenewalPayment();
        $inProduction = sfConfig::get('sf_environment') === 'prod';

        $futureRenewalProcess = new BwmFutureRenewalProcess($emailSender, $invoiceHandler, $bwmRenewalStore, $inProduction);
        $futureRenewalProcess->handleFutureRenewalNotifications($this);

        $dueRenewalProcess = new BwmDueRenewalProcess($emailSender, $invoiceHandler, $bwmRenewalStore, $paymentHandler, $inProduction);
        $dueRenewalProcess->handleDuePayments($this);
    }

    protected function configure()
    {
        $this->addOptions(
            array(
                new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'pwn'),
                new sfCommandOption('env', null, sfCommandOption::PARAMETER_OPTIONAL, 'The environment', 'dev')
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function logInfo($section, $message)
    {
        $args = func_get_args();
        if (count($args) > 2) {
            $message = vsprintf($message, array_slice($args, 2));
        }

        $this->logSection($section, $message);
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function logError($section, $errorMessage)
    {
        $args = func_get_args();
        if (count($args) > 2) {
            $errorMessage = vsprintf($errorMessage, array_slice($args, 2));
        }

        $this->logSection($section, $errorMessage, null, 'ERROR');
        return $this;
    }
}
