<?php

class PostPayGenerateBundleAllowancesTask extends sfBaseTask
{
    protected function configure()
    {
        $yearOption = new sfCommandOption(
            'year',
            null,
            sfCommandOption::PARAMETER_REQUIRED,
            'Year'
        );
        $monthOption = new sfCommandOption(
            'month',
            null,
            sfCommandOption::PARAMETER_REQUIRED,
            'Month'
        );

        $regenerateOption = new sfCommandOption(
            'regenerate',
            null,
            sfCommandOption::PARAMETER_OPTIONAL,
            'Force regenerate',
            false
        );
        $appOptions = array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name','pwn'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_OPTIONAL, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_OPTIONAL, 'The connection name', 'doctrine'),
        );

        $this->addOptions(array_merge($appOptions, array($yearOption, $monthOption, $regenerateOption)));

        $this->namespace        = 'postpay';
        $this->name             = 'GenerateBundleAllowances';
        $this->briefDescription = 'This task (re)generate new bundle allowances for given year and month.';

        $description = array(
            'This task (re)generate new bundle allowances for given year and month.',
            'Call it with:',
            'symfony postpay:GenerateBundleAllowances --year=2013 --month=6 [regenerate]',
            'Additional informations:',
            'Optional --regenerate defaults to false',
        );

        $this->detailedDescription = implode("\n", $description);
    }

    /**
     * Executes the current task.
     *
     * @param array $arguments  An array of arguments
     * @param array $options    An array of options
     * @return integer 0 if everything went fine, or an error code
     * @throws Exception
     */
    protected function execute($arguments = array(), $options = array())
    {

        $hermesUrl = sfConfig::get('app_hermesenvurl', '');
        if (empty($hermesUrl)) {
            throw new Exception('Hermes Url is missing');
        }

        Hermes_Client_Rest::init($hermesUrl);

        $accounts = Hermes_Client_Rest::call(
            'Bundle.getPostPayAccountsForAllowanceGeneration',
            array(
                'year'  => $options['year'],
                'month' => $options['month'],
            )
        );

        $added = array();
        $notAdded = array();
        foreach ($accounts as $account) {
            try {
                Hermes_Client_Rest::call(
                    'Bundle.generateBundleAllowance',
                    array(
                        'account_id'        => $account['account_id'],
                        'product_group_id'  => $account['product_group_id'],
                        'year'              => $options['year'],
                        'month'             => $options['month'],
                        'force'             => $options['regenerate'],
                    )
                );
                $added[] = $account;
            } catch (Exception $e) {
                $notAdded[] = $account;
            }
        }

        echo "count(added):".count($added)."\n";
        echo "count(notAdded):".count($notAdded)."\n";
    }
}
