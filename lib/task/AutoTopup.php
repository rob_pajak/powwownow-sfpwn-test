<?php

/*
 *  CLASS: topup
*
*  Reflect an account due for topup. Methods to attempt topup and return result and introspect accounot details
*
*/

class AutoTopup{

    private $accountId;

    private $topupAmount;
    
    private $organisation;

    private $currency;

    private $topupStatus=NULL;

    private $testMode=1;

    /*
     * __construct
    *
    * sets contructor args to private props
    *
    * @params account_id (int), topupAmount (str), currency(str)
    *
    */

    public function __construct($args){


        $this->accountId=$args['id'];

        $this->topupAmount = $args['amount'];

        $this->currency = $args['currency'];
        
        $this->organisation = $args['organisation'];

        if(isset($_SERVER['ENV']) && strtoupper($_SERVER['ENV']) == "PRODUCTION"  ){
        
            $this->testMode=0;
        
        }        
        
    }

    /*
     * PUBLIC: executeTopup
    *
    * ATtempt auto topup via hermes call. Return boolaen flag representation of result
    * @return BOOLAEN
    *
    */


    public function executeTopup(){


        $args=array('account_id' => $this->accountId,

                'test_mode' => $this->testMode
        );

        try{

            $topup = Hermes_Client_Rest::call('Plus.createAutoTopup', $args);

        }catch(Exception $e){

            throw $e;

        }

        $this->topupStatus=$topup['status'];

        if( $topup['status'] == 'Yes' ){

            // autotopup failed

            return TRUE;

        }else{

            // auto topup succeeded

            return  FALSE;

        }

    }

    public function getAccountId(){

        return $this->accountId;
    }

    
    public function getOrganisation(){
    
        return $this->organisation;
    }   
    
    public function getTopupStatus(){

        return $this->topupStatus ;

    }
    
    public function getAmount(){
        
        return $this->currency.$this->topupAmount;
        
    }

}
