<?php

/**
 * Class ExportTrustPilotCsvTask
 *
 * This Task is used to deal with an input CSV, which in-turn is used to produce an output CSV.
 * Since I am to make this work with all websites and domains, the other trustPilotTask will be deprecated.
 *
 * The Input CSV can have any order for its data, but must be listed down as an argument.
 * The Output CSV will always have the same order: Customer Name, Customer Email, TrustPilot URL
 *
 * To Run this task:
 * symfony ExportTrustPilotCsv
 *      --input="/var/www/sfPwn/input.csv" --output="/var/www/sfPwn/output.csv"
 *
 * symfony ExportTrustPilotCsv
 *      --input="/var/www/sfPwn/inputGo.csv" --output="/var/www/sfPwn/outputGo.csv"
 *      --colOrder="email,ref" --siteUrl="conferencegenie.co.uk"
 *
 * symfony ExportTrustPilotCsv
 *      --input="/var/www/sfPwn/inputPlusPremium.csv"
 *      --output="/var/www/sfPwn/outputPlusPremium.csv"
 *      --colOrder="name,email,ref"
 *      --siteUrl="conferencegenie.co.uk"
 *
 * symfony ExportTrustPilotCsv
 *      --input="/var/www/sfPwn/inputGerman.csv"
 *      --output="/var/www/sfPwn/outputGerman.csv"
 *      --colOrder="name,email,ref"
 *      --siteUrl="powwownow.de"
 *      --trustPilotUrl="trustpilot.de"
 *
 * @author Asfer Tamimi
 *
 */
class ExportTrustPilotCsvTask extends sfBaseTask
{
    /**
     * @var sfUser $user
     */
    protected $user;

    /**
     * @var array $taskOptions
     */
    private $taskOptions;

    /**
     * The Required Input Options
     * @var array $required
     */
    private $required = array('input', 'colOrder', 'output', 'siteUrl');

    /**
     * The Required Fields in the Input CSV
     * @var array $requiredFields
     */
    private $requiredFields = array('email', 'ref');

    /**
     * This will contain the Input Data
     * @var array $data
     */
    private $data = array();

    /**
     * Output Formatted Url
     * @var string $outputUrlFormattedFull
     */
    private $outputUrlFormattedFull = 'http://www.%s/evaluate/%s?a=%s&b=%s&c=%s&e=%s';

    /**
     * Task Configuration
     */
    protected function configure()
    {
        $this->namespace           = '';
        $this->name                = 'ExportTrustPilotCsv';
        $this->briefDescription    = '';
        $this->detailedDescription = '';

        // Add Task Options
        $this->addOptions(
            array(
                new sfCommandOption('input', null, sfCommandOption::PARAMETER_REQUIRED, 'Input CSV File Path'),
                new sfCommandOption(
                    'colOrder',
                    null,
                    sfCommandOption::PARAMETER_REQUIRED,
                    'Column Order',
                    'name,email,ref'
                ),
                new sfCommandOption(
                    'trustPilotUrl',
                    null,
                    sfCommandOption::PARAMETER_OPTIONAL,
                    'TrustPilot Url',
                    'trustpilot.co.uk'
                ),
                new sfCommandOption(
                    'siteUrl',
                    null,
                    sfCommandOption::PARAMETER_REQUIRED,
                    'Site Url',
                    'powwownow.co.uk'
                ),
                new sfCommandOption('output', null, sfCommandOption::PARAMETER_REQUIRED, 'Output CSV File Path'),
            )
        );
    }

    /**
     * @param array $arguments
     * @param array $options
     * @return bool false
     */
    protected function execute($arguments = array(), $options = array())
    {
        sfContext::createInstance(
            sfApplicationConfiguration::getApplicationConfiguration('pwn', 'prod', true)
        );

        $this->user        = sfContext::getInstance()->getUser();
        $this->taskOptions = array_merge($options, $arguments);
        $this->reBuildArguments();

        $this->processData();

        return false;
    }

    /**
     * Takes the Initial Arguments and Returns All Arguments which are NOT null and NOT the task name
     * @throws Exception
     */
    private function reBuildArguments()
    {
        unset($this->taskOptions['help']);
        unset($this->taskOptions['quiet']);
        unset($this->taskOptions['trace']);
        unset($this->taskOptions['version']);
        unset($this->taskOptions['color']);
        unset($this->taskOptions['task']);
        foreach ($this->taskOptions as $k => $v) {
            if (is_null($v)) {
                unset($this->taskOptions[$k]);
            }
        }

        foreach ($this->required as $required) {
            if (!isset($this->taskOptions[$required])) {
                throw new Exception('Required argument not given: ' . $required);
            }
        }

        switch ($this->taskOptions['siteUrl']) {
            case 'powwownow.co.uk':
            case 'www.powwownow.co.uk':
                $this->taskOptions['secretKey'] = 'i52wm98kx73cy46ot29bs83ja67fr45e';
                break;
            case 'powwownow.de':
            case 'www.powwownow.de':
                $this->taskOptions['secretKey'] = 'c85ea37rt64by29xo48sk75qn23ig69z';
                break;
            case 'conferencegenie.co.uk':
            case 'www.conferencegenie.co.uk':
                $this->taskOptions['secretKey'] = 'd35ty74zs69fp82jk35io46wc82nq97r';
                break;
            default:
                throw new Exception($this->taskOptions['siteUrl'] . ' site not supported.');
        }

        if (!file_exists($this->taskOptions['input'])) {
            throw new Exception('Input File does not exist: ' . $this->taskOptions['input']);
        } elseif (!is_readable($this->taskOptions['input'])) {
            throw new Exception('Input File could not be read: ' . $this->taskOptions['input']);
        }

        $this->taskOptions['columnOrder'] = explode(',', $this->taskOptions['colOrder']);

        foreach ($this->requiredFields as $required) {
            if (!in_array($required, $this->taskOptions['columnOrder'])) {
                throw new Exception('Column Order Did not Contain Required Field: ' . $required);
            }
        }
    }

    /**
     * Process the Data by first Inputting, Processing and then Outputting the Data
     * @throws Exception
     */
    private function processData()
    {
        $this->data = array();

        // Obtain the Input Data and Process it
        if (($handle = fopen($this->taskOptions['input'], 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, ',')) !== false) {
                $rowContents          = array_combine($this->taskOptions['columnOrder'], $row);
                $rowContents['name']  = trim($rowContents['name']);
                $rowContents['email'] = trim($rowContents['email']);
                $rowContents['ref']   = trim($rowContents['ref']);

                if (empty($rowContents['name'])) {
                    $rowContents['name'] = ' ';
                }

                $rowContents['url'] = sprintf(
                    $this->outputUrlFormattedFull,
                    $this->taskOptions['trustPilotUrl'],
                    $this->taskOptions['siteUrl'],
                    $rowContents['ref'],
                    base64_encode($rowContents['email']),
                    urlencode($rowContents['name']),
                    sha1($this->taskOptions['secretKey'] . $rowContents['email'] . $rowContents['ref'])
                );

                $this->data[] = array(
                    'name'  => $rowContents['name'],
                    'email' => $rowContents['email'],
                    'url'   => $rowContents['url']
                );
            }
            fclose($handle);
        } else {
            throw new Exception('Error Opening File: ' . $this->taskOptions['input']);
        }

        // Output the Data
        $handle = fopen($this->taskOptions['output'], 'w');
        foreach ($this->data as $fields) {
            fputcsv($handle, $fields);
        }
        fclose($handle);
    }
}
