<?php

class PostPayGenerateInvoicesTask extends sfBaseTask
{
    protected function configure()
    {
        $yearOption = new sfCommandOption(
            'year',
            null,
            sfCommandOption::PARAMETER_REQUIRED,
            'Year'
        );
        $monthOption = new sfCommandOption(
            'month',
            null,
            sfCommandOption::PARAMETER_REQUIRED,
            'Month'
        );
        $appOptions = array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'pwn'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_OPTIONAL, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_OPTIONAL, 'The connection name', 'doctrine'),
        );

        $this->addOptions(array_merge($appOptions, array($yearOption, $monthOption)));

        $this->namespace = 'postpay';
        $this->name = 'GenerateInvoices';
        $this->briefDescription = 'This task generate post pay invoices for given year and month.';

        $description = array(
            'This task generate post pay invoices for the given year and month.',
            'Month and year default to the current month and year if one of them is not given.',
            'Call it with:',
            'symfony postpay:GenerateInvoices [--year=2013] [--month=6]',
        );

        $this->detailedDescription = implode("\n", $description);
    }

    /**
     * Executes the current task.
     *
     * @param array $arguments An array of arguments
     * @param array $options An array of options
     * @return integer 0 if everything went fine, or an error code
     * @throws Exception
     */
    protected function execute($arguments = array(), $options = array())
    {
        // Retrieve the hermes URL.
        $hermesUrl = sfConfig::get('app_hermesenvurl', '');
        if (empty($hermesUrl)) {
            throw new Exception('Hermes Url is missing');
        }

        // Retrieve the month and year.
        if (!empty($options['year']) && !empty($options['month'])) {
            $year = $options['year'];
            $month = $options['month'];
        } else {
            $year = date('Y');
            $month = date('n');
        }

        // As we are not (yet) changing the month and year used by the invoice generation method, we need to calculate
        // the previous month and year.
        $previousMonthTime = strtotime('-1 month', mktime(0, 0, 0, $month, 1, $year));
        $previousMonth = date('n', $previousMonthTime);
        $previousMonthsYear = date('Y', $previousMonthTime);

        Hermes_Client_Rest::init($hermesUrl);
        $accounts = Hermes_Client_Rest::call(
            'Invoice.getAccountsForInvoicing',
            array('year' => $year, 'month' => $month)
        );

        $generated = array();
        $notGenerated = array();
        foreach ($accounts['account_ids'] as $accountId) {
            try {
                Hermes_Client_Rest::call(
                    'Invoice.generatePlusPostPayInvoice',
                    array(
                        'account_id' => $accountId,
                        'year'       => $previousMonthsYear,
                        'month'      => $previousMonth,
                    )
                );
                $generated[] = $accountId;
            } catch (Exception $e) {
                $notGenerated[] = "Invoice not generated for $accountId. Exception message: " . $e->getMessage();
            }
        }

        echo "Count(generated): " . count($generated) . ".\n";
        echo "Count(notGenerated): " . count($notGenerated) . ".\n";
        if ($accounts['warnings']) {
            echo "Warnings found in accounts selection: " . print_r($accounts['warnings'], true) . ".\n";
        }
    }
}
