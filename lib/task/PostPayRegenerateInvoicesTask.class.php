<?php

class PostPayRegenerateInvoicesTask extends sfBaseTask
{
    protected function configure()
    {
        $yearOption = new sfCommandOption(
            'year',
            null,
            sfCommandOption::PARAMETER_REQUIRED,
            'Year'
        );
        $monthOption = new sfCommandOption(
            'month',
            null,
            sfCommandOption::PARAMETER_REQUIRED,
            'Month'
        );

        $appOptions = array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name','pwn'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_OPTIONAL, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_OPTIONAL, 'The connection name', 'doctrine'),
        );

        $this->addOptions(array_merge($appOptions, array($yearOption, $monthOption)));

        $this->namespace        = 'postpay';
        $this->name             = 'RegenerateInvoices';
        $this->briefDescription = 'This task regenerate post pay invoices for given year and month.';

        $description = array(
            'This task regenerate post pay invoices for given year and month.',
            'Call it with:',
            'symfony postpay:RegenerateInvoices --year=2013 --month=6',
        );

        $this->detailedDescription = implode("\n", $description);
    }
    /**
     * Executes the current task.
     *
     * @param array $arguments  An array of arguments
     * @param array $options    An array of options
     *
     * @return integer 0 if everything went fine, or an error code
     */
    protected function execute($arguments = array(), $options = array())
    {

        $hermesUrl = sfConfig::get('app_hermesenvurl', '');
        if (empty($hermesUrl))
            throw new Exception('Hermes Url is missing');

        Hermes_Client_Rest::init($hermesUrl);

        $date = new DateTime($options['year'].'-'.$options['month'].'-01 00:00:00');

        $invoices = Hermes_Client_Rest::call(
            'Invoice.getPostPayInvoices',
            array(
                'year'      => $options['year'],
                'month'     => $options['month'],
                'status_id' => 4,
            )
        );

        $regenerated = array();
        $notRegenerated = array();
        if (count($invoices['invoices']) > 0) {
            foreach ($invoices['invoices'] as $invoice) {
                try {
                    $invoiceRegenerated = Hermes_Client_Rest::call(
                        'Invoice.generatePlusPostPayInvoice',
                        array(
                            'account_id'        => $invoice['account_id'],
                            'year'              => $options['year'],
                            'month'             => $options['month'],
                            'invoice_id'        => $invoice['id'],
                        )
                    );
                    $regenerated[] = $invoice;
                } catch (Exception $e) {
                    $notRegenerated[] = $invoice;
                }
            }
        }
        echo "count(regenerated):".count($regenerated)."\n";
        echo "count(notRegenerated):".count($notRegenerated)."\n";
    }
}
