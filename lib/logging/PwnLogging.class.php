<?php
/**
 * Pwn Logging to Override the Normal Logging
 * 
 * This is used to override the Production set Error Levels for Error Logging
 * 
 * @author Asfer
 *
 */
class PwnLogging {

   /**
    * PwnLogging::logme('This is the Message to be shown','error level');
    */
   public static function logme($message,$priority) {

      if (!in_array($priority,array('emerg','alert','crit','err','warning','notice','info','debug'))) return;

      $oldLogLvl = sfContext::getInstance()->getLogger()->getLogLevel();
      sfContext::getInstance()->getLogger()->setLogLevel(7);
      sfContext::getInstance()->getLogger()->$priority($message);
      sfContext::getInstance()->getLogger()->setLogLevel($oldLogLvl);
   
   }

}
