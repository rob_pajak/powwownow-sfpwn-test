<?php 

class acWebDebugPanelHermesApi extends sfWebDebugPanel
{
  public function getTitle()
  {
    return '<img src="/sfimages/hermesApi.png" alt="Hermes API" height="16" width="16" /> Hermes API';
  }
 
  public function getPanelTitle()
  {
    return 'Hermes API';
  }
 
  public function getPanelContent()
  {
 
    $content = '';
 
    $logs = $this->webDebug->getLogger()->getLogs();

    $lc="";
    
    $rowNo = 1;
    foreach (Hermes_Client_Rest::getProcessedRequestsData() as $row) {

        $toggler = $this->getToggler("debug_hermes_calls_debug_${rowNo}" , 'Toggle response');
        $lc .= sprintf('<li><p class="sfWebDebugDatabaseQuery"><a href="%s" target="_blank">%s</a> %s</p><div class="sfWebDebugDatabaseLogInfo">%0.3f [s]</div><div class="sfWebDebugDatabaseLogInfo" id="debug_hermes_calls_debug_' . $rowNo .'" style="display: none"><span style="text-decoration:underline;">response: </span><br /><pre>%s</pre></div></li>', $row['url'], $row['url'], $toggler, (float) $row["elapsedTime"], htmlspecialchars($row["response"]));
        $rowNo++;

    }

    $listContent = sprintf('<ol style="margin-left: 20px; display: none; " id="debug_hermes_calls_debug">%s</ol>', $lc);
 
    $hermesVerStr = "";
 
    try {
        $hermesVer = Hermes_Client_Rest::call('getVersion', array());       
        if (is_array($hermesVer) === false) {
            throw new Exception('Returned data is not an array');
        }
        foreach ($hermesVer as $k=>$v) {
            $hermesVerStr .= sprintf("<i>%s</i>: %s, &nbsp;", $k, $v);
        }
    }

    
    catch (Exception $e) {
        $hermesVerStr = 'Unable to get Hermes version related information';
    }
 
    $toggler = $this->getToggler('debug_hermes_calls_debug', 'Toggle information');
 
    return sprintf('<h1>List Hermes API calls %s</h1><div id="sfWebDeugHermesApiCallsLogs"><h3>%s</h3>%s</div>',  $toggler, $hermesVerStr, $listContent);

  }

  public static function listenToLoadDebugWebPanelEvent(sfEvent $event)
  {

    $event->getSubject()->setPanel('Hermes API', new self($event->getSubject()) );

  }
  
}
