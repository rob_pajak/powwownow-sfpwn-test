<?php

/**
 * Wrapper class for a bundle row with a possible add-on.
 *
 * Note that in future release, it's possible that bundles should support multiple add-ons.
 */
class Bundle
{
    /**
     * @var Bundle
     */
    private $addon;

    /**
     * @var bool|int
     */
    private $addonId = false;

    /**
     * The original bundle row as returned from the database, minus the upgrade_bundle key.
     *
     * @var array
     */
    private $bundleRow;

    /**
     * @var int
     */
    private $productGroupId;

    /**
     * @var bool
     */
    private $hasAgreedToTAndC;

    /**
     * @var bool
     */
    private $isSkeleton;

    /**
     * @var string
     */
    private $selectedPin;

    public function __construct(array $bundleRow, $agreedToTAndC = true, $skeleton = false)
    {
        if (isset($bundleRow['upgrade_bundle'])) {
            if ($bundleRow['upgrade_bundle']) {
                $this->storeAddon($bundleRow['upgrade_bundle']);
            }
            unset($bundleRow['upgrade_bundle']);
        } elseif (isset($bundleRow['addons'])) {
            if ($bundleRow['addons']) {
                $this->storeAddon(reset($bundleRow['addons']));
            }
            unset($bundleRow['addons']);
        }
        $this->bundleRow = $bundleRow;
        $this->productGroupId = (int) $bundleRow['product_group_id'];
        if (isset($bundleRow['agreed_to_t_and_c'])) {
            $this->hasAgreedToTAndC = $bundleRow['agreed_to_t_and_c'];
        } else {
            $this->hasAgreedToTAndC = $agreedToTAndC;
        }
        if (isset($bundleRow['skeleton'])) {
            $this->isSkeleton = $bundleRow['skeleton'];
        } else {
            $this->isSkeleton = $skeleton;
        }
        if (!empty($bundleRow['pins'])) {
            $this->selectedPin = $bundleRow['pins']['chairman_pin'];
        }
    }

    private function storeAddon(array $addon)
    {
        $this->addon = new Bundle($addon);
        $this->addonId = $this->addon->getProductGroupId();
    }

    /**
     * Upgrades the current bundle with the given add-on.
     *
     * @param array $addon
     * @return $this
     */
    public function upgradeBundle(array $addon)
    {
        $this->storeAddon($addon);
        return $this;
    }

    /**
     * Checks if the bundle has an available add-on.
     *
     * @return bool
     */
    public function hasAddon()
    {
        return (bool) $this->addonId;
    }

    /**
     * Returns the add-on bundle.
     *
     * @return Bundle
     */
    public function getAddon()
    {
        return $this->addon;
    }

    /**
     * Checks if the given product group id is the same as the bundle add-on product group id.
     *
     * @param int $productGroupId
     * @return bool
     */
    public function isValidAddonId($productGroupId)
    {
        return $this->addonId === (int) $productGroupId;
    }

    public function getProductGroupId()
    {
        return $this->productGroupId;
    }

    public function isIndividualBundle()
    {
        return !empty($this->bundleRow['is_individual_bundle']);
    }

    public function isLandLineBundle()
    {
        return empty($this->bundleRow['is_individual_bundle']);
    }

    public function getGroupName()
    {
        return $this->bundleRow['group_name'];
    }

    public function getMonthlyPrice()
    {
        if ($this->hasAddon()) {
            return $this->addon->getMonthlyPrice();
        } else {
            return $this->getMasterMonthlyPrice();
        }
    }

    public function getMasterMonthlyPrice()
    {
        return (float) $this->bundleRow['price_per_month'];
    }

    public function calculateProRataCost($date = null)
    {
        //This has been added to prevent rounding issues
        if ($this->hasAddon()) {
            $masterPrice = $this->calculateProRataOnMonthlyPrice($this->getMasterMonthlyPrice(),$date);
            $addonPrice = $this->calculateProRataOnMonthlyPrice($this->addon->getMonthlyPrice() - $this->getMasterMonthlyPrice(),$date);
            return $masterPrice + $addonPrice;
        } else {
            return $this->calculateProRataOnMonthlyPrice($this->getMonthlyPrice(), $date);
        }
    }

    public function calculateMasterProRataCost($date = null)
    {
        return $this->calculateProRataOnMonthlyPrice($this->getMasterMonthlyPrice(), $date);
    }

    public function calculateProRataOnMonthlyPrice($monthlyPrice, $date = null)
    {
        if ($date === null) {
            $date = time();
        }
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, date('m', $date), date('Y', $date));
        $daysToPay = $daysInMonth - (int) date('d', $date) + 1;
        return round(($monthlyPrice / (float) $daysInMonth) * $daysToPay, 2);
    }

    public function getAssignableProductGroupId()
    {
        if ($this->hasAddon()) {
            return $this->getAddon()->getProductGroupId();
        }
        return $this->getProductGroupId();
    }

    public function isSkeleton()
    {
        return $this->isSkeleton;
    }

    public function hasAgreedToTAndC()
    {
        return $this->hasAgreedToTAndC;
    }

    public function getSelectedPin()
    {
        return $this->selectedPin;
    }
}
