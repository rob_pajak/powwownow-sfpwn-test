<?php

class ProductCreationResult
{
    private $success = true;
    private $pinsAssigned = array();
    private $products = array();
    private $errorMessages = array();

    public function hasErrorOccurred()
    {
        return $this->success === false;
    }

    public function isSuccessful()
    {
        return $this->success === true;
    }

    public function addErrorMessage($message)
    {
        $this->success = false;
        $this->errorMessages[] = $message;
    }

    public function addProduct($productId, $product)
    {
        $this->products[$productId] = $product;
    }

    public function addPinsAssignedToProduct(array $pinRefs, $productId)
    {
        $this->pinsAssigned[$productId] = $pinRefs;
    }

    public function getAddedProducts()
    {
        return $this->products;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getPinsAssigned()
    {
        return $this->pinsAssigned;
    }
}
