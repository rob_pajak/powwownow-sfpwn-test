<?php

/**
 * Deals with figuring out what the source field (in the contact and account table) should be
 *
 * We use the source field to determine where a user signed up, this sounds straight forward, but with CRO this gets
 * tricky as different variations get served up on the same URL and by the same ajax request
 */
class Source
{
    /**
     * Gets the source based on supplied parameters
     *
     * This currently only needs supports switch_to_plus, as other registrations are simpler (they have an action per page)
     *
     * @param string $event switch_to_plus
     * @param string $referrer
     * @param string $sessionVariation
     * @return string
     */
    public function get($event = null, $referrer = null, $sessionVariation = null)
    {

        $referrerParts = parse_url($referrer);

        switch ($event) {
            case 'switch_to_plus':
                if (!is_null($sessionVariation) && ($referrerParts['path'] == '/' || $referrerParts['path'] == '/homepage-g/plus')) {
                    return '/Plus_Reg_Switch_' . $sessionVariation;
                } else {
                    return '/Plus_Reg_Switch';
                }
                break;
        }

        return $event;
    }
}
