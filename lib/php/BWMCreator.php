<?php

class BWMCreator extends ProductCreator
{
    /**
     * Calls the relevant Hermes methods to create the new BWMs.
     *
     * @param array $bwm
     * @param ProductCreationResult $result
     * @return ProductCreationResult
     *   The altered $result object.
     */
    public function createAndAssign($bwm, ProductCreationResult $result)
    {
        // Assert that the BWM is valid.
        if (!$this->isValidBWM($bwm)) {
            $result->addErrorMessage('An attempt to create an invalid BWM was found.');
            return $result;
        }

        // Expand the BWM into the required variables.
        $pinRefs = $bwm['pins'];
        $groupName = $bwm[0]['bwm_label'];
        $transcript = $bwm[0]['transcript'];
        $accountId = $this->accountId;
        $serviceRef = 850;
        $products = array();

        // Retrieve the products (the DNISes) to be assigned.
        foreach ($bwm[1] as $dnis) {
            if (!$this->isValidDNIS($dnis)) {
                continue;
            }
            $products[] = array(
                'geo_city_id' => $dnis['id'],
                'dnis_type' => $dnis['type'],
                'rate' =>  $dnis['rate'],
                'country_code' => $dnis['country_code'],
            );
        }

        if (!$products) {
            $result->addErrorMessage('No valid DNISes were found.');
            return $result;
        }

        // Call the hermes method to create the BWM.
        $assignParameters = array(
            'service_ref' => $serviceRef,
            'pin_refs' => $pinRefs,
            'group_name' => $groupName,
            'account_id' => $accountId,
            'transcript' => $transcript,
            'products' => $products,
            'payment_id' => $this->getPaymentId(),
        );

        try {
            $assignResult = Hermes_Client_Rest::call('BWM.doAssign', $assignParameters);
            $productId = $assignResult['product_group_id'];

            // Given the BWM has been created, and thus pins have been assigned, update the result which will be returned.
            $result->addPinsAssignedToProduct($pinRefs, $productId);
            $result->addProduct($productId, $bwm);
        } catch (Exception $e) {
            // Pass the error to the caller.
            $result->addErrorMessage('Error whilst trying to create new BWM.');
        }

        return $result;
    }

    /**
     * Quick and dirty validation of a new BWM.
     *
     * @param array $bwm
     * @return bool
     */
    private function isValidBWM(array $bwm)
    {
        // Validate high-level properties.
        // Note that the list of pins must exist, but can be empty.
        if (!isset($bwm['pins']) || !is_array($bwm['pins'])
            || empty($bwm[0]) || !is_array($bwm[0])
            || empty($bwm[1]) || !is_array($bwm[1])) {
            return false;
        }

        // Validate 'group_name' and 'transcript'.
        if (empty($bwm[0]['bwm_label']) || empty($bwm[0]['transcript'])) {
            return false;
        }

        return true;
    }

    /**
     * Quick and dirty check for valid selected DNISes.
     *
     * @param array $dnis
     * @return bool
     */
    private function isValidDNIS($dnis)
    {
        $hasIdOrCountryCode = !empty($dnis['id']) || !empty($dnis['country_code']);
        return $hasIdOrCountryCode && !empty($dnis['type']) && !empty($dnis['rate']);
    }
}
