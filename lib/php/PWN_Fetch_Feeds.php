<?php

class PWN_Fetch_Feeds {
  static protected 
     $_data = array(
        /* Twitter API 1.0 has been deprecated - now returning an error */
        'twitter' => array(
                    'url' => NULL,
                    //'url' => "https://api.twitter.com/1/statuses/user_timeline.json?screen_name=powwownow",
                    'callback' => '_processTwitter'
                ),
        'facebook' => array(
                    'url' => "http://www.facebook.com/feeds/page.php?id=7110094798&format=json",
                    'callback' => '_processFacebook'
                ),
        'pwn_blog'    => array(
                    'url' => "http://www.powwownow.co.uk/blog/feed/rss",
                    'callback' => '_processBlog'      
                ),
        'pwn_news' => array(
                    'url' => NULL,
                    'callback' => '_processNewsAndPress',
                ),
    ),
    $_items = 4,
    $_maxLength = 70;

  public static function execute($arguments = array(), $options = array())
  {
    
    foreach (self::$_data as $type => $d) {
        $ret = '';
        //echo $type . "\n";
        self::_processRequest($d['url'], $ret, $d['callback']);
        //r_dump($ret);
        $rsp[$type] = $ret;
    }
    
    return $rsp;

  }

  protected static function _processRequest($url, &$ret, $callback) {

    //echo "testing -1\n";

    if (isset($url)) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,       $url);
        curl_setopt($ch, CURL_HTTP_VERSION_1_1, true);
        //curl_setopt($ch, CURLOPT_REFERER,   'http://www.powwownow.co.uk');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:2.0.1) Gecko/20100101 Firefox/4.0.1');
        curl_setopt($ch, CURLOPT_ENCODING,  'gzip, deflate');
        curl_setopt($ch, CURLOPT_HEADER,    false);
        
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT,   10);
    
        $rsp = curl_exec($ch);

        $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($resultCode != 200) {
            throw new Exception('something went wrong with cURL reuqest');
        }
    } else {
        $rsp = '';
    }

    if (isset($callback) && is_callable(array('self', $callback))) {
        try {
            $ret = call_user_func(array('self', $callback), $rsp);
        }
        catch (Exception $e) {
            $ret = array();
        }
    } 

  }

    protected static function _processTwitter($ret) {

        $ret = array();

        $t = new PowwownowTwitter;

        foreach($t->response() AS $i) {

            $tmp = trim(html_entity_decode(
                preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $i->user->screen_name . ': ' . $i->text), ENT_QUOTES, 'UTF-8'));

            $ret[] = array(
                'text' => (mb_strlen($tmp, 'UTF-8') > self::$_maxLength+3) ? mb_substr($tmp, 0, self::$_maxLength, "UTF-8") . "..." : $tmp,
                'url'  => 'http://twitter.com/Powwownow/statuses/' . (preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $i->id_str)),
                'created_at' => date('Y-m-d h:i:s', strtotime($i->created_at))
            );
    }
        return $ret;
    }

    protected static function _pretendTwitter($ret) {

        /* Twitter API 1.0 has been deprecated - now returning an error
        $ret_decoded = json_decode($ret);
        $ret = array();
        $items = 0;

        foreach ($ret_decoded as $a => $b) {

          $tmp = trim(html_entity_decode(
                  preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $b->user->screen_name . ': ' . $b->text), ENT_QUOTES, 'UTF-8'));

          $ret[] = array(
              'text' => (mb_strlen($tmp, 'UTF-8') > self::$_maxLength+3) ? mb_substr($tmp, 0, self::$_maxLength, "UTF-8") . "..." : $tmp,
              'url'  => 'http://twitter.com/Powwownow/statuses/' . (preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $b->id_str)),
              'created_at' => date('Y-m-d h:i:s', strtotime($b->created_at)),
          );
          if (++$items >= self::$_items) {
              return $ret;
          }
        }

        */

        $ret = array();

        $ret[] = array('text'         => 'Guest blog post from @markshaw. Twitter Chats – Why you should be embracing...',
                       'url'          => 'https://twitter.com/Powwownow/status/346559009425346560',
                       'created_at'   => '2013-06-17 14:24:00'
        );

        $ret[] = array('text'         => 'Bring Your Own Device – flexible or unsecure?',
                       'url'          => 'https://twitter.com/Powwownow/status/345553356737114112',
                       'created_at'   => '2013-06-14 07:48:00'
        );

        $ret[] = array('text'         => 'A great free e-guide to download on how flexible working can help your...',
                       'url'          => 'https://twitter.com/Powwownow/status/345189078997807105',
                       'created_at'   => '2013-06-13 07:41:00'
        );

        $ret[] = array('text'         => '3 Classic Follow Up Mistakes Most People Make With Their Sales Calls',
                       'url'          => 'https://twitter.com/Powwownow/status/345137475934642176',
                       'created_at'   => '2013-06-13 16:16:00'
        );


        return $ret;

    }

  protected static function _processFacebook($ret) {

      $ret_decoded = json_decode($ret);
      $ret = array();
      $items = 0;
      
      foreach ($ret_decoded->entries as $a => $b) {

        //var_dump($b['link']); die();
        
        $tmp = trim(html_entity_decode(
                preg_replace("#(\r\n|\n)+#", "", 
                    preg_replace("/&#x([a-f0-9]{4});/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $b->title)), ENT_QUOTES, 'UTF-8'));

        $ret[] = array(
            'text' => (mb_strlen($tmp, 'UTF-8') > self::$_maxLength+3) ? mb_substr($tmp, 0, self::$_maxLength, "UTF-8") . "..." : $tmp,
            'url'  => $b->alternate,
            'created_at' => date('Y-m-d h:i:s', strtotime($b->updated)),
        );

        if (++$items >= self::$_items) {
            return $ret;
        }

    }

    return $ret;

  }

  protected static function _processBlog($ret) {

      $xml = new SimpleXMLElement($ret); 
      //var_dump($xml);

      $ret = array();
      $items = 0;

      foreach ($xml->channel->item as $a => $b) {

         $tmp = trim(html_entity_decode(
                preg_replace("#(\r\n|\n)+#", "", 
                    preg_replace("/&#x([a-f0-9]{4});/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $b->title)), ENT_QUOTES, 'UTF-8'));

         $ret[] = array(
            'text' => (mb_strlen($tmp, 'UTF-8') > self::$_maxLength+3) ? mb_substr($tmp, 0, self::$_maxLength, "UTF-8") . "..." : $tmp,
            'url'  => (string)$b->link,
            'created_at' => date('Y-m-d h:i:s', strtotime($b->pubDate)),
         );

        if (++$items >= self::$_items) {
            return $ret;
        }

    }

    return $ret;

  }

  protected static function _processNewsAndPress($ret) {

      $params = array(
          'mysql:host=db.th.via-vox.net;dbname=teamsite_live', 
          'teamsite',
          'teamsite',
      );
      $conn = Doctrine_Manager::getInstance()->openConnection($params, 'teamsite_db', false);
      
      $query = "SELECT * FROM `NEWSARTICLECONTENT` WHERE `status` = 1 AND PATH NOT LIKE 'templatedata/en_GB/news_article/data/%/%' ORDER BY order_date DESC LIMIT ". self::$_items ;
      
      //var_dump($query); die();
      
      $ret = array();
      
      foreach ($conn->fetchAll($query) as $a => $b){

          $tmp = trim(html_entity_decode(
                preg_replace("#(\r\n|\n)+#", "", 
                    preg_replace("/&#x([a-f0-9]{4});/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $b['TITLE'])), ENT_QUOTES, 'UTF-8'));

          $tmp2 = explode('/', $b['PATH']);
          $url = '/News/' . (end($tmp2));
                    
          $ret[] = array(
            'text' => (mb_strlen($tmp, 'UTF-8') > self::$_maxLength+3) ? mb_substr($tmp, 0, self::$_maxLength, "UTF-8") . "..." : $tmp,
            'url'  => $url,
            'created_at' => date('Y-m-d h:i:s', strtotime($b['PUBLISH_DATE'])),
           );
      }

      return $ret;
      
  }

}
