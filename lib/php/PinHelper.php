<?php

/**
 * Helper methods for working with Pins
 *
 * @author Wiseman
 */
class PinHelper {

    /**
     * Returns the 'default' pin / pin pair for the contact. (With Caching)
     *
     * A user does not really have a default pin, but we need to show one in places such as the website header
     *  inside mypowwownow, and what we show should be consistent.
     *
     * @todo Make it support other services when needed, it would be nice to make them return:
     * @todo should this be moved to Hermes?
     *
     * @param integer $contactRef (Required)
     * @return array | hermesCallWithCaching
     *         'pin' => 'chair pin',
     *         'pin_ref' => 'chair pin ref',
     *         'participant_pin' => 'participant pin',
     *         'participant_pin_ref' => 'participant pin_ref')
     *
     * @author Wiseman
     * @amend Asfer Tamimi
     */
    public static function getDefaultPins($contactRef) {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');

        try {
            return hermesCallWithCaching('getDefaultPins',array('contact_ref' => $contactRef),'array',3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getDefaultPins Hermes call has timed out. Contact Ref: ' . $contactRef);
            return array();
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getDefaultPins Hermes call is faulty. Contact Ref: ' . $contactRef . ', Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
            return array();
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Contact Ref: ' . $contactRef . ' tried to getDefaultPins but an Error Occurred. Error Message: ' . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode());
            return array();
        }
    }

    /**
     * Get the Call Settings PIN Information
     *
     * @param string $userType
     * @param int    $contactRef
     * @param array $selectedPinRefs This can either be from GET or Posted Values
     * @param bool $showParticipantEditingRefs
     * @return array $pinInformation
     * @author Asfer Tamimi
     */
    public static function getPinInformation(
        $userType,
        $contactRef,
        $selectedPinRefs,
        $showParticipantEditingRefs = false
    ) {
        $pin = false; // The Initial Pin Information to be Shown
        $editingRefs = array(); // Editable PIN Refs
        $editingPins = array(); // Editable PINs
        $cantEditRefs = array(); // UnEditable PIN Refs

        // User PINs
        $usersPins = self::getContactPinPairs($contactRef,$userType);

        // Check the SelectedPinRefs
        if (!empty($selectedPinRefs)) {
            // Go Through the UserPins Array and check which Pin_Refs the User wants to edit and has permission to do so
            foreach ($usersPins as $p) {
                if (in_array($p['chair_pin_ref'], $selectedPinRefs)) {
                    $tmp = self::updatePinInformation($p, $showParticipantEditingRefs);
                    $editingRefs = array_merge($editingRefs, $tmp['editingPinRefs']);
                    $editingPins = array_merge($editingPins, $tmp['editingPins']);
                    if (!$pin) {
                        $pin = $p;
                    }
                }
            }

            // Check which pins the User wanted to edit, but Does NOT have permission to do so.
            $cantEditRefs = array_diff($selectedPinRefs, $editingRefs);
        } else {
            // If the User has not selected a Pin, Choose the First PIN, as this should be their 'main' PIN.
            $pin = current($usersPins);
            $tmp = self::updatePinInformation($pin, $showParticipantEditingRefs);
            $editingRefs = array_merge($editingRefs, $tmp['editingPinRefs']);
            $editingPins = array_merge($editingPins, $tmp['editingPins']);
        }

        return array(
            'pin'          => $pin,
            'editingRefs'  => $editingRefs,
            'cantEditRefs' => $cantEditRefs,
            'editingPins'  => $editingPins,
            'usersPins'    => $usersPins
        );
    }

    /**
     * Update Pin Information Array
     *
     * @param array $pin
     * @param bool $showParticipantEditingRefs
     * @return array
     */
    private static function updatePinInformation ($pin, $showParticipantEditingRefs)
    {
        $editingPinRefs[] = $pin['chair_pin_ref'];
        if (!empty($pin['participant_pin'])) {
            $editingPins[] = array(
                'chair_pin' => $pin['chair_pin'],
                'participant_pin' => $pin['participant_pin']
            );

            if ($showParticipantEditingRefs) {
                $editingPinRefs[] = $pin['participant_pin_ref'];
            }
        } else {
            $editingPins[] = array('chair_pin' => $pin['chair_pin']);
        }

        return array(
            'editingPins' => $editingPins,
            'editingPinRefs' => $editingPinRefs
        );
    }

    /**
     * Update PIN
     *
     * @param  array   $args
     * @param  integer $contact_ref
     * @return array   $result
     * @author Asfer Tamimi
     *
     */
    public static function updatePin($args, $contact_ref)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('updatePin',$args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('updatePin Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'updatePin Hermes call is faulty' .
                ', Error message: ' . $e->getMessage() .
                ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'updatePin Exception Called. Contact Ref: ' . $contact_ref .
                ', tried to update PIN but an Error Occurred. Error Message: ' . $e->getMessage() .
                ', Error Code: ' . $e->getCode()
            );
        }

        return $result;
    }

    /**
     * Deactivates / Removes an Array on PIN Pairs from a specific Contact Ref
     * Only permits a user to delete pins they are an admin for
     * 
     * @param  integer $contact_ref
     * @param  array   $pin_refs
     * @return array   $result
     * @author Asfer Tamimi
     *
     */
    public static function deactivatePINPairs($contact_ref, $pin_refs = array()) {

        $success = array();
        $failure = array();
        $message = '';

        if (count($pin_refs) == 0) {
            sfContext::getInstance()->getLogger()->err('There is Nothing to Deactivate','info');
            return array('success' => $success, 'failed' => $failure);
        }

        $pinPairs = self::getContactPinPairs($contact_ref,'ADMIN');

        foreach ($pinPairs as $id => $pin) {
            if (in_array($pin['chair_pin_ref'],$pin_refs)) {
                // Chair PIN
                $result = self::updatePin(array(
                    'pin_ref' => $pin['chair_pin_ref'], 
                    'active'  => 'N'
                ),$contact_ref);
                if (count($result) == 0) {
                    $failure[] = $pin['chair_pin_ref'];
                } else {
                    $success[] = $pin['chair_pin_ref'];
                }

                // Participant PIN
                $result = self::updatePin(array(
                    'pin_ref' => $pin['participant_pin_ref'], 
                    'active'  => 'N'
                ),$contact_ref);
                if (count($result) == 0) {
                    $failure[] = $pin['participant_pin_ref'];
                } else {
                    $success[] = $pin['participant_pin_ref'];
                }                
            }
        }

        // If the PIN to be deactivated is not found in the Users $pinPairs Array
        // They Normally will not be added to the $failure Array.
        // Add Them to the Array here.
        $noPermissions = array_diff($pin_refs, $success, $failure);
        $failure = array_merge($failure, $noPermissions);

        if (count($success)>0 && count($failure)==0) {
            $message = 'FORM_RESPONSE_PINS_DELETED_SUCCESS';
        } elseif (count($failure)>0 && count($success)==0) {
            $message = 'FORM_RESPONSE_PINS_DELETED_FAILURE';
        } elseif (count($failure)>0 && count($success)>0) {
            $message = 'FORM_RESPONSE_PINS_DELETED_SUCCESS_AND_FAILURE';
        }

        return array('success' => $success, 'failure' => $failure, 'message' => $message);
    }

    /**
     * Adds A PIN Pair to a Plus Account
     * 
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function doPlusAddPins(Array $args) {
        $errorMessages = array();
        $result        = array();

        try {
            $result = Hermes_Client_Rest::Call('doPlusAddPins',$args);
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Contact Ref: ' . $args['contact_ref'] . ', tried to add a PIN Pair to their Account, but an Exception was called. Args: '
                    . serialize($args) . ', Error: ' . serialize($e->getResponseBody())
            );

            switch ($e->getCode()) {
                case '001:000:014':
                    $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                    break;
                case '001:000:000':
                    $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                    break;
                default:
                    $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                    break;
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Contact Ref: ' . $args['contact_ref'] . ', tried to add a PIN Pair to their Account, but an Exception was called. Args: ' . serialize($args) . ', Error: ' . serialize($e->getMessage()));
        }

        return array('result' => $result, 'error' => $errorMessages);
    }    

    /**
     * Takes an Array of Contact PIN Pairs (Probably from PinHelper::getContactPinPairs)
     * Then Returns an Array of Contact Refs
     *
     * @param  array $pinPairs
     * @return array $contact_refs
     * @author Asfer Tamimi
     *
     */
    public static function getContactPinPairsRefs($pinPairs) {
        $contact_refs = array();
        foreach ($pinPairs as $id => $pin) {
            $contact_refs[] = $pin['chair_pin_ref'];
        }
        return $contact_refs;
    }

    /**
     * Takes the Contact Ref, Account Type and a Check to see all account Contact PIN Pairs.
     * This Method is a combination of 4+ Teamsite methods.
     *
     * @todo [DEV-698] move this to Hermes?
     *
     * @param  int  $contact_ref
     * @param  string  $accountType
     * @param  bool $othersOnly
     * @return array  $pinPairs | array()
     * @author Asfer Tamimi
     */
    public static function getContactPinPairs($contact_ref, $accountType = 'USER', $othersOnly = false) {
        if (empty($contact_ref)) return array();
        if ('USER' == $accountType) {
            $hermesCall = 'getContactPins';
        } elseif ('ADMIN' == $accountType) {
            $hermesCall = 'getAccountPinsByContactRef';
        } else {
            return array();
        }

        // Depending on Account Type Select Hermes Call
        try {
            $pins = Hermes_Client_Rest::call($hermesCall, array('contact_ref' => $contact_ref));
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err("$hermesCall Hermes call has timed out.");
            return array();
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err("$hermesCall Hermes call is faulty. Error message: " . $e->getMessage() . ', Error Code: ' . $e->getCode());
            return array();
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err("$hermesCall Failed. Error Message: " . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode());
            return array();
        }

        // Split the Results into Chair and Participant PINs, using the master PIN as the array key
        $chairPins       = array();
        $participantPins = array();
        foreach ($pins as $pin) {
            if (empty($pin['master_pin'])) {
                $chairPins[$pin['pin']] = $pin;
            } else {
                $participantPins[$pin['master_pin']] = $pin;
            }
        }

        // We are going to store the pair of pins in this array
        $pinPairs = array();

        // Add the chair pins to our $pinPair array, preceeding each key with chair_
        foreach ($chairPins as $ref => $pin) {
            foreach ($pin as $k => $v) {
                $pinPairs[$ref]['chair_' . $k] = $v;
            }
        }

        // Add the particpant pins to our $pinPair array, examining the array key preceeding each key with participant_
        foreach ($participantPins as $ref => $pin) {
            foreach ($pin as $k => $v) {
                $pinPairs[$ref]['participant_' . $k] = $v;
            }
        }

        // Remove pins which belong to the passed contact_ref
        if ($othersOnly && 'ADMIN' == $accountType) {
            foreach ($pinPairs as $k => $pair) {
                if ($pair['chair_contact_ref'] == $contact_ref) {
                    unset($pinPairs[$k]);
                }
            }
        }

        // Return our pins as pairs
        return $pinPairs;
    }

    /**
     * Update a Premium Account with an Existing Premium User, for a new PIN Pair
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     */
    public static function doPremiumAddPins($args) {

        $errorMessages = array();
        $result        = array();

        try {
            $result = Hermes_Client_Rest::call('doPremiumAddPins',$args);
        } catch(Exception $e) {
            sfContext::getInstance()->getLogger()->err('doPremiumAddPins Exception Occured using Args: '. serialize($args) . ', Error: ' . (isset($e->responseBody)) ? serialize($e->responseBody) : serialize($e->getMessage()));
            $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
        }        

        return array ('result' => $result, 'error' => $errorMessages);
    }

    /**
     * Update a Plus Account with a New Contact and PINS, for a new PIN Pair
     *
     * @param  array   $args - Main Arguments to be sent to doPlusAddContactAndPins Hermes Method
     * @param  bool    $notify - Do you want to send an Dot Mailer Email?
     * @param  integer $contact_ref - User Contact Ref
     * @return array('result' => $result, 'error' => $errorMessages, 'emailresult' => $emailresult)
     * @author Asfer Tamimi
     *
     */
    public static function doPlusAddContactAndPins(array $args,$notify = false,$contact_ref) {
        $errorMessages = array();
        $result        = array();
        $emailResult   = array();

        try {
            $result = Hermes_Client_Rest::call('doPlusAddContactAndPins',$args);

            if ($notify) {
                try {
                    $emailResult = Hermes_Client_Rest::Call('DotMailer.sendPlusUserCreated',array(
                        'account_id'   => $args['account_id'],
                        'email'        => $args['email'],
                        'userpassword' => $args['password']
                    ));
                } catch(Exception $e) {
                    $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    sfContext::getInstance()->getLogger()->err('doPlusAddContactAndPins Exception Occured using Args: '. serialize($args) . ', Error: ' . (isset($e->responseBody)) ? serialize($e->responseBody) : serialize($e->getMessage()));
                }
            }

        } catch(Hermes_Client_Exception $e) {
            if ($e->getMessage()) {
                sfContext::getInstance()->getLogger()->err(
                    'doPlusAddContactAndPins Exception Occured using Args: '. serialize($args) . ', Error: '
                        . serialize($e->getResponseBody())
                );

                switch ($e->getCode()) {
                    case '001:000:001':
                        $accountResult = Common::getStandardAccount($args['email']);
                        $errorMessages = (count($accountResult) == 0) ? array('message' => 'FORM_CREATE_USER_EMAIL_TAKEN', 'field_name' => 'email') : array('message' => 'FORM_CREATE_USER_EMAIL_TAKEN_INVITE', 'field_name' => 'email');
                        break;
                    case '001:000:007':
                        $errorMessages = array('message' => 'FORM_CREATE_USER_EMAIL_TAKEN_BY_INACTIVE_ACCOUNT', 'field_name' => 'alert');
                        break;
                    default:
                        $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                        break;
                }
            } else {
                $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                sfContext::getInstance()->getLogger()->err(
                    'doPlusAddContactAndPins Exception Occured using Args: '. serialize($args) . ', Error: '
                        . serialize($e->getResponseBody())
                );
            }
        } catch (Exception $e) {
            $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
            sfContext::getInstance()->getLogger()->err(
                'doPlusAddContactAndPins Exception Occured using Args: '. serialize($args) . ', Error: '
                    . serialize($e->getMessage())
            );
        }

        // Clear Cache
        try {
            PWN_Cache_Clearer::modifiedPin($contact_ref);
            PWN_Cache_Clearer::modifiedContact($contact_ref);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('An exception occurred clearing the Cache for Either PIN or Contact');
        }

        return array('result' => $result, 'error' => $errorMessages, 'emailresult' => $emailResult);
    }

    /**
     * Update a Premium Account with a New Contact and PINS, for a new PIN Pair
     *
     * @param  array $args   - The Main Arguments to pass to the doPremiumAddContactAndPins Hermes Method
     * @return array ('result' => $result, 'error' => $errorMessages);
     * @author Asfer Tamimi
     *
     */
    public static function doPremiumAddContactAndPins(Array $args) {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $errorMessages = array();
        $result        = array();

        try {
            $result = hermesCallWithCaching('doPremiumAddContactAndPins',$args,'array',3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('doPremiumAddContactAndPins Hermes call has timed out. Args: ' . print_r($args,true));
            return array ('result' => $result, 'error' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'));
        } catch (Hermes_Client_Exception $e) {
            switch ($e->getCode()) {
                case '001:000:001':
                    $errorMessages = array('message' => 'FORM_CREATE_USER_EMAIL_TAKEN', 'field_name' => 'email');
                    break;
                case '001:000:002':
                    $errorMessages = array('message' => 'FORM_CREATE_PIN_ERROR', 'field_name' => 'alert');
                    break;

                // Service is Not Premium
                case '001:000:028':

                // An Invalid Argument value was passed.
                case '000:003:001':

                // Invalid Service, Customer or Customer Service
                case '001:000:024':
                case '001:000:025':
                case '001:000:026':
                    $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
                default:
                    $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }
            return array ('result' => $result, 'error' => $errorMessages);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('doPremiumAddContactAndPins but an Error Occurred. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
            return array ('result' => $result, 'error' => array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert'));
        }


        // Clear Cache
        try {
            PWN_Cache_Clearer::modifiedPin(sfContext::getInstance()->getUser()->getAttribute('contact_ref'));
            PWN_Cache_Clearer::modifiedContact(sfContext::getInstance()->getUser()->getAttribute('contact_ref'));
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('An exception occurred clearing the Cache for Either PIN or Contact');
        }

        return array ('result' => $result, 'error' => $errorMessages);
    }
}