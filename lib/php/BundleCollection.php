<?php

class BundleCollection
{
    /**
     * @var Bundle[]
     */
    private $bundles;

    /**
     * @var Bundle[]
     */
    private $individualBundles;

    public function __construct(array $bundleRows = array())
    {
        $this->setBundles($bundleRows);
    }

    public function setBundles(array $bundleRows)
    {
        $this->analyseBundles($bundleRows);
        return $this;
    }

    public function getBundles()
    {
        return $this->bundles;
    }

    public function getIndividualBundleProductGroupIds()
    {
        if (!$this->individualBundles) {
            return array();
        }
        return array_combine(array_keys($this->individualBundles), array_keys($this->individualBundles));
    }

    private function analyseBundles(array $bundleRows)
    {
        $this->bundles = array();
        foreach ($bundleRows as $row) {
            $bundle = new Bundle($row);
            $this->bundles[$bundle->getProductGroupId()] = $bundle;
        }

        $this->collectIndividualBundles($this->bundles);
    }

    private function collectIndividualBundles(array $bundles)
    {
        $this->individualBundles = array();
        /** @var Bundle $bundle */
        foreach ($bundles as $bundle) {
            if ($bundle->isIndividualBundle()) {
                $this->individualBundles[$bundle->getProductGroupId()] = $bundle;
                if ($bundle->hasAddon()) {
                    $this->individualBundles[$bundle->getAddon()->getProductGroupId()] = $bundle->getAddon();
                }
            }
        }
    }
}
