<?php

require_once('IpRetriever.php');

/**
 * Class Plus_Authenticate
 */
class Plus_Authenticate
{
    /**
     * @desc
     *  Log in user.
     * @param $email
     * @param $password
     * @param $rememberMe
     * @return bool
     * @throws Exception
     * @throws sfException
     * @deprecated
     *  User LoginManager instead
     */
    public static function logIn($email, $password, $rememberMe = false)
    {
        self::logOut(true);

        // Check Contact
        $contactDetails['email']    = $email;
        $contactDetails['password'] = $password;

        $ipRetriever                  = new IpRetriever();
        $contactDetails['ip_address'] = $ipRetriever->retrieveIPAddress();

        $result = Hermes_Client_Rest::call('authenticateContact', $contactDetails);

        // Get User Details
        $user = sfContext::getInstance()->getUser();

        // Check Contact Response
        if (!$result || count($result) === 0 || empty($result['contact_ref'])) {
            return false;
        }

        return self::setCredentials($email, $result, $rememberMe);
    }


    /**
     * @param $email
     * @param $contact_ref
     * @return bool
     * @throws Exception
     * @throws sfException
     */
    public static function logInWithContactRef($email, $contact_ref)
    {

        self::logOut(true);

        // Start Session
        $sessionStatus = self::startSession();
        if ($sessionStatus === false) {
            throw new Exception('Authenticate::Session Failed to Start for Email ' . $email);
        }

        if (is_null($email) || is_null($contact_ref)) {
            throw new Exception('Authenticate::Session Failed to Start for Email ' . $email);
        }

        // Check Contact
        $contactDetails['email']       = $email;
        $contactDetails['contact_ref'] = $contact_ref;

        $ipRetriever                  = new IpRetriever();
        $contactDetails['ip_address'] = $ipRetriever->retrieveIPAddress();

        $ret = Hermes_Client_Rest::call('authenticateContact', $contactDetails);

        // Get User Details
        $user = sfContext::getInstance()->getUser();

        // Check Contact Response
        if (!$ret || count($ret) === 0 || empty($ret['contact_ref'])) {
            return false;
        }

        return self::setCredentials($email, $ret);
    }

    /**
     * Logs a user in via God Mode (a feature in customer services)
     *
     * @param array $args
     *     $args['email'] - Email
     *     $args['contact_ref'] - Contact Ref
     *     $args['accessor_user_reference'] - User Reference
     *     $args['country_site_redirect'] - Country Name
     *     $args['password'] - Password
     * @return array - True if user authenticated, otherwise an error in an array
     *
     * @author Asfer
     */
    public static function logInViaGodMode($args)
    {
        // Logout
        self::logOut(true);

        $email                 = $args['email'];
        $contact_ref           = $args['contact_ref'];
        $accessorUserReference = isset($args['accessor_user_reference']) ? $args['accessor_user_reference'] : null;
        $countryName           = isset($args['country_site_redirect']) ? $args['country_site_redirect'] : 'GBR';
        $password              = $args['password'];

        // Start Session
        $sessionStatus = self::startSession();
        if ($sessionStatus === false) {
            sfContext::getInstance()->getLogger()->err(
                __CLASS__ . '::' . __METHOD__ . "::Authenticate::Session Failed to Start for contact_ref: $contact_ref"
            );
            return array('error' => 'session_failed');
        }

        // Check Contact Ref
        if (is_null($contact_ref) || !is_numeric($contact_ref)) {
            sfContext::getInstance()->getLogger()->err(
                __CLASS__ . '::' . __METHOD__ . "::Authenticate Failed due to Incorrect Contact Ref: $contact_ref"
            );
            return array('error' => 'contact_ref_failure');
        }

        // Get Contact Information (Email Mainly)
        $contactDetails = Common::getContactByRef(array('contact_ref' => $contact_ref));
        if (isset($contactDetails['error']) || !isset($contactDetails['email'])) {
            sfContext::getInstance()->getLogger()->err(
                __CLASS__ . '::' . __METHOD__ . "::Authenticate Failed due to Failed Contact Information being given. Contact Ref: $contact_ref"
            );
            return array('error' => 'contact_info_failure');
        }

        // Get Contact Information
        $contact = Common::getContact($contactDetails['email'], null, true);
        if (isset($contactDetails['error']) || !isset($contactDetails['email'])) {
            sfContext::getInstance()->getLogger()->err(
                __CLASS__ . '::' . __METHOD__ . "::Authenticate Failed due to Failed Contact Information being given. Contact Ref: $contact_ref"
            );
            return array('error' => 'contact_info_failure');
        }

        // Update the God Mode Access Log
        if ($accessorUserReference) {
            try {
                Hermes_Client_Rest::call(
                    'GodMode.logAccess',
                    array(
                        'contact_ref' => $contact_ref,
                        'user_ref'    => $accessorUserReference,
                    )
                );
            } catch (Hermes_Client_Request_Timeout_Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'GodMode.logAccess Hermes call has timed out.'
                );
            } catch (Hermes_Client_Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'GodMode.logAccess Hermes call is faulty, Error message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode()
                );
            } catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'GodMode.logAccess Exception Called. Unable to log GodMode access. Error Message: ' . $e->getMessage(
                    ) . ', Error Code: ' . $e->getCode()
                );
            }
        }

        // Check the Country Name to decide where to Authenticate
        sfContext::getInstance()->getLogger()->err("Country $countryName, Authenticating");
        switch ($countryName) {
            case 'GBR':
                sfContext::getInstance()->getUser()->setAuthenticated(true);
                $_SESSION['authenticated'] = true;

                if (self::setCredentials($contact['email'], $contact)) {
                    return array('status' => 'success');
                } else {
                    return array('status' => 'fail');
                }
            // Add International Site Authentication
            case "AUT":
            case "BEL":
            case "CAN":
            case "FRA":
            case "DEU":
            case "IRL":
            case "ITA":
            case "NLD":
            case "POL":
            case "ZAF":
            case "ESP":
            case "SWE":
            case "CHE":
            case "USA":
                // Initialise the CURL setup to send the information to Zend CMS
                $crl = curl_init();
                curl_setopt_array(
                    $crl,
                    array(
                        CURLOPT_URL            => SiteURL::getSiteURL($countryName) . '/mypwn/login/dogodmodelogin',
                        CURLOPT_HEADER         => false,
                        CURLOPT_POST           => true,
                        CURLOPT_POSTFIELDS     => array(
                            'loginEmail'        => $email,
                            'god_mode_password' => $password,
                            'contact_ref'       => $contact_ref,
                            'initial_check'     => 1
                        ),
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_CONNECTTIMEOUT => 30,
                        CURLOPT_FOLLOWLOCATION => false,
                    )
                );
                $result = curl_exec($crl);
                curl_close($crl);
                if (strstr($result, 'success')) {
                    return array('status' => 'success');
                } else {
                    return array('status' => 'fail');
                }
            default:
                sfContext::getInstance()->getLogger()->err("Unknown Country Selected: $countryName");
                return array('error' => 'invalid_country');
        }
    }

    /**
     * @desc
     *  Sets up session variables with relevant user info (user type, service ref etc).
     *  It does not log a user in, use self::logIn() for that
     * @param $emailAddress
     * @param $contact
     * @return bool
     * @throws sfException
     * @deprecated
     *  User LoginManager instead
     */
    public static function setCredentials($emailAddress, $contact)
    {
        self::startSession();

        // Get User Details
        $user = sfContext::getInstance()->getUser();
        $user->clearCredentials();
        $user->setAuthenticated(true);

        $user->setAttribute('authenticated', true);
        $user->setAttribute('service_user', isset($contact['service_user']) ? $contact['service_user'] : '');
        $user->setAttribute('contact_ref', isset($contact['contact_ref']) ? $contact['contact_ref'] : '');
        $user->setAttribute('first_name', isset($contact['first_name']) ? $contact['first_name'] : '');
        $user->setAttribute('last_name', isset($contact['last_name']) ? $contact['last_name'] : '');
        $user->setAttribute('email', $emailAddress);
        $user->setAttribute(
            'time_limited_pins',
            isset($contact['time_limited_pins']) ? $contact['time_limited_pins'] : false
        );
        $user->setAttribute('account_id', isset($contact['account_id']) ? $contact['account_id'] : '');
        $user->setAttribute(
            'previous_login_time',
            isset($contact['previous_login_time']) ? $contact['previous_login_time'] : ''
        );
        $user->setAttribute('service_ref', isset($contact['service_ref']) ? $contact['service_ref'] : '');

        switch ($contact['service_user'] ) {
            case 'POWWOWNOW':
                $user->addCredentials('powwownow', 'user', 'POWWOWNOW');
                $user->setAttribute('service', 'powwownow');
                $user->setAttribute('user_type', 'user');
                break;
            case 'PLUS_USER':
                $user->addCredentials('plus', 'user', 'PLUS_USER');
                $user->setAttribute('account_id', $contact['account_id']);
                $user->setAttribute('service', 'plus');
                $user->setAttribute('user_type', 'user');
                break;
            case 'PLUS_ADMIN':
                $user->addCredentials('plus', 'admin', 'PLUS_ADMIN');
                $user->setAttribute('account_id', $contact['account_id']);
                $user->setAttribute('service', 'plus');
                $user->setAttribute('user_type', 'admin');
                break;
            case 'PREMIUM_USER':
                $user->addCredentials('premium', 'user', 'PREMIUM_USER');
                $user->setAttribute('service', 'premium');
                $user->setAttribute('user_type', 'user');
                break;
            case 'PREMIUM_ADMIN':
                $user->addCredentials('premium', 'admin', 'PREMIUM_ADMIN');
                $user->setAttribute('service', 'premium');
                $user->setAttribute('user_type', 'admin');
                break;
        }

        try {
            $isVirgin = self::isVirgin();
        } catch (Exception $e) {
            $isVirgin = false;
            sfContext::getInstance()->getLogger()->err('self:isVirgin threw an exception: ' . $e->getMessage());
        }

        $user->setAttribute('is_virgin', $isVirgin);

        return session_regenerate_id();
    }

    /**
     * Logs current user out
     *
     * If $clearAllData is true then it will also wipe all user data (name, email etc)
     * from the session
     *
     * @param bool $clearAllData
     *
     * @author Wiseman
     * @deprecated
     *  User LoginManager instead
     */
    public static function logOut($clearAllData = true)
    {
        self::startSession();
        $user = sfContext::getInstance()->getUser();
        $user->setAuthenticated(false);
        $user->clearCredentials();

        unset($_SESSION['OAUTH_STATE']);
        unset($_SESSION['OAUTH_ACCESS_TOKEN']);

        if ($clearAllData) {
            // Clears SF user session data
            $user->getAttributeHolder()->removeNamespace();
        }

        session_regenerate_id();
    }

    /**
     * @desc
     *  Start session, if it has not already been started
     * @return bool
     */
    public static function startSession()
    {
        if (session_id() === null) {
            session_name('mypwn');
            ini_set('session.cookie_httponly', true);
            $status = session_start();
        }
        return (isset($status)) ? $status : true;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private static function isVirgin()
    {
        self::startSession();

        if (empty($_SESSION['contact_ref'])) {
            throw new Exception('contact_ref not set in plusAuthenticate');
        }

        $vTest = Hermes_Client_Rest::call('isVirginUser', array('contact_ref' => $_SESSION['contact_ref']));

        return $vTest['isVirgin'];

    }
}
