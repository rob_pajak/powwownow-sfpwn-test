<?php
/**
 * Class TrustPilot
 *
 * Feeds
 * http://s.trustpilot.com/tpelements/892079/f.json.gz // We are using this one
 * http://s.trustpilot.com/tpelements/892079/f.jsonp
 * https://ssl.trustpilot.com/tpelements/892079/f.jsonp
 *
 * Usage of the TrustPilot API
 *
 * $trustPilotFeed = TrustPilot::getFeed(true); // If you are running from PHP
 * var Feed = <?php echo TrustPilot::getFeed(false); ?> // If you are running from Javascript (?)
 *
 * $trustPilotFeed->FeedUpdateTime->UnixTime; // int 1399624143
 * $trustPilotFeed->FeedUpdateTime->Human; // string '09 May 2014 08:29:03 GMT'
 * $trustPilotFeed->FeedUpdateTime->HumanDate; // string '9. May'
 *
 * $trustPilotFeed->ReviewCount->Total // int 55
 * $trustPilotFeed->ReviewCount->DistributionOverStars // array (size=5)
 *                                                          0 => int 0
 *                                                          1 => int 1
 *                                                          2 => int 0
 *                                                          3 => int 6
 *                                                          4 => int 48
 *
 * $trustPilotFeed->ReviewPageUrl // string 'http://www.trustpilot.co.uk/review/powwownow.co.uk'
 * $trustPilotFeed->Reviews // array (shows the last 10 Reviews
 *
 * $trustPilotFeed->TrustScore->Score // int 94 (out of 100)
 * $trustPilotFeed->TrustScore->Stars // int 5
 * $trustPilotFeed->TrustScore->Human // string 'Excellent' (Or what ever TrustPilot say)
 *
 * $trustPilotFeed->TrustScore->StarsImageUrls->large // string '//s3-eu-west-1.amazonaws.com/s.trustpilot.com/images/tpelements/stars/l/5.png'
 * $trustPilotFeed->TrustScore->StarsImageUrls->medium // string '//s3-eu-west-1.amazonaws.com/s.trustpilot.com/images/tpelements/stars/m/5.png'
 * $trustPilotFeed->TrustScore->StarsImageUrls->small // string '//s3-eu-west-1.amazonaws.com/s.trustpilot.com/images/tpelements/stars/s/5.png'
 *
 * @author Asfer Tamimi
 *
 */
class TrustPilot
{
    private static $jsonFeed = "http://s.trustpilot.com/tpelements/892079/f.json.gz";
    private static $jsonFeedBackup = '/trustpilot/f.json';

    /**
     * Return the Feed, bypassing the Caching
     *
     * @param bool $decoded
     * @return bool|mixed
     */
    public static function getFeedRAW($decoded = false)
    {
        try {
            $fileContents = file_get_contents(self::$jsonFeed);

            if (!function_exists('gzdecode')) {
                $fileDecoded = self::getGZipDecodeReplacement($fileContents);
            } else {
                $fileDecoded = gzdecode($fileContents);
            }

            if ($decoded) {
                $fileDecoded = json_decode($fileDecoded);
            }

            /**
             * According to TrustPilot always changing their file format,
             * this can be false as the file has already been gzuncompressed
             * So use the Original File Contents
             */
            if (is_object($fileDecoded) === false) {
                $fileDecoded = json_decode($fileContents);
            }

            /**
             * To be extra sure that even that is not an object, we have a copy of the json file kept locally
             */
            if (is_object($fileDecoded) === false) {
                $fileDecoded = json_decode(file_get_contents(sfConfig::get('sf_web_dir') . self::$jsonFeedBackup));
            }

            // Check to see if the File is still not an object
            if (!is_object($fileDecoded)) {
                throw new Exception('File is not Object');
            }

            return $fileDecoded;
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                __CLASS__ . ' :: ' . __METHOD__ . ' :: ' . 'Could Not Return successful JSON From Feed' .
                print_r($e->getMessage(), true)
            );
        }

        return false;
    }

    /**
     * Return the Cached Feed
     *
     * @param bool $decoded
     * @return mixed
     */
    public static function getFeed($decoded = false)
    {
        $cache = new sfFileCache(array(
            'cache_dir' => sfConfig::get('sf_cache_dir') . '/function',
            'lifetime'  => 172800 //2 Days
        ));
        $fc    = new sfFunctionCache($cache);

        try {
            $result = $fc->call('TrustPilot::getFeedRAW', array('decoded' => $decoded));
            return $result;
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                __CLASS__ . ' :: ' . __METHOD__ . ' :: ' . 'Could Not Return successful JSON From Feed' .
                print_r($e->getMessage(), true) . ' :: ' .
                print_r($e->getCode(), true) . ' :: '
            );
        }

        return false;
    }

    private function getGZipDecodeReplacement($data)
    {
        return gzinflate(substr($data,10,-8));
    }
}
