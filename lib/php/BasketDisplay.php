<?php

/**
 * @todo move this into a separate parent class for Basket products.
 */
class BasketDisplay
{
    /**
     * Prepares basket products for display.
     *
     * @param array $products
     * @return array
     */
    public function prepareProductsForBasketDisplay(array $products)
    {
        $processed = array();
        foreach ($products as $productId => $product) {
            if ($product instanceof Bundle) {
                $processed[] = $this->prepareBundleObjectForBasketDisplay($product);
            } elseif (isset($product[1]['BWM_total_cost'])) {
                $processed[] = $this->prepareBWMForBasketDisplay($productId, $product);
            } elseif (isset($product['type']) && $product['type'] === 'credit') {
                $processed[] = $this->prepareCallCreditForBasketDisplay($productId, $product);
            } elseif (isset($product['second_allowance'])) {
                $processed[] = $this->prepareBundleObjectForBasketDisplay(new Bundle($product));
            }
        }
        return $processed;
    }

    /**
     * @param int $productId
     * @param string $type
     * @param string $label
     * @param float $total
     * @param null|float $oneOffCharge
     * @param null|float $monthlyCharge
     * @param null|float $yearlyCharge
     * @return array
     */
    private function prepareBasketProductForDisplay($productId, $type, $label, $total, $oneOffCharge = null, $monthlyCharge = null, $yearlyCharge = null)
    {
        if ($total !== null) {
            $total = $this->prepareCost($total);
        }
        if ($oneOffCharge !== null) {
            $oneOffCharge = $this->prepareCost($oneOffCharge);
        }
        if ($monthlyCharge !== null) {
            $monthlyCharge = $this->prepareCost($monthlyCharge);
        }
        if ($yearlyCharge !== null) {
            $yearlyCharge = $this->prepareCost($yearlyCharge);
        }

        return array(
            'product_id' => $productId,
            'label' => $label,
            'one_off_charge' => $oneOffCharge,
            'monthly_charge' => $monthlyCharge,
            'yearly_charge' => $yearlyCharge,
            'total' => $total,
            'type' => $type,
            'removable' => true,
            'extra_data' => array()
        );
    }

    public function prepareCallCreditForBasketDisplay($productId, array $product)
    {
        $creditRow = $this->prepareBasketProductForDisplay(
            $productId,
            'credit',
            $product['label'],
            $product['amount'],
            $product['amount']
        );
        $creditRow['editable_amount'] = $product['editable_amount'];
        $creditRow['editable_recharge'] = $product['editable_recharge'];
        $creditRow['auto-recharge'] = $product['auto-recharge'];
        $creditRow['removable'] = $product['removable'];
        $creditRow['editable_charge'] = $product['amount'];

        return $creditRow;
    }

    /**
     * Prepares a BWM for display on the Basket page.
     *
     * @param int $productId
     * @param array $bwm
     * @return array
     *   All the required data for the Basket page.
     */
    public function prepareBWMForBasketDisplay($productId, array $bwm)
    {
        return $this->prepareBasketProductForDisplay(
            $productId,
            'bwm',
            $bwm[0]['bwm_label'],
            $bwm[1]['BWM_total_cost']['total'] + $bwm[1]['BWM_total_cost']['connectionFee'],
            $bwm[1]['BWM_total_cost']['connectionFee'],
            null,
            $bwm[1]['BWM_total_cost']['total']
        );
    }

    public function prepareBundleObjectForBasketDisplay(Bundle $bundle, $masterTotal = 0)
    {
        if ($masterTotal) {
            $proRataCharge = $bundle->calculateProRataOnMonthlyPrice($bundle->getMasterMonthlyPrice() - $masterTotal);
        } else {
            $proRataCharge = $bundle->calculateMasterProRataCost();
        }
        $product = $this->prepareBasketProductForDisplay(
            $bundle->getProductGroupId(),
            'bundle',
            $bundle->getGroupName() . ' (monthly-rolling contract)',
            $proRataCharge,
            null,
            $bundle->getMasterMonthlyPrice() - $masterTotal
        );
        $product['extra_data']['is_individual_bundle'] = $bundle->isIndividualBundle();
        $product['extra_data']['has_addon'] = $bundle->hasAddon();
        if ($bundle->hasAddon()) {
            $product['extra_data']['addon'] = $this->prepareBundleObjectForBasketDisplay($bundle->getAddon(), $bundle->getMasterMonthlyPrice());
        }

        if ($bundle->isSkeleton() && !$bundle->hasAgreedToTAndC()) {
            $product['extra_data']['show_bundle_actions'] = true;

            if (!$bundle->hasAddon()) {
                $product['extra_data']['show_addon_form'] = true;
                $addon = $this->retrievePossibleBundleUpgrade($product['product_id']);
                // Assuming that ALL bundles have at least on available add-on.
                $bundleAddon = new Bundle($addon);
                $product['extra_data']['addon_price'] = $bundleAddon->getMasterMonthlyPrice() - $bundle->getMasterMonthlyPrice();
            }
            if (!$bundle->hasAgreedToTAndC()) {
                $product['extra_data']['show_t_and_c_form'] = true;
            }
            if ($bundle->isIndividualBundle()) {
                $product['extra_data']['show_pin_pair_form'] = true;
                $product['extra_data']['pin_pair_form'] = new IndividualUserBundleAssignForm(array('user' => $bundle->getSelectedPin()));
            }
        }

        return $product;
    }

    private function retrievePossibleBundleUpgrade($bundleId)
    {
        $bundleProducts = plusCommon::retrieveBundleProducts(sfContext::getInstance()->getUser()->getCulture());
        if (!isset($bundleProducts[$bundleId]['addons'])) {
            return false;
        }
        return $bundleProducts[$bundleId]['addons'][0];
    }

    private function prepareCost($cost)
    {
        return sprintf('£%s', number_format($cost, 2, '.', ','));
    }
}
