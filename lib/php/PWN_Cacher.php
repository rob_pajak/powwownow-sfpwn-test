<?php
/**
 * PWN_Cacher: static class providing cache-ing capability
 *
 * @package     PWNUtils
 * @author      Robert Pajak

 * @created 2011-05-03
 */
class PWN_Cacher {

    /**
     * private variable to store information where cache Files should be stored in
     */
    protected static $_cacheDir = '/var/www/pwn_generated/php_includes/cache/';
    protected static $_filePrefix = "_cached_";
    protected static $_fileExtension = ".cache";

    /**
     * public static method to init PWN_Cacher
     */
    public static function initCacheDir($dirName) {

        // check if this Directory exist
        if (is_dir($dirName) != TRUE) {

            if (mkdir($dirName, 0777) != TRUE) {

                throw new Exception("PWN_Cacher::initCacheDir - requested dir: ${dirName} doesn't exist and couldn't be created");

            }

        }

        if (!is_writable($dirName)) {

            throw new Exception("PWN_Cacher::initCacheDir - requested dir: '${dirName}' MUST be writeable, but is not.");

        }

        self::$_cacheDir = $dirName;

    }

    protected static function _getStringFromArgs($args) {

        if (!is_array($args)) {

            PWN_Logger::log("PWN_Logger::_getStringFromArgs => ${args} is not an array => returning `~|~${args}~|~`", PWN_Logger::DEBUG_VERBOSE);
            return "~|~".$args."~|~";

        } else {

            ksort($args);

        }

        $imploded = "";

        foreach ($args as $key => $value) {

            if (is_array($value)) {

                $value = self::_getStringFromArgs($value);

            }

            $imploded .= "~k|~".$key."~v|~".$value;

        }
        
        PWN_Logger::log("PWN_Logger::_getStringFromArgs => returning `${imploded}`", PWN_Logger::DEBUG_VERBOSE);
        return $imploded;

    }

    public static function getContent($call, $args=array(), $ttl=7200, $regenerateCache = false) {

        if (self::$_cacheDir == '') {

            throw new Exception("PWN_Cacher::getContent - cache dir was not init.");

        }

        //check what is the requested Time to Live
        if ($ttl<0) {

            // just return the content of the cache
            PWN_Logger::log("Requested call with bypassing file cache, new cache content will not be generated", PWN_Logger::DEBUG);
            return call_user_func($call, $args);

        }

        $imploded = self::_getStringFromArgs($args);

        $cachedFilename = self::$_filePrefix . sha1(">>${call}<<".$imploded).self::$_fileExtension;

        // check if there is a content in the cache
        if (file_exists(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename)) {
            PWN_Logger::log("file '${cachedFilename}' exists in the cache dir", PWN_Logger::DEBUG_VERBOSE);


            $timeDiff = (time() - filemtime(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename));
            PWN_Logger::log("file '${cachedFilename}' is ${timeDiff} sec old ", PWN_Logger::DEBUG_VERBOSE);

            if ($regenerateCache == true) {

                PWN_Logger::log("Requested REGENERATING content of the file '${cachedFilename}'", PWN_Logger::DEBUG);
                unlink(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename);

            }

            else if ((time() - filemtime(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename) > $ttl)) {

                PWN_Logger::log("file '${cachedFilename}' is too old => removing it", PWN_Logger::DEBUG);
                unlink(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename);

            } else {

                // Serve content from the cache
                PWN_Logger::log("Served cached content. File '${cachedFilename}' contains still valid content for ${call} with arguments: " . print_r($args, true) . "\n", PWN_Logger::DEBUG);
                //return unserialize(file_get_contents(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename));

                $errPrev = error_get_last();

                $cnt = file_get_contents(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename);

                $content = @unserialize($cnt);

                if ($content === false) {

                    $errCurr = error_get_last(); 

                    if ((count(array_diff((array)$errPrev,(array)$errCurr))>0) || (count(array_diff((array)$errCurr, (array)$errPrev))>0)){

                        throw new Exception ("unserialize was not successfull: " . $cnt);

                    }

               }

               return $content;

            }

        } else {

            PWN_Logger::log("file '${cachedFilename}' doesn't exists in the cache dir", PWN_Logger::DEBUG_VERBOSE);

        }

        try {

            $content = call_user_func($call, $args);
            PWN_Logger::log("call_user_func returned: " . print_r($content, true), PWN_Logger::DEBUG_VERBOSE);
            file_put_contents(self::$_cacheDir . DIRECTORY_SEPARATOR. $cachedFilename, serialize($content));
            PWN_Logger::log("Cached call ${call} with arguments:\n" . print_r($args, true) . "=> saved to a file '${cachedFilename}'", PWN_Logger::DEBUG);
            return $content;

        } 
        catch (Exception $e) 
        {

            PWN_Logger::log("Exception has been caught while calling ${call} with arguments:\n" . print_r($args, true) . "\nFull Error: ". serialize($e), PWN_Logger::ERROR);
            throw $e;

        }

    }


    public static function clearContent($call, $args=array()) {

        if (self::$_cacheDir == '') {

            throw new Exception("PWN_Cacher::clearContent - cache dir was not init.");

        }

        $imploded = self::_getStringFromArgs($args);

        $cachedFilename = self::$_cacheDir . DIRECTORY_SEPARATOR . self::$_filePrefix . sha1(">>${call}<<".$imploded).self::$_fileExtension;

        // check if there is a content in the cache
        if (file_exists($cachedFilename)) {

            PWN_Logger::log("file '${cachedFilename}' exists in the cache dir - about to be deleted", PWN_Logger::DEBUG_VERBOSE);
            $ret = unlink($cachedFilename);
            PWN_Logger::log("result of unlink('${cachedFilename}') ".print_r($ret, true), PWN_Logger::DEBUG_VERBOSE);
            return $ret;

        } else {

            PWN_Logger::log("file '${cachedFilename}' doesn't exists in the cache dir", PWN_Logger::DEBUG_VERBOSE);
            return true;

        }

    }

    public static function setCacheFilePrefix($prefix) {

        if (isset($prefix) && is_string($prefix)) {

            self::$_filePrefix = $prefix;
            return true;

        }

        return false;

    }

    public static function setCacheFileExtension($extension) {

        if (isset($extension) && is_string($extension)) {

            self::$_fileExtension = $extension;
            return true;

        }

        return false;

    }

}

/*
 *  Examples of usage:
 *  just uncomment it
 * 
 */

/*
require_once('Logger.php');
PWN_Logger::initLogFile();
PWN_Logger::setCurrentLogLevel(PWN_Logger::DEBUG);
PWN_Cacher::initCacheDir('/var/log/hermes/test2');
echo ucwords('hello world!') . "\n";
echo call_user_func('ucwords', 'hello world!')."\n";
echo PWN_Cacher::getContent('ucwords', 'hello world!') . "\n";

class exmpl {
	
	public static function demo (array $arg) {

        echo "Real class method is beeing called\n";

        $ret = array();

        foreach ($arg as $k => $v) {

            if (is_numeric($v)) {

                $ret[$k] = $v+2;

            } else {

               $ret[$k] = $v .' + 2';

            }

        }

        return $ret;

     }
}
echo "***************\n";

//var_dump(exmpl::demo(array('first' => 1, 'second' => 2, 'third' => 'blah blah blah'))) . "\n";
//var_dump(call_user_func('exmpl::demo', array('first' => 1, 'second' => 2, 'third' => 'blah blah blah'))) . "\n";
var_dump(PWN_Cacher::getContent('exmpl::demo', array('second' => 2, 'first' => 1, 'third' => 'blah blah blah'))) . "\n";
var_dump(PWN_Cacher::getContent('exmpl::demo', array('third' => 'blah blah blah', 'first' => 1, 'second' => 2))) . "\n";

// 120 sec cache
var_dump(PWN_Cacher::getContent('exmpl::demo', array('first' => 1, 'third' => 'blah blah blah', 'second' => 2), 120)) . "\n";

// force cache regenerate
var_dump(PWN_Cacher::getContent('exmpl::demo', array('first' => 1, 'third' => 'blah blah blah', 'second' => 2), 7200, true)) . "\n";

// ignore cache
var_dump(PWN_Cacher::getContent('exmpl::demo', array('first' => 1, 'third' => 'blah blah blah', 'second' => 2), -1)) . "\n";

*/

