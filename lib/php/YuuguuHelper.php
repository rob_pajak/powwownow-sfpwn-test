<?php
/**
 * Helper methods for working with Yuuguu
 *
 * @author Asfer
 */
class YuuguuHelper {

    // Yuuguu API Request
    // private static $request;

    /**
     * Do Yuuguu Registration (Taken from TS::PWN_MYPWN_App.php) (Non Cached)
     *
     * @param  string  $email
     * @param  integer $service_ref
     * @param  string $password
     * @return array $result - Hermes Result or Empty Array
     * @author Asfer Tamimi
     *
     */    
    public static function doYuuguuRegistrationLegacy ($email, $service_ref, $password) {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('doYuuguuRegistrationLegacy',array(
                'email'       => $email,
                'service_ref' => $service_ref,
                'password'    => $password
            ));
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('doYuuguuRegistrationLegacy Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('doYuuguuRegistrationLegacy Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Email: ' . $email . ', Service Ref: ' . $service_ref . ', Password: ' . $password . ', tried to doYuuguuRegistrationLegacy but an Error Occurred. Error Message: ' . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode());
        }
        return $result;
    }

    /**
     * Register for Yuuguu. 
     * Do is some funky logic for it, and it will probably be used in a few places on website.
     *
     * @param  integer $contact_ref
     * @param  string  $first_name
     * @param  string  $last_name
     * @param  string  $email
     * @param  integer $service_ref
     * @param  string  $password
     * @return array ('result' => $result, 'error' => $error) - Return Both Success and Error
     * @author Asfer Tamimi
     *
     */
    public static function registerYuuguu($contact_ref, $first_name, $last_name, $email, $service_ref, $password) {
        $errorMessages = array();
        $result        = array();
        $status        = false;

        // Register with Yuuguu - Step 1a - Check if the Contact Exists, using the Password as False
        $resultStep1a = self::doYuuguuRegistrationLegacy($email,$service_ref,false);

        // Register with Yuuguu - Step 1b - Not Found from Step 1a
        if (isset($resultStep1a['status']) && $resultStep1a['status'] == 'not_found') {
            $resultStep1b = self::doYuuguuRegistrationLegacy($email,$service_ref,$password);
        }

        // Register with Yuuguu - Step 2a - Success from Step 1a
        if (
            (isset($resultStep1a['status']) && $resultStep1a['status'] == 'success')
            ||
            (isset($resultStep1b['status']) && $resultStep1b['status'] == 'success')
            ) {
            $resultStep2   = self::registerYuuguuStep2($contact_ref,$first_name,$last_name);
            $success       = 'success';
            $result        = (isset($resultStep2['message']) && '' == $resultStep2['message']) ? $resultStep2 : $result;
            $errorMessages = (empty($result)) ? $resultStep2 : $errorMessages;
        
        // Register with Yuuguu - Step 2b - All Other Status Codes from Step 1a
        } elseif (isset($resultStep1a['status']) && $status != 'success') {
            $status = (isset($resultStep1b['status'])) ? $resultStep1b['status'] : $resultStep1a['status'];
            if ($status == 'denied') {
                sfContext::getInstance()->getLogger()->err('Denied Error was shown for this user');
                $errorMessages = array('message' => 'API_RESPONSE_WEB_CONFERENCE_STATUS_DENIED', 'field_name' => 'alert');
            } elseif ($status == 'not_found') {
                sfContext::getInstance()->getLogger()->err('User was not found for Contact Ref: ' . $contact_ref);
                $errorMessages = array('message' => 'FORM_VALIDATION_PASSWORD_MISMATCH', 'field_name' => 'webConferencing[password]');
            } else {
                sfContext::getInstance()->getLogger()->err('Generic Error Shown for Status: ' . $status . ', For Contact Ref: ' . $contact_ref);
                $errorMessages = array('message' => 'API_RESPONSE_WEB_CONFERENCE_ERROR', 'field_name' => 'alert');
            }
        }

        return array('result' => $result, 'error' => $errorMessages);
    }

    /**
     * Register for Yuuguu. 
     * Step 2a - Update Contact
     *
     * @param  integer $contact_ref
     * @param  string  $first_name
     * @param  string  $last_name
     * @return array
     * @author Asfer Tamimi
     *
     */
    protected static function registerYuuguuStep2($contact_ref, $first_name, $last_name) {
        $result = Common::updateContact(array(
            'contact_ref' => $contact_ref,
            'hasYuuguu'   => 'Y',
            'first_name'  => $first_name,
            'last_name'   => $last_name
        ));
        if (isset($result['statusCode']) && $result['statusCode'] != 202) {
            sfContext::getInstance()->getLogger()->err('Contact Record Was Not Updated. Contact Ref: ' . $contact_ref);
            $result = array('message' => 'FORM_UPDATE_CONTACT_ERROR', 'field_name' => 'alert');
        } else {
            $result = array('message' => '', 'field_name' => 'alert');
            sfContext::getInstance()->getUser()->setAttribute('first_name',$first_name);
            sfContext::getInstance()->getUser()->setAttribute('last_name',$last_name);            
        }
        return $result;
    }

//     /**
//      * Connect to Yuuguu API (Most Code Obtained from php-models::YuuguuApi)
//      * 
//      * @param string   $email
//      * @return $object $request
//      * @author Unknown + Asfer Tamimi
//      *
//      */
//     public static function createYuuguuRequest($email) {
//         $yuuguuDetails = sfConfig::get('app_yuuguu');
//         self::$request = new CurlHttp(
//             $yuuguuDetails['apiServer'] . ($email) ? '/' . urlencode($email) : '',
//             $yuuguuDetails['ssl_client_cert'],
//             $yuuguuDetails['ssl_key'],
//             $yuuguuDetails['ssl_server_cert']
//         );
//     }

//     /**
//      * Delete a User from Yuuguu
//      *
//      * @param string $email
//      * @param object $request
//      * @return string
//      * @author Unknown + Asfer Tamimi
//      *
//      */
//     public static function deleteYuuguuUser($email) {
//         self::createYuuguuRequest($email);
//         $result = self::yuuguuRequest(array(),true);
//         list($code,$body) = $result;
//         print_r($result);
//         switch ($code) {
//             case 204:       return 'success';
//             case 404:       return 'not_found';
//             case 403:       return 'denied';
//             default:        return 'error' . $code;
//         }
//     }

//     /**
//      * Make a Yuuguu Request
//      *
//      * @param array $args
//      * @param bool  $delete
//      * @return array
//      * @author Unknown + Asfer Tamimi
//      *
//      */
//     private static function yuuguuRequest($args,$delete = false) {
//         self::$request->setMethod($delete ? HTTP_REQUEST_METHOD_DELETE : HTTP_REQUEST_METHOD_POST);
//         self::$request->setCURLOption(CURLOPT_VERBOSE,true);
//         self::$request->addHeader('Content-type','application/x-www-form-urlencoded');
        
//         $body = '';
//         if (is_array($args)) {
//             foreach ($args as $k => $v) {
//                 $body .= ($body?'&':'')."$k=".urlencode($v);
//             }
//         } else {
//             return array(0,'No input to send? '.print_r($args,true));
//         }
        
//         self::$request->setBody($body);
//         self::$request->sendRequest();
        
//         $code = self::$request->getResponseCode();
        
//         return array($code,$body . ($code?'':' API returns: ' . self::$request->getResponseBody()));
//     }
}