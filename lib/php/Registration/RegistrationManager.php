<?php

class RegistrationManager
{
    /**
     * @var RegistrationStrategyAbstract
     */
    private $registrationStrategy;

    /**
     * @param $source
     */
    public function __construct($source)
    {
        $this->loadStrategy($source);
    }

    /**
     * @param $source
     */
    private function loadStrategy($source)
    {
        switch($source) {
            case 'GBR-Mobile':
                $this->registrationStrategy = new MobileRegistrationStrategy($source);
                break;
            default:
                $this->registrationStrategy = new DefaultRegistrationStrategy($source);
                break;
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function isValid(array $data)
    {
        return $this->registrationStrategy->isValid($data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function doRegistration(array $data)
    {
        return $this->registrationStrategy->doRegistration($data);
    }
}