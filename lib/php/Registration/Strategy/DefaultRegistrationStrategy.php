<?php

class DefaultRegistrationStrategy extends RegistrationStrategyAbstract
{
    /**
     * Return url in format:
     * array(
     *   array(
     *    'url' => '@cx2_post_registration_version_h1',
     *    'weight' => 1
     *   ),
     *    array(
     *     'url' => '@cx2_post_registration_version_h2',
     *     'weight' => 3
     *   )
     * )
     *
     * @var array
     */
    protected $returnUrls = array(
        array(
            'url' => '@cx2_post_registration_version_h1',
            'weight' => 1
        )
    );

    /**
     * Valid registration params
     *
     * @param array $data
     * @return bool
     */
    public function isValid(array $data)
    {
        if (!isset($data['email']) && !$this->isValidEmail($data['email'])) {
            return false;
        }
        return true;
    }

    /**
     * Validates email address
     *
     * @param $email
     * @return bool
     */
    private function isValidEmail($email)
    {
        if (!is_string($email) || strlen($email) > 50) {
            return false;
        }

        // Symfony email validation.
        $validationGenerator = new validationGroupFactory();
        $validator           = $validationGenerator->email();
        try {
            $validator->clean($email);
        } catch (sfValidatorError $e) {
            return false;
        }
        return true;
    }
}