<?php

/**
 * Class RegistrationStrategyAbstract
 *
 * Abstract class for each registration strategy. This class contains
 * hermes calls, validation and mechanism to do split test for return urls.
 *
 */
abstract class RegistrationStrategyAbstract
{
    /**
     * Return url in format:
     * array(
     *   array(
     *    'url' => '@cx2_post_registration_version_h1',
     *    'weight' => 1
     *   ),
     *    array(
     *     'url' => '@cx2_post_registration_version_h2',
     *     'weight' => 3
     *   )
     * )
     *
     * @var array
     */
    protected $returnUrls = array();

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * @var string
     */
    protected $source;

    /**
     * @param string $source
     */
    public function __construct($source)
    {
        $this->source = $source;
    }

    abstract public function isValid(array $data);

    /**
     * Return url from $this->returnUrs array,
     * if there is more than one element it returns
     * url based on sequence and weight
     *
     * @return string|null
     */
    public function getReturnUrl()
    {
        if (count($this->returnUrls) > 0) {
            if (count($this->returnUrls) == 1) {
                return $this->returnUrls[0]['url'];
            } else {
                $urlWeight = array();
                foreach ($this->returnUrls as $row) {
                    for ($i = 0;$i < $row['weight'];$i++) {
                        $urlWeight[] = $row['url'];
                    }
                }
                $currentVariant = apc_fetch(get_class($this) . '_registrationCounter');
                if ($currentVariant === false) {
                    $currentVariant = 0;
                }

                $currentVariant = ($currentVariant) % (count($urlWeight));
                apc_store(get_class($this) . '_registrationCounter', $currentVariant  + 1);
                return $urlWeight[$currentVariant];
            }
        }
        return null;
    }

    /**
     * @param array $data
     * @return array
     */
    public function doRegistration(array $data)
    {
        $this->errors = array();

        if (!$this->isValid($data)) {
            return array('error_messages' => $this->errors);
        }

        $response = $this->doHermesCall(array(
            'email'  => $data['email'],
            'locale' => 'en_GB',
            'source' => $this->source
        ));

        if (!$response) {
            return array('error_messages' => $this->errors);
        }

        $this->saveGoalReferrers($response);
        $shomeiResponse = $this->createShomeiRecord($response);

        if (!$this->loginUser($data['email'], $response['password'])) {
            return array('error_messages' => $this->errors);
        }

        return array(
            'response' => $response,
            'return_url' => $this->getReturnUrl(),
            'shomei_response' => $shomeiResponse
        );
    }

    /**
     * @param $data
     * @return array|bool
     */
    protected function doHermesCall($data)
    {
        try {
            return Hermes_Client_Rest::call('Default.newDoFullRegistrationGenerateRandomPassword',$data);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $this->logError('doFullRegistrationGenerateRandomPassword Hermes call has timed out. Email: ' .$data['email']);
            $this->errors[] = array('message' => 'FORM_COMMUNICATION_ERROR','field_name' => 'alert');
        } catch (Hermes_Client_Exception $e) {
            switch ($e->getCode()) {
                case '001:000:001': //premium, plus user
                    $this->errors[] = array('message' => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_OTHER');
                    break;
                case '001:000:064': //free user
                    $this->errors[] = array('message' => 'API_RESPONSE_EMAIL_ALREADY_TAKEN','field_name' => 'email');
                    break;
                case '001:000:007':
                    $this->errors[] = array('message' => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE','field_name' => 'alert');
                    break;
                default:
                    $this->errors[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }
        }
        return false;
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     */
    protected function loginUser($email,$password)
    {
        try {
            Plus_Authenticate::logIn($email, $password);
        } catch (Exception $e) {
            $this->logError('Exception occurred when authenticating contact: '. $email . ' ' . $e->getError(), 'err');
            $this->errors[] = array('message' => 'FORM_RESPONSE_LOG_IN_UNABLE', 'field_name' => 'email');
            return false;
        }
        return true;
    }

    /**
     * @param array $response
     */
    protected function saveGoalReferrers(array $response)
    {
        try {
            if (!empty($response['pin']['pin_ref'])) {
                PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                PWN_Logger::log(
                    'pin:' . $response['pin']['pin_ref']. ' goal: conferencing ' . __FILE__ . '[' . __LINE__ . ']',
                    PWN_Logger::INFO,
                    'trackerLog'
                );
                PWN_Tracking_GoalReferrers::achieved('conferencing', $response['pin']['pin_ref'], true);
            } else {
                $this->logError('After a registration, pin ref was null: ' . serialize($response));
            }
        } catch (Exception $e) {
            $this->logError('Could not track goal referrers: ' . serialize($e));
        }
    }

    /**
     * Create Shomei Record
     * @param array $response
     * @return array
     */
    protected function createShomeiRecord(array $response)
    {
        $shomei = new Shomei();
        $shomeiResult = $shomei->registerFreePin($response['pin']['pin_ref']);
        return array(
            'pin_ref_hash' => $shomeiResult['apiResult']['responsiveBody']['transactionIdentifier']
        );
    }

    /**
     * @param string $msg
     */
    protected function logError($msg)
    {
        sfContext::getInstance()->getLogger()->err($msg);
    }
}