<?php

class MobileRegistrationStrategy extends DefaultRegistrationStrategy
{
    protected $returnUrls = array(
        array(
            'url' => '@mobile_on_board_page',
            'weight' => 1
        )
    );
}
