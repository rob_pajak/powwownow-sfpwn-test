<?php

abstract class ProductCreator
{
    /**
     * @var myUser
     */
    protected $user;

    /**
     * Cached account id retrieved the user object.
     *
     * @var int
     */
    protected $accountId;

    /**
     * @var PinRetriever
     */
    protected $pinRetriever;

    /**
     * @var bool
     */
    protected $isPurchaseFromBundle;

    /**
     * The current payment in progress.
     *
     * @var PaymentSession
     */
    protected $session;

    /**
     * @param myUser
     * @param PinRetriever $pinRetriever
     * @param bool $isPurchaseFromBundle
     * @param PaymentSession $session = null
     */
    public function __construct(myUser $user, PinRetriever $pinRetriever, $isPurchaseFromBundle = false, PaymentSession $session = null)
    {
        $this->user = $user;
        $this->accountId = $user->getAccountId();
        $this->pinRetriever = $pinRetriever;
        $this->isPurchaseFromBundle = !empty($isPurchaseFromBundle);
        $this->session = $session;
    }

    protected function getPinRefsFromContactRefs($contactRefs)
    {
        $pinRefs = array();
        foreach ($contactRefs as $contactRef) {
            $contactPins = $this->pinRetriever->retrievePinsFromContact($contactRef);
            $pinRefs = array_merge($pinRefs, $contactPins);
        }
        return $pinRefs;
    }

    protected function getPaymentId()
    {
        if ($this->session) {
            return $this->session->getPaymentId();
        }
        return null;
    }

    /**
     * Creates and assigns the product.
     *
     * @param mixed $product
     * @param ProductCreationResult $result
     *   Result collection object.
     * @return ProductCreationResult
     *   The altered $result object.
     */
    abstract public function createAndAssign($product, ProductCreationResult $result);
}
