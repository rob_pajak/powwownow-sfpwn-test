<?php

class BundleCreator extends ProductCreator
{
    /**
     * @param AssignableBundleWrapper $bundleWrapper
     * @param ProductCreationResult $result
     * @return ProductCreationResult
     */
    public function createAndAssign($bundleWrapper, ProductCreationResult $result)
    {
        $bundlePins = $bundleWrapper->getPins();
        $bundleId = $bundleWrapper->getProductGroupIdForAssignment();
        $bundle = $bundleWrapper->getBundle();

        $assignmentResult = $this->assignBundleToAccount($this->accountId, $bundleId, $result);
        if (!$assignmentResult) {
            return $result;
        }

        $pinAssignmentResult = $this->assignBundleToAllPins($bundleId, $bundlePins, $result);
        if (!$pinAssignmentResult) {
            return $result;
        }

        // For Landline Bundles, assign all relevant LandLine feature products to all the pins of the account.
        if ($bundle->isLandLineBundle()) {
            $this->assignLandLineFeatures($bundle->hasAddon(), $result);
        }

        // Take payment, because we don't work for free.
        // Directly after, store a record of the purchase for the reports.
        $proRataCost = $bundle->calculateProRataCost();
        $this->chargeAdminForBundle($bundle, $proRataCost, $result);

        if ($bundle->hasAddon()) {
            $this->storeBundlePurchaseRecord($proRataCost, time(), $bundle->getAddon()->getProductGroupId(), $result);
        }
        else {
            $this->storeBundlePurchaseRecord($proRataCost, time(), $bundle->getProductGroupId(), $result);
        }

        $result->addProduct($bundle->getProductGroupId(), $bundle);
        return $result;
    }

    /**
     * Assigns the LandLine feature products to the PINs of the account.
     *
     * The account PINs include all admin PINs (which is overhead, because admins cannot disable these products),
     * and all PINs of the users linked to the account.
     *
     * @param bool $includeWorldWide
     *   In addition to the UK LandLine feature, also includes the WorldWide LandLine feature.
     * @param ProductCreationResult $result
     */
    private function assignLandLineFeatures($includeWorldWide, ProductCreationResult $result)
    {
        $contactRef = $this->user->getContactRef();
        $features = array(sfConfig::get('app_landline_feature_products_uk'));
        if ($includeWorldWide) {
            $features[] = sfConfig::get('app_landline_feature_products_worldwide');
        }

        try {
            plusCommon::assignAllProductsToAllPinsOfContact($contactRef, $features);
        } catch (Exception $e) {
            $result->addErrorMessage('Unable to assign all relevent LandLine features to the PINs of your account.');
        }
    }

    /**
     * Makes a call to Bundle.assignNewBundle to assign the bundle to the account and mark the account as post pay.
     *
     * @param int $accountId
     * @param int $bundleId
     * @param ProductCreationResult $result
     * @return array
     *   Returns the altered $result array
     */
    private function assignBundleToAccount($accountId, $bundleId, ProductCreationResult $result)
    {
        try {
            Hermes_Client_Rest::call(
                'Bundle.assignNewBundle', array(
                    'account_id' => $accountId,
                    'product_group_id' => $bundleId,
                )
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            $result->addErrorMessage('Assignment call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            $result->addErrorMessage($this->addErrorMessageFromHermesCode($e->getCode()));
        } catch (Exception $e) {
            $this->addDefaultErrorMessage($result);
        }
        return $result->isSuccessful();
    }

    /**
     * Assigns the bundle to all given pins.
     *
     * @param int $bundleId
     * @param array $pinRefs
     * @param ProductCreationResult $result
     * @return ProductCreationResult
     *   Returns the altered $result object.
     */
    private function assignBundleToAllPins($bundleId, array $pinRefs, ProductCreationResult $result)
    {
        $successfullyAssignedPins = array();
        foreach ($pinRefs as $pinRef) {
            try {
                Hermes_Client_Rest::call(
                    'Default.updatePlusPinProductGroups', array(
                        'pin_ref' => $pinRef,
                        'assign_product_group_ids' => array($bundleId),
                        'use_transactions' => false
                    )
                );
                $successfullyAssignedPins[] = $pinRef;
            } catch (Hermes_Client_Exception $e) {
                $result->addErrorMessage('We were unable to assign the bundle to PIN with pin reference ' . $pinRef);
            } catch (Exception $e) {
                $this->addDefaultErrorMessage($result);
            }
        }
        $result->addPinsAssignedToProduct($successfullyAssignedPins, $bundleId);
        return $result->isSuccessful();
    }

    private function chargeAdminForBundle(Bundle $bundle, $proRataCharge, ProductCreationResult $result)
    {
        try {
            Hermes_Client_Rest::call(
                'Bundle.chargeProRataCostForBundle',
                array(
                    'account_id' => $this->accountId,
                    'bundle_cost' => $proRataCharge,
                    'purchase_from_balance' => $this->isPurchaseFromBundle
                )
            );
        } catch (Hermes_Client_Exception $e) {
            $result->addErrorMessage('We were unable to charge £' . $proRataCharge . ' to the account with ID ' . $this->accountId . ' for the bundle ' . $bundle->getGroupName());
        } catch (Exception $e) {
            $this->addDefaultErrorMessage($result);
        }
    }

    /**
     * Stores a record of the purchase for reports and reference.
     *
     * @param float $proRataCharge
     * @param int $timeOfPurchase
     * @param int $productGroupId
     * @param ProductCreationResult $result
     * @author Maarten Jacobs
     */
    private function storeBundlePurchaseRecord($proRataCharge, $timeOfPurchase, $productGroupId, ProductCreationResult $result)
    {
        try {
            Hermes_Client_Rest::call(
                'Bundle.storeBundlePurchaseRecord',
                array(
                    'account_id' => $this->accountId,
                    'bundle_cost' => $proRataCharge,
                    'time_of_purchase' => $timeOfPurchase,
                    'bundle_product_group_id' => $productGroupId,
                )
            );
        } catch (Hermes_Client_Exception $e) {
            $result->addErrorMessage('Unable to store a record of your purchase, but the purchase has gone through.');
        } catch (Exception $e) {
            $this->addDefaultErrorMessage($result);
        }
    }

    private function addErrorMessageFromHermesCode($code)
    {
        switch ($code) {
            case '000:003:001':
                return 'An invalid parameter was passed.';
            case '001:025:002':
                return 'The account already has a bundle assigned.';
            case '001:025:003':
                return 'The requested bundle was not found.';
            case '001:025:004':
                return 'The account could not be marked as post pay customer.';
        }
        return $this->getDefaultErrorMessage();
    }

    private function getDefaultErrorMessage()
    {
        return 'An unidentified error has occurred.';
    }

    private function addDefaultErrorMessage(ProductCreationResult $result)
    {
        $result->addErrorMessage($this->getDefaultErrorMessage());
    }
}
