<?php

/**
 * Shared functions for controllers to work on the basket.
 *
 * The Basket is a collection of 3 lists:
 *  - basket: which are, for the moment, only the newly configured BWMs. They are mapped by a temporary id starting with
 *    'new-bwm-' followed by an integer (mostly the time). The data assigned depends on the stage of the flow.
 *  - basket-added: the collection of items selected on the Select-Products page that were not previously assigned to
 *    the account. These include the items in the basket.
 *  - basket-removed: the collection of items selected on the Select-Products that were previously assigned to the account,
 *    but were not selected for activation.
 *
 * @author Maarten Jacobs
 */
class Basket
{
    static public $vatFactor = 1.2;
    /**
     * The user session.
     *
     * @var sfUser
     */
    protected $user;

    /**
     * A list of bundle objects stored in the basket.
     * Both the array representation and object representation is tracked, but it is preferred to return the object.
     *
     * @var array
     */
    private $bundleObjects = array();

    private $proRataKey = 'prorata_charge';

    /**
     * @param sfUser $user
     * @param bool $recalculateProRata
     */
    public function __construct($user, $recalculateProRata = true)
    {
        $this->user = $user;
        if ($recalculateProRata) {
            $this->recalculateProRataCharge();
        }
    }

    private function recalculateProRataCharge()
    {
        $addedProducts = $this->retrieveAddedProducts();
        foreach ($addedProducts as $productId => &$product) {
            $bundle = $this->retrieveBundleObject($productId);
            if ($bundle) {
                $product[$this->proRataKey] = $bundle->calculateProRataCost();
            }
        }
        unset($product);
        $this->storeAddedProducts($addedProducts);
    }

    /**
     * Returns the current list of added items.
     *
     * @return array
     */
    public function retrieveAddedProducts()
    {
        $products = $this->user->getAttribute('basket-added', array());
        if (!is_array($products)) {
            $products = $this->clearAddedItems();
        } else {
            $this->retrieveAndStoreBundleObjects($products);
        }
        return $products;
    }

    /**
     * Returns a product in the basket by its ids
     * @param int $id product Id
     * @return array
     */
    public function retrieveAddedProductById($id)
    {
        $products = $this->retrieveAddedProducts();

        if (array_key_exists($id, $products)) {
            return $products[$id];
        }

        return null;
    }

    /**
     * Returns the current list of removed items.
     *
     * @return array
     */
    public function retrieveRemovedProducts()
    {
        $products = $this->user->getAttribute('basket-removed', array());
        if (!is_array($products)) {
            $products = $this->clearRemovedItems();
        }
        return $products;
    }

    /**
     * Resets the basket container and returns the new empty list.
     *
     * @param string $key
     * @return array
     */
    protected function clearBasketContainer($key)
    {
        $basket = array();
        $this->user->setAttribute($key, $basket);
        return $basket;
    }

    /**
     * Stores the added products.
     *
     * @param array $addedProducts
     */
    public function storeAddedProducts(array $addedProducts)
    {
        $this->user->setAttribute('basket-added', $addedProducts);
    }

    /**
     * Stores the removed products.
     *
     * @param array $removedProducts
     */
    public function storeRemovedProducts(array $removedProducts)
    {
        $this->user->setAttribute('basket-removed', $removedProducts);
    }

    /**
     * Clears the list of added products.
     *
     * @return array
     */
    public function clearAddedItems()
    {
        return $this->clearBasketContainer('basket-added');
    }

    /**
     * Clears the list of removed products.
     *
     * @return array
     */
    public function clearRemovedItems()
    {
        return $this->clearBasketContainer('basket-removed');
    }

    /**
     * Stores a product to be "added" (or "assigned") to a number of pins at the end of the basket flow.
     * 
     * @param int $productId
     * @param array $productAttr
     */
    public function addProduct($productId, array $productAttr = array()) {
        $productId = (int) $productId;

        $addedItems = $this->retrieveAddedProducts();
        $addedItems[$productId] = $productAttr;
        $this->storeAddedProducts($addedItems);
    }

    /**
     * Adds a non-existing product (i.e. no product id defined) to the basket.
     *
     * @param array $productAttr
     * @return int
     *   The new product ID.
     */
    public function addNonExistingProduct(array $productAttr = array()) {
        $productId = $this->nextNonExistingProductId();
        $this->addProduct($productId, $productAttr);
        return $productId;
    }

    /**
     * Validate amount of credit purchase with BWM
     *
     * @param float $amount
     *
     * @return boolean
     *
     * @author Michal Macierzynski
     */
    public function validateBWMCreditAmount($amount) {
        return $amount >= 5 && $amount <= 4166;
    }

    /**
     * Validate amount of credit purchase without BWM
     *
     * @param float $amount
     *
     * @return boolean
     *
     * @author Michal Macierzynski
     */
    public function validateCreditAmount($amount) {
        $minimumAmount = ProductHelper::retrieveMinimumCallCreditAmount($this);
        return $amount >= $minimumAmount && $amount <= 4166;
    }

    /**
     * Calculates the next available product id for non-existing products.
     *
     * @return int
     */
    protected function nextNonExistingProductId() {
        $productKeys = array_keys($this->retrieveAddedProducts());
        sort($productKeys, SORT_NUMERIC);
        if (!$productKeys || !$this->isNonExistingKey($productKeys[0])) {
            return -1;
        } else {
            return $productKeys[0] - 1;
        }
    }

    /**
     * Retrieves all data linked to non-existing products.
     *
     * @param bool $nonBwm
     * @return array
     */
    public function retrieveNonExistingProducts($nonBwm=false) {
        $addedProducts = $this->retrieveAddedProducts();
        $productKeys = array_filter(array_keys($addedProducts), array($this, 'isNonExistingKey'));
        $nonExistingProducts = array();
        foreach ($productKeys as $productKey) {
            // here skips non bwm products or bwm products depends of parameter
            if (
                ($nonBwm && isset($addedProducts[$productKey][1]['BWM_total_cost']))
                || (!$nonBwm && !isset($addedProducts[$productKey][1]['BWM_total_cost']))
            ) continue;
            $nonExistingProducts[$productKey] = $addedProducts[$productKey];
        }

        return $nonExistingProducts;
    }

    /**
     * Returns if basket has call credit added
     * @return bool
     */
    public function hasCallCreditBeenAdded()
    {
        return (bool) $this->retrieveNonExistingProducts(true);
    }

    public function retrieveCallCreditProducts()
    {
        return array_filter($this->retrieveNonExistingProducts(true), array($this, 'isCallCreditType'));
    }

    protected function isCallCreditType($value)
    {
        return isset($value['type']) && $value['type'] === 'credit';
    }

    /**
     * @param $key
     * @return bool
     */
    protected function isNonExistingKey($key) {
        return $key < 0;
    }

    /**
     * Stores a product to be "removed" (or "unassigned") from the account at the end of the basket flow.
     *
     * @param int $productId
     */
    public function addProductRemoval($productId) {
        $productId = (int) $productId;

        $removedItems = $this->retrieveRemovedProducts();
        $removedItems[$productId] = array();
        $this->storeRemovedProducts($removedItems);
    }

    /**
     * Assert that the total number of added/removed products is zero.
     *
     * @return bool
     */
    public function isEmpty() {
        $addedProductsCount = count($this->retrieveAddedProducts());
        $removedProductsCount = count($this->retrieveRemovedProducts());
        $totalCount = $addedProductsCount + $removedProductsCount;

        return $totalCount === 0;
    }

    /**
     * Clears all basket stores.
     */
    public function clearBasket() {
        $this->clearAddedItems();
        $this->clearRemovedItems();
    }

    /**
     * Assert that pins have been assigned to every added product.
     *
     * @return bool
     */
    public function arePinsAssigned() {
        $addedProducts = $this->retrieveAddedProducts();
        foreach ($addedProducts as $product) {
            if (!isset($product['pins'])) {
                return false;
            }
        }
        // If we reached this point, then all added products have pins assigned.
        return true;
    }

    /**
     * Assert that the basket contains added products.
     *
     * @return bool
     */
    public function hasAddedProducts()
    {
        return count($this->retrieveAddedProducts()) > 0;
    }

    /**
     * Assert that the basket contains removed products.
     *
     * @return bool
     */
    public function hasRemovedProducts()
    {
        return count($this->retrieveRemovedProducts()) > 0;
    }

    /**
     * Assert that the product has been marked for addition.
     *
     * @param int $productId
     * @return bool
     */
    public function hasProductBeenMarkedForAddition($productId)
    {
        $addedProducts = $this->retrieveAddedProducts();
        return isset($addedProducts[$productId]);
    }

    /**
     * Assert that the product has been marked for removal.
     *
     * @param int $productId
     * @return bool
     */
    public function hasProductBeenMarkedForRemoval($productId)
    {
        $removedProducts = $this->retrieveRemovedProducts();
        return isset($removedProducts[$productId]);
    }

    /**
     * Calculates the total cost of the basket.
     *
     * @return float
     */
    public function calculateTotalCost() {
        $addedProducts = $this->retrieveAddedProducts();
        $total = 0.0;

        foreach ($addedProducts as $productId => $addedProduct) {
            // Currently, only BWMs have a cost price.
            if (!empty($addedProduct[1]['BWM_total_cost']['total']) && isset($addedProduct[1]['BWM_total_cost']['connectionFee'])) {
                // Add total DNISes cost.
                $total += (float) $addedProduct[1]['BWM_total_cost']['total'];
                // Add connection fee cost.
                $total += (float) $addedProduct[1]['BWM_total_cost']['connectionFee'];
            } elseif (isset($addedProduct['amount'])) {
                $total += (float) $addedProduct['amount'];
            } elseif (isset($this->bundleObjects[$productId])) {
                $bundle = $this->retrieveBundleObject($productId);
                $total += (float) $bundle->calculateProRataCost();
            }
        }

        return $total;
    }

    /**
     * Removes an added product from the basket.
     *
     * @param int $productId
     * @return bool
     */
    public function removeAddedProduct($productId) {
        $addedProducts = $this->retrieveAddedProducts();

        // Check if we can remove and do so if possible.
        if (isset($addedProducts[$productId])) {
            unset($addedProducts[$productId]);
        } else {
            // In case of unfound product, return a boolean to denote status.
            return false;
        }

        // Store changes to basket.
        $this->storeAddedProducts($addedProducts);

        return true;
    }

    /**
     * Ascertain that BWMs have been added.
     *
     * @return bool
     */
    public function hasBWMBeenAdded() {
        return count($this->retrieveNonExistingProducts()) > 0;
    }

    /**
     * Assert the existence of Call Credit worth less than 20 pounds.
     *
     * @return bool
     */
    public function hasCallCreditOfLessThanMinimumAmountBeenAdded()
    {
        $creditProducts = array_filter($this->retrieveNonExistingProducts(true), array($this, 'isCallCreditWorthLessThanMinimumAmount'));
        return !empty($creditProducts);
    }

    /**
     * Removes all Call Credit products with less than
     *
     * @return array
     */
    public function removeCallCreditsWorthLessThanMinimumAmount()
    {
        $creditProducts = array_filter($this->retrieveNonExistingProducts(true), array($this, 'isCallCreditWorthLessThanMinimumAmount'));
        foreach ($creditProducts as $creditId => $credit) {
            $this->removeAddedProduct($creditId);
        }
        return array_keys($creditProducts);
    }

    public function isCallCreditWorthLessThanMinimumAmount(array $callCredit)
    {
        $minimumAmount = sfConfig::get('app_callCredit_minimumAmount', 5);
        return isset($callCredit['amount']) && $callCredit['amount'] < $minimumAmount;
    }

    /**
     * Retrieves the first bundle found.
     *
     * @return bool|array
     */
    public function retrieveAddedBundle()
    {
        foreach ($this->retrieveAddedProducts() as $product) {
            if (isset($product['second_allowance'])) {
                return $product;
            }
        }
        return false;
    }

    /**
     * Retrieves the first bundle object found.
     *
     * @return bool|Bundle
     */
    public function retrieveAddedBundleObject()
    {
        return reset($this->bundleObjects);
    }

    /**
     * Retrieves all product ids from Bundle products in the basket.
     *
     * @return array
     */
    public function retrieveBundleProductIds()
    {
        return array_keys($this->bundleObjects);
    }

    /**
     * Checks if a Bundle product has been added to the basket.
     *
     * @return bool
     */
    public function hasBundleBeenAdded()
    {
        $bundleIds = $this->retrieveBundleProductIds();
        return !empty($bundleIds);
    }

    /**
     * Adds a bundle product to the basket.
     *
     * @param int $bundleId
     * @param array $bundleInformation
     * @param bool $agreedToTAndC
     * @param bool $skeleton
     * @return $this
     */
    public function addBundle($bundleId, array $bundleInformation, $agreedToTAndC = true, $skeleton = false)
    {
        $bundle = $this->storeBundleObject($bundleId, $bundleInformation, $agreedToTAndC, $skeleton);
        $bundleInformation['prorata_charge'] = $bundle->calculateProRataCost();
        $bundleInformation['agreed_to_t_and_c'] = $agreedToTAndC;
        $bundleInformation['skeleton'] = $skeleton;
        $this->addProduct($bundleId, $bundleInformation);
        return $this;
    }

    /**
     * Adds the bare-minimum of bundle construct to the Basket.
     *
     * @param int $bundleId
     * @param array $bundleInformation
     * @return $this
     */
    public function addBundleSkeleton($bundleId, array $bundleInformation)
    {
        return $this->addBundle($bundleId, $bundleInformation, false, true);
    }

    /**
     * Mark the Bundle Terms and Conditions as agreed.
     *
     * @param int $bundleId
     * @return $this
     */
    public function agreeToBundleTAndC($bundleId)
    {
        $bundle = $this->retrieveAddedProductById($bundleId);
        $bundle['agreed_to_t_and_c'] = true;
        $this->addProduct($bundleId, $bundle);
        return $this;
    }

    /**
     * Upgrades a bundle with the given add-on.
     *
     * @param int $bundleId
     * @param array $addon
     * @return $this
     */
    public function upgradeBundle($bundleId, array $addon)
    {
        $bundleObject = $this->retrieveAddedBundleObject();
        $bundleObject->upgradeBundle($addon);
        $this->bundleObjects[$bundleId] = $bundleObject;

        $bundle = $this->retrieveAddedProductById($bundleId);
        $bundle['addons'] = array($addon);
        $this->addProduct($bundleId, $bundle);

        return $bundleObject->getAddon();
    }

    public function selectPinPairForAYCMBundle($bundleId, array $pinPair)
    {
        $bundle = $this->retrieveAddedProductById($bundleId);
        $bundle['pins'] = $pinPair;
        $this->addProduct($bundleId, $bundle);
        return $this;
    }

    /**
     * Checks if there are any bundles in the basket which are not ready for checkout.
     *
     * @return bool
     */
    public function hasInvalidBundles()
    {
        $bundleObject = $this->retrieveAddedBundleObject();
        return $bundleObject && $bundleObject->isSkeleton() && !$bundleObject->hasAgreedToTAndC();
    }

    /**
     * Retrieves the calculated pro-rata charge of the bundle.
     *
     * @param int $bundleId
     * @return float|bool
     */
    public function getBundleProRataCharge($bundleId)
    {
        $bundle = $this->retrieveBundleObject($bundleId);
        if (!$bundle) {
            return false;
        }
        return $bundle->calculateProRataCost();
    }

    /**
     * Removes all Call Credit products from the basket, and returns the product ids of the removed products.
     *
     * @return array
     */
    public function removeCallCreditProducts()
    {
        $callCreditProducts = $this->retrieveCallCreditProducts();
        $removedIds = array_keys($callCreditProducts);
        foreach ($removedIds as $id) {
            $this->removeAddedProduct($id);
        }
        return $removedIds;
    }

    /**
     * @param $bundleId
     * @return bool|Bundle
     */
    public function retrieveBundleObject($bundleId)
    {
        if (isset($this->bundleObjects[$bundleId])) {
            return $this->bundleObjects[$bundleId];
        }
        return false;
    }

    private function retrieveAndStoreBundleObjects(array $addedProducts)
    {
        foreach ($addedProducts as $productId => $product) {
            if (isset($product['second_allowance'])) {
                $this->storeBundleObject($productId, $product);
            }
        }
        return $this;
    }

    /**
     * @param int $bundleId
     * @param array $bundleRow
     * @param bool $agreedToTAndC
     * @param bool $skeleton
     * @return Bundle
     */
    private function storeBundleObject($bundleId, array $bundleRow, $agreedToTAndC = true, $skeleton = false)
    {
        $this->bundleObjects[$bundleId] = new Bundle($bundleRow, $agreedToTAndC, $skeleton);
        return $this->bundleObjects[$bundleId];
    }

    /**
     * create items for invoice
     *
     * @return array
     */
    public function getInvoiceItems($payment_currency) {
        $invoiceItems = array();
        if($this->hasBundleBeenAdded()) {
            $bundle = $this->retrieveAddedBundle();
            $invoiceItems[] = array(
                'order_no'   => 0,
                'item'       => $bundle['group_name'] . ' bundle.',
                'amount_net' => $bundle['prorata_charge'],
                'currency'   => $payment_currency,
            );

        }
        if($this->hasBWMBeenAdded()) {
            $BWMs = $this->retrieveNonExistingProducts();
            foreach($BWMs as $item) {
                $invoiceItems[] = array(
                    'order_no'   => 0,
                    'item'       => $item[0]['bwm_label'],
                    'amount_net' => $item[1]['BWM_total_cost']['total'] + $item[1]['BWM_total_cost']['connectionFee'],
                    'currency'   => $payment_currency,
                );
            }
        }
        $nonBWMs = $this->retrieveNonExistingProducts(true);
        foreach($nonBWMs as $item) {
            $invoiceItems[] = array(
                'order_no'   => 0,
                'item'       => $item['label'],
                'amount_net' => $item['amount'],
                'currency'   => $payment_currency,
            );
        }

        return $invoiceItems;
    }
}
