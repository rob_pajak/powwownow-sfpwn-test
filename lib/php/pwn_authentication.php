<?php

require_once('IpRetriever.php');

/**
 * Class to control the Authentication in Symfony
 *
 * @author Asfer Tamimi
 * @todo The Whole thing. DO NOT use this Class
 *
 */
class PWN_Authentication extends sfUser implements sfSecurityUser {

    /**
     * Logs a user in by checking their name and password. If user Successfully Logs in, Set the Credentials
     *
     * @param string $email
     * @param string $password
     * @return array
     * @throws Exception
     * @author Asfer Tamimi
     *
     */
    public function login($email, $password) {
        try {
            // Check Input
            if (empty($email) || empty($password) || is_null($email) || is_null($password)) {
                throw new Exception('Email or Password Variables are not Valid. Email: ' . $email . ', Password: ' . $password);
            }

            $ipRetriever = new IpRetriever();
            $ipAddress   = $ipRetriever->retrieveIPAddress();

            // Authenticate Contact
            $result = Hermes_Client_Rest::call('authenticateContact', array(
                'email'      => $email,
                'password'   => $password,
                'ip_address' => $ipAddress,
            ));

            // Check Result
            if (!$result || empty($result) || empty($result['contact_ref'])) {
                // Authentication Failed
                sfContext::getInstance()->getLogger()->err('Authentication Failed');
                return array();
            } else {
                // Authentication Success
                sfContext::getInstance()->getLogger()->err('Authentication Success');

                // Clear Current Credentials by Logging Out
                $this->logout();

                // Start New Session
                $sessionStatus = $this->startSession();
                if (!$sessionStatus) {
                    throw new Exception('Authenticate::Session Failed to Start for Email: ' . $email);
                }

                // Set Credentials
                $result['email'] = $email;
                return $this->setPWNCredentials($result);
            }

        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Login Authentication Exception Called: ' . $e->getResponseBody()
                    . '. Email: ' . $email . ', Password: ' . $password
            );

            echo "Login Authentication Debugging Follows\n";
            print_r($e->getResponseBody());
            print_r($e->getCode());
            print_r($e->getMessage());

            if ($e->getCode() === '000:011:001') {
                return false;
            }
            // @todo: And again, why?!
            throw new Exception($e);
        }
    }

    /**
     * Logs a user in via God Mode. (A Feature in Customer Services)
     *
     * @param string $email
     * @return array
     * @throws Exception
     * @author Asfer Tamimi
     *
     */
    public function loginViaGodMode($email) {
        try {
            // Check Input
            if (is_null($email) ) {
                throw new Exception('Email is not Valid. Email: ' . $email);
            }

            // Authenticate Contact
            $result = Hermes_Client_Rest::call('getContact', array(
                'email'      => $email,
            ));

            // Check Result
            if (!$result || empty($result) || empty($result['contact_ref'])) {
                // Authentication Failed
                sfContext::getInstance()->getLogger()->err('Authentication Failed');
                return array();
            } else {
                // Authentication Success
                sfContext::getInstance()->getLogger()->err('Authentication Success');

                // Clear Current Credentials by Logging Out
                $this->logout();

                // Start New Session
                $sessionStatus = $this->startSession();
                if (!$sessionStatus) {
                    throw new Exception('Authenticate::Session Failed to Start for Email: ' . $email);
                }

                // Set Credentials
                $result['email'] = $email;
                return $this->setPWNCredentials($result);
            }

        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Login Authentication Exception Called: ' . $e->getResponseBody() . '. Email: ' . $email
            );

            echo "Login Authentication Debugging Follows\n";
            print_r($e->getResponseBody());
            print_r($e->getCode());
            print_r($e->getMessage());

            if ($e->getCode() === '000:011:001') {
                return false;
            }
            // @todo: And again, why?!
            throw new Exception($e);
        }
    }

    /**
     * Logs a user out by clearing all the Session Credentials and Authentication Settings
     *
     * @author Asfer Tamimi (Partially taken from plusAuthentication::logout)
     *
     */
    public function logOut() {
        // Start the Session / Check the Session
        $this->startSession();

        // Clear Symfony Credentials
        $this->credentials = array();
        $this->authenticated = false;
        $this->getAttributeHolder()->clear();

        // Clear Teamsite Credentials
        unset($_SESSION['service_user'],$_SESSION['contact_ref'],$_SESSION['service_ref'],$_SESSION['service']);
        unset($_SESSION['first_name'],$_SESSION['last_name'],$_SESSION['email'],$_SESSION['time_limited_pins']);
        unset($_SESSION['service_type'],$_SESSION['user_type'],$_SESSION['account_id'],$_SESSION['is_virgin']);
        unset($_SESSION['previous_login_time']);

        // Regenerate Session
        session_regenerate_id();
    }

    /**
     * Sets the PWN Credentials. This Occurs after the User Successfully Logs in
     *
     * @param array $contact
     * @return bool
     * @author Asfer Tamimi
     *
     */
    public function setPWNCredentials($contact) {
        // Start the Session / Check the Session
        $this->startSession();

        // Set the Teamsite Credentials
        $_SESSION['authenticated']       = true;
        $_SESSION['service_user']        = isset($contact['service_user']) ? $contact['service_user'] : '';
        $_SESSION['contact_ref']         = isset($contact['contact_ref']) ? $contact['contact_ref'] : '';
        $_SESSION['service_ref']         = isset($contact['service_ref']) ? $contact['service_ref'] : '';
        $_SESSION['first_name']          = isset($contact['first_name']) ? $contact['first_name'] : '';
        $_SESSION['last_name']           = isset($contact['last_name']) ? $contact['last_name'] : '';
        $_SESSION['email']               = isset($contact['email']) ? $contact['email'] : '';
        $_SESSION['time_limited_pins']   = isset($contact['time_limited_pins']) ? $contact['time_limited_pins'] : false;
        $_SESSION['account_id']          = isset($contact['account_id']) ? $contact['account_id'] : '';
        $_SESSION['previous_login_time'] = isset($contact['previous_login_time']) ? $contact['previous_login_time'] : '';
        $_SESSION['is_virgin']           = $this->isVirgin($_SESSION['contact_ref']);

        // Set the Symfony Credentials
        $this->authenticated = true;
        $this->setAttributes = array(
            'service_user'         => $_SESSION['service_user'],
            'contact_ref'          => $_SESSION['contact_ref'],
            'service_ref'          => $_SESSION['service_ref'],
            'first_name'           => $_SESSION['first_name'],
            'last_name'            => $_SESSION['last_name'],
            'email'                => $_SESSION['email'],
            'time_limited_pins'    => $_SESSION['time_limited_pins'],
            'account_id'           => $_SESSION['account_id'],
            'previous_login_time'  => $_SESSION['previous_login_time'],
            'is_virgin'            => $_SESSION['is_virgin']
        );

        $user = sfContext::getInstance()->getUser();

        // Set the Service Credentials for Both Teamsite and Symfony
        switch ($_SESSION['service_user']) {
            case 'POWWOWNOW':
                $_SESSION['service_type'] = 'powwownow';
                $_SESSION['user_type']    = 'user';
                $user->addCredentials('powwownow', 'user', 'POWWOWNOW');
                $this->setAttribute('service', 'powwownow');
                $this->setAttribute('user_type', 'user');
                break;
            case 'PLUS_USER':
                $_SESSION['service_type'] = 'plus';
                $_SESSION['user_type']    = 'user';
                $user->addCredentials('plus', 'user', 'PLUS_USER');
                $this->setAttribute('service', 'plus');
                $this->setAttribute('user_type', 'user');
                break;
            case 'PLUS_ADMIN':
                $_SESSION['service_type'] = 'plus';
                $_SESSION['user_type']    = 'admin';
                $user->addCredentials('plus', 'admin', 'PLUS_ADMIN');
                $this->setAttribute('service', 'plus');
                $this->setAttribute('user_type', 'admin');
                break;
            case 'PREMIUM_USER':
                $_SESSION['service_type'] = 'premium';
                $_SESSION['user_type']    = 'user';
                $user->addCredentials('premium', 'user', 'PREMIUM_USER');
                $this->setAttribute('service', 'premium');
                $this->setAttribute('user_type', 'user');
                break;
            case 'PREMIUM_ADMIN':
                $_SESSION['service_type'] = 'premium';
                $_SESSION['user_type']    = 'admin';
                $user->addCredentials('premium', 'admin', 'PREMIUM_ADMIN');
                $this->setAttribute('service', 'premium');
                $this->setAttribute('user_type', 'admin');
                break;
        }

        // Regenerate Session ID
        session_regenerate_id();

        return true;
    }

    /**
     * Start the Session, it it has not already been Started
     *
     * @author Asfer Tamimi (Stolen from plusAuthenticate::startSession)
     *
     */
    public function startSession() {
        if (!isset($_SESSION)) {
            session_name('mypwn');
            ini_set('session.cookie_httponly', true);
        }
    }

    /**
     * Check to see if a User is a Virgin
     *
     * @param integer $contact_ref
     * @return bool
     * @author Asfer Tamimi (Stolen from plusAuthenticate::isVirgin)
     *
     */
    public function isVirgin($contact_ref) {
        try {
            if (empty($contact_ref)) {
                sfContext::getInstance()->getLogger()->err('Contact Ref not Set for checking Virginity');
                return false;
            }

            $result = Hermes_Client_Rest::call('isVirginUser',array(
                'contact_ref' => $contact_ref
            ));
            return $result['isVirgin'];
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('isVirginUser Throwed an Exception. Message:' . $e->getMessage() . ', Code: ' . $e->getCode());
            return false;
        }
    }

    /**
     * Checks that a User is Logged in and of a Specific Type. If they are Authorised, they will not be redirected
     *
     * @param string $type
     * @param array  $allowed
     * @param string $redirectUrl
     * @return bool
     * @author Asfer Tamimi (Stolen from plusAuthenticate::ensureLoggedIn()
     *
     */
    public function ensureLoggedIn($type = null, $allowed = array(), $redirectUrl = '/Login') {
        // Check if the User is Authorised
        $authorised = $this->isAuthorised($type, $allowed);

        // If They are not Authorised, Redirect Them
        if (!$authorised) {
            header('Location: http://' . $_SERVER['HTTP_HOST'] . $redirectUrl);
            return false;
        }
        return true;
    }

    /**
     * Checks to see if the User is Logged in, and Additionally checks to see if the User is the Correct User Type
     * Checks against the service_type, user_type and the service_user
     *
     * -------------------------------------------------------------------------------
     * - $type       - $allowed
     * -------------------------------------------------------------------------------
     * - service     - powwownow, plus, premium
     * - user        - user, admin
     * - combination - POWWOWNOW, PLUS_ADMIN, PLUS_USER, PREMIUM_ADMIN, PREMIUM_USER
     * -------------------------------------------------------------------------------
     *
     * @param string $type
     * @param array  $allowed
     * @return bool
     * @throws Exception
     * @author Asfer Tamimi (Stolen from plusAuthenticate::isAuthorised)
     *
     */
    public function isAuthorised($type = 'service', $allowed = array()) {
        // Check if the User is Authenticated
        if (!$this->authenticated) {
            sfContext::getInstance()->getLogger()->info('User is not Authenticated');
            return false;
        }

        // Check the Allowed Array
        if (!is_array($allowed) || empty($allowed)) {
            sfContext::getInstance()->getLogger()->err('Allowed Array is Empty');
            return false;
        }

        // Check the Type
        switch ($type) {
            case 'user':
                return in_array($this->getAttribute('user_type',null),$allowed);
                break;
            case 'combination':
                return in_array($this->getAttribute('service_user',null),$allowed);
                break;
            case 'service':
            case null:
            default:
                return in_array($this->getAttribute('service_type',null),$allowed);
                break;
        }

        // If we are here, then authentication has failed
        return false;
    }

    /**
     * Set Multiple Attributes. Used in Setting Credentials
     *
     * @param array $attributes
     * @author Asfer Tamimi
     *
     */
    public function setAttributes($attributes) {
        foreach ($attributes as $name => $value) {
            $this->setAttribute($name, $value);
        }
    }
}
