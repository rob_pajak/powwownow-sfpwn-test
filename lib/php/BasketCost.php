<?php

class BasketCost
{
    /**
     * Calculates the total cost of the balance, taking into account the current, negative balance and added bundles.
     *
     * @param float $balance
     * @param Basket $basket
     * @return float
     */
    public function totalWithBalance($balance, Basket $basket)
    {
        $total = $basket->calculateTotalCost();
        if ($balance < 0) {
            $total += abs($balance);
        }
        return $total;
    }
}
