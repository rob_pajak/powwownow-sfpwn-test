<?php

/**
 * A helper for reusable behaviour to controllers
 */
class controllerHelper
{
    /**
     * Sets up a request to be a JSON ajax form response
     *
     * @param sfWebRequest $request
     * @param sfAction $controller
     * @return true
     */
    public static function jsonFormResponse(sfWebRequest $request, sfAction $controller)
    {
        if (!$request->isXmlHttpRequest()) {
            $controller->logMessage($request->getUri() . ' was not requested not as an xmlHttpRequest', 'warning');
            $controller->forward404();
        }

        $controller->getResponse()->setHttpHeader('Content-type','application/json');

        $controller->setLayout(false);

        return true;
    }
}
