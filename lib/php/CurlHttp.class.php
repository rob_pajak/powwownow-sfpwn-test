<?php
/*Taken from PHP-Models*/
define('HTTP_REQUEST_METHOD_DELETE',				"DELETE");
define('HTTP_REQUEST_METHOD_GET',					"GET");
define('HTTP_REQUEST_METHOD_POST',				"POST");
define('HTTP_REQUEST_METHOD_HEAD',				"HEAD");
define('HTTP_REQUEST_METHOD_OPTIONS',				"OPTIONS");
define('HTTP_REQUEST_METHOD_PUT',					"PUT");
define('HTTP_REQUEST_METHOD_TRACE',				"TRACE");

class CurlHttp {

	private $curl_res = null;
	private $HTTPMethod = HTTP_REQUEST_METHOD_GET;
	private $URL = null;
	private $Port = null;
	private $Header = array();
	private $CURLOptions = array();
	private $QueryString = null;
	private $responseCode = null;
	private $responseBdy = null;
	function __construct($url=null,$ssl_cert=null, $ssl_key=null, $server_cert=null, $options=null) {
		$this->initalise($url);
		if ($ssl_cert != null) {
			curl_setopt($this->curl_res,CURLOPT_SSL_VERIFYPEER,TRUE);
	 		curl_setopt($this->curl_res,CURLOPT_SSL_VERIFYHOST,1);
	 		curl_setopt($this->curl_res,CURLOPT_CAINFO,$server_cert);
	 		curl_setopt($this->curl_res,CURLOPT_SSLCERT,$ssl_cert);
	 		curl_setopt($this->curl_res,CURLOPT_SSLKEY,$ssl_key);
		}
    }

    function setMethod($method)
    {
    	$this->HTTPMethod = $method;
    }

    function addHeader($headrKey, $headerItem)
    {
        if (isset($this->Header[$headrKey])) {
            $this->Header[$headrKey] .= $headerItem;
        } else {
            $this->Header[$headrKey] = $headerItem;
        }
    }

    function setBody($data)
    {
    	curl_setopt($this->curl_res,CURLOPT_POST,true);
    	curl_setopt($this->curl_res,CURLOPT_POSTFIELDS,$data);
    }

	function setUrl($url)
	{
		$this->URL = $url;
	}
	function setPort($port)
	{
		$this->Port = $port;
	}
	function changeUrl($url,$port=null)
	{
		$this->close();
		$this->initalise();
		$this->URL = $url;
		$this->Port = $port;
	}

    function sendRequest()
    {
    	$headers = $this->prepareHeader();

		curl_setopt($this->curl_res, CURLOPT_URL,$this->URL);
    	if ($this->Port != null) {
    		curl_setopt($this->curl_res, CURLOPT_PORT,$this->Port);
    	}
    	curl_setopt($this->curl_res, CURLOPT_CUSTOMREQUEST,$this->HTTPMethod);
    	curl_setopt($this->curl_res, CURLOPT_HTTPHEADER, $headers);
    	curl_setopt($this->curl_res, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl_res, CURLOPT_TIMEOUT, 60);

    	$this->responseBdy = curl_exec($this->curl_res);

        if (curl_errno($this->curl_res)) {
        	$this->responseBdy = curl_error($this->curl_res);
        	return false;
        } else {
        	return true;
        }
    }

	function getResponseBody()
	{
		return $this->responseBdy;
	}
    function getResponseCode()
    {
    	$this->responseCode = curl_getinfo($this->curl_res, CURLINFO_HTTP_CODE);

    	return $this->responseCode;
    }

	function setCURLOption($opt,$value)
	{
		curl_setopt($this->curl_res,$opt,$value);
	}

	private function prepareHeader()
	{
		$header = array();
		$i = 0;
		foreach($this->Header as $key=>$value)
		{
			$header[$i] = $key.": ".$value;
			$i++;
		}
		return $header;
	}

	private function initalise($url)
	{
		if ($this->curl_res != null) {
			$this->close();
		}
		$this->curl_res = curl_init();
		$this->URL = $url;
		$this->Header = array();
		$this->CURLOptions = array();
		$this->QueryString = null;
		$this->responseCode = null;
		$this->responseBdy = null;
	}
    function close()
    {
    	if ($this->curl_res != null) {
			curl_close($this->curl_res);
			$this->curl_res = null;
			$this->HTTPMethod = HTTP_REQUEST_METHOD_GET;
		}
    }
}
?>
