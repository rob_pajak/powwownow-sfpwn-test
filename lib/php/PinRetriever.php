<?php

class PinRetriever
{
    /**
     * Cached pins for a contact reference map.
     *
     * @var array
     */
    protected $pinsForContactMap;

    /**
     * Retrieves the pins for the given contact.
     * The pins for a contact are cached.
     *
     * @param int $contactRef
     * @param bool $reset
     * @return array
     */
    public function retrievePinsFromContact($contactRef, $reset = false)
    {
        if (!isset($this->pinsForContactMap[$contactRef]) || $reset) {
            $this->pinsForContactMap[$contactRef] = array();

            try {
                $contactPins = Hermes_Client_Rest::call('getContactPins',array(
                    'contact_ref' => $contactRef
                ));
            } catch (Exception $e) {
                $this->logException('An exception was thrown when calling getContactPins.', $e);
                return array();
            }
            foreach ($contactPins as $pin) {
                if (isset($pin['master_pin'])) {
                    $this->pinsForContactMap[$contactRef][] = $pin['pin_ref'];
                }
            }
        }
        return $this->pinsForContactMap[$contactRef];
    }

    /**
     * Makes a call to Default.getAccountPinsByContactRef and collects the pins by pin_ref.
     *
     * @param int $contactRef
     * @return array
     */
    public function getAccountPinsByContactRef($contactRef)
    {
        try {
            $contactPins = Hermes_Client_Rest::call(
                'getAccountPinsByContactRef',
                array(
                    'contact_ref' => $contactRef,
                    'active' => 'Y'
                )
            );
        } catch (Exception $e) {
            $this->logException('An exception was thrown when calling getAccountPinsByContactRef.', $e);
            return array();
        }

        $pins = array();
        foreach ($contactPins as $pin) {
            $pins[$pin['pin_ref']] = array(
                'pin_ref' => $pin['pin_ref'],
                'master_pin' => $pin['master_pin'],
                'pin' => $pin['pin']
            );
        }

        return $pins;
    }

    private function logException($context, Exception $e)
    {
        sfContext::getInstance()->getLogger()->err(
            sprintf(
                $context . ' The exception message is "%s". The exception code is "%s".',
                $e->getMessage(),
                $e->getCode()
            )
        );
    }
}
