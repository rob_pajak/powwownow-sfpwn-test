<?php

/**
 * Wraps a Bundle to hold pins (or contact refs) to be assigned to the product.
 *
 * @author Maarten Jacobs
 */
class AssignableBundleWrapper
{
    /**
     * @var Bundle
     */
    private $bundle;

    /**
     * @var array
     */
    private $pins;

    public function __construct(Bundle $bundle, array $pins = array())
    {
        $this->bundle = $bundle;
        $this->setPins($pins);
    }

    public function getPins()
    {
        return $this->pins;
    }

    public function setPins($pins)
    {
        $this->pins = $pins;
        return $this;
    }

    public function getBundle()
    {
        return $this->bundle;
    }

    public function getProductGroupIdForAssignment()
    {
        if ($this->bundle->hasAddon()) {
            return $this->bundle->getAddon()->getProductGroupId();
        } else {
            return $this->bundle->getProductGroupId();
        }
    }
}
