<?php

/**
 * Exceptions for handling ajax form exceptions
 */
class AjaxFormException extends Exception
{
    /**
     * Array of reasons for exception occuring
     */
    private $errorReasons = null;

    /**
     * Standard exception construct with addition of $errorReasons
     */
    public function __construct($message, $code = 0, Exception $previous = null, array $errorReasons = null) {

        $this->errorReasons = $errorReasons;

        parent::__construct($message, $code);
    }

    /**
     * Returns error reasons. If none are supplied then fetches default
     * @return array array('error_messages' => array('message' => 'ERROR_REASON_PROBABLY_A_JS_VALUE', 'field_name' => 'alert'))
     */
    public function getErrorReasons()
    {
        if (is_null($this->errorReasons)) {
            $this->errorReasons = $this->getDefaultErrorReason();
        }
//        print_r($this->errorReasons);
        return array('error_messages' => array($this->errorReasons));
//        return $this->errorReasons;
    }

    /**
     * Gets default error reason - this is the communication error for an alert field
     */
    private function getDefaultErrorReason()
    {
        return array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
//        return array('error_messages' => array(array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert')));
    }
}
