<?php

/**
 * Class HermesCalls
 *
 * This Class will only store Hermes Calls, No Exception Handling.
 * It will therefore be used for mocking, for actual testing.
 * @author Asfer Tamimi
 */
class HermesCalls
{
    /**
     * Get IMeet User by the Contact Ref
     * @param array $args
     * @param bool $cache
     * @return mixed
     */
    public static function iMeetGetIMeetUserByContactRef(array $args, $cache = false)
    {
        if ($cache) {
            sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
            return hermesCallWithCaching('IMeet.getIMeetUserByContactRef', $args);
        } else {
            return Hermes_Client_Rest::callPOST('IMeet.getIMeetUserByContactRef', $args);
        }
    }

    /**
     * Register a Free Pin User for Shomei
     * @param array $args
     * @param bool $cache
     * @return mixed
     */
    public static function shomeiRegisterFreePinForShomei($args, $cache = false)
    {
        if ($cache) {
            sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
            return hermesCallWithCaching('Shomei.registerFreePinForShomei', $args);
        } else {
            return Hermes_Client_Rest::callPOST('Shomei.registerFreePinForShomei', $args);
        }
    }
}
