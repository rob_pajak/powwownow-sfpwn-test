<?php

/**
 * Listens to the WorldPay Successful transaction event, to trigger the requested updates to the account.
 *
 * @author Maarten Jacobs
 */
class BasketProcess
{
    /**
     * Cached products for pins map.
     *
     * @var array
     */
    protected $productsForPinsMap;

    /**
     * Dispatcher instance for logging.
     *
     * @var sfEventDispatcher
     */
    protected $dispatcher;

    /**
     * The current user session.
     *
     * @var sfUser
     */
    protected $user;

    /**
     * A cached instance of the basket for validation and assignment of products.
     *
     * @var Basket
     */
    protected $basket;

    /**
     * Cached user's contact reference.
     *
     * @var int
     */
    protected $contactRef;

    /**
     * Cached user's account id.
     *
     * @var int
     */
    protected $accountId;

    /**
     * Cached user's locale.
     *
     * @var string
     */
    protected $locale;

    /**
     * Cached account information.
     *
     * @var array
     */
    protected $accountInfo;

    /**
     * The products that will be added to the account, and assigned the pins as requested.
     *
     * If the current admin is an admin, the added products may have pins in the data structure.
     * That is, the added products is a map of product ids to data.
     * The data assigned to the product id includes everything necessary for assignment to the account (only product id),
     * but also the data required for the assignment of pins to products. The data required for pins to products assignment
     * comes from the assign-products-basket page. Thus, the contact refs (not the pins) are collected in an array
     * assigned to the key 'pins'.
     *
     * Example:
     * <code>
     *     $addedProducts = array(
     *          4 => array(
     *              'pins' => array(
     *                  323,
     *                  58792,
     *              ),
     *          ),
     *          5 => array(
     *              'pins' => array(
     *                  3207,
     *                  234,
     *              ),
     *          ),
     *     );
     * </code>
     *
     * @var array
     */
    protected $addedProducts = array();

    /**
     * The products that will be removed from the account, and removed from all pins assigned to the account.
     *
     * The format is a list of product ids.
     *
     * @var array
     */
    protected $removedProducts = array();

    /**
     * The id of the payment against which the basket has been purchased.
     *
     * @var int
     */
    protected $paymentId;

    /**
     * Indication to clear the basket on completion of the changes.
     *
     * @var bool
     */
    public $clearBasketOnCompletion = true;

    /**
     * Construct a new instance of the current class.
     *
     * @param sfUser $user
     * @param int $paymentId
     * @param bool $useBasket
     *   Indicate to take the added products from the account by default.
     */
    public function __construct(sfUser $user, $paymentId, $useBasket = true)
    {
        $this->user = $user;
        $this->basket = new Basket($user);
        $this->paymentId = $paymentId;

        if ($useBasket) {
            // Set the added products from the basket by default.
            $this->setAddedProducts($this->basket->retrieveAddedProducts());
        }

        // Cache the necessary user variables for later use.
        $this->contactRef = $user->getAttribute('contact_ref');
        $this->accountId = $user->getAttribute('account_id');
        $this->locale = $user->getCulture();

        // Keep an instance of the dispatcher for logging and event firing.
        $this->dispatcher = ProjectConfiguration::getActive()->getEventDispatcher();
    }

    /**
     * Returns the list of products to be added to the current account and its assigned pins.
     *
     * @return array
     */
    public function retrieveAddedProducts()
    {
        return $this->addedProducts;
    }

    /**
     * Sets the list of products to be added to the current account and its assigned pins.
     *
     * @param array $addedProducts
     * @return BasketProcess
     */
    public function setAddedProducts(array $addedProducts)
    {
        $this->addedProducts = $addedProducts;
        return $this;
    }

    /**
     * Returns the list of products to be removed from the product.
     *
     * @return array
     */
    public function retrieveRemovedProducts()
    {
        return $this->removedProducts;
    }

    /**
     * Sets the list of products to be removed from the product.
     *
     * @param array $removedProducts
     * @return BasketProcess
     */
    public function setRemovedProducts(array $removedProducts)
    {
        $this->removedProducts = $removedProducts;
        return $this;
    }

    /**
     * Triggers the update handler, and returns the summary details.
     *
     * Handles the creation of constituent creator objects.
     *
     * @param bool $isPurchaseFromBundle
     * @param myUser $user
     * @param PaymentSession $session
     * @return bool|array
     */
    public static function updateAccountDefaultHandler($isPurchaseFromBundle, myUser $user, PaymentSession $session)
    {
        $processor = new BasketProcess($user, $session->getPaymentId());
        $pinRetriever = new PinRetriever();
        $bwmCreator = new BWMCreator($user, $pinRetriever, $isPurchaseFromBundle, $session);
        $bundleCreator = new BundleCreator($user, $pinRetriever, $isPurchaseFromBundle, $session);

        return self::updateAccountHandler($user, $processor, $bwmCreator, $bundleCreator, $pinRetriever);
    }

    public static function updateAccountHandler(
        sfUser $user,
        BasketProcess $processor,
        BWMCreator $bwmCreator,
        BundleCreator $bundleCreator,
        PinRetriever $pinRetriever)
    {
        if ($processor->isValidBasket()) {
            // Save the balance before any changes were made.
            $beforeBalance = $processor->retrieveBalance();

            // Update the account pins.
            $summaryDetails = $processor->applyChanges($bwmCreator, $bundleCreator, $pinRetriever);

            // Save the balance after the changes have been made.
            $afterBalance = $processor->retrieveBalance(true);

            // Add the before and after balance to the summary details.
            $summaryDetails['balance_before'] = $beforeBalance;
            $summaryDetails['balance_after'] = $afterBalance;

            // Store the variables necessary to display a summary for the user.
            // These details should be retrieved and displayed on the account update summary page.
            $user->setAttribute('summary-details', $summaryDetails);

            return $summaryDetails;
        }
        return false;
    }

    /**
     * Assert that the stored basket contains valid changes to apply to the account.
     *
     * @return bool
     */
    public function isValidBasket()
    {
        return !$this->basket->isEmpty();
    }

    /**
     * Retrieves the balance of the user.
     *
     * @param bool $reset
     * @return mixed
     *   If the balance can be retrieved, returns the balance.
     *   If not, returns false.
     */
    public function retrieveBalance($reset = false) {
        if ($reset || empty($this->accountInfo)) {
            try {
                $this->accountInfo = plusCommon::getAccountBalanceAndUsers($this->accountId, $this->contactRef);
            }
            catch (Exception $e) {
                $this->logMessage('Error calling getAccountBalanceAndUsers. Error message: ' . $e->getMessage(), 'err');
                return false;
            }
        }
        if (!isset($this->accountInfo['balance'])) {
            return false;
        }
        return $this->accountInfo['balance'];
    }

    /**
     * Assigns and unassigns the product ids accordingly.
     *
     * @param array $addedProductIds
     * @param array $removedProductIds
     * @return bool
     */
    protected function updateAccountProductGroups(array $addedProductIds, array $removedProductIds) {
        // Retrieve all assigned product id to the account.
        $accountProductGroups = Hermes_Client_Rest::call('getPlusAccountProductGroups', array(
            'account_id' => $this->accountId,
            'locale' => $this->locale
        ));
        // We only need the product group ids.
        $accountProductGroupIds = array();
        foreach ($accountProductGroups as $product) {
            $accountProductGroupIds[] = $product['product_group_id'];
        }
        // Assert that all to-add products have not been assigned to the account.
        $addableProductIds = array_diff($addedProductIds, $accountProductGroupIds);

        // Assert that all to-remove products have been assigned to the account.
        $removableProductIds = array_intersect($accountProductGroupIds, $removedProductIds);

        // Opt-out if no product groups need to be assigned and unassigned.
        if (!$addableProductIds && !$removableProductIds) {
            return false;
        }

        // Call Hermes to assign and unassign accordingly.
        Hermes_Client_Rest::call('doUpdatePlusAccountProductGroups', array(
            'account_id'                 => $this->accountId,
            'assign_product_group_ids'   => $addableProductIds,
            'unassign_product_group_ids' => $removableProductIds
        ));
        $this->clearTeamsiteCache($this->contactRef);

        return true;
    }

    /**
     * Clears the cached data related to the contact reference.
     *
     * @param int $contactRef
     */
    protected function clearTeamsiteCache($contactRef) {
        try {
            PWN_Cache_Clearer::modifiedPin($contactRef);
            PWN_Cache_Clearer::modifiedContact($contactRef);
            $this->logMessage('Cleared Cache for contact_ref: ' . $contactRef . ' while switching to Plus', 'info');
        } catch (Exception $e) {
            $this->logMessage('Unable to clear cache after doUpdatePlusAccountProductGroups' .  serialize($e), 'err');
        }
    }

    /**
     * Retrieves all pins assigned to the contact.
     *
     * Note: retrieves pins regardless of the state of master_pin.
     *
     * @param int $contactRef
     * @return array
     */
    protected function retrieveAllPins($contactRef) {
        $contactPins = Hermes_Client_Rest::call('getAccountPinsByContactRef', array(
            'contact_ref' => $contactRef,
            'active' => 'Y'
        ));

        $pins = array();
        foreach ($contactPins as $pin) {
            $pins[$pin['pin_ref']] = array(
                'pin_ref' => $pin['pin_ref'],
                'master_pin' => $pin['master_pin'],
                'pin' => $pin['pin']
            );
        }

        return $pins;
    }

    /**
     * Retrieves a list of products that can be unassigned from the pin (i.e. are currently assigned to the pin).
     *
     * @param int $pinRef
     * @param array $removedProductIds
     * @param string $locale
     * @return array
     */
    protected function retrieveUnassignableProductsForPin($pinRef, array $removedProductIds, $locale) {
        // Find all product groups currently assigned to the pin.
        $pinProductGroups = $this->retrieveProductGroupsFromPin($pinRef, $locale);
        // Opt-out if no product groups are assigned.
        if (!$pinProductGroups) {
            // No product groups are assigned; no product groups can be unassigned.
            return array();
        }

        // Select the intersection of the assigned and remove-products.
        // This is the list of products that will actually be unassigned.
        $canBeUnassigned = array_intersect($pinProductGroups, $removedProductIds);

        // Return the intersection.
        return $canBeUnassigned;
    }

    /**
     * Retrieves a list of products that can be assigned to the pin (i.e. are not yet assigned to the pin).
     *
     * @param $pinRef
     * @param array $addedProductIds
     * @param string $locale
     * @return array
     */
    protected function retrieveAssignableProductsForPin($pinRef, array $addedProductIds, $locale) {
        // Find all product groups currently assigned to the pin.
        $pinProductGroups = $this->retrieveProductGroupsFromPin($pinRef, $locale);

        // Assert that all added product ids are not in the currently assigned products.
        $canBeAssigned = array_diff($addedProductIds, $pinProductGroups);

        // Return those product ids that are not currently assigned to the pin.
        return $canBeAssigned;
    }

    /**
     * Retrieves the product groups assigned to the pin.
     *
     * NOTE: works on cached data.
     *
     * @param int $pinRef
     * @param string $locale
     * @param bool $reset
     * @return array
     */
    protected function retrieveProductGroupsFromPin($pinRef, $locale, $reset = false) {
        if (!isset($this->productsForPinsMap[$pinRef][$locale]) || $reset) {
            if (!isset($this->productsForPinsMap[$pinRef])) {
                $this->productsForPinsMap[$pinRef] = array();
            }

            // Make the Hermes call to retrieve the plus pin product groups.
            $pinProductGroups = Hermes_Client_Rest::call('getPlusPinProductGroups',array(
                'pin_ref' => $pinRef,
                'locale'  => $locale
            ));

            // Store only the product group ids.
            $assignedProductGroupIds = array();
            foreach ($pinProductGroups as $productGroup) {
                $assignedProductGroupIds[] = $productGroup['product_group_id'];
            }

            $this->productsForPinsMap[$pinRef][$locale] = $assignedProductGroupIds;
        }
        return $this->productsForPinsMap[$pinRef][$locale];
    }

    /**
     * Updates the pins of the admin to set chairman_present to Start.
     *
     * @param int $contactRef
     */
    protected function updateAdminPinsToChairmanPresent($contactRef) {
        // Obtain the Admin PINs, Check the chairman_present Field, Update the chairman_present Field to 'Start'
        $pins = Hermes_Client_Rest::call('getContactPins', array(
            'contact_ref' => $contactRef,
            'active_only' => 'Y'
        ));

        foreach($pins as $pin) {
            if ($pin['chairman_present'] == '') {
                Hermes_Client_Rest::call('updatePin', array(
                    'pin_ref'          => $pin['pin_ref'],
                    'chairman_present' => 'Start'
                ));
            }
        }
    }

    /**
     * Check if a BWM has been added to the basket.
     *
     * @param array $bwmProducts
     * @return bool
     */
    protected function hasBWMBeenAdded(array $bwmProducts) {
        return count($bwmProducts) > 0;
    }

    /**
     * Removes all added BWMs from the basket,
     *
     * @param array $addedProducts
     * @return array
     */
    protected function removeBWMsFromProducts(array $addedProducts) {
        $filteredItems = array();
        foreach ($addedProducts as $productId => $product) {
            if ($this->isNotBWMKey($productId)) {
                $filteredItems[$productId] = $product;
            }
        }
        return $filteredItems;
    }

    /**
     * Ascertain that the key is not a BWM key.
     *
     * @param int $key
     * @return bool
     */
    protected function isNotBWMKey($key) {
        return $key > 0;
    }

    protected function initialisePinsOnProducts(array $products, array $pins)
    {
        foreach ($products as &$product) {
            if (!isset($product['pins'])) {
                $product['pins'] = $pins;
            }
        }
        unset($product);
        return $products;
    }

    private function filterOutCallCreditFromAddedProducts($addedProducts)
    {
        $filteredProducts = array();
        foreach ($addedProducts as $productId => $product){
            if (empty($product['type']) || $product['type'] !== 'credit') {
                $filteredProducts[$productId] = $product;
            }
        }
        return $filteredProducts;
    }

    private function mergeProductCreationAssignedProducts(array $createResult, array $addedProducts)
    {
        foreach ($createResult as $productId => $productData) {
            $addedProducts[$productId] = $productData;
        }
        return $addedProducts;
    }

    /**
     * Applies the requested changes of the basket.
     *
     * @param BWMCreator $bwmCreator
     * @param BundleCreator $bundleCreator
     * @param PinRetriever $pinRetriever
     * @return array
     *   The data required to display a summary update.
     */
    public function applyChanges(BWMCreator $bwmCreator, BundleCreator $bundleCreator, PinRetriever $pinRetriever)
    {
        $pins = $this->retrieveAllPins($this->contactRef);
        $allPins = $pinRetriever->getAccountPinsByContactRef($this->contactRef);
        $allPinRefs = array_keys($allPins);

        $summaryDetails = array(
            'error' => false,
            'error_messages' => array(),
        );

        // Keep a copy of all added and removed products.
        // Because pins are optional, we assign an empty array (or for single admins, see above) as the pins if none
        // are set.
        $addedProducts = $fullAddedProducts = $this->initialisePinsOnProducts(
            $this->filterOutCallCreditFromAddedProducts($this->retrieveAddedProducts()),
            array()
        );
        $removedProducts = $this->retrieveRemovedProducts();

        // Before the end of this function, we will pass all Call Credit products back to the listener, so it can be
        // used on the purchase-confirmation and other pages.
        $creditProducts = $this->basket->retrieveCallCreditProducts();
        // Log the purchased call credit products.
        foreach ($creditProducts as $creditProduct) {
            $logResult = $this->logCallCreditPurchase($this->accountId, $creditProduct['amount'], $this->paymentId);
            if ($logResult !== true) {
                // Failing to log the call credit purchase is not a major issue, but we need to raise it somehow.
                $summaryDetails['error'] = true;
                $summaryDetails['error_messages'][] = $logResult;
            }
        }

        // Handle the edge-case: BWMs.
        $bwmProducts = $this->basket->retrieveNonExistingProducts();
        if ($this->hasBWMBeenAdded($bwmProducts)) {
            // Create the new BWM.
            foreach ($bwmProducts as $bwmProduct) {
                // Always attach in admin pins.
                if (!isset($bwmProduct['pins'])) {
                    $bwmProduct['pins'] = array();
                }
                $bwmProduct['pins'] = array_unique(array_merge($bwmProduct['pins'], $allPinRefs));

                $bwmResult = $bwmCreator->createAndAssign($bwmProduct, new ProductCreationResult());

                // Handle errors, if any.
                if ($bwmResult->hasErrorOccurred()) {
                    $summaryDetails['error'] = true;
                    $summaryDetails['error_messages'] = array_merge($summaryDetails['error_messages'], $bwmResult->getErrorMessages());
                } else {
                    // Handle success.

                    // Alter the added products to reflect the newly added BWM, and log the separate purchase.
                    // BWM will never have more than 1 added product.
                    foreach ($bwmResult->getAddedProducts() as $productGroupId => $productData) {
                        $fullAddedProducts[$productGroupId] = $productData;

                        $logResult = $this->logProductPurchase($this->accountId, $this->paymentId, $productGroupId, $this->calculateBwmCost($bwmProduct));
                        if ($logResult !== true) {
                            // Failing to log the product purchase is not a major issue, but we need to raise it somehow.
                            $summaryDetails['error'] = true;
                            $summaryDetails['error_messages'][] = $logResult;
                        }
                    }
                }
            }

            // Remove added BWMs; this prevents the rest of the flow from trying to assign to a non-existing group.
            $addedProducts = $this->removeBWMsFromProducts($addedProducts);
            $fullAddedProducts = $this->removeBWMsFromProducts($fullAddedProducts);
        }

        if ($this->basket->hasBundleBeenAdded()) {
            $bundle = $this->basket->retrieveAddedBundleObject();
            $bundleMap = $this->basket->retrieveAddedBundle();
            $bundleWrapper = new AssignableBundleWrapper($bundle);
            if (!empty($bundleMap['pins'])) {
                $bundlePins = $bundleMap['pins'];
                $bundleWrapper->setPins(array($bundlePins['participant_pin_ref'], $bundlePins['chairman_pin_ref']));
            } else {
                $bundleWrapper->setPins($allPinRefs);
            }
            $bundleResult = $bundleCreator->createAndAssign($bundleWrapper, new ProductCreationResult());

            if ($bundleResult->hasErrorOccurred()) {
                $summaryDetails['error'] = true;
                $summaryDetails['error_messages'] = array_merge($summaryDetails['error_messages'], $bundleResult->getErrorMessages());
            } else {
                $fullAddedProducts = $this->mergeProductCreationAssignedProducts($bundleResult->getAddedProducts(), $fullAddedProducts);
            }

            unset($addedProducts[$bundle->getProductGroupId()]);
            if ($bundle->hasAddon()) {
                unset($addedProducts[$bundle->getAddon()->getProductGroupId()]);
            }

            $logResult = $this->logProductPurchase($this->accountId, $this->paymentId, $bundle->getAssignableProductGroupId(), $bundle->calculateProRataCost());
            if ($logResult !== true) {
                // Failing to log the product purchase is not a major issue, but we need to raise it somehow.
                $summaryDetails['error'] = true;
                $summaryDetails['error_messages'][] = $logResult;
            }
        }

        // Keep a flat list of product group ids for Hermes call methods.
        $addedProductIds = array_keys($addedProducts);
        $removedProductIds = array_keys($removedProducts);

        // Because of the BWMs edge-case, there is the possibility of empty added products.
        // In that case, skip to the end of this method.
        if ($addedProductIds || $removedProductIds) {
            // Update the plus account product groups.
            try {
                $this->updateAccountProductGroups($addedProductIds, $removedProductIds);
            }
            catch (Exception $e) {
                $this->logMessage('Error calling updateAccountProductGroups. Error message: ' . $e->getMessage(), 'err');
                $summaryDetails['error'] = true;
                $summaryDetails['error_messages'][] = 'Error whilst trying to link products to account.';
                return $summaryDetails;
            }

            // Update the pins to products.
            // Retain a map of pin to contactRef for later display of the account summary.
            $pinToContactMap = array();
            // Form a product pin to product map of the assign-products stage.
            // This is useful for the next stage.
            $pinsToSelectedProducts = array();

            foreach ($addedProducts as $productId => $productData) {
                if (!empty($productData['pins']) && is_array($productData['pins'])) {
                    $contactRefs = $productData['pins'];

                    // Convert the contact refs to pins.
                    foreach ($contactRefs as $subContactRef) {
                        $contactPins = $pinRetriever->retrievePinsFromContact($subContactRef);
                        foreach ($contactPins as $contactPin) {
                            if (!isset($pinsToSelectedProducts[$contactPin])) {
                                $pinsToSelectedProducts[$contactPin] = array();
                            }
                            $pinsToSelectedProducts[$contactPin][] = $productId;

                            // The contact ref is stored to the pin for later logging of which products were effectively
                            // enabled for the contact.
                            $pinToContactMap[$contactPin] = $subContactRef;
                        }
                    }
                }
            }

            // For the assignment and unassignment, we require a map of pin reference to assign and unassign product ids.
            // Build that map here, but make sure to exclude the products to be assigned if they already have been assigned
            // to the pin. Similarly with the products to unassign, exclude all products that are not assigned to the pin.
            $pinsToProducts = array();
            foreach ($pins as $pinRef => $pin) {
                // In a previous phase, the admin should have assigned pins to products.
                // Now we process that map and use those as the basis for assignable products.
                $pinSelectedProducts = array();
                if (isset($pinsToSelectedProducts[$pinRef])) {
                    $pinSelectedProducts = $pinsToSelectedProducts[$pinRef];
                }

                $assignableProducts = $this->retrieveAssignableProductsForPin($pinRef, $pinSelectedProducts, $this->locale);
                $unassignableProducts = $this->retrieveUnassignableProductsForPin($pinRef, $removedProductIds, $this->locale);

                // Opt-out for this pin, if no products need to be assigned or unassigned.
                if (!$assignableProducts && !$unassignableProducts) {
                    continue;
                }

                // Store the pin_ref for the later Hermes call.
                $pinsToProducts[$pinRef] = $pin;
                // Store the assignable products.
                $pinsToProducts[$pinRef]['assign_product_group_ids'] = $assignableProducts;
                // Store the unassignable products.
                $pinsToProducts[$pinRef]['unassign_product_group_ids'] = $unassignableProducts;
                // Assign the master pin of the pin to the products as well.
                $masterPin = plusCommon::getPinPair($pins, $pinRef);
                if ($masterPin) {
                    if (!isset($pinsToProducts[$masterPin])) {
                        $pinsToProducts[$masterPin] = $assignableProducts;
                    }
                    else {
                        $pinsToProducts[$masterPin] = array_merge($assignableProducts, $pinsToProducts[$masterPin]);
                    }
                }
            }

            // Actually assign the products by making the Hermes call.
            foreach ($pinsToProducts as &$updateParameters) {
                // Store the response for later use.
                $updateParameters['response'] = Hermes_Client_Rest::call('updatePlusPinProductGroups', array(
                    'pin_ref' => $updateParameters['pin_ref'],
                    'assign_product_group_ids' => $updateParameters['assign_product_group_ids'],
                    'unassign_product_group_ids' => $updateParameters['unassign_product_group_ids'],
                ));
            }
            // Remove the reference to the last parameters.
            unset($updateParameters);
        }

        // Update the admin pins to set chairman_present.
        $this->updateAdminPinsToChairmanPresent($this->contactRef);

        // Clear the basket, if needed.
        if ($this->clearBasketOnCompletion) {
            $this->basket->clearBasket();
        }

        // Merge Call Credit products back in for display.
        foreach ($creditProducts as $creditId => $creditProduct) {
            if (isset($creditProduct['editable_amount'])) {
                $creditProduct['editable_amount'] = false;
            }
            if (isset($creditProduct['editable_recharge'])) {
                $creditProduct['editable_recharge'] = false;
            }
            $fullAddedProducts[$creditId] = $creditProduct;
        }

        // Return a full report of the changes made.
        $summaryDetails['added_products'] = $fullAddedProducts;
        $summaryDetails['removed_products'] = $removedProducts;
        $summaryDetails['manages_users'] = $this->accountInfo['accountUsers'] !== 'S';
        return $summaryDetails;
    }

    /**
     * Copied method of the sfActions::logMessage method.
     *
     * @param string $message
     * @param string $priority
     */
    protected function logMessage($message, $priority = 'info')
    {
        if (sfConfig::get('sf_logging_enabled')) {
            $this->dispatcher->notify(new sfEvent($this, 'application.log', array($message, 'priority' => constant('sfLogger::'.strtoupper($priority)))));
        }
    }

    /**
     * Sums the cost of a BWM for the logs.
     *
     * @param array $bwmBasketProduct
     * @return float|int
     * @author Maarten Jacobs
     */
    private function calculateBwmCost(array $bwmBasketProduct)
    {
        // Add total DNISes cost.
        $total = (float) $bwmBasketProduct[1]['BWM_total_cost']['total'];
        // Add connection fee cost.
        $total += (float) $bwmBasketProduct[1]['BWM_total_cost']['connectionFee'];
        return $total;
    }

    /**
     * Calls Hermes to log the call credit purchase.
     *
     * @param int $accountId
     * @param float $creditAmount
     * @param int $paymentId
     * @return bool|string
     * @author Maarten Jacobs
     */
    private function logCallCreditPurchase($accountId, $creditAmount, $paymentId)
    {
        try {
            Hermes_Client_Rest::call(
                'Plus.logCallCreditPurchase',
                array(
                    'account_id' => $accountId,
                    'credit_amount' => $creditAmount,
                    'payment_id' => $paymentId,
                )
            );
        } catch (Exception $e) {
            $errorMessage = 'Unable to log call credit purchase.';
            $this->logMessage($errorMessage . ' Exception message: ' . $e->getMessage() . '. Exception code: ' . $e->getCode(), 'err');
            return $errorMessage;
        }
        return true;
    }

    /**
     * Calls Hermes to log the separate product purchase.
     *
     * @param int $accountId
     * @param int $paymentId
     * @param int $productGroupId
     * @param float $cost
     * @return bool|string
     * @author Maarten Jacobs
     */
    private function logProductPurchase($accountId, $paymentId, $productGroupId, $cost)
    {
        try {
            Hermes_Client_Rest::call(
                'Plus.logProductPurchase',
                array(
                    'account_id' => $accountId,
                    'payment_id' => $paymentId,
                    'product_group_id' => $productGroupId,
                    'cost' => $cost,
                )
            );
        } catch (Exception $e) {
            $errorMessage = 'Unable to log separate product purchase.';
            $this->logMessage($errorMessage . ' Exception message: ' . $e->getMessage() . '. Exception code: ' . $e->getCode(), 'err');
            return $errorMessage;
        }
        return true;
    }
}
