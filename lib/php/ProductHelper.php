<?php
/**
 * Class ProductHelper
 */
class ProductHelper {

    /**
     * Returns true if the contactRef has a bundle assigned to at least on if its pins
     *
     * @arg int contactRef
     * @arg int accountId
     * @return bool
     */
    public static function contactHasBundleAssignedToAPin($contactRef, $accountId)
    {
        try {
            $bundleResponse = Hermes_Client_Rest::call('Bundle.getAccountBundle', array(
                'account_id' => $accountId,
                'year' => date('Y'),
                'month' => date('n'),
            ));
        } catch (Exception $e) {
            // If bundle was not found
            if ($e->getCode() == '001:025:001') {
                return false;
            }
        }

        $contactPinInfo = Hermes_Client_Rest::call('getContactPins', array(
            'contact_ref' => $contactRef,
            'active_only' => 'Y',
        ));

        $groups =  Hermes_Client_Rest::call(
            'getPlusPinProductGroups',
            array('pin_ref' => $contactPinInfo[0]['pin_ref'])
        );

        $groupIdsAssignedToPin = array();
        foreach ($groups as $group) {
            $groupIdsAssignedToPin[] = $group['product_group_id'];
        }

        if (in_array($bundleResponse['bundle']['product_group_id'], $groupIdsAssignedToPin)) {
            return true;
        }

        return false;
    }

    /**
     * Assigns a list of pin_refs to all features.
     *
     * @param array $pinRefs
     * @return array|bool
     *   On success, a map of pin_ref to assigned product group ids.
     */
    public static function assignPinsToFeatures(array $pinRefs)
    {
        try {
            $response = Hermes_Client_Rest::call('ProductGroup.assignAllFeaturesToPins', array('pins' => $pinRefs));
            return $response['assigned'];
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('Exception occurred for api_assignAllFeaturesToPins: ' . $e->getMessage());
        }
        return false;
    }

    public static function retrieveMinimumCallCreditAmount(Basket $basket)
    {
        if ($basket->hasBWMBeenAdded()) {
            return sfConfig::get('app_callCredit_minimumAmountWhenBWMPresent', 5);
        } else {
            return sfConfig::get('app_callCredit_minimumAmount', 5);
        }
    }

    /**
     * Assigns the given admin PIN refs to the non-AYMC, non-feature products.
     *
     * @param int $accountId
     * @param array $pinRefs
     * @return bool
     *   Status variable: false = error; true = success
     * @author Maarten Jacobs
     */
    public static function assignAdminPinsToNonAYCMFeatureProducts($accountId, array $pinRefs)
    {
        try {
            $accountProductIdsResponse = Hermes_Client_Rest::call('Plus.retrieveAssignedNonAYCMFeatureAdminProducts', array('account_id' => $accountId));
            $accountProductIds = $accountProductIdsResponse['admin_products'];
        } catch (Exception $e) {
            self::logException("An exception was thrown when trying to retrieve the admin's enabled products.", $e);

            // Push the error up the flow.
            return false;
        }

        if (!$accountProductIds) {
            // If there are no admin products, which aren't AYCM or Feature, then we're done.
            return true;
        } else {
            $success = true;

            foreach ($pinRefs as $pinRef) {
                try {
                    Hermes_Client_Rest::call(
                        'Default.updatePlusPinProductGroups', array(
                            'pin_ref' => $pinRef,
                            'assign_product_group_ids' => $accountProductIds,
                            'use_transactions' => false,
                        )
                    );
                } catch (Exception $e) {
                    self::logException("An exception was thrown when trying to assign one of the new admin's pins to the account products.", $e);

                    $success = false;
                }
            }

            return $success;
        }
    }

    private static function logException($messageContext, Exception $e)
    {
        $errorMessage = sprintf(
            $messageContext . ' The exception message is "%s". And the exception code is "%s".',
            $e->getMessage(),
            $e->getCode()
        );
        sfContext::getInstance()->getLogger()->err($errorMessage);
    }
}
