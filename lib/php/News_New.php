<?php

class News_New
{
    /**
     * @var string $locale
     */
    private $locale;

    /**
     * @var array $articles
     */
    private $articles;

    /**
     * @var array $resources
     */
    private $resources;

    /**
     * Construct
     * @param $locale
     */
    function __construct($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get All News Articles
     * @param array $args
     * @return array $result
     */
    public function getNewsArticles($args = array())
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array();
        try {
            $result         = hermesCallWithCaching('News.getNewsArticles', $args, 'array', 3600, true);
            $this->articles = $result;
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'News.getNewsArticles Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'News.getNewsArticles Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'News.getNewsArticles Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
        return $result;
    }

    /**
     * Sort the Resources from the Articles Array.
     * It is sorted as $resource[$articleID][$resource_type][$resource_value],
     * therefore it can be checked quickly later on for specific values.
     */
    public function sortResourceListFromArticles()
    {
        $resources      = array();
        $resourcesFinal = array();
        foreach ($this->articles as $article) {
            if (isset($article['resource']) && !empty($article['resource'])) {
                $resources[$article['id']] = $article['resource'];
            }
        }

        foreach ($resources as $articleId => $resource) {
            foreach ($resource as $resourceInfo) {
                $resourcesFinal[$articleId][$resourceInfo['resource_type']] = $resourceInfo['resource_value'];
            }
        }
        $this->resources = $resourcesFinal;
    }

    /**
     * Build and Return the Articles Array, for the DataTables, which will be displayed on the Web Page
     * @return string
     */
    public function getNewsListForDataTable()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $articlesArr = array();

        foreach ($this->articles as $article) {
            $articleLink    = url_for('@news') . '/' . $article['url'];
            $articleDate    = $this->getArticleDate($article['accuracy'], $article['first_published']);
            $previewContent = html_entity_decode($article['content']);
            $previewContent = explode(" ", $previewContent);
            $previewContent = array_splice($previewContent, 0, 20);
            $previewContent = implode(" ", $previewContent) . '...';
            $previewContent = strip_tags($previewContent);
            $previewContent = utf8_encode($previewContent);
            $previewImage   = $this->getPreviewImage($article['id'], $articleLink);
            $citationLink   = isset($this->resources[$article['id']]['CitationLink']) ? $this->resources[$article['id']]['CitationLink'] : '';
            $citationText   = isset($this->resources[$article['id']]['CitationText']) ? $this->resources[$article['id']]['CitationText'] : '';

            // Start Building the Actual News Block
            $articlesList = $previewImage;
            $articlesList .= '<h2><a href="' . $articleLink . '">' . $article['title'] . '</a></h2>';
            $articlesList .= '<h4>';
            $articlesList .= ('0000-00-00 00:00:00' !== $article['first_published']) ? $articleDate . ', ' : '';
            $articlesList .= '<a target="_blank" class="dotted external" title="' . $citationText . '" href="' . $citationLink . '">' . $citationText . '</a>';
            $articlesList .= '</h4>';
            $articlesList .= '<p>' . $previewContent . '</p>';
            $articlesList .= '<p><a href="' . $articleLink . '"><span>Read more</span></p>';

            $articlesArr[][0] = $articlesList;
        }

        return $articlesArr;
    }

    /**
     * Get the Article Date
     * @param string $accuracy
     * @param datetime $publishDate
     * @return string date
     */
    private function getArticleDate($accuracy, $publishDate)
    {
        $accuracyArr = array(
            'Year'  => 'Y',
            'Month' => 'F Y',
            'Date'  => 'd F Y',
        );

        if (isset($accuracyArr[$accuracy])) {
            return date($accuracyArr[$accuracy], strtotime($publishDate));
        } else {
            // Return the 'Date' Format
            return date($accuracyArr['Date'], strtotime($publishDate));
        }
    }

    /**
     * Get the Preview Image used on the News Listing Page
     * @param $articleId
     * @param $articleLink
     * @return string
     */
    private function getPreviewImage($articleId, $articleLink)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('DataUri');

        $defaultImagePath = '/sfimages/conference-call.png';
        $imgAlt = 'Thumbnail';

        if (!isset($this->resources[$articleId]['ArticleThumbnail'])) {
            if (file_exists($defaultImagePath)) {
                $imgSrc = getDataURI($defaultImagePath);
            } elseif (file_exists(sfConfig::get('sf_web_dir') . $defaultImagePath)) {
                $imgSrc = getDataURI(sfConfig::get('sf_web_dir') . $defaultImagePath);
            } else {
                sfContext::getInstance()->getLogger()->err(
                    'News Thumbnail Image Not Found: ' . $defaultImagePath . ', for article: ' . $articleId
                );
                $imgSrc = '';
            }
        } else {
            if (isset($this->resources[$articleId]['ArticleThumbnailAlt'])) {
                $imgAlt = $this->resources[$articleId]['ArticleThumbnailAlt'];
            }

            if (file_exists($this->resources[$articleId]['ArticleThumbnail'])) {
                $imgSrc = getDataURI($this->resources[$articleId]['ArticleThumbnail']);
            } elseif (file_exists(sfConfig::get('sf_web_dir') . $this->resources[$articleId]['ArticleThumbnail'])) {
                $imgSrc = getDataURI(sfConfig::get('sf_web_dir') . $this->resources[$articleId]['ArticleThumbnail']);
            } elseif (file_exists($defaultImagePath)) {
                sfContext::getInstance()->getLogger()->err(
                    'News Thumbnail Image Not Found: ' . $this->resources[$articleId]['ArticleThumbnail'] .
                    ', using default Image: ' . $defaultImagePath . ', for article: ' . $articleId
                );
                $imgSrc = getDataURI($defaultImagePath);
            } elseif (file_exists(sfConfig::get('sf_web_dir') . $defaultImagePath)) {
                sfContext::getInstance()->getLogger()->err(
                    'News Thumbnail Image Not Found: ' . $this->resources[$articleId]['ArticleThumbnail'] .
                    ', using default Image: ' . $defaultImagePath . ', for article: ' . $articleId
                );
                $imgSrc = getDataURI(sfConfig::get('sf_web_dir') . $defaultImagePath);
            } else {
                sfContext::getInstance()->getLogger()->err(
                    'News Thumbnail Images Not Found: ' . $this->resources[$articleId]['ArticleThumbnail'] .
                    ' or ' . $defaultImagePath . ', for article: ' . $articleId
                );
                $imgSrc = '';
            }
        }

        return '<a href="' . $articleLink . '"><img src="' . $imgSrc . '" width="90" alt="' . $imgAlt . '"/></a>';
    }

    /**
     * @return array
     */
    public function getArticleImageInformation()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('DataUri');

        $articleID = $this->articles[0]['id'];
        $resources = $this->resources[$articleID];

        return array(
            'ArticleImage' => (isset($resources['ArticleImage'])) ? getDataURI($resources['ArticleImage']) : '',
            'ArticleImageAlt' =>  (isset($resources['ArticleImageAlt'])) ? $resources['ArticleImageAlt'] : 'News Image',
        );
    }

    /**
     * @return array
     */
    public function getCitationInformation()
    {
        $articleID = $this->articles[0]['id'];
        $resources = $this->resources[$articleID];

        return array(
            'CitationLink' => (isset($resources['CitationLink'])) ? $resources['CitationLink'] : '#',
            'CitationText' =>  (isset($resources['CitationText'])) ? $resources['CitationText'] : 'Link to source',
        );
    }

}