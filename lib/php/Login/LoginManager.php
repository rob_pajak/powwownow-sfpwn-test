<?php

/**
 * Class LoginManager
 */
class LoginManager extends LoginAbstract
{
    /**
     * @param $emailAddress
     * @param $password
     * @param $rememberUser
     * @return sfUser|void
     * @throws LoginException
     */
    public function doLogin($emailAddress, $password, $rememberUser)
    {
        $isValidContact = $this->doAuthenticateContact($emailAddress, $password);
        if (is_array($isValidContact)) {
            if ($rememberUser == 'true') {
                $this->setCookieToRememberUser('true');
                $this->setPhpCookieSession();
            } else {
                $this->setCookieToRememberUser('false');
            }
            return $this->setCredentials($emailAddress, $isValidContact);
        }
        throw new LoginException('doAuthenticateContact failed with args (' . $emailAddress . ')', 500);
    }

    /**
     * @return bool|void
     */
    public function doLogout()
    {
        parent::doLogOut();
        $this->destroyCookieToRememberUser();
        return $this->destroyPhpCookieSession();
    }

    /**
     * @param $status
     * @return bool
     */
    protected function setCookieToRememberUser($status)
    {
        return setcookie('KEEP_ME_SIGNED_IN', $status);
    }

    /**
     * @return bool
     */
    protected function destroyCookieToRememberUser()
    {
        return setcookie('KEEP_ME_SIGNED_IN', '');
    }

    /**
     *
     */
    protected function setPhpCookieSession()
    {
        return session_set_cookie_params(time()+60*60*24*30);
    }

    /**
     * @return bool
     */
    protected function destroyPhpCookieSession()
    {
        return session_destroy();
    }
}