<?php

require_once('LoginException.php');

/**
 * Class LoginAbstract
 */
abstract class LoginAbstract
{

    /**
     * @var sfUser
     */
    protected $user;

    /**
     * @throws sfException
     */
    public function __construct()
    {
        $this->user = sfContext::getInstance()->getUser();
    }

    /**
     * @param $emailAddress
     * @param $password
     * @return sfUser
     * @throws LoginException
     */
    public function doLogin($emailAddress, $password)
    {
        $isValidContact = $this->doAuthenticateContact($emailAddress, $password);

        if (is_array($isValidContact)) {
            return $this->setCredentials($emailAddress, $isValidContact);
        }

        throw new LoginException('doAuthenticateContact failed with args (' . $emailAddress . ')', 500);
    }

    /**
     *
     */
    public function doLogOut()
    {
        $this->user->setAuthenticated(false);
        $this->user->clearCredentials();
        $this->user->getAttributeHolder()->removeNamespace();
    }

    /**
     * @param $emailAddress
     * @param $password
     * @return array|bool
     */
    protected function doAuthenticateContact($emailAddress, $password)
    {
        $ipRetriever = new IpRetriever();
        $contactDetails = array();
        $contactDetails['email']    = $emailAddress;
        $contactDetails['password'] = $password;
        $contactDetails['ip_address'] = $ipRetriever->retrieveIPAddress();

        try {
            $result = Hermes_Client_Rest::call('authenticateContact', $contactDetails);
        } catch (Hermes_Client_Exception $hermesClientException) {
            $result = $hermesClientException->getCode();
        } catch (Exception $exception) {
            $result = $exception->getCode();
        }
        return $result;
    }

    /**
     * @param $emailAddress
     * @param $contactDetails
     * @return sfUser
     */
    protected function setCredentials($emailAddress, $contactDetails)
    {
        $this->user->clearCredentials();
        $this->user->setAuthenticated(true);
        $this->user->setAttribute('authenticated', true);
        $this->user->setAttribute('email', $emailAddress);
        $accountId = $contactDetails['account_id'];

        foreach ($contactDetails as $key => $value) {
            $this->user->setAttribute($key, $value);
        }

        switch ($contactDetails['service_user']) {
            case 'POWWOWNOW':
                $this->user->addCredentials('powwownow', 'user', 'POWWOWNOW');
                $this->user->setAttribute('service', 'powwownow');
                $this->user->setAttribute('user_type', 'user');
                break;
            case 'PLUS_USER':
                $this->user->addCredentials('plus', 'user', 'PLUS_USER');
                $this->user->setAttribute('account_id', $accountId);
                $this->user->setAttribute('service', 'plus');
                $this->user->setAttribute('user_type', 'user');
                break;
            case 'PLUS_ADMIN':
                $this->user->addCredentials('plus', 'admin', 'PLUS_ADMIN');
                $this->user->setAttribute('account_id', $accountId);
                $this->user->setAttribute('service', 'plus');
                $this->user->setAttribute('user_type', 'admin');
                break;
            case 'PREMIUM_USER':
                $this->user->addCredentials('premium', 'user', 'PREMIUM_USER');
                $this->user->setAttribute('service', 'premium');
                $this->user->setAttribute('user_type', 'user');
                break;
            case 'PREMIUM_ADMIN':
                $this->user->addCredentials('premium', 'admin', 'PREMIUM_ADMIN');
                $this->user->setAttribute('service', 'premium');
                $this->user->setAttribute('user_type', 'admin');
                break;
        }
        return $this->user;
    }
}
