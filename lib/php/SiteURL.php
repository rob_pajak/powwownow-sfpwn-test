<?php
class SiteURL {
    public static function getSiteURL($country)
    {
        //Hack for functional testing
        if (isset($_SERVER['APPLICATION_ENV'])) {
            $env = $_SERVER['APPLICATION_ENV'];
        } elseif (getenv('APPLICATION_ENV')) {
            $env = getenv('APPLICATION_ENV');
        } elseif (php_sapi_name() === 'cli') {
            $env = 'staging';
        }

        if ('dev' === $env) {
            // Stage01 For International Website, DEU using dev03, GBR using dev.powwownow.co.uk
            switch ($country) {
                case "AUT":
                    return "aut.powwownow.via-vox.net";
                case "BEL":
                    return "bel.powwownow.via-vox.net";
                case "CAN":
                    return "can.powwownow.via-vox.net";
                case "FRA":
                    return "fra.powwownow.via-vox.net";
                case "DEU":
                    return "pwn-web.de.dev03.rm.via-vox.net";
                case "IRL":
                    return "irl.powwownow.via-vox.net";
                case "ITA":
                    return "ita.powwownow.via-vox.net";
                case "NLD":
                    return "nld.powwownow.via-vox.net";
                case "POL":
                    return "pol.powwownow.via-vox.net";
                case "ZAF":
                    return "zaf.powwownow.via-vox.net";
                case "ESP":
                    return "esp.powwownow.via-vox.net";
                case "SWE":
                    return "swe.powwownow.via-vox.net";
                case "CHE":
                    return "che.powwownow.via-vox.net";
                case "USA":
                    return "usa.powwownow.via-vox.net";
                case "GBR":
                default:
                    return "dev.powwownow.co.uk";
            }
        } elseif ('staging' === $env) {
            // Stage01 For International Website, DEU using stage06, GBR using staging.powwownow.co.uk
            switch ($country) {
                case "AUT":
                    return "staging.powwownow.at";
                case "BEL":
                    return "staging.powwownow.be";
                case "CAN":
                    return "staging.can.powwownow.com";
                case "FRA":
                    return "staging.powwownow.fr";
                case "DEU":
                    return "pwn-web.de.stage06.rm.via-vox.net";
                case "IRL":
                    return "staging.powwownow.ie";
                case "ITA":
                    return "staging.powwownow.it";
                case "NLD":
                    return "staging.powwownow.nl";
                case "POL":
                    return "staging.powwownow.pl";
                case "ZAF":
                    return "staging.powwownow.co.za";
                case "ESP":
                    return "staging.powwownow.es";
                case "SWE":
                    return "staging.powwownow.se";
                case "CHE":
                    return "staging.powwownow.ch";
                case "USA":
                    return "staging.powwownow.com";
                case "GBR":
                default:
                    return "staging.powwownow.co.uk";
            }
        } else {
            // Use Live Websites for all
            switch ($country) {
                case "AUT":
                    return "www.powwownow.at";
                case "BEL":
                    return "www.powwownow.be";
                case "CAN":
                    return "can.powwownow.com";
                case "FRA":
                    return "www.powwownow.fr";
                case "DEU":
                    return "www.powwownow.de";
                case "IRL":
                    return "www.powwownow.ie";
                case "ITA":
                    return "www.powwownow.it";
                case "NLD":
                    return "www.powwownow.nl";
                case "POL":
                    return "www.powwownow.pl";
                case "ZAF":
                    return "www.powwownow.co.za";
                case "ESP":
                    return "www.powwownow.es";
                case "SWE":
                    return "www.powwownow.se";
                case "CHE":
                    return "www.powwownow.ch";
                case "USA":
                    return "www.powwownow.com";
                case "GBR":
                default:
                    return "www.powwownow.co.uk";
            }
        }


    }
}



