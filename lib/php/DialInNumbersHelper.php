<?php

/**
 * Helper methods for working with Dial-In-Numbers
 *
 * @author Asfer Tamimi
 */
class DialInNumbersHelper {

    /**
     * Obtain the Share Cost Dial In Numbers for a Specific Language
     *
     * @param  string  $language
     * @return array   $result
     * @author Asfer Tamimi
     *
     */
    public static function getAllSharedCostDialInNumbers($language = 'en') {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array();

        try {
            $result = hermesCallWithCaching('getAllSharedCostDialInNumbers', array('language_code' => $language),'array', 3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getAllSharedCostDialInNumbers Hermes call has timed out.');
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getAllSharedCostDialInNumbers Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('getAllSharedCostDialInNumbers Failed. Language: ' . $language . ', Error Message: ' . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode());
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        }

        return $result;
    }

    /**
     * Clean the the Share Cost Dial In Numbers Array
     *
     * @param  array $dialInNumbers
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function cleanGetAllSharedCostDialInNumbers($dialInNumbers) {
        $result = array();
        foreach ($dialInNumbers as $k => $v) {
            foreach ($v as $j => $row) {
                $germanCheck = ($row['country_code'] == 'DEU' && $row['is_a_default_mobile_number'] == 1) ? ' (mobile number)' : '';
                $mobileCheck = ($row['mobile'] == 0) ? ' <a href="#postcomment"><sup>1</sup></a>' : '';
                $internCheck = ($row['dial_international'] == 0 && $row['international_formatted'] == '') ?  ' <a href="#postcomment"><sup>2</sup></a>' : '';
                $natioaCheck = (strlen($row['national_formatted']) == 5) ? ' <a href="#postcomment"><sup>3</sup></a>' : '';
                $mobDefCheck = ($row['is_a_default_mobile_number'] == 1) ? ' (mobile number)' : '';

                // Cost Calculations
                if ($row['country_code'] == 'DEU' && $row['is_a_default_mobile_number'] == 1) {
                    $cpmCheck = 'Variable';
                } elseif (empty($row['cost_per_minute'])) {
                    $cpmCheck = 'Variable';
                } else {
                    // Change Requested by Sarah @ 12-03-2013
                    if ($row['iso2'] == 'SE' && $row['currency_name_translated'] == 'Swedish krona') {
                        $cpm = $row['cost_per_minute_value'] * 1;
                    } elseif ($row['iso2'] == 'PL' && $row['currency_name_translated'] == 'Polish zloty') {
                        $cpm = $row['cost_per_minute_value'] * 1;
                    } else {
                        $cpm = number_format(($row['cost_per_minute_value']*100),1);
                    }

                    if ($cpm == round($cpm)) $cpm = round($cpm);
                    $cpmCheck = $cpm . ' ' . $row['currency_name_translated'];
                }

                 $result[] = array(
                    'country_name_language'   => $row['country_name_translated'] . $mobileCheck . $internCheck . $natioaCheck . $mobDefCheck,
                    'national_formatted'      => $row['national_formatted'],
                    'international_formatted' => $row['lang'],
                    'cpm_check'               => $cpmCheck,
                );
            }
        }
        return $result;
    }

    /**
     * Obtain Dial In Numbers
     * @param int $service
     * @param string $locale
     * @param bool $mobile
     * @return array $result
     */
    public static function getGetDialInNumbers($service = 801, $locale = 'en_GB', $mobile = false) {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array();
        $params = array(
            'service_ref' => $service,
            'locale'      => $locale
        );
        if ($mobile) {
            $params['get_default_mobile'] = true;
        }

        try {
            $result = hermesCallWithCaching('getDialInNumbers', $params, 'array', 3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getDialInNumbers Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getDialInNumbers Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('getDialInNumbers Failed. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode());
        }

        return $result;
    }

    /**
     * Get the Local Dial In Numbers.
     * Will Return the Local Shared Cost and Mobile Dial In Numbers
     *
     * @param array $args
     *    $args['locale'] - The Current Locale
     *    $args['service_ref'] - The Service Ref
     *    $args['country'] - The Current Country
     *
     * @return array $result
     */
    public static function getLocalDialInNumbers($args) {
        $result = array();

        $result['localList']  = self::getGetDialInNumbers($args['service_ref'], $args['locale'], false);
        $result['mobileList'] = self::getGetDialInNumbers($args['service_ref'], $args['locale'], true);

        $result['local']  = getLocalDialInNumber($result['localList'], $args['locale'], $args['country']);
        $result['mobile'] = getLocalMobileDialInNumber($result['mobileList'], $args['locale'], $args['country']);

        if ('' == $result['local']) {
            $result['local'] = $args['default_local'];
        }

        if ('' == $result['mobile']) {
            $result['mobile'] = $args['default_mobile'];
        }

        return $result;
    }


}
