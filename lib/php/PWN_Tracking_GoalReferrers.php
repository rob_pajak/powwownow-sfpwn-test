<?php
/**
 * Uses data from the PWN_Tracking_PageVisitLog and extracts the page referrer
 * information
 *
 * Everything you need to do with this class can be done by:
 * PWN_Tracking_GoalReferrers::achieved($goal, $pinRef);
 * All methods are defined as protected
 */

class  PWN_Tracking_GoalReferrers
{
    /**
     * Records a new goal and stores it in DB
     *
     * This static function does everything a user of this class
     * needs to do all in one
     *
     * @param string $goal A string to identify the goal, eg 'plus'
     * @param int $pinRef Pin ref of the goal achieved
     * @param null|bool $updateLogOnFailure If true, will always marked the referrers in the log
     *                    as persistently stored, even if an exception occurred.
     * @return array
     * @throws Exception
     */
    public static function achieved($goal, $pinRef, $updateLogOnFailure = false)
    {
        $logger      = new PWN_Tracking_PageVisitLog();

        try {
            // Get the page views from the log
            $logContents = $logger->getContents();

            // Extract the referrer data from the log
            $goalObj  = new PWN_Tracking_GoalReferrers();
            $goalData = $goalObj->getData($goal, $pinRef, $logContents);

            // Store the referrer data into DB
            $insertedRowIds = $goalObj->storeData($goalData);

            // Update the page log noting that the current referrers in the log
            // have now been stored in the DB
            $logger->updateAll('referrerPersistentlyStored', true);

        } catch (Exception $e) {

            if ($updateLogOnFailure) {
                $logger->updateAll('referrerPersistentlyStored', true);
            }
            throw $e;

        }

        return $insertedRowIds;
    }

    /**
     * If session_start has not already been called, then start it
     */
    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    /**
     * Takes data from the log and turns it in to an array to pass to $this->storeData
     *
     * @param string $goal A string to identify the goal, eg 'plus'
     * @param int $pinRef Pin ref of the goal achieved
     * @param array $log Entries from the pge log
     * @return array
     * @throws Exception
     */
    protected function getData($goal, $pinRef, Array $log)
    {
        if (0 === count($log)) {
            sfContext::getInstance()->getLogger()->err(__METHOD__.', Page Visits Log is Empty');
            return array();
        }

        if (!is_numeric($pinRef)) {
            sfContext::getInstance()->getLogger()->err(__METHOD__.", Supplied PIN Ref: $pinRef is not Numeric");
            throw new Exception('Supplied $pinRef "' . $pinRef . '"is not numeric');
        }

        if (empty($goal)) {
            sfContext::getInstance()->getLogger()->err(__METHOD__.', Supplied goal is empty');
            throw new Exception('Supplied goal is empty');
        }

        // A user can register with more than one pin, store the first pin they
        // registered with so we can tie future pins to the same user
        if (!$this->_getInitialPinRef()) {
            $this->_setInitialPinRef($pinRef);
        }

        // Validate and then separate the log in to page views which have been
        // persistently stored and those which haven't
        //
        // If the log entry has already been persistently stored then we generally
        // don't need it unless the user succeeds more than one goal within the
        // same referrer, then we need to get the last referrer from the $storedLog
        $storedLog   = array();
        $unstoredLog = array();

        // Stores the referrer information we are about to extract
        $storedReferrers   = array();
        $unstoredReferrers = array();

        // Loop round the page visit log, separating the log in to stored and
        // unstored, and extracting each different referrer to $storedReferrers and $unstoredReferrers
        foreach ($log as $k => $pageVisit) {
            if (!(!$pageVisit || $pageVisit instanceof PWN_Tracking_PageVisitLog_Entry)) {
                sfContext::getInstance()->getLogger()->err(
                    "Corrupt PWN_Tracking_PageVisitLog_Entry Found on... Goal: $goal, PIN Ref: $pinRef, Entry: " . serialize($pageVisit)
                );
                continue;
            }

            if ($pageVisit->referrerPersistentlyStored) {
                $storedLog[] = $pageVisit;
            } else {
                $unstoredLog[] = $pageVisit;
            }

            // Get the data if the page view referrer was from a different domain
            // to the current server, or if it came from the blog
            if (
                (parse_url($pageVisit->referrerUrl, PHP_URL_HOST) != $_SERVER['HTTP_HOST'] && parse_url(
                        $pageVisit->referrerUrl,
                        PHP_URL_HOST
                    ) != $_SERVER['SERVER_NAME'])
                || strpos($pageVisit->referrerUrl, 'powwownow.co.uk/blog') !== false
            ) {

                // Obtain the Browser Information from Browscap.ini
                $info = @get_browser($pageVisit->httpUserAgent, true);

                $referrer = array(
                    'initial_url_visit_datetime' => $pageVisit->datetime,
                    'initial_url'                => $pageVisit->url,
                    'referrer_url'               => $pageVisit->referrerUrl,
                    'page_visit_number'          => $k,
                    'pin_ref'                    => $pinRef,
                    'initial_pin_ref'            => $this->_getInitialPinRef(),
                    'ip'                         => $pageVisit->ip,
                    'http_user_agent'            => $pageVisit->httpUserAgent,
                    'mobile_tablet_name'         => $info['ismobiledevice'],
                    'operating_system'           => $info['platform'],
                    'browser_name'               => $info['browser'],
                    'browser_version'            => $info['version'],
                    'screen_width'               => (!empty($_COOKIE['screenWidth'])) ? $_COOKIE['screenWidth'] : 'Not Detected',
                    'screen_height'              => (!empty($_COOKIE['screenHeight'])) ? $_COOKIE['screenHeight'] : 'Not Detected',
                );

                $referrer['screen_resolution'] = ('Not Detected' != $referrer['screen_width'] && 'Not Detected' != $referrer['screen_height']) ? $referrer['screen_width'] . ' x ' . $referrer['screen_height'] . ' pixels' : 'Not Detected';

                if ($pageVisit->referrerPersistentlyStored) {
                    $storedReferrers[] = $referrer;
                } else {
                    $unstoredReferrers[] = $referrer;
                }
            }
        }

        if (0 == count($unstoredReferrers) && 0 < count($storedReferrers)) {
            $unstoredReferrers[] = end($storedReferrers);
        }

        // If we still do not have any $unstoredReferrers then we have managed to reach a strange edge case.
        // In this case set the referrer as empty and get the other details from the first unstored page view presuming
        // we have one, otherwise put in now value.
        if (empty($unstoredReferrers)) {

            $firstPageView = reset($unstoredLog);

            if (!empty($firstPageView)) {
                $assumedDateTime  = $firstPageView->datetime;
                $assumedUrl       = $firstPageView->url;
                $assumedReferrer  = $firstPageView->referrerUrl;
                $assumedIp        = $firstPageView->ip;
                $assumedUserAgent = $firstPageView->httpUserAgent;
            } else {
                $assumedDateTime  = date('Y-m-d H:i:s');
                $assumedUrl       = '';
                $assumedReferrer  = '';
                $assumedIp        = '';
                $assumedUserAgent = '';
            }

            $screenWidth  = (!empty($_COOKIE['screenWidth'])) ? $_COOKIE['screenWidth'] : 'Not Detected';
            $screenHeight = (!empty($_COOKIE['screenHeight'])) ? $_COOKIE['screenHeight'] : 'Not Detected';
            $screenRes    = ('Not Detected' != $screenWidth && 'Not Detected' != $screenHeight) ? $screenWidth . ' x ' . $screenHeight . ' pixels' : 'Not Detected';
            $pinRef       = (!is_null($pinRef)) ? $pinRef : $this->_getInitialPinRef();

            // Obtain the Browser Information from Browscap.ini
            $info = @get_browser($assumedUserAgent, true);

            $unstoredReferrers[] = array(
                'initial_url_visit_datetime' => $assumedDateTime,
                'initial_url'                => $assumedUrl,
                'referrer_url'               => $assumedReferrer,
                'page_visit_number'          => 0,
                'pin_ref'                    => $pinRef,
                'initial_pin_ref'            => $this->_getInitialPinRef(),
                'ip'                         => $assumedIp,
                'http_user_agent'            => $assumedUserAgent,
                'mobile_tablet_name'         => isset($info['ismobiledevice']) ? $info['ismobiledevice'] : '',
                'operating_system'           => isset($info['platform']) ? $info['platform'] : '',
                'browser_name'               => isset($info['browser']) ? $info['browser'] : '',
                'browser_version'            => isset($info['version']) ? $info['version'] : '',
                'screen_width'               => $screenWidth,
                'screen_height'              => $screenHeight,
                'screen_resolution'          => $screenRes
            );

            // Logging Added to track this strange behaviour
            sfContext::getInstance()->getLogger()->err(
                "Funky Edge Case Web User Tracking Issue RAW Information: " . serialize($log)
            );
            sfContext::getInstance()->getLogger()->err(
                "Funky Edge Case Web User Tracking Issue Sorted Information: " . print_r($unstoredReferrers, true)
            );
        }

        // For each referrer, calculate how many page views between landing
        // on the site and achieving the goal (the goal always occurs on
        // the last page view)
        foreach ($unstoredReferrers as $k => $v) {
            // If there is another referrer after this one..
            if (isset($unstoredReferrers[$k + 1])) {
                // ... then count page visits between this referrer and the next
                $unstoredReferrers[$k]['pages_visited_count'] = $unstoredReferrers[$k + 1]['page_visit_number'] - $unstoredReferrers[$k]['page_visit_number'];
            } else {
                // ... otherwise count total number of page visits, and subtract this page visit number
                $unstoredReferrers[$k]['pages_visited_count'] = count(
                        $log
                    ) - $unstoredReferrers[$k]['page_visit_number'];
            }
        }

        // We don't need page_visit_number anymore, it was only to calculate
        // pages_visited_count
        foreach ($unstoredReferrers as $k => $v) {
            unset($unstoredReferrers[$k]['page_visit_number']);
        }

        // As this function is only called when a goal is achieved, add
        // the goal and url the last referrer
        $lastPageVisited = end($unstoredLog);
        if (!empty($lastPageVisited)) {
            $lastVisitedUrl      = $lastPageVisited->url;
            $lastVisitedReferrer = $lastPageVisited->referrerUrl;
        } else {
            $lastVisitedUrl      = '';
            $lastVisitedReferrer = '';
        }
        $unstoredReferrers[count($unstoredReferrers) - 1]['goal']              = $goal;
        $unstoredReferrers[count($unstoredReferrers) - 1]['goal_url']          = $lastVisitedUrl;
        $unstoredReferrers[count($unstoredReferrers) - 1]['goal_referrer_url'] = $lastVisitedReferrer;

        return $unstoredReferrers;
    }

    /**
     * Calls Hermes to store the referrer data in DB
     *
     * @param $goalData
     * @return array
     * @throws Exception
     */
    protected function storeData($goalData)
    {
        //Affiliate tracking
        $cnt = count($goalData);
        for ($a = 0; $a < $cnt; $a++) {
            if (isset($goalData[$a]) && (isset($goalData[$a]['initial_url'])) && (preg_match(
                        '/\/affiliate\/([a-zA-Z0-9_]{1,30})\/{0,1}([a-zA-Z0-9_]{0,5})\/{0,1}$/',
                        $goalData[$a]['initial_url'],
                        $match
                    ) != 0)
            ) {
                if (trim($match[1]) != '') {
                    $goalData[$a]['affiliate_code'] = substr($match[1], 0, 30);
                }
            }
            if (isset($match[2]) && trim($match[2]) != '') {
                $goalData[$a]['affiliate_code_campaign_id'] = substr($match[2], 0, 5);
            }
        }

        $response = $this->webTrackingGoalReferrersSave($goalData);
        if (isset($response['errorCode'])) {
            throw new Exception('Hermes failed to store webTrackingGoalReferrer data: Message: ' . $response['errorMessage'] .
                ' ErrorCode: ' . $response['errorCode'] .
                ' HttpCode: ' . $response['httpCode'] .
                ' httpStatus: ' . $response['httpStatus']
            );
        }

        $return = array();

        foreach ($response['goalReferrers'] as $v) {
            $return[] = $v;
        }

        return $return;

    }

    /**
     * Takes the tracking data array, SERIALIZES IT, and called Hermes
     * Catches any errors and throws them as an exception
     * @param array $referrerData
     * @return array|mixed
     */
    protected function webTrackingGoalReferrersSave(array $referrerData)
    {
        try {
            return Hermes_Client_Rest::callPOST(
                'webTrackingGoalReferrersSave',
                array('referrer_data' => serialize($referrerData)),
                'array'
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'webTrackingGoalReferrersSave Hermes call has timed out, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode() . ', HTTP Code: ' . $e->getHTTPStatusCode(
                ) . ', httpStatus: ' . $e->getHTTPStatus()
            );
            return array(
                'errorMessage' => $e->getMessage(),
                'errorCode'    => $e->getCode(),
                'httpCode'     => $e->getHTTPStatusCode(),
                'httpStatus'   => $e->getHTTPStatus()
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'webTrackingGoalReferrersSave Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode() . ', HTTP Code: ' . $e->getHTTPStatusCode(
                ) . ', httpStatus: ' . $e->getHTTPStatus()
            );
            return array(
                'errorMessage' => $e->getMessage(),
                'errorCode'    => $e->getCode(),
                'httpCode'     => $e->getHTTPStatusCode(),
                'httpStatus'   => $e->getHTTPStatus()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'webTrackingGoalReferrersSave Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            return array(
                'errorMessage' => $e->getMessage(),
                'errorCode'    => $e->getCode(),
            );
        }
    }

    /**
     * Sets the pin ref a user first registers with
     * @param int
     * @return self
     */
    protected function _setInitialPinRef($pinRef)
    {
        $_SESSION['initial_pin_ref'] = $pinRef;
        return $this;
    }

    /**
     * Gets the pin ref a user first registered with
     * @return int|bool Initial pin_ref, or false if not found
     */
    protected function _getInitialPinRef()
    {
        if (isset($_SESSION['initial_pin_ref'])) {
            return $_SESSION['initial_pin_ref'];
        }
        return false;
    }
}
