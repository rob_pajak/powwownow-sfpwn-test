<?php

/**
 * Class Shomei
 *
 * This Class is used to Register for a Free PIN for Shomei
 *
 * You can read more about the API at https://sites.google.com/site/shomeiapi/v1
 *
 * @author Asfer Tamimi
 *
 */
class Shomei
{
    /**
     * AdvertiserName [test, uk, deu]
     * @var
     */
    private $advertiserName;

    /**
     * Set the AdvertiserName
     */
    public function __construct()
    {
        if (isset($_SERVER['APPLICATION_ENV']) &&
            (in_array($_SERVER['APPLICATION_ENV'], array('dev', 'sqi', 'staging')))
        ) {
            $this->advertiserName = 'test';
        } else {
            $this->advertiserName = 'uk';
        }
    }

    /**
     * Register a Free Pin User for Shomei
     * Used on All Free PIN Registrations
     * @param string $pinRef
     * @param string | bool $creationTime [Default = false]
     * @param string $countryCode [Default = GBR]
     * @return array
     */
    public function registerFreePin($pinRef, $creationTime = false, $countryCode = 'GBR')
    {
        return HermesCommon::shomeiRegisterFreePinForShomei(
            array(
                'creation_time'   => $creationTime,
                'pin_ref'         => $pinRef,
                'advertiser_name' => $this->advertiserName,
                'country_code'    => $countryCode
            )
        );
    }
}
