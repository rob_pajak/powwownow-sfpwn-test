<?php

/**
 * Clears cache for the listed actions.
 *
 * When certain actions happen (delete pin, update contact etc), cache
 * needs to be cleared for multiple functions. The methods in this class
 * clear the relevant cache for you
 *
 * @todo:
 * This class could be expanded to clear the cache for admins associated
 * with a contact or pin etc. But due to time limitations its been decided
 * that we will not do this at this stage
 *
 * As of 2011-12-01, these methods are cached but deemed clearing was not needed
 * PWN_Cacher::clearContent('PWN_App::_getDialinNumbersContent', $locale);
 * PWN_Cacher::clearContent('PWN_App::_getDefaultDialinNumber', $countryCode);
 * PWN_Cacher::clearContent('PWN_App::_getNumberRatesTableContents', $languageCode);
 * PWN_Cacher::clearContent('MYPWN_App::_getDialInNumbers', array('service_ref' => $_SESSION['service_ref'], 'locale' => $locale ));
 * PWN_Cacher::clearContent('MYPWN_App::_getConferenceIds', $args);
 * PWN_Cacher::clearContent('MYPWN_App::_doYuuguuRegistrationLegacy', $args);
 * PWN_Cacher::clearContent('PWN_App::_getChangeRegionContent', $locale);
 * PWN_Cacher::clearContent('MYPWN_App::_getCallHistory', $args);
 * PWN_Cacher::clearContent('MYPWN_App::_getRecording', $args);
 * PWN_Cacher::clearContent('getAllNewsArticles', null);
 * PWN_Cacher::clearContent('file_get_contents', $feedUrl);
 *
 */
class PWN_Cache_Clearer {

    /**
     * Clears cache for pin creation, deletion, modification etc
     *
     * @param $contactRef int
     * @param $language string
     *
     * @return array Method names and bool of state of status clear
     */
    public static function modifiedPin($contactRef, $language = 'en') {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $output     = array();
        $exceptions = array();

        try {
            $method = 'MYPWN_App::_myPinsContent';
            $output[$method] = PWN_Cacher::clearContent($method, $contactRef);
            // $output[''] = hermesCallRemoveCaching('',array('contact_ref' => $contactRef), 'all'); // Invalid and Unused Method
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getContactPins';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            $output['getContactPins'] = hermesCallRemoveCaching('getContactPins',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getContactByRef';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            $output['getContactByRef'] = hermesCallRemoveCaching('getContactByRef',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getServiceRefsByContactRef';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            $output['getServiceRefs'] = hermesCallRemoveCaching('getServiceRefs',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getServiceRefsAsHtml';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            // $output['getContactByRef'] = hermesCallRemoveCaching('getContactByRef',array('contact_ref' => $contactRef), 'all'); // Not Needed, used above.
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getContactDialInNumbers';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef, 'language' => $language));
            $output['getContactDialInNumbers'] = hermesCallRemoveCaching('getContactDialInNumbers',array('contact_ref' => $contactRef, 'language' => $language), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getPremiumAccountDetailsByContactRef';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            $output['getPremiumAccountDetailsByContactRef'] = hermesCallRemoveCaching('getPremiumAccountDetailsByContactRef',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getAccountPinsByContactRef';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            $output['getAccountPinsByContactRef'] = hermesCallRemoveCaching('getAccountPinsByContactRef',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getAccountContactsByContactRef';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            //$output['getContactByRef'] = hermesCallRemoveCaching('getContactByRef',array('contact_ref' => $contactRef), 'all'); // Not Needed, used above.
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getDialledNumbersAsHtml';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            $output['getDialledNumbers'] = hermesCallRemoveCaching('getDialledNumbers',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        // plus specific

        try {
            $method = 'MYPWN_App::_getContactsAccountPinsAsPairs';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            //$output['getAccountPinsByContactRef'] = hermesCallRemoveCaching('getAccountPinsByContactRef',array('contact_ref' => $contactRef), 'all'); // Not Needed, used above
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getContactPinsAsPairs';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            //$output['getContactPins'] = hermesCallRemoveCaching('getContactPins',array('contact_ref' => $contactRef), 'all'); // Not Needed, used above
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'PWN_App::_getContactDialinNumbers';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            $output['getContactDialInNumbers'] = hermesCallRemoveCaching('getContactDialInNumbers',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'PWN_App::_getContactDialinNumbers';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef, 'language' => 'en'));
            $output['getContactDialInNumbers'] = hermesCallRemoveCaching('getContactDialInNumbers',array('contact_ref' => $contactRef, 'language' => 'en'), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        if (!empty($exceptions)) {
            throw new Exception('Not all cache was successfully cleared:' . serialize($exceptions) . serialize($output));
        }

        return $output;

    }

    /**
     * Clears cache for contact creation, deletion, modification etc
     *
     * @param $contactRef int
     *
     * @return array Method names and bool of state of status clear
     */
    public static function modifiedContact($contactRef) {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $output     = array();
        $exceptions = array();

        try {
            $method = 'MYPWN_App::_getContactByRef';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            $output['getContactByRef'] = hermesCallRemoveCaching('getContactByRef',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getServiceRefsByContactRef';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            $output['getServiceRefs'] = hermesCallRemoveCaching('getServiceRefs',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getServiceRefsAsHtml';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            // $output['getServiceRefs'] = hermesCallRemoveCaching('getServiceRefs',array('contact_ref' => $contactRef), 'all'); // Not Needed, used above
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getPremiumAccountDetailsByContactRef';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $contactRef));
            $output['getPremiumAccountDetailsByContactRef'] = hermesCallRemoveCaching('getPremiumAccountDetailsByContactRef',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getAccountContactsByContactRef';
            $output[$method] = PWN_Cacher::clearContent($method, array($contactRef));
            $output['getAccountPinsByContactRef'] = hermesCallRemoveCaching('getAccountPinsByContactRef',array('contact_ref' => $contactRef), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        if (!empty($exceptions)) {
            throw new Exception('Not all cache was successfully cleared:' . serialize($exceptions) . serialize($output));
        }

        return $output;
    }

    /**
     * Clears cache for modififying recordings
     *
     * @param array | int $args
     * @todo The MYPWN_App::_getRecordingsByContactRef Method has upto 3 Parameters. Include all 3 Parameters into this
     * @return array Method names and bool of state of status clear
     *
     */
    public static function modifiedRecording($args) {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $output     = array();
        $exceptions = array();

        try {
            $method = 'MYPWN_App::_getRecordingsByContactRef';

            if (is_array($args)) {
                $output[$method] = PWN_Cacher::clearContent($method,$args);
                $output['getRecordingsByContactRef'] = hermesCallRemoveCaching('getRecordingsByContactRef',$args,'all');
            } else {
                $output[$method] = PWN_Cacher::clearContent($method, array($args));
                $output['getRecordingsByContactRef'] = hermesCallRemoveCaching('getRecordingsByContactRef',array('contact_ref' => $args, 'show_all_users' => false), 'all');
            }
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        if (!empty($exceptions)) {
            throw new Exception('Not all cache was successfully cleared:' . serialize($exceptions) . serialize($output));
        }

        return $output;
    }

    /**
     * Cache clearing for after someone as requested a wallet card
     * @param $args array ('contact_ref' => int, 'pin_ref' => int)
     */
    public static function walletCardRequest(array $args) {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $output     = array();
        $exceptions = array();

        try {
            $method = 'MYPWN_App::_getWalletCardAddress';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $args['contact_ref'], 'pin_ref' => $args['pin_ref']));
            $output['getWalletCardAddress1'] = hermesCallRemoveCaching('getWalletCardAddress',array('contact_ref' => $args['contact_ref'], 'pin_ref' => $args['pin_ref']), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        try {
            $method = 'MYPWN_App::_getWalletCardAddress';
            $output[$method] = PWN_Cacher::clearContent($method, array('contact_ref' => $args['contact_ref'], 'pin_ref' => false));
            $output['getWalletCardAddress2'] = hermesCallRemoveCaching('getWalletCardAddress',array('contact_ref' => $args['contact_ref'], 'pin_ref' => false), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        if (!empty($exceptions)) {
            throw new Exception('Not all cache was successfully cleared:' . serialize($exceptions) . serialize($output));
        }

        return $output;
    }

    /**
     * Cache clearing after someone has updated an account
     * @param $args array ('account_id' => int)
     */
    public static function modifiedAccount(array $args) {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $output     = array();
        $exceptions = array();

        try {
            $method = 'MYPWN_App::_getPlusAccount';
            $output[$method] = PWN_Cacher::clearContent($method, array('account_id' => $args['account_id']));
            $output['getPlusAccount'] = hermesCallRemoveCaching('getPlusAccount',array('account_id' => $args['account_id']), 'all');
        } catch (Exception $e) {
            $exceptions[] = $e;
        }

        if (!empty($exceptions)) {
            throw new Exception('Not all cache was successfully cleared:' . serialize($exceptions) . serialize($output));
        }

        return $output;
    }

    /**
     * Cache clearing after someone has updated their Dial IN Numbers. Also Updates their Users Contacts Dial In Numbers
     * @param $args array ('accountInfo' => arr)
     * @param $args array ('language' => string)
     */
    public static function modifiedDialInNumbers(array $args) {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $output     = array();
        $exceptions = array();

        foreach($args['accountInfo'] as $pinPair) {
            try {
                $output['getContactDialInNumbers'][] = hermesCallRemoveCaching('getContactDialInNumbers',array('contact_ref' => $pinPair['chair_contact_ref'],'language' => $args['language']),'all');
            } catch (Exception $e) {
                $exceptions[] = $e;
            }
        }

        if (!empty($exceptions)) {
            throw new Exception('Not all cache was successfully cleared:' . serialize($exceptions) . serialize($output));
        }

        return $output;
    }    

}
