<?php

/**
 * Class for common plus methods [Indentation = 3 Spaces]
 *
 * @author Vitaly + Asfer
 *
 */
class plusCommon
{

    /**
     * Cached result for the getPlusAccount method.
     *
     * @var array
     */
    protected static $accounts;

    /**
     * Cached result for the getPlusAccountAdmin method.
     *
     * @var array
     */
    protected static $accountAdmins = array();

    /**
     * Cached result from getAllPlusProductGroups.
     *
     * @var array
     */
    protected static $productGroups;

    /**
     * Cached result from calculateMinimumCreditRequired.
     *
     * @var float
     */
    protected static $minimumCost;

    /**
     * Cached result from retrieveBundleProducts.
     *
     * @var array
     */
    protected static $bundleProducts = array();

    /**
     * Cached result from retrievePostPayStatus.
     *
     * @var boolean
     */
    protected static $postPayStatus = array();

    /**
     * Cached result from getRemainingMinuteAllowanceForMonthAndYear.
     *
     * @var array
     */
    protected static $remainingMinuteAllowance = array();

    /**
     * Cached result from retrievePinProductGroups.
     *
     * @var array
     */
    protected static $pinProductGroups = array();

    /**
     * Cached result from retrievePinsFromContact.
     *
     * @var array
     */
    protected static $contactPins = array();

    /**
     * Cached result from retrieveIndividualBundleAssignedPinPair.
     *
     * @var array
     */
    protected static $individualBundlePinPairs = array();

    /**
     * Cached result from retrieveAccountBundle.
     *
     * @var array
     */
    protected static $accountBundles = array();

    /**
     * Cached result from doesPlusAdminHaveValidRecurringPaymentAgreementSetUp.
     *
     * @var array
     */
    protected static $validAgreements = array();

    /**
     * Tell if user is switchable to plus
     *
     * @param $email string
     *
     * @return bool True if user is switchable, otherwise false
     *
     * @author Vitaly
     *
     */
    public static function isSwitchableToPlus($email)
    {
        $pins = array();

        // Get Initial Contact Information
        $contactData = Common::getContact($email, null, true);
        if (isset($contactData['error'])) {
            $contactData = array();
        }
        if (isset($contactData['contact_ref'])) {
            $pins = Common::getContactPins($contactData['contact_ref'], true);

            if (count($pins) > 1 || $pins[0]['service_ref'] != 801) {
                sfContext::getInstance()->getLogger()->err(
                    __METHOD__ . ", Contact ref " . $contactData['contact_ref'],
                    " cannot be converted to plus."
                );
                return false;
            }
        }

        return true;
    }

    /**
     * Obtain the Account Balance and the Amount of Users an Account Has
     *
     * @param $account_id  int This is the Account ID
     * @param $contact_ref int This is the Contact Ref
     * @param $clearCache bool Do you want to Clear Cache before getting the Plus Account
     *
     * @return array('balance' => 0.00, 'unformatted_balance' => 0.000, 'accountUsers' => S | M) | array()
     *
     * @author Asfer
     *
     * @todo Refactor to return a Balance object containing the formatted and unformatted balance.
     */
    public static function getAccountBalanceAndUsers($account_id, $contact_ref, $clearCache = false)
    {
        // Check to see if the Required Parameters are given
        if (!isset($account_id) || !isset($contact_ref)) {
            return array();
        }

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $plusAccount = array();
        $accountPins = array();

        if ($clearCache) {
            try {
                hermesCallRemoveCaching('getPlusAccount', array('account_id' => $account_id), 'array');
                sfContext::getInstance()->getLogger()->info('Cleared Cache for Account: ' . $account_id);
            } catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'Unable to clear cache for Account: ' . $account_id .
                    ', Error Message: ' . print_r($e->getMessage(), true) .
                    ', Error Code: ' . print_r($e->getCode(), true)
                );
            }

            try {
                hermesCallRemoveCaching(
                    'getAccountPinsByContactRef',
                    array(
                        'contact_ref' => $contact_ref,
                        'active'      => 'Y'
                    ),
                    'array'
                );
                sfContext::getInstance()->getLogger()->info('Cleared Cache for Contact: ' . $contact_ref);
            } catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'Unable to clear cache for Contact: ' . $contact_ref .
                    ', Error Message: ' . print_r($e->getMessage(), true) .
                    ', Error Code: ' . print_r($e->getCode(), true)
                );
            }
        }

        // Obtain the Account Balance
        try {
            $plusAccount = hermesCallWithCaching('getPlusAccount', array('account_id' => $account_id), 'array', 3600);
            $rawBalance     = $plusAccount['account']['credit_balance'];
            $accountBalance = $plusAccount['account']['credit_balance_formatted'];
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getPlusAccount Hermes call has timed out.');
            return $plusAccount;
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getPlusAccount Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            return $plusAccount;
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "getPlusAccount Failed to Return a Result for Account ID :: $account_id, Error Message: " . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            return $plusAccount;
        }

        // Obtain PINs for the Admin
        try {
            $accountPins = hermesCallWithCaching('getAccountPinsByContactRef', array(
                    'contact_ref' => $contact_ref,
                    'active'      => 'Y'
                ), 'array', 3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getAccountPinsByContactRef Hermes call has timed out.');
            return $accountPins;
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getAccountPinsByContactRef Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            return $accountPins;
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "getAccountPinsByContactRef Failed to Return a Result for Account ID :: $account_id, Error Message: " . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            return $accountPins;
        }

        // Check if there are more than 1 User within the Account AND Check if the Admin has more than 1 PIN Pair
        $AdminPinCount = 0;
        foreach ($accountPins as $pin) {
            if ($pin['contact_ref'] == $contact_ref) {
                $AdminPinCount++;
            }
        }
        $hasUsers = ($AdminPinCount !== count($accountPins)) ? 'M' : 'S';

        return array(
            'balance'             => $accountBalance,
            'unformatted_balance' => $rawBalance,
            'accountUsers'        => $hasUsers,
        );
    }

    /**
     * Returns the account balance and users from plusCommon::getAccountBalanceAndUsers, but with a Balance object.
     *
     * @param int $accountId
     * @param int $contactRef
     * @return array
     */
    public static function getAccountBalanceContainerAndUsers($accountId, $contactRef)
    {
        $balanceAndUsers = self::getAccountBalanceAndUsers($accountId, $contactRef);
        return array(
            'balance'      => new Balance($balanceAndUsers['unformatted_balance'], $balanceAndUsers['balance']),
            'accountUsers' => $balanceAndUsers['accountUsers'],
        );
    }

    /**
     * Returns the account balance and users from plusCommon::getAccountBalanceAndUsers, but with a Balance object.
     *
     * @param int $accountId
     * @param int $contactRef
     * @param bool $reset
     * @return array
     */
    public static function getAccountBalanceContainerAndUsersWithReset($accountId, $contactRef, $reset)
    {
        $balanceAndUsers = self::getAccountBalanceAndUsers($accountId, $contactRef, $reset);
        return array(
            'balance'      => new Balance($balanceAndUsers['unformatted_balance'], $balanceAndUsers['balance']),
            'accountUsers' => $balanceAndUsers['accountUsers'],
        );
    }

    /**
     * Updates the assigned products to an account.
     *
     * @param int $accountId
     * @param array $assignedProducts
     * @param array $unassignedProducts
     */
    public static function updatePlusAccountProductGroups(
        $accountId,
        array $assignedProducts,
        array $unassignedProducts
    ) {
        Hermes_Client_Rest::call(
            'doUpdatePlusAccountProductGroups',
            array(
                'account_id'                 => $accountId,
                'assign_product_group_ids'   => $assignedProducts,
                'unassign_product_group_ids' => $unassignedProducts
            )
        );
    }

    /**
     * Retrieves the pin_ref that references the master_pin.
     *
     * @param array $pins
     * @param int $pinRef
     * @return int
     */
    public static function getPinPair(array $pins, $pinRef)
    {
        // Find the master pin related to the pin reference.
        foreach ($pins as $pin) {
            if ($pin['pin_ref'] == $pinRef) {
                $master_pin = $pin['master_pin'];
                break;
            }
        }

        // In case a master pin was found, return its pin reference.
        if (isset($master_pin)) {
            foreach ($pins as $pin) {
                if ($pin['pin'] == $master_pin) {
                    return $pin['pin_ref'];
                }
            }
        }

        return 0;
    }

    /**
     * Retrieves all usage rates related to the product group.
     *
     * @param int $productGroupID
     * @return array
     */
    public static function retrieveUsageRatesForProductGroup($productGroupID)
    {
        return Hermes_Client_Rest::call(
            'getPlusUsageRatesPerProductGroup',
            array(
                'group_id'               => $productGroupID,
                'aggregate_numbers'      => 1,
                'dnis_type_translations' => array(
                    array(
                        'find'    => 'Freefone',
                        'replace' => 'Freephone'
                    ),
                    array(
                        'find'    => 'Geographic',
                        'replace' => 'Landline'
                    ),
                )
            )
        );
    }

    /**
     * Retrieves the product groups assigned to the account.
     *
     * Note: uses cached result.
     *
     * @param int $accountId
     * @param string $locale
     * @param bool $reset
     * @return array
     */
    protected static function retrieveProductGroups($accountId, $locale, $reset = false)
    {
        if (!isset(plusCommon::$productGroups[$accountId][$locale]) || $reset) {
            if (!isset(plusCommon::$productGroups[$accountId])) {
                plusCommon::$productGroups[$accountId] = array();
            }

            plusCommon::$productGroups[$accountId][$locale] = Hermes_Client_Rest::call(
                'getAllPlusProductGroups',
                array(
                    'locale'     => $locale,
                    'account_id' => $accountId
                )
            );
        }
        return plusCommon::$productGroups[$accountId][$locale];
    }

    /**
     * Retrieves the products assigned to the account, mapped by their product group id.
     *
     * @param int $accountId
     * @param string $locale
     * @param bool $reset
     * @return array
     */
    public static function retrieveAssignedProducts($accountId, $locale, $reset = false)
    {
        $productGroups       = self::retrieveProductGroups($accountId, $locale, $reset);
        $assignedProducts    = $productGroups['assigned_products'];
        $assignedProductsMap = array();
        foreach ($assignedProducts as $product) {
            $assignedProductsMap[$product['product_group_id']] = $product;
        }
        return $assignedProductsMap;
    }

    /**
     * Retrieves all predefined bundle products.
     *
     * @param string $locale
     * @param bool $reset
     * @return array
     */
    public static function retrieveBundleProducts($locale, $reset = false)
    {
        if (!isset(plusCommon::$bundleProducts[$locale]) || $reset) {
            plusCommon::$bundleProducts[$locale] = plusCommon::retrieveMappedBundleProducts($locale);
        }
        return plusCommon::$bundleProducts[$locale];
    }

    private static function retrieveMappedBundleProducts($locale)
    {
        $response          = Hermes_Client_Rest::call(
            'getAllPlusProductGroups',
            array(
                'locale' => $locale,
                'bundle' => true,
            )
        );
        $productList       = $response['product_list'];
        $mappedProductList = array();
        foreach ($productList as $product) {
            $mappedProductList[$product['product_group_id']] = $product;
        }
        return $mappedProductList;
    }

    /**
     * Retrieves all data related to the product group ids and returns the map of product group id to data.
     *
     * @param int $accountId
     * @param string $locale
     * @param array $productGroupIds
     * @param bool $reset
     * @return array
     */
    public static function fillOutProductGroups($accountId, $locale, array $productGroupIds, $reset = false)
    {
        $allProductGroups = plusCommon::retrieveProductGroups($accountId, $locale, $reset);
        $productList      = $allProductGroups['product_list'];
        $productMap       = array_fill_keys($productGroupIds, 0);
        $productCount     = count($productMap);

        foreach ($productList as $product) {
            $productId = (int)$product['product_group_id'];
            if (isset($productMap[$productId]) && !is_array($productMap[$productId])) {
                $productMap[$productId] = $product;
                $productCount--;

                if (!$productCount) {
                    break;
                }
            }
        }

        return $productMap;
    }

    /**
     * Builds the contact ref to contact data map for myPinProducts and similar pages.
     *
     * @todo refactor code.
     *
     * @param array $contactPinInfo
     *   Result from Default.getAccountPinsByContactRef
     * @return array
     */
    public static function buildContactMap($contactPinInfo)
    {
        // Create Users Array and Product Status for the User
        $users = array();
        foreach ($contactPinInfo as $contactInfo) {
            $contactRef = $contactInfo['contact_ref'];

            if (!isset($users[$contactRef]) && !empty($contactInfo['master_pin'])) {
                $users[$contactRef] = array(
                    'contact_ref' => $contactRef,
                    'first_name'  => $contactInfo['first_name'],
                    'last_name'   => $contactInfo['last_name'],
                    'email'       => $contactInfo['email'],
                );
            }
        }
        return $users;
    }

    /**
     * Calculate the minimum credit required to pass worldpay phase.
     *
     * @return int
     */
    public static function calculateMinimumCreditRequired()
    {
        if (!isset(self::$minimumCost)) {
            /** @var myUser $user */
            $user = sfContext::getInstance()->getUser();

            // Retrieve the setup cost of the basket and add it to the minimum cost.
            $basket            = new Basket($user);
            $setupCost         = abs($basket->calculateTotalCost());
            self::$minimumCost = $setupCost;

            $creditProducts = $basket->retrieveCallCreditProducts();
            $bundleAdded    = $basket->hasBundleBeenAdded();
            if (!$creditProducts && !$bundleAdded && !$user->isPostPayCustomer()) {
                self::$minimumCost += 5;
            }
        }

        return self::$minimumCost;
    }

    /**
     * Assert that the product is a BWM.
     *
     * @param array $product
     * @return bool
     */
    public static function isBWMProduct(array $product)
    {
        $allocated  = (int)$product['allocated'];
        $dedicated  = (int)$product['dedicated'];
        $serviceRef = (int)$product['service_ref'];
        // $isVoiceProduct = $product['product_type'] === 'VOICE';
        return $allocated === 1 && $dedicated === 1 && $serviceRef === 850; // && $isVoiceProduct;
    }

    /**
     * Add a flag to the product map indicating to prevent removal.
     *
     * @param array $product
     * @return array
     */
    protected static function markNonRemovableProduct(array $product)
    {
        if ($product['is_BWM']) {
            $product['prevent_removal'] = true;
        }
        return $product;
    }

    public static function storePaymentSession(PaymentSession $session)
    {
        return Hermes_Client_Rest::callPOST('Plus.startPaymentSession', $session->asArray());
    }

    /**
     * Retrieves the Payment Session by the cart ID, which is the primary key of the payment_session table.
     *
     * @param string $cartID
     * @return bool|PaymentSession
     * @author Maarten Jacobs
     */
    public static function retrievePaymentSession($cartID)
    {
        if (!$cartID) {
            return false;
        }
        $paymentSession = Hermes_Client_Rest::call('Plus.retrievePaymentSession', array('cart_id' => $cartID));
        if (!$paymentSession) {
            return false;
        }
        return new PaymentSession($paymentSession['cart_id'], $paymentSession['account_id'], $paymentSession);
    }

    /**
     * Retrieves a Payment Session by the linked payment_id, which is the primary key of the payment table.
     *
     * @param int $paymentId
     * @param sfLogger $logger
     * @return bool|PaymentSession
     * @author Maarten Jacobs
     */
    public static function retrievePaymentSessionByPaymentId($paymentId, sfLogger $logger)
    {
        if (!$paymentId) {
            return false;
        }
        try {
            $paymentSession = Hermes_Client_Rest::call(
                'Plus.retrievePaymentSessionByPaymentId',
                array('payment_id' => $paymentId)
            );
        } catch (Exception $e) {
            $paymentSession = false;
            $logger->err(
                'An exception was thrown whilst trying to retrieve the payment session by payment id. The exception message was "'
                . $e->getMessage() . '", and the exception code was ' . $e->getCode() . '.'
            );
        }
        if (!$paymentSession) {
            return false;
        }
        return new PaymentSession($paymentSession['cart_id'], $paymentSession['account_id'], $paymentSession);
    }

    /**
     * Process a single potential BWM.
     *
     * @param array $product
     * @param array $usageRates
     * @return array
     */
    public static function processPotentialBWM($product, array $usageRates)
    {
        $productId = (int)$product['product_group_id'];

        $product['is_BWM'] = plusCommon::isBWMProduct($product);
        if ($product['is_BWM']) {
            $product = plusCommon::markNonRemovableProduct($product);

            // Add the DNISes to the BWM, for displaying a generic description.
            $product['dnises'] = array();
            if (isset($usageRates[$productId])) {
                $product['dnises'] = $usageRates[$productId];
            }

            // If we have the necessary data: add the next cyclic charge date, for the DNISes.
            if (array_key_exists('cyclic_start_date', $product) &&
                !empty($product['account_cyclic_payment_interval_value']) &&
                !empty($product['account_cyclic_payment_interval_type'])
            ) {
                try {
                    $startDate = date('d-m-Y');
                    if (!empty($product['cyclic_start_date'])) {
                        $date = new DateTime($product['cyclic_start_date']);
                        if ($date) {
                            // Valid start date, so use it instead.
                            $startDate = $date->format('d-m-Y');

                            // Let the tooltip handler know there is a definite start date.
                            $product['is_definite_start_date'] = true;
                        }
                    }

                    $nextChargeDateResult               = Hermes_Client_Rest::call(
                        'BWM.nextCyclicChargeDate',
                        array(
                            'start_date'     => $startDate,
                            'interval_type'  => $product['account_cyclic_payment_interval_type'],
                            'interval_value' => $product['account_cyclic_payment_interval_value'],
                        )
                    );
                    $product['next_cyclic_charge_date'] = $nextChargeDateResult['next_cyclic_charge_date'];
                } catch (Exception $e) {
                    // For now, do nothing as the possibility is very small.
                }
            }

            $product['group_thumbnail'] = $product['group_thumbnail_en'] = '/sfimages/products/cowgirl-bwm.png';
        }

        return $product;
    }

    /**
     * Adds the necessary processing to BWMs in the product lists.
     *
     * @param array $products
     * @param array $usageRates
     * @return array
     */
    public static function processPotentialBWMs(array $products, array $usageRates)
    {
        foreach ($products as &$product) {
            $product = plusCommon::processPotentialBWM($product, $usageRates);
        }
        return $products;
    }

    /**
     * Checks if the user has to purchase credit
     *
     * @param $myUser user
     *
     * @return Bool
     */
    public static function hasToPurchaseCredit($balance)
    {
        $basket = new Basket(sfContext::getInstance()->getUser());
        if ($basket->hasBWMBeenAdded()) {
            return true;
        } elseif ($balance < 5) {
            return true;
        }
        return false;
    }

    /**
     * Retrieves and caches the Plus account.
     *
     * @param int $accountId
     * @param bool $reset
     * @return array
     */
    public static function getPlusAccount($accountId, $reset = false)
    {
        if (!isset(self::$accounts[$accountId]) || $reset) {
            self::$accounts[$accountId] = array();
            try {
                $accountResponse            = Hermes_Client_Rest::call(
                    'getPlusAccount',
                    array('account_id' => $accountId)
                );
                self::$accounts[$accountId] = $accountResponse['account'];
            } catch (Exception $e) {
                // @todo: log errors from common libraries.
            }
        }
        return self::$accounts[$accountId];
    }

    /**
     * Retrieves and caches the Plus admin account.
     *
     * @param int $accountId
     * @param bool $reset
     * @return array
     */
    public static function getPlusAccountAdmin($accountId, $reset = false)
    {
        if (!isset(self::$accountAdmins[$accountId]) || $reset) {
            self::$accountAdmins[$accountId] = array();
            try {
                self::$accountAdmins[$accountId] = Hermes_Client_Rest::call(
                    'getPlusAccountAdmin',
                    array('account_id' => $accountId)
                );
            } catch (Exception $e) {
                // @todo: log errors from common libraries.
            }
        }
        return self::$accountAdmins[$accountId];
    }

    /**
     * Checks if the account is a PostPay customer.
     *
     * @param int $accountId
     * @param bool $reset
     * @return bool
     */
    public static function retrievePostPayStatus($accountId, $reset = false)
    {
        if (!isset(self::$postPayStatus[$accountId]) || $reset) {
            $response                        = Hermes_Client_Rest::call(
                'Bundle.postPayStatus',
                array('account_id' => $accountId)
            );
            self::$postPayStatus[$accountId] = $response['status'];
        }
        return self::$postPayStatus[$accountId];
    }

    /**
     * Retrieves the remaining minute allowance for a given month and year.
     *
     * @param int $accountId
     * @param int $month
     * @param int $year
     * @param bool $reset
     * @return int
     */
    public static function getRemainingMinutesAndSecondsAllowanceForMonthAndYear(
        $accountId,
        $month,
        $year,
        $reset = false
    ) {
        if (!isset(self::$remainingMinuteAllowance[$accountId])) {
            self::$remainingMinuteAllowance[$accountId] = array();
        }
        if (!isset(self::$remainingMinuteAllowance[$accountId][$year])) {
            self::$remainingMinuteAllowance[$accountId][$year] = array();
        }

        $bundleAllowanceResponse = array();
        $args = array('month' => $month, 'year' => $year, 'account_id' => $accountId);

        try {
            $bundleAllowanceResponse = Hermes_Client_Rest::call('Bundle.getRemainingBundleAllowance', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Bundle.getRemainingBundleAllowance Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Bundle.getRemainingBundleAllowance Hermes call is faulty, Error message: ' . $e->getMessage() .
                ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Bundle.getRemainingBundleAllowance Exception Called. Error Message: ' . $e->getMessage() .
                ', Error Code: ' . $e->getCode()
            );

            // Hardcoded - this situation shouldn't appear in normal
            // circumstances - email will be send to development group and
            // user should have 0 assigned as a bundle allowance

            mail(
                'webteam@powwownow.com',
                '[' . $_SERVER['SERVER_NAME'] . ']'
                . 'Bundle::getAccountBundle could not find an allowance.',
                sprintf(
                    'Could not find account bundle allowance for passed values'
                    . 'account id: %s; year: %s; month: %s, session: %s',
                    $accountId,
                    $year,
                    $month,
                    print_r(sfContext::getInstance()->getUser()->getAttributeHolder()->getAll(), true)
                )
            );

            $bundleAllowanceResponse = array(
                'code'      => 204,
                'message'   => 'Action successfully completed.',
                'allowance' => '0',
            );

        }

        $minutesAllowance = (int)floor($bundleAllowanceResponse['allowance'] / 60);
        $secondsRemainder = (int)$bundleAllowanceResponse['allowance'] % 60.0;

        self::$remainingMinuteAllowance[$accountId][$year][$month] = array(
            'minutes' => $minutesAllowance,
            'seconds' => $secondsRemainder,
        );

        return self::$remainingMinuteAllowance[$accountId][$year][$month];
    }

    /**
     * Retrieves the remaining minute allowance for the current month and year.
     *
     * @param int $accountId
     * @param bool $reset
     * @return int
     */
    public static function getRemainingMinuteAllowance($accountId, $reset = false)
    {
        $month     = date('m');
        $year      = date('Y');
        $allowance = self::getRemainingMinutesAndSecondsAllowanceForMonthAndYear($accountId, $month, $year, $reset);
        return $allowance['minutes'];
    }

    /**
     * Retrieves the remaining bundle allowance, in digital clock format.
     *
     * @param int $accountId
     * @param bool $reset
     * @return string
     */
    public static function getRemainingAllowanceFormatted($accountId, $reset = false)
    {
        $month     = date('m');
        $year      = date('Y');
        $allowance = self::getRemainingMinutesAndSecondsAllowanceForMonthAndYear($accountId, $month, $year, $reset);
        return sprintf("%d:%d", $allowance['minutes'], $allowance['seconds']);
    }

    /**
     * Assigns a list of products to all pins linked to the contact of an account.
     *
     * @param int $contactRef
     * @param array $productIds
     * @return array
     *   A list of all pin references that were used in the process.
     */
    public static function assignAllProductsToAllPinsOfContact($contactRef, array $productIds)
    {
        $pinRefs = array_keys(self::retrievePinsFromContact($contactRef));
        foreach ($productIds as $productId) {
            self::assignProductToPins($pinRefs, $productId);
        }
        return $pinRefs;
    }

    /**
     * Assigns a product to every pin in a given list of pins.
     *
     * @param array $pinRefs
     * @param int $productId
     */
    public static function assignProductToPins(array $pinRefs, $productId)
    {
        foreach ($pinRefs as $pinRef) {
            if (!self::isProductAssignedToPin($pinRef, $productId)) {
                Hermes_Client_Rest::call(
                    'Default.updatePlusPinProductGroups',
                    array(
                        'pin_ref'                  => $pinRef,
                        'assign_product_group_ids' => array($productId),
                    )
                );
            }
        }
    }

    /**
     * Retrieves a map of product groups assigned to the pin, mapped by product group id.
     *
     * @param int $pinRef
     * @param bool $reset
     * @return array
     */
    public static function retrievePinProductGroups($pinRef, $reset = false)
    {
        if (!isset(self::$pinProductGroups[$pinRef]) || $reset) {
            self::$pinProductGroups[$pinRef] = array();
            $productGroups                   = Hermes_Client_Rest::call(
                'getPlusPinProductGroups',
                array(
                    'pin_ref' => $pinRef
                )
            );
            foreach ($productGroups as $productGroup) {
                self::$pinProductGroups[$pinRef][$productGroup['product_group_id']] = $productGroup;
            }
        }
        return self::$pinProductGroups[$pinRef];
    }

    /**
     * Checks if a product is assigned to all products assigned to a set of pins.
     *
     * @param array $pinRefs
     * @param int $productId
     * @return bool
     */
    public static function isProductAssignedToAllPins(array $pinRefs, $productId)
    {
        foreach ($pinRefs as $pinRef) {
            if (!self::isProductAssignedToPin($pinRef, $productId)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if a product is assigned to any of the given pins.
     *
     * @param array $pinRefs
     * @param int $productId
     * @return bool
     */
    public static function isProductAssignedToAnyPin(array $pinRefs, $productId)
    {
        foreach ($pinRefs as $pinRef) {
            if (self::isProductAssignedToPin($pinRef, $productId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a product is assigned to a pin.
     *
     * @param int $pinRef
     * @param int $productId
     * @return bool
     */
    public static function isProductAssignedToPin($pinRef, $productId)
    {
        $pinProductGroups = self::retrievePinProductGroups($pinRef);
        return isset($pinProductGroups[$productId]);
    }

    /**
     * Retrieves all pins assigned to the contact.
     *
     * @param int $contactRef
     * @param bool $reset
     * @return array
     */
    public static function retrievePinsFromContact($contactRef, $reset = false)
    {
        if (!isset(self::$contactPins[$contactRef]) || $reset) {
            $pinRetriever                   = new PinRetriever();
            self::$contactPins[$contactRef] = $pinRetriever->getAccountPinsByContactRef($contactRef);
        }
        return self::$contactPins[$contactRef];
    }

    /**
     * Retrieves a contact pin map with chairman pins as keys.
     *
     * @param int $contactRef
     * @return array
     */
    public static function retrieveContactPinMap($contactRef)
    {
        $contactPinInfo = Hermes_Client_Rest::call(
            'getAccountPinsByContactRef',
            array('contact_ref' => $contactRef, 'active' => 'Y')
        );
        $userMap        = array();
        foreach ($contactPinInfo as $contactPin) {
            $isMasterPin = empty($contactPin['master_pin']);
            $pin         = $isMasterPin ? $contactPin['pin'] : $contactPin['master_pin'];
            if (!isset($userMap[$pin])) {
                $userMap[$pin] = array(
                    'first_name' => $contactPin['first_name'],
                    'last_name'  => $contactPin['last_name'],
                    'email'      => $contactPin['email'],
                );
            }
            if ($isMasterPin) {
                $userMap[$pin]['chairman_pin']     = $contactPin['pin'];
                $userMap[$pin]['chairman_pin_ref'] = $contactPin['pin_ref'];
            } else {
                $userMap[$pin]['participant_pin']     = $contactPin['pin'];
                $userMap[$pin]['participant_pin_ref'] = $contactPin['pin_ref'];
            }
        }
        return $userMap;
    }

    /**
     * Retrieves the PIN pair assigned to the individual bundle.
     *
     * @param int $productGroupId
     * @param int $contactRef
     * @param bool $reset
     * @return bool|array
     */
    public static function retrieveIndividualBundleAssignedPinPair($productGroupId, $contactRef, $reset = false)
    {
        if ($reset || !isset(self::$individualBundlePinPairs[$productGroupId][$contactRef])) {
            if (!isset(self::$individualBundlePinPairs[$productGroupId])) {
                self::$individualBundlePinPairs[$productGroupId] = array();
            }
            try {
                self::$individualBundlePinPairs[$productGroupId][$contactRef] = Hermes_Client_Rest::call(
                    'Bundle.getIndividualBundleAssignedPinPair',
                    array(
                        'product_group_id' => $productGroupId,
                        'contact_ref'      => $contactRef,
                    )
                );
            } catch (Exception $e) {
                return false;
            }
        }
        return self::$individualBundlePinPairs[$productGroupId][$contactRef];
    }

    public static function retrieveAccountBundle($accountId, $month = null, $year = null, $reset = false)
    {
        if (!$month) {
            $month = date('m');
        }
        if (!$year) {
            $year = date('Y');
        }

        if ($reset || !isset(self::$accountBundles[$accountId][$year][$month])) {
            if (!isset(self::$accountBundles[$accountId])) {
                self::$accountBundles[$accountId] = array();
            }
            if (!isset(self::$accountBundles[$accountId][$year])) {
                self::$accountBundles[$accountId][$year] = array();
            }
            $accountBundle                                   = Hermes_Client_Rest::call(
                'Bundle.getAccountBundle',
                array(
                    'account_id' => $accountId,
                    'month'      => $month,
                    'year'       => $year,
                )
            );
            self::$accountBundles[$accountId][$year][$month] = $accountBundle['bundle'];
        }

        return self::$accountBundles[$accountId][$year][$month];
    }

    /**
     * Checks if the customer has a valid Recurring Payment Agreement set up.
     *
     * Note that for a Recurring Payment Agreement to be valid, it MUST be created after (or in) June 2013 AND be active.
     *
     * @param int $accountId
     * @param bool $reset
     * @return bool
     * @author Maarten Jacobs
     */
    public static function doesPlusAdminHaveValidRecurringPaymentAgreementSetUp($accountId, $reset = false)
    {
        if (!isset(self::$validAgreements[$accountId]) || $reset) {
            $checkResponse                     = Hermes_Client_Rest::call(
                'Bundle.doesPlusAdminHaveValidRecurringPaymentAgreementSetUp',
                array(
                    'account_id' => $accountId,
                )
            );
            self::$validAgreements[$accountId] = $checkResponse['has_valid_agreement'];
        }
        return self::$validAgreements[$accountId];
    }

    /**
     * Select Which Plus Form Template to be used in selecting for the Plus Popup Registration.
     * Currently used on the /Conference-Call/Call-Minute-Bundles page
     *
     * @param $arguments
     * @param $user
     * @param $auth
     * @return array
     */
    public static function doPlusRegistrationTemplateSelection($arguments, $user, $auth)
    {
        $output = '';
        $error  = array();
        $logger = sfContext::getInstance()->getLogger();

        if ($auth) {
            // User is Authenticated
            $logger->err(__METHOD__ . ', PlusLogic :: User Is Authenticated');

            // Check if Session Email is switchable
            $isSwitchable = self::isSwitchableToPlus($user->getAttribute('email'));
            if ($isSwitchable) {
                $logger->err(__METHOD__ . ', PlusLogic :: User Is Switchable');
                $output = 'plus_form_without_password';
            } else {
                $output = 'plus_no_switch';
            }
        } else {
            // User is Not Authenticated
            $logger->err(__METHOD__ . ', PlusLogic :: User Is Not Authenticated');

            // First Time coming onto Form
            if (!isset($arguments['email']) && !isset($arguments['password'])) {
                $logger->err(__METHOD__ . ', PlusLogic :: First Time');
                $output = 'plus_email_form';
            }

            // Email is set but there is no Password
            if (isset($arguments['email']) && !isset($arguments['password'])) {
                // Check if Posted Email is switchable
                $isSwitchable = plusCommon::isSwitchableToPlus($arguments['email']);

                // Contact Data
                $contactData = Common::getContact($arguments['email']);
                if (isset($contactData['error'])) {
                    $error = array(
                        'errors' => array(
                            'error_messages' => array(
                                'message'    => 'FORM_COMMUNICATION_ERROR',
                                'field_name' => 'alert'
                            )
                        )
                    );
                } else {
                    // Set the Password
                    if (!isset($contactData['password'])) {
                        $contactData['password'] = null;
                    }

                    // Decide What Form to show
                    switch ($contactData['password']) {
                        // Powwownow User Without a Password - Switching
                        case '1e895a7034cb8f2b1b7da1a6220c1737':
                            if ($isSwitchable) {
                                $logger->err(__METHOD__ . ', PlusLogic :: User Is Switchable');
                                $output = 'plus_form_with_password';
                            } else {
                                $logger->err(__METHOD__ . ', PlusLogic :: User Is Not Switchable');
                                $output = 'plus_no_switch';
                            }
                            break;

                        // Completely New Plus Registration - SignUp
                        case null:
                            $logger->err(__METHOD__ . ', PlusLogic :: User Is New Plus Registration');
                            $output = 'plus_form_with_password';
                            break;

                        // Powwwonow User With a Password
                        default:
                            $logger->err(__METHOD__ . ', PlusLogic :: User Is PWN User with Password');
                            if ($isSwitchable) {
                                $output = 'plus_password_form';
                            } else {
                                $output = 'plus_no_switch';
                            }
                    }
                }
            }

            // Email and Password have both been Posted
            if (isset($arguments['email']) && isset($arguments['password'])) {
                // Check if Posted Email is switchable
                $isSwitchable = plusCommon::isSwitchableToPlus($arguments['email']);

                // Authenticate the User
                try {
                    Plus_Authenticate::logIn($arguments['email'], $arguments['password']);

                    if ($isSwitchable) {
                        $output = 'plus_form_without_password';
                    } else {
                        $output = 'plus_no_switch';
                    }

                } catch (Exception $e) {
                    $error = array(
                        'errors' => array(
                            'error_messages' => array(
                                'message'    => 'FORM_VALIDATION_WRONG_PASSWORD',
                                'field_name' => 'password'
                            )
                        )
                    );
                }
            }
        }

        if ($output == 'plus_no_switch' && isset($contactData)) {
            $output = self::getPlusNoSwitchTemplate(
                isset($contactData['service_user']) ? $contactData['service_user'] : array()
            );
        }

        return array(
            'output'      => $output,
            'error'       => $error,
            'contactData' => isset($contactData) ? $contactData : array()
        );
    }

    /**
     * Get the Plus No Switch Template to be used
     * @param $service_user
     * @return string
     */
    public static function getPlusNoSwitchTemplate($service_user)
    {
        switch ($service_user) {
            case 'POWWOWNOW':
                return 'plusNoSwitchPowwownow';
            case 'PLUS_USER':
                return 'plusNoSwitchPlusUser';
            case 'PLUS_ADMIN':
                return 'plusNoSwitchPlusAdmin';
            case 'PREMIUM_USER':
                return 'plusNoSwitchPremiumUser';
            case 'PREMIUM_ADMIN':
                return 'plusNoSwitchPremiumAdmin';
            default:
                return 'plus_no_switch';
        }
    }


    /**
     * Upgrade Enhanced User To Plus
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function upgradeEnhancedToPlus($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('upgradeEnhancedToPlus', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('upgradeEnhancedToPlus Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'upgradeEnhancedToPlus Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'upgradeEnhancedToPlus Failed. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
        }

        return $result;
    }

    /**
     * Do Plus Registration
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function doPlusRegistration($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('doPlusRegistration', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('doPlusRegistration Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doPlusRegistration Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doPlusRegistration Failed. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
        }

        return $result;
    }


}
