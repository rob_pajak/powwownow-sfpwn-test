<?php

interface BwmRenewalInvoiceHandlerInterface
{
    /**
     * Creates a pending invoice for the Plus account
     *
     * @param int $accountId
     * @param DateTime $dueDate
     * @param float $totalCost
     * @param int $numberOfDedicated
     * @return bool
     */
    public function createPendingInvoice($accountId, DateTime $dueDate, $totalCost, $numberOfDedicated);

    /**
     * Finds the BWM renewal invoice by account id, and marks it as paid.
     *
     * @param int $accountId
     * @return bool
     */
    public function markInvoiceAsPaid($accountId);

    /**
     * Finds the BWM renewal invoice by account id, and marks it as having a failed payment.
     *
     * @param int $accountId
     * @return bool
     */
    public function markBwmRenewalInvoiceAsHavingFailedPayment($accountId);
}
