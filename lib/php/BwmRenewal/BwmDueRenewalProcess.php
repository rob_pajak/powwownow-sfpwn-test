<?php

require_once dirname(__FILE__) . '/CommandOutputInterface.php';

/**
 * Charges customers with a BWM due to be renewed today, and already have auto topup enabled.
 */
class BwmDueRenewalProcess
{
    const TASK_NAME = 'Due renewals:';
    const EMAIL_GROUP = 'failedbwmrenewal@powwownow.co.uk';

    /**
     * @var BwmRenewalEmailSenderInterface
     */
    private $emailSender;

    /**
     * @var BwmRenewalInvoiceHandlerInterface
     */
    private $invoiceHandler;

    /**
     * @var BwmRenewalStoreInterface
     */
    private $renewalStore;

    /**
     * @var BwmRenewalPaymentInterface
     */
    private $paymentHandler;

    /**
     * @var bool
     */
    private $inProduction;

    /**
     * @param BwmRenewalEmailSenderInterface $emailSender
     * @param BwmRenewalInvoiceHandlerInterface $invoiceHandler
     * @param BwmRenewalStoreInterface $renewalStore
     * @param BwmRenewalPaymentInterface $paymentHandler
     * @param bool $inProduction
     */
    public function __construct(
        BwmRenewalEmailSenderInterface $emailSender,
        BwmRenewalInvoiceHandlerInterface $invoiceHandler,
        BwmRenewalStoreInterface $renewalStore,
        BwmRenewalPaymentInterface $paymentHandler,
        $inProduction
    )
    {
        $this->emailSender = $emailSender;
        $this->invoiceHandler = $invoiceHandler;
        $this->renewalStore = $renewalStore;
        $this->paymentHandler = $paymentHandler;
        $this->inProduction = $inProduction;
    }

    /**
     * Runs the due-renewal process, which takes the following steps:
     *  1. Retrieve due BWM renewals for today, and iterate over each of these renewals.
     *  2. Try to take payment through WorldPay if, and only if, they have auto-topup enabled (i.e. a recurring payment
     *     agreement).
     *    2.a. If the payment failed: notify the customer and notify the email group which will take payment manually.
     *    2.b. If the payment was successful: continue.
     *  3. Mark the invoice as paid.
     *  4. Notify the customer of his available invoice.
     *
     * @param CommandOutputInterface $output
     * @return bool
     */
    public function handleDuePayments(CommandOutputInterface $output)
    {
        // Retrieve all active accounts with auto topup that are due today.
        $dueAccounts = $this->tryToRetrieveTodaysDueRenewals($output);
        if (!$dueAccounts) {
            return false;
        }

        $output->logInfo(self::TASK_NAME, 'Processing %d accounts with due BWM renewals.', count($dueAccounts));
        foreach ($dueAccounts as $dueAccount) {
            $accountId = $dueAccount['account_id'];
            $accountEmail = $dueAccount['account_email'];

            if ($dueAccount['topup_bundle_id']) {
                // Try to take payment through WorldPay.
                if ($this->tryToTakePayment($output, $accountId, $dueAccount['account_cyclic_payment'], $dueAccount['topup_bundle_id']) === false) {
                    if ($this->inProduction) {
                        $this->sendPaymentNotProcessedEmail($output, $accountId, $accountEmail);
                        $this->sendPaymentNotProcessedEmailToEmailGroup($output, $accountId);
                        $this->markInvoiceAsHavingFailedPayment($output, $accountId);
                    } else {
                        $output->logInfo(
                            self::TASK_NAME,
                            'Skipping the notification for failed payment for the account %d, as we are not in production.',
                            $accountId
                        );
                    }
                    continue;
                }

                // Mark the invoice as paid.
                if ($this->markInvoiceAsPaid($output, $accountId) === false) {
                    continue;
                }

                // Notify the customer that the invoice is available.
                if ($this->inProduction) {
                    if ($this->sendInvoiceAvailableEmail($output, $accountId, $accountEmail) === false) {
                        continue;
                    }
                } else {
                    $output->logInfo(
                        self::TASK_NAME,
                        'Skipping the notification for available invoice for the account %d, as we are not in production.',
                        $accountId
                    );
                }
            } else {
                $output->logInfo(self::TASK_NAME, 'Skipping due BWM renewal payment for PAYG account %d', $accountId);
            }
        }

        // No significant, unhandled errors occurred.
        return true;
    }

    /**
     * Tries to retrieve all BWM renewals for today. Logs the exception and returns false on failure.
     *
     * @param CommandOutputInterface $output
     * @return array|bool
     */
    private function tryToRetrieveTodaysDueRenewals(CommandOutputInterface $output)
    {
        try {
            return $this->renewalStore->retrieveDueBwmRenewals(new DateTime());
        } catch (Exception $e) {
            $output->logError(self::TASK_NAME, 'Failed to retrieve due BWM renewals.');
            $this->logException($output, $e->getMessage());
        }
        return false;
    }

    /**
     * Tries to take payment for the due renewal. Logs the exception and returns false on failure.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     * @param float $paymentAmount
     * @param int $topUpBundleId
     * @return bool
     */
    private function tryToTakePayment(CommandOutputInterface $output, $accountId, $paymentAmount, $topUpBundleId)
    {
        $output->logInfo(self::TASK_NAME, 'Trying to take payment from the account %d using top-up bundle with id %d.', $accountId, $topUpBundleId);
        try {
            // The inProduction boolean is negated because the take-payment method takes a testMode variable,
            // which is the inverse of inProduction.
            $this->paymentHandler->tryToTakePaymentForBwmRenewal($accountId, $paymentAmount, $topUpBundleId, !$this->inProduction);
        } catch (Exception $e) {
            $output->logError(
                self::TASK_NAME,
                'Failed to take payment from the account %d. The top-up bundle used has the id %d.',
                $accountId,
                $topUpBundleId
            );
            $this->logException($output, $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Tries to mark the invoice for the due renewal as paid. Logs the exception and returns false on failure.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     * @return bool
     */
    private function markInvoiceAsPaid(CommandOutputInterface $output, $accountId)
    {
        $output->logInfo(
            self::TASK_NAME,
            'Marking the pending BWM renewal invoice for the account %d as paid.',
            $accountId
        );
        try {
            $this->invoiceHandler->markInvoiceAsPaid($accountId);
        } catch (Exception $e) {
            $output->logError(
                self::TASK_NAME,
                'Failed to mark BWM renewal invoice as paid, for the account %d.',
                $accountId
            );
            $this->logException($output, $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Tries to notify the customer that the invoice is available. Logs the exception and returns false on failure.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     * @param string $adminEmail
     * @return bool
     */
    private function sendInvoiceAvailableEmail(CommandOutputInterface $output, $accountId, $adminEmail)
    {
        $output->logInfo(
            self::TASK_NAME,
            'Notifying the account %d that the BWM renewal invoice is available.',
            $accountId
        );
        try {
            $this->emailSender->sendInvoiceAvailableEmail($adminEmail);
        } catch (Exception $e) {
            $output->logError(
                self::TASK_NAME,
                'Failed to notify plus admin of available invoice, for the account %d.',
                $accountId
            );
            $this->logException($output, $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Tries to notify the customer that the payment has failed. Logs the exception on failure.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     * @param string $accountEmail
     */
    private function sendPaymentNotProcessedEmail(CommandOutputInterface $output, $accountId, $accountEmail)
    {
        $output->logInfo(self::TASK_NAME, 'Notifying account %d about failed payment.', $accountId);
        try {
            $this->emailSender->sendPaymentNotProcessedEmail($accountEmail);
        } catch (Exception $e) {
            $output->logError(self::TASK_NAME, 'Failed to notify account %d about failed payment.', $accountId);
            $this->logException($output, $e->getMessage());
        }
    }

    /**
     * Tries to notify the relevant email group that the payment has failed. Logs the exception on failure.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     */
    private function sendPaymentNotProcessedEmailToEmailGroup(CommandOutputInterface $output, $accountId)
    {
        $output->logInfo(self::TASK_NAME, 'Notifying email group about failed payment for account %d.', $accountId);
        try {
            $this->emailSender->sendPaymentNotProcessedEmailToEmailGroup(self::EMAIL_GROUP, $accountId);
        } catch (Exception $e) {
            $output->logError(
                self::TASK_NAME,
                'Failed to notify email group about failed payment for account %d.',
                $accountId
            );
            $this->logException($output, $e->getMessage());
        }
    }

    /**
     * @param CommandOutputInterface $output
     * @param string $exceptionMessage
     */
    private function logException(CommandOutputInterface $output, $exceptionMessage)
    {
        $output->logError(self::TASK_NAME, 'Exception message: %s', $exceptionMessage);
    }

    /**
     * Marks the pending BWM renewal invoice as having a failed payment.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     * @author Maarten Jacobs
     */
    private function markInvoiceAsHavingFailedPayment(CommandOutputInterface $output, $accountId)
    {
        $output->logInfo(
            self::TASK_NAME,
            'Marking pending BWM renewal as having a failed payment for account %d.',
            $accountId
        );
        try {
            $this->invoiceHandler->markBwmRenewalInvoiceAsHavingFailedPayment($accountId);
        } catch (Exception $e) {
            $output->logError(
                self::TASK_NAME,
                'Failed to mark the pending renewal as having a failed payment for account %d.',
                $accountId
            );
            $this->logException($output, $e->getMessage());
        }
    }
}
