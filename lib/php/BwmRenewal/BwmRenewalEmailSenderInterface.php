<?php

interface BwmRenewalEmailSenderInterface
{
    /**
     * Notifies a Plus customer that a payment for the BWM renewal is due.
     *
     * @param string $accountId
     */
    public function sendPaymentDueEmail($accountId);

    /**
     * Notifies a customer that the invoice is available in myPwn.
     *
     * @param string $email
     * @return bool
     */
    public function sendInvoiceAvailableEmail($email);

    /**
     * Notifies a customer that the payment could not be processed.
     *
     * @param string $email
     * @return bool
     */
     public function sendPaymentNotProcessedEmail($email);

    /**
     * Notifies the responsible email group that the payment could not be processed.
     *
     * @param string $email
     * @param int $accountId
     * @return bool
     */
    public function sendPaymentNotProcessedEmailToEmailGroup($email, $accountId);
}
