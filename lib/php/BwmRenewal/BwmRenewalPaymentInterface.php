<?php

interface BwmRenewalPaymentInterface
{
    /**
     * @param int $accountId
     * @param float $paymentAmount
     * @param int $topUpBundleId
     * @param bool $testMode
     * @return bool
     * @throws Exception
     */
    public function tryToTakePaymentForBwmRenewal($accountId, $paymentAmount, $topUpBundleId, $testMode);
}
