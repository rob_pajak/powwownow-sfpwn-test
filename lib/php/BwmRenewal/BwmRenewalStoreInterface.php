<?php

interface BwmRenewalStoreInterface
{
    /**
     * Retrieves all due BWM renewals for the given date.
     *
     * @param DateTime $dueDate
     * @return array
     */
    public function retrieveDueBwmRenewals(DateTime $dueDate);
}
