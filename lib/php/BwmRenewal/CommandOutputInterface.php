<?php

interface CommandOutputInterface
{
    /**
     * @param string $section
     * @param string $message
     * @param mixed ... Arguments passed to sprintf-compatible method.
     * @return CommandOutputInterface
     */
    public function logInfo($section, $message);

    /**
     * @param string $section
     * @param string $errorMessage
     * @param mixed ... Arguments passed to sprintf-compatible method.
     * @return CommandOutputInterface
     */
    public function logError($section, $errorMessage);
}
