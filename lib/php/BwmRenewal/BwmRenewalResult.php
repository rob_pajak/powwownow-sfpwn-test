<?php

/**
 * Collects the result of a BWM future or due renewal.
 *
 * @author Maarten Jacobs
 */
class BwmRenewalResult
{
    /**
     * @var array
     */
    private $errors = array();

    /**
     * @var array
     */
    private $log = array();

    /**
     * @var bool
     */
    private $success = true;

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * Add an error and any data related to the error.
     *
     * @param string $errorLine
     * @param array $dueAccount
     */
    public function addError($errorLine, array $dueAccount)
    {
        $this->errors[] = array(
            'error_message' => $errorLine,
            'account_id' => $dueAccount['account_id'],
        );
        $this->success = false;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $logLine
     */
    public function addLogLine($logLine)
    {
        $this->log[] = $logLine;
    }

    /**
     * @return array
     */
    public function getLog()
    {
        return $this->log;
    }
}
