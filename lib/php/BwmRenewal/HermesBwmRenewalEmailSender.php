<?php

/**
 * Implementation of the email functionality to notify Plus admins of BWM renewals.
 *
 * @author Maarten Jacobs
 */
class HermesBwmRenewalEmailSender implements BwmRenewalEmailSenderInterface
{
    /**
     * {@inheritDoc}
     */
    public function sendPaymentDueEmail($accountId)
    {
        Hermes_Client_Rest::call('BWM.sendBwmRenewalNotification', array('account_id' => $accountId));
    }

    /**
     * {@inheritDoc}
     */
    public function sendInvoiceAvailableEmail($email)
    {
        Hermes_Client_Rest::call(
            'DotMailer.sendTriggeredCampaignEmail',
            array(
                'address_book_name' => 'BWM_12month_Renewal_Successful',
                'email' => $email,
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function sendPaymentNotProcessedEmail($email)
    {
        Hermes_Client_Rest::call(
            'DotMailer.sendTriggeredCampaignEmail',
            array(
                'address_book_name' => 'BWM_12month_Renewal_Unsuccessful',
                'email' => $email,
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function sendPaymentNotProcessedEmailToEmailGroup($email, $accountId)
    {
        Hermes_Client_Rest::call(
            'DotMailer.sendTriggeredCampaignEmail',
            array(
                'address_book_name' => 'BWM - Failed renewal notification for email group',
                'email' => $email,
                'campaign_arguments' => array('accountId' => $accountId),
            )
        );
    }
}
