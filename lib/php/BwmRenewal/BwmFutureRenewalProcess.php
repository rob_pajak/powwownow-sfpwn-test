<?php

require_once dirname(__FILE__) . '/CommandOutputInterface.php';

/**
 * Handles the notifications and pending invoice creation for upcoming BWM renewals.
 */
class BwmFutureRenewalProcess
{
    const TASK_NAME = 'Generating upcoming renewals:';

    /**
     * @var BwmRenewalEmailSenderInterface
     */
    private $emailSender;

    /**
     * @var BwmRenewalInvoiceHandlerInterface
     */
    private $invoiceHandler;

    /**
     * @var BwmRenewalStoreInterface
     */
    private $renewalStore;

    /**
     * @var bool
     */
    private $inProduction;

    /**
     * @param BwmRenewalEmailSenderInterface $emailSender
     * @param BwmRenewalInvoiceHandlerInterface $invoiceHandler
     * @param BwmRenewalStoreInterface $renewalStore
     * @param bool $inProduction
     */
    public function __construct(
        BwmRenewalEmailSenderInterface $emailSender,
        BwmRenewalInvoiceHandlerInterface $invoiceHandler,
        BwmRenewalStoreInterface $renewalStore,
        $inProduction
    )
    {
        $this->emailSender = $emailSender;
        $this->invoiceHandler = $invoiceHandler;
        $this->renewalStore = $renewalStore;
        $this->inProduction = $inProduction;
    }

    /**
     * Runs the future-renewal process, which takes the following steps:
     *  1. Retrieve the renewals that are due in 7 days and iterates over each renewal.
     *  2. Create a pending invoice for the renewal.
     *  3. Notify the Plus admin that a BWM renewal payment is due in 7 days.
     *
     * @param CommandOutputInterface $output
     * @return bool
     */
    public function handleFutureRenewalNotifications(CommandOutputInterface $output)
    {
        $dueDate = new DateTime('+7 days');

        // Retrieve all active accounts that will be renewed in exactly 7 days from today.
        $dueAccounts = $this->tryToRetrieveDueRenewals($output, $dueDate);
        if (!$dueAccounts) {
            return false;
        }

        $output->logInfo(self::TASK_NAME, 'Processing %d accounts with upcoming BWM renewals.', count($dueAccounts));
        foreach ($dueAccounts as $dueAccount) {
            $accountId = $dueAccount['account_id'];
            $adminEmail = $dueAccount['account_email'];

            // Create an invoice for the renewal, listing all dedicated numbers.
            $invoiceResult = $this->createPendingInvoice(
                $output,
                $accountId,
                $dueDate,
                $dueAccount['account_cyclic_payment'],
                $dueAccount['dedicated_number_count']
            );
            if ($invoiceResult === false) {
                continue;
            }

            // Notify the customer.
            if ($this->inProduction) {
                $this->sendPaymentDueEmail($output, $accountId, $adminEmail);
            }
        }

        // No significant, unhandled errors occurred.
        return true;
    }

    /**
     * Tries to retrieve the due renewals. Logs the exception and returns false on failure.
     *
     * @param CommandOutputInterface $output
     * @param DateTime $dueDate
     * @return array|bool
     */
    private function tryToRetrieveDueRenewals(CommandOutputInterface $output, DateTime $dueDate)
    {
        try {
            return $this->renewalStore->retrieveDueBwmRenewals($dueDate);
        } catch (Exception $e) {
            $output->logError(self::TASK_NAME, 'Failed to retrieve upcoming BWM renewals.');
            $this->logException($output, $e->getMessage());
        }
        return false;
    }

    /**
     * Tries to create a pending invoice for the upcoming BWM renewal. Logs the exception and returns false on failure.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     * @param DateTime $dueDate
     * @param float $paymentAmount
     * @param int $dedicatedNumberCount
     * @return bool
     */
    private function createPendingInvoice(
        CommandOutputInterface $output,
        $accountId,
        DateTime $dueDate,
        $paymentAmount,
        $dedicatedNumberCount
    )
    {
        $output->logInfo(self::TASK_NAME, 'Creating a pending BWM renewal invoice for account %d.', $accountId);
        try {
            $this->invoiceHandler->createPendingInvoice(
                $accountId,
                $dueDate,
                $paymentAmount,
                $dedicatedNumberCount
            );
        } catch (Exception $e) {
            $output->logError(self::TASK_NAME, 'Failed to create pending invoice for the account %d.', $accountId);
            $this->logException($output, $e->getMessage());
            return false;
        }
        $output->logInfo(self::TASK_NAME, 'Created a pending BWM renewal invoice for account %d.', $accountId);
        return true;
    }

    /**
     * Tries to notify the plus admin that a BWM renewal payment is due in 7 days.  Logs the exception and returns false
     * on failure.
     *
     * @param CommandOutputInterface $output
     * @param int $accountId
     * @return bool
     */
    private function sendPaymentDueEmail(CommandOutputInterface $output, $accountId)
    {
        $output->logInfo(self::TASK_NAME, 'Notifying account %d that BWM renewal is upcoming.', $accountId);
        try {
            $this->emailSender->sendPaymentDueEmail($accountId);
        } catch (Exception $e) {
            $output->logError(self::TASK_NAME, 'Failed to notify account %d for upcoming BWM renewal.', $accountId);
            $this->logException($output, $e->getMessage());
            return false;
        }
        $output->logInfo(self::TASK_NAME, 'Notified account %d that BWM renewal is upcoming.', $accountId);
        return true;
    }

    /**
     * @param CommandOutputInterface $output
     * @param string $exceptionMessage
     */
    private function logException(CommandOutputInterface $output, $exceptionMessage)
    {
        $output->logError(self::TASK_NAME, 'Exception message: %s', $exceptionMessage);
    }
}
