<?php

class HermesBwmRenewalPayment implements BwmRenewalPaymentInterface
{
    /**
     * {@inheritDoc}
     */
    public function tryToTakePaymentForBwmRenewal($accountId, $paymentAmount, $topUpBundleId, $testMode)
    {
        try {
            return Hermes_Client_Rest::call(
                'BWM.chargeCustomerForBwmRenewal',
                array(
                    'account_id' => $accountId,
                    'payment_amount_without_vat' => $paymentAmount,
                    'test_mode' => $testMode ? 1 : 0,
                    'topup_bundle_id' => $topUpBundleId,
                )
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            throw new Exception('Call to take payment has timed out. Error message: ' . $e->getMessage());
        } catch (Hermes_Client_Exception $e) {
            throw new Exception('Call to take payment failed. Error message: ' . $e->getMessage());
        } catch (Exception $e) {
            throw new Exception('Unknown exception occurred. Error message: ' . $e->getMessage());
        }
    }
}
