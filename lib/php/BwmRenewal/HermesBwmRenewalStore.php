<?php

class HermesBwmRenewalStore implements BwmRenewalStoreInterface
{
    /**
     * {@inheritDoc}
     */
    public function retrieveDueBwmRenewals(DateTime $dueDate)
    {
        return Hermes_Client_Rest::call('BWM.getDueBwmRenewals', array('due_date' => $dueDate->format('Y-m-d')));
    }
}
