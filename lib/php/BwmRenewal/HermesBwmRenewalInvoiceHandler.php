<?php

class HermesBwmRenewalInvoiceHandler implements BwmRenewalInvoiceHandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function createPendingInvoice($accountId, DateTime $dueDate, $totalCost, $numberOfDedicated)
    {
        return Hermes_Client_Rest::call(
            'BWM.createBwmRenewalInvoice',
            array(
                'account_id' => $accountId,
                'account_cyclic_payment' => $totalCost,
                'dedicated_number_count' => $numberOfDedicated,
                'due_date' => $dueDate->format('Y-m-d'),
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function markInvoiceAsPaid($accountId)
    {
        return Hermes_Client_Rest::call(
            'BWM.markPendingBwmRenewalInvoiceForAccountAsPaid',
            array('account_id' => $accountId)
        );
    }

    /**
     * {@inheritDoc}
     */
    public function markBwmRenewalInvoiceAsHavingFailedPayment($accountId)
    {
        return Hermes_Client_Rest::call(
            'BWM.markBwmRenewalInvoiceAsHavingFailedPayment',
            array('account_id' => $accountId)
        );
    }
}
