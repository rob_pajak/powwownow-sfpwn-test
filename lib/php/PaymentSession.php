<?php

class PaymentSession
{
    private $cartID;
    private $accountId;
    private $paymentId;
    private $addedProducts;
    private $amountPreVAT;
    private $percentage;
    private $autotopup;
    private $processedPayment;
    private $creationTime;
    private $affiliateCode;

    public function __construct($cartID, $accountId, array $sessionData = array())
    {
        $this->cartID = $cartID;
        $this->accountId = $accountId;
        $this->paymentId = isset($sessionData['payment_id']) ? $sessionData['payment_id'] : null;
        $this->addedProducts = isset($sessionData['added_products']) ? $sessionData['added_products'] : null;
        $this->amountPreVAT = isset($sessionData['amount_pre_vat']) ? $sessionData['amount_pre_vat'] : null;
        $this->percentage = isset($sessionData['vat_percentage']) ? $sessionData['vat_percentage'] : null;
        $this->autotopup = !empty($sessionData['autotopup']);
        if (!empty($sessionData['summary_processed'])) {
            $this->processedPayment = $sessionData['summary_processed'];
        }
        if (!empty($sessionData['creation_time'])) {
            $this->creationTime = strtotime($sessionData['creation_time']);
        }
        if (!empty($sessionData['affiliate_code'])) {
            $this->affiliateCode = $sessionData['affiliate_code'];
        } else {
            $this->affiliateCode = 'customer';
        }
    }

    public function getCartID()
    {
        return $this->cartID;
    }

    public function getAccountId()
    {
        return $this->accountId;
    }

    public function hasPaymentId()
    {
        return isset($this->paymentId);
    }

    public function getPaymentId()
    {
        return $this->paymentId;
    }

    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;
        return $this;
    }

    public function hasAddedProducts()
    {
        return isset($this->addedProducts);
    }

    public function getAddedProducts()
    {
        return $this->addedProducts;
    }

    public function setAddedProducts($addedProducts)
    {
        $this->addedProducts = $addedProducts;
        return $this;
    }

    public function hasAmountPreVAT()
    {
        return isset($this->amountPreVAT);
    }

    public function getAmountPreVAT()
    {
        return $this->amountPreVAT;
    }

    public function setAmountPreVAT($amountPreVAT)
    {
        $this->amountPreVAT = $amountPreVAT;
        return $this;
    }

    public function hasVATPercentage()
    {
        return isset($this->percentage);
    }

    public function getVATPercentage()
    {
        return $this->percentage;
    }

    public function setVATPercentage($percentage)
    {
        $this->percentage = $percentage;
        return $this;
    }

    public function getAutotopup()
    {
        return $this->autotopup;
    }

    public function setAutotopup($autotopup)
    {
        $this->autotopup = !empty($autotopup);
        return $this;
    }

    public function hasProcessedPayment()
    {
        return !empty($this->processedPayment);
    }

    public function getProcessedPayment()
    {
        return $this->processedPayment;
    }

    public function getCreationTime()
    {
        return $this->creationTime;
    }

    public function getAffiliateCode()
    {
        return $this->affiliateCode;
    }

    public function setAffiliateCode($affiliateCode)
    {
        $this->affiliateCode = $affiliateCode;
        return $this;
    }

    public function asArray()
    {
        return array(
            'cart_id' => $this->cartID,
            'account_id' => $this->accountId,
            'payment_id' => $this->paymentId,
            'added_products' => $this->addedProducts,
            'amount_pre_vat' => $this->amountPreVAT,
            'vat_percentage' => $this->percentage,
            'autotopup' => $this->autotopup,
            'affiliate_code' => $this->affiliateCode,
        );
    }
}
