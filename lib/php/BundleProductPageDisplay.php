<?php

/**
 * Collection of preparation steps to display bundles on the product pages.
 *
 * @author Maarten Jacobs
 */
class BundleProductPageDisplay
{
    /**
     * Runs through the preparation steps to display the bundle products.
     *
     * @param array $bundles
     * @param boolean $isPostPayCustomer
     * @param boolean $bundleInBasket
     * @param int $bundleIdFromBasket
     * @return array
     */
    public function prepareBundlesForDisplay(array $bundles, $isPostPayCustomer, $bundleInBasket, $bundleIdFromBasket)
    {
        uasort($bundles, array($this, 'sortBundlesByIndividualStatus'));
        $bundles = $this->groupBundlesByBasicWithUpgrade($bundles);
        $bundleLinks = sfConfig::get('app_bundle_links');
        $bundleDialogs = sfConfig::get('app_bundle_dialog_default');
        $bundleAddOnTitles = sfConfig::get('app_bundle_addon_titles');
        $bundleAddOnLabels = sfConfig::get('app_bundle_addon_labels');
        foreach ($bundles as &$bundle) {
            $bundle = $this->determineButtonsToShowForBundle($bundle, $isPostPayCustomer, $bundleInBasket, $bundleIdFromBasket);
            $bundle = $this->addBundleDescriptionLinks($bundle, $bundleLinks);
            $bundle = $this->addBundleProductDialog($bundle, $bundleDialogs);
            $bundle = $this->addBundleIcons($bundle);
            $bundle = $this->addBundleAddOnTitleAndLabel($bundle, $bundleAddOnTitles, $bundleAddOnLabels);
            $bundle = $this->addBundleComplianceLabel($bundle);
        }
        unset($bundle);
        return $bundles;
    }

    /**
     * Compares two bundles. Prioritises individual bundles.
     *
     * @param array $bundleA
     * @param array $bundleB
     * @return int
     */
    protected function sortBundlesByIndividualStatus(array $bundleA, array $bundleB)
    {
        if (!empty($bundleA['is_individual_bundle'])) {
            return -1;
        } else {
            return strcmp($bundleA['group_name'], $bundleB['group_name']);
        }
    }

    /**
     * Based on the number of minutes and add-on status, adds description links and hover text.
     *
     * @param array $bundle
     * @param array $bundleLinks
     * @return array
     *   The altered bundle array
     */
    private function addBundleDescriptionLinks(array $bundle, array $bundleLinks)
    {
        $bundleKey = $bundle['identifier'];
        if (isset($bundleLinks[$bundleKey])) {
            $bundle['links'] = $bundleLinks[$bundleKey];
        }
        return $bundle;
    }

    /**
     * @param array $bundle
     * @param boolean $isPostPayCustomer
     * @param boolean $bundleInBasket
     * @param int $bundleIdFromBasket
     * @return array
     */
    private function determineButtonsToShowForBundle(array $bundle, $isPostPayCustomer, $bundleInBasket, $bundleIdFromBasket)
    {
        $bundle['product_group_id'] = (int) $bundle['product_group_id'];
        $bundleIdFromBasket = (int) $bundleIdFromBasket;

        if ($isPostPayCustomer || ($bundleInBasket && $bundleIdFromBasket !== $bundle['product_group_id'])) {
            $bundle['show_button'] = 'disabled-select-button';
        } elseif ($bundleInBasket && $bundleIdFromBasket === $bundle['product_group_id']) {
            $bundle['show_button'] = 'basket-select-button';
        } else {
            $bundle['show_button'] = 'select-button';
        }
        return $bundle;
    }

    /**
     * Groups Bundles by basic with a single add-on (possibly to change in the future).
     *
     * @param array $bundles
     * @return array
     */
    private function groupBundlesByBasicWithUpgrade($bundles)
    {
        foreach ($bundles as &$bundle) {
            if (!empty($bundle['addons'])) {
                $bundle['upgrade_bundle'] = array_shift($bundle['addons']);
            }
        }
        return $bundles;
    }

    /**
     * Based on the number of minutes and add-on status, adds the dialog content.
     *
     * @param array $bundle
     * @param array $bundleDialogs
     * @return array
     *   The altered bundle array
     */
    protected function addBundleProductDialog(array $bundle, array $bundleDialogs)
    {
        $bundle['product_dialog'] = $this->retrieveDialogForBundle($bundle, $bundleDialogs);
        return $bundle;
    }

    /**
     * Retrieves the dialog relevant to the bundle.
     *
     * @param array $bundle
     * @param array $bundleDialogs
     * @return bool
     */
    public function retrieveDialogForBundle(array $bundle, array $bundleDialogs)
    {
        $bundleKey = $bundle['identifier'];
        if (isset($bundleDialogs[$bundleKey])) {
            return $bundleDialogs[$bundleKey];
        }
        return false;
    }

    /**
     * Based on the number of minutes and add-on status, adds icon to the bundle.
     *
     * @param array $bundle
     * @return array
     *   The altered bundle array
     */
    protected function addBundleIcons(array $bundle)
    {
        if (isset($bundle['group_thumbnail'])) {
            $bundle['icon'] = $bundle['group_thumbnail'];
        } else {
            $bundle['icon'] = '/sfimages/products/cowgirl-bwm.png';
        }
        return $bundle;
    }

    /**
     * Based on the number of minutes, adds a label to the add-on bundle.
     *
     * @param array $bundle
     * @param array $addOnTitles
     * @param array $addOnLabels
     * @return array
     */
    private function addBundleAddOnTitleAndLabel(array $bundle, array $addOnTitles, array $addOnLabels)
    {
        $bundleKey = $bundle['identifier'];
        if (isset($addOnTitles[$bundleKey])) {
            $bundle['upgrade_bundle']['title'] = $addOnTitles[$bundleKey];
        }
        if (isset($addOnLabels[$bundleKey])) {
            $bundle['upgrade_bundle']['label'] = $addOnLabels[$bundleKey];
        }
        return $bundle;
    }

    /**
     * Adds the label to be used for the compliance checkbox (Terms and Conditions, Fair Use policy, etc.).
     *
     * @param array $bundle
     * @return array
     */
    private function addBundleComplianceLabel(array $bundle)
    {
        $bundle['compliance_label'] = 'I have read and agreed to the <a href="/Terms-And-Conditions">terms and conditions</a> for this product.';
        return $bundle;
    }
}
