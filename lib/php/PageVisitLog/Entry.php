<?php
/**
 * The class for each individual entry in to the PWN_Tracking_PageVisitLog
 */
class PWN_Tracking_PageVisitLog_Entry
{
    public $datetime;
    public $url;
    public $referrerUrl;
    public $referrerPersistentlyStored;
    public $ip;
    public $httpUserAgent;

    /**
     * A new log entry, if values are not supplied, will default to those for the current page request
     *
     * @param null $datetime
     * @param null $url
     * @param null $referrerUrl
     * @param null $ip
     * @param bool $referrerPersistentlyStored
     * @param null $httpUserAgent
     */
    public function __construct(
        $datetime = null,
        $url = null,
        $referrerUrl = null,
        $ip = null,
        $referrerPersistentlyStored = false,
        $httpUserAgent = null
    ) {
        if (null == $datetime) {
            $this->datetime = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
        }
        if (null == $url) {
            $this->url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        }
        if (null == $referrerUrl && isset($_SERVER['HTTP_REFERER'])) {
            $this->referrerUrl = $_SERVER['HTTP_REFERER'];
        }
        if (null == $ip && isset($_SERVER['REMOTE_ADDR'])) {
            $this->ip = $_SERVER['REMOTE_ADDR'];
        }
        if (null == $httpUserAgent && isset($_SERVER['HTTP_USER_AGENT'])) {
            $this->httpUserAgent = $_SERVER['HTTP_USER_AGENT'];
        }

        $this->referrerPersistentlyStored = $referrerPersistentlyStored;
    }
}
