<?php

/**
 * Simple container class holding the balance, formatted (rounded to 2 digits precision) and unformatted (3 digits precision).
 *
 * @author Maarten Jacobs
 */
class Balance
{
    private $formattedBalance;
    private $unformattedBalance;

    /**
     * @param string $raw
     * @param string $formatted
     */
    public function __construct($raw, $formatted)
    {
        $this->formattedBalance = $formatted;
        $this->unformattedBalance = $raw;
    }

    /**
     * Returns the formatted balance, with 2 digits precision (as it is stored in the database).
     *
     * @return string
     */
    public function formatted()
    {
        return $this->formattedBalance;
    }

    /**
     * Returns the balance with 3 digits precision (as it is stored in the database).
     *
     * @return float
     */
    public function raw()
    {
        return $this->unformattedBalance;
    }
}
