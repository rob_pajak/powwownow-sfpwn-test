<?php
/**
 * Ip Retriever(as the name implies) retrieves user's IP address from header,
 * the reason of using it is that sfPwn site is now standing behind DDOS service
 * which is spoofing normal IP address header and because of that we should use
 * IP address from 'x-forwarded-for' header.
 *
 * PHP Version 5.2
 *
 * @copyright 2013 Kamil Skowron <kamil.skowron@powwownow.com>
 * @license   http://www.powwownow.co.uk Proprietary software
 * @link      http://powwownow.co.uk
 */
class IpRetriever
{
    /**
     * Placeholder for a pointer to symfony request object
     *
     * @var sfWebRequest|null
     */
    private $request = null;

    /**
     * Customized constructor can take request object or it will try to retrieve
     * and assign sfContext's one.
     *
     * @param sfWebRequest|null $request If request passed here it automatically
     * assigned to object as private $request property
     *
     * @return self $this Constructor returns object of class
     */
    public function __construct($request = null)
    {
        if ($request === null) {
            $request = sfContext::getInstance()->getRequest();
        }

        $this->request = $request;
    }

    /**
     * Checks if the given IP address is one of the IP addresses of our proxy.
     *
     * The proxy IP addresses are defined in the application configuration file
     * (commonly app.yml).The pattern used is a prefix-pattern: if the proxy IP
     * is a range, then the last part of the IP (the range-part) is not given,
     * so we check that the IP starts with this pattern. If the proxy IP is not
     * a range, it is checked as a literal match against the given IP.
     *
     * @param string $ipAddress  IP address to be checked is it not proxy
     *
     * @return bool Check status
     *
     * @author Maarten Jacobs
     *
     * @amends Kamil Skowron <kamil.skowron@powwownow.com> - just moved from
     * controller to this place
     */
    public function isProxyIp($ipAddress)
    {
        $proxyIPs = sfConfig::get('app_proxy_ips');
        foreach ($proxyIPs as $ip) {
            switch ($ip['type']) {
                case 'prefix':
                    if (strpos($ipAddress, $ip['ip']) === 0) {
                        return true;
                    }
                    break;
                case 'literal':
                    if ($ipAddress === $ip['ip']) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    /**
     * Retrieves the IP-address of the request based on the headers and the
     * original IP address.
     *
     * In case the original request originates from our proxy, we retrieve the
     * IP address from the X-Forwarded-For header.
     *
     * @return array|null
     *
     * @author Maarten Jacobs
     *
     * @amends Kamil Skowron <kamil.skowron@powwownow.com> - just moved from
     * controller to this place
     */
    public function retrieveIPAddress()
    {
        $ip = $this->request->getHttpHeader('addr', 'remote');
        if ($this->isProxyIp($ip)) {
            return $this->request->getHttpHeader('x-forwarded-for');
        }

        return $ip;
    }
}