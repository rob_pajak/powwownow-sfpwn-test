<?php

class BundleProductDisplay
{
    /**
     * Prepares the assigned products for the display on the Assign-Products page and the My-Products page.
     *
     * @param array $assignedProducts
     * @param int $contactRef
     * @return array
     */
    public function prepareAssignedProducts(array $assignedProducts, $contactRef)
    {
        $bundles = plusCommon::retrieveBundleProducts(sfContext::getInstance()->getUser()->getCulture());
        return $this->prepareAssignedProductsForBundles($assignedProducts, $bundles, new BundleProductPageDisplay(), $contactRef);
    }

    private function prepareAssignedProductsForBundles(array $assignedProducts, array $bundles, BundleProductPageDisplay $bundleDisplay, $contactRef)
    {
        $bundleIds = array_fill_keys(array_keys($bundles), true);
        $bundleAddons = array();
        $bundleAddonIds = array();
        foreach ($bundles as $bundle) {
            if (!empty($bundle['addons'])) {
                foreach ($bundle['addons'] as $addon) {
                    if (!isset($bundleAddonIds[$addon['product_group_id']])) {
                        $bundleAddonIds[$addon['product_group_id']] = array();
                    }
                    $bundleAddonIds[$addon['product_group_id']][] = $bundle['product_group_id'];
                    $bundleAddons[$addon['product_group_id']] = $addon;
                }
            }
        }

        $bundleDialogs = sfConfig::get('app_bundle_dialog_default');

        if (!empty($bundle['addons'])) {
            $bundleDialogs = sfConfig::get('app_bundle_dialog_with_addon');
        }

        foreach ($assignedProducts as $productKey => $product) {
            $productGroupId = $product['product_group_id'];
            $assignedProducts[$productKey]['is_bundle'] = $isBundle = isset($bundleIds[$productGroupId]) || isset($bundleAddonIds[$productGroupId]);
            $assignedProducts[$productKey]['is_individual_bundle'] = $isIndividualBundle = (isset($bundles[$productGroupId]) && $bundles[$productGroupId]['is_individual_bundle'])
                || (!empty($bundleAddonIds[$productGroupId]) && $bundles[reset($bundleAddonIds[$productGroupId])]['is_individual_bundle']);
            if (isset($bundleAddonIds[$productGroupId])) {
                $parentBundleId = reset($bundleAddonIds[$productGroupId]);
                $assignedProducts[$productKey]['group_name'] = $bundles[$parentBundleId]['group_name'] . ' + Int. Add-On';
                $assignedProducts[$productKey]['group_name_en'] = $bundles[$parentBundleId]['group_name'] . ' + Int. Add-On';
            }
            if ($isBundle) {
                $assignedProducts[$productKey]['is_addon'] = !isset($bundleIds[$productGroupId]);
                if (isset($bundleIds[$productGroupId])) {
                    $bundle = $bundles[$productGroupId];
                } else {
                    $bundle = $bundleAddons[$productGroupId];
                }
                if ($isIndividualBundle) {
                    $assignedProducts[$productKey]['pin_pair'] = $this->retrieveIndividualBundleAssignedPinPair($productGroupId, $contactRef);
                }
                $assignedProducts[$productKey]['activation_date'] = date('d/m/Y', strtotime($assignedProducts[$productKey]['activation_date']));
                $assignedProducts[$productKey]['renewal_date'] = date('01/m/Y', strtotime('next month'));
                $assignedProducts[$productKey]['product_dialog'] = $bundleDisplay->retrieveDialogForBundle($bundle, $bundleDialogs);
            }
        }

        return $assignedProducts;
    }

    private function retrieveIndividualBundleAssignedPinPair($productGroupId, $contactRef)
    {
        if (sfContext::getInstance()->getUser()->hasCredential('PLUS_ADMIN')) {
            $assignedPinPairResult = plusCommon::retrieveIndividualBundleAssignedPinPair($productGroupId, $contactRef);
            if ($assignedPinPairResult) {
                $assignedContactEmail = $assignedPinPairResult['email'];
                $assignedPinPair = $assignedPinPairResult['pin_pair'];
                $firstPin = array_shift($assignedPinPair);
                $secondPin = array_shift($assignedPinPair);
                return sprintf(
                    '%s/%s (%s)',
                    $firstPin['pin'],
                    $secondPin['pin'],
                    $assignedContactEmail
                );
            }
        }
        return false;
    }
}
