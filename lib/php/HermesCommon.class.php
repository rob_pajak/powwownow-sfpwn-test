<?php

/**
 * Class HermesCommon.
 * This is a Class for Static Hermes Calls.
 * It also has Exception Handling here, for the actual Hermes Calls done in.
 * @author Asfer Tamimi
 */
class HermesCommon
{
    /**
     * This is the Generic Call with all the Try Catch and Exception Handling for all the Exceptions given by Hermes
     * @param $method
     * @param array $args
     * @param bool $cache
     * @param $hermesCallMethod
     * @return array
     */
    private static function doGenericCall($method, array $args, $cache = false, $hermesCallMethod)
    {
        $result = array();
        try {
            if (method_exists('HermesCalls', $hermesCallMethod)) {
                $result = call_user_func_array("HermesCalls::$hermesCallMethod", array($args, $cache));
            }
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "$method Hermes Call Has Timed Out Using Arguments: " . print_r($args, true)
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "$method Hermes Call is Faulty, Error Message: " .
                $e->getMessage() . ', Error Code: ' . $e->getCode() . ', Arguments: ' . print_r($args, true)
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "$method Exception Called. Error Message: " .
                $e->getMessage() . ', Error Code: ' . $e->getCode() . ', Arguments: ' . print_r($args, true)
            );
        }
        return $result;
    }

    /**
     * Get IMeet User by the Contact Ref
     * @param $args
     * @return array
     */
    public static function iMeetGetIMeetUserByContactRef($args)
    {
        return self::doGenericCall('IMeet.getIMeetUserByContactRef', $args, false, 'iMeetGetIMeetUserByContactRef');
    }

    /**
     * Register a Free Pin User for Shomei
     * @param $args
     * @return array
     */
    public static function shomeiRegisterFreePinForShomei($args)
    {
        return self::doGenericCall('Shomei.registerFreePinForShomei', $args, false, 'shomeiRegisterFreePinForShomei');
    }
}
