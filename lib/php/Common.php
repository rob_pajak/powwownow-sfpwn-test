<?php

/**
 * Provides common functions for all user types.
 *
 * @todo Refactor to cache data on the Context, instead of static methods.
 *
 * @author Maarten Jacobs
 */
class Common
{

    /**
     * Cache for the getCountriesList method.
     *
     * @var array
     */
    protected static $countriesList = array();

    /**
     * Cache for the hasEditableEmail method.
     *
     * @var array
     */
    protected static $hasEditableEmail = array();

    /**
     * Cache for the getContactPins method.
     *
     * @var array
     */
    protected static $contactPins = array();

    /**
     * Cache for the getContactByContactRef method.
     *
     * @var array
     */
    protected static $contacts = array();

    /**
     * Cache for the getPremiumAccountDetailsByContactRef method.
     *
     * @var array
     */
    protected static $premiumContacts = array();

    /**
     * Returns the list of countries from Hermes.
     *
     * Note: the countries list is cached by default.
     * The Country Check needs to be in the Database
     *
     * @param string $locale
     * @param bool $reset
     * @return array
     * @author Maarten Jacobs
     * @author Asfer Tamimi (Saint Barthelemy Does not have a Country ID, so got from http://countrycode.org/saintbarthelemy)
     *
     */
    public static function getCountriesList($locale, $reset = false)
    {
        $locale = '' . $locale;

        if (!isset(self::$countriesList[$locale]) || $reset) {
            self::$countriesList[$locale] = array();

            try {
                $response = Hermes_Client_Rest::call('getCountries', array('locale' => $locale));
                foreach ($response as $row) {
                    $countryId = $row['country_id'];
                    if (strstr($row['text'], 'Saint B')) {
                        self::$countriesList[$locale]['BLM'] = $row['text'];
                    } else {
                        self::$countriesList[$locale][$countryId] = $row['text'];
                    }
                }
            } catch (Exception $e) {
                // @todo: log message? Possibly get context and retrieve logger.
            }
        }

        return self::$countriesList[$locale];
    }

    /**
     * Obtain the Whole Countries Array from Database and Cache the Result
     *
     * @param  string $locale
     * @return array $result
     * @author Asfer Tamimi
     * @todo Ask Maarten about his Method, and maybe either one or the other needs to be removed.
     *
     */
    public static function getCountries($locale = 'en_GB')
    {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');

        try {
            $result = hermesCallWithCaching('getCountries', array('locale' => $locale), 'array', 3600);
            foreach ($result as $k => $v) {
                if (strstr($v['text'], 'Saint B')) {
                    $result[$k]['country_id'] = 'BLM';
                }
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Get Countries Return for Locale: ' . $locale . ', Error: ' . serialize($e)
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Asserts and caches that the user has an editable email.
     *
     * @param string $serviceUser
     *   The service of the contact; mostly stored in the session.
     * @param int $contactRef
     * @param bool $reset
     * @return bool
     * @author Maarten Jacobs
     */
    public static function hasEditableEmail($serviceUser, $contactRef, $reset = false)
    {
        if (!isset(self::$hasEditableEmail[$serviceUser][$contactRef]) || $reset) {
            if (!isset(self::$hasEditableEmail[$serviceUser])) {
                self::$hasEditableEmail[$serviceUser] = array();
            }

            // Available only for powwownow service now
            $editableEmail = $serviceUser === 'POWWOWNOW';

            // Assert that all pins assigned to the contact are of the same service.
            if ($editableEmail == true) {
                $activePins        = self::getContactPins($contactRef);
                $serviceComparison = (int)$activePins[0]['service_ref'];

                foreach ($activePins as $pins) {
                    if (((int)$pins['service_ref']) !== $serviceComparison) {
                        $editableEmail = false;
                        break;
                    }
                }
            }
            self::$hasEditableEmail[$serviceUser][$contactRef] = $editableEmail;
        }
        return self::$hasEditableEmail[$serviceUser][$contactRef];
    }

    /**
     * Retrieves and caches the pins for a given contact.
     *
     * @param $contactRef
     * @param bool $reset
     * @return array
     * @author Maarten Jacobs
     */
    public static function getContactPins($contactRef, $reset = false)
    {
        if (!isset(self::$contactPins[$contactRef]) || $reset) {
            self::$contactPins[$contactRef] = array();
            try {
                self::$contactPins[$contactRef] = Hermes_Client_Rest::call(
                    'getContactPins',
                    array('contact_ref' => $contactRef)
                );
            } catch (Exception $e) {
                // silence is golden
            }
        }
        return self::$contactPins[$contactRef];
    }

    /**
     * Retrieves and caches the contact by contact ref.
     *
     * @param int $contactRef
     * @param bool $reset
     * @return array
     */
    public static function getContactByContactRef($contactRef, $reset = false)
    {
        if (!isset(self::$contacts[$contactRef]) || $reset) {
            self::$contacts[$contactRef] = array();
            try {
                self::$contacts[$contactRef] = Hermes_Client_Rest::call(
                    'getContactByRef',
                    array('contact_ref' => $contactRef)
                );
            } catch (Exception $e) {
                // silence is golden
            }
        }
        return self::$contacts[$contactRef];
    }

    /**
     * Retrieves and caches the premium contact by contact ref.
     *
     * @param int $contactRef
     * @param bool $reset
     * @return array
     */
    public static function getPremiumAccountDetailsByContactRef($contactRef, $reset = false)
    {
        if (!isset(self::$premiumContacts[$contactRef]) || $reset) {
            self::$premiumContacts[$contactRef] = array();
            try {
                self::$premiumContacts[$contactRef] = Hermes_Client_Rest::call(
                    'getPremiumAccountDetailsByContactRef',
                    array('contact_ref' => $contactRef)
                );
            } catch (Exception $e) {
                // silence is golden
            }
        }
        return self::$premiumContacts[$contactRef];
    }

    /**
     * Retrieves the list of values assigned to the given column names.
     *
     * @param array $source
     * @param array $columns
     * @return array
     *
     * @todo: replace with call to array_column() when implemented
     * @see https://wiki.php.net/rfc/array_column
     */
    public static function retrieveArrayColumns(array $source, array $columns)
    {
        $matching = array();
        foreach ($columns as $column) {
            if (isset($source[$column])) {
                $matching[$column] = $source[$column];
            }
        }
        return $matching;
    }


    /**
     * Updates the Pin Pairs Array which is Given by adding an additional field for the Wallet Card Information
     *
     * @param  array $pinPairs
     * @param  integer $contact_ref
     * @return array $pinPairs
     * @author Asfer Tamimi
     *
     */
    public static function updatePinPairsToWalletCardInfo($pinPairs, $contact_ref)
    {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $errors = false;

        foreach ($pinPairs as $i => $pin) {
            try {
                if (!isset($pin['chair_pin_ref'])) {
                    continue;
                }

                $walletCardInfo = hermesCallWithCaching(
                    'getWalletCardAddress',
                    array(
                        'contact_ref' => $contact_ref,
                        'pin_ref'     => $pin['chair_pin_ref']
                    ),
                    'array',
                    3600
                );

                $pinPairs[$i]['remaining_wallet_card_requests'] = (isset($walletCardInfo['remaining_wallet_card_requests'])) ? $walletCardInfo['remaining_wallet_card_requests'] : 3;

            } catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'Wallet Card Information Failed to Return for Contact Ref: ' . $contact_ref . ', and Pin Ref: ' . $pin['chair_pin_ref'] . ', Error: ' . serialize(
                        $e
                    )
                );
                $pinPairs[$i]['remaining_wallet_card_requests'] = -1;
                $errors                                         = true;
            }
        }

        return array(
            'pinPairs'    => $pinPairs,
            'errorsFound' => $errors
        );
    }

    /**
     * Obtain the Wallet Card Address for a specific Contact Ref
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getWalletCardAddress($args)
    {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');

        try {
            $result = hermesCallWithCaching(
                'getWalletCardAddress',
                array(
                    'contact_ref' => $args['contact_ref'],
                    'pin_ref'     => $args['pin_ref']
                ),
                'array',
                3600
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Wallet Card Address Failed to Return for args: ' . serialize($args) . ', Error: ' . serialize($e)
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Obtain the Default Dial In Number for a User, given the Service and Locale
     *
     * @param  integer $service
     * @param  string $locale
     * @return string $dialInNumber
     * @author Asfer Tamimi
     *
     */
    public static function getDefaultDialInNumber($service = 801, $locale = 'en_GB')
    {

        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');

        $countryCode = substr($locale, -2);

        try {
            $result = hermesCallWithCaching(
                'getDialInNumbers',
                array(
                    'service_ref' => $service,
                    'locale'      => $locale
                ),
                'array',
                3600
            );

            $dialInNumber = $result[$countryCode]['national_formatted'];

            if ($locale == 'de_DE') {
                $dialInNumber .= " <sup>*</sup>";
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Default Dial In Numbers Failed to Return for Service: ' . $service . ', locale: ' . $locale . ', Error: ' . serialize(
                    $e
                )
            );
            $dialInNumber = '0844 4 73 73 73';
        }

        return $dialInNumber;
    }

    /**
     * Create a Wallet Card Request for a User, given the Wallet Card Information
     *
     * {"error_messages":[{"message":"WALLET_CARD_REQUEST_EXCEEDED_MAX_SINGLE","field_name":"alert"}]}
     * {"error_messages":[{"message":"WALLET_CARD_REQUEST_NO_PERMISSION_SINGLE","field_name":"alert"}]}
     * {"error_messages":[{"message":"WALLET_CARD_REQUEST_FAILURE_SINGLE","field_name":"alert"}]}
     * {"error_messages":[{"message":"FORM_COMMUNICATION_ERROR","field_name":"alert"}]}
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function createWalletCardRequest($args)
    {

        $errorMessages = array();
        $result        = array();

        try {
            $result = Hermes_Client_Rest::call(
                'createWalletCardRequest',
                array(
                    'contact_ref'  => $args['contact_ref'],
                    'pin_ref'      => $args['pin_ref'],
                    'organisation' => $args['company'],
                    'building'     => '',
                    'street'       => $args['street'],
                    'town'         => $args['town'],
                    'county'       => $args['county'],
                    'post_code'    => $args['post_code'],
                    'country_code' => $args['country'],
                )
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'createWalletCardRequest Hermes call has timed out. Contact Ref: ' . $args['contact_ref']
            );
            $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'createWalletCardRequest Hermes Client Exception. ErrorCode: ' . $e->getCode(
                ) . ', Message: ' . $e->getMessage()
            );
            switch ($e->getCode()) {
                case '001:001:001':
                    $errorMessages = ($args['isAdmin'])
                        ? array('message' => 'WALLET_CARD_REQUEST_EXCEEDED_MAX_SINGLE')
                        : array('message' => 'WALLET_CARD_REQUEST_EXCEEDED_MAX', 'field_name' => 'alert');
                    break;
                case '001:000:004':
                    $errorMessages = ($args['isAdmin'])
                        ? array('message' => 'WALLET_CARD_REQUEST_NO_PERMISSION_SINGLE')
                        : array('message' => 'WALLET_CARD_REQUEST_NO_PERMISSION', 'field_name' => 'alert');
                    break;
                default:
                    $errorMessages = ($args['isAdmin'])
                        ? array('message' => 'WALLET_CARD_REQUEST_FAILURE_SINGLE')
                        : array('message' => 'WALLET_CARD_REQUEST_FAILURE', 'field_name' => 'alert');
                    break;
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'createWalletCardRequest Unknown Error Occurred: ' . $e->getMessage()
            );
            $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
        }

        try {
            $cacheCleared = PWN_Cache_Clearer::walletCardRequest(
                array('contact_ref' => $args['contact_ref'], 'pin_ref' => $args['pin_ref'])
            );
            sfContext::getInstance()->getLogger()->info(
                'Cache cleared for Contact Ref: ' . $args['contact_ref'] . ', Cache: ' . serialize($cacheCleared)
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Not all cache could be cleared: ' . serialize($e));
        }

        return array('result' => $result, 'error' => $errorMessages);
    }

    /**
     * Update Contact
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function updateContact($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('updateContact', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('updateContact Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'updateContact Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'updateContact Failed. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
        }

        if (empty($result)) {
            return $result;
        }

        // Clear Contact Related Cache
        try {
            PWN_Cache_Clearer::modifiedPin($args['contact_ref']);
            PWN_Cache_Clearer::modifiedContact($args['contact_ref']);
            sfContext::getInstance()->getLogger()->info('Cache cleared for Contact Ref: ' . $args['contact_ref']);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Not all cache could be cleared: ' . serialize($e));
        }

        return $result;
    }

    /**
     * Return Request Welcome Pack Message
     *
     * @param  string $errorMessage
     * @param  string $pinPairs | ''
     * @param  bool $isAdmin
     * @return string  $message
     * @author Asfer Tamimi
     *
     */
    public static function getRequestWelcomePackMessage($errorMessage, $pinPairs = '', $isAdmin = false)
    {

        sfContext::getInstance()->getConfiguration()->loadHelpers('I18N');

        $message = '';
        // A Single PIN Pair / Enhanced PIN User 
        // An Error Can be split into 4 Types
        // 1 - WALLET_CARD_REQUEST_FAILURE
        // 2 - WALLET_CARD_REQUEST_EXCEEDED_MAX
        // 3 - WALLET_CARD_REQUEST_NO_PERMISSION
        // 4 - FORM_COMMUNICATION_ERROR
        // var WALLET_CARD_REQUEST_FAILURE       = 'An error occurred ordering your welcome pack';
        // var WALLET_CARD_REQUEST_EXCEEDED_MAX  = 'Your welcome pack request was unsuccessful as it has been requested 3 times already. Please Contact Us if you would like more welcome packs';
        // var WALLET_CARD_REQUEST_NO_PERMISSION = 'You do not have permission to request welcome packs for that User';

        // Multiple PIN Pain Users / Admins
        // There can be two Types of Errors
        // 1 - Success + Error
        // 2 - All Error
        // An Error Can be split into 4 Types:
        // 1 - WALLET_CARD_REQUEST_FAILURE_SINGLE - Currently being used for all of them
        // 2 - WALLET_CARD_REQUEST_EXCEEDED_MAX_SINGLE
        // 3 - WALLET_CARD_REQUEST_NO_PERMISSION_SINGLE
        // 4 - FORM_COMMUNICATION_ERROR
        // var WALLET_CARD_REQUEST_FAILURE_SINGLE       = 'An error occurred ordering the welcome pack for the PIN pairs of %%PINPAIR%%';
        // var WALLET_CARD_REQUEST_EXCEEDED_MAX_SINGLE  = 'Your welcome pack request was unsuccessful for the PIN pairs of %%PINPAIR%%, as it has been requested 3 times already. Please Contact Us if you would like more welcome packs for this PIN pair.';
        // var WALLET_CARD_REQUEST_NO_PERMISSION_SINGLE = 'You do not have permission to request welcome packs for the PIN pair of %%PINPAIR%%.';

        if ($isAdmin && $errorMessage == 'FORM_COMMUNICATION_ERROR') {
            return __('An error occurred ordering the welcome pack for the PIN pairs of ') . $pinPairs . '. ';
        } elseif (!$isAdmin && $errorMessage == 'FORM_COMMUNICATION_ERROR') {
            return __('An error occurred ordering your welcome pack');
        }

        switch ($errorMessage) {
            case 'WALLET_CARD_REQUEST_SUCCESS':
                $message = __('Thank you for your request. Your welcome pack is on its way!');
                break;
            case 'WALLET_CARD_REQUEST_SUCCESS_SINGLE':
                $message = __(
                        'Thank you for your request. Your welcome pack is on its way for the PIN pairs of '
                    ) . $pinPairs . '.';
                break;
            case 'WALLET_CARD_REQUEST_EXCEEDED_MAX':
                $message = __(
                    'Your welcome pack request was unsuccessful as it has been requested 3 times already. Please Contact Us if you would like more welcome packs'
                );
                break;
            case 'WALLET_CARD_REQUEST_NO_PERMISSION':
                $message = __('You do not have permission to request welcome packs for that User');
                break;
            case 'WALLET_CARD_REQUEST_EXCEEDED_MAX_SINGLE':
                $message = __('Your welcome pack request was unsuccessful for the PIN pairs of ') . $pinPairs . __(
                        ', as it has been requested 3 times already. Please Contact Us if you would like more welcome packs for this PIN pair. '
                    );
                break;
            case 'WALLET_CARD_REQUEST_NO_PERMISSION_SINGLE':
                $message = __(
                        'You do not have permission to request welcome packs for the PIN pair of '
                    ) . $pinPairs . '. ';
                break;
            case 'WALLET_CARD_REQUEST_FAILURE_SINGLE':
                $message = __('An error occurred ordering the welcome pack for the PIN pairs of ') . $pinPairs . '. ';
                break;
            case 'WALLET_CARD_REQUEST_FAILURE':
            default:
                $message = __('An error occurred ordering your welcome pack');
                break;
        }

        return $message;
    }

    /**
     * Obtain the Contact Details for a Given Contact Ref and Master Service Ref
     *
     * @param  array $args
     * @return array $result (Cached)
     * @author Asfer Tamimi
     */
    public static function getContactByRef($args)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array('error' => 'FORM_COMMUNICATION_ERROR');

        try {
            return hermesCallWithCaching('getContactByRef', $args, 'array', 3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getContactByRef Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getContactByRef Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getContactByRef Failed. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
        }

        return $result;
    }

    /**
     * Obtain the Contact Details for a Given Email Address
     *
     * @param string $email
     * @param int $service_ref [Default: NULL]
     * @param bool $clearCache [Default: false]
     * @return array $result (Cached)
     * @author Asfer Tamimi
     *
     */
    public static function getContact($email, $service_ref = null, $clearCache = false)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array('error' => 'FORM_COMMUNICATION_ERROR');
        $args   = array('email' => $email);
        if (!is_null($service_ref)) {
            $args['service_ref'] = $service_ref;
        }

        if ($clearCache) {
            // Clear Teamsite and Symfony Cache
            try {
                hermesCallRemoveCaching('getContact', $args, 'all');
                sfContext::getInstance()->getLogger()->info(
                    'Cleared Cache for email: ' . $email
                );
            } catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'Unable to clear cache for email: ' . $email . '. Error: ' . serialize($e)
                );
            }
        }

        try {
            $result = hermesCallWithCaching('getContact', $args, 'array', 3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getContact Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getContact Hermes call is faulty. Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode(
                )
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getContact Failed. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
        }

        return $result;
    }

    /**
     * Check if a Password is Invalid.
     * Returns an Error String or a false
     *
     * @param  string $password
     * @return string | false
     * @author Asfer Tamimi
     *
     */
    public static function checkifInvalidPassword($password)
    {
        if (empty($password)) {
            return 'FORM_VALIDATION_NEW_PASSWORD_EMPTY';
        } elseif (strlen($password) < 6) {
            return 'FORM_VALIDATION_PASSWORD_LESS_THAN_SIX_CHARS';
        } elseif (substr($password, 0, 1) == ' ' || substr($password, -1) == ' ') {
            return 'FORM_VALIDATION_INVALID_PASSWORD_SPACES';
        } else {
            return false;
        }
    }

    /**
     * Check if an Email Address is a Standard Account on Service 801 and only has 1 PIN
     *
     * @param string $email
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getStandardAccount($email)
    {
        $result = array();

        try {
            $result = Hermes_Client_Rest::call('getStandardAccount', array('email' => $email));
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getStandardAccount Failed to Return a Result for Email: ' . $email . ', Error: ' . serialize($e)
            );
        }

        return $result;
    }

    /**
     * Updates the Given Array, to be Filtered Prior being sent to the Datatable Component
     *
     * @param array $data
     * @param string $page
     * @param array $selectionArr - Optional
     * @return array
     * @author Asfer Tamimi
     *
     */
    public static function getFilteredData($data, $page, $selectionArr = array())
    {
        foreach ($data as $id => $row) {
            switch ($page) {
                case 'USERS':
                case 'PINS':
                    // Updates to Row Array
                    if (!isset($row['chair_first_name']) || empty($row['chair_first_name'])) {
                        $row['chair_first_name'] = sfContext::getInstance()->getUser()->getAttribute('first_name');
                    }

                    if (!isset($row['chair_last_name']) || empty($row['chair_last_name'])) {
                        $row['chair_last_name'] = sfContext::getInstance()->getUser()->getAttribute('last_name');
                    }

                    if (!isset($row['chair_email']) || empty($row['chair_email'])) {
                        $row['chair_email'] = sfContext::getInstance()->getUser()->getAttribute('email');
                    }

                    // Updates to Data Array
                    $data[$id]['chair_email']     = $row['chair_email'];
                    $data[$id]['checkboxcol']     = '<input type="checkbox" name="r[]" value="' . $row['chair_pin_ref'] . '"/>';
                    $data[$id]['chair_full_name'] = $row['chair_first_name'] . ' ' . $row['chair_last_name'];

                    if (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_ADMIN')) {
                        $data[$id]['chair_full_name'] .= '<br/><a href="/myPwn/User-Details/' . $row['chair_contact_ref'] . '" title="Edit User">Edit User</a>';
                    }
                    break;

                case 'Request-Welcome-Pack':
                    // Check if the Chair PIN Ref || PIN exists.
                    if (!isset($row['chair_pin_ref'], $row['chair_pin']) && sfContext::getInstance()->getUser(
                        )->hasCredential('admin')
                    ) {
                        unset($data[$id]);
                        continue;
                    }

                    // Updates to Row Array
                    if (!isset($row['chair_first_name']) || empty($row['chair_first_name'])) {
                        $row['chair_first_name'] = sfContext::getInstance()->getUser()->getAttribute('first_name');
                    }

                    if (!isset($row['chair_last_name']) || empty($row['chair_last_name'])) {
                        $row['chair_last_name'] = sfContext::getInstance()->getUser()->getAttribute('last_name');
                    }

                    // Updates to Data Array
                    $data[$id]['chair_full_name'] = $row['chair_first_name'] . ' ' . $row['chair_last_name'];
                    if (sfContext::getInstance()->getUser()->hasCredential('admin')) {
                        $data[$id]['checkboxcol'] = '<input onclick="requestWelcomePackUpdatePreview(\'' . $row['chair_pin'] . '\', \'' . $row['participant_pin'] . '\', \'' . $row['chair_first_name'] . '\', \'' . $row['chair_last_name'] . '\');" type="checkbox" id="requestWelcomePack_pin_ref" name="requestWelcomePack[pin_ref][]" value="' . $row['chair_pin_ref'] . ',' . $row['chair_pin'] . ',' . $row['participant_pin'] . '"/>';
                        $data[$id]['checkboxcol'] = (in_array($row['chair_pin_ref'], $selectionArr)) ? str_replace(
                            '/>',
                            ' checked="checked"/>',
                            $data[$id]['checkboxcol']
                        ) : $data[$id]['checkboxcol'];
                    } elseif (sfContext::getInstance()->getUser()->hasCredential('PREMIUM_USER')) {
                        $data[$id]['checkboxcol'] = '<input onclick="requestWelcomePackUpdatePreview(\'' . $row['chair_pin'] . '\', \'' . $row['participant_pin'] . '\', \'' . $row['chair_first_name'] . '\', \'' . $row['chair_last_name'] . '\');" type="radio" id="requestWelcomePack_pin_ref" name="requestWelcomePack[pin_ref][]" value="' . $row['chair_pin_ref'] . ',' . $row['chair_pin'] . ',' . $row['participant_pin'] . '"/>';
                        $data[$id]['checkboxcol'] = (in_array($row['chair_pin_ref'], $selectionArr)) ? str_replace(
                            '/>',
                            ' checked="checked"/>',
                            $data[$id]['checkboxcol']
                        ) : $data[$id]['checkboxcol'];
                    } else {
                        $data[$id]['checkboxcol'] = '&nbsp;';
                    }
                    break;

                case 'Call-Settings':
                    // Updates to Row Array
                    if (!isset($row['chair_first_name']) || empty($row['chair_first_name'])) {
                        $row['chair_first_name'] = sfContext::getInstance()->getUser()->getAttribute('first_name');
                    }

                    if (!isset($row['chair_last_name']) || empty($row['chair_last_name'])) {
                        $row['chair_last_name'] = sfContext::getInstance()->getUser()->getAttribute('last_name');
                    }

                    // Updates to Data Array
                    $data[$id]['chair_full_name'] = $row['chair_first_name'] . ' ' . $row['chair_last_name'];
                    $data[$id]['checkboxcol']     = '<input type="checkbox" id="callSettings_pin_ref" name="callSettings[pin_ref][]" value="' . $row['chair_pin_ref'] . '"/>';
                    $data[$id]['checkboxcol']     = (in_array($row['chair_pin_ref'], $selectionArr)) ? str_replace(
                        '/>',
                        ' checked="checked"/>',
                        $data[$id]['checkboxcol']
                    ) : $data[$id]['checkboxcol'];
                    break;

                case 'Recordings':
                    $recordingSD = strtotime($row['start_date']);
                    $recordingED = strtotime($row['end_date']);
                    $fromdate    = strtotime(
                        (isset($selectionArr['from_date']) ? $selectionArr['from_date'] : date(
                            "Y-m-d",
                            mktime(0, 0, 0, date("m"), date("d"), date("Y") - 1)
                        ))
                    );
                    $todate      = strtotime(
                        (isset($selectionArr['to_date']) ? $selectionArr['to_date'] . ' 23:59:59' : date(
                            "Y-m-d 23:59:59"
                        ))
                    );

                    /*if (!$row['recording_verified_in_filesystem']) {
                        unset($data[$id]);
                        continue;
                    } else*/
                    if (empty($row['filepath'])) {
                        unset($data[$id]);
                        continue;
                    } elseif ($fromdate > $recordingSD || $recordingED > $todate) {
                        unset($data[$id]);
                        continue;
                    } elseif (!empty($selectionArr['description']) && !strstr(
                            $row['description'],
                            $selectionArr['description']
                        )
                    ) {
                        unset($data[$id]);
                        continue;
                    } elseif (!empty($selectionArr['pin']) && (!strstr(
                                $row['chairperson_pin'],
                                $selectionArr['pin']
                            ) && !strstr($row['participant_pin'], $selectionArr['pin']))
                    ) {
                        unset($data[$id]);
                        continue;
                    }

                    // Last Column - Needs to be placed somewhere else
                    $download = '<a href="/s/Download-Recording?recording_ref=' . $row['recording_ref'] . '&amp;action=download" target="_blank" style="border:none;" title="Download recording as mp3">';
                    $download .= '<img style="margin-right:10px;" src="/sfimages/download-grey.png" alt="Download recording as mp3"></a>';
                    $download .= '<a class="rec_link" href="javascript:recordingsShareRecordings(\'' . $row['recording_ref'] . '\',\'' . $row['publish'] . '\',\'' . $row['publish_expire_formatted'] . '\',\'' . ((isset($row['password'])) ? '1' : '0') . '\',\'ref_' . $id . '\')" style="border:none;" title="Share recording as mp3"><img src="/sfimages/options-grey.png" alt="Share recording as mp3"></a>';
                    if ($row['publish'] == 1) {
                        $download .= '&nbsp;&nbsp;<a style="color: #FFB027" href="http://www.powwownow.co.uk/Recordings/recording/id/' . $row['recording_ref'] . '" target="_blank">[Public Link]</a>';
                    }
                    $download .= '<input type="hidden" value="' . $row['recording_ref'] . '" id="ref_' . $id . '"></td>';

                    // Updates to Data Array
                    if (isset($row['pins_active']) && (int)$row['pins_active'] !== 1) {
                        $inactivePinIndicator           = '<sup class="deactivated-pin" alt="Deactivated pin" title="Deactivated pin">1</sup>';
                        $data[$id]['pin_pair']          = $row['participant_pin'] . $inactivePinIndicator;
                        $data['contains_inactive_pins'] = true;
                    } else {
                        $data[$id]['pin_pair'] = $row['chairperson_pin'] . ' / ' . $row['participant_pin'];
                    }
                    $data[$id]['play_recording'] = '<audio width="220" preload="none" src="/s/Download-Recording?recording_ref= ' . $row['recording_ref'] . '&action=listen&autostart=yes" type="audio/mp3" controls="controls"></audio>';

                    $data[$id]['download_share_recording'] = $download;

                    // Remove other Fields from Array
                    $kArr = array(
                        'pin_pair',
                        'start_date',
                        'description',
                        'duration_formatted',
                        'play_recording',
                        'download_share_recording'
                    );
                    foreach ($row as $k => $v) {
                        if (!in_array($k, $kArr)) {
                            unset($data[$id][$k]);
                        }
                    }
                    break;

                case 'Call-History':
                    // Updates to Data Array
                    $data[$id]['start_date_time'] = $row['start_date'] . ' ' . $row['start_time'];
                    $data[$id]['end_date_time']   = $row['end_date'] . ' ' . $row['end_time'];
                    $data[$id]['pin_pair']        = $row['pin'] . ' / ' . $row['master_pin'];

                    if (isset($data[$id]['duration'])) {
                        $data[$id]['duration'] = self::formatCallHistoryDuration($data[$id]['duration']);
                    }
                    if (isset($data[$id]['dnis_type']) && $data[$id]['dnis_type'] === 'Geographic') {
                        $data[$id]['dnis_type'] = 'Landline';
                    }
                    break;

                case 'Dial-In-Numbers':
                    $germanCheck = ($row['country_code'] == 'DEU' && $row['is_a_default_mobile_number'] == 1) ? '(' . 'mobile number' . ')' : '';
                    $mobileCheck = ($row['mobile'] == 0) ? ' <a href="#postcomment"><sup>1</sup></a>' : '';
                    $internCheck = ($row['dial_international'] == 0 && $row['international_formatted'] == '') ? ' <a href="#postcomment"><sup>2</sup></a>' : '';
                    $natioaCheck = (strlen(
                            $row['national_formatted']
                        ) == 5) ? ' <a href="#postcomment"><sup>2</sup></a> <a href="#postcomment"><sup>3</sup></a>' : '';
                    $oneparCheck = ($row['one_participant_limit'] == 'Y') ? ' <a href="#postcomment"><sup>4</sup></a>' : '';

                    // German Variable Rate Check
                    if ($row['country_code'] == 'DEU' && $row['is_a_default_mobile_number'] == 1) {
                        $cpmCheck = 'Variable';
                    } else {
                        // Change Requested by Sarah @ 12-03-2013
                        if ($row['iso2'] == 'SE' && $row['currency_name_en'] == 'Swedish krona') {
                            $cpm = $row['cost_per_minute_value'] * 1;
                        } elseif ($row['iso2'] == 'PL' && $row['currency_name_en'] == 'Polish zloty') {
                            $cpm = $row['cost_per_minute_value'] * 1;
                        } else {
                            $cpm = number_format(($row['cost_per_minute_value'] * 100), 1);
                        }

                        if ($cpm == round($cpm)) {
                            $cpm = round($cpm);
                        }
                        $cpmCheck = $cpm . ' ' . $row['currency_name_translated'];
                    }

                    // DNIS Type Check
                    // Change Requested by Sarah / Maria @ 16-04-2012
                    if ($row['dnis_type'] == 'Freefone') {
                        $row['dnis_type'] = 'Freephone';
                    }

                    // Change Requested by Sarah @ 25-07-2012
                    if ($row['dnis_type'] == 'Geographic' && sfContext::getInstance()->getUser()->getAttribute(
                            'service'
                        ) == 'plus'
                    ) {
                        $row['dnis_type'] = 'Landline';
                    }

                    $decicated = '';
                    if ($row['dedicated_dnis'] == 1) {
                        $decicated = '<br /> (Dedicated number with branding)';
                    }

                    $data[$id] = array(
                        'country_name_language'   => $row['country_name'] . ' (' . $row['lang'] . ')' . $germanCheck . $mobileCheck . $internCheck . $natioaCheck . $oneparCheck . $decicated,
                        'national_formatted'      => $row['national_formatted'],
                        'international_formatted' => $row['international_formatted'],
                        'cpm_check'               => $cpmCheck,
                        'dnis_type'               => $row['dnis_type']
                    );
                    break;

                default:
                    // Do Nothing
            }
        }
        return $data;
    }

    /**
     * Formats a duration in seconds to a format of "hours:minutes:seconds".
     *
     * All parts of the format (h, m and s) are zerofilled to 2 characters.
     * Examples:
     *      formatCallHistoryDuration(7856) -> "02:10:56"
     *      formatCallHistoryDuration(3600) -> "01:00:00"
     *
     * @param float $duration
     * @return string
     * @author Maarten Jacobs
     */
    private static function formatCallHistoryDuration($duration)
    {
        $duration = (int)$duration;
        // Note that the hours are maxed to two digits.
        // It is highly unlikely that a conference call took longer than a few hours anyway.
        return sprintf("%02d:%02d:%02d", $duration / 3600, $duration % 3600 / 60, $duration % 60);
    }

    /**
     * Retrieve Recordings for a User (Cached)
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getRecordingsByContactRef(Array $args)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');

        try {
            $result = hermesCallWithCaching(
                'getRecordingsByContactRef',
                array(
                    'contact_ref'    => $args['contact_ref'],
                    'show_all_users' => (isset($args['show_all_users'])) ? $args['show_all_users'] : false
                ),
                'array',
                360
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getRecordingsByContactRef Failed to Return a Result for Args: ' . serialize(
                    $args
                ) . ', Error: ' . serialize($e)
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Retrieve published recording status
     *
     * @param array $args
     * @return array
     *
     * @author Albert Kozlowski
     */
    public static function getPublishedRecordingStatus(array $args)
    {
        try {
            $result = Hermes_Client_Rest::Call(
                'getPublishedRecordingStatus',
                array(
                    'recording_ref' => $args['recording_ref'],
                )
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getPublishedRecordingStatus Failed to Return a Result for Args: ' . serialize(
                    $args
                ) . ', Error: ' . (isset($e->responseBody)) ? serialize($e->responseBody) : serialize($e->getMessage())
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Retrieve published recording data
     *
     * @param array $args
     * @return array
     *
     * @author Albert Kozlowski
     */
    public static function getPublishedRecording(array $args)
    {
        try {
            $result = Hermes_Client_Rest::Call(
                'getPublishedRecording',
                array(
                    'recording_ref' => $args['recording_ref'],
                )
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getPublishedRecording Failed to Return a Result for Args: ' . serialize(
                    $args
                ) . ', Error: ' . (isset($e->responseBody)) ? serialize($e->responseBody) : serialize($e->getMessage())
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Retrieve Recordings for a User (Not Cached) (Cached version was causing crazy exceptions being called for unknown reasons)
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getRecording(Array $args)
    {
        try {
            $result = Hermes_Client_Rest::Call(
                'getRecording',
                array(
                    'contact_ref'   => $args['contact_ref'],
                    'recording_ref' => $args['recording_ref'],
                )
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getRecording Failed to Return a Result for Args: ' . serialize(
                    $args
                ) . ', Error: ' . (isset($e->responseBody)) ? serialize($e->responseBody) : serialize($e->getMessage())
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Update Recording
     *
     * @param  array $args
     * @return array('result' => $result, 'error' => $errorMessages);
     * @author Asfer Tamimi
     *
     */
    public static function updateRecording(array $args)
    {
        $errorMessages = array();
        $result        = array();
        $contact_ref   = $args['contact_ref'];
        unset($args['contact_ref']);

        try {
            $result = Hermes_Client_Rest::Call('updateRecording', $args);
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Contact Ref: ' . $contact_ref . ', tried to add a PIN Pair to their Account, but an Exception was called. Args: '
                . serialize($args) . ', Error: ' . serialize($e->getResponseBody())
            );

            switch ($e->getCode()) {
                case '001:003:001':
                    $errorMessages = array('message' => 'FORM_RESPONSE_RECORDINGS_NOT_FOUND', 'field_name' => 'alert');
                    break;
                case '000:003:001':
                    $errorMessages = array(
                        'message'    => 'FORM_RESPONSE_RECORDINGS_INVALID_VALUE',
                        'field_name' => 'alert'
                    );
                    break;
                default:
                    $errorMessages = array('message' => 'FORM_UNKNOWN_ERROR', 'field_name' => 'alert');
                    break;
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Contact Ref: ' . $contact_ref . ', tried to add a PIN Pair to their Account, but an Exception was called. Args: '
                . serialize($args) . ', Error: ' . serialize($e->getMessage())
            );
        }

        if (count($errorMessages) == 0) {
            try {
                PWN_Cache_Clearer::modifiedRecording(array('contact_ref' => $contact_ref, 'show_all_users' => true));
                PWN_Cache_Clearer::modifiedRecording(array('contact_ref' => $contact_ref, 'show_all_users' => false));
                sfContext::getInstance()->getLogger()->info(
                    'Cache Cleared for modifiedRecording for Contact Ref: ' . $contact_ref
                );
            } catch (Exception $e) {
                sfContext::getInstance()->getLogger()->err(
                    'An exception occurred calling PWN_Cache_Clearer::modifiedRecording for Contact Ref: '
                    . $contact_ref . ', Error: ' . serialize($e)
                );
            }
        }

        return array('result' => $result, 'error' => $errorMessages);
    }

    /**
     * Get Conference Ids for a Specific User (Non Cached While in Development)
     *
     * @param integer $contact_ref
     * @return array  $result
     * @author Asfer Tamimi
     *
     */
    public static function getConferenceIds($contact_ref)
    {
        try {
            $result = Hermes_Client_Rest::Call('getConferenceIds', array('contact_ref' => $contact_ref));
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getConferenceIds Failed to Return a Result for Contact Ref: ' . $contact_ref . ', Error: ' . (isset($e->responseBody)) ? serialize(
                    $e->responseBody
                ) : serialize($e->getMessage())
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Get Call History Details (Non Cached While in Development)
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getCallHistory(Array $args)
    {
        $errorMessages = array();
        $result        = array();

        try {
            $result = Hermes_Client_Rest::Call('CallHistory.getCallHistory', $args);
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'An Exception occured trying to get Call History Details. Args: ' . serialize($args)
                . ', Error: ' . serialize($e->getResponseBody())
            );

            switch ($e->getCode()) {
                case '000:002:002':
                    $errorMessages = array('message' => 'ARG_NAME_NOT_SUPPLIED', 'field_name' => 'alert');
                    break;
                case '001:000:023':
                    $errorMessages = array('message' => 'DATE_TOO_OLD', 'field_name' => 'callhistory[from_date]');
                    break;
                case '001:002:001':
                    $errorMessages = array(
                        'message'    => 'CONTACT_REF_OR_SERVICE_REF_NOT_FOUND',
                        'field_name' => 'callhistory[service]'
                    );
                    break;
                default:
                    $errorMessages = array('message' => 'FORM_UNKNOWN_ERROR', 'field_name' => 'alert');
                    break;
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'An Exception occured trying to get Call History Details. Args: ' . serialize($args) . ', Error: '
                . serialize($e->getMessage())
            );
        }

        return array('result' => $result, 'error' => $errorMessages);
    }

    /**
     * Get Service Refs (Non Cached While in Development)
     *
     * @param  array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getServiceRefs($args)
    {
        try {
            $result = Hermes_Client_Rest::Call('getServiceRefs', $args);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getServiceRefs Failed to Return a Result for Args: ' . serialize($args) . ', Error: '
                . (isset($e->responseBody)) ? serialize($e->responseBody) : serialize($e->getMessage())
            );
            $result = array();
        }

        return $result;
    }

    /**
     * Get Regional Links (Cached)
     *
     * @param string $locale
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getRegionalLinks($locale)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array();

        try {
            $result = hermesCallWithCaching('getWebsites', array('locale' => $locale), 'array', 86400);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getWebsites Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getWebsites Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "getWebsites Failed to Return a Result for Locale :: $locale, Error Message: " . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
        return $result;
    }

    /**
     * Retrieves Arguments from the combined $postDetails and $currentDetails
     *
     * @param array $postDetails
     * @param array $currentDetails
     * @return array
     * @author Asfer Tamimi
     *
     */
    public static function getArguments(array $postDetails, array $currentDetails)
    {
        if ((count($postDetails) + count($currentDetails)) == (count(array_merge($postDetails, $currentDetails)))) {
            return array_merge($postDetails, $currentDetails);
        } else {
            $newCurrentDetails = array();
            foreach ($currentDetails as $k => $v) {
                $newCurrentDetails['current_' . $k] = $v;
            }
            if ((count($postDetails) + count($newCurrentDetails)) == (count(
                    array_merge($postDetails, $newCurrentDetails)
                ))
            ) {
                return array_merge($postDetails, $newCurrentDetails);
            } else {
                sfContext::getInstance()->getLogger()->err('Error Occurred trying to Retrieve Arguments');
                return array();
            }
        }
    }

    /**
     * Send Contact Us Email (Taken from TS::PWN_App.php)
     *
     * @param  array $args - Contact Us Form Arguments
     * @param  string $domain - Domain / Server Name ($_SERVER['SERVER_NAME'])
     * @param  string $locale - Locale (Default = en_GB)
     * @param  integer $service_ref - Service Ref (Default = 801)
     * @return array
     * @author Unknown + Asfer Tamimi
     *
     */
    public static function sendContactUsEmail($args, $domain, $locale = 'en_GB', $service_ref = 801)
    {
        try {
            $result = Hermes_Client_Rest::call(
                'sendContactUsEmail',
                array(
                    'first_name'  => $args['first_name'],
                    'last_name'   => $args['last_name'],
                    'email'       => $args['email'],
                    'phone'       => $args['contact_number'],
                    'message'     => 'Company: ' . $args['company'] . PHP_EOL . 'Message: ' . $args['comments'],
                    'domain'      => $domain,
                    'locale'      => $locale,
                    'service_ref' => $service_ref

                )
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('sendContactUsEmail Hermes call has timed out.');
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'sendContactUsEmail Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'sendContactUsEmail Failed. Args: ' . serialize(
                    $args
                ) . ', Domain: ' . $domain . ', Locale: ' . $locale . ', Service Ref: ' . $service_ref . ', Error Message: ' . serialize(
                    $e->getMessage()
                ) . ', Error Code: ' . $e->getCode()
            );
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        }

        return $result;
    }

    /**
     * Send Contact Us Email Alternative (Taken from TS::PWN_App.php)
     *
     * @param  array $args - Contact Us Alt Form Arguments
     * @param  string $domain - Domain / Server Name ($_SERVER['SERVER_NAME'])
     * @param  string $locale - Locale (Default = en_GB)
     * @param  integer $service_ref - Service Ref (Default = 801)
     * @return boolean
     * @author Alexander Farrow
     *
     */
    public static function sendImInterestedEmail($args, $domain, $locale = 'en_GB', $service_ref = 801)
    {
        $mailto = "iminterested@powwownow.com";
        $subject = "I'm Interested Response";

        $emailBody = "Name: ".$args['first_name']." ".$args['last_name']."\n";
        $emailBody .= "Email: ".$args['email']."\n";
        $emailBody .= "Phone: ".$args['contact_number']."\n";
        $emailBody .= "Domain: ".$domain."\n";
        $emailBody .= "Locale: ".$locale."\n";
        $emailBody .= "Service Ref: ".$service_ref."\n";
        $emailBody .= "Campaign: ".$args['utm_campaign']."\n\n";
        $emailBody .= $args['comments'];

        $result = mail($mailto, $subject, $emailBody);

        return $result;
    }


    /**
     * Create or Update Account (Taken from TS::PWN_App.php)
     *
     * @param  array $args - Create A Login Arguments + Additional Registration Arguments
     * @return array
     * @author Unknown + Asfer Tamimi
     *
     */
    public static function doCreateOrUpdateAccount($args)
    {
        $errorMessages = array();

        // Check Required Arguments
        if (!isset($args['email'], $args['first_name'], $args['last_name'], $args['password'], $args['locale'])) {
            sfContext::getInstance()->getLogger()->err(
                'Required Arguments not given for doCreateOrUpdateAccount. Args: ' . serialize($args)
            );
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        }

        // Check Browser and Operating System
        if (!isset($args['browser'], $args['os'])) {
            $bd = sfContext::getInstance()->getUser()->getAttribute('browser_detection', array());
            if (count($bd) == 0) {
                $args['browser'] = '';
                $args['os']      = '';
            } else {
                $bd = $bd['data'];

                if (!isset($args['browser'])) {
                    $args['browser'] = $bd['browser_name'] . ' ' . $bd['browser_number'];
                }

                if (!isset($args['os'])) {
                    $args['os'] = $bd['os'] . ' ' . $bd['os_number'];
                }
            }
        }

        if (!isset($args['ip'])) {
            if (isset($_SERVER['REMOTE_ADDR'])) {
                $args['ip'] = $_SERVER['REMOTE_ADDR'];
            } else {
                $args['ip'] = '127.0.0.1';
            }
        }

        try {
            $result = Hermes_Client_Rest::call(
                'doCreateOrUpdateAccount',
                array(
                    'email'                => $args['email'],
                    'first_name'           => $args['first_name'],
                    'last_name'            => $args['last_name'],
                    'password'             => $args['password'],
                    'source'               => $args['source'],
                    'locale'               => $args['locale'],
                    'registration_ip'      => $args['ip'],
                    'registration_browser' => $args['browser'],
                    'registration_os'      => $args['os']
                )
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('doCreateOrUpdateAccount Hermes call has timed out.');
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doCreateOrUpdateAccount Failed. Args: ' . serialize($args) . ', Error Message: '
                . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode()
            );

            if ($e->getResponseBody()) {
                switch ($e->getCode()) {
                    case '001:000:001':
                        $errorMessages = array(
                            'message'    => 'API_RESPONSE_EMAIL_TAKEN_AND_NON_STANDARD_PWN',
                            'field_name' => 'email'
                        );
                        break;
                    case '001:000:005':
                        $errorMessages = array(
                            'message'    => 'API_RESPONSE_ACCOUNT_ALREADY_CREATED_FOR_REQUESTED_DATA',
                            'field_name' => 'email'
                        );
                        break;
                    case '001:000:007':
                        $errorMessages = array(
                            'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE',
                            'field_name' => 'email'
                        );
                        break;
                    default:
                        $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                        break;
                }
            } else {
                $errorMessages = array('message' => 'FORM_UNKNOWN_ERROR', 'field_name' => 'alert');
            }
            return array('error' => $errorMessages);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doCreateOrUpdateAccount Hermes call is faulty. Error message: ' . $e->getMessage()
                . ', Error Code: ' . $e->getCode()
            );
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        }

        return $result;
    }

    /**
     * Obtain the Contact Dial In Numbers for a Specific Contact Ref
     *
     * @param  integer $contact_ref
     * @param  string $language
     * @return array   $dialInNumbers|array('error' => 'FORM_COMMUNICATION_ERROR')
     * @author Asfer Tamimi
     *
     */
    public static function getContactDialInNumbers($contact_ref, $language = 'en')
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $dialInNumbers = array();

        try {
            $dialInNumbers = hermesCallWithCaching(
                'getContactDialInNumbers',
                array(
                    'contact_ref' => $contact_ref,
                    'language'    => $language
                ),
                'array',
                3600
            );
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('getContactDialInNumbers Hermes call has timed out.');
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getContactDialInNumbers Hermes call is faulty. Error message: ' . $e->getMessage()
                . ', Error Code: ' . $e->getCode()
            );
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'getContactDialInNumbers Failed. Contact_ref: ' . $contact_ref . ', Language: ' . $language
                . ', Error Message: ' . serialize($e->getMessage()) . ', Error Code: ' . $e->getCode()
            );
            return array('error' => 'FORM_COMMUNICATION_ERROR');
        }

        return $dialInNumbers;
    }

    /**
     * Obtain the Ledger Operation types
     *
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getLedgerOperationTypes()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array();

        try {
            $result = hermesCallWithCaching('Ledger.getLedgerOperationTypes', array(), 'array', 7200);
            return $result;
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('Ledger.getLedgerOperationTypes Hermes call has timed out.');
            return $result;
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Ledger.getLedgerOperationTypes Hermes call is faulty. Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            return $result;
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Ledger.getLedgerOperationTypes Failed. Error Message: ' . serialize(
                    $e->getMessage()
                ) . ', Error Code: ' . $e->getCode()
            );
            return $result;
        }
    }

    /**
     * Performs basic registration
     *
     * Is used by executeBasicRegistration*Ajax
     *
     * Echo's json error message on validation failure or error, else returns array of created details
     *
     * Responses are:
     *   {"error_messages":[{"message":"FORM_VALIDATION_INVALID_EMAIL_ADDRESS","field_name":"email"}]}
     *   {"error_messages":[{"message":"API_RESPONSE_EMAIL_ALREADY_TAKEN","field_name":"email"}]}
     *   {"error_messages":[{"message":"API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE","field_name":"email"}]}
     *   {"error_messages":[{"message":"FORM_COMMUNICATION_ERROR","field_name":"email"}]}
     *
     * @author Wiseman
     *
     * @param string $email - Email address to be registered
     * @param string $registrationType - generatePassword | null The type of registration to perform
     * @return array | sfView::NONE
     */
    public static function performRegistration($email, $registrationType = null, $tick = true)
    {
        sfContext::getInstance()->getResponse()->setContentType('application/json');

        // Error Messages
        $errorMessages = array();

        // Perform Email Validation
        try {
            $v     = new sfValidatorEmail();
            $email = $v->clean($email);
        } catch (sfValidatorError $e) {
            echo json_encode(
                array(
                    'error_messages' => array(
                        'message'    => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS',
                        'field_name' => 'email'
                    )
                )
            );
            return sfView::NONE;
        }

        // Perform Tick Validation
        if (!$tick) {
            echo json_encode(
                array('error_messages' => array('message' => 'FORM_VALIDATION_T_AND_C', 'field_name' => 'agree_tick'))
            );
            return sfView::NONE;
        }

        // Get page variation from session, we send this to hermes upon registration, default to 'Home'
        $variation = sfContext::getInstance()->getUser()->getAttribute('homePageCroVariation', 'Home', 'cro');

        try {
            // Performs relevant registration based on $registrationType
            switch ($registrationType) {
                case 'generatePassword':
                    $apiResponse = Hermes_Client_Rest::call(
                        'Default.doFullRegistrationGenerateRandomPassword',
                        array(
                            'email'  => $email,
                            'locale' => 'en_GB',
                            'source' => '/PWN-' . $variation . '-Reg'
                        )
                    );

                    $returnArr = array(
                        'pin'         => $apiResponse['pin']['pin'],
                        'pin_ref'     => $apiResponse['pin']['pin_ref'],
                        'contact_ref' => $apiResponse['contact']['contact_ref'],
                        'password'    => $apiResponse['password'],
                    );
                    break;
                default:
                    $apiResponse = Hermes_Client_Rest::call(
                        'doBasicRegistration',
                        array(
                            'email'  => $email,
                            'locale' => 'en_GB',
                            'source' => '/PWN-' . $variation . '-Reg'
                        )
                    );
                    $returnArr   = array(
                        'pin'         => $apiResponse['pin']['pin'],
                        'pin_ref'     => $apiResponse['pin']['pin_ref'],
                        'contact_ref' => $apiResponse['contact']['contact_ref'],
                    );
                    break;
            }

        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "doBasicRegistration Hermes call has timed out. Email: '$email'"
            );
            echo json_encode(
                array(
                    'error_messages' => array(
                        array(
                            'message'    => 'FORM_COMMUNICATION_ERROR',
                            'field_name' => 'alert'
                        )
                    )
                )
            );
            return sfView::NONE;
        } catch (Hermes_Client_Exception $e) {
            switch ($e->getCode()) {
                case '001:000:001':
                    try {
                        $pinResponse = Hermes_Client_Rest::call('getPins', array('email' => $email));

                        if (count($pinResponse) == 1) {
                            $errorMessages[] = array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN',
                                'field_name' => 'email'
                            );
                        } else {
                            $errorMessages[] = array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_OTHER',
                                'field_name' => 'email'
                            );
                        }
                    } catch (Hermes_Client_Request_Timeout_Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Hermes call has timed out. Email: ' . $email
                        );
                        $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    } catch (Hermes_Client_Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Hermes Client Exception. ErrorCode: ' . $e->getCode(
                            ) . ', Message: ' . $e->getMessage()
                        );
                        $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    } catch (Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Unknown Error Occurred: ' . $e->getMessage()
                        );
                        $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    }
                    break;
                case '001:000:007':
                    $errorMessages[] = array(
                        'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE',
                        'field_name' => 'alert'
                    );
                    break;
                default:
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }
            echo json_encode(array('error_messages' => $errorMessages));
            return sfView::NONE;
        }

        try {
            if (!empty($apiResponse['pin']['pin_ref'])) {
                $goal = 'conferencing';
                try {
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                } catch (Exception $e) {

                }
                PWN_Logger::log(
                    'pin:' . $apiResponse['pin']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                    PWN_Logger::INFO,
                    'trackerLog'
                );
                PWN_Tracking_GoalReferrers::achieved('conferencing', $apiResponse['pin']['pin_ref'], true);
            } else {
                sfContext::getInstance()->getLogger()->err(
                    'After a registration, pin ref was null: ' . serialize($apiResponse)
                );
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Could not track goal referrers: ' . serialize($e));
        }

        if (isset($returnArr)) {
            return $returnArr;
        } else {
            return sfView::NONE;
        }
    }

    /**
     * Performs FREE PIN Registration
     *
     * It is used by the Unified FREE PIN Registration Ajax
     *
     * @param string $email - Email Address to be Registered
     * @param bool $tick - Does the Form have a Terms and Conditions Tick Box
     * @param string $trafficSource - Registration Source
     * @param string $locale
     * @return array - An Empty Array or an Error Message
     * @author Asfer Tamimi
     */
    public static function performFreePinRegistration(
        $email,
        $tick = true,
        $trafficSource = '/PWN-E-Reg',
        $locale = 'en_GB'
    ) {

        $errorMessages = array();

        // Perform Email Validation
        try {
            $v     = new sfValidatorEmail();
            $email = $v->clean($email);
        } catch (sfValidatorError $e) {
            return array(
                'error_messages' => array(
                    'message'    => 'FORM_VALIDATION_INVALID_EMAIL_ADDRESS',
                    'field_name' => 'email'
                )
            );
        }

        // Perform Tick Validation
        if (!$tick) {
            return array(
                'error_messages' => array(
                    'message'    => 'FORM_VALIDATION_T_AND_C',
                    'field_name' => 'agree_tick'
                )
            );
        }

        try {
            $apiResponse = Hermes_Client_Rest::call(
                'doBasicRegistration',
                array(
                    'email'  => $email,
                    'locale' => $locale,
                    'source' => $trafficSource
                )
            );
            $returnArr   = array(
                'pin'         => $apiResponse['pin']['pin'],
                'pin_ref'     => $apiResponse['pin']['pin_ref'],
                'contact_ref' => $apiResponse['contact']['contact_ref'],
            );

        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "doBasicRegistration Hermes call has timed out. Email: '$email'"
            );
            return array(
                'error_messages' => array(
                    array(
                        'message'    => 'FORM_COMMUNICATION_ERROR',
                        'field_name' => 'alert'
                    )
                )
            );
        } catch (Hermes_Client_Exception $e) {
            switch ($e->getCode()) {
                case '001:000:001':
                    try {
                        $pinResponse = Hermes_Client_Rest::call('getPins', array('email' => $email));

                        if (count($pinResponse) == 1) {
                            $errorMessages = array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN',
                                'field_name' => 'email'
                            );
                            sfContext::getInstance()->getLogger()->err(
                                "getPins Hermes Email Already Taken. Email: '$email' Error Message: " . $e->getMessage()
                            );
                        } else {
                            $errorMessages = array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_OTHER',
                                'field_name' => 'email'
                            );
                            sfContext::getInstance()->getLogger()->err(
                                "getPins Hermes Email Already Taken Other. Email: '$email' Error Message: " . $e->getMessage(
                                )
                            );
                        }
                    } catch (Hermes_Client_Request_Timeout_Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Hermes call has timed out. Email: ' . $email
                        );
                        $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    } catch (Hermes_Client_Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Hermes Client Exception. ErrorCode: ' . $e->getCode(
                            ) . ', Message: ' . $e->getMessage()
                        );
                        $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    } catch (Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Unknown Error Occurred: ' . $e->getMessage()
                        );
                        $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    }
                    break;
                case '001:000:007':
                    $errorMessages = array(
                        'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE',
                        'field_name' => 'alert'
                    );
                    sfContext::getInstance()->getLogger()->err(
                        'doBasicRegistration Email Already Exists, but it is Inactive. Error Message: ' . $e->getMessage(
                        )
                    );
                    break;
                default:
                    $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    $tr            = $e->getTrace();
                    $tr            = array_slice($tr, 0, -11);
                    sfContext::getInstance()->getLogger()->err(
                        'doBasicRegistration Unknown Error Occurred. Email: ' . $email . ',  Error Message: ' . $e->getMessage(
                        ) . ", Error code: " . $e->getCode() . ", trace:\n" . var_export($tr, true)
                    );
                    break;
            }

            return array('error_messages' => $errorMessages);
        }

        try {
            if (!empty($apiResponse['pin']['pin_ref'])) {
                $goal = 'conferencing';
                try {
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                } catch (Exception $e) {

                }
                PWN_Logger::log(
                    'pin:' . $apiResponse['pin']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                    PWN_Logger::INFO,
                    'trackerLog'
                );
                PWN_Tracking_GoalReferrers::achieved('conferencing', $apiResponse['pin']['pin_ref'], true);
            } else {
                sfContext::getInstance()->getLogger()->err(
                    'After a registration, pin ref was null: ' . serialize($apiResponse)
                );
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Could not track goal referrers for PIN ref: ' . $apiResponse['pin']['pin_ref'] . ', error: ' . $e->getMessage(
                )
            );
        }

        if (isset($returnArr)) {
            return $returnArr;
        } else {
            return array();
        }
    }

    /**
     * Obtain the Product Selector - Get Bundles
     *
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getBundlesForProductSelector()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('hermesCallWithCaching');
        $result = array();

        try {
            $result = hermesCallWithCaching('ProductSelector.getBundles', array(), 'array', 3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('ProductSelector.getBundles Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.getBundles Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.getBundles Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }

        // Change the order of the bundles, so that AYCM is always the first bundle.
        $result['bundles'] = array(
            'AYCM' => $result['bundles']['AYCM'],
            1000   => $result['bundles'][1000],
            2500   => $result['bundles'][2500],
            5000   => $result['bundles'][5000],
        );

        return $result;
    }

    /**
     * Store the Product Selector Information
     *
     * @param array $args - The Arguments to be stored in the database table
     * @return bool
     * @author Asfer Tamimi
     *
     */
    public static function storeProductSelector($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('ProductSelector.storeProductSelectorTracking', $args, 'array', 3600);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.storeProductSelectorTracking Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.storeProductSelectorTracking Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.storeProductSelectorTracking Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
        return $result;
    }

    /**
     * Update the Product Selector Information
     *
     * @param array $args - The Arguments to be updated in the database table
     * @return bool
     * @author Asfer Tamimi
     *
     */
    public static function updateProductSelector($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('ProductSelector.updateProductSelectorTracking', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.updateProductSelectorTracking Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.updateProductSelectorTracking Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'ProductSelector.updateProductSelectorTracking Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
        return $result;
    }

    /**
     * Confusingly, we use this on the forgot password page rather than the reset password page
     * This Initiates a Password Reset Process
     *
     * @param array $args - The $email, and Locale to be used for the Forgotten Password
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function doPasswordReset($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('doPasswordReset', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('doPasswordReset Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doPasswordReset Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
            switch ($e->getCode()) {
                case '001:000:000':
                    $errorMessage = array('message' => 'API_RESPONSE_NO_ACCOUNT_FOR_EMAIL', 'field_name' => 'email');
                    break;
                case '001:000:008':
                    $errorMessage = array('message' => 'API_RESPONSE_PASSWORD_NOT_SET', 'field_name' => 'email');
                    break;
                default:
                    $errorMessage = array('message' => 'FORGOTTEN_PASSWORD_UNKNOWN_ERROR', 'field_name' => 'email');
            }

            $result = array('error_messages' => $errorMessage);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doPasswordReset Exception Called. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode(
                )
            );
        }
        return $result;
    }

    /**
     *
     * @param array $args - The $contact_ref, $token to be used for the Forgotten Password
     * @return array $result
     * @author Alexander Farrow
     *
     */
    public static function checkPasswordResetToken($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('checkPasswordResetToken', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err('checkPasswordResetToken Hermes call has timed out.');
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'checkPasswordResetToken Hermes call is faulty, Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doPasswordReset Exception Called. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );
        }
        return (isset($result['statusCode']) && $result['statusCode'] === 100);
    }

    public static function isValidEmail($email)
    {
        // Type and length validation.
        if (!is_string($email) || strlen($email) > 50) {
            return false;
        }

        // Symfony email validation.
        $validationGenerator = new validationGroupFactory();
        $validator           = $validationGenerator->email();
        try {
            $validator->clean($email);
        } catch (sfValidatorError $e) {
            return false;
        }
        return true;
    }

    /**
     * Send an Email to a Friend. Triggered from the Tell-A-Friend Page
     *
     * @param array $args - The Arguments to be stored in the database table
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function sendTellAFriendEmail($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('sendTellAFriendEmail', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'sendTellAFriendEmail Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'sendTellAFriendEmail Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'sendTellAFriendEmail Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
        return $result;
    }

    public static function doOptOutContact($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('doOptOutContact', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doOptOutContact Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doOptOutContact Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );

            switch ($e->getCode()) {
                case '001:000:000':
                    $result['error_messages'] = array('message' => 'FORM_VALIDATION_EMAIL_NOT_FOUND', 'field_name' => 'unsubscribe[email]');
                    break;
                default:
                    $result['error_messages'] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doOptOutContact Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
        return $result;
    }

    /**
     * Deactivate Account
     *
     * @param array $args - Array of email and pin
     * @return array $result
     * @author Alexander Farrow
     *
     */
    public static function doDeactivateAccount($args)
    {
        $result = array();

        try {
            return Hermes_Client_Rest::call('doDeactivateAccount',$args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doDeactivateAccount Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doDeactivateAccount Hermes call is faulty, Error message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode()
            );

            switch ($e->getCode()) {
                case '001:000:004':
                    $errorMessages = array('message' => 'PIN_AND_CONTACT_DO_NOT_MATCH', 'field_name' => 'deletepin[pin]');
                    break;
                case '001:000:009':
                    $errorMessages = array('message' => 'CANNOT_DEACTIVATE_ACCOUNT_USING_PARTICIPANT_PIN', 'field_name' => 'deletepin[pin]');
                    break;
                case '001:000:010':
                    $errorMessages = array('message' => 'PIN_ALREADY_INACTIVE', 'field_name' => 'deletepin[pin]');
                    break;
                default:
                    $errorMessages = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }
            $result = array('error_messages' => $errorMessages);
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'doDeactivateAccount Exception Called. Error Message: ' . $e->getMessage() . ', Error Code: ' . $e->getCode(
                )
            );
        }
        return($result);
    }

    /**
     * Performs United Free Registration (with password registration) and performs user login in)
     *
     * @author Robert
     *
     * @param string $email - Email address to be registered
     * @param string $source - Email address to be registered
     *
     * @return array | @throws Exception
     */
    public static function performUnitedRegistration($email, $source)
    {

        // Error Messages
        $errorMessages = array();

        try {
            $apiResponse = Hermes_Client_Rest::call(
                  'Default.doFullRegistrationGenerateRandomPassword',
                  array(
                    'email'  => $email,
                    'locale' => 'en_GB',
                    'source' => $source
                   )
            );

            $returnArr = array(
                'pin'         => $apiResponse['pin']['pin'],
                'pin_ref'     => $apiResponse['pin']['pin_ref'],
                'contact_ref' => $apiResponse['contact']['contact_ref'],
                'password'    => $apiResponse['password'],
            );
            
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                "doFullRegistrationGenerateRandomPassword Hermes call has timed out. Email: '$email'"
            );
            return array(
                    'error_messages' => array(
                        array(
                            'message'    => 'FORM_COMMUNICATION_ERROR',
                            'field_name' => 'alert'
                        )
                    )
                );
            
        } catch (Hermes_Client_Exception $e) {
            switch ($e->getCode()) {
                case '001:000:001':
                    try {
                        $pinResponse = Hermes_Client_Rest::call('getPins', array('email' => $email));

                        if (count($pinResponse) == 1) {
                            $errorMessages[] = array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN',
                                'field_name' => 'email'
                            );
                        } else {
                            $errorMessages[] = array(
                                'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_OTHER',
                                'field_name' => 'email'
                            );
                        }
                    } catch (Hermes_Client_Request_Timeout_Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Hermes call has timed out. Email: ' . $email
                        );
                        $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    } catch (Hermes_Client_Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Hermes Client Exception. ErrorCode: ' . $e->getCode(
                            ) . ', Message: ' . $e->getMessage()
                        );
                        $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    } catch (Exception $e) {
                        sfContext::getInstance()->getLogger()->err(
                            'getPins Unknown Error Occurred: ' . $e->getMessage()
                        );
                        $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    }
                    break;
                case '001:000:007':
                    $errorMessages[] = array(
                        'message'    => 'API_RESPONSE_EMAIL_ALREADY_TAKEN_BUT_INACTIVE',
                        'field_name' => 'alert'
                    );
                    break;
                default:
                    $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR', 'field_name' => 'alert');
                    break;
            }
            return array('error_messages' => $errorMessages);
            
        }

        try {
            if (!empty($apiResponse['pin']['pin_ref'])) {
                $goal = 'conferencing';
                try {
                    PWN_Logger::initLogFile('/var/log/hermes/tracker.log', 'trackerLog');
                } catch (Exception $e) {

                }
                PWN_Logger::log(
                    'pin:' . $apiResponse['pin']['pin_ref'] . ' goal: ' . $goal . ' ' . __FILE__ . '[' . __LINE__ . ']',
                    PWN_Logger::INFO,
                    'trackerLog'
                );
                PWN_Tracking_GoalReferrers::achieved('conferencing', $apiResponse['pin']['pin_ref'], true);
            } else {
                sfContext::getInstance()->getLogger()->err(
                    'After a registration, pin ref was null: ' . serialize($apiResponse)
                );
            }
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err('Could not track goal referrers: ' . serialize($e));
        }

        if (isset($returnArr)) {
            return $returnArr;
        } else {
            //return sfView::NONE;
        }
    }

    /**
     * Get Top-up History for Plus Account
     *
     * @param array $args
     * @return array $result
     * @author Asfer Tamimi
     *
     */
    public static function getTopupHistoryForAccount($args)
    {
        $result = array();
        try {
            $result = Hermes_Client_Rest::call('Plus.getTopupHistoryForAccount', $args);
        } catch (Hermes_Client_Request_Timeout_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Plus.getTopupHistoryForAccount Hermes call has timed out.'
            );
        } catch (Hermes_Client_Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Plus.getTopupHistoryForAccount Hermes call is faulty, Error message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        } catch (Exception $e) {
            sfContext::getInstance()->getLogger()->err(
                'Plus.getTopupHistoryForAccount Exception Called. Error Message: ' . $e->getMessage(
                ) . ', Error Code: ' . $e->getCode()
            );
        }
        return $result;
    }
    
}
