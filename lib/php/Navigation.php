<?php

/**
 * Class for getting navigation links and filtering out links depending on if a user has products, an admin has users etc
 *
 * @todo This was moved from commonComponents, but there is still a lot of refactoring we can do:
 *  * Links are currently only removed only by certain position on the menu (ie leftMenu) and certain user type
 *  * Makes options like disable_for_postpay support both true and false
 *
 * @author Wiseman Maarten Asfer
 */
class Navigation
{
    /**
     * Gets navigation links for the specified user
     *
     * @param myUser $user
     * @return mixed
     */
    public function getNavigationLinks(myUser $user)
    {
        $navigationLinks = sfConfig::get('app_navigation_links');

        if ($user->hasCredential('PLUS_ADMIN') && plusCommon::retrievePostPayStatus(
                $user->getAttribute('account_id')
            )
        ) {
            $navigationLinks = $this->alterPostPayPlusAdminLinks($navigationLinks);
        }

        if ($user->hasCredential('PLUS_USER') && !ProductHelper::contactHasBundleAssignedToAPin(
                $user->getAttribute('contact_ref'),
                $user->getAttribute('account_id')
            )
        ) {
            $navigationLinks = $this->alterBundleAssignedLinks($navigationLinks);
        }

        return $navigationLinks;
    }

    private function alterPostPayPlusAdminLinks($links)
    {
        $plusAdminLinks = $links['PLUS_ADMIN']['leftMenu'];
        foreach ($plusAdminLinks as $linkKey => $linkGroup) {
            if (!empty($linkGroup['submenu'])) {
                $plusAdminLinks[$linkKey]['submenu'] = array_filter(
                    $plusAdminLinks[$linkKey]['submenu'],
                    array($this, 'filterMenuLinksForPostPayPlusAdmin')
                );
            }
        }
        $links['PLUS_ADMIN']['leftMenu'] = $plusAdminLinks;
        return $links;
    }

    protected function filterMenuLinksForPostPayPlusAdmin($link)
    {
        return empty($link['disable_for_postpay']);
    }

    private function alterBundleAssignedLinks($links)
    {
        $plusAdminLinks = $links['PLUS_USER']['leftMenu'];
        foreach ($plusAdminLinks as $linkKey => $linkGroup) {
            if (!empty($linkGroup['submenu'])) {
                $plusAdminLinks[$linkKey]['submenu'] = array_filter(
                    $plusAdminLinks[$linkKey]['submenu'],
                    array($this, 'filterMenuLinksForBundleAssignedPlusUser')
                );
            }
        }
        $links['PLUS_USER']['leftMenu'] = $plusAdminLinks;
        return $links;
    }

    protected function filterMenuLinksForBundleAssignedPlusUser($link)
    {
        return empty($link['hide_if_bundle_assigned']);
    }
}
