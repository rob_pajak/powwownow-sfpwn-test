<?php

/**
 * Maintains a log of a users page visits into a session var
 *
 * The log is only stored in a session var, if you want to persistently store
 * it then you would need to do that separately, probably using the $this->getContents()
 * method.
 *
 * You will want to make sure the server is configured to have a reasonable session
 * expiry time, eg, 30 days.
 *
 * The log should be seen as 'read only', writing to it directly will not work,
 * but there is $this->updateAll() to update a value to every entry in the log.
 * (In future, if needed, we could add an update() function which modify a value,
 * serializes it, then puts it back in to the log)
 *
 * The most common usage of this class is logging page views, this is done by
 * putting the following on every page:
 *
 * <code>
 * $visitLogger = new PWN_Tracking_PageVisitLog();
 * $visitLogger->logVisit();
 * </code>
 *
 * To get the contents of the log use:
 * <code>
 * $visitLogger = new PWN_Tracking_PageVisitLog();
 * $visitLogger->getContents();
 * </code>
 *
 * Class implements Iterator, so you can run a foreach() on it
 *
 * @uses Iterator Means this class can be iterated round to get each log entry
 * @uses PWN_Tracking_PageVisitLog_Entry Each log entry in the log is a serialized PWN_Tracking_PageVisitLog_Entry
 * @author Mark Wiseman
 *
 */
class PWN_Tracking_PageVisitLog implements Iterator
{
    /**
     * @var integer Required by Iterator to store current position when looping
     *      through an array
     */
    private $_position = 0;

    /**
     * Starts session if it isn't already
     */
    public function __construct()
    {

        //if(!isset($_SESSION)) {
        if (session_id() == '') {
            session_start();
        }
    }

    /**
     * Logs current page visit to the log, each log entry is a serialized instance
     * of PWN_Tracking_PageVisitLog_Entry
     *
     * @return self
     */
    public function logVisit()
    {
        // If the log has not been created then create it
        if (!isset($_SESSION['page_visits'])) {
            $_SESSION['page_visits'] = array();
        }

        // Add the current page visit and associated data to the log
        $visit                     = new PWN_Tracking_PageVisitLog_Entry();
        $_SESSION['page_visits'][] = serialize($visit);

        return $this;
    }

    /**
     * Gets the complete unserialized contents of the log
     *
     * Returns a copy rather than a reference, so it is not writable (a log should
     * really be read only)
     *
     * @return array
     */
    public function getContents()
    {

        if (!isset($_SESSION['page_visits'])) {
            return array();
        }

        $return = array();

        foreach ($this as $v) {
            $return[] = $v;
        }

        return $return;
    }

    /**
     * In theory a log should be historical and read only, but unfortunately it
     * turns out this is not the case!
     *
     * This function updates all entries in the log with $key->value
     *
     * @param $key
     * @param $value
     * @param array $ids
     * @return $this
     */
    public function updateAll($key, $value, array $ids = null)
    {
        foreach ($this as $k => $v) {
            $v->$key                     = $value;
            $_SESSION['page_visits'][$k] = serialize($v);
        }

        return $this;
    }

    /**
     * Empties the log
     * @return self
     */
    public function clear()
    {
        if (isset($_SESSION['page_visits'])) {
            $_SESSION['page_visits'] = array();
        }
        return $this;
    }

    /**
     * Required by Iterator abstract class
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Required by Iterator abstract class
     */
    public function current()
    {
        return unserialize($_SESSION['page_visits'][$this->position]);
    }

    /**
     * Required by Iterator abstract class
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Required by Iterator abstract class
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Required by Iterator abstract class
     */
    public function valid()
    {
        return isset($_SESSION['page_visits'][$this->position]);
    }

}
