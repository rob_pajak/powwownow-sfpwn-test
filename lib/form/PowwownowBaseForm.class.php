<?php

/**
 * Powwownow Base Form - Base Form Class
 *
 * @package    pwn
 * @subpackage form
 * @author     Asfer Tamimi
 */
class PowwownowBaseForm extends BaseForm
{
    /**
     * @var array
     */
    private $formArray;

    /**
     * @var sfBasicSecurityUser
     */
    public $user;

    /**
     * @var sfWebRequest
     */
    public $request;

    /**
     * Construct
     * @param array $defaults
     * @param array $options
     * @param null $CSRFSecret
     */
    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null)
    {
        if (!empty($options['user']) && $options['user'] instanceof sfUser) {
            $this->user = $options['user'];
            unset($options['user']);
        } else {
            $this->user = sfContext::getInstance()->getUser();
        }

        $this->request = sfContext::getInstance()->getRequest();

        parent::__construct($defaults, $options, $CSRFSecret);
    }

    /**
     * Form Configuration
     */
    public function configure()
    {
        $this->disableCSRFProtection();
        $this->disableLocalCSRFProtection();
        var_dump(sfConfig::get('sf_csrf_secret'));
    }

    /**
     * Final Form Configuration
     * @param string $formName
     */
    public function finalConfiguration($formName)
    {
        // Form Elements
        $this->setWidgets(
            $this->getFormWidgets()
        );

        // Form Labels
        $this->widgetSchema->setLabels(
            $this->getFormSchemaLabels()
        );

        // Set Validation
        $this->setValidators(
            $this->getFormValidation()
        );

        // Set the Widget Schema
        $this->widgetSchema->setNameFormat($formName . '[%s]');
    }

    /**
     * Get Form Errors
     * @return array
     */
    public function getFormErrors()
    {
        $errors = false;

        foreach ($this as $name => $error) {
            if (!$error->hasError() || strstr($name, 'csrf')) {
                continue;
            }

            sfContext::getInstance()->getLogger()->err(
                'Form Error Occurred: ' . $error->getError()->getMessageFormat() .
                ' for Field: ' . $this->getName() . '[' . $name . ']' .
                ' for Values: ' . print_r($this->getValues(), true)
            );

            $errors = array(
                'error_messages' => array(
                    'message'    => $error->getError()->getMessageFormat(),
                    'field_name' => $this->getName() . '[' . $name . ']'
                )
            );
            break;
        }

        // Check For Global Errors
        if (empty($errors) && $this->hasGlobalErrors()) {
            foreach ($this->getGlobalErrors() as $name => $error) {
                if (strstr($name, 'csrf')) {
                    continue;
                }
                sfContext::getInstance()->getLogger()->err(
                    print_r(
                        array_merge(
                            array('name' => $name),
                            print_r(unserialize($error->serialize()), true)
                        )
                    , true)
                );
            }
        }

        return $errors;
    }

    /**
     * Set the Form Array
     * @param $formArray
     */
    public function setFormArray($formArray)
    {
        $this->formArray = $formArray;
    }

    /**
     * Get Form Widgets
     * @return mixed
     */
    public function getFormWidgets()
    {
        $result = array();
        foreach ($this->formArray as $fieldName => $fieldInfo) {
            $class      = $fieldInfo['widget']['function'];
            $params     = (isset($fieldInfo['widget']['params'])) ? $fieldInfo['widget']['params'] : array();
            $attributes = (isset($fieldInfo['widget']['attributes'])) ? $fieldInfo['widget']['attributes'] : array();

            $result[$fieldName] = new $class($params, $attributes);
        }
        return $result;
    }

    /**
     * Get Form Schema Labels
     * @return array
     */
    public function getFormSchemaLabels()
    {
        $result = array();
        foreach ($this->formArray as $fieldName => $fieldInfo) {
            $result[$fieldName] = $fieldInfo['label'];
        }
        return $result;
    }

    /**
     * Get Form Validation Rule
     * @return array
     */
    public function getFormValidation()
    {
        $result = array();
        foreach ($this->formArray as $fieldName => $fieldInfo) {
            $result[$fieldName] = $fieldInfo['validator'];
        }
        return $result;
    }

    /**
     * Return the Array Keys aka the Form Fields
     * @return array
     */
    public function getFormFields()
    {
        return array_keys($this->formArray);
    }
}
