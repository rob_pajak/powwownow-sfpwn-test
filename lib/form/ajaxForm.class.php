<?php

/**
 * Abstract class for creating JSON Ajax forms
 *
 * SF1 form configuration is done by implementing configure(), but we are implementing it here to do the standard
 * configuration. So subclasses should implement configureForm() for configuration
 */
abstract class ajaxForm extends PwnForm
{

    /**
     * Place to put your own form configuration (setWidgets, validationGroupFactory etc)
     *
     * @return mixed
     */
    abstract public function configureForm();

    /**
     * Configuration for all forms of this type
     */
    public function configure()
    {
        // CSRF protection generates a unique token every time the form is submitted, this doesn't work with ajax
        // forms so we need to disable it
        $this->disableCSRFProtection();

        return $this->configureForm();
    }

}
