<?php

/**
 * PWNForm - pwn base form class
 *
 * @author marcin
 */

/**
 * PWNForm - Powwownow Base Form Class
 * 
 * @package    powwownow
 * @subpackage form
 * @author     Marcin 
 * @amend      Asfer [15th March 2012]
 */
class PWNForm extends BaseForm {

    /**
     * @tutorial how to output result 
     * echo json_encode($sf_data->getRaw('errors'));
     *
     * @kludge If you do not disable CSRF protection, calling isValid() on a form will return false but this method
     *         will not return any errors
     *
     * // @todo use get errors instead?
     * foreach($this->updatePasswordForm->getErrorSchema()->getErrors() as $e) {
     *     var_dump($e->__toString());
     * }
     *
     * @return array
     */
    public function pwn_getErrors() {
        $errors = array();

        foreach ($this as $name => $error) {
            //Check if there is no Error, so continue
            if (!$error->hasError())
                continue;

            //Only Return the First Error, In addition Log the Error
            sfContext::getInstance()->getLogger()->err('Error Occured: ' . $error->getError()->getMessageFormat() . ' for field_name: ' . $this->getName() . '[' . $name . ']');
            if (strstr($name, 'csrf')) {
                //$errors[] = array('message' => 'ERROR_MESSAGE_CSRF', 'field_name' => 'alert');
            } else {
                $errors[] = array('message' => $error->getError()->getMessageFormat(), 'field_name' => $this->getName() . '[' . $name . ']');
            }
            break;
        }

        //Check Count of Errors, Then Look at Global Errors
        if (count($errors) == 0 && $this->hasGlobalErrors()) {
            foreach ($this->getGlobalErrors() as $name => $error) {
                sfContext::getInstance()->getLogger()->err('Global Error Occured: ' . $error);
                //$errors[] = array('message' => 'Global Error Occured: '.$error, 'field_name' => 'alert');
                //break;
            }
        }

        return array('error_messages' => $errors);
    }

    public function submitButton($label,$options = array()) {
        
        $ht = array();
        foreach($options AS $k => $v)
            $ht[] = $k . '="' . $v . '"';            
        
        return '<div class="form-action grid_sub_24" ' . implode(' ',$ht) . '>
                    <button class="button-orange" type="submit">
                        <span>' . $label . '</span>
                    </button>
                </div>';
    }

}
