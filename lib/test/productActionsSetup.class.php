<?php 

class ProductActionsSetup{
    
    public static function getPlusProductIds(){
    
        try{
             
            $plus_product_groups = Hermes_Client_Rest::call( 'getAllPlusProductGroups' , array() );
    
        }catch(Exception $e){
    
            echo $e->getMessage(). "\r\n";
            exit;
    
        }
       
        $prod_ids =array();

        if(count($plus_product_groups['product_list']) > 0 ){

            foreach($plus_product_groups['product_list'] as $product_group){
                          
                $prod_ids[]= $product_group['product_group_id'];

            }
           
        }        
            
        return $prod_ids;
    
    }
    
    
}