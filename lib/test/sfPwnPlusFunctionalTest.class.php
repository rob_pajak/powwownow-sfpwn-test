<?php

class sfPwnPlusFunctionalTest extends sfTestFunctional
{

    public $fixtures = array();
    private $user_email = 'rob.macgregor+plusftest_';
    private $user_email_domain = '@powwownow.com';
    private $password = 'r1chm0nd';

    public static $unique = 0;

    public function __construct(sfBrowser $obj, $user_email = null)
    {
        if (!is_null($user_email)) {

            $this->user_email = $user_email . '+plusftest_';
        }

        parent::__construct($obj, null, array('response' => 'sfPwnTesterResponse'));
    }

    public function registerPlusUser()
    {

        $ts = time();

        $this->user_email .= $ts . $this->user_email_domain;

        $args = array(
            'email'        => $this->user_email,
            'title'        => 'Mr',
            'first_name'   => 'functional',
            'last_name'    => 'tester',
            'phone'        => '0000000000',
            'mobile_phone' => '0700000000',
            'organisation' => 'testing inc.',
            'location'     => 'UK',
            'password'     => $this->password,
            'service_ref'  => '850',
            'source'       => 'functional Test',
            'locale'       => 'en_GB'
        );
        try {

            Hermes_Client_Rest::call('doPlusRegistrationRegister', $args);
        } catch (Exception $e) {

            throw $e;
        }

        echo 'created user ' . $this->user_email . "\r\n";

        return $this->fixtures['users'][] = array('email' => $this->user_email, 'password' => $this->password);
    }

    public function args($source, $injection = array())
    {

        $path = 'test/args/';

        if (!file_exists($path . $source)) {
            throw new Exception('Fa!l: Data source file not found ' . $source);
        }

        extract($injection);

        return eval('return ' . trim(str_replace('<?php', '', file_get_contents($path . $source))));
    }

    public function unique()
    {
        return 'functional' . date("YmdHis") . ++sfPwnPlusFunctionalTest::$unique;
    }
}