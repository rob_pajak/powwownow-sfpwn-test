<?php

class sfPwnTesterResponse extends sfTesterResponse
{
    public function checkElementWithoutWhitespace($selector, $value, $options = array())
    {
        return parent::checkElement($selector, "/^\s*$value\s*$/", $options);
    }

    public function checkElementContainsText($selector, $value, $options = array())
    {
        return parent::checkElement($selector, "/$value/", $options);
    }
}
