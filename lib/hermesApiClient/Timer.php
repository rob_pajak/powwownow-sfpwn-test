<?php 


/**
 * PWN_Timer: static class providing logging capability
 *
 * @package     PWNUtils
 * @author      Robert Pajak
 * 
 * @created     2011-02-14
 */
final class PWN_Timer {

    /**
     * Static protected class variable
     */
    protected static $_started = array();

    /**
     * start a timer
     * 
     * @param string $timerName name of the timer you want to start (init)
     * 
     * @return mixed true on success or throw an Exception 
     */
    static public function start($timerName = 'timer1') {

        if (array_key_exists($timerName, self::$_started)) {

            throw new Exception("Timer $timerName already started. Use different timer name, or reset timer");

        }

        self::$_started[$timerName] = microtime(true);

        PWN_Logger::log("==>  logger $timerName started", PWN_Logger::DEBUG_VERBOSE);

        return true;

    }

    /**
     * get measured time difference for specific timer
     * 
     * @param integer $precision precision for returned time
     * @param string $timerName timer name
     * 
     * @return mixed (float) time difference for timer or Exception is beeing thrown
     */
    public static function getTime($precision = 4, $timerName = 'timer1') {

        if (!array_key_exists($timerName, self::$_started)) {

            throw new Exception("Timer $timerName NOT started.");

        }

        if (!is_int($precision) || $precision < 0) {

            $ret = (sprintf("%0.4f", (microtime(true) - self::$_started[$timerName])));

        } else {

            $ret = (sprintf("%0.".$precision."f", (microtime(true) - self::$_started[$timerName])));

        }

        PWN_Logger::log("==> logger $timerName getTime returned $ret", PWN_Logger::DEBUG_VERBOSE);
        return $ret;

    }


    /**
     * reset timer
     * 
     * @param string $timerName timer name
     * 
     * @return true on success
     */
    public static function reset($timerName = 'timer1') {

        if (!array_key_exists($timerName, self::$_started)) {

            self::start($timerName);
            PWN_Logger::log("==> logger $timerName reset (started)", PWN_Logger::DEBUG_VERBOSE);

        } else {

            self::$_started[$timerName] = microtime(true);
            PWN_Logger::log("==> logger $timerName reset", PWN_Logger::DEBUG_VERBOSE);

        }

        return true;

    }

    /**
     * Get Timer Names
     *
     * @return array of timers
     * 
     */
     public static function getTimers() {

         $ret = array_keys(self::$_started);
         sort($ret);
         return $ret;

     }

}
