<?php

require_once dirname(__FILE__) . '/Exception.php';

class Hermes_Client_XML_Request_Exception extends Hermes_Client_Exception
{

    protected function decodeResponseBody($responseBody, $assocResponseBody = true) {
        $xml = new SimpleXMLElement($responseBody);
        if ($assocResponseBody) {
            $xml = (array) $xml;
        }
        return $xml;
    }

}
