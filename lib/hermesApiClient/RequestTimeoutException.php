<?php

require_once dirname(__FILE__) . '/Exception.php';

class Hermes_Client_Request_Timeout_Exception extends Hermes_Client_Exception
{

    public function __construct($responseBody, $responseInfo, $verb, $requestBody, $requestLength, $assocResponseBody = true) {
        parent::__construct($responseBody, $responseInfo, $verb, $requestBody, $requestLength, $assocResponseBody = true);
        $this->message = "Request to server has timed out.";
    }

    public function __toString() {
        return $this->getMessage();
    }

}
