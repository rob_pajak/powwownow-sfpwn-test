<?php
/**
 * PWN_Logger: static class providing logging capability
 *
 * @package     PWNUtils
 * @author      Robert Pajak

 * @created 2011-02-14
 * @edited  2011-05-10 - rewrited to use non blocking, non exclusive lock writing functions
 */
class PWN_Logger {

    /**
     * Some constans
     * 
     */
    const CRITICAL = 0;
    const ERROR = 1;
    const WARNING = 2;
    const INFO = 3;
    const DEBUG = 4;
    const DEBUG_VERBOSE = 5;

    /**
     *  Log level names
     * 
     */
    static protected $_LOG_LEVEL_NAMES = array(0 => 'CRITICAL', 1 => 'ERROR', 2 => 'WARNING', 3 => 'INFO', 4 => 'DEBUG', 5 => 'DEBUG_VERBOSE');

    /**
     * private variable to store current APPLICATION Log level
     * and resource handlers for opened logs
     */
    protected static $_logFileHandlers = array();
    protected static $_logFiles = array();
    protected static $_currentLogLevel = self::INFO;

    /**
     * get current APPLICATION Log Level
     * 
     * @return int Current Logger Level 
     */
    public static function getCurrentLogLevel() {

        return self::getCurrentLogLevel();

    }

    /**
     * set current APPLICATION Log Level
     *
     * @param int $newLogLevel new Logger Log level value
     * 
     * @return mixed true on success or Exception is beeing thrown
     */
    public static function setCurrentLogLevel($newLogLevel) {

         if ($newLogLevel <0 || ($newLogLevel >5)) {

             throw new Exception ("Log level has to be between 0 and 5, $newLogLevel passed");

         }

         self::$_currentLogLevel = $newLogLevel;

         return true;

    }

    /**
     * INIT new logger file and assign it with logger name
     * 
     * @param string $loggerName name of the logger for future reference
     * @param string $logFilename log file name (including path)
     * 
     * @return mixed true on success or Exception is beeing thrown
     * 
     * 
     * -------     DEPRECATED   ---------------------
     * 
     */
    static public function initLogFileBlocking($logFilename = '/var/log/PWNLogger.log', $loggerName = 'logger1') {

        if (array_key_exists($loggerName, self::$_logFileHandlers )) {

            throw new Exception("$loggerName: Log File already instansiated");

        }

        $pid = posix_getpid();
        file_put_contents('/var/log/hermes/file.log', "Process {$pid} is about to fopen file: ${$logFilename}\n", FILE_APPEND);
        $res = fopen($logFilename, 'a+');
        file_put_contents('/var/log/hermes/file.log', "Process {$pid} is succesfully opened: ${$logFilename}\n", FILE_APPEND);
        
        if ($res === FALSE) {

            throw new Exception("$loggerName: Log File OPEN FAILURE ($logFilename)");

        }

        self::$_logFileHandlers[$loggerName] =  array('res' => $res, 'file' => $logFilename); 

        return true;

    }

    /**
     * log message
     * 
     * Depending of Logger currentLogLevel property message will be logged in (if requested Log Level is smaller or equal) or simply ignored
     *
     * @param string $message message to be logged
     * @param int $logLevel requested Log Level for this message
     * @param string $loggerName logger name
     * 
     * @return mixed true on success or Exception is beeing thrown
     * 
     * 
     * 
     *    -------------     DEPRECATED      -------------
     * 
     * 
     */
    static public function logBlocking($message, $logLevel = 3, $loggerName = 'logger1') {


        if ((count(self::$_logFileHandlers) < 1) || !(array_key_exists($loggerName, self::$_logFileHandlers)) || !(is_resource(self::$_logFileHandlers[$loggerName]['res']))) {

                if (self::initLogFile($loggerName) !== TRUE) {

                    throw new Exception("$loggerName: Log File OPEN FAILURE");

                }

        }

        // Log messages only if current log level is greater than requested log level)
        // If it's not ... do nothing :)
        if (self::$_currentLogLevel >=  $logLevel) {

            $pid = posix_getpid();
            file_put_contents('/var/log/hermes/file.log', "Process {$pid} is about to change lock to Exclusive lock\n", FILE_APPEND);

            if (flock(self::$_logFileHandlers[$loggerName]['res'], LOCK_EX | LOCK_NB)) {

                file_put_contents('/var/log/hermes/file.log', "Process {$pid} is succesfully changed lock to Exclusive lock\n", FILE_APPEND);

                $dt = '[' . self::$_LOG_LEVEL_NAMES[$logLevel] ." " . date("y-m-d H:i:s") . ']';

                $IP = (isset($_SERVER['REMOTE_ADDR'])) ? '{ '.  $_SERVER['REMOTE_ADDR'] . ' }' : '';
                $forwardedFrom = (isset($_SERVER['X_FORWARDED_FOR HTTP'])) ? '->{ '.  $_SERVER['X_FORWARDED_FOR HTTP'] . ' }' : '';

                $ext = (self::$_currentLogLevel >= self::DEBUG_VERBOSE) ? ', REQUEST: '.serialize($_REQUEST).', $_SERVER: '.serialize($_SERVER) : '';

                fwrite(self::$_logFileHandlers[$loggerName]['res'], $dt . $IP . $forwardedFrom  . " " . $message . $ext. "\n");
                fflush(self::$_logFileHandlers[$loggerName]['res']);
                file_put_contents('/var/log/hermes/file.log', "Process {$pid} is about to reverting lock to Shared lock\n", FILE_APPEND);
                if (!flock(self::$_logFileHandlers[$loggerName]['res'], LOCK_SH | LOCK_NB)) {
                    throw new Exception("FAILURE => PWN_logger::log releasing an exclusive lock");
                }
                file_put_contents('/var/log/hermes/file.log', "Process {$pid} is reverted lock to Shared lock\n", FILE_APPEND);
            } else {
                file_put_contents('/var/log/hermes/file.log', "Process {$pid} failed to get Exclusive lock\n", FILE_APPEND);
                throw new Exception("FAILURE => PWN_logger::log create an exclusive lock");
            }

        }

        return true;

     }



/**
     * INIT new logger file and assign it with logger name
     * 
     * @param string $loggerName name of the logger for future reference
     * @param string $logFilename log file name (including path)
     * 
     * @return mixed true on success or Exception is beeing thrown
     * 
     */
    static public function initLogFile($logFilename = '/var/log/PWNLogger.log', $loggerName = 'logger1') {

        if (array_key_exists($loggerName, self::$_logFiles )) {

            throw new Exception("$loggerName: Log File already exists");

        }

        self::$_logFiles[$loggerName] =  array('file' => $logFilename); 

        return true;

    }

    /**
     * log message
     * 
     * Depending of Logger currentLogLevel property message will be logged in (if requested Log Level is smaller or equal) or simply ignored
     *
     * @param string $message message to be logged
     * @param int $logLevel requested Log Level for this message
     * @param string $loggerName logger name
     * 
     * @return mixed true on success or Exception is beeing thrown
     * 
     * 
     */
    static public function log($message, $logLevel = 3, $loggerName = 'logger1') {


        if ((count(self::$_logFiles) < 1) || !(array_key_exists($loggerName, self::$_logFiles))) {

                if (self::initLogFile($loggerName) !== TRUE) {

                    throw new Exception("$loggerName: Log File OPEN FAILURE");

                }

        }

        // Log messages only if current log level is greater than requested log level)
        // If it's not ... do nothing :)
        if (self::$_currentLogLevel >=  $logLevel) {

            $dt = '[' . self::$_LOG_LEVEL_NAMES[$logLevel] ." " . date("y-m-d H:i:s") . ']';

            $IP = (isset($_SERVER['REMOTE_ADDR'])) ? '{ '.  $_SERVER['REMOTE_ADDR'] . ' }' : '';
            $forwardedFrom = (isset($_SERVER['X_FORWARDED_FOR HTTP'])) ? '->{ '.  $_SERVER['X_FORWARDED_FOR HTTP'] . ' }' : '';

            $ext = (self::$_currentLogLevel >= self::DEBUG_VERBOSE) ? ', REQUEST: '.serialize($_REQUEST).', $_SERVER: '.serialize($_SERVER) : '';

            if (file_put_contents(self::$_logFiles[$loggerName]['file'], $dt . $IP . $forwardedFrom  . " " . $message . $ext. "\n", FILE_APPEND) === false) {

                throw new exception("FAILURE => Attempt to log the message ". $dt . $IP . $forwardedFrom  . " " . $message . $ext." to a file ".self::$_logFiles[$loggerName]['file'] . " unsucessful");

            }

        }

        return true;

     }


    /**
     * Get Loggers Name
     *
     * @return array array of strings (names) of initialized loggers
     * 
     */
     public static function getLoggersBlocking() {

         $ret = array_keys(self::$_logFileHandlers);
         sort($ret);
         return $ret;

     }

    /**
     * Get Loggers Name
     *
     * @return array array of strings (names) of initialized loggers
     * 
     */
     public static function getLoggers() {

//         var_dump(self::$_logFiles);

         $ret = array_keys(self::$_logFiles);
         sort($ret);
         return $ret;

     }

    /**
     * Get Logger File for specific (defined) logger
     *
     * @param string $loggerName name of the logger
     * 
     * @return mixed file for logger Name or Exception is beeing thrown if logger was not init
     * 
     */
     public static function getLoggerFileBlocking($loggerName = 'logger1') {

         if (!(array_key_exists($loggerName, self::$_logFileHandlers)) || !isset(self::$_logFileHandlers[$loggerName]['res'])) {

             throw new Exception ("$loggerName was not init. Couldn't get file information");
 
         }

         return self::$_logFileHandlers[$loggerName]['file'];

     }

    /**
     * Get Logger File for specific (defined) logger
     *
     * @param string $loggerName name of the logger
     * 
     * @return mixed file for logger Name or Exception is beeing thrown if logger was not init
     * 
     */
     public static function getLoggerFile($loggerName = 'logger1') {

         if (!(array_key_exists($loggerName, self::$_logFiles))) {

             throw new Exception ("$loggerName was not init. Couldn't get file information");
 
         }

         return self::$_logFiles[$loggerName]['file'];

     }


}

/* Examples of usage:
 * 
 */

/*
// logger1 is the default logger
PWN_Logger::initLogFile('/tmp/1.log');

//Optional - change Logger Log level
PWN_Logger::setCurrentLogLevel(PWN_Logger::DEBUG);

//Simply log message (if requested Log Level is greater than current Logger Log Level message will not append to log)
PWN_Logger::log('This should display: DEBUG message, log level DEBUG ', PWN_Logger::DEBUG);

//Optional - change Logger Log level
PWN_Logger::setCurrentLogLevel(PWN_Logger::INFO);

PWN_Logger::log('This should not display: DEBUG message, log level INFO ', PWN_Logger::DEBUG);

//Init other logger
PWN_Logger::initLogFile('/tmp/sqls.log', 'loggerSQL');

//And log another message
PWN_Logger::log('This should display: logging to SQL log, WARNING level', PWN_Logger::WARNING, 'loggerSQL');

PWN_Logger::setCurrentLogLevel(PWN_Logger::ERROR);
PWN_Logger::log('This should not display: logging to SQL log, WARNING level, but log level ERROR', PWN_Logger::WARNING, 'loggerSQL');

var_dump(PWN_Logger::getLoggers());

var_dump(PWN_Logger::getLoggerFile());
var_dump(PWN_Logger::getLoggerFile('loggerSQL'));

*/

