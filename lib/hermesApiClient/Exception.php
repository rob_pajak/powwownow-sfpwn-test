<?php

/**
 * Wraps an Exception thrown by the Hermes Server, exposing access to the responseBody and HTTP Code if any.
 */
class Hermes_Client_Exception extends Exception {

    /**
     * Exposed for backwards-compatibility.
     *
     * @var array|mixed|object|string
     */
    public $responseBody;
    public $responseInfo;
    public $verb;
    public $requestLength;
    public $requestBody;

    protected $httpStatusCode;
    protected $httpStatus;

    /**
     * @param string|array|object $responseBody
     * @param array $responseInfo
     * @param string $verb
     * @param string $requestBody
     * @param string $requestLength
     * @param bool $assocResponseBody
     */
    public function __construct($responseBody, $responseInfo, $verb, $requestBody, $requestLength, $assocResponseBody = true) {
        // Set the responseBody and httpStatus, if any.
        if (is_string($responseBody)) {
            $responseBody = $this->decodeResponseBody($responseBody, $assocResponseBody);
        }
        $this->responseBody = $responseBody;
        $this->httpStatusCode = $this->retrieveByKeyOrProperty($responseBody, 'httpCode');
        $this->httpStatus = $this->retrieveByKeyOrProperty($responseBody, 'httpStatus');
        $this->responseInfo = $responseInfo;

        // Store other exposed variables; these are rarely used.
        $this->verb = $verb;
        $this->requestBody = $requestBody;
        $this->requestLength = $requestLength;

        $message = $this->retrieveByKeyOrProperty($responseBody, 'errorMessage');
        parent::__construct($message, 0);

        // Because Exception expects the code to be numeric, we override it after the parent constructor call.
        $this->code = $this->retrieveByKeyOrProperty($responseBody, 'errorCode', 0);
    }

    protected function decodeResponseBody($responseBody, $assocResponseBody = true) {
        return json_decode($responseBody, $assocResponseBody);
    }

    public function getResponseBody() {
        return $this->responseBody;
    }

    public function getServerException() {
        return $this->getPrevious();
    }

    public function getHTTPStatusCode($default = 500) {
        if (empty($this->httpStatusCode)) {
            return $default;
        }
        return $this->httpStatusCode;
    }

    public function getHTTPStatus() {
        return $this->httpStatus;
    }

    public function getResponseInfo() {
        return $this->responseInfo;
    }

    public function __toString() {
        return sprintf("Invalid response from server: (%s) %s [HTTP Status Code %s]", $this->getCode(), $this->getMessage(), $this->httpStatusCode);
    }

    protected function retrieveByKeyOrProperty($container, $key, $default = null) {
        $value = $default;
        if (isset($container[$key])) {
            $value = $container[$key];
        } else if (isset($container->$key)) {
            $value = $container->$key;
        }
        return $value;
    }

}
