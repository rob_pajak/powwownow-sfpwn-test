<?php

// Checking if all neccessary PHP modules are loaded or required modules are compilled into PHP core 
if (!extension_loaded('curl')) {
    if (!@dl('curl.so')) {
        throw new Exception("PHP CURL not compilled into PHP core, module was not loaded by default, and coundn't be loaded manually");
    }
}

/**
 * Makes a Restful request using cURL.
 *
 * @package     HermesAPI Client
 * @author      Robert Pajak & Joseph Smith
 */
class Hermes_Client_RestRequest  {

    protected $url;  
    protected $verb;  
    protected $requestBody;  
    protected $requestLength;  
    protected $username;  
    protected $password;  
    protected $acceptType;  
    protected $responseBody;  
    protected $responseInfo;
    protected $_exceptions;

    /**
     * Class constructor
     * 
     * @param string $url requested API Server URL
     * @param string $verb request method, defaults to GET
     * @param string $requestBody body of the request
     * @param boolean $exceptions indicator how Hermes Client should behave in case of not 200 response code: by default it'll throw an Exception
     */
    public function __construct($url = null, $verb = 'GET', $requestBody = null, $exceptions = true)
    {  
        $this->url               = $url;
        $this->verb              = $verb;
        $this->requestBody       = $requestBody;

        // @todo shouldn't we send proper header with proper length? Or curl librarry is automatically replace it with value based on requestBody?
        $this->requestLength     = 0;  
        $this->username          = null;  
        $this->password          = null;  
        $this->acceptType        = 'application/json';  
        $this->responseBody      = null;  
        $this->responseInfo      = null;  
  
        if ($this->requestBody !== null)  {  
            $this->buildPostBody();  
        }
        
        $this->_exceptions = $exceptions;
    }  

    /**
     * flush method to reset all protected properties to default values
     */
    public function flush()  
    {  
        $this->requestBody       = null;  
        $this->requestLength     = 0;  
        $this->verb              = 'GET';  
        $this->responseBody      = null;  
        $this->responseInfo      = null;  
    }

    /**
     * execute method to perform a API Call
     *
     * @throws Hermes_Client_Request_Timeout_Exception
     * @throws Hermes_Client_Exception|Hermes_Client_XML_Request_Exception
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function execute() {

        $ch = curl_init();
        $this->setAuth($ch);
        $errorNo = false;
        
        try {

            switch (strtoupper($this->verb)) {

                case 'GET':
                    $errorNo = $this->executeGet($ch);
                    break;

                case 'POST':
                    $errorNo = $this->executePost($ch);
                    break;

                case 'PUT':
                    $this->executePut($ch);
                    break;

                case 'DELETE':
                    $this->executeDelete($ch);
                    break;

                default:
                    throw new InvalidArgumentException('Current verb (' . $this->verb . ') is an invalid REST verb.');

            }

        } catch(Exception $e) {
            curl_close($ch);
            throw $e;
        }

        // Handle request timeout.
        if ($this->hasRequestTimedOut($errorNo)) {
            throw new Hermes_Client_Request_Timeout_Exception($this->responseBody, $this->responseInfo, $this->verb, $this->requestBody, $this->requestLength);
        }

        // Handle invalid response from server.
        if ($this->_exceptions === true && $this->responseInfo['http_code'] != '200') {
            if ($this->isResponseXML()) {
                $clientException = new Hermes_Client_XML_Request_Exception($this->responseBody, $this->responseInfo, $this->verb, $this->requestBody, $this->requestLength);
            } else {
                $clientException = new Hermes_Client_Exception($this->responseBody, $this->responseInfo, $this->verb, $this->requestBody, $this->requestLength);
            }
            throw $clientException;
        }
    }

    protected function hasRequestTimedOut($errorNo) {
        return (int) $errorNo === 28;
    }

    protected function isResponseXML() {
        return is_string($this->responseBody) && substr($this->responseBody, 0, 5) === '<?xml';
    }

    /**
     * Constructs the request parameters into the necessary format.
     *
     * @param array|null $data
     * @throws InvalidArgumentException
     */
    public function buildPostBody($data = null) {

        $data = ($data !== null) ? $data : $this->requestBody;

        if (!is_array($data)) {

            throw new InvalidArgumentException('Invalid data input for postBody. Array expected');

        }

        $data = http_build_query($data, '', '&');
        $this->requestBody = $data;
 
    }

    /**
     * Returns the result of the response.
     * 
     * @return string
     */
    public function getResponseBody() {

        return $this->responseBody;

    }

    /**
     * Returns all information on the response.
     * 
     * @return array
     */
    public function getResponseInfo() {

        return $this->responseInfo;

    }

    /**
     * Executes the request, handling GET-specific processing.
     *
     * @param resource $ch
     * @return int
     *   Curl error number.
     */
    protected function executeGet($ch) {

        return $this->doExecute($ch);

    }

    /**
     * Executes the request, handling POST-specific processing.
     *
     * @param resource $ch
     * @return int
     *   Curl error number.
     */
    protected function executePost($ch) {

        if (!is_string($this->requestBody)) {

            $this->buildPostBody();

        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_COOKIE, 'XDEBUG_SESSION=PHPSTORM');

        return $this->doExecute($ch);

    }

    /**
     * executePut method 
     * 
     * @return void
     */
    protected function executePut($ch) {

        throw new Exception("Put method not implemented yet");

    }

    /**
     * executeDelete method 
     * 
     * @return void
     */
    protected function executeDelete($ch) {

        throw new Exception("Delete method not implemented yet");

    }

    /**
     * doExecute method
     *
     * @param resource $curlHandle
     * @return int
     */
    protected function doExecute(&$curlHandle) {

        $this->setCurlOpts($curlHandle);
        $this->responseBody = curl_exec($curlHandle);
        $this->responseInfo = curl_getinfo($curlHandle);

        $errorNo = curl_errno($curlHandle);

        curl_close($curlHandle);

        return $errorNo;
    }

    /**
     * setCurlOpts method 
     * 
     * @return void
     */
    protected function setCurlOpts(&$curlHandle) {

        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 15);
        curl_setopt($curlHandle, CURLOPT_URL, $this->url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array ('Accept: ' . $this->acceptType));
        if (isset($_COOKIE['XDEBUG_SESSION'])) {
            curl_setopt($curlHandle, CURLOPT_COOKIE, 'XDEBUG_SESSION='.$_COOKIE['XDEBUG_SESSION']);
        }

    }

    /**
     * setCurlOpts method 
     * 
     * @return void
     */
    protected function setAuth(&$curlHandle) {

        if ($this->username !== null && $this->password !== null) {
            curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
            curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        }

    }

}
