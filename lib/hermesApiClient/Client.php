<?php

/**
 * @package     HermesAPI Client
 * @author      Robert Pajak & Joseph Smith
 */
class Hermes_Client
{
    /**
     * URL of the Hermes_Server
     */
    protected static $_serverUrl = null;

    /**
     * Set $serverUrl
     *
     * @param string $serverUrl
     */
    public static function init($serverUrl)
    {
        self::$_serverUrl = $serverUrl;
    }
    
    /**
     * Set $serverUrl
     *
     * @param string $serverUrl
     */
    public static function setServerUrl($serverUrl)
    {
        self::$_serverUrl = $serverUrl;
    }
    
    /**
     * Get $serverUrl
     *
     * @return string       The URL of the Hermes_Server
     */
    public static function getServerUrl()
    {
        return self::$_serverUrl;
    }
}
