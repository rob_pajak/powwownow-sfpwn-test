<?php

/**
 * @package     HermesAPI Client
 * @author      Robert Pajak & Joseph Smith
 */
class Hermes_Client_Rest extends Hermes_Client {

    /**
     * define protocol type (rest)
     */
    private static $_protocol = 'rest';
    
    private static $_requests = array();


    /**
     *  Core method for Hermes_Client_Rest_class 
     * 
     *  @param string $name requested method
     *  @param array $arguments requested arguments
     *  @param string $format output format, default JSON, possible XML
     * 
     *  @return mixed response in requested format, defaults to JSON
     */
    private static function _execute($name, $arguments, $format = 'json') {
        self::validateExecuteArguments($arguments);

        $data = array(
            'protocol' => self::$_protocol,
            'format' => $format,
            'method' => $name,
            'arguments' => $arguments,
        );
        $url = self::$_serverUrl . '?' . http_build_query($data);
        $request = new Hermes_Client_RestRequest($url);

        $dispatcher = sfApplicationConfiguration::getActive()->getEventDispatcher();
        $ext = "${name}() with parameters " . print_r($arguments,true) . " requested output format: ${format}";
        $dispatcher->notify(new sfEvent('hermesAPI_client', 'application.log', array(
            'priority' => sfLogger::INFO,
            'Hermes API Client - Before execute: ' . $ext,
        )));
        $timer = sfTimerManager::getTimer("HermesApi \"${name}()\"");

        $request->execute();
        
        $timer->addTime();
        $dispatcher->notify(new sfEvent('hermesAPI_client', 'application.log', array(
            'priority' => sfLogger::INFO,
            'Hermes API Client - After execute: ' . $ext,
        )));
        $elapsedTime = $timer->getElapsedTime();
        self::$_requests[] = array(
            'name' => $name,
            'arguments' => $arguments,
            'format' => $format,
            'url' => $url,
            'elapsedTime' =>
            $elapsedTime,
            'response' => $request->getResponseBody()
        );

        return $request->getResponseBody();

    }

    /**
     * Builds a Hermes_Client_RestRequest to be executed with the POST verb.
     *
     * @param string $format
     * @param string $name
     *   The name of the exposed Hermes method.
     * @param $arguments
     *   The arguments for the method.
     * @return Hermes_Client_RestRequest
     */
    protected static function buildPOSTRequest($format, $name, $arguments)
    {
        $data = array(
            'protocol' => self::$_protocol,
            'format' => $format,
            'method' => $name,
            'arguments' => $arguments,
        );
        return new Hermes_Client_RestRequest(self::$_serverUrl, 'POST', $data);
    }

    protected static function validateExecuteArguments($arguments)
    {
        if (self::$_serverUrl == null) {
            throw new Exception('Server URL not specified');
        }

        if (!is_array($arguments)) {
            throw new Exception('Must supply a single argument of key/value pairs');
        }
    }

    /**
     * Build and execute a POST request to the Hermes API.
     *
     * @param string $name
     * @param array $arguments
     * @param string $format
     * @return string
     */
    protected static function executePOST($name, $arguments, $format = 'json') {
        self::validateExecuteArguments($arguments);

        $request = self::buildPOSTRequest($format, $name, $arguments);

        $dispatcher = sfApplicationConfiguration::getActive()
            ->getEventDispatcher();

        $ext = "${name}() with parameters " . print_r($arguments,true) . " requested output format: ${format}";

        $dispatcher->notify(new sfEvent('hermesAPI_client', 'application.log', array(
            'priority' => sfLogger::INFO,
            'Hermes API Client - Before execute: ' . $ext,
        )));

        $timer = sfTimerManager::getTimer("HermesApi \"${name}()\"");

        $request->execute();

        $timer->addTime();

        $dispatcher->notify(new sfEvent('hermesAPI_client', 'application.log', array(
            'priority' => sfLogger::INFO,
            'Hermes API Client - After execute: ' . $ext,
        )));

        $elapsedTime = $timer->getElapsedTime();

        self::$_requests[] = array(
            'name' => $name,
            'arguments' =>
            $arguments,
            'format' => $format,
            'url' => self::$_serverUrl,
            'elapsedTime' => $elapsedTime,
            'response' => $request->getResponseBody()
        );

        return $request->getResponseBody();
    }


    /**
     *  call function
     * 
     *  @param string $name requested method
     *  @param array $arguments requested arguments
     * 
     *  @return array response 
     */
    public static function call($name, $arguments) {
        return json_decode(self::_execute($name, $arguments), true);

    }

    /**
     *  callJson function
     * 
     *  @param string $name requested method
     *  @param array $arguments requested arguments
     * 
     *  @return string JSON encoded response 
     */
    public static function callJson($name, $arguments) {

        return self::_execute($name, $arguments);

    }

    /**
     *  callXml function
     * 
     *  @param string $name requested method
     *  @param array $arguments requested arguments
     * 
     *  @return string XML encoded response 
     */
    public static function callXml($name, $arguments) {

        return self::_execute($name, $arguments, 'xml');

    }

    /**
     * Call the Hermes method with the POST verb.
     *
     * @param string $name
     *   The name of the exposed Hermes method, including its namespace.
     * @param array $arguments
     *   Arguments for the Hermes method.
     * @return mixed
     */
    public static function callPOST($name, $arguments) {
        return json_decode(self::executePOST($name, $arguments), true);
    }

    /**
     * Call the Hermes method with the POST verb, expecting JSON as response.
     *
     * @param string $name
     *   The name of the exposed Hermes method, including its namespace.
     * @param array $arguments
     *   Arguments for the Hermes method.
     * @return string
     */
    public static function callPOSTJson($name, $arguments) {
        return self::executePOST($name, $arguments);
    }

    /**
     * Call the Hermes method with the POST verb, expecting XML as response.
     *
     * @param string $name
     *   The name of the exposed Hermes method, including its namespace.
     * @param array $arguments
     *   Arguments for the Hermes method.
     * @return string
     */
    public static function callPOSTXml($name, $arguments) {
        return self::executePOST($name, $arguments, 'xml');
    }

    public static function getProcessedRequestsData() {

        return self::$_requests;

    }

}

// Check that the class exists before trying to use it. This is for Ajax Usage Protection
if (class_exists('sfConfig')) {
   Hermes_Client_Rest::init(sfConfig::get('app_hermesurl', 'hermes.via-vox.net/rest.php'));
}
