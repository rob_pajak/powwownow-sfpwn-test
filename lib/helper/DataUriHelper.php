<?php
/**
 * @param $filename
 * @param string $mime
 *
 * @return string
 */
function getDataURI($filename, $mime = '') {
    
    $browser = sfContext::getInstance()->getUser()->getAttribute('browser_detection', null); 
    
    if ($browser === null) {
        return $filename;
    }
    //IE8 cant render a datauri past a certain size.
    if ( ($browser['data']['browser_number'] === '7.0') || ($browser['data']['browser_number'] === '8.0')) {
        return $filename;
    }

    $image = $filename;

    if (!file_exists($filename)) {
        $image = sfConfig::get('sf_web_dir') . $filename;
    }

    if (file_exists($image) && file_get_contents($image)){
        return 'data:'.(function_exists('mime_content_type') ? mime_content_type($image) : $mime).';base64,'.base64_encode(file_get_contents($image));
    } else {
        return '';
    }
}
