<?php
function getTrafficLightImg($stat) {
    if ($stat>=99.5) {
        return '<img src="/sfimages/light_green.png" alt="Green Light"/>';
    }elseif ($stat>=99) {
        return '<img src="/sfimages/light_amber.png" alt="Amber Light"/>';
    }else{
        return '<img src="/sfimages/light_red.png" alt="Red Light"/>';
    }
}
function getTrafficLightShape($stat) {
    if ($stat>=99.5) {
        return '<div class="circle_green">&nbsp;</div>';
    }elseif ($stat>=99) {
        return '<div class="circle_amber">&nbsp;</div>';
    }else{
        return '<div class="circle_red">&nbsp;</div>';
    }
}

function getDateRangeFormatting($date1,$date2) {
    $date1Month = date("M",strtotime($date1));
    $date2Month = date("M",strtotime($date2));
    $date1Year = date("Y",strtotime($date1));
    $date2Year = date("Y",strtotime($date2));

    if ($date1Month == $date2Month && $date1Year == $date2Year) {
        return date("F Y",strtotime($date1));
    }else{
        return date("F Y",strtotime($date1)) . ' - ' . date("F Y",strtotime($date2));
    }
}

function getHMS($hours) {
    $h  = floor($hours);
    $m1 = ($hours - $h)*60;
    $m  = floor($m1);
    $s  = floor(($m1 - $m)*60);

    return $h . ' Hours ' . $m . ' Minutes ' . $s . ' Seconds';
}