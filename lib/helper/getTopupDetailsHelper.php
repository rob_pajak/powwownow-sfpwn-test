<?php
/**
 * Get the topUpDetails
 *
 * @param array $args [payment_id]
 * @param array $args [account_id]
 *
 * @return string
 *
 * @author Asfer, Stealing Vitalys' Code :)
 *
 */
function getTopupDetails(array $args)
{
    if (isset($args['payment_id'])) {
        try {
            // Obtain the Topup Details
            $topUpDetail = Hermes_Client_Rest::call('Plus.getOrderDetails', array('order_id' => $args['payment_id']));

            if (count($topUpDetail) > 0 && $topUpDetail['account_id'] == $args['account_id']) {
                $topUpDetail['transaction_type'] = ('auto' == $topUpDetail['transaction_type']) ? 'Auto Topup' : 'Manual Topup';
                return $topUpDetail;
            } else {
                $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR');
                return array_merge(
                    array('error_found' => true),
                    array('error_messages' => $errorMessages)
                );
            }
        } catch (Exception $e) {
            $this->logMessage('Exception for Plus.getOrderDetails: ' . $e->getMessage(), 'info');
            $errorMessages[] = array('message' => 'FORM_COMMUNICATION_ERROR');
            return array_merge(
                array('httpCode' => (!empty($e->responseBody['httpCode'])) ? $e->responseBody['httpCode'] : 500),
                array('error_found' => true),
                array('error_messages' => $errorMessages)
            );
        }
    }

    return array();
}
