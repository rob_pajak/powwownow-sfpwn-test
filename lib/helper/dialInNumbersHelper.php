<?php
/**
 *  Dial-in Numbers Helper
 *
 *
 *  @author Asfer
 */

/**
 * Generates an array for the given dial-in Numbers Array.
 *
 * @todo Make more Dynamic
 * @param arr $dns
 * @param str $service
 * @return array
 */
function getDialInNumbers($dns,$service) {
    $dialInNumbers = array();

    foreach ($dns as $id => $details) {
        $germanCheck = ($details['country_code'] == 'DEU' && $details['is_a_default_mobile_number'] == 1) ? '(' . 'mobile number' . ')' : '';
        $mobileCheck = ($details['mobile'] == 0) ? ' <a href="#postcomment"><sup>1</sup></a>' : '';
        $internCheck = ($details['dial_international'] == 0 && $details['international_formatted'] == '') ?  ' <a href="#postcomment"><sup>2</sup></a>' : '';
        $natioaCheck = (strlen($details['national_formatted']) == 5) ? ' <a href="#postcomment"><sup>2</sup></a> <a href="#postcomment"><sup>3</sup></a>' : '';
        $oneparCheck = ($details['one_participant_limit'] == 'Y') ? ' <a href="#postcomment"><sup>4</sup></a>' : '';

        // German Variable Rate Check
        if ($details['country_code'] == 'DEU' && $details['is_a_default_mobile_number'] == 1) {
            $cpmCheck = 'Variable';
        } else {
            if ($details['iso2'] == 'SE' && $details['currency_name_en'] == 'Swedish krona') {
                $cpm = $details['cost_per_minute_value'] * 1;
            } elseif ($details['iso2'] == 'PL' && $details['currency_name_en'] == 'Polish zloty') {
                $cpm = $details['cost_per_minute_value'] * 1;
            } else {
                $cpm = number_format(($details['cost_per_minute_value']*100),1);
            }

            if ($cpm == round($cpm)) $cpm = round($cpm);
            $cpmCheck = $cpm . ' ' . $details['currency_name_translated'];
        }

        // DNIS Type Check
        // Change Requested by Sarah / Maria @ 16-04-2012
        if ($details['dnis_type'] == 'Freefone') $details['dnis_type'] = 'Freephone';

        // Change Requested by Sarah @ 25-07-2012
        if ($details['dnis_type'] == 'Geographic' && $service=='plus') $details['dnis_type'] = 'Landline';

        $dialInNumbers[] = array(
            $details['country_name'] . ' (' . $details['lang'] . ')' . $germanCheck . $mobileCheck . $internCheck . $natioaCheck . $oneparCheck,
            $details['national_formatted'],
            $details['international_formatted'],
            $cpmCheck,
            $details['dnis_type']
        );
    }
    return $dialInNumbers;
}

/**
 * Generates html rows in table for the given dial-in Numbers Array.
 *
 * @todo Make more Dynamic, and less Hardcoded
 * @param arr $dns
 * @return string
 */
function getDialInNumbersHeaderPopup($dns) {
    $dialInNumbers = '';
    $count = 1;

    foreach ($dns as $id => $details) {
        $dialInNumbers.= ($count % 2) ? '<tr class="odd">' : '<tr>';
        $dialInNumbers.= '<td>'.$details['country'].'</td>';
        $dialInNumbers.= '<td>'.$details['national_formatted'].'</td>';
        $dialInNumbers.= '</tr>';
        $count++;
    }

    // Hardcoded Skype and Worldwide Numbers
    $dialInNumbers.= ($count % 2) ? '<tr class="odd">' : '<tr>';
    $dialInNumbers.= '<td class="worldwide"></td><td>+44 844 4 73 73 73 or</td></tr>';
    $dialInNumbers.= ($count % 2) ? '<tr class="odd">' : '<tr>';
    $dialInNumbers.= '<td class="skype"></td><td>+49 1803 001 178</td></tr>';

    return $dialInNumbers;
}

/**
 * Returns the Local Dial-in Number for your country
 *
 * @param array $dialInNumbers
 * @param string $locale
 * @param string $country
 * @return string
 */
function getLocalDialInNumber($dialInNumbers, $locale, $country) {
    foreach ($dialInNumbers as $id => $details) {
        if ($locale == $details['locale'] && $country == $details['country_code']) {
            return $details['national_formatted'];
            break;
        }
    }
    return '';
}

/**
 * Returns the Local Mobile Dial-in Number for your country
 *
 * @param array $dialInNumbers
 * @param string $locale
 * @param string $country
 * @return string
 */
function getLocalMobileDialInNumber($dialInNumbers, $locale, $country) {
    foreach ($dialInNumbers as $id => $details) {
        if ($locale == $details['locale'] && $country == $details['country_code'] && 'Y' == $details['active_mobile']) {
            return $details['national_formatted'];
            break;
        }
    }
    return '';
}

/**
 * Returns the International Formatted Dial-in Number for your country
 *
 * @param array $dialInNumbers
 * @param string $locale
 * @param string $country
 * @return string
 */
function getInternationalDialInNumber($dialInNumbers, $locale, $country) {
    foreach ($dialInNumbers as $id => $details) {
        if ($locale == $details['locale'] && $country == $details['country_code']) {
            return $details['international_formatted'];
            break;
        }
    }
    return '';
}

