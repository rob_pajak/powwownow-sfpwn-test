<?php
/**
 * As scheduler has its own domain (and is on a seperate server),
 * we provide a way for a user to invisibly log in using a
 * special link.
 *
 * This helper returns the path and parameters for that special link,
 * for you to append to the scheduler domain name
 *
 * Can be called in a view by:
 *
 * <code>
 *     http://scheduler.powwownow.com<?= getSchedulerAutoLoginLink(sfContext::getInstance()->getUser()->getAttribute('contact_ref'), sfContext::getInstance()->getUser()->getAttribute('email')); ?>
 * </code>
 *
 * @todo @kludge This is a security risk, but as scheduler as a seperate site
 * is planned to be scrapped, this is seen as a temporary solution until
 * we migrate scheduler into mypwn
 *
 * @param int $contactRef
 * @param string $email
 *
 * @author Mark Wiseman
 * @return string
 *
 */
function getSchedulerAutoLoginLink($contactRef, $email) {
    return '/index/index/c/' . $contactRef .'/k/' . md5(date('dmY') . $email). '/e/' . $email;
}
