<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pwnFormFieldHelper
 *
 * @author marcin
 */

/**
 * render form field in pwn wway
 * 
 * @version 0.1
 * @author marcin
 * @param object $form
 * @param string $field
 * @param array $options
 * @return string 
 */
function render_form_field($form, $field, $options = array()) {

    return
            '<div class="form-field">'
            . $form[$field]->renderLabel()
            . '<span class="mypwn-input-container">'
            . $form[$field]->render($options)
            . '</span>'
            . '</div>';
}

