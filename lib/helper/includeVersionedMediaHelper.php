<?php
/**
 * Created by JetBrains PhpStorm.
 * User: michalmacierzynski
 * Date: 16/04/13
 */

/**
 * Includes stylesheets with ctime of files
 */
function include_versioned_stylesheets()
{
    $response = sfContext::getInstance()->getResponse();
    sfConfig::set('symfony.asset.stylesheets_included', true);
    $html = '';
    foreach ($response->getStylesheets() as $file => $options) {
        $filepath = sfConfig::get('sf_web_dir') . '/' .  stylesheet_path($file);
        if(file_exists($filepath)) {
            $file .= '?v=' . filectime($filepath);
        }
        $html .= stylesheet_tag($file, $options);
    }
    echo $html;
}

/**
 * Includes javascripts with ctime of files
 */
function include_versioned_javascripts()
{
    $response = sfContext::getInstance()->getResponse();
    sfConfig::set('symfony.asset.javascripts_included', true);
    $html = '';
    foreach ($response->getJavascripts() as $file => $options) {
        $filepath = sfConfig::get('sf_web_dir') . '/' .  javascript_path($file);
        if(file_exists($filepath)) {
            $file .= '?v=' . filectime($filepath);
        }
        $html .= javascript_include_tag($file, $options);
    }
    echo $html;
}

/**
 * This is the Contents of the CSS File only, used currently in the Worldpay Confirmation Pages
 */
function include_stylesheets_contents()
{
    $response = sfContext::getInstance()->getResponse();
    sfConfig::set('symfony.asset.stylesheets_included', true);
    $html = '<style type="text/css">';
    foreach ($response->getStylesheets() as $file => $options) {
        $filepath = sfConfig::get('sf_web_dir') . '/' .  stylesheet_path($file);
        if(file_exists($filepath)) {
            $contents = file_get_contents($filepath);
            $contents = include_stylesheets_contents_filters($contents);
            $html .= $contents . "\n";
        }

    }
    $html .= '</style>';

    echo $html;
}

function include_stylesheets_contents_filters($contents)
{
    if (strstr($contents, '/shared/fonts/rockwellstd')) {
        $domain = SiteURL::getSiteURL('GBR');

        if ($domain === 'dev.powwownow.co.uk') {
            $domain = 'devdemo.powwownow.co.uk';
        }

        $contents = str_replace(
            "url('/shared/fonts/rockwellstd-webfont.",
            "url('https://" . $domain . "/shared/fonts/rockwellstd-webfont.",
            $contents
        );
    }

    return $contents;
}
