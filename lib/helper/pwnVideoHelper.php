<?php
/**
 * pwnVideo Helper
 *
 * embed HTML5 videos with fallback to flash for legacy browsers
 * 
 * @version 1.0
 * @author Vitaly
 * @param array $options
 * @return string 
 */

/*  Parameters:
 *  $options['width']		- dimensions
 *  $options['height']		- dimensions
 *  $options['url_mp4'] 	- URL to mp4 video
 *	$options['url_webm']	- URL for webm
 *	$options['url_ogv']		- URL for ogv
 *	$options['url_image']	- URL to still image
 *	$options['title']		- Video title
 *
 *  use FQDN
 */
/**
 * @param array $options
 * @return string
 * @deprecated Please use output_video instead
 */
function embed_html5_video($options = array()) {
	// set defaults if not set
//    $options['width'] = (isset($options['width'])) ? $options['width'] : '460';
//	$options['height'] = (isset($options['height'])) ? $options['height'] : '260';
//	$options['url_mp4'] = (isset($options['url_mp4'])) ? $options['url_mp4'] : '';
//	$options['url_image'] = (isset($options['url_image'])) ? $options['url_image'] : '';
//	$options['title'] = (isset($options['title'])) ? $options['title'] : '';

    $videoCode = '<video controls="controls" poster="'. $options['url_image'] .'" width="'. $options['width'] .'" height="'. $options['height'] .'">';

    //MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7
    $videoCode .= (isset($options['url_mp4']) ? '<source src="'. $options['url_mp4'] .'" type="video/mp4" />' : '');

    // WebM/VP8 for Firefox4, Opera, and Chrome
	$videoCode .= (isset($options['url_webm']) ? '<source src="'. $options['url_webm'] .'" type="video/webm" />' : '');

	// Ogg/Vorbis for older Firefox and Opera versions
    $videoCode .= (isset($options['url_ogv']) ? '<source src="'. $options['url_ogv'] .'" type="video/ogg" />' : '');

    // not-valid W3C fallback for IE only
    $videoCode .= '<!--[if IE]>
                <object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="'. $options['width'] .'" height="'. $options['height'] .'">
                <![endif]-->
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" width="'. $options['width'] .'" height="'. $options['height'] .'">
                <!--<![endif]-->
                <param name="codebase" value="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" />
                <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
                <param name="allowFullScreen" value="true" />
                <param name="wmode" value="transparent" />
                <param name="flashVars" value="config={\'playlist\':[\'';

	$videoCode .= $options['url_image'];

	$videoCode .= "',{'url':'";

	$videoCode .= $options['url_mp4'];

	$videoCode .= '\',\'autoPlay\':false}]}" />';

    $videoCode .= '<img alt="'. $options['title'] .'" src="'. $options['url_image'] .'" width="'. $options['width'] .'" height="'. $options['height'] .'" title="No video playback capabilities" />
                </object>
            </video>';

	return $videoCode;

}

/**
 * @param array $options
 * @return string
 * @example
 *      <?php echo output_video (
 *           array (
 *              'config' =>
 *                  array(
 *                      'autoplay' => true,
 *                      'controls' => true
 *                  ),
 *                  'video' => array(
 *                      'mp4'   => '/sfvideo/pwn_3steps460x260_LR.mp4',
 *                      'webm'  => '/sfvideo/pwn_3steps460x260_LR.webm',
 *                      'ogg'   => '/sfvideo/pwn_3steps460x260_LR.ogv'
 *                  )
 *              )
 *       ); ?>
 */
function output_video($options = array())
{
    if (!empty($options)) {

        if (array_key_exists('config', $options)) {

            $config = $options['config'];

            $config['width'] = (isset($config['width'])) ? $config['width'] : '460';
            $config['height'] = (isset($config['height'])) ? $config['height'] : '260';

            if (isset($config['controls']) && (bool)$config['controls'] === true) {
                $config['controls'] = 'controls';
            } else {
                $config['controls'] = '';
            }
            if (isset($config['autoplay']) && (bool)$config['autoplay'] === true) {
                $config['autoplay'] = 'autoplay';
            } else {
                $config['autoplay'] = '';
            }

            if (!isset($config['preload']) || ($config['preload'] != 'auto' && $config['preload'] != 'metadata' && $config['preload'] != 'none')) {
                $config['preload'] = 'auto';
            }

            // Flash Config Defaults
            $config['allowscriptaccess'] = (isset($options['allowscriptaccess'])) ? $options['allowscriptaccess'] : 'always';
            $config['bgcolor'] = (isset($options['bgcolor'])) ? $options['bgcolor'] : '#fff';
            $config['allowfullscreen'] = (isset($options['allowfullscreen'])) ? $options['allowfullscreen'] : 'true';
        }
        if (array_key_exists('video', $options)) {

            $video = $options['video'];

            if (!array_key_exists('mp4',$video) && (empty($video['mp4']) || $video['mp4'] === null)) {
                return 'Failed Generating HTML5 Video Markup - MP4 Format Required';
            }
            if (!array_key_exists('mp4',$video) && (empty($video['webm']) || $video['mp4'] === null)) {
                return 'Failed Generating HTML5 Video Markup - WebM Format Required';
            }
            if (!array_key_exists('mp4',$video) && (empty($video['ogg']) || $video['mp4'] === null)) {
                return 'Failed Generating HTML5 Video Markup - Ogg Format Required';
            }

            // Optional - Still Image
            if (!isset($video['image'])) {
                $video['image'] = '';
            }
        }

        if (array_key_exists('called', $options)) {
            $called = $options['called'];
            $called = ($called == 'html') ? '</script>' : '<\/script>';
        } else {
            $called = '</script>';
        }

        $config['playerID'] = "altContent" . rand();

        return _video($config, $video, $called);
    }
}

/**
 * Generates HTML5 WC3 Valid Markup.
 * @param $config
 * @param $video
 * @param $called
 * @return string
 */
function _video($config, $video, $called)
{
    return <<<EOT
<script type="text/javascript" src="/shared/SWFObject/swfobject.js">{$called}
<script type="text/javascript">
    var flashvars = {skin: "/sfswf/mySkin.swf", video: "{$video['mp4']}", play: "true"};
    var params = {};
    var attributes = {};
    attributes.id = "flashContent";
    swfobject.embedSWF("/sfswf/player.swf?v1.3.5", "{$config['playerID']}", "{$config['width']}", "{$config['height']}", "10.0.0", false, flashvars, params, attributes);
{$called}
<video class="pwn-video-helper" width="{$config['width']}" height="{$config['height']}" poster="{$video['image']}" {$config['controls']} {$config['autoplay']} preload="${config['preload']}">
    <source type="video/mp4" src="{$video['mp4']}">
    <source type="video/webm" src="{$video['webm']}">
    <source type="video/ogg" src="{$video['ogg']}">

    <div id="{$config['playerID']}">
        <p>
            <strong>You do not have Flash installed, or it is older than the required 10.0.0. </strong><br>
            <strong>Click below to install the latest version and then try again.</strong><br>
            <a target="_blank" href="https://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/misc/images/160x41_get_flashplayer.gif" width="112" height="33" alt="Get Adobe Flash player">
            </a>
        </p>
    </div>
</video>
EOT;

}
