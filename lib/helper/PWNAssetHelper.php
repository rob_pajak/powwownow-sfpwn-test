<?php
/**
 * Header and Bottom of the File Javascript Includes Helper
 * @author Robert P
 * @author Asfer Tamimi
 */


/**
 * Include Javascript in the Header
 */
function pwn_include_javascript_header()
{
    echo pwn_get_javascript('header');
}

/**
 * Include Javascript in the Bottom of the File
 */
function pwn_include_javascript_bof()
{
    echo pwn_get_javascript('bof');
}

/**
 * Produce a List of Javascript Files along with the File Version, to keep a Cached version of the File
 * @param string $placement
 * @return string
 *
 */
function pwn_get_javascript($placement)
{
    /** @var sfWebResponse $response */
    $response = sfContext::getInstance()->getResponse();

    // Set Symfony Asset
    sfConfig::set('symfony.asset.javascripts_included', true);

    $html = '';

    foreach ($response->getJavascripts() as $file => $options) {
        $versionFile = (isset($options['version']) && !$options['version']) ? $options['version'] : true;

        // Obtain Version
        $filePath = sfConfig::get('sf_web_dir') . '/' . stylesheet_path($file);
        if (file_exists($filePath) && $versionFile) {
            $file .= '?v=' . filectime($filePath);
        }

        if (isset($options['version'])) {
            unset($options['version']);
        }
        $options['type'] = 'text/javascript';

        // Check Placement - Header
        if ((isset($options['header']) && $options['header'] === true) && $placement == 'header') {
            unset($options['header']);
            $html .= javascript_include_tag_html5($file, $options);
        }

        // Check Placement - Bottom of the File
        if ((!isset($options['header']) || $options['header'] !== true) && $placement == 'bof') {
            if (isset($options['header'])) {
                unset($options['header']);
            }
            $html .= javascript_include_tag_html5($file, $options);
        }
    }

    return $html;
}


/**
 * Returns a <script> include tag per source given as argument.
 *
 * <b>Examples:</b>
 * <code>
 *  echo javascript_include_tag('xmlhr');
 *    => <script language="JavaScript" type="text/javascript" src="/js/xmlhr.js"></script>
 *  echo javascript_include_tag('common.javascript', '/elsewhere/cools');
 *    => <script language="JavaScript" type="text/javascript" src="/js/common.javascript"></script>
 *       <script language="JavaScript" type="text/javascript" src="/elsewhere/cools.js"></script>
 * </code>
 *
 * @return string XHTML compliant <script> tag(s)
 * @see    javascript_path
 */
function javascript_include_tag_html5()
{
    $sources       = func_get_args();
    $sourceOptions = (func_num_args() > 1 && is_array($sources[func_num_args() - 1])) ? array_pop($sources) : array();

    $html = '';
    foreach ($sources as $source) {
        $absolute = false;
        if (isset($sourceOptions['absolute'])) {
            unset($sourceOptions['absolute']);
            $absolute = true;
        }

        $condition = null;
        if (isset($sourceOptions['condition'])) {
            $condition = $sourceOptions['condition'];
            unset($sourceOptions['condition']);
        }

        if (!isset($sourceOptions['raw_name'])) {
            $source = javascript_path($source, $absolute);
        } else {
            unset($sourceOptions['raw_name']);
        }

        $options = array_merge(array('src' => $source), $sourceOptions);
        $tag     = content_tag('script', '', $options);

        if (null !== $condition) {
            $tag = comment_as_conditional($condition, $tag);
        }

        $html .= $tag . "\n";
    }

    return $html;
}

/**
 * Returns a <link> tag for the canonical link
 *
 * <b>Examples:</b>
 * <code>
 *  echo include_canonical_tag('/PathName/PageName');
 *    => <link rel="canonical" href="PROTOCOL://HOST/PathName/PageName"/>
 * </code>
 *
 * @return string HTML5 compliant <script> tag(s)
 */
function include_canonical_tag()
{
    $url    = sfContext::getInstance()->getRouting()->getCurrentInternalUri(true);
    $html   = array();
    $html[] = (int)$_SERVER['SERVER_PORT'] === 80 ? 'http://' : 'https://';
    $html[] = $_SERVER['SERVER_NAME'];
    $html[] = url_for($url);

    $html = '<link rel="canonical" href="' . implode('', $html) . '"/>' . "\n";

    return $html;
}
