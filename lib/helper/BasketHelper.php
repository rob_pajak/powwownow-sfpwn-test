<?php
/**
 * View helper functions for accessing the basket.
 *
 * @author Maarten Jacobs
 */

/**
 * Assert that the product has been marked for assignment.
 *
 * @param int $productId
 * @return bool
 */
function hasProductBeenMarkedAsAdded($productId) {
    $basket = new Basket(sfContext::getInstance()->getUser());
    return $basket->hasProductBeenMarkedForAddition($productId);
}

/**
 * Determines whether the label of the add button should be 'Add' or 'Pending', depending on the status of the basket.
 *
 * @param int $productId
 * @return string
 */
function determineAddLabel($productId) {
    if (hasProductBeenMarkedAsAdded($productId)) {
        return 'Added to Basket';
    }
    return 'Add to Basket';
}

/**
 * Determines whether to show the continue button on page load.
 *
 * @param bool $isPlusAdmin
 * @return bool
 */
function showContinueButtonInitially($isPlusAdmin) {
    // If the user is not a Plus admin, he does not relate to the Basket.
    // So always show the continue button.
    if (!$isPlusAdmin) {
        return true;
    }

    // If the basket is empty, don't show the continue button.
    $basket = new Basket(sfContext::getInstance()->getUser());
    if ($basket->isEmpty()) {
        return false;
    }

    return true;
}

/**
 * Generates the label for the add-BWM button.
 *
 * @return string
 */
function addBWMButtonLabel() {
    return 'Select';
}

/**
 * Calculates and returns the setup cost of the basket.
 *
 * @return float
 */
function totalBasketCost() {
    $basket = new Basket(sfContext::getInstance()->getUser());
    return abs($basket->calculateTotalCost());
}
