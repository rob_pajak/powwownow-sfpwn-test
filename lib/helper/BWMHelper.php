<?php
/**
 * Common functions for displaying BWMs.
 */

/**
 * In case of a BWM product, forms a generic description including the types of numbers.
 * In case the product is not a BWM, returns the group_summary.
 *
 * @param array $bwmProduct
 * @return string
 */
function formBWMDescription(array $bwmProduct) {
    if (!isset($bwmProduct['is_BWM']) || !$bwmProduct['is_BWM']) {
        return $bwmProduct['group_summary'];
    }
    $dnises = $bwmProduct['dnises'];

    // Form the 'consists of' part of the description:
    // Collect the types of dnises and the count of each assigned to the product.
    $typeCountMap = array();
    foreach ($dnises as $dnis) {
        $type = translateDNISType($dnis['dnis_type']);
        if (!isset($typeCountMap[$type])) {
            $typeCountMap[$type] = 0;
        }
        $typeCountMap[$type]++;
    }

    return concatenateDNISTypes($typeCountMap);
}

/**
 * Forms a description for a new BWM, which does not have any definite numbers yet.
 *
 * @param array $dnises
 * @return string
 */
function formNewBWMDescription(array $dnises) {
    // Form the type count map.
    $typeCountMap = array();
    foreach ($dnises as $dnis) {
        if (!empty($dnis['type'])) {
            $type = translateDNISType($dnis['type']);
            if (!isset($typeCountMap[$type])) {
                $typeCountMap[$type] = 0;
            }
            $typeCountMap[$type]++;
        }
    }

    return concatenateDNISTypes($typeCountMap);
}

/**
 * Concatenates the calculated types with their respective counts.
 *
 * @param array $typeCountMap
 * @return string
 */
function concatenateDNISTypes(array $typeCountMap) {
    // Form the type count parts of the phrase.
    // That is, "2 [type] numbers" or "1 [type] number".
    $types = array();
    foreach ($typeCountMap as $type => $count) {
        $numberSuffix = 'number';
        if ($count > 1) {
            $numberSuffix .= 's';
        }
        $types[] = sprintf("%d %s %s", $count, $type, $numberSuffix);
    }

    // Concatenate the types and counts into a readable sentence that lists them.
    // i.e. "Your Branded Welcome Message consists of 2 Freephone numbers, 4 Landline numbers and 1 Shared Cost number."
    // i.e. "Your Branded Welcome Message consists of 2 Freephone numbers and 1 Shared Cost number."
    switch (count($types)) {
        case 0:
            $typePhrase = 'no numbers';
            break;
        case 1:
            $typePhrase = $types[0];
            break;
        case 2:
            $typePhrase = implode(' and ', $types);
            break;
        default:
            $latterType = array_pop($types);
            $typePhrase = implode(', ', $types);
            $typePhrase .= ' and ' . $latterType;
            break;
    }

    // Form the description completeness.
    $description = sprintf(
        "Your Branded Welcome Message consists of %s.",
        $typePhrase
    );

    return $description;
}

/**
 * Translates the DNIS types for display.
 *
 * i.e. Geographic to Landline.
 *
 * @param string $type
 * @return string
 */
function translateDNISType($type) {
    // Handle the 'Geographic' type-case:
    if (strcasecmp($type, 'Geographic') === 0) {
        return 'Landline';
    }
    return $type;
}
