<?php
/**
 * Hermes Functional Caching
 *
 * @author Robert Pajak (Added the Add Caching
 * @author Asfer Tamimi (Added Post Methods and the Removal of Cachine)
 *
 */

/**
 * Call Hermes with Caching. Can be done using Array, XML and JSON. Can also use POST Methods too.
 *
 * @param string $method
 * @param array $args
 * @param string $responseType
 * @param int $lifetime
 * @param bool $doPost
 * @return array $ret
 * @throws Exception
 * @author Robert Pajak
 * @author Asfer Tamimi
 */
function hermesCallWithCaching($method, $args, $responseType = 'array', $lifetime = 3600, $doPost = false)
{
    $cache = new sfFileCache(array(
        'cache_dir' => sfConfig::get('sf_cache_dir') . '/function',
        'lifetime'  => $lifetime
    ));
    $fc    = new sfFunctionCache($cache);

    try {
        if ($doPost) {
            if ($responseType == 'array') {
                $ret = $fc->call('Hermes_Client_Rest::callPOST', array($method, $args));
            } elseif ($responseType == 'json') {
                $ret = $fc->call('Hermes_Client_Rest::callPOSTJson', array($method, $args));
            } elseif ($responseType == 'xml') {
                $ret = $fc->call('Hermes_Client_Rest::callPOSTXml', array($method, $args));
            } else {
                throw new Exception ('Only array/json/xml Hermes responses are supported');
            }
        } else {
            if ($responseType == 'array') {
                $ret = $fc->call('Hermes_Client_Rest::call', array($method, $args));
            } elseif ($responseType == 'json') {
                $ret = $fc->call('Hermes_Client_Rest::callJson', array($method, $args));
            } elseif ($responseType == 'xml') {
                $ret = $fc->call('Hermes_Client_Rest::callXml', array($method, $args));
            } else {
                throw new Exception ('Only array/json/xml Hermes responses are supported');
            }
        }
    } catch (Exception $e) {
        sfContext::getInstance()->getLogger()->err(
            "${method} Hermes Method failed. Error Message: " . $e->getMessage() . ', Error Code: ' . $e->getCode()
        );
        throw $e;
    }
    return $ret;
}

/**
 * Remove Caching from a Hermes Call, which had Caching. Will return false if Caching did not exist
 *
 * @param string $method
 * @param array $args
 * @param string $responseType
 * @param bool $doPost
 * @return bool
 * @throws Exception
 * @author Asfer Tamimi
 *
 */
function hermesCallRemoveCaching($method, $args, $responseType = 'array', $doPost = false)
{
    try {
        $cache = new sfFileCache(array('cache_dir' => sfConfig::get('sf_cache_dir') . '/function'), 3600);
        $fc    = new sfFunctionCache($cache);

        if ($doPost) {
            if ($responseType == 'array') {
                $key = $fc->computeCacheKey('Hermes_Client_Rest::callPOST', array($method, $args));
                return $cache->remove($key);
            } elseif ($responseType == 'json') {
                $key = $fc->computeCacheKey('Hermes_Client_Rest::callPOSTJson', array($method, $args));
                return $cache->remove($key);
            } elseif ($responseType == 'xml') {
                $key = $fc->computeCacheKey('Hermes_Client_Rest::callPOSTXml', array($method, $args));
                return $cache->remove($key);
            } elseif ($responseType == 'all') {
                $key1 = $fc->computeCacheKey('Hermes_Client_Rest::callPOST', array($method, $args));
                $key2 = $fc->computeCacheKey('Hermes_Client_Rest::callPOSTJson', array($method, $args));
                $key3 = $fc->computeCacheKey('Hermes_Client_Rest::callPOSTXml', array($method, $args));
                $clear1 = $cache->remove($key1);
                $clear2 = $cache->remove($key2);
                $clear3 = $cache->remove($key3);
                $clear = (true === $clear1 || true === $clear2 || true === $clear3) ? true : false;
                return $clear;
            } else {
                throw new Exception ('Only array/json/xml/all Hermes responses are supported');
            }
        } else {
            if ($responseType == 'array') {
                $key = $fc->computeCacheKey('Hermes_Client_Rest::call', array($method, $args));
                return $cache->remove($key);
            } elseif ($responseType == 'json') {
                $key = $fc->computeCacheKey('Hermes_Client_Rest::callJson', array($method, $args));
                return $cache->remove($key);
            } elseif ($responseType == 'xml') {
                $key = $fc->computeCacheKey('Hermes_Client_Rest::callXml', array($method, $args));
                return $cache->remove($key);
            } elseif ($responseType == 'all') {
                $key1 = $fc->computeCacheKey('Hermes_Client_Rest::call', array($method, $args));
                $key2 = $fc->computeCacheKey('Hermes_Client_Rest::callJson', array($method, $args));
                $key3 = $fc->computeCacheKey('Hermes_Client_Rest::callXml', array($method, $args));
                $clear1 = $cache->remove($key1);
                $clear2 = $cache->remove($key2);
                $clear3 = $cache->remove($key3);
                $clear = (true === $clear1 || true === $clear2 || true === $clear3) ? true : false;
                return $clear;
            } else {
                throw new Exception ('Only array/json/xml/all Hermes responses are supported');
            }
        }
    } catch (Exception $e) {
        sfContext::getInstance()->getLogger()->err(
            "${method} Hermes Method Failed, using args: " . $args . ", Error: " . serialize($e)
        );
        throw $e;
    }
}

