<?php

class PwnSocialUser
{
    static public $allowedSocialMedia = array('LinkedIn','Google','Facebook');

    private $media;
    private $id;
    private $first_name;
    private $last_name;
    private $email;
    private $formatted_name;
    private $date_of_birth;
    private $gender;
    private $about_me;
    private $phone_number;
    private $preferred_username;
    private $known_language;
    private $education_history;
    private $work_history;
    private $interest;
    private $location;

    /**
     * Constructor, media is required and must be in one of allowed social media
     *
     * @param $media

     * @throws Exception
     */
    public function __construct($media)
    {
        if (!in_array($media,self::$allowedSocialMedia))
            throw new Exception('Not allowed socia media');

        $this->media = $media;
    }

    public function resetToken()
    {
        $client = new oauth_client_class;

        $client->server = $this->media;
        $success = $client->Initialize();

        $client->ResetAccessToken();
    }
    /**
     * Links given contact_ref with social media
     *
     * @param $contactRef
     *
     * @throws Exception
     */
    public function linkAccount($contactRef)
    {
        if ((string) $this->id == '')
            throw new Exception('Social id is not provided');

        Hermes_Client_Rest::call(
            'Social.updateOAuthUser',
            array(
                'social_id'     => $this->id,
                'server'        => $this->media,
                'contact_ref'   => $contactRef
            )
        );
    }

    /**
     * Updates social user data in db
     *
     * @throws Exception
     */
    public function updateSocialUserData()
    {
        if ((string) $this->id == '')
            throw new Exception('Social id is not provided');
        $contact_ref = $this->getContactRefByOAuthUser();

        $socialUser = $this->getSocialUserData();
        unset($socialUser['id']);
        Hermes_Client_Rest::call(
            'Social.updateOAuthUserData',
            array_merge(
                array(
                    'oauth_user_id'   => $contact_ref['id'],
                ),
                $socialUser
            )
        );

    }

    /**
     * Return array of email, contact_ref for given social id and media
     *
     * @return array
     * @throws Exception
     */
    public function getContactRefByOAuthUser()
    {
        if ((string) $this->id == '')
            throw new Exception('Social id is not provided');

        $contact_ref = Hermes_Client_Rest::call(
            'Social.getContactRefByOAuthUser',
            array(
                'social_id'     => $this->id,
                'server'        => $this->media,
            )
        );

        return $contact_ref;
    }

    /**
     * This function retrieves data from social media
     * If it is necessary call oauth dialog
     * If client flag exit is true script needs to perform exit because
     *   client tries to performs redirection to oauth authorization dialog
     *
     * @return bool
     * @throws Exception
     */
    public function retrieveDataFromSocialMedia()
    {
        require_once(sfConfig::get('sf_lib_dir').'/http.php');
        require_once(sfConfig::get('sf_lib_dir').'/oauth_client.php');

        $client = new oauth_client_class;

        $client->server = $this->media;

        $client->redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].'/login/SocialConnect/'.$this->media;

        $conf = sfConfig::get('app_social_'.strtolower($this->media));
        $client->client_id = $conf['client_id'];
        $client->client_secret = $conf['client_secret'];

        $client->debug = true;
        $client->debug_http = true;

        switch ($this->media) {
            case "Facebook":
                $client->scope = 'email user_education_history user_work_history user_hometown '.
                    'user_location user_about_me user_birthday user_interests';
                break;
            case "Google":
                $client->scope = 'https://www.googleapis.com/auth/userinfo.email '.
                    'https://www.googleapis.com/auth/userinfo.profile ' .
                    'https://www.googleapis.com/auth/plus.me';
                break;
            case "LinkedIn":
                $client->scope = 'r_fullprofile r_emailaddress r_contactinfo ';
                break;
        }

        $success = $client->Initialize();
        if ($success) {
            $success = $client->Process();
            if ($success) {
                switch ($this->media) {
                    case "Facebook":
                        $success = $client->CallAPI(
                            'https://graph.facebook.com/me',
                            'GET', array(), array('FailOnAccessError'=>true), $user);

                        if($client->exit)
                            exit;

                        if ((string) $user->id == '')
                            throw new Exception('Error calling API');

                        $dob_split = explode('/',$user->birthday);
                        $this->id = $user->id;
                        $this->first_name = $user->first_name;
                        $this->last_name = $user->last_name;
                        $this->email = $user->email;
                        $this->formatted_name = $user->username;
                        $this->date_of_birth = (count($dob_split)==3?$dob_split[2].'-'.$dob_split[0].'-'.$dob_split[1]:null );
                        $this->gender = $user->gender;
                        $this->about_me = $user->bio;
                        $this->phone_number = null;
                        $this->preferred_username = $user->username;
                        $this->known_language = $user->locale;
                        $this->education_history = (isset($user->education[0]->school->name)?$user->education[0]->school->name:null);
                        $this->work_history = (isset($user->work[0]->employer->name)?$user->work[0]->employer->name:null);
                        $this->interest = null;
                        $this->location = $user->location->name;
                        return true;
                        break;
                    case "Google":
                        $success = $client->CallAPI(
                            'https://www.googleapis.com/plus/v1/people/me',
                            'GET', array(), array('FailOnAccessError'=>true), $user);
                        $success = $client->CallAPI(
                            'https://www.googleapis.com/oauth2/v1/userinfo',
                            'GET', array(), array('FailOnAccessError'=>true), $userInfo);

                        if($client->exit)
                            exit;

                        if ((string) $userInfo->id == '')
                            throw new Exception('Error calling API');

                        if (isset($user->organizations) && count($user->organizations) > 0) {
                            foreach ($user->organizations as $_org) {
                                switch ($_org->type) {
                                    case "school":
                                        if (!isset($education) || (isset($education) && $_org->primary == 1))
                                            $education = $_org->name;
                                        break;
                                    case "work":
                                        if (!isset($work) || (isset($work) && $_org->primary == 1))
                                            $work = $_org->name;
                                        break;
                                }
                            }
                        }
                        if (!isset($education)) $education = null;
                        if (!isset($work)) $work = null;

                        if (isset($user->placesLived) && count($user->placesLived) > 0) {
                            foreach ($user->placesLived as $_loc) {
                                if (!isset($location) || (isset($location) && $_loc->primary == 1))
                                    $location = $_loc->value;
                            }
                        }
                        if (!isset($location)) $location = null;

                        $this->id = $userInfo->id;
                        $this->first_name = $userInfo->given_name;
                        $this->last_name = $userInfo->family_name;
                        $this->email = $userInfo->email;
                        $this->formatted_name = $userInfo->name;
                        $this->date_of_birth = $userInfo->birthday;
                        $this->gender = $userInfo->gender;
                        $this->about_me = $user->aboutMe;
                        $this->phone_number = null;
                        $this->preferred_username = $user->username;
                        $this->known_language = $userInfo->locale;
                        $this->education_history = $education;
                        $this->work_history = $work;
                        $this->interest = null;
                        $this->location = $location;
                        return true;
                        break;
                    case "LinkedIn":
                        $success = $client->CallAPI(
                            'http://api.linkedin.com/v1/people/~:(id,first-name,last-name,industry,formatted-name,headline'.
                            ',email-address,phone-numbers,languages,educations,interests,location:(name),main-address)',
                            'GET', array(
                                'format'=>'json'
                            ), array('FailOnAccessError'=>true), $user);

                        if($client->exit)
                            exit;

                        if ((string) $user->id == '')
                            throw new Exception('Error calling API');

                        if (isset($user->educations->values) && count($user->educations->values) > 0) {
                            foreach ($user->educations->values as $_org) {
                                $education = $_org->schoolName;
                                break;
                            }
                        }
                        if (!isset($education)) $education = null;

                        $this->id = $user->id;
                        $this->first_name = $user->firstName;
                        $this->last_name = $user->lastName;
                        $this->email = $user->emailAddress;
                        $this->formatted_name = $user->formattedName;
                        $this->date_of_birth = null;
                        $this->gender = null;
                        $this->about_me = null;
                        $this->phone_number = null;
                        $this->preferred_username =  null;
                        $this->known_language =  null;
                        $this->education_history = $education;
                        $this->work_history = $user->headline;
                        $this->interest = null;
                        $this->location = $user->location->name;
                        return true;
                        break;
                }
            }
        }
        if($client->exit)
            exit;

        throw new Exception('Error retrieving data');
    }

    /**
     * Returns all social user data in array
     *
     * @return array
     */
    public function getSocialUserData()
    {
        return array(
            'id'                    => $this->id,
            'first_name'            => $this->first_name,
            'last_name'             => $this->last_name,
            'email'                 => $this->email,
            'formatted_name'        => $this->formatted_name,
            'date_of_birth'         => $this->date_of_birth,
            'gender'                => $this->gender,
            'about_me'              => $this->about_me,
            'phone_number'          => $this->phone_number,
            'preferred_username'    => $this->preferred_username,
            'known_language'        => $this->known_language,
            'education_history'     => $this->education_history,
            'work_history'          => $this->work_history,
            'interest'              => $this->interest,
            'location'              => $this->location,

        );
    }
}