<?php
require_once '/usr/share/php/symfony/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration {
    public function setup() {
        $this->enablePlugins('sfDoctrinePlugin');
        sfWidgetFormSchema::setDefaultFormFormatterName('pwn');

        $env = sfConfig::get('sf_environment');
        if ( $env === 'test' || php_sapi_name() === 'cli' ) {
//            $this->enablePlugins('sfPhpunitPlugin');
            $this->enablePlugins('sfJwtPhpUnitPlugin');
        }
    }
}
